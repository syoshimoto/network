using UnityEngine;
using System.Collections;

public class ThirdPersonNetwork : Photon.MonoBehaviour
{
    ThirdPersonCamera cameraScript;
    ThirdPersonController controllerScript;
    PlayerController playerController;
    TopCamera topCamera;

    void Awake()
    {
        cameraScript = GetComponent<ThirdPersonCamera>();
        controllerScript = GetComponent<ThirdPersonController>();
        topCamera = GetComponent<TopCamera>();
        playerController = GetComponent<PlayerController>();

         if (photonView.isMine)
        {
            controllerScript.enabled = true;
        }
        else
        {           
            controllerScript.enabled = true;
            controllerScript.isControllable = false;
        }
        cameraScript.enabled = false;

        gameObject.name = gameObject.name + photonView.viewID;
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //We own this player: send the others our data
            // stream.SendNext((int)controllerScript._characterState);
            // stream.SendNext(transform.position);
            // stream.SendNext(transform.rotation);
            // stream.SendNext(playerController.GetHp()); 
        }
        else
        {
            //Network player, receive data
            // controllerScript._characterState = (CharacterState)(int)stream.ReceiveNext();
            // correctPlayerPos = (Vector3)stream.ReceiveNext();
            // correctPlayerRot = (Quaternion)stream.ReceiveNext();
            // playerController.SetHp((int)stream.ReceiveNext());
        }
    }

    private Vector3 correctPlayerPos = Vector3.zero; //We lerp towards this
    private Quaternion correctPlayerRot = Quaternion.identity; //We lerp towards this

}