﻿using UnityEngine;
using System.Collections;

public class Scene
{
	public static string Title = "TitleScene";
	public static string TearcherStart = "TeacherStartScene";
	public static string Maching = "MachingScene";
	public static string Battle = "BattleScene";
	public static string BattleWatching = "BattleWatchingScene";
	public static string Description = "DescriptionScene";
}
