﻿using UnityEngine;
using System.Collections;

public class MainCanvas : MonoBehaviour
{
	[SerializeField]
	UIPlayer prefabPlayer;
	int count = 0;
	private void Start()
	{
		PlayerManager.GetInstance().SetCanvas(this);
		CreatePlayer(PlayerManager.GetInstance().GetMyPlayerNetwork());
	}
	public void CreatePlayer(PlayerNetwork network)
	{
		var obj = Instantiate(Resources.Load("prefabs/UIPlayer"), Vector3.zero, Quaternion.identity) as GameObject;
		obj.transform.SetParent(transform);
		obj.transform.localPosition = new Vector3(0, count * 140, 0);
		UIPlayer player = obj.GetComponent<UIPlayer>();
		player.SetPlayerNetwork(network);
		count++;
	}
	public void AddTemp()
	{
		// PlayerManager.GetInstance().GetMyPlayerNetwork().playerController.AddHp();
		// PlayerManager.GetInstance().GetMyPlayerNetwork().transform.position += new Vector3(0.3f, 0, 0);
	}
}
