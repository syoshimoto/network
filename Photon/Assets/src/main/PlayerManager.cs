﻿using UnityEngine;
using System.Collections;

public class PlayerManager
{
	private PlayerNetwork myPlayerNetwork;
	private MainCanvas canvas;
	public void AddPlayer(PlayerNetwork player)
	{
		canvas.CreatePlayer(player);
	}
	public void AddMyPlayer(PlayerNetwork player)
	{
		myPlayerNetwork = player;
	}
	public PlayerNetwork GetMyPlayerNetwork() { return myPlayerNetwork;}
	public void SetCanvas(MainCanvas canvas)
	{
		this.canvas = canvas;
	}

	public static PlayerManager GetInstance()
	{
		if (mInstance == null)
		{
			mInstance = new PlayerManager();
		}
		return mInstance;
	}
	private PlayerManager()
	{
	}
	private static PlayerManager mInstance;
}
