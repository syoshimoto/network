﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPlayer : MonoBehaviour
{
	[SerializeField]
	private Text name;
	[SerializeField]
	private Text hp;
	PlayerNetwork network;
	public void SetHp(int hp)
	{
		this.hp.text = hp.ToString();
	}	
	public void SetName(string text)
	{
		this.name.text = text;
	}
	public void SetPlayerNetwork(PlayerNetwork network)
	{
		this.network = network;
	}
	private void Update()
	{
		// SetHp(network.playerController.GetHp());
		// SetName("id" + network.photonView.viewID.ToString());
	}
}
