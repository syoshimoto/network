﻿using UnityEngine;
using System.Collections;

public enum GameState
{
    Start,
    Battle,
    Description,
    Result,
}

public class DataManager
{
	public static DataManager GetInstance()
	{
		if (mInstance == null)
		{
			mInstance = new DataManager();
		}
		return mInstance;
	}
	private static DataManager mInstance;
	private DataManager(){}	

    private GameState mState;
    public GameState State { get { return mState;}}
    private bool isTeacher;
    public void SetIsTeacher(bool isTeacher) { this.isTeacher = isTeacher;}
    public bool IsTeacher { get { return isTeacher;}}
    public PlayerNetwork playerNetwork;

	public void Initialize(PlayerNetwork playerNetwork)
	{
		mState = GameState.Start;
		this.playerNetwork = playerNetwork;
	}
	public void ChangeToBattle()
	{
		mState = GameState.Battle;
		if (isTeacher)
		{
	        PhotonNetwork.LoadLevel(Scene.Battle);
	    }
	    else
	    {
	        PhotonNetwork.LoadLevel(Scene.BattleWatching);
	    }
	}
	public void ChangeToDescription()
	{
		mState = GameState.Description;
	    PhotonNetwork.LoadLevel(Scene.Description);
	}
}
