﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerNetwork : Photon.MonoBehaviour
{
    public PlayerController playerController;

    public List<int> answers;

    void Awake()
    {
        playerController = GetComponent<PlayerController>();
         if (photonView.isMine)
        {
            PlayerManager.GetInstance().AddMyPlayer(this);
            DataManager.GetInstance().Initialize(this);
        }
        else
        {           
            PlayerManager.GetInstance().AddPlayer(this);
        }
        gameObject.name = gameObject.name + photonView.viewID;
        DontDestroyOnLoad(this);
        answers = new List<int>();
        for (int i = 0; i < 10; i++)
        {
            answers.Add(0);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        switch(DataManager.GetInstance().State)
        {
        case GameState.Start:
            UpdateStart(stream, info);
            break;
        case GameState.Battle:
            UpdateBattle(stream, info);
            break;
        }
    }
    private void UpdateStart(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(DataManager.GetInstance().State); 
        }
        else
        {
            GameState state = (GameState)stream.ReceiveNext();
            if (state == GameState.Battle)
            {
                DataManager.GetInstance().ChangeToBattle();
            }
        }
    }
    private void UpdateBattle(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(DataManager.GetInstance().State); 
            foreach(var answer in answers)
            {
                stream.SendNext(answer);
            }
        }
        else
        {
            GameState state = (GameState)stream.ReceiveNext();
            if (state == GameState.Description)
            {
                DataManager.GetInstance().ChangeToDescription();
            }
            int i = 0;
            foreach(var answer in answers)
            {
                answers[i] = (int)stream.ReceiveNext();
                i++;
            }
        }
    }
}