﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// ExitGames.Client.Photon.Chat.ChatChannel[]
// ExitGames.Client.Photon.Chat.ChatChannel[]
struct ChatChannelU5BU5D_t7_15  : public Array_t { };
