﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// PhotonPlayer[]
// PhotonPlayer[]
struct PhotonPlayerU5BU5D_t8_101  : public Array_t { };
// FriendInfo[]
// FriendInfo[]
struct FriendInfoU5BU5D_t8_182  : public Array_t { };
// PhotonAnimatorView/SynchronizedParameter[]
// PhotonAnimatorView/SynchronizedParameter[]
struct SynchronizedParameterU5BU5D_t8_183  : public Array_t { };
// PickupItem[]
// PickupItem[]
struct PickupItemU5BU5D_t8_181  : public Array_t { };
// PunTeams/Team[]
// PunTeams/Team[]
struct TeamU5BU5D_t8_184  : public Array_t { };
// CubeInter/State[]
// CubeInter/State[]
struct StateU5BU5D_t8_43  : public Array_t { };
// RoomInfo[]
// RoomInfo[]
struct RoomInfoU5BU5D_t8_99  : public Array_t { };
// PhotonView[]
// PhotonView[]
struct PhotonViewU5BU5D_t8_180  : public Array_t { };
// Photon.MonoBehaviour[]
// Photon.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t8_188  : public Array_t { };
// Region[]
// Region[]
struct RegionU5BU5D_t8_185  : public Array_t { };
// TypedLobbyInfo[]
// TypedLobbyInfo[]
struct TypedLobbyInfoU5BU5D_t8_186  : public Array_t { };
// PhotonAnimatorView/SynchronizedLayer[]
// PhotonAnimatorView/SynchronizedLayer[]
struct SynchronizedLayerU5BU5D_t8_187  : public Array_t { };
