﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.Chat.ChatChannel
struct ChatChannel_t7_1;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.String[]
struct StringU5BU5D_t1_202;
// System.Object[]
struct ObjectU5BU5D_t1_157;

#include "codegen/il2cpp-codegen.h"

// System.Void ExitGames.Client.Photon.Chat.ChatChannel::.ctor(System.String)
extern "C" void ChatChannel__ctor_m7_0 (ChatChannel_t7_1 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatChannel::get_IsPrivate()
extern "C" bool ChatChannel_get_IsPrivate_m7_1 (ChatChannel_t7_1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatChannel::set_IsPrivate(System.Boolean)
extern "C" void ChatChannel_set_IsPrivate_m7_2 (ChatChannel_t7_1 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.Chat.ChatChannel::get_MessageCount()
extern "C" int32_t ChatChannel_get_MessageCount_m7_3 (ChatChannel_t7_1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatChannel::Add(System.String,System.Object)
extern "C" void ChatChannel_Add_m7_4 (ChatChannel_t7_1 * __this, String_t* ___sender, Object_t * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatChannel::Add(System.String[],System.Object[])
extern "C" void ChatChannel_Add_m7_5 (ChatChannel_t7_1 * __this, StringU5BU5D_t1_202* ___senders, ObjectU5BU5D_t1_157* ___messages, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatChannel::ClearMessages()
extern "C" void ChatChannel_ClearMessages_m7_6 (ChatChannel_t7_1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.Chat.ChatChannel::ToStringMessages()
extern "C" String_t* ChatChannel_ToStringMessages_m7_7 (ChatChannel_t7_1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
