﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.Chat.ChatClient
struct ChatClient_t7_2;
// ExitGames.Client.Photon.Chat.IChatClientListener
struct IChatClientListener_t7_5;
// System.String
struct String_t;
// ExitGames.Client.Photon.EventData
struct EventData_t5_44;
// ExitGames.Client.Photon.OperationResponse
struct OperationResponse_t5_43;
// ExitGames.Client.Photon.Chat.AuthenticationValues
struct AuthenticationValues_t7_4;
// System.String[]
struct StringU5BU5D_t1_202;
// System.Object
struct Object_t;
// ExitGames.Client.Photon.Chat.ChatChannel
struct ChatChannel_t7_1;

#include "codegen/il2cpp-codegen.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DebugLevel.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_StatusCode.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_1.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_2.h"

// System.Void ExitGames.Client.Photon.Chat.ChatClient::.ctor(ExitGames.Client.Photon.Chat.IChatClientListener,ExitGames.Client.Photon.ConnectionProtocol)
extern "C" void ChatClient__ctor_m7_8 (ChatClient_t7_2 * __this, Object_t * ___listener, uint8_t ___protocol, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::ExitGames.Client.Photon.IPhotonPeerListener.DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern "C" void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_DebugReturn_m7_9 (ChatClient_t7_2 * __this, uint8_t ___level, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::ExitGames.Client.Photon.IPhotonPeerListener.OnEvent(ExitGames.Client.Photon.EventData)
extern "C" void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnEvent_m7_10 (ChatClient_t7_2 * __this, EventData_t5_44 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::ExitGames.Client.Photon.IPhotonPeerListener.OnOperationResponse(ExitGames.Client.Photon.OperationResponse)
extern "C" void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnOperationResponse_m7_11 (ChatClient_t7_2 * __this, OperationResponse_t5_43 * ___operationResponse, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::ExitGames.Client.Photon.IPhotonPeerListener.OnStatusChanged(ExitGames.Client.Photon.StatusCode)
extern "C" void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnStatusChanged_m7_12 (ChatClient_t7_2 * __this, int32_t ___statusCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.Chat.ChatClient::get_NameServerAddress()
extern "C" String_t* ChatClient_get_NameServerAddress_m7_13 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_NameServerAddress(System.String)
extern "C" void ChatClient_set_NameServerAddress_m7_14 (ChatClient_t7_2 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.Chat.ChatClient::get_FrontendAddress()
extern "C" String_t* ChatClient_get_FrontendAddress_m7_15 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_FrontendAddress(System.String)
extern "C" void ChatClient_set_FrontendAddress_m7_16 (ChatClient_t7_2 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.Chat.ChatClient::get_ChatRegion()
extern "C" String_t* ChatClient_get_ChatRegion_m7_17 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_ChatRegion(System.String)
extern "C" void ChatClient_set_ChatRegion_m7_18 (ChatClient_t7_2 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.Chat.ChatState ExitGames.Client.Photon.Chat.ChatClient::get_State()
extern "C" int32_t ChatClient_get_State_m7_19 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_State(ExitGames.Client.Photon.Chat.ChatState)
extern "C" void ChatClient_set_State_m7_20 (ChatClient_t7_2 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.Chat.ChatDisconnectCause ExitGames.Client.Photon.Chat.ChatClient::get_DisconnectedCause()
extern "C" int32_t ChatClient_get_DisconnectedCause_m7_21 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_DisconnectedCause(ExitGames.Client.Photon.Chat.ChatDisconnectCause)
extern "C" void ChatClient_set_DisconnectedCause_m7_22 (ChatClient_t7_2 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::get_CanChat()
extern "C" bool ChatClient_get_CanChat_m7_23 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::get_HasPeer()
extern "C" bool ChatClient_get_HasPeer_m7_24 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.Chat.ChatClient::get_AppVersion()
extern "C" String_t* ChatClient_get_AppVersion_m7_25 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_AppVersion(System.String)
extern "C" void ChatClient_set_AppVersion_m7_26 (ChatClient_t7_2 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.Chat.ChatClient::get_AppId()
extern "C" String_t* ChatClient_get_AppId_m7_27 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_AppId(System.String)
extern "C" void ChatClient_set_AppId_m7_28 (ChatClient_t7_2 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.Chat.AuthenticationValues ExitGames.Client.Photon.Chat.ChatClient::get_AuthValues()
extern "C" AuthenticationValues_t7_4 * ChatClient_get_AuthValues_m7_29 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_AuthValues(ExitGames.Client.Photon.Chat.AuthenticationValues)
extern "C" void ChatClient_set_AuthValues_m7_30 (ChatClient_t7_2 * __this, AuthenticationValues_t7_4 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.Chat.ChatClient::get_UserId()
extern "C" String_t* ChatClient_get_UserId_m7_31 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_UserId(System.String)
extern "C" void ChatClient_set_UserId_m7_32 (ChatClient_t7_2 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::Connect(System.String,System.String,ExitGames.Client.Photon.Chat.AuthenticationValues)
extern "C" bool ChatClient_Connect_m7_33 (ChatClient_t7_2 * __this, String_t* ___appId, String_t* ___appVersion, AuthenticationValues_t7_4 * ___authValues, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::Service()
extern "C" void ChatClient_Service_m7_34 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::Disconnect()
extern "C" void ChatClient_Disconnect_m7_35 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::StopThread()
extern "C" void ChatClient_StopThread_m7_36 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::Subscribe(System.String[])
extern "C" bool ChatClient_Subscribe_m7_37 (ChatClient_t7_2 * __this, StringU5BU5D_t1_202* ___channels, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::Subscribe(System.String[],System.Int32)
extern "C" bool ChatClient_Subscribe_m7_38 (ChatClient_t7_2 * __this, StringU5BU5D_t1_202* ___channels, int32_t ___messagesFromHistory, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::Unsubscribe(System.String[])
extern "C" bool ChatClient_Unsubscribe_m7_39 (ChatClient_t7_2 * __this, StringU5BU5D_t1_202* ___channels, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::PublishMessage(System.String,System.Object)
extern "C" bool ChatClient_PublishMessage_m7_40 (ChatClient_t7_2 * __this, String_t* ___channelName, Object_t * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::SendPrivateMessage(System.String,System.Object)
extern "C" bool ChatClient_SendPrivateMessage_m7_41 (ChatClient_t7_2 * __this, String_t* ___target, Object_t * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::SendPrivateMessage(System.String,System.Object,System.Boolean)
extern "C" bool ChatClient_SendPrivateMessage_m7_42 (ChatClient_t7_2 * __this, String_t* ___target, Object_t * ___message, bool ___encrypt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::SetOnlineStatus(System.Int32,System.Object,System.Boolean)
extern "C" bool ChatClient_SetOnlineStatus_m7_43 (ChatClient_t7_2 * __this, int32_t ___status, Object_t * ___message, bool ___skipMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::SetOnlineStatus(System.Int32)
extern "C" bool ChatClient_SetOnlineStatus_m7_44 (ChatClient_t7_2 * __this, int32_t ___status, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::SetOnlineStatus(System.Int32,System.Object)
extern "C" bool ChatClient_SetOnlineStatus_m7_45 (ChatClient_t7_2 * __this, int32_t ___status, Object_t * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::AddFriends(System.String[])
extern "C" bool ChatClient_AddFriends_m7_46 (ChatClient_t7_2 * __this, StringU5BU5D_t1_202* ___friends, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::RemoveFriends(System.String[])
extern "C" bool ChatClient_RemoveFriends_m7_47 (ChatClient_t7_2 * __this, StringU5BU5D_t1_202* ___friends, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.Chat.ChatClient::GetPrivateChannelNameByUser(System.String)
extern "C" String_t* ChatClient_GetPrivateChannelNameByUser_m7_48 (ChatClient_t7_2 * __this, String_t* ___userName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::TryGetChannel(System.String,System.Boolean,ExitGames.Client.Photon.Chat.ChatChannel&)
extern "C" bool ChatClient_TryGetChannel_m7_49 (ChatClient_t7_2 * __this, String_t* ___channelName, bool ___isPrivate, ChatChannel_t7_1 ** ___channel, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::TryGetChannel(System.String,ExitGames.Client.Photon.Chat.ChatChannel&)
extern "C" bool ChatClient_TryGetChannel_m7_50 (ChatClient_t7_2 * __this, String_t* ___channelName, ChatChannel_t7_1 ** ___channel, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::SendAcksOnly()
extern "C" void ChatClient_SendAcksOnly_m7_51 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_DebugOut(ExitGames.Client.Photon.DebugLevel)
extern "C" void ChatClient_set_DebugOut_m7_52 (ChatClient_t7_2 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.Chat.ChatClient::get_DebugOut()
extern "C" uint8_t ChatClient_get_DebugOut_m7_53 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::SendChannelOperation(System.String[],System.Byte,System.Int32)
extern "C" bool ChatClient_SendChannelOperation_m7_54 (ChatClient_t7_2 * __this, StringU5BU5D_t1_202* ___channels, uint8_t ___operation, int32_t ___historyLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::HandlePrivateMessageEvent(ExitGames.Client.Photon.EventData)
extern "C" void ChatClient_HandlePrivateMessageEvent_m7_55 (ChatClient_t7_2 * __this, EventData_t5_44 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::HandleChatMessagesEvent(ExitGames.Client.Photon.EventData)
extern "C" void ChatClient_HandleChatMessagesEvent_m7_56 (ChatClient_t7_2 * __this, EventData_t5_44 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::HandleSubscribeEvent(ExitGames.Client.Photon.EventData)
extern "C" void ChatClient_HandleSubscribeEvent_m7_57 (ChatClient_t7_2 * __this, EventData_t5_44 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::HandleUnsubscribeEvent(ExitGames.Client.Photon.EventData)
extern "C" void ChatClient_HandleUnsubscribeEvent_m7_58 (ChatClient_t7_2 * __this, EventData_t5_44 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::HandleAuthResponse(ExitGames.Client.Photon.OperationResponse)
extern "C" void ChatClient_HandleAuthResponse_m7_59 (ChatClient_t7_2 * __this, OperationResponse_t5_43 * ___operationResponse, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::HandleStatusUpdate(ExitGames.Client.Photon.EventData)
extern "C" void ChatClient_HandleStatusUpdate_m7_60 (ChatClient_t7_2 * __this, EventData_t5_44 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Chat.ChatClient::ConnectToFrontEnd()
extern "C" void ChatClient_ConnectToFrontEnd_m7_61 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::AuthenticateOnFrontEnd()
extern "C" bool ChatClient_AuthenticateOnFrontEnd_m7_62 (ChatClient_t7_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
