﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.Chat.ChatEventCode
struct ChatEventCode_t7_7;

#include "codegen/il2cpp-codegen.h"

// System.Void ExitGames.Client.Photon.Chat.ChatEventCode::.ctor()
extern "C" void ChatEventCode__ctor_m7_63 (ChatEventCode_t7_7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
