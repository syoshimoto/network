﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.Chat.ChatParameterCode
struct ChatParameterCode_t7_9;

#include "codegen/il2cpp-codegen.h"

// System.Void ExitGames.Client.Photon.Chat.ChatParameterCode::.ctor()
extern "C" void ChatParameterCode__ctor_m7_65 (ChatParameterCode_t7_9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
