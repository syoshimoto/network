﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct Dictionary_2_t1_933;

#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPeer.h"

// ExitGames.Client.Photon.Chat.ChatPeer
struct  ChatPeer_t7_3  : public PhotonPeer_t5_38
{
};
struct ChatPeer_t7_3_StaticFields{
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32> ExitGames.Client.Photon.Chat.ChatPeer::ProtocolToNameServerPort
	Dictionary_2_t1_933 * ___ProtocolToNameServerPort_7;
};
