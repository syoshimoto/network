﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.Chat.ErrorCode
struct ErrorCode_t7_12;

#include "codegen/il2cpp-codegen.h"

// System.Void ExitGames.Client.Photon.Chat.ErrorCode::.ctor()
extern "C" void ErrorCode__ctor_m7_90 (ErrorCode_t7_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
