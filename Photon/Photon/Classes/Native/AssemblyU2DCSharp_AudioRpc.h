﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip
struct AudioClip_t6_121;
// UnityEngine.AudioSource
struct AudioSource_t6_122;

#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"

// AudioRpc
struct  AudioRpc_t8_52  : public MonoBehaviour_t8_6
{
	// UnityEngine.AudioClip AudioRpc::marco
	AudioClip_t6_121 * ___marco_2;
	// UnityEngine.AudioClip AudioRpc::polo
	AudioClip_t6_121 * ___polo_3;
	// UnityEngine.AudioSource AudioRpc::m_Source
	AudioSource_t6_122 * ___m_Source_4;
};
