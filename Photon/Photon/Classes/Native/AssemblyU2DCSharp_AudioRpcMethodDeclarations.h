﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioRpc
struct AudioRpc_t8_52;

#include "codegen/il2cpp-codegen.h"

// System.Void AudioRpc::.ctor()
extern "C" void AudioRpc__ctor_m8_237 (AudioRpc_t8_52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRpc::Awake()
extern "C" void AudioRpc_Awake_m8_238 (AudioRpc_t8_52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRpc::Marco()
extern "C" void AudioRpc_Marco_m8_239 (AudioRpc_t8_52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRpc::Polo()
extern "C" void AudioRpc_Polo_m8_240 (AudioRpc_t8_52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRpc::OnApplicationFocus(System.Boolean)
extern "C" void AudioRpc_OnApplicationFocus_m8_241 (AudioRpc_t8_52 * __this, bool ___focus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
