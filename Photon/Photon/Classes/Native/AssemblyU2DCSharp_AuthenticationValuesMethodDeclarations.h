﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AuthenticationValues
struct AuthenticationValues_t8_97;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t1_71;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CustomAuthenticationType.h"

// System.Void AuthenticationValues::.ctor()
extern "C" void AuthenticationValues__ctor_m8_351 (AuthenticationValues_t8_97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthenticationValues::.ctor(System.String)
extern "C" void AuthenticationValues__ctor_m8_352 (AuthenticationValues_t8_97 * __this, String_t* ___userId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CustomAuthenticationType AuthenticationValues::get_AuthType()
extern "C" uint8_t AuthenticationValues_get_AuthType_m8_353 (AuthenticationValues_t8_97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthenticationValues::set_AuthType(CustomAuthenticationType)
extern "C" void AuthenticationValues_set_AuthType_m8_354 (AuthenticationValues_t8_97 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AuthenticationValues::get_AuthGetParameters()
extern "C" String_t* AuthenticationValues_get_AuthGetParameters_m8_355 (AuthenticationValues_t8_97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthenticationValues::set_AuthGetParameters(System.String)
extern "C" void AuthenticationValues_set_AuthGetParameters_m8_356 (AuthenticationValues_t8_97 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AuthenticationValues::get_AuthPostData()
extern "C" Object_t * AuthenticationValues_get_AuthPostData_m8_357 (AuthenticationValues_t8_97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthenticationValues::set_AuthPostData(System.Object)
extern "C" void AuthenticationValues_set_AuthPostData_m8_358 (AuthenticationValues_t8_97 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AuthenticationValues::get_Token()
extern "C" String_t* AuthenticationValues_get_Token_m8_359 (AuthenticationValues_t8_97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthenticationValues::set_Token(System.String)
extern "C" void AuthenticationValues_set_Token_m8_360 (AuthenticationValues_t8_97 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AuthenticationValues::get_UserId()
extern "C" String_t* AuthenticationValues_get_UserId_m8_361 (AuthenticationValues_t8_97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthenticationValues::set_UserId(System.String)
extern "C" void AuthenticationValues_set_UserId_m8_362 (AuthenticationValues_t8_97 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthenticationValues::SetAuthPostData(System.String)
extern "C" void AuthenticationValues_SetAuthPostData_m8_363 (AuthenticationValues_t8_97 * __this, String_t* ___stringData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthenticationValues::SetAuthPostData(System.Byte[])
extern "C" void AuthenticationValues_SetAuthPostData_m8_364 (AuthenticationValues_t8_97 * __this, ByteU5BU5D_t1_71* ___byteData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthenticationValues::AddAuthParameter(System.String,System.String)
extern "C" void AuthenticationValues_AddAuthParameter_m8_365 (AuthenticationValues_t8_97 * __this, String_t* ___key, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AuthenticationValues::ToString()
extern "C" String_t* AuthenticationValues_ToString_m8_366 (AuthenticationValues_t8_97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
