﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_CharacterState.h"

// CharacterState
struct  CharacterState_t8_48 
{
	// System.Int32 CharacterState::value__
	int32_t ___value___1;
};
