﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_202;
// ExitGames.Client.Photon.Chat.ChatChannel
struct ChatChannel_t7_1;
// ExitGames.Client.Photon.Chat.ChatClient
struct ChatClient_t7_2;
// ChatGui
struct ChatGui_t8_14;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// ChatGui
struct  ChatGui_t8_14  : public MonoBehaviour_t6_80
{
	// System.String ChatGui::ChatAppId
	String_t* ___ChatAppId_2;
	// System.String[] ChatGui::ChannelsToJoinOnConnect
	StringU5BU5D_t1_202* ___ChannelsToJoinOnConnect_3;
	// System.Int32 ChatGui::HistoryLengthToFetch
	int32_t ___HistoryLengthToFetch_4;
	// System.Boolean ChatGui::DemoPublishOnSubscribe
	bool ___DemoPublishOnSubscribe_5;
	// ExitGames.Client.Photon.Chat.ChatChannel ChatGui::selectedChannel
	ChatChannel_t7_1 * ___selectedChannel_6;
	// System.String ChatGui::selectedChannelName
	String_t* ___selectedChannelName_7;
	// System.Int32 ChatGui::selectedChannelIndex
	int32_t ___selectedChannelIndex_8;
	// System.Boolean ChatGui::doingPrivateChat
	bool ___doingPrivateChat_9;
	// ExitGames.Client.Photon.Chat.ChatClient ChatGui::chatClient
	ChatClient_t7_2 * ___chatClient_10;
	// UnityEngine.Rect ChatGui::GuiRect
	Rect_t6_52  ___GuiRect_11;
	// System.Boolean ChatGui::IsVisible
	bool ___IsVisible_12;
	// System.Boolean ChatGui::AlignBottom
	bool ___AlignBottom_13;
	// System.Boolean ChatGui::FullScreen
	bool ___FullScreen_14;
	// System.String ChatGui::inputLine
	String_t* ___inputLine_15;
	// System.String ChatGui::userIdInput
	String_t* ___userIdInput_16;
	// UnityEngine.Vector2 ChatGui::scrollPos
	Vector2_t6_48  ___scrollPos_17;
	// System.String ChatGui::<UserName>k__BackingField
	String_t* ___U3CUserNameU3Ek__BackingField_21;
};
struct ChatGui_t8_14_StaticFields{
	// System.String ChatGui::WelcomeText
	String_t* ___WelcomeText_18;
	// System.String ChatGui::HelpText
	String_t* ___HelpText_19;
	// ChatGui ChatGui::instance
	ChatGui_t8_14 * ___instance_20;
};
