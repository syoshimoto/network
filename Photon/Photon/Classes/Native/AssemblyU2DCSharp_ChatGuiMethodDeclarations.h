﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatGui
struct ChatGui_t8_14;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_202;
// System.Boolean[]
struct BooleanU5BU5D_t1_264;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DebugLevel.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_1.h"

// System.Void ChatGui::.ctor()
extern "C" void ChatGui__ctor_m8_33 (ChatGui_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::.cctor()
extern "C" void ChatGui__cctor_m8_34 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatGui::get_UserName()
extern "C" String_t* ChatGui_get_UserName_m8_35 (ChatGui_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::set_UserName(System.String)
extern "C" void ChatGui_set_UserName_m8_36 (ChatGui_t8_14 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChatGui ChatGui::get_Instance()
extern "C" ChatGui_t8_14 * ChatGui_get_Instance_m8_37 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::Awake()
extern "C" void ChatGui_Awake_m8_38 (ChatGui_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::Start()
extern "C" void ChatGui_Start_m8_39 (ChatGui_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::OnApplicationQuit()
extern "C" void ChatGui_OnApplicationQuit_m8_40 (ChatGui_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::OnDestroy()
extern "C" void ChatGui_OnDestroy_m8_41 (ChatGui_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::Update()
extern "C" void ChatGui_Update_m8_42 (ChatGui_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::OnGUI()
extern "C" void ChatGui_OnGUI_m8_43 (ChatGui_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::GuiSendsMsg()
extern "C" void ChatGui_GuiSendsMsg_m8_44 (ChatGui_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::PostHelpToCurrentChannel()
extern "C" void ChatGui_PostHelpToCurrentChannel_m8_45 (ChatGui_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::OnConnected()
extern "C" void ChatGui_OnConnected_m8_46 (ChatGui_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern "C" void ChatGui_DebugReturn_m8_47 (ChatGui_t8_14 * __this, uint8_t ___level, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::OnDisconnected()
extern "C" void ChatGui_OnDisconnected_m8_48 (ChatGui_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::OnChatStateChange(ExitGames.Client.Photon.Chat.ChatState)
extern "C" void ChatGui_OnChatStateChange_m8_49 (ChatGui_t8_14 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::OnSubscribed(System.String[],System.Boolean[])
extern "C" void ChatGui_OnSubscribed_m8_50 (ChatGui_t8_14 * __this, StringU5BU5D_t1_202* ___channels, BooleanU5BU5D_t1_264* ___results, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::OnUnsubscribed(System.String[])
extern "C" void ChatGui_OnUnsubscribed_m8_51 (ChatGui_t8_14 * __this, StringU5BU5D_t1_202* ___channels, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::OnGetMessages(System.String,System.String[],System.Object[])
extern "C" void ChatGui_OnGetMessages_m8_52 (ChatGui_t8_14 * __this, String_t* ___channelName, StringU5BU5D_t1_202* ___senders, ObjectU5BU5D_t1_157* ___messages, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::OnPrivateMessage(System.String,System.Object,System.String)
extern "C" void ChatGui_OnPrivateMessage_m8_53 (ChatGui_t8_14 * __this, String_t* ___sender, Object_t * ___message, String_t* ___channelName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatGui::OnStatusUpdate(System.String,System.Int32,System.Boolean,System.Object)
extern "C" void ChatGui_OnStatusUpdate_m8_54 (ChatGui_t8_14 * __this, String_t* ___user, int32_t ___status, bool ___gotMessage, Object_t * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
