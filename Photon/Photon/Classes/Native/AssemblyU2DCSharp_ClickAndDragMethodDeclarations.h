﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClickAndDrag
struct ClickAndDrag_t8_7;

#include "codegen/il2cpp-codegen.h"

// System.Void ClickAndDrag::.ctor()
extern "C" void ClickAndDrag__ctor_m8_16 (ClickAndDrag_t8_7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickAndDrag::Update()
extern "C" void ClickAndDrag_Update_m8_17 (ClickAndDrag_t8_7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
