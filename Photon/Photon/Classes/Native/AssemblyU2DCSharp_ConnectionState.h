﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ConnectionState.h"

// ConnectionState
struct  ConnectionState_t8_68 
{
	// System.Int32 ConnectionState::value__
	int32_t ___value___1;
};
