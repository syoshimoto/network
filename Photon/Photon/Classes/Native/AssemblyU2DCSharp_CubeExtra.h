﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// CubeExtra
struct  CubeExtra_t8_40  : public MonoBehaviour_t8_6
{
	// UnityEngine.Vector3 CubeExtra::latestCorrectPos
	Vector3_t6_49  ___latestCorrectPos_2;
	// UnityEngine.Vector3 CubeExtra::lastMovement
	Vector3_t6_49  ___lastMovement_3;
	// System.Single CubeExtra::lastTime
	float ___lastTime_4;
};
