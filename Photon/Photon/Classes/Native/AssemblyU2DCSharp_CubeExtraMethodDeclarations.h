﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CubeExtra
struct CubeExtra_t8_40;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"

// System.Void CubeExtra::.ctor()
extern "C" void CubeExtra__ctor_m8_162 (CubeExtra_t8_40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeExtra::Awake()
extern "C" void CubeExtra_Awake_m8_163 (CubeExtra_t8_40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeExtra::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void CubeExtra_OnPhotonSerializeView_m8_164 (CubeExtra_t8_40 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeExtra::Update()
extern "C" void CubeExtra_Update_m8_165 (CubeExtra_t8_40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
