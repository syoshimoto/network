﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CubeInter/State[]
struct StateU5BU5D_t8_43;

#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"

// CubeInter
struct  CubeInter_t8_42  : public MonoBehaviour_t8_6
{
	// System.Double CubeInter::interpolationBackTime
	double ___interpolationBackTime_2;
	// CubeInter/State[] CubeInter::m_BufferedState
	StateU5BU5D_t8_43* ___m_BufferedState_3;
	// System.Int32 CubeInter::m_TimestampCount
	int32_t ___m_TimestampCount_4;
};
