﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CubeInter
struct CubeInter_t8_42;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"

// System.Void CubeInter::.ctor()
extern "C" void CubeInter__ctor_m8_166 (CubeInter_t8_42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeInter::Awake()
extern "C" void CubeInter_Awake_m8_167 (CubeInter_t8_42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeInter::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void CubeInter_OnPhotonSerializeView_m8_168 (CubeInter_t8_42 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeInter::Update()
extern "C" void CubeInter_Update_m8_169 (CubeInter_t8_42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
