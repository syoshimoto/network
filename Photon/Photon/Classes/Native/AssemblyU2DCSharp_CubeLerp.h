﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// CubeLerp
struct  CubeLerp_t8_44  : public MonoBehaviour_t8_6
{
	// UnityEngine.Vector3 CubeLerp::latestCorrectPos
	Vector3_t6_49  ___latestCorrectPos_2;
	// UnityEngine.Vector3 CubeLerp::onUpdatePos
	Vector3_t6_49  ___onUpdatePos_3;
	// System.Single CubeLerp::fraction
	float ___fraction_4;
};
