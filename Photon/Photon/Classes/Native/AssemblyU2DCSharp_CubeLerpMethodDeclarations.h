﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CubeLerp
struct CubeLerp_t8_44;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"

// System.Void CubeLerp::.ctor()
extern "C" void CubeLerp__ctor_m8_170 (CubeLerp_t8_44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeLerp::Awake()
extern "C" void CubeLerp_Awake_m8_171 (CubeLerp_t8_44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeLerp::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void CubeLerp_OnPhotonSerializeView_m8_172 (CubeLerp_t8_44 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeLerp::Update()
extern "C" void CubeLerp_Update_m8_173 (CubeLerp_t8_44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
