﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoBoxesGui
struct DemoBoxesGui_t8_4;

#include "codegen/il2cpp-codegen.h"

// System.Void DemoBoxesGui::.ctor()
extern "C" void DemoBoxesGui__ctor_m8_12 (DemoBoxesGui_t8_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoBoxesGui::OnGUI()
extern "C" void DemoBoxesGui_OnGUI_m8_13 (DemoBoxesGui_t8_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
