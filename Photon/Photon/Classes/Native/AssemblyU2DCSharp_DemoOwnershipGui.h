﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUISkin
struct GUISkin_t6_161;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// DemoOwnershipGui
struct  DemoOwnershipGui_t8_8  : public MonoBehaviour_t6_80
{
	// UnityEngine.GUISkin DemoOwnershipGui::Skin
	GUISkin_t6_161 * ___Skin_2;
	// System.Boolean DemoOwnershipGui::TransferOwnershipOnRequest
	bool ___TransferOwnershipOnRequest_3;
};
