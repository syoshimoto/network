﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoOwnershipGui
struct DemoOwnershipGui_t8_8;
// System.Object[]
struct ObjectU5BU5D_t1_157;

#include "codegen/il2cpp-codegen.h"

// System.Void DemoOwnershipGui::.ctor()
extern "C" void DemoOwnershipGui__ctor_m8_18 (DemoOwnershipGui_t8_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoOwnershipGui::OnOwnershipRequest(System.Object[])
extern "C" void DemoOwnershipGui_OnOwnershipRequest_m8_19 (DemoOwnershipGui_t8_8 * __this, ObjectU5BU5D_t1_157* ___viewAndPlayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoOwnershipGui::OnGUI()
extern "C" void DemoOwnershipGui_OnGUI_m8_20 (DemoOwnershipGui_t8_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
