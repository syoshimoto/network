﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.ActorProperties
struct ActorProperties_t8_83;

#include "codegen/il2cpp-codegen.h"

// System.Void ExitGames.Client.Photon.ActorProperties::.ctor()
extern "C" void ActorProperties__ctor_m8_337 (ActorProperties_t8_83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
