﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.EventCode
struct EventCode_t8_85;

#include "codegen/il2cpp-codegen.h"

// System.Void ExitGames.Client.Photon.EventCode::.ctor()
extern "C" void EventCode__ctor_m8_339 (EventCode_t8_85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
