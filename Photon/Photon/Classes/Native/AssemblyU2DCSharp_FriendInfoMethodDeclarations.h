﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendInfo
struct FriendInfo_t8_74;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendInfo::.ctor()
extern "C" void FriendInfo__ctor_m8_304 (FriendInfo_t8_74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FriendInfo::get_Name()
extern "C" String_t* FriendInfo_get_Name_m8_305 (FriendInfo_t8_74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendInfo::set_Name(System.String)
extern "C" void FriendInfo_set_Name_m8_306 (FriendInfo_t8_74 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendInfo::get_IsOnline()
extern "C" bool FriendInfo_get_IsOnline_m8_307 (FriendInfo_t8_74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendInfo::set_IsOnline(System.Boolean)
extern "C" void FriendInfo_set_IsOnline_m8_308 (FriendInfo_t8_74 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FriendInfo::get_Room()
extern "C" String_t* FriendInfo_get_Room_m8_309 (FriendInfo_t8_74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendInfo::set_Room(System.String)
extern "C" void FriendInfo_set_Room_m8_310 (FriendInfo_t8_74 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendInfo::get_IsInRoom()
extern "C" bool FriendInfo_get_IsInRoom_m8_311 (FriendInfo_t8_74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FriendInfo::ToString()
extern "C" String_t* FriendInfo_ToString_m8_312 (FriendInfo_t8_74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
