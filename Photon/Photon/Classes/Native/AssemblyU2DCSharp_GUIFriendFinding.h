﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_202;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Rect.h"

// GUIFriendFinding
struct  GUIFriendFinding_t8_18  : public MonoBehaviour_t6_80
{
	// System.String[] GUIFriendFinding::friendListOfSomeCommunity
	StringU5BU5D_t1_202* ___friendListOfSomeCommunity_2;
	// UnityEngine.Rect GUIFriendFinding::GuiRect
	Rect_t6_52  ___GuiRect_3;
};
