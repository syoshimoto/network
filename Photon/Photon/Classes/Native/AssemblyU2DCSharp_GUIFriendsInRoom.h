﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Rect.h"

// GUIFriendsInRoom
struct  GUIFriendsInRoom_t8_19  : public MonoBehaviour_t6_80
{
	// UnityEngine.Rect GUIFriendsInRoom::GuiRect
	Rect_t6_52  ___GuiRect_2;
};
