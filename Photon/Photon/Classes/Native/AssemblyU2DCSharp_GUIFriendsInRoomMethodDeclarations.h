﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GUIFriendsInRoom
struct GUIFriendsInRoom_t8_19;

#include "codegen/il2cpp-codegen.h"

// System.Void GUIFriendsInRoom::.ctor()
extern "C" void GUIFriendsInRoom__ctor_m8_75 (GUIFriendsInRoom_t8_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIFriendsInRoom::Start()
extern "C" void GUIFriendsInRoom_Start_m8_76 (GUIFriendsInRoom_t8_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIFriendsInRoom::OnGUI()
extern "C" void GUIFriendsInRoom_OnGUI_m8_77 (GUIFriendsInRoom_t8_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
