﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhotonView
struct PhotonView_t8_3;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// GameLogic
struct  GameLogic_t8_54  : public MonoBehaviour_t6_80
{
};
struct GameLogic_t8_54_StaticFields{
	// System.Int32 GameLogic::playerWhoIsIt
	int32_t ___playerWhoIsIt_2;
	// PhotonView GameLogic::ScenePhotonView
	PhotonView_t8_3 * ___ScenePhotonView_3;
};
