﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t6_85;
// UnityEngine.Transform
struct Transform_t6_61;

#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"

// HighlightOwnedGameObj
struct  HighlightOwnedGameObj_t8_149  : public MonoBehaviour_t8_6
{
	// UnityEngine.GameObject HighlightOwnedGameObj::PointerPrefab
	GameObject_t6_85 * ___PointerPrefab_2;
	// System.Single HighlightOwnedGameObj::Offset
	float ___Offset_3;
	// UnityEngine.Transform HighlightOwnedGameObj::markerTransform
	Transform_t6_61 * ___markerTransform_4;
};
