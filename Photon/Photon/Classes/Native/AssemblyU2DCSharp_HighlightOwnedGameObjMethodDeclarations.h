﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HighlightOwnedGameObj
struct HighlightOwnedGameObj_t8_149;

#include "codegen/il2cpp-codegen.h"

// System.Void HighlightOwnedGameObj::.ctor()
extern "C" void HighlightOwnedGameObj__ctor_m8_928 (HighlightOwnedGameObj_t8_149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighlightOwnedGameObj::Update()
extern "C" void HighlightOwnedGameObj_Update_m8_929 (HighlightOwnedGameObj_t8_149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
