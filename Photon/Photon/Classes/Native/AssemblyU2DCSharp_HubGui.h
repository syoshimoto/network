﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUISkin
struct GUISkin_t6_161;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t6_166;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "AssemblyU2DCSharp_HubGui_DemoBtn.h"

// HubGui
struct  HubGui_t8_22  : public MonoBehaviour_t6_80
{
	// UnityEngine.GUISkin HubGui::Skin
	GUISkin_t6_161 * ___Skin_2;
	// UnityEngine.Vector2 HubGui::scrollPos
	Vector2_t6_48  ___scrollPos_3;
	// System.String HubGui::demoDescription
	String_t* ___demoDescription_4;
	// HubGui/DemoBtn HubGui::demoBtn
	DemoBtn_t8_21  ___demoBtn_5;
	// HubGui/DemoBtn HubGui::webLink
	DemoBtn_t8_21  ___webLink_6;
	// UnityEngine.GUIStyle HubGui::m_Headline
	GUIStyle_t6_166 * ___m_Headline_7;
};
