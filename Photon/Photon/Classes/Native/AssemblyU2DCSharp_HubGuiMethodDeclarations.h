﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HubGui
struct HubGui_t8_22;

#include "codegen/il2cpp-codegen.h"

// System.Void HubGui::.ctor()
extern "C" void HubGui__ctor_m8_80 (HubGui_t8_22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HubGui::Start()
extern "C" void HubGui_Start_m8_81 (HubGui_t8_22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HubGui::OnGUI()
extern "C" void HubGui_OnGUI_m8_82 (HubGui_t8_22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
