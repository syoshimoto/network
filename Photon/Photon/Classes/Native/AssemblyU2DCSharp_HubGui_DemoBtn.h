﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"

// HubGui/DemoBtn
struct  DemoBtn_t8_21 
{
	// System.String HubGui/DemoBtn::Text
	String_t* ___Text_0;
	// System.String HubGui/DemoBtn::Link
	String_t* ___Link_1;
};
// Native definition for marshalling of: HubGui/DemoBtn
struct DemoBtn_t8_21_marshaled
{
	char* ___Text_0;
	char* ___Link_1;
};
