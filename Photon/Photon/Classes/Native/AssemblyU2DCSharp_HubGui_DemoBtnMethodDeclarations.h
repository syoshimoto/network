﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void DemoBtn_t8_21_marshal(const DemoBtn_t8_21& unmarshaled, DemoBtn_t8_21_marshaled& marshaled);
extern "C" void DemoBtn_t8_21_marshal_back(const DemoBtn_t8_21_marshaled& marshaled, DemoBtn_t8_21& unmarshaled);
extern "C" void DemoBtn_t8_21_marshal_cleanup(DemoBtn_t8_21_marshaled& marshaled);
