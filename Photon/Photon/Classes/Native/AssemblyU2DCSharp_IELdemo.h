﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform[]
struct TransformU5BU5D_t6_270;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// IELdemo
struct  IELdemo_t8_45  : public MonoBehaviour_t6_80
{
	// UnityEngine.Transform[] IELdemo::cubes
	TransformU5BU5D_t6_270* ___cubes_2;
};
