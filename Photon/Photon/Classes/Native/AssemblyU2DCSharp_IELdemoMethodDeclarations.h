﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IELdemo
struct IELdemo_t8_45;

#include "codegen/il2cpp-codegen.h"

// System.Void IELdemo::.ctor()
extern "C" void IELdemo__ctor_m8_174 (IELdemo_t8_45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IELdemo::Awake()
extern "C" void IELdemo_Awake_m8_175 (IELdemo_t8_45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IELdemo::OnConnectedToMaster()
extern "C" void IELdemo_OnConnectedToMaster_m8_176 (IELdemo_t8_45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IELdemo::OnPhotonRandomJoinFailed()
extern "C" void IELdemo_OnPhotonRandomJoinFailed_m8_177 (IELdemo_t8_45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IELdemo::OnJoinedRoom()
extern "C" void IELdemo_OnJoinedRoom_m8_178 (IELdemo_t8_45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IELdemo::OnCreatedRoom()
extern "C" void IELdemo_OnCreatedRoom_m8_179 (IELdemo_t8_45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IELdemo::Update()
extern "C" void IELdemo_Update_m8_180 (IELdemo_t8_45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IELdemo::OnGUI()
extern "C" void IELdemo_OnGUI_m8_181 (IELdemo_t8_45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
