﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t6_138;
// PhotonView
struct PhotonView_t8_3;
// PhotonTransformView
struct PhotonTransformView_t8_39;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// IdleRunJump
struct  IdleRunJump_t8_58  : public MonoBehaviour_t6_80
{
	// UnityEngine.Animator IdleRunJump::animator
	Animator_t6_138 * ___animator_2;
	// System.Single IdleRunJump::DirectionDampTime
	float ___DirectionDampTime_3;
	// System.Boolean IdleRunJump::ApplyGravity
	bool ___ApplyGravity_4;
	// System.Single IdleRunJump::SynchronizedMaxSpeed
	float ___SynchronizedMaxSpeed_5;
	// System.Single IdleRunJump::TurnSpeedModifier
	float ___TurnSpeedModifier_6;
	// System.Single IdleRunJump::SynchronizedTurnSpeed
	float ___SynchronizedTurnSpeed_7;
	// System.Single IdleRunJump::SynchronizedSpeedAcceleration
	float ___SynchronizedSpeedAcceleration_8;
	// PhotonView IdleRunJump::m_PhotonView
	PhotonView_t8_3 * ___m_PhotonView_9;
	// PhotonTransformView IdleRunJump::m_TransformView
	PhotonTransformView_t8_39 * ___m_TransformView_10;
	// System.Single IdleRunJump::m_SpeedModifier
	float ___m_SpeedModifier_11;
};
