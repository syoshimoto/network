﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IdleRunJump
struct IdleRunJump_t8_58;

#include "codegen/il2cpp-codegen.h"

// System.Void IdleRunJump::.ctor()
extern "C" void IdleRunJump__ctor_m8_265 (IdleRunJump_t8_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IdleRunJump::Start()
extern "C" void IdleRunJump_Start_m8_266 (IdleRunJump_t8_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IdleRunJump::Update()
extern "C" void IdleRunJump_Update_m8_267 (IdleRunJump_t8_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
