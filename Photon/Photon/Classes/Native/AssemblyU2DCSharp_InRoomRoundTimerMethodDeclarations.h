﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InRoomRoundTimer
struct InRoomRoundTimer_t8_151;
// ExitGames.Client.Photon.Hashtable
struct Hashtable_t5_1;
// PhotonPlayer
struct PhotonPlayer_t8_102;

#include "codegen/il2cpp-codegen.h"

// System.Void InRoomRoundTimer::.ctor()
extern "C" void InRoomRoundTimer__ctor_m8_936 (InRoomRoundTimer_t8_151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InRoomRoundTimer::StartRoundNow()
extern "C" void InRoomRoundTimer_StartRoundNow_m8_937 (InRoomRoundTimer_t8_151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InRoomRoundTimer::OnJoinedRoom()
extern "C" void InRoomRoundTimer_OnJoinedRoom_m8_938 (InRoomRoundTimer_t8_151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InRoomRoundTimer::OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable)
extern "C" void InRoomRoundTimer_OnPhotonCustomRoomPropertiesChanged_m8_939 (InRoomRoundTimer_t8_151 * __this, Hashtable_t5_1 * ___propertiesThatChanged, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InRoomRoundTimer::OnMasterClientSwitched(PhotonPlayer)
extern "C" void InRoomRoundTimer_OnMasterClientSwitched_m8_940 (InRoomRoundTimer_t8_151 * __this, PhotonPlayer_t8_102 * ___newMasterClient, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InRoomRoundTimer::Update()
extern "C" void InRoomRoundTimer_Update_m8_941 (InRoomRoundTimer_t8_151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InRoomRoundTimer::OnGUI()
extern "C" void InRoomRoundTimer_OnGUI_m8_942 (InRoomRoundTimer_t8_151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
