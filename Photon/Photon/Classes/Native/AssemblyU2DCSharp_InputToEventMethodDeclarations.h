﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InputToEvent
struct InputToEvent_t8_152;
// UnityEngine.GameObject
struct GameObject_t6_85;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void InputToEvent::.ctor()
extern "C" void InputToEvent__ctor_m8_943 (InputToEvent_t8_152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject InputToEvent::get_goPointedAt()
extern "C" GameObject_t6_85 * InputToEvent_get_goPointedAt_m8_944 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputToEvent::set_goPointedAt(UnityEngine.GameObject)
extern "C" void InputToEvent_set_goPointedAt_m8_945 (Object_t * __this /* static, unused */, GameObject_t6_85 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 InputToEvent::get_DragVector()
extern "C" Vector2_t6_48  InputToEvent_get_DragVector_m8_946 (InputToEvent_t8_152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputToEvent::Start()
extern "C" void InputToEvent_Start_m8_947 (InputToEvent_t8_152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputToEvent::Update()
extern "C" void InputToEvent_Update_m8_948 (InputToEvent_t8_152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputToEvent::Press(UnityEngine.Vector2)
extern "C" void InputToEvent_Press_m8_949 (InputToEvent_t8_152 * __this, Vector2_t6_48  ___screenPos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputToEvent::Release(UnityEngine.Vector2)
extern "C" void InputToEvent_Release_m8_950 (InputToEvent_t8_152 * __this, Vector2_t6_48  ___screenPos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject InputToEvent::RaycastObject(UnityEngine.Vector2)
extern "C" GameObject_t6_85 * InputToEvent_RaycastObject_m8_951 (InputToEvent_t8_152 * __this, Vector2_t6_48  ___screenPos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
