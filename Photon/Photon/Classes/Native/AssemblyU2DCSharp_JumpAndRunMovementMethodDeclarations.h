﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JumpAndRunMovement
struct JumpAndRunMovement_t8_2;

#include "codegen/il2cpp-codegen.h"

// System.Void JumpAndRunMovement::.ctor()
extern "C" void JumpAndRunMovement__ctor_m8_2 (JumpAndRunMovement_t8_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpAndRunMovement::Awake()
extern "C" void JumpAndRunMovement_Awake_m8_3 (JumpAndRunMovement_t8_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpAndRunMovement::Update()
extern "C" void JumpAndRunMovement_Update_m8_4 (JumpAndRunMovement_t8_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpAndRunMovement::FixedUpdate()
extern "C" void JumpAndRunMovement_FixedUpdate_m8_5 (JumpAndRunMovement_t8_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpAndRunMovement::UpdateFacingDirection()
extern "C" void JumpAndRunMovement_UpdateFacingDirection_m8_6 (JumpAndRunMovement_t8_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpAndRunMovement::UpdateJumping()
extern "C" void JumpAndRunMovement_UpdateJumping_m8_7 (JumpAndRunMovement_t8_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpAndRunMovement::DoJump()
extern "C" void JumpAndRunMovement_DoJump_m8_8 (JumpAndRunMovement_t8_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpAndRunMovement::UpdateMovement()
extern "C" void JumpAndRunMovement_UpdateMovement_m8_9 (JumpAndRunMovement_t8_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpAndRunMovement::UpdateIsRunning()
extern "C" void JumpAndRunMovement_UpdateIsRunning_m8_10 (JumpAndRunMovement_t8_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JumpAndRunMovement::UpdateIsGrounded()
extern "C" void JumpAndRunMovement_UpdateIsGrounded_m8_11 (JumpAndRunMovement_t8_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
