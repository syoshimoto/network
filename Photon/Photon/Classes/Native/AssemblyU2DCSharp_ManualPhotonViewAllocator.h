﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t6_85;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// ManualPhotonViewAllocator
struct  ManualPhotonViewAllocator_t8_153  : public MonoBehaviour_t6_80
{
	// UnityEngine.GameObject ManualPhotonViewAllocator::Prefab
	GameObject_t6_85 * ___Prefab_2;
};
