﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Renderer
struct Renderer_t6_25;

#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"

// MaterialPerOwner
struct  MaterialPerOwner_t8_10  : public MonoBehaviour_t8_6
{
	// System.Int32 MaterialPerOwner::assignedColorForUserId
	int32_t ___assignedColorForUserId_2;
	// UnityEngine.Renderer MaterialPerOwner::m_Renderer
	Renderer_t6_25 * ___m_Renderer_3;
};
