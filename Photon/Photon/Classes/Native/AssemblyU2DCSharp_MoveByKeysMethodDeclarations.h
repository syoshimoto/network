﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveByKeys
struct MoveByKeys_t8_154;

#include "codegen/il2cpp-codegen.h"

// System.Void MoveByKeys::.ctor()
extern "C" void MoveByKeys__ctor_m8_955 (MoveByKeys_t8_154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveByKeys::Start()
extern "C" void MoveByKeys_Start_m8_956 (MoveByKeys_t8_154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveByKeys::FixedUpdate()
extern "C" void MoveByKeys_FixedUpdate_m8_957 (MoveByKeys_t8_154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
