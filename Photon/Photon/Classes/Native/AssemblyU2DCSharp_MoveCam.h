﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t6_61;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// MoveCam
struct  MoveCam_t8_23  : public MonoBehaviour_t6_80
{
	// UnityEngine.Vector3 MoveCam::originalPos
	Vector3_t6_49  ___originalPos_2;
	// UnityEngine.Vector3 MoveCam::randomPos
	Vector3_t6_49  ___randomPos_3;
	// UnityEngine.Transform MoveCam::camTransform
	Transform_t6_61 * ___camTransform_4;
	// UnityEngine.Transform MoveCam::lookAt
	Transform_t6_61 * ___lookAt_5;
};
