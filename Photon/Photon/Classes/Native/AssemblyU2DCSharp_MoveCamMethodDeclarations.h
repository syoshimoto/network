﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveCam
struct MoveCam_t8_23;

#include "codegen/il2cpp-codegen.h"

// System.Void MoveCam::.ctor()
extern "C" void MoveCam__ctor_m8_83 (MoveCam_t8_23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCam::Start()
extern "C" void MoveCam_Start_m8_84 (MoveCam_t8_23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveCam::Update()
extern "C" void MoveCam_Update_m8_85 (MoveCam_t8_23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
