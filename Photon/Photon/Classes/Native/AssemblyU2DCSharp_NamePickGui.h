﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ChatGui
struct ChatGui_t8_14;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Rect.h"

// NamePickGui
struct  NamePickGui_t8_15  : public MonoBehaviour_t6_80
{
	// UnityEngine.Vector2 NamePickGui::GuiSize
	Vector2_t6_48  ___GuiSize_3;
	// System.String NamePickGui::InputLine
	String_t* ___InputLine_4;
	// UnityEngine.Rect NamePickGui::guiCenteredRect
	Rect_t6_52  ___guiCenteredRect_5;
	// ChatGui NamePickGui::chatComponent
	ChatGui_t8_14 * ___chatComponent_6;
	// System.String NamePickGui::helpText
	String_t* ___helpText_7;
};
