﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NamePickGui
struct NamePickGui_t8_15;

#include "codegen/il2cpp-codegen.h"

// System.Void NamePickGui::.ctor()
extern "C" void NamePickGui__ctor_m8_55 (NamePickGui_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NamePickGui::Awake()
extern "C" void NamePickGui_Awake_m8_56 (NamePickGui_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NamePickGui::OnGUI()
extern "C" void NamePickGui_OnGUI_m8_57 (NamePickGui_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NamePickGui::StartChat()
extern "C" void NamePickGui_StartChat_m8_58 (NamePickGui_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
