﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// NetworkCharacter
struct  NetworkCharacter_t8_56  : public MonoBehaviour_t8_6
{
	// UnityEngine.Vector3 NetworkCharacter::correctPlayerPos
	Vector3_t6_49  ___correctPlayerPos_2;
	// UnityEngine.Quaternion NetworkCharacter::correctPlayerRot
	Quaternion_t6_51  ___correctPlayerRot_3;
};
