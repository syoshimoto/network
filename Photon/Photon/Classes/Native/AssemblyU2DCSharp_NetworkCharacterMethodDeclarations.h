﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkCharacter
struct NetworkCharacter_t8_56;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"

// System.Void NetworkCharacter::.ctor()
extern "C" void NetworkCharacter__ctor_m8_255 (NetworkCharacter_t8_56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkCharacter::Update()
extern "C" void NetworkCharacter_Update_m8_256 (NetworkCharacter_t8_56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkCharacter::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void NetworkCharacter_OnPhotonSerializeView_m8_257 (NetworkCharacter_t8_56 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
