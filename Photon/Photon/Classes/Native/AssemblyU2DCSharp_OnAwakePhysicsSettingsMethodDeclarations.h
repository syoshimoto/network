﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnAwakePhysicsSettings
struct OnAwakePhysicsSettings_t8_5;

#include "codegen/il2cpp-codegen.h"

// System.Void OnAwakePhysicsSettings::.ctor()
extern "C" void OnAwakePhysicsSettings__ctor_m8_14 (OnAwakePhysicsSettings_t8_5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnAwakePhysicsSettings::Awake()
extern "C" void OnAwakePhysicsSettings_Awake_m8_15 (OnAwakePhysicsSettings_t8_5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
