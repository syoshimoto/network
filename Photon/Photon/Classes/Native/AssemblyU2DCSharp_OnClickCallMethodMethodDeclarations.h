﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnClickCallMethod
struct OnClickCallMethod_t8_20;

#include "codegen/il2cpp-codegen.h"

// System.Void OnClickCallMethod::.ctor()
extern "C" void OnClickCallMethod__ctor_m8_78 (OnClickCallMethod_t8_20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnClickCallMethod::OnClick()
extern "C" void OnClickCallMethod_OnClick_m8_79 (OnClickCallMethod_t8_20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
