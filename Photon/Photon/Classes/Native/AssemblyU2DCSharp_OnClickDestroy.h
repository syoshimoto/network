﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"

// OnClickDestroy
struct  OnClickDestroy_t8_157  : public MonoBehaviour_t8_6
{
	// System.Boolean OnClickDestroy::DestroyByRpc
	bool ___DestroyByRpc_2;
};
