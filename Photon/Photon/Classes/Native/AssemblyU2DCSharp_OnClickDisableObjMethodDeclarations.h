﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnClickDisableObj
struct OnClickDisableObj_t8_11;

#include "codegen/il2cpp-codegen.h"

// System.Void OnClickDisableObj::.ctor()
extern "C" void OnClickDisableObj__ctor_m8_26 (OnClickDisableObj_t8_11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnClickDisableObj::OnClick()
extern "C" void OnClickDisableObj_OnClick_m8_27 (OnClickDisableObj_t8_11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
