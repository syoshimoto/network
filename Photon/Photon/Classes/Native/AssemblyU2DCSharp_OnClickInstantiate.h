﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t6_85;
// System.String[]
struct StringU5BU5D_t1_202;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// OnClickInstantiate
struct  OnClickInstantiate_t8_158  : public MonoBehaviour_t6_80
{
	// UnityEngine.GameObject OnClickInstantiate::Prefab
	GameObject_t6_85 * ___Prefab_2;
	// System.Int32 OnClickInstantiate::InstantiateType
	int32_t ___InstantiateType_3;
	// System.String[] OnClickInstantiate::InstantiateTypeNames
	StringU5BU5D_t1_202* ___InstantiateTypeNames_4;
	// System.Boolean OnClickInstantiate::showGui
	bool ___showGui_5;
};
