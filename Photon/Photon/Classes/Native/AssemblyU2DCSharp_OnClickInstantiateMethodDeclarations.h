﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnClickInstantiate
struct OnClickInstantiate_t8_158;

#include "codegen/il2cpp-codegen.h"

// System.Void OnClickInstantiate::.ctor()
extern "C" void OnClickInstantiate__ctor_m8_972 (OnClickInstantiate_t8_158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnClickInstantiate::OnClick()
extern "C" void OnClickInstantiate_OnClick_m8_973 (OnClickInstantiate_t8_158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnClickInstantiate::OnGUI()
extern "C" void OnClickInstantiate_OnGUI_m8_974 (OnClickInstantiate_t8_158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
