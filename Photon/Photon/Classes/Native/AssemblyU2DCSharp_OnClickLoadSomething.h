﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "AssemblyU2DCSharp_OnClickLoadSomething_ResourceTypeOption.h"

// OnClickLoadSomething
struct  OnClickLoadSomething_t8_160  : public MonoBehaviour_t6_80
{
	// OnClickLoadSomething/ResourceTypeOption OnClickLoadSomething::ResourceTypeToLoad
	uint8_t ___ResourceTypeToLoad_2;
	// System.String OnClickLoadSomething::ResourceToLoad
	String_t* ___ResourceToLoad_3;
};
