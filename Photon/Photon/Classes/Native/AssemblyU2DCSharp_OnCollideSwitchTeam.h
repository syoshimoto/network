﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "AssemblyU2DCSharp_PunTeams_Team.h"

// OnCollideSwitchTeam
struct  OnCollideSwitchTeam_t8_29  : public MonoBehaviour_t6_80
{
	// PunTeams/Team OnCollideSwitchTeam::TeamToSwitchTo
	uint8_t ___TeamToSwitchTo_2;
};
