﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnCollideSwitchTeam
struct OnCollideSwitchTeam_t8_29;
// UnityEngine.Collider
struct Collider_t6_100;

#include "codegen/il2cpp-codegen.h"

// System.Void OnCollideSwitchTeam::.ctor()
extern "C" void OnCollideSwitchTeam__ctor_m8_103 (OnCollideSwitchTeam_t8_29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnCollideSwitchTeam::OnTriggerEnter(UnityEngine.Collider)
extern "C" void OnCollideSwitchTeam_OnTriggerEnter_m8_104 (OnCollideSwitchTeam_t8_29 * __this, Collider_t6_100 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
