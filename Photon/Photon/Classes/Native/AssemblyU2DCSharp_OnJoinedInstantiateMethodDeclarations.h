﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnJoinedInstantiate
struct OnJoinedInstantiate_t8_161;

#include "codegen/il2cpp-codegen.h"

// System.Void OnJoinedInstantiate::.ctor()
extern "C" void OnJoinedInstantiate__ctor_m8_977 (OnJoinedInstantiate_t8_161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnJoinedInstantiate::OnJoinedRoom()
extern "C" void OnJoinedInstantiate_OnJoinedRoom_m8_978 (OnJoinedInstantiate_t8_161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
