﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnPickedUpScript
struct OnPickedUpScript_t8_30;
// PickupItem
struct PickupItem_t8_163;

#include "codegen/il2cpp-codegen.h"

// System.Void OnPickedUpScript::.ctor()
extern "C" void OnPickedUpScript__ctor_m8_105 (OnPickedUpScript_t8_30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnPickedUpScript::OnPickedUp(PickupItem)
extern "C" void OnPickedUpScript_OnPickedUp_m8_106 (OnPickedUpScript_t8_30 * __this, PickupItem_t8_163 * ___item, const MethodInfo* method) IL2CPP_METHOD_ATTR;
