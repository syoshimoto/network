﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonAnimatorView
struct PhotonAnimatorView_t8_27;
// System.String
struct String_t;
// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer>
struct List_1_t1_950;
// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>
struct List_1_t1_949;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizeType.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_ParameterType.h"

// System.Void PhotonAnimatorView::.ctor()
extern "C" void PhotonAnimatorView__ctor_m8_869 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonAnimatorView::Awake()
extern "C" void PhotonAnimatorView_Awake_m8_870 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonAnimatorView::Update()
extern "C" void PhotonAnimatorView_Update_m8_871 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonAnimatorView::DoesLayerSynchronizeTypeExist(System.Int32)
extern "C" bool PhotonAnimatorView_DoesLayerSynchronizeTypeExist_m8_872 (PhotonAnimatorView_t8_27 * __this, int32_t ___layerIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonAnimatorView::DoesParameterSynchronizeTypeExist(System.String)
extern "C" bool PhotonAnimatorView_DoesParameterSynchronizeTypeExist_m8_873 (PhotonAnimatorView_t8_27 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedLayer> PhotonAnimatorView::GetSynchronizedLayers()
extern "C" List_1_t1_950 * PhotonAnimatorView_GetSynchronizedLayers_m8_874 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter> PhotonAnimatorView::GetSynchronizedParameters()
extern "C" List_1_t1_949 * PhotonAnimatorView_GetSynchronizedParameters_m8_875 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonAnimatorView/SynchronizeType PhotonAnimatorView::GetLayerSynchronizeType(System.Int32)
extern "C" int32_t PhotonAnimatorView_GetLayerSynchronizeType_m8_876 (PhotonAnimatorView_t8_27 * __this, int32_t ___layerIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonAnimatorView/SynchronizeType PhotonAnimatorView::GetParameterSynchronizeType(System.String)
extern "C" int32_t PhotonAnimatorView_GetParameterSynchronizeType_m8_877 (PhotonAnimatorView_t8_27 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonAnimatorView::SetLayerSynchronized(System.Int32,PhotonAnimatorView/SynchronizeType)
extern "C" void PhotonAnimatorView_SetLayerSynchronized_m8_878 (PhotonAnimatorView_t8_27 * __this, int32_t ___layerIndex, int32_t ___synchronizeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonAnimatorView::SetParameterSynchronized(System.String,PhotonAnimatorView/ParameterType,PhotonAnimatorView/SynchronizeType)
extern "C" void PhotonAnimatorView_SetParameterSynchronized_m8_879 (PhotonAnimatorView_t8_27 * __this, String_t* ___name, int32_t ___type, int32_t ___synchronizeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonAnimatorView::SerializeDataContinuously()
extern "C" void PhotonAnimatorView_SerializeDataContinuously_m8_880 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonAnimatorView::DeserializeDataContinuously()
extern "C" void PhotonAnimatorView_DeserializeDataContinuously_m8_881 (PhotonAnimatorView_t8_27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonAnimatorView::SerializeDataDiscretly(PhotonStream)
extern "C" void PhotonAnimatorView_SerializeDataDiscretly_m8_882 (PhotonAnimatorView_t8_27 * __this, PhotonStream_t8_106 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonAnimatorView::DeserializeDataDiscretly(PhotonStream)
extern "C" void PhotonAnimatorView_DeserializeDataDiscretly_m8_883 (PhotonAnimatorView_t8_27 * __this, PhotonStream_t8_106 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonAnimatorView::SerializeSynchronizationTypeState(PhotonStream)
extern "C" void PhotonAnimatorView_SerializeSynchronizationTypeState_m8_884 (PhotonAnimatorView_t8_27 * __this, PhotonStream_t8_106 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonAnimatorView::DeserializeSynchronizationTypeState(PhotonStream)
extern "C" void PhotonAnimatorView_DeserializeSynchronizationTypeState_m8_885 (PhotonAnimatorView_t8_27 * __this, PhotonStream_t8_106 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonAnimatorView::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void PhotonAnimatorView_OnPhotonSerializeView_m8_886 (PhotonAnimatorView_t8_27 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
