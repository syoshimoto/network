﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_ParameterType.h"

// PhotonAnimatorView/ParameterType
struct  ParameterType_t8_126 
{
	// System.Int32 PhotonAnimatorView/ParameterType::value__
	int32_t ___value___1;
};
