﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonAnimatorView/SynchronizedLayer
struct SynchronizedLayer_t8_129;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonAnimatorView/SynchronizedLayer::.ctor()
extern "C" void SynchronizedLayer__ctor_m8_856 (SynchronizedLayer_t8_129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
