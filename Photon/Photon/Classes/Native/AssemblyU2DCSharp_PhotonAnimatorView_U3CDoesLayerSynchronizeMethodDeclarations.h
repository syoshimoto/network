﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonAnimatorView/<DoesLayerSynchronizeTypeExist>c__AnonStorey3
struct U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130;
// PhotonAnimatorView/SynchronizedLayer
struct SynchronizedLayer_t8_129;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonAnimatorView/<DoesLayerSynchronizeTypeExist>c__AnonStorey3::.ctor()
extern "C" void U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3__ctor_m8_857 (U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonAnimatorView/<DoesLayerSynchronizeTypeExist>c__AnonStorey3::<>m__0(PhotonAnimatorView/SynchronizedLayer)
extern "C" bool U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_U3CU3Em__0_m8_858 (U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_t8_130 * __this, SynchronizedLayer_t8_129 * ___item, const MethodInfo* method) IL2CPP_METHOD_ATTR;
