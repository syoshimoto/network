﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonAnimatorView/<DoesParameterSynchronizeTypeExist>c__AnonStorey4
struct U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131;
// PhotonAnimatorView/SynchronizedParameter
struct SynchronizedParameter_t8_128;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonAnimatorView/<DoesParameterSynchronizeTypeExist>c__AnonStorey4::.ctor()
extern "C" void U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4__ctor_m8_859 (U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonAnimatorView/<DoesParameterSynchronizeTypeExist>c__AnonStorey4::<>m__1(PhotonAnimatorView/SynchronizedParameter)
extern "C" bool U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_U3CU3Em__1_m8_860 (U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_t8_131 * __this, SynchronizedParameter_t8_128 * ___item, const MethodInfo* method) IL2CPP_METHOD_ATTR;
