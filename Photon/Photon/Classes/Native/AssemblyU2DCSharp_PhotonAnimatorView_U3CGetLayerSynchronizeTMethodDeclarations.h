﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonAnimatorView/<GetLayerSynchronizeType>c__AnonStorey5
struct U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132;
// PhotonAnimatorView/SynchronizedLayer
struct SynchronizedLayer_t8_129;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonAnimatorView/<GetLayerSynchronizeType>c__AnonStorey5::.ctor()
extern "C" void U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5__ctor_m8_861 (U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonAnimatorView/<GetLayerSynchronizeType>c__AnonStorey5::<>m__2(PhotonAnimatorView/SynchronizedLayer)
extern "C" bool U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_U3CU3Em__2_m8_862 (U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_t8_132 * __this, SynchronizedLayer_t8_129 * ___item, const MethodInfo* method) IL2CPP_METHOD_ATTR;
