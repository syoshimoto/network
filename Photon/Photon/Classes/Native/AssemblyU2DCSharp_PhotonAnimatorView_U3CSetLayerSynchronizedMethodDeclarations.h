﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonAnimatorView/<SetLayerSynchronized>c__AnonStorey7
struct U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134;
// PhotonAnimatorView/SynchronizedLayer
struct SynchronizedLayer_t8_129;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonAnimatorView/<SetLayerSynchronized>c__AnonStorey7::.ctor()
extern "C" void U3CSetLayerSynchronizedU3Ec__AnonStorey7__ctor_m8_865 (U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonAnimatorView/<SetLayerSynchronized>c__AnonStorey7::<>m__4(PhotonAnimatorView/SynchronizedLayer)
extern "C" bool U3CSetLayerSynchronizedU3Ec__AnonStorey7_U3CU3Em__4_m8_866 (U3CSetLayerSynchronizedU3Ec__AnonStorey7_t8_134 * __this, SynchronizedLayer_t8_129 * ___item, const MethodInfo* method) IL2CPP_METHOD_ATTR;
