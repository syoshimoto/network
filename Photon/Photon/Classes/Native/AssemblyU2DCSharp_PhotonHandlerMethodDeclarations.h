﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonHandler
struct PhotonHandler_t8_111;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CloudRegionCode.h"

// System.Void PhotonHandler::.ctor()
extern "C" void PhotonHandler__ctor_m8_571 (PhotonHandler_t8_111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::.cctor()
extern "C" void PhotonHandler__cctor_m8_572 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::Awake()
extern "C" void PhotonHandler_Awake_m8_573 (PhotonHandler_t8_111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::OnApplicationQuit()
extern "C" void PhotonHandler_OnApplicationQuit_m8_574 (PhotonHandler_t8_111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::OnApplicationPause(System.Boolean)
extern "C" void PhotonHandler_OnApplicationPause_m8_575 (PhotonHandler_t8_111 * __this, bool ___pause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::OnDestroy()
extern "C" void PhotonHandler_OnDestroy_m8_576 (PhotonHandler_t8_111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::Update()
extern "C" void PhotonHandler_Update_m8_577 (PhotonHandler_t8_111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::OnLevelWasLoaded(System.Int32)
extern "C" void PhotonHandler_OnLevelWasLoaded_m8_578 (PhotonHandler_t8_111 * __this, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::OnJoinedRoom()
extern "C" void PhotonHandler_OnJoinedRoom_m8_579 (PhotonHandler_t8_111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::OnCreatedRoom()
extern "C" void PhotonHandler_OnCreatedRoom_m8_580 (PhotonHandler_t8_111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::StartFallbackSendAckThread()
extern "C" void PhotonHandler_StartFallbackSendAckThread_m8_581 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::StopFallbackSendAckThread()
extern "C" void PhotonHandler_StopFallbackSendAckThread_m8_582 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonHandler::FallbackSendAckThread()
extern "C" bool PhotonHandler_FallbackSendAckThread_m8_583 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CloudRegionCode PhotonHandler::get_BestRegionCodeInPreferences()
extern "C" int32_t PhotonHandler_get_BestRegionCodeInPreferences_m8_584 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::set_BestRegionCodeInPreferences(CloudRegionCode)
extern "C" void PhotonHandler_set_BestRegionCodeInPreferences_m8_585 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonHandler::PingAvailableRegionsAndConnectToBest()
extern "C" void PhotonHandler_PingAvailableRegionsAndConnectToBest_m8_586 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PhotonHandler::PingAvailableRegionsCoroutine(System.Boolean)
extern "C" Object_t * PhotonHandler_PingAvailableRegionsCoroutine_m8_587 (PhotonHandler_t8_111 * __this, bool ___connectToBest, const MethodInfo* method) IL2CPP_METHOD_ATTR;
