﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonLagSimulationGui
struct PhotonLagSimulationGui_t8_112;
// ExitGames.Client.Photon.PhotonPeer
struct PhotonPeer_t5_38;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonLagSimulationGui::.ctor()
extern "C" void PhotonLagSimulationGui__ctor_m8_588 (PhotonLagSimulationGui_t8_112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.PhotonPeer PhotonLagSimulationGui::get_Peer()
extern "C" PhotonPeer_t5_38 * PhotonLagSimulationGui_get_Peer_m8_589 (PhotonLagSimulationGui_t8_112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonLagSimulationGui::set_Peer(ExitGames.Client.Photon.PhotonPeer)
extern "C" void PhotonLagSimulationGui_set_Peer_m8_590 (PhotonLagSimulationGui_t8_112 * __this, PhotonPeer_t5_38 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonLagSimulationGui::Start()
extern "C" void PhotonLagSimulationGui_Start_m8_591 (PhotonLagSimulationGui_t8_112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonLagSimulationGui::OnGUI()
extern "C" void PhotonLagSimulationGui_OnGUI_m8_592 (PhotonLagSimulationGui_t8_112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonLagSimulationGui::NetSimHasNoPeerWindow(System.Int32)
extern "C" void PhotonLagSimulationGui_NetSimHasNoPeerWindow_m8_593 (PhotonLagSimulationGui_t8_112 * __this, int32_t ___windowId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonLagSimulationGui::NetSimWindow(System.Int32)
extern "C" void PhotonLagSimulationGui_NetSimWindow_m8_594 (PhotonLagSimulationGui_t8_112 * __this, int32_t ___windowId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
