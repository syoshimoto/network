﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_PhotonLogLevel.h"

// PhotonLogLevel
struct  PhotonLogLevel_t8_63 
{
	// System.Int32 PhotonLogLevel::value__
	int32_t ___value___1;
};
