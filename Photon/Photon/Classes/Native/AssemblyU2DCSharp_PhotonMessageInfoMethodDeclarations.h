﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;
// PhotonPlayer
struct PhotonPlayer_t8_102;
// PhotonView
struct PhotonView_t8_3;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonMessageInfo::.ctor()
extern "C" void PhotonMessageInfo__ctor_m8_524 (PhotonMessageInfo_t8_104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonMessageInfo::.ctor(PhotonPlayer,System.Int32,PhotonView)
extern "C" void PhotonMessageInfo__ctor_m8_525 (PhotonMessageInfo_t8_104 * __this, PhotonPlayer_t8_102 * ___player, int32_t ___timestamp, PhotonView_t8_3 * ___view, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double PhotonMessageInfo::get_timestamp()
extern "C" double PhotonMessageInfo_get_timestamp_m8_526 (PhotonMessageInfo_t8_104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhotonMessageInfo::ToString()
extern "C" String_t* PhotonMessageInfo_ToString_m8_527 (PhotonMessageInfo_t8_104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
