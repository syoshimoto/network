﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// AuthenticationValues
struct AuthenticationValues_t8_97;
// Room
struct Room_t8_100;
// PhotonPlayer
struct PhotonPlayer_t8_102;
// PhotonPlayer[]
struct PhotonPlayerU5BU5D_t8_101;
// System.Collections.Generic.List`1<FriendInfo>
struct List_1_t1_946;
// IPunPrefabPool
struct IPunPrefabPool_t8_103;
// System.Collections.Generic.List`1<TypedLobbyInfo>
struct List_1_t1_934;
// TypedLobby
struct TypedLobby_t8_79;
// System.String[]
struct StringU5BU5D_t1_202;
// RoomOptions
struct RoomOptions_t8_78;
// ExitGames.Client.Photon.Hashtable
struct Hashtable_t5_1;
// RoomInfo[]
struct RoomInfoU5BU5D_t8_99;
// System.Object
struct Object_t;
// RaiseEventOptions
struct RaiseEventOptions_t8_93;
// System.Int32[]
struct Int32U5BU5D_t1_160;
// UnityEngine.GameObject
struct GameObject_t6_85;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// PhotonView
struct PhotonView_t8_3;
// System.Type
struct Type_t;
// System.Collections.Generic.HashSet`1<UnityEngine.GameObject>
struct HashSet_1_t2_16;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ConnectionState.h"
#include "AssemblyU2DCSharp_PeerState.h"
#include "AssemblyU2DCSharp_ServerConnection.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"
#include "AssemblyU2DCSharp_CloudRegionCode.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_MatchmakingMode.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "AssemblyU2DCSharp_PhotonTargets.h"

// System.Void PhotonNetwork::.cctor()
extern "C" void PhotonNetwork__cctor_m8_599 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhotonNetwork::get_gameVersion()
extern "C" String_t* PhotonNetwork_get_gameVersion_m8_600 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_gameVersion(System.String)
extern "C" void PhotonNetwork_set_gameVersion_m8_601 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhotonNetwork::get_ServerAddress()
extern "C" String_t* PhotonNetwork_get_ServerAddress_m8_602 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_connected()
extern "C" bool PhotonNetwork_get_connected_m8_603 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_connecting()
extern "C" bool PhotonNetwork_get_connecting_m8_604 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_connectedAndReady()
extern "C" bool PhotonNetwork_get_connectedAndReady_m8_605 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ConnectionState PhotonNetwork::get_connectionState()
extern "C" int32_t PhotonNetwork_get_connectionState_m8_606 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PeerState PhotonNetwork::get_connectionStateDetailed()
extern "C" int32_t PhotonNetwork_get_connectionStateDetailed_m8_607 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ServerConnection PhotonNetwork::get_Server()
extern "C" int32_t PhotonNetwork_get_Server_m8_608 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AuthenticationValues PhotonNetwork::get_AuthValues()
extern "C" AuthenticationValues_t8_97 * PhotonNetwork_get_AuthValues_m8_609 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_AuthValues(AuthenticationValues)
extern "C" void PhotonNetwork_set_AuthValues_m8_610 (Object_t * __this /* static, unused */, AuthenticationValues_t8_97 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Room PhotonNetwork::get_room()
extern "C" Room_t8_100 * PhotonNetwork_get_room_m8_611 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonPlayer PhotonNetwork::get_player()
extern "C" PhotonPlayer_t8_102 * PhotonNetwork_get_player_m8_612 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonPlayer PhotonNetwork::get_masterClient()
extern "C" PhotonPlayer_t8_102 * PhotonNetwork_get_masterClient_m8_613 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhotonNetwork::get_playerName()
extern "C" String_t* PhotonNetwork_get_playerName_m8_614 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_playerName(System.String)
extern "C" void PhotonNetwork_set_playerName_m8_615 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonPlayer[] PhotonNetwork::get_playerList()
extern "C" PhotonPlayerU5BU5D_t8_101* PhotonNetwork_get_playerList_m8_616 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonPlayer[] PhotonNetwork::get_otherPlayers()
extern "C" PhotonPlayerU5BU5D_t8_101* PhotonNetwork_get_otherPlayers_m8_617 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FriendInfo> PhotonNetwork::get_Friends()
extern "C" List_1_t1_946 * PhotonNetwork_get_Friends_m8_618 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_Friends(System.Collections.Generic.List`1<FriendInfo>)
extern "C" void PhotonNetwork_set_Friends_m8_619 (Object_t * __this /* static, unused */, List_1_t1_946 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_FriendsListAge()
extern "C" int32_t PhotonNetwork_get_FriendsListAge_m8_620 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IPunPrefabPool PhotonNetwork::get_PrefabPool()
extern "C" Object_t * PhotonNetwork_get_PrefabPool_m8_621 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_PrefabPool(IPunPrefabPool)
extern "C" void PhotonNetwork_set_PrefabPool_m8_622 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_offlineMode()
extern "C" bool PhotonNetwork_get_offlineMode_m8_623 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_offlineMode(System.Boolean)
extern "C" void PhotonNetwork_set_offlineMode_m8_624 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_automaticallySyncScene()
extern "C" bool PhotonNetwork_get_automaticallySyncScene_m8_625 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_automaticallySyncScene(System.Boolean)
extern "C" void PhotonNetwork_set_automaticallySyncScene_m8_626 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_autoCleanUpPlayerObjects()
extern "C" bool PhotonNetwork_get_autoCleanUpPlayerObjects_m8_627 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_autoCleanUpPlayerObjects(System.Boolean)
extern "C" void PhotonNetwork_set_autoCleanUpPlayerObjects_m8_628 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_autoJoinLobby()
extern "C" bool PhotonNetwork_get_autoJoinLobby_m8_629 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_autoJoinLobby(System.Boolean)
extern "C" void PhotonNetwork_set_autoJoinLobby_m8_630 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_EnableLobbyStatistics()
extern "C" bool PhotonNetwork_get_EnableLobbyStatistics_m8_631 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_EnableLobbyStatistics(System.Boolean)
extern "C" void PhotonNetwork_set_EnableLobbyStatistics_m8_632 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<TypedLobbyInfo> PhotonNetwork::get_LobbyStatistics()
extern "C" List_1_t1_934 * PhotonNetwork_get_LobbyStatistics_m8_633 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_LobbyStatistics(System.Collections.Generic.List`1<TypedLobbyInfo>)
extern "C" void PhotonNetwork_set_LobbyStatistics_m8_634 (Object_t * __this /* static, unused */, List_1_t1_934 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_insideLobby()
extern "C" bool PhotonNetwork_get_insideLobby_m8_635 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TypedLobby PhotonNetwork::get_lobby()
extern "C" TypedLobby_t8_79 * PhotonNetwork_get_lobby_m8_636 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_lobby(TypedLobby)
extern "C" void PhotonNetwork_set_lobby_m8_637 (Object_t * __this /* static, unused */, TypedLobby_t8_79 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_sendRate()
extern "C" int32_t PhotonNetwork_get_sendRate_m8_638 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_sendRate(System.Int32)
extern "C" void PhotonNetwork_set_sendRate_m8_639 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_sendRateOnSerialize()
extern "C" int32_t PhotonNetwork_get_sendRateOnSerialize_m8_640 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_sendRateOnSerialize(System.Int32)
extern "C" void PhotonNetwork_set_sendRateOnSerialize_m8_641 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_isMessageQueueRunning()
extern "C" bool PhotonNetwork_get_isMessageQueueRunning_m8_642 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_isMessageQueueRunning(System.Boolean)
extern "C" void PhotonNetwork_set_isMessageQueueRunning_m8_643 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_unreliableCommandsLimit()
extern "C" int32_t PhotonNetwork_get_unreliableCommandsLimit_m8_644 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_unreliableCommandsLimit(System.Int32)
extern "C" void PhotonNetwork_set_unreliableCommandsLimit_m8_645 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double PhotonNetwork::get_time()
extern "C" double PhotonNetwork_get_time_m8_646 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_ServerTimestamp()
extern "C" int32_t PhotonNetwork_get_ServerTimestamp_m8_647 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_isMasterClient()
extern "C" bool PhotonNetwork_get_isMasterClient_m8_648 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_inRoom()
extern "C" bool PhotonNetwork_get_inRoom_m8_649 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_isNonMasterClientInRoom()
extern "C" bool PhotonNetwork_get_isNonMasterClientInRoom_m8_650 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_countOfPlayersOnMaster()
extern "C" int32_t PhotonNetwork_get_countOfPlayersOnMaster_m8_651 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_countOfPlayersInRooms()
extern "C" int32_t PhotonNetwork_get_countOfPlayersInRooms_m8_652 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_countOfPlayers()
extern "C" int32_t PhotonNetwork_get_countOfPlayers_m8_653 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_countOfRooms()
extern "C" int32_t PhotonNetwork_get_countOfRooms_m8_654 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_NetworkStatisticsEnabled()
extern "C" bool PhotonNetwork_get_NetworkStatisticsEnabled_m8_655 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_NetworkStatisticsEnabled(System.Boolean)
extern "C" void PhotonNetwork_set_NetworkStatisticsEnabled_m8_656 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_ResentReliableCommands()
extern "C" int32_t PhotonNetwork_get_ResentReliableCommands_m8_657 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::get_CrcCheckEnabled()
extern "C" bool PhotonNetwork_get_CrcCheckEnabled_m8_658 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_CrcCheckEnabled(System.Boolean)
extern "C" void PhotonNetwork_set_CrcCheckEnabled_m8_659 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_PacketLossByCrcCheck()
extern "C" int32_t PhotonNetwork_get_PacketLossByCrcCheck_m8_660 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_MaxResendsBeforeDisconnect()
extern "C" int32_t PhotonNetwork_get_MaxResendsBeforeDisconnect_m8_661 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_MaxResendsBeforeDisconnect(System.Int32)
extern "C" void PhotonNetwork_set_MaxResendsBeforeDisconnect_m8_662 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::get_QuickResends()
extern "C" int32_t PhotonNetwork_get_QuickResends_m8_663 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::set_QuickResends(System.Int32)
extern "C" void PhotonNetwork_set_QuickResends_m8_664 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::SwitchToProtocol(ExitGames.Client.Photon.ConnectionProtocol)
extern "C" void PhotonNetwork_SwitchToProtocol_m8_665 (Object_t * __this /* static, unused */, uint8_t ___cp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::ConnectUsingSettings(System.String)
extern "C" bool PhotonNetwork_ConnectUsingSettings_m8_666 (Object_t * __this /* static, unused */, String_t* ___gameVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::ConnectToMaster(System.String,System.Int32,System.String,System.String)
extern "C" bool PhotonNetwork_ConnectToMaster_m8_667 (Object_t * __this /* static, unused */, String_t* ___masterServerAddress, int32_t ___port, String_t* ___appID, String_t* ___gameVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::ConnectToBestCloudServer(System.String)
extern "C" bool PhotonNetwork_ConnectToBestCloudServer_m8_668 (Object_t * __this /* static, unused */, String_t* ___gameVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::ConnectToRegion(CloudRegionCode,System.String)
extern "C" bool PhotonNetwork_ConnectToRegion_m8_669 (Object_t * __this /* static, unused */, int32_t ___region, String_t* ___gameVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::OverrideBestCloudServer(CloudRegionCode)
extern "C" void PhotonNetwork_OverrideBestCloudServer_m8_670 (Object_t * __this /* static, unused */, int32_t ___region, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::RefreshCloudServerRating()
extern "C" void PhotonNetwork_RefreshCloudServerRating_m8_671 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::NetworkStatisticsReset()
extern "C" void PhotonNetwork_NetworkStatisticsReset_m8_672 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhotonNetwork::NetworkStatisticsToString()
extern "C" String_t* PhotonNetwork_NetworkStatisticsToString_m8_673 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::InitializeSecurity()
extern "C" void PhotonNetwork_InitializeSecurity_m8_674 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::VerifyCanUseNetwork()
extern "C" bool PhotonNetwork_VerifyCanUseNetwork_m8_675 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::Disconnect()
extern "C" void PhotonNetwork_Disconnect_m8_676 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::FindFriends(System.String[])
extern "C" bool PhotonNetwork_FindFriends_m8_677 (Object_t * __this /* static, unused */, StringU5BU5D_t1_202* ___friendsToFind, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::CreateRoom(System.String)
extern "C" bool PhotonNetwork_CreateRoom_m8_678 (Object_t * __this /* static, unused */, String_t* ___roomName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::CreateRoom(System.String,RoomOptions,TypedLobby)
extern "C" bool PhotonNetwork_CreateRoom_m8_679 (Object_t * __this /* static, unused */, String_t* ___roomName, RoomOptions_t8_78 * ___roomOptions, TypedLobby_t8_79 * ___typedLobby, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::CreateRoom(System.String,RoomOptions,TypedLobby,System.String[])
extern "C" bool PhotonNetwork_CreateRoom_m8_680 (Object_t * __this /* static, unused */, String_t* ___roomName, RoomOptions_t8_78 * ___roomOptions, TypedLobby_t8_79 * ___typedLobby, StringU5BU5D_t1_202* ___expectedUsers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::JoinRoom(System.String)
extern "C" bool PhotonNetwork_JoinRoom_m8_681 (Object_t * __this /* static, unused */, String_t* ___roomName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::JoinOrCreateRoom(System.String,RoomOptions,TypedLobby)
extern "C" bool PhotonNetwork_JoinOrCreateRoom_m8_682 (Object_t * __this /* static, unused */, String_t* ___roomName, RoomOptions_t8_78 * ___roomOptions, TypedLobby_t8_79 * ___typedLobby, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::JoinRandomRoom()
extern "C" bool PhotonNetwork_JoinRandomRoom_m8_683 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::JoinRandomRoom(ExitGames.Client.Photon.Hashtable,System.Byte)
extern "C" bool PhotonNetwork_JoinRandomRoom_m8_684 (Object_t * __this /* static, unused */, Hashtable_t5_1 * ___expectedCustomRoomProperties, uint8_t ___expectedMaxPlayers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::JoinRandomRoom(ExitGames.Client.Photon.Hashtable,System.Byte,ExitGames.Client.Photon.MatchmakingMode,TypedLobby,System.String)
extern "C" bool PhotonNetwork_JoinRandomRoom_m8_685 (Object_t * __this /* static, unused */, Hashtable_t5_1 * ___expectedCustomRoomProperties, uint8_t ___expectedMaxPlayers, uint8_t ___matchingType, TypedLobby_t8_79 * ___typedLobby, String_t* ___sqlLobbyFilter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::EnterOfflineRoom(System.String,RoomOptions,System.Boolean)
extern "C" void PhotonNetwork_EnterOfflineRoom_m8_686 (Object_t * __this /* static, unused */, String_t* ___roomName, RoomOptions_t8_78 * ___roomOptions, bool ___createdRoom, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::JoinLobby()
extern "C" bool PhotonNetwork_JoinLobby_m8_687 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::JoinLobby(TypedLobby)
extern "C" bool PhotonNetwork_JoinLobby_m8_688 (Object_t * __this /* static, unused */, TypedLobby_t8_79 * ___typedLobby, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::LeaveLobby()
extern "C" bool PhotonNetwork_LeaveLobby_m8_689 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::LeaveRoom()
extern "C" bool PhotonNetwork_LeaveRoom_m8_690 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RoomInfo[] PhotonNetwork::GetRoomList()
extern "C" RoomInfoU5BU5D_t8_99* PhotonNetwork_GetRoomList_m8_691 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::SetPlayerCustomProperties(ExitGames.Client.Photon.Hashtable)
extern "C" void PhotonNetwork_SetPlayerCustomProperties_m8_692 (Object_t * __this /* static, unused */, Hashtable_t5_1 * ___customProperties, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::RemovePlayerCustomProperties(System.String[])
extern "C" void PhotonNetwork_RemovePlayerCustomProperties_m8_693 (Object_t * __this /* static, unused */, StringU5BU5D_t1_202* ___customPropertiesToDelete, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::RaiseEvent(System.Byte,System.Object,System.Boolean,RaiseEventOptions)
extern "C" bool PhotonNetwork_RaiseEvent_m8_694 (Object_t * __this /* static, unused */, uint8_t ___eventCode, Object_t * ___eventContent, bool ___sendReliable, RaiseEventOptions_t8_93 * ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::AllocateViewID()
extern "C" int32_t PhotonNetwork_AllocateViewID_m8_695 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::AllocateSceneViewID()
extern "C" int32_t PhotonNetwork_AllocateSceneViewID_m8_696 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::AllocateViewID(System.Int32)
extern "C" int32_t PhotonNetwork_AllocateViewID_m8_697 (Object_t * __this /* static, unused */, int32_t ___ownerId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] PhotonNetwork::AllocateSceneViewIDs(System.Int32)
extern "C" Int32U5BU5D_t1_160* PhotonNetwork_AllocateSceneViewIDs_m8_698 (Object_t * __this /* static, unused */, int32_t ___countOfNewViews, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::UnAllocateViewID(System.Int32)
extern "C" void PhotonNetwork_UnAllocateViewID_m8_699 (Object_t * __this /* static, unused */, int32_t ___viewID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PhotonNetwork::Instantiate(System.String,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C" GameObject_t6_85 * PhotonNetwork_Instantiate_m8_700 (Object_t * __this /* static, unused */, String_t* ___prefabName, Vector3_t6_49  ___position, Quaternion_t6_51  ___rotation, int32_t ___group, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PhotonNetwork::Instantiate(System.String,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,System.Object[])
extern "C" GameObject_t6_85 * PhotonNetwork_Instantiate_m8_701 (Object_t * __this /* static, unused */, String_t* ___prefabName, Vector3_t6_49  ___position, Quaternion_t6_51  ___rotation, int32_t ___group, ObjectU5BU5D_t1_157* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PhotonNetwork::InstantiateSceneObject(System.String,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,System.Object[])
extern "C" GameObject_t6_85 * PhotonNetwork_InstantiateSceneObject_m8_702 (Object_t * __this /* static, unused */, String_t* ___prefabName, Vector3_t6_49  ___position, Quaternion_t6_51  ___rotation, int32_t ___group, ObjectU5BU5D_t1_157* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonNetwork::GetPing()
extern "C" int32_t PhotonNetwork_GetPing_m8_703 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::FetchServerTimestamp()
extern "C" void PhotonNetwork_FetchServerTimestamp_m8_704 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::SendOutgoingCommands()
extern "C" void PhotonNetwork_SendOutgoingCommands_m8_705 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::CloseConnection(PhotonPlayer)
extern "C" bool PhotonNetwork_CloseConnection_m8_706 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___kickPlayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::SetMasterClient(PhotonPlayer)
extern "C" bool PhotonNetwork_SetMasterClient_m8_707 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___masterClientPlayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::Destroy(PhotonView)
extern "C" void PhotonNetwork_Destroy_m8_708 (Object_t * __this /* static, unused */, PhotonView_t8_3 * ___targetView, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::Destroy(UnityEngine.GameObject)
extern "C" void PhotonNetwork_Destroy_m8_709 (Object_t * __this /* static, unused */, GameObject_t6_85 * ___targetGo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::DestroyPlayerObjects(PhotonPlayer)
extern "C" void PhotonNetwork_DestroyPlayerObjects_m8_710 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___targetPlayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::DestroyPlayerObjects(System.Int32)
extern "C" void PhotonNetwork_DestroyPlayerObjects_m8_711 (Object_t * __this /* static, unused */, int32_t ___targetPlayerId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::DestroyAll()
extern "C" void PhotonNetwork_DestroyAll_m8_712 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::RemoveRPCs(PhotonPlayer)
extern "C" void PhotonNetwork_RemoveRPCs_m8_713 (Object_t * __this /* static, unused */, PhotonPlayer_t8_102 * ___targetPlayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::RemoveRPCs(PhotonView)
extern "C" void PhotonNetwork_RemoveRPCs_m8_714 (Object_t * __this /* static, unused */, PhotonView_t8_3 * ___targetPhotonView, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::RemoveRPCsInGroup(System.Int32)
extern "C" void PhotonNetwork_RemoveRPCsInGroup_m8_715 (Object_t * __this /* static, unused */, int32_t ___targetGroup, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::RPC(PhotonView,System.String,PhotonTargets,System.Boolean,System.Object[])
extern "C" void PhotonNetwork_RPC_m8_716 (Object_t * __this /* static, unused */, PhotonView_t8_3 * ___view, String_t* ___methodName, int32_t ___target, bool ___encrypt, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::RPC(PhotonView,System.String,PhotonPlayer,System.Boolean,System.Object[])
extern "C" void PhotonNetwork_RPC_m8_717 (Object_t * __this /* static, unused */, PhotonView_t8_3 * ___view, String_t* ___methodName, PhotonPlayer_t8_102 * ___targetPlayer, bool ___encrpyt, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::CacheSendMonoMessageTargets(System.Type)
extern "C" void PhotonNetwork_CacheSendMonoMessageTargets_m8_718 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.HashSet`1<UnityEngine.GameObject> PhotonNetwork::FindGameObjectsWithComponent(System.Type)
extern "C" HashSet_1_t2_16 * PhotonNetwork_FindGameObjectsWithComponent_m8_719 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::SetReceivingEnabled(System.Int32,System.Boolean)
extern "C" void PhotonNetwork_SetReceivingEnabled_m8_720 (Object_t * __this /* static, unused */, int32_t ___group, bool ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::SetReceivingEnabled(System.Int32[],System.Int32[])
extern "C" void PhotonNetwork_SetReceivingEnabled_m8_721 (Object_t * __this /* static, unused */, Int32U5BU5D_t1_160* ___enableGroups, Int32U5BU5D_t1_160* ___disableGroups, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::SetSendingEnabled(System.Int32,System.Boolean)
extern "C" void PhotonNetwork_SetSendingEnabled_m8_722 (Object_t * __this /* static, unused */, int32_t ___group, bool ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::SetSendingEnabled(System.Int32[],System.Int32[])
extern "C" void PhotonNetwork_SetSendingEnabled_m8_723 (Object_t * __this /* static, unused */, Int32U5BU5D_t1_160* ___enableGroups, Int32U5BU5D_t1_160* ___disableGroups, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::SetLevelPrefix(System.Int16)
extern "C" void PhotonNetwork_SetLevelPrefix_m8_724 (Object_t * __this /* static, unused */, int16_t ___prefix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::LoadLevel(System.Int32)
extern "C" void PhotonNetwork_LoadLevel_m8_725 (Object_t * __this /* static, unused */, int32_t ___levelNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonNetwork::LoadLevel(System.String)
extern "C" void PhotonNetwork_LoadLevel_m8_726 (Object_t * __this /* static, unused */, String_t* ___levelName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonNetwork::WebRpc(System.String,System.Object)
extern "C" bool PhotonNetwork_WebRpc_m8_727 (Object_t * __this /* static, unused */, String_t* ___name, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
