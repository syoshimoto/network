﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_Int32.h"

// PhotonNetwork/EventCallback
struct  EventCallback_t8_113  : public MulticastDelegate_t1_21
{
};
