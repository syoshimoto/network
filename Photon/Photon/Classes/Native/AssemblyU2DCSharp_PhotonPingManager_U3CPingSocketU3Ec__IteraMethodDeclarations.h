﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonPingManager/<PingSocket>c__Iterator1
struct U3CPingSocketU3Ec__Iterator1_t8_122;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonPingManager/<PingSocket>c__Iterator1::.ctor()
extern "C" void U3CPingSocketU3Ec__Iterator1__ctor_m8_794 (U3CPingSocketU3Ec__Iterator1_t8_122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PhotonPingManager/<PingSocket>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CPingSocketU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_795 (U3CPingSocketU3Ec__Iterator1_t8_122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PhotonPingManager/<PingSocket>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CPingSocketU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m8_796 (U3CPingSocketU3Ec__Iterator1_t8_122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonPingManager/<PingSocket>c__Iterator1::MoveNext()
extern "C" bool U3CPingSocketU3Ec__Iterator1_MoveNext_m8_797 (U3CPingSocketU3Ec__Iterator1_t8_122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonPingManager/<PingSocket>c__Iterator1::Dispose()
extern "C" void U3CPingSocketU3Ec__Iterator1_Dispose_m8_798 (U3CPingSocketU3Ec__Iterator1_t8_122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonPingManager/<PingSocket>c__Iterator1::Reset()
extern "C" void U3CPingSocketU3Ec__Iterator1_Reset_m8_799 (U3CPingSocketU3Ec__Iterator1_t8_122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
