﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonRigidbodyView
struct PhotonRigidbodyView_t8_137;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"

// System.Void PhotonRigidbodyView::.ctor()
extern "C" void PhotonRigidbodyView__ctor_m8_890 (PhotonRigidbodyView_t8_137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonRigidbodyView::Awake()
extern "C" void PhotonRigidbodyView_Awake_m8_891 (PhotonRigidbodyView_t8_137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonRigidbodyView::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void PhotonRigidbodyView_OnPhotonSerializeView_m8_892 (PhotonRigidbodyView_t8_137 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
