﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhotonTransformViewPositionModel
struct PhotonTransformViewPositionModel_t8_138;
// PhotonTransformViewRotationModel
struct PhotonTransformViewRotationModel_t8_139;
// PhotonTransformViewScaleModel
struct PhotonTransformViewScaleModel_t8_140;
// PhotonTransformViewPositionControl
struct PhotonTransformViewPositionControl_t8_141;
// PhotonTransformViewRotationControl
struct PhotonTransformViewRotationControl_t8_142;
// PhotonTransformViewScaleControl
struct PhotonTransformViewScaleControl_t8_143;
// PhotonView
struct PhotonView_t8_3;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// PhotonTransformView
struct  PhotonTransformView_t8_39  : public MonoBehaviour_t6_80
{
	// PhotonTransformViewPositionModel PhotonTransformView::m_PositionModel
	PhotonTransformViewPositionModel_t8_138 * ___m_PositionModel_2;
	// PhotonTransformViewRotationModel PhotonTransformView::m_RotationModel
	PhotonTransformViewRotationModel_t8_139 * ___m_RotationModel_3;
	// PhotonTransformViewScaleModel PhotonTransformView::m_ScaleModel
	PhotonTransformViewScaleModel_t8_140 * ___m_ScaleModel_4;
	// PhotonTransformViewPositionControl PhotonTransformView::m_PositionControl
	PhotonTransformViewPositionControl_t8_141 * ___m_PositionControl_5;
	// PhotonTransformViewRotationControl PhotonTransformView::m_RotationControl
	PhotonTransformViewRotationControl_t8_142 * ___m_RotationControl_6;
	// PhotonTransformViewScaleControl PhotonTransformView::m_ScaleControl
	PhotonTransformViewScaleControl_t8_143 * ___m_ScaleControl_7;
	// PhotonView PhotonTransformView::m_PhotonView
	PhotonView_t8_3 * ___m_PhotonView_8;
	// System.Boolean PhotonTransformView::m_ReceivedNetworkUpdate
	bool ___m_ReceivedNetworkUpdate_9;
};
