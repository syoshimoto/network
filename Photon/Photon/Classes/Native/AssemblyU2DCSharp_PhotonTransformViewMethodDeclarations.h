﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonTransformView
struct PhotonTransformView_t8_39;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void PhotonTransformView::.ctor()
extern "C" void PhotonTransformView__ctor_m8_893 (PhotonTransformView_t8_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformView::Awake()
extern "C" void PhotonTransformView_Awake_m8_894 (PhotonTransformView_t8_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformView::Update()
extern "C" void PhotonTransformView_Update_m8_895 (PhotonTransformView_t8_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformView::UpdatePosition()
extern "C" void PhotonTransformView_UpdatePosition_m8_896 (PhotonTransformView_t8_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformView::UpdateRotation()
extern "C" void PhotonTransformView_UpdateRotation_m8_897 (PhotonTransformView_t8_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformView::UpdateScale()
extern "C" void PhotonTransformView_UpdateScale_m8_898 (PhotonTransformView_t8_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformView::SetSynchronizedValues(UnityEngine.Vector3,System.Single)
extern "C" void PhotonTransformView_SetSynchronizedValues_m8_899 (PhotonTransformView_t8_39 * __this, Vector3_t6_49  ___speed, float ___turnSpeed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformView::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void PhotonTransformView_OnPhotonSerializeView_m8_900 (PhotonTransformView_t8_39 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformView::DoDrawEstimatedPositionError()
extern "C" void PhotonTransformView_DoDrawEstimatedPositionError_m8_901 (PhotonTransformView_t8_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
