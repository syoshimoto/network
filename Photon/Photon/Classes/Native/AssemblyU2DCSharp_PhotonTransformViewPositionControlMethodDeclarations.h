﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonTransformViewPositionControl
struct PhotonTransformViewPositionControl_t8_141;
// PhotonTransformViewPositionModel
struct PhotonTransformViewPositionModel_t8_138;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void PhotonTransformViewPositionControl::.ctor(PhotonTransformViewPositionModel)
extern "C" void PhotonTransformViewPositionControl__ctor_m8_902 (PhotonTransformViewPositionControl_t8_141 * __this, PhotonTransformViewPositionModel_t8_138 * ___model, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PhotonTransformViewPositionControl::GetOldestStoredNetworkPosition()
extern "C" Vector3_t6_49  PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m8_903 (PhotonTransformViewPositionControl_t8_141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformViewPositionControl::SetSynchronizedValues(UnityEngine.Vector3,System.Single)
extern "C" void PhotonTransformViewPositionControl_SetSynchronizedValues_m8_904 (PhotonTransformViewPositionControl_t8_141 * __this, Vector3_t6_49  ___speed, float ___turnSpeed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PhotonTransformViewPositionControl::UpdatePosition(UnityEngine.Vector3)
extern "C" Vector3_t6_49  PhotonTransformViewPositionControl_UpdatePosition_m8_905 (PhotonTransformViewPositionControl_t8_141 * __this, Vector3_t6_49  ___currentPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PhotonTransformViewPositionControl::GetNetworkPosition()
extern "C" Vector3_t6_49  PhotonTransformViewPositionControl_GetNetworkPosition_m8_906 (PhotonTransformViewPositionControl_t8_141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PhotonTransformViewPositionControl::GetExtrapolatedPositionOffset()
extern "C" Vector3_t6_49  PhotonTransformViewPositionControl_GetExtrapolatedPositionOffset_m8_907 (PhotonTransformViewPositionControl_t8_141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformViewPositionControl::OnPhotonSerializeView(UnityEngine.Vector3,PhotonStream,PhotonMessageInfo)
extern "C" void PhotonTransformViewPositionControl_OnPhotonSerializeView_m8_908 (PhotonTransformViewPositionControl_t8_141 * __this, Vector3_t6_49  ___currentPosition, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformViewPositionControl::SerializeData(UnityEngine.Vector3,PhotonStream,PhotonMessageInfo)
extern "C" void PhotonTransformViewPositionControl_SerializeData_m8_909 (PhotonTransformViewPositionControl_t8_141 * __this, Vector3_t6_49  ___currentPosition, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonTransformViewPositionControl::DeserializeData(PhotonStream,PhotonMessageInfo)
extern "C" void PhotonTransformViewPositionControl_DeserializeData_m8_910 (PhotonTransformViewPositionControl_t8_141 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
