﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel_Interpola.h"

// PhotonTransformViewPositionModel/InterpolateOptions
struct  InterpolateOptions_t8_144 
{
	// System.Int32 PhotonTransformViewPositionModel/InterpolateOptions::value__
	int32_t ___value___1;
};
