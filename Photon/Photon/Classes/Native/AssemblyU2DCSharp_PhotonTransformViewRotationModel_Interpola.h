﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationModel_Interpola.h"

// PhotonTransformViewRotationModel/InterpolateOptions
struct  InterpolateOptions_t8_146 
{
	// System.Int32 PhotonTransformViewRotationModel/InterpolateOptions::value__
	int32_t ___value___1;
};
