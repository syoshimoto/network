﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhotonTransformViewScaleModel
struct PhotonTransformViewScaleModel_t8_140;

#include "mscorlib_System_Object.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// PhotonTransformViewScaleControl
struct  PhotonTransformViewScaleControl_t8_143  : public Object_t
{
	// PhotonTransformViewScaleModel PhotonTransformViewScaleControl::m_Model
	PhotonTransformViewScaleModel_t8_140 * ___m_Model_0;
	// UnityEngine.Vector3 PhotonTransformViewScaleControl::m_NetworkScale
	Vector3_t6_49  ___m_NetworkScale_1;
};
