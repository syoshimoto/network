﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonView
struct PhotonView_t8_3;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// PhotonPlayer
struct PhotonPlayer_t8_102;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;
// UnityEngine.Component
struct Component_t6_26;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t6_85;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhotonTargets.h"

// System.Void PhotonView::.ctor()
extern "C" void PhotonView__ctor_m8_762 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonView::get_prefix()
extern "C" int32_t PhotonView_get_prefix_m8_763 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::set_prefix(System.Int32)
extern "C" void PhotonView_set_prefix_m8_764 (PhotonView_t8_3 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] PhotonView::get_instantiationData()
extern "C" ObjectU5BU5D_t1_157* PhotonView_get_instantiationData_m8_765 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::set_instantiationData(System.Object[])
extern "C" void PhotonView_set_instantiationData_m8_766 (PhotonView_t8_3 * __this, ObjectU5BU5D_t1_157* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonView::get_viewID()
extern "C" int32_t PhotonView_get_viewID_m8_767 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::set_viewID(System.Int32)
extern "C" void PhotonView_set_viewID_m8_768 (PhotonView_t8_3 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonView::get_isSceneView()
extern "C" bool PhotonView_get_isSceneView_m8_769 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonPlayer PhotonView::get_owner()
extern "C" PhotonPlayer_t8_102 * PhotonView_get_owner_m8_770 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonView::get_OwnerActorNr()
extern "C" int32_t PhotonView_get_OwnerActorNr_m8_771 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonView::get_isOwnerActive()
extern "C" bool PhotonView_get_isOwnerActive_m8_772 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhotonView::get_CreatorActorNr()
extern "C" int32_t PhotonView_get_CreatorActorNr_m8_773 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhotonView::get_isMine()
extern "C" bool PhotonView_get_isMine_m8_774 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::Awake()
extern "C" void PhotonView_Awake_m8_775 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::RequestOwnership()
extern "C" void PhotonView_RequestOwnership_m8_776 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::TransferOwnership(PhotonPlayer)
extern "C" void PhotonView_TransferOwnership_m8_777 (PhotonView_t8_3 * __this, PhotonPlayer_t8_102 * ___newOwner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::TransferOwnership(System.Int32)
extern "C" void PhotonView_TransferOwnership_m8_778 (PhotonView_t8_3 * __this, int32_t ___newOwnerId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::OnDestroy()
extern "C" void PhotonView_OnDestroy_m8_779 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::SerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void PhotonView_SerializeView_m8_780 (PhotonView_t8_3 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::DeserializeView(PhotonStream,PhotonMessageInfo)
extern "C" void PhotonView_DeserializeView_m8_781 (PhotonView_t8_3 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::DeserializeComponent(UnityEngine.Component,PhotonStream,PhotonMessageInfo)
extern "C" void PhotonView_DeserializeComponent_m8_782 (PhotonView_t8_3 * __this, Component_t6_26 * ___component, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::SerializeComponent(UnityEngine.Component,PhotonStream,PhotonMessageInfo)
extern "C" void PhotonView_SerializeComponent_m8_783 (PhotonView_t8_3 * __this, Component_t6_26 * ___component, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::ExecuteComponentOnSerialize(UnityEngine.Component,PhotonStream,PhotonMessageInfo)
extern "C" void PhotonView_ExecuteComponentOnSerialize_m8_784 (PhotonView_t8_3 * __this, Component_t6_26 * ___component, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::RefreshRpcMonoBehaviourCache()
extern "C" void PhotonView_RefreshRpcMonoBehaviourCache_m8_785 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::RPC(System.String,PhotonTargets,System.Object[])
extern "C" void PhotonView_RPC_m8_786 (PhotonView_t8_3 * __this, String_t* ___methodName, int32_t ___target, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::RpcSecure(System.String,PhotonTargets,System.Boolean,System.Object[])
extern "C" void PhotonView_RpcSecure_m8_787 (PhotonView_t8_3 * __this, String_t* ___methodName, int32_t ___target, bool ___encrypt, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::RPC(System.String,PhotonPlayer,System.Object[])
extern "C" void PhotonView_RPC_m8_788 (PhotonView_t8_3 * __this, String_t* ___methodName, PhotonPlayer_t8_102 * ___targetPlayer, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonView::RpcSecure(System.String,PhotonPlayer,System.Boolean,System.Object[])
extern "C" void PhotonView_RpcSecure_m8_789 (PhotonView_t8_3 * __this, String_t* ___methodName, PhotonPlayer_t8_102 * ___targetPlayer, bool ___encrypt, ObjectU5BU5D_t1_157* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonView PhotonView::Get(UnityEngine.Component)
extern "C" PhotonView_t8_3 * PhotonView_Get_m8_790 (Object_t * __this /* static, unused */, Component_t6_26 * ___component, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonView PhotonView::Get(UnityEngine.GameObject)
extern "C" PhotonView_t8_3 * PhotonView_Get_m8_791 (Object_t * __this /* static, unused */, GameObject_t6_85 * ___gameObj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonView PhotonView::Find(System.Int32)
extern "C" PhotonView_t8_3 * PhotonView_Find_m8_792 (Object_t * __this /* static, unused */, int32_t ___viewID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhotonView::ToString()
extern "C" String_t* PhotonView_ToString_m8_793 (PhotonView_t8_3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
