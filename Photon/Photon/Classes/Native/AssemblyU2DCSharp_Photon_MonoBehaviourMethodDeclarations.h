﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Photon.MonoBehaviour
struct MonoBehaviour_t8_6;
// PhotonView
struct PhotonView_t8_3;

#include "codegen/il2cpp-codegen.h"

// System.Void Photon.MonoBehaviour::.ctor()
extern "C" void MonoBehaviour__ctor_m8_494 (MonoBehaviour_t8_6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonView Photon.MonoBehaviour::get_photonView()
extern "C" PhotonView_t8_3 * MonoBehaviour_get_photonView_m8_495 (MonoBehaviour_t8_6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonView Photon.MonoBehaviour::get_networkView()
extern "C" PhotonView_t8_3 * MonoBehaviour_get_networkView_m8_496 (MonoBehaviour_t8_6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
