﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Photon.PunBehaviour
struct PunBehaviour_t8_26;
// PhotonPlayer
struct PhotonPlayer_t8_102;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;
// ExitGames.Client.Photon.Hashtable
struct Hashtable_t5_1;
// System.String
struct String_t;
// ExitGames.Client.Photon.OperationResponse
struct OperationResponse_t5_43;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DisconnectCause.h"

// System.Void Photon.PunBehaviour::.ctor()
extern "C" void PunBehaviour__ctor_m8_497 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnConnectedToPhoton()
extern "C" void PunBehaviour_OnConnectedToPhoton_m8_498 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnLeftRoom()
extern "C" void PunBehaviour_OnLeftRoom_m8_499 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnMasterClientSwitched(PhotonPlayer)
extern "C" void PunBehaviour_OnMasterClientSwitched_m8_500 (PunBehaviour_t8_26 * __this, PhotonPlayer_t8_102 * ___newMasterClient, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnPhotonCreateRoomFailed(System.Object[])
extern "C" void PunBehaviour_OnPhotonCreateRoomFailed_m8_501 (PunBehaviour_t8_26 * __this, ObjectU5BU5D_t1_157* ___codeAndMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnPhotonJoinRoomFailed(System.Object[])
extern "C" void PunBehaviour_OnPhotonJoinRoomFailed_m8_502 (PunBehaviour_t8_26 * __this, ObjectU5BU5D_t1_157* ___codeAndMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnCreatedRoom()
extern "C" void PunBehaviour_OnCreatedRoom_m8_503 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnJoinedLobby()
extern "C" void PunBehaviour_OnJoinedLobby_m8_504 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnLeftLobby()
extern "C" void PunBehaviour_OnLeftLobby_m8_505 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnFailedToConnectToPhoton(DisconnectCause)
extern "C" void PunBehaviour_OnFailedToConnectToPhoton_m8_506 (PunBehaviour_t8_26 * __this, int32_t ___cause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnDisconnectedFromPhoton()
extern "C" void PunBehaviour_OnDisconnectedFromPhoton_m8_507 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnConnectionFail(DisconnectCause)
extern "C" void PunBehaviour_OnConnectionFail_m8_508 (PunBehaviour_t8_26 * __this, int32_t ___cause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnPhotonInstantiate(PhotonMessageInfo)
extern "C" void PunBehaviour_OnPhotonInstantiate_m8_509 (PunBehaviour_t8_26 * __this, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnReceivedRoomListUpdate()
extern "C" void PunBehaviour_OnReceivedRoomListUpdate_m8_510 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnJoinedRoom()
extern "C" void PunBehaviour_OnJoinedRoom_m8_511 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnPhotonPlayerConnected(PhotonPlayer)
extern "C" void PunBehaviour_OnPhotonPlayerConnected_m8_512 (PunBehaviour_t8_26 * __this, PhotonPlayer_t8_102 * ___newPlayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnPhotonPlayerDisconnected(PhotonPlayer)
extern "C" void PunBehaviour_OnPhotonPlayerDisconnected_m8_513 (PunBehaviour_t8_26 * __this, PhotonPlayer_t8_102 * ___otherPlayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnPhotonRandomJoinFailed(System.Object[])
extern "C" void PunBehaviour_OnPhotonRandomJoinFailed_m8_514 (PunBehaviour_t8_26 * __this, ObjectU5BU5D_t1_157* ___codeAndMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnConnectedToMaster()
extern "C" void PunBehaviour_OnConnectedToMaster_m8_515 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnPhotonMaxCccuReached()
extern "C" void PunBehaviour_OnPhotonMaxCccuReached_m8_516 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable)
extern "C" void PunBehaviour_OnPhotonCustomRoomPropertiesChanged_m8_517 (PunBehaviour_t8_26 * __this, Hashtable_t5_1 * ___propertiesThatChanged, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnPhotonPlayerPropertiesChanged(System.Object[])
extern "C" void PunBehaviour_OnPhotonPlayerPropertiesChanged_m8_518 (PunBehaviour_t8_26 * __this, ObjectU5BU5D_t1_157* ___playerAndUpdatedProps, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnUpdatedFriendList()
extern "C" void PunBehaviour_OnUpdatedFriendList_m8_519 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnCustomAuthenticationFailed(System.String)
extern "C" void PunBehaviour_OnCustomAuthenticationFailed_m8_520 (PunBehaviour_t8_26 * __this, String_t* ___debugMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnWebRpcResponse(ExitGames.Client.Photon.OperationResponse)
extern "C" void PunBehaviour_OnWebRpcResponse_m8_521 (PunBehaviour_t8_26 * __this, OperationResponse_t5_43 * ___response, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnOwnershipRequest(System.Object[])
extern "C" void PunBehaviour_OnOwnershipRequest_m8_522 (PunBehaviour_t8_26 * __this, ObjectU5BU5D_t1_157* ___viewAndPlayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Photon.PunBehaviour::OnLobbyStatisticsUpdate()
extern "C" void PunBehaviour_OnLobbyStatisticsUpdate_m8_523 (PunBehaviour_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
