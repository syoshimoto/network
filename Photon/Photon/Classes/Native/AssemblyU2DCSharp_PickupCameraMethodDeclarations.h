﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickupCamera
struct PickupCamera_t8_31;
// UnityEngine.Transform
struct Transform_t6_61;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void PickupCamera::.ctor()
extern "C" void PickupCamera__ctor_m8_107 (PickupCamera_t8_31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupCamera::OnEnable()
extern "C" void PickupCamera_OnEnable_m8_108 (PickupCamera_t8_31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupCamera::DebugDrawStuff()
extern "C" void PickupCamera_DebugDrawStuff_m8_109 (PickupCamera_t8_31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PickupCamera::AngleDistance(System.Single,System.Single)
extern "C" float PickupCamera_AngleDistance_m8_110 (PickupCamera_t8_31 * __this, float ___a, float ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupCamera::Apply(UnityEngine.Transform,UnityEngine.Vector3)
extern "C" void PickupCamera_Apply_m8_111 (PickupCamera_t8_31 * __this, Transform_t6_61 * ___dummyTarget, Vector3_t6_49  ___dummyCenter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupCamera::LateUpdate()
extern "C" void PickupCamera_LateUpdate_m8_112 (PickupCamera_t8_31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupCamera::Cut(UnityEngine.Transform,UnityEngine.Vector3)
extern "C" void PickupCamera_Cut_m8_113 (PickupCamera_t8_31 * __this, Transform_t6_61 * ___dummyTarget, Vector3_t6_49  ___dummyCenter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupCamera::SetUpRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void PickupCamera_SetUpRotation_m8_114 (PickupCamera_t8_31 * __this, Vector3_t6_49  ___centerPos, Vector3_t6_49  ___headPos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PickupCamera::GetCenterOffset()
extern "C" Vector3_t6_49  PickupCamera_GetCenterOffset_m8_115 (PickupCamera_t8_31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
