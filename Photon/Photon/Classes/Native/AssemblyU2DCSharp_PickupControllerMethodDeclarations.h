﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickupController
struct PickupController_t8_32;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t6_98;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void PickupController::.ctor()
extern "C" void PickupController__ctor_m8_116 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupController::Awake()
extern "C" void PickupController_Awake_m8_117 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupController::Update()
extern "C" void PickupController_Update_m8_118 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupController::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void PickupController_OnPhotonSerializeView_m8_119 (PickupController_t8_32 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupController::UpdateSmoothedMovementDirection()
extern "C" void PickupController_UpdateSmoothedMovementDirection_m8_120 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupController::ApplyJumping()
extern "C" void PickupController_ApplyJumping_m8_121 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupController::ApplyGravity()
extern "C" void PickupController_ApplyGravity_m8_122 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PickupController::CalculateJumpVerticalSpeed(System.Single)
extern "C" float PickupController_CalculateJumpVerticalSpeed_m8_123 (PickupController_t8_32 * __this, float ___targetJumpHeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupController::DidJump()
extern "C" void PickupController_DidJump_m8_124 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupController::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C" void PickupController_OnControllerColliderHit_m8_125 (PickupController_t8_32 * __this, ControllerColliderHit_t6_98 * ___hit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PickupController::GetSpeed()
extern "C" float PickupController_GetSpeed_m8_126 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PickupController::IsJumping()
extern "C" bool PickupController_IsJumping_m8_127 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PickupController::IsGrounded()
extern "C" bool PickupController_IsGrounded_m8_128 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PickupController::GetDirection()
extern "C" Vector3_t6_49  PickupController_GetDirection_m8_129 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PickupController::IsMovingBackwards()
extern "C" bool PickupController_IsMovingBackwards_m8_130 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PickupController::GetLockCameraTimer()
extern "C" float PickupController_GetLockCameraTimer_m8_131 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PickupController::IsMoving()
extern "C" bool PickupController_IsMoving_m8_132 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PickupController::HasJumpReachedApex()
extern "C" bool PickupController_HasJumpReachedApex_m8_133 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PickupController::IsGroundedWithTimeout()
extern "C" bool PickupController_IsGroundedWithTimeout_m8_134 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupController::Reset()
extern "C" void PickupController_Reset_m8_135 (PickupController_t8_32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
