﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickupDemoGui
struct PickupDemoGui_t8_34;

#include "codegen/il2cpp-codegen.h"

// System.Void PickupDemoGui::.ctor()
extern "C" void PickupDemoGui__ctor_m8_136 (PickupDemoGui_t8_34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupDemoGui::OnGUI()
extern "C" void PickupDemoGui_OnGUI_m8_137 (PickupDemoGui_t8_34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
