﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickupItemSimple
struct PickupItemSimple_t8_164;
// UnityEngine.Collider
struct Collider_t6_100;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"

// System.Void PickupItemSimple::.ctor()
extern "C" void PickupItemSimple__ctor_m8_993 (PickupItemSimple_t8_164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItemSimple::OnTriggerEnter(UnityEngine.Collider)
extern "C" void PickupItemSimple_OnTriggerEnter_m8_994 (PickupItemSimple_t8_164 * __this, Collider_t6_100 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItemSimple::Pickup()
extern "C" void PickupItemSimple_Pickup_m8_995 (PickupItemSimple_t8_164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItemSimple::PunPickupSimple(PhotonMessageInfo)
extern "C" void PickupItemSimple_PunPickupSimple_m8_996 (PickupItemSimple_t8_164 * __this, PhotonMessageInfo_t8_104 * ___msgInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItemSimple::RespawnAfter()
extern "C" void PickupItemSimple_RespawnAfter_m8_997 (PickupItemSimple_t8_164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
