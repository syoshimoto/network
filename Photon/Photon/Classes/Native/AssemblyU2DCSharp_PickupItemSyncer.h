﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"

// PickupItemSyncer
struct  PickupItemSyncer_t8_165  : public MonoBehaviour_t8_6
{
	// System.Boolean PickupItemSyncer::IsWaitingForPickupInit
	bool ___IsWaitingForPickupInit_3;
};
