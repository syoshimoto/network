﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickupItemSyncer
struct PickupItemSyncer_t8_165;
// PhotonPlayer
struct PhotonPlayer_t8_102;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;
// System.Single[]
struct SingleU5BU5D_t1_863;

#include "codegen/il2cpp-codegen.h"

// System.Void PickupItemSyncer::.ctor()
extern "C" void PickupItemSyncer__ctor_m8_998 (PickupItemSyncer_t8_165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItemSyncer::OnPhotonPlayerConnected(PhotonPlayer)
extern "C" void PickupItemSyncer_OnPhotonPlayerConnected_m8_999 (PickupItemSyncer_t8_165 * __this, PhotonPlayer_t8_102 * ___newPlayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItemSyncer::OnJoinedRoom()
extern "C" void PickupItemSyncer_OnJoinedRoom_m8_1000 (PickupItemSyncer_t8_165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItemSyncer::AskForPickupItemSpawnTimes()
extern "C" void PickupItemSyncer_AskForPickupItemSpawnTimes_m8_1001 (PickupItemSyncer_t8_165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItemSyncer::RequestForPickupTimes(PhotonMessageInfo)
extern "C" void PickupItemSyncer_RequestForPickupTimes_m8_1002 (PickupItemSyncer_t8_165 * __this, PhotonMessageInfo_t8_104 * ___msgInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItemSyncer::SendPickedUpItems(PhotonPlayer)
extern "C" void PickupItemSyncer_SendPickedUpItems_m8_1003 (PickupItemSyncer_t8_165 * __this, PhotonPlayer_t8_102 * ___targtePlayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupItemSyncer::PickupItemInit(System.Double,System.Single[])
extern "C" void PickupItemSyncer_PickupItemInit_m8_1004 (PickupItemSyncer_t8_165 * __this, double ___timeBase, SingleU5BU5D_t1_863* ___inactivePickupsAndTimes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
