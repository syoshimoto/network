﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickupTriggerForward
struct PickupTriggerForward_t8_35;
// UnityEngine.Collider
struct Collider_t6_100;

#include "codegen/il2cpp-codegen.h"

// System.Void PickupTriggerForward::.ctor()
extern "C" void PickupTriggerForward__ctor_m8_138 (PickupTriggerForward_t8_35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupTriggerForward::OnTriggerEnter(UnityEngine.Collider)
extern "C" void PickupTriggerForward_OnTriggerEnter_m8_139 (PickupTriggerForward_t8_35 * __this, Collider_t6_100 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
