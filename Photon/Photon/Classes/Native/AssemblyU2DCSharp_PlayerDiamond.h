﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t6_61;
// PhotonView
struct PhotonView_t8_3;
// UnityEngine.Renderer
struct Renderer_t6_25;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// PlayerDiamond
struct  PlayerDiamond_t8_59  : public MonoBehaviour_t6_80
{
	// UnityEngine.Transform PlayerDiamond::HeadTransform
	Transform_t6_61 * ___HeadTransform_2;
	// System.Single PlayerDiamond::HeightOffset
	float ___HeightOffset_3;
	// PhotonView PlayerDiamond::m_PhotonView
	PhotonView_t8_3 * ___m_PhotonView_4;
	// UnityEngine.Renderer PlayerDiamond::m_DiamondRenderer
	Renderer_t6_25 * ___m_DiamondRenderer_5;
	// System.Single PlayerDiamond::m_Rotation
	float ___m_Rotation_6;
	// System.Single PlayerDiamond::m_Height
	float ___m_Height_7;
};
