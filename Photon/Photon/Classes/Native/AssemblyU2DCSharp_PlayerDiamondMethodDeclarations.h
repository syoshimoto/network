﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerDiamond
struct PlayerDiamond_t8_59;
// PhotonView
struct PhotonView_t8_3;
// UnityEngine.Renderer
struct Renderer_t6_25;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerDiamond::.ctor()
extern "C" void PlayerDiamond__ctor_m8_268 (PlayerDiamond_t8_59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhotonView PlayerDiamond::get_PhotonView()
extern "C" PhotonView_t8_3 * PlayerDiamond_get_PhotonView_m8_269 (PlayerDiamond_t8_59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Renderer PlayerDiamond::get_DiamondRenderer()
extern "C" Renderer_t6_25 * PlayerDiamond_get_DiamondRenderer_m8_270 (PlayerDiamond_t8_59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerDiamond::Start()
extern "C" void PlayerDiamond_Start_m8_271 (PlayerDiamond_t8_59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerDiamond::Update()
extern "C" void PlayerDiamond_Update_m8_272 (PlayerDiamond_t8_59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerDiamond::UpdateDiamondPosition()
extern "C" void PlayerDiamond_UpdateDiamondPosition_m8_273 (PlayerDiamond_t8_59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerDiamond::UpdateDiamondRotation()
extern "C" void PlayerDiamond_UpdateDiamondRotation_m8_274 (PlayerDiamond_t8_59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerDiamond::UpdateDiamondVisibility()
extern "C" void PlayerDiamond_UpdateDiamondVisibility_m8_275 (PlayerDiamond_t8_59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
