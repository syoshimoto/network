﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PunEvent
struct PunEvent_t8_105;

#include "codegen/il2cpp-codegen.h"

// System.Void PunEvent::.ctor()
extern "C" void PunEvent__ctor_m8_536 (PunEvent_t8_105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
