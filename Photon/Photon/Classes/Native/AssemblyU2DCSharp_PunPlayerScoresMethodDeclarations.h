﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PunPlayerScores
struct PunPlayerScores_t8_167;

#include "codegen/il2cpp-codegen.h"

// System.Void PunPlayerScores::.ctor()
extern "C" void PunPlayerScores__ctor_m8_1007 (PunPlayerScores_t8_167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
