﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>
struct Dictionary_2_t1_951;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// PunTeams
struct  PunTeams_t8_170  : public MonoBehaviour_t6_80
{
};
struct PunTeams_t8_170_StaticFields{
	// System.Collections.Generic.Dictionary`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>> PunTeams::PlayersPerTeam
	Dictionary_2_t1_951 * ___PlayersPerTeam_3;
};
