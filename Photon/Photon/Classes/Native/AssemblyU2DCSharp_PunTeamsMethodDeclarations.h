﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PunTeams
struct PunTeams_t8_170;
// System.Object[]
struct ObjectU5BU5D_t1_157;

#include "codegen/il2cpp-codegen.h"

// System.Void PunTeams::.ctor()
extern "C" void PunTeams__ctor_m8_1011 (PunTeams_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PunTeams::Start()
extern "C" void PunTeams_Start_m8_1012 (PunTeams_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PunTeams::OnJoinedRoom()
extern "C" void PunTeams_OnJoinedRoom_m8_1013 (PunTeams_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PunTeams::OnPhotonPlayerPropertiesChanged(System.Object[])
extern "C" void PunTeams_OnPhotonPlayerPropertiesChanged_m8_1014 (PunTeams_t8_170 * __this, ObjectU5BU5D_t1_157* ___playerAndUpdatedProps, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PunTeams::UpdateTeams()
extern "C" void PunTeams_UpdateTeams_m8_1015 (PunTeams_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
