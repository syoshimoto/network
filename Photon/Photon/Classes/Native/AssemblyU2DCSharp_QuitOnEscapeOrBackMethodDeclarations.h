﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QuitOnEscapeOrBack
struct QuitOnEscapeOrBack_t8_172;

#include "codegen/il2cpp-codegen.h"

// System.Void QuitOnEscapeOrBack::.ctor()
extern "C" void QuitOnEscapeOrBack__ctor_m8_1018 (QuitOnEscapeOrBack_t8_172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuitOnEscapeOrBack::Update()
extern "C" void QuitOnEscapeOrBack_Update_m8_1019 (QuitOnEscapeOrBack_t8_172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
