﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RPGMovement
struct RPGMovement_t8_38;

#include "codegen/il2cpp-codegen.h"

// System.Void RPGMovement::.ctor()
extern "C" void RPGMovement__ctor_m8_150 (RPGMovement_t8_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGMovement::Start()
extern "C" void RPGMovement_Start_m8_151 (RPGMovement_t8_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGMovement::Update()
extern "C" void RPGMovement_Update_m8_152 (RPGMovement_t8_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGMovement::UpdateAnimation()
extern "C" void RPGMovement_UpdateAnimation_m8_153 (RPGMovement_t8_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGMovement::ResetSpeedValues()
extern "C" void RPGMovement_ResetSpeedValues_m8_154 (RPGMovement_t8_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGMovement::ApplySynchronizedValues()
extern "C" void RPGMovement_ApplySynchronizedValues_m8_155 (RPGMovement_t8_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGMovement::ApplyGravityToCharacterController()
extern "C" void RPGMovement_ApplyGravityToCharacterController_m8_156 (RPGMovement_t8_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGMovement::MoveCharacterController()
extern "C" void RPGMovement_MoveCharacterController_m8_157 (RPGMovement_t8_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGMovement::UpdateForwardMovement()
extern "C" void RPGMovement_UpdateForwardMovement_m8_158 (RPGMovement_t8_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGMovement::UpdateBackwardMovement()
extern "C" void RPGMovement_UpdateBackwardMovement_m8_159 (RPGMovement_t8_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGMovement::UpdateStrafeMovement()
extern "C" void RPGMovement_UpdateStrafeMovement_m8_160 (RPGMovement_t8_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RPGMovement::UpdateRotateMovement()
extern "C" void RPGMovement_UpdateRotateMovement_m8_161 (RPGMovement_t8_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
