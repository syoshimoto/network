﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RaiseEventOptions
struct RaiseEventOptions_t8_93;

#include "codegen/il2cpp-codegen.h"

// System.Void RaiseEventOptions::.ctor()
extern "C" void RaiseEventOptions__ctor_m8_342 (RaiseEventOptions_t8_93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaiseEventOptions::.cctor()
extern "C" void RaiseEventOptions__cctor_m8_343 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
