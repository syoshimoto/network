﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhotonView
struct PhotonView_t8_3;

#include "AssemblyU2DCSharp_Photon_PunBehaviour.h"

// RandomMatchmaker
struct  RandomMatchmaker_t8_57  : public PunBehaviour_t8_26
{
	// PhotonView RandomMatchmaker::myPhotonView
	PhotonView_t8_3 * ___myPhotonView_2;
};
