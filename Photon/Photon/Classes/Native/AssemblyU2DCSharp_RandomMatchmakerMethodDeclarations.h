﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RandomMatchmaker
struct RandomMatchmaker_t8_57;

#include "codegen/il2cpp-codegen.h"

// System.Void RandomMatchmaker::.ctor()
extern "C" void RandomMatchmaker__ctor_m8_258 (RandomMatchmaker_t8_57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RandomMatchmaker::Start()
extern "C" void RandomMatchmaker_Start_m8_259 (RandomMatchmaker_t8_57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RandomMatchmaker::OnJoinedLobby()
extern "C" void RandomMatchmaker_OnJoinedLobby_m8_260 (RandomMatchmaker_t8_57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RandomMatchmaker::OnConnectedToMaster()
extern "C" void RandomMatchmaker_OnConnectedToMaster_m8_261 (RandomMatchmaker_t8_57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RandomMatchmaker::OnPhotonRandomJoinFailed()
extern "C" void RandomMatchmaker_OnPhotonRandomJoinFailed_m8_262 (RandomMatchmaker_t8_57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RandomMatchmaker::OnJoinedRoom()
extern "C" void RandomMatchmaker_OnJoinedRoom_m8_263 (RandomMatchmaker_t8_57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RandomMatchmaker::OnGUI()
extern "C" void RandomMatchmaker_OnGUI_m8_264 (RandomMatchmaker_t8_57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
