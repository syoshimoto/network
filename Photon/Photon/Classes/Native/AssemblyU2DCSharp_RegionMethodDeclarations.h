﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Region
struct Region_t8_110;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CloudRegionCode.h"
#include "AssemblyU2DCSharp_CloudRegionFlag.h"

// System.Void Region::.ctor()
extern "C" void Region__ctor_m8_845 (Region_t8_110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CloudRegionCode Region::Parse(System.String)
extern "C" int32_t Region_Parse_m8_846 (Object_t * __this /* static, unused */, String_t* ___codeAsString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CloudRegionFlag Region::ParseFlag(System.String)
extern "C" int32_t Region_ParseFlag_m8_847 (Object_t * __this /* static, unused */, String_t* ___codeAsString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Region::ToString()
extern "C" String_t* Region_ToString_m8_848 (Region_t8_110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
