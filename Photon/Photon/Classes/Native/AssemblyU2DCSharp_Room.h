﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_202;

#include "AssemblyU2DCSharp_RoomInfo.h"

// Room
struct  Room_t8_100  : public RoomInfo_t8_124
{
	// System.String[] Room::<propertiesListedInLobby>k__BackingField
	StringU5BU5D_t1_202* ___U3CpropertiesListedInLobbyU3Ek__BackingField_11;
};
