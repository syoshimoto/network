﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RoomOptions
struct RoomOptions_t8_78;

#include "codegen/il2cpp-codegen.h"

// System.Void RoomOptions::.ctor()
extern "C" void RoomOptions__ctor_m8_528 (RoomOptions_t8_78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RoomOptions::get_isVisible()
extern "C" bool RoomOptions_get_isVisible_m8_529 (RoomOptions_t8_78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomOptions::set_isVisible(System.Boolean)
extern "C" void RoomOptions_set_isVisible_m8_530 (RoomOptions_t8_78 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RoomOptions::get_isOpen()
extern "C" bool RoomOptions_get_isOpen_m8_531 (RoomOptions_t8_78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomOptions::set_isOpen(System.Boolean)
extern "C" void RoomOptions_set_isOpen_m8_532 (RoomOptions_t8_78 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RoomOptions::get_cleanupCacheOnLeave()
extern "C" bool RoomOptions_get_cleanupCacheOnLeave_m8_533 (RoomOptions_t8_78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomOptions::set_cleanupCacheOnLeave(System.Boolean)
extern "C" void RoomOptions_set_cleanupCacheOnLeave_m8_534 (RoomOptions_t8_78 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RoomOptions::get_suppressRoomEvents()
extern "C" bool RoomOptions_get_suppressRoomEvents_m8_535 (RoomOptions_t8_78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
