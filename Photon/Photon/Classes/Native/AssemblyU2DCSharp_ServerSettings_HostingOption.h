﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ServerSettings_HostingOption.h"

// ServerSettings/HostingOption
struct  HostingOption_t8_125 
{
	// System.Int32 ServerSettings/HostingOption::value__
	int32_t ___value___1;
};
