﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t6_85;
// UnityEngine.TextMesh
struct TextMesh_t6_145;
// UnityEngine.Font
struct Font_t6_148;

#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"

// ShowInfoOfPlayer
struct  ShowInfoOfPlayer_t8_174  : public MonoBehaviour_t8_6
{
	// UnityEngine.GameObject ShowInfoOfPlayer::textGo
	GameObject_t6_85 * ___textGo_2;
	// UnityEngine.TextMesh ShowInfoOfPlayer::tm
	TextMesh_t6_145 * ___tm_3;
	// System.Single ShowInfoOfPlayer::CharacterSize
	float ___CharacterSize_4;
	// UnityEngine.Font ShowInfoOfPlayer::font
	Font_t6_148 * ___font_5;
	// System.Boolean ShowInfoOfPlayer::DisableOnOwnObjects
	bool ___DisableOnOwnObjects_6;
};
