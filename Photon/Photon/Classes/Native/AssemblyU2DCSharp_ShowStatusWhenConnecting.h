﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUISkin
struct GUISkin_t6_161;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// ShowStatusWhenConnecting
struct  ShowStatusWhenConnecting_t8_175  : public MonoBehaviour_t6_80
{
	// UnityEngine.GUISkin ShowStatusWhenConnecting::Skin
	GUISkin_t6_161 * ___Skin_2;
};
