﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// SmoothSyncMovement
struct  SmoothSyncMovement_t8_176  : public MonoBehaviour_t8_6
{
	// System.Single SmoothSyncMovement::SmoothingDelay
	float ___SmoothingDelay_2;
	// UnityEngine.Vector3 SmoothSyncMovement::correctPlayerPos
	Vector3_t6_49  ___correctPlayerPos_3;
	// UnityEngine.Quaternion SmoothSyncMovement::correctPlayerRot
	Quaternion_t6_51  ___correctPlayerRot_4;
};
