﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmoothSyncMovement
struct SmoothSyncMovement_t8_176;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"

// System.Void SmoothSyncMovement::.ctor()
extern "C" void SmoothSyncMovement__ctor_m8_1028 (SmoothSyncMovement_t8_176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmoothSyncMovement::Awake()
extern "C" void SmoothSyncMovement_Awake_m8_1029 (SmoothSyncMovement_t8_176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmoothSyncMovement::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void SmoothSyncMovement_OnPhotonSerializeView_m8_1030 (SmoothSyncMovement_t8_176 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmoothSyncMovement::Update()
extern "C" void SmoothSyncMovement_Update_m8_1031 (SmoothSyncMovement_t8_176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
