﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// SupportLogger
struct  SupportLogger_t8_177  : public MonoBehaviour_t6_80
{
	// System.Boolean SupportLogger::LogTrafficStats
	bool ___LogTrafficStats_2;
};
