﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SupportLogger
struct SupportLogger_t8_177;

#include "codegen/il2cpp-codegen.h"

// System.Void SupportLogger::.ctor()
extern "C" void SupportLogger__ctor_m8_1032 (SupportLogger_t8_177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogger::Start()
extern "C" void SupportLogger_Start_m8_1033 (SupportLogger_t8_177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
