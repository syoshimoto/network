﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SupportLogging
struct SupportLogging_t8_178;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DisconnectCause.h"

// System.Void SupportLogging::.ctor()
extern "C" void SupportLogging__ctor_m8_1034 (SupportLogging_t8_178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogging::Start()
extern "C" void SupportLogging_Start_m8_1035 (SupportLogging_t8_178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogging::OnApplicationPause(System.Boolean)
extern "C" void SupportLogging_OnApplicationPause_m8_1036 (SupportLogging_t8_178 * __this, bool ___pause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogging::OnApplicationQuit()
extern "C" void SupportLogging_OnApplicationQuit_m8_1037 (SupportLogging_t8_178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogging::LogStats()
extern "C" void SupportLogging_LogStats_m8_1038 (SupportLogging_t8_178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogging::LogBasics()
extern "C" void SupportLogging_LogBasics_m8_1039 (SupportLogging_t8_178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogging::OnConnectedToPhoton()
extern "C" void SupportLogging_OnConnectedToPhoton_m8_1040 (SupportLogging_t8_178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogging::OnFailedToConnectToPhoton(DisconnectCause)
extern "C" void SupportLogging_OnFailedToConnectToPhoton_m8_1041 (SupportLogging_t8_178 * __this, int32_t ___cause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogging::OnJoinedLobby()
extern "C" void SupportLogging_OnJoinedLobby_m8_1042 (SupportLogging_t8_178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogging::OnJoinedRoom()
extern "C" void SupportLogging_OnJoinedRoom_m8_1043 (SupportLogging_t8_178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogging::OnCreatedRoom()
extern "C" void SupportLogging_OnCreatedRoom_m8_1044 (SupportLogging_t8_178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogging::OnLeftRoom()
extern "C" void SupportLogging_OnLeftRoom_m8_1045 (SupportLogging_t8_178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SupportLogging::OnDisconnectedFromPhoton()
extern "C" void SupportLogging_OnDisconnectedFromPhoton_m8_1046 (SupportLogging_t8_178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
