﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdPersonCamera
struct ThirdPersonCamera_t8_46;
// UnityEngine.Transform
struct Transform_t6_61;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void ThirdPersonCamera::.ctor()
extern "C" void ThirdPersonCamera__ctor_m8_182 (ThirdPersonCamera_t8_46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonCamera::OnEnable()
extern "C" void ThirdPersonCamera_OnEnable_m8_183 (ThirdPersonCamera_t8_46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonCamera::DebugDrawStuff()
extern "C" void ThirdPersonCamera_DebugDrawStuff_m8_184 (ThirdPersonCamera_t8_46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ThirdPersonCamera::AngleDistance(System.Single,System.Single)
extern "C" float ThirdPersonCamera_AngleDistance_m8_185 (ThirdPersonCamera_t8_46 * __this, float ___a, float ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonCamera::Apply(UnityEngine.Transform,UnityEngine.Vector3)
extern "C" void ThirdPersonCamera_Apply_m8_186 (ThirdPersonCamera_t8_46 * __this, Transform_t6_61 * ___dummyTarget, Vector3_t6_49  ___dummyCenter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonCamera::LateUpdate()
extern "C" void ThirdPersonCamera_LateUpdate_m8_187 (ThirdPersonCamera_t8_46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonCamera::Cut(UnityEngine.Transform,UnityEngine.Vector3)
extern "C" void ThirdPersonCamera_Cut_m8_188 (ThirdPersonCamera_t8_46 * __this, Transform_t6_61 * ___dummyTarget, Vector3_t6_49  ___dummyCenter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonCamera::SetUpRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void ThirdPersonCamera_SetUpRotation_m8_189 (ThirdPersonCamera_t8_46 * __this, Vector3_t6_49  ___centerPos, Vector3_t6_49  ___headPos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ThirdPersonCamera::GetCenterOffset()
extern "C" Vector3_t6_49  ThirdPersonCamera_GetCenterOffset_m8_190 (ThirdPersonCamera_t8_46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
