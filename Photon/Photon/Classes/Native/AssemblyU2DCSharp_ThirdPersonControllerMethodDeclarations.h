﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdPersonController
struct ThirdPersonController_t8_47;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t6_98;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void ThirdPersonController::.ctor()
extern "C" void ThirdPersonController__ctor_m8_191 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonController::Awake()
extern "C" void ThirdPersonController_Awake_m8_192 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonController::UpdateSmoothedMovementDirection()
extern "C" void ThirdPersonController_UpdateSmoothedMovementDirection_m8_193 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonController::ApplyJumping()
extern "C" void ThirdPersonController_ApplyJumping_m8_194 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonController::ApplyGravity()
extern "C" void ThirdPersonController_ApplyGravity_m8_195 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ThirdPersonController::CalculateJumpVerticalSpeed(System.Single)
extern "C" float ThirdPersonController_CalculateJumpVerticalSpeed_m8_196 (ThirdPersonController_t8_47 * __this, float ___targetJumpHeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonController::DidJump()
extern "C" void ThirdPersonController_DidJump_m8_197 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonController::Update()
extern "C" void ThirdPersonController_Update_m8_198 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonController::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C" void ThirdPersonController_OnControllerColliderHit_m8_199 (ThirdPersonController_t8_47 * __this, ControllerColliderHit_t6_98 * ___hit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ThirdPersonController::GetSpeed()
extern "C" float ThirdPersonController_GetSpeed_m8_200 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdPersonController::IsJumping()
extern "C" bool ThirdPersonController_IsJumping_m8_201 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdPersonController::IsGrounded()
extern "C" bool ThirdPersonController_IsGrounded_m8_202 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ThirdPersonController::GetDirection()
extern "C" Vector3_t6_49  ThirdPersonController_GetDirection_m8_203 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdPersonController::IsMovingBackwards()
extern "C" bool ThirdPersonController_IsMovingBackwards_m8_204 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ThirdPersonController::GetLockCameraTimer()
extern "C" float ThirdPersonController_GetLockCameraTimer_m8_205 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdPersonController::IsMoving()
extern "C" bool ThirdPersonController_IsMoving_m8_206 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdPersonController::HasJumpReachedApex()
extern "C" bool ThirdPersonController_HasJumpReachedApex_m8_207 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ThirdPersonController::IsGroundedWithTimeout()
extern "C" bool ThirdPersonController_IsGroundedWithTimeout_m8_208 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonController::Reset()
extern "C" void ThirdPersonController_Reset_m8_209 (ThirdPersonController_t8_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
