﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ThirdPersonCamera
struct ThirdPersonCamera_t8_46;
// ThirdPersonController
struct ThirdPersonController_t8_47;

#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// ThirdPersonNetwork
struct  ThirdPersonNetwork_t8_49  : public MonoBehaviour_t8_6
{
	// ThirdPersonCamera ThirdPersonNetwork::cameraScript
	ThirdPersonCamera_t8_46 * ___cameraScript_2;
	// ThirdPersonController ThirdPersonNetwork::controllerScript
	ThirdPersonController_t8_47 * ___controllerScript_3;
	// UnityEngine.Vector3 ThirdPersonNetwork::correctPlayerPos
	Vector3_t6_49  ___correctPlayerPos_4;
	// UnityEngine.Quaternion ThirdPersonNetwork::correctPlayerRot
	Quaternion_t6_51  ___correctPlayerRot_5;
};
