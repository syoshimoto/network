﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThirdPersonNetwork
struct ThirdPersonNetwork_t8_49;
// PhotonStream
struct PhotonStream_t8_106;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"

// System.Void ThirdPersonNetwork::.ctor()
extern "C" void ThirdPersonNetwork__ctor_m8_210 (ThirdPersonNetwork_t8_49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonNetwork::Awake()
extern "C" void ThirdPersonNetwork_Awake_m8_211 (ThirdPersonNetwork_t8_49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonNetwork::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" void ThirdPersonNetwork_OnPhotonSerializeView_m8_212 (ThirdPersonNetwork_t8_49 * __this, PhotonStream_t8_106 * ___stream, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThirdPersonNetwork::Update()
extern "C" void ThirdPersonNetwork_Update_m8_213 (ThirdPersonNetwork_t8_49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
