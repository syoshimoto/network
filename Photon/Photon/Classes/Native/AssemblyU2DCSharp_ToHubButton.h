﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t6_33;
// ToHubButton
struct ToHubButton_t8_24;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Rect.h"

// ToHubButton
struct  ToHubButton_t8_24  : public MonoBehaviour_t6_80
{
	// UnityEngine.Texture2D ToHubButton::ButtonTexture
	Texture2D_t6_33 * ___ButtonTexture_2;
	// UnityEngine.Rect ToHubButton::ButtonRect
	Rect_t6_52  ___ButtonRect_3;
};
struct ToHubButton_t8_24_StaticFields{
	// ToHubButton ToHubButton::instance
	ToHubButton_t8_24 * ___instance_4;
};
