﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_TypedLobby.h"

// TypedLobbyInfo
struct  TypedLobbyInfo_t8_95  : public TypedLobby_t8_79
{
	// System.Int32 TypedLobbyInfo::PlayerCount
	int32_t ___PlayerCount_3;
	// System.Int32 TypedLobbyInfo::RoomCount
	int32_t ___RoomCount_4;
};
