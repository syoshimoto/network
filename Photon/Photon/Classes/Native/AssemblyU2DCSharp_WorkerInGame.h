﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t6_61;

#include "AssemblyU2DCSharp_Photon_MonoBehaviour.h"

// WorkerInGame
struct  WorkerInGame_t8_50  : public MonoBehaviour_t8_6
{
	// UnityEngine.Transform WorkerInGame::playerPrefab
	Transform_t6_61 * ___playerPrefab_2;
};
