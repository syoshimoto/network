﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WorkerInGame
struct WorkerInGame_t8_50;
// PhotonPlayer
struct PhotonPlayer_t8_102;
// PhotonMessageInfo
struct PhotonMessageInfo_t8_104;

#include "codegen/il2cpp-codegen.h"

// System.Void WorkerInGame::.ctor()
extern "C" void WorkerInGame__ctor_m8_214 (WorkerInGame_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerInGame::Awake()
extern "C" void WorkerInGame_Awake_m8_215 (WorkerInGame_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerInGame::OnGUI()
extern "C" void WorkerInGame_OnGUI_m8_216 (WorkerInGame_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerInGame::OnMasterClientSwitched(PhotonPlayer)
extern "C" void WorkerInGame_OnMasterClientSwitched_m8_217 (WorkerInGame_t8_50 * __this, PhotonPlayer_t8_102 * ___player, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerInGame::OnLeftRoom()
extern "C" void WorkerInGame_OnLeftRoom_m8_218 (WorkerInGame_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerInGame::OnDisconnectedFromPhoton()
extern "C" void WorkerInGame_OnDisconnectedFromPhoton_m8_219 (WorkerInGame_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerInGame::OnPhotonInstantiate(PhotonMessageInfo)
extern "C" void WorkerInGame_OnPhotonInstantiate_m8_220 (WorkerInGame_t8_50 * __this, PhotonMessageInfo_t8_104 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerInGame::OnPhotonPlayerConnected(PhotonPlayer)
extern "C" void WorkerInGame_OnPhotonPlayerConnected_m8_221 (WorkerInGame_t8_50 * __this, PhotonPlayer_t8_102 * ___player, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerInGame::OnPhotonPlayerDisconnected(PhotonPlayer)
extern "C" void WorkerInGame_OnPhotonPlayerDisconnected_m8_222 (WorkerInGame_t8_50 * __this, PhotonPlayer_t8_102 * ___player, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorkerInGame::OnFailedToConnectToPhoton()
extern "C" void WorkerInGame_OnFailedToConnectToPhoton_m8_223 (WorkerInGame_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
