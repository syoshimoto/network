﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ExitGames.Client.Photon.Chat.ChatChannel
struct ChatChannel_t7_1;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.String[]
struct StringU5BU5D_t1_202;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// ExitGames.Client.Photon.Chat.ChatClient
struct ChatClient_t7_2;
// ExitGames.Client.Photon.Chat.IChatClientListener
struct IChatClientListener_t7_5;
// ExitGames.Client.Photon.EventData
struct EventData_t5_44;
// ExitGames.Client.Photon.OperationResponse
struct OperationResponse_t5_43;
// ExitGames.Client.Photon.Chat.AuthenticationValues
struct AuthenticationValues_t7_4;
// ExitGames.Client.Photon.Chat.ChatEventCode
struct ChatEventCode_t7_7;
// ExitGames.Client.Photon.Chat.ChatOperationCode
struct ChatOperationCode_t7_8;
// ExitGames.Client.Photon.Chat.ChatParameterCode
struct ChatParameterCode_t7_9;
// ExitGames.Client.Photon.Chat.ChatPeer
struct ChatPeer_t7_3;
// ExitGames.Client.Photon.IPhotonPeerListener
struct IPhotonPeerListener_t5_17;
// System.Byte[]
struct ByteU5BU5D_t1_71;
// ExitGames.Client.Photon.Chat.ParameterCode
struct ParameterCode_t7_11;
// ExitGames.Client.Photon.Chat.ErrorCode
struct ErrorCode_t7_12;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3EMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_CMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13MethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_0.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_0MethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_6MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_11MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_1.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_6.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_11.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DebugLevel.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EventData.h"
#include "mscorlib_System_Byte.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_OperationResponse.h"
#include "mscorlib_System_Int16.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_StatusCode.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPeerMethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPeer.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_A.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_2.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_AMethodDeclarations.h"
#include "mscorlib_System_EnvironmentMethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerStateValue.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_3MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_3.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EventDataMethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_OperationResponseMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_2MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_3.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_3MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_4.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_4MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_5.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_5MethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_12MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_12.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_7.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_7MethodDeclarations.h"
#include "System_System_UriMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_P.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_PMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_E.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_EMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_1MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_8.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_8MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ExitGames.Client.Photon.Chat.ChatChannel::.ctor(System.String)
extern TypeInfo* List_1_t1_829_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_931_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_5519_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_5617_MethodInfo_var;
extern "C" void ChatChannel__ctor_m7_0 (ChatChannel_t7_1 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_829_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		List_1_t1_931_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1049);
		List_1__ctor_m1_5519_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		List_1__ctor_m1_5617_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483829);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_829 * L_0 = (List_1_t1_829 *)il2cpp_codegen_object_new (List_1_t1_829_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5519(L_0, /*hidden argument*/List_1__ctor_m1_5519_MethodInfo_var);
		__this->___Senders_1 = L_0;
		List_1_t1_931 * L_1 = (List_1_t1_931 *)il2cpp_codegen_object_new (List_1_t1_931_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5617(L_1, /*hidden argument*/List_1__ctor_m1_5617_MethodInfo_var);
		__this->___Messages_2 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___name;
		__this->___Name_0 = L_2;
		return;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatChannel::get_IsPrivate()
extern "C" bool ChatChannel_get_IsPrivate_m7_1 (ChatChannel_t7_1 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CIsPrivateU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatChannel::set_IsPrivate(System.Boolean)
extern "C" void ChatChannel_set_IsPrivate_m7_2 (ChatChannel_t7_1 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CIsPrivateU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Int32 ExitGames.Client.Photon.Chat.ChatChannel::get_MessageCount()
extern "C" int32_t ChatChannel_get_MessageCount_m7_3 (ChatChannel_t7_1 * __this, const MethodInfo* method)
{
	{
		List_1_t1_931 * L_0 = (__this->___Messages_2);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, L_0);
		return L_1;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatChannel::Add(System.String,System.Object)
extern "C" void ChatChannel_Add_m7_4 (ChatChannel_t7_1 * __this, String_t* ___sender, Object_t * ___message, const MethodInfo* method)
{
	{
		List_1_t1_829 * L_0 = (__this->___Senders_1);
		String_t* L_1 = ___sender;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_0, L_1);
		List_1_t1_931 * L_2 = (__this->___Messages_2);
		Object_t * L_3 = ___message;
		NullCheck(L_2);
		VirtActionInvoker1< Object_t * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, L_2, L_3);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatChannel::Add(System.String[],System.Object[])
extern const MethodInfo* List_1_AddRange_m1_5618_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m1_5619_MethodInfo_var;
extern "C" void ChatChannel_Add_m7_5 (ChatChannel_t7_1 * __this, StringU5BU5D_t1_202* ___senders, ObjectU5BU5D_t1_157* ___messages, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_AddRange_m1_5618_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483830);
		List_1_AddRange_m1_5619_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483831);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_829 * L_0 = (__this->___Senders_1);
		StringU5BU5D_t1_202* L_1 = ___senders;
		NullCheck(L_0);
		List_1_AddRange_m1_5618(L_0, (Object_t*)(Object_t*)L_1, /*hidden argument*/List_1_AddRange_m1_5618_MethodInfo_var);
		List_1_t1_931 * L_2 = (__this->___Messages_2);
		ObjectU5BU5D_t1_157* L_3 = ___messages;
		NullCheck(L_2);
		List_1_AddRange_m1_5619(L_2, (Object_t*)(Object_t*)L_3, /*hidden argument*/List_1_AddRange_m1_5619_MethodInfo_var);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatChannel::ClearMessages()
extern "C" void ChatChannel_ClearMessages_m7_6 (ChatChannel_t7_1 * __this, const MethodInfo* method)
{
	{
		List_1_t1_829 * L_0 = (__this->___Senders_1);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Collections.Generic.List`1<System.String>::Clear() */, L_0);
		List_1_t1_931 * L_1 = (__this->___Messages_2);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Collections.Generic.List`1<System.Object>::Clear() */, L_1);
		return;
	}
}
// System.String ExitGames.Client.Photon.Chat.ChatChannel::ToStringMessages()
extern TypeInfo* StringBuilder_t1_145_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2937;
extern "C" String_t* ChatChannel_ToStringMessages_m7_7 (ChatChannel_t7_1 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2937 = il2cpp_codegen_string_literal_from_index(2937);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_145 * V_0 = {0};
	int32_t V_1 = 0;
	{
		StringBuilder_t1_145 * L_0 = (StringBuilder_t1_145 *)il2cpp_codegen_object_new (StringBuilder_t1_145_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_4329(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003a;
	}

IL_000d:
	{
		StringBuilder_t1_145 * L_1 = V_0;
		List_1_t1_829 * L_2 = (__this->___Senders_1);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		String_t* L_4 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_2, L_3);
		List_1_t1_931 * L_5 = (__this->___Messages_2);
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Object_t * L_7 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(28 /* !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32) */, L_5, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m1_411(NULL /*static, unused*/, _stringLiteral2937, L_4, L_7, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_AppendLine_m1_4354(L_1, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_10 = V_1;
		List_1_t1_931 * L_11 = (__this->___Messages_2);
		NullCheck(L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_000d;
		}
	}
	{
		StringBuilder_t1_145 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = StringBuilder_ToString_m1_4341(L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::.ctor(ExitGames.Client.Photon.Chat.IChatClientListener,ExitGames.Client.Photon.ConnectionProtocol)
extern TypeInfo* ChatPeer_t7_3_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_932_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5620_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2938;
extern "C" void ChatClient__ctor_m7_8 (ChatClient_t7_2 * __this, Object_t * ___listener, uint8_t ___protocol, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChatPeer_t7_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1051);
		Dictionary_2_t1_932_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1052);
		Dictionary_2__ctor_m1_5620_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483832);
		_stringLiteral2938 = il2cpp_codegen_string_literal_from_index(2938);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___chatRegion_2 = _stringLiteral2938;
		__this->___msDeltaForServiceCalls_8 = ((int32_t)50);
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___listener;
		__this->___listener_5 = L_0;
		ChatClient_set_State_m7_20(__this, 0, /*hidden argument*/NULL);
		uint8_t L_1 = ___protocol;
		ChatPeer_t7_3 * L_2 = (ChatPeer_t7_3 *)il2cpp_codegen_object_new (ChatPeer_t7_3_il2cpp_TypeInfo_var);
		ChatPeer__ctor_m7_66(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___chatPeer_6 = L_2;
		Dictionary_2_t1_932 * L_3 = (Dictionary_2_t1_932 *)il2cpp_codegen_object_new (Dictionary_2_t1_932_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5620(L_3, /*hidden argument*/Dictionary_2__ctor_m1_5620_MethodInfo_var);
		__this->___PublicChannels_3 = L_3;
		Dictionary_2_t1_932 * L_4 = (Dictionary_2_t1_932 *)il2cpp_codegen_object_new (Dictionary_2_t1_932_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5620(L_4, /*hidden argument*/Dictionary_2__ctor_m1_5620_MethodInfo_var);
		__this->___PrivateChannels_4 = L_4;
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::ExitGames.Client.Photon.IPhotonPeerListener.DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern "C" void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_DebugReturn_m7_9 (ChatClient_t7_2 * __this, uint8_t ___level, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___listener_5);
		uint8_t L_1 = ___level;
		String_t* L_2 = ___message;
		NullCheck(L_0);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::ExitGames.Client.Photon.IPhotonPeerListener.OnEvent(ExitGames.Client.Photon.EventData)
extern "C" void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnEvent_m7_10 (ChatClient_t7_2 * __this, EventData_t5_44 * ___eventData, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	{
		EventData_t5_44 * L_0 = ___eventData;
		NullCheck(L_0);
		uint8_t L_1 = (L_0->___Code_0);
		V_0 = L_1;
		uint8_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_002e;
		}
		if (L_2 == 1)
		{
			goto IL_006a;
		}
		if (L_2 == 2)
		{
			goto IL_003a;
		}
		if (L_2 == 3)
		{
			goto IL_006a;
		}
		if (L_2 == 4)
		{
			goto IL_0046;
		}
		if (L_2 == 5)
		{
			goto IL_0052;
		}
		if (L_2 == 6)
		{
			goto IL_005e;
		}
	}
	{
		goto IL_006a;
	}

IL_002e:
	{
		EventData_t5_44 * L_3 = ___eventData;
		ChatClient_HandleChatMessagesEvent_m7_56(__this, L_3, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_003a:
	{
		EventData_t5_44 * L_4 = ___eventData;
		ChatClient_HandlePrivateMessageEvent_m7_55(__this, L_4, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_0046:
	{
		EventData_t5_44 * L_5 = ___eventData;
		ChatClient_HandleStatusUpdate_m7_60(__this, L_5, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_0052:
	{
		EventData_t5_44 * L_6 = ___eventData;
		ChatClient_HandleSubscribeEvent_m7_57(__this, L_6, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_005e:
	{
		EventData_t5_44 * L_7 = ___eventData;
		ChatClient_HandleUnsubscribeEvent_m7_58(__this, L_7, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_006a:
	{
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::ExitGames.Client.Photon.IPhotonPeerListener.OnOperationResponse(ExitGames.Client.Photon.OperationResponse)
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern TypeInfo* Int16_t1_13_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2939;
extern Il2CppCodeGenString* _stringLiteral2940;
extern "C" void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnOperationResponse_m7_11 (ChatClient_t7_2 * __this, OperationResponse_t5_43 * ___operationResponse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		Int16_t1_13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		_stringLiteral2939 = il2cpp_codegen_string_literal_from_index(2939);
		_stringLiteral2940 = il2cpp_codegen_string_literal_from_index(2940);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0x0;
	{
		OperationResponse_t5_43 * L_0 = ___operationResponse;
		NullCheck(L_0);
		uint8_t L_1 = (L_0->___OperationCode_0);
		V_0 = L_1;
		uint8_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_0039;
		}
		if (L_2 == 1)
		{
			goto IL_0039;
		}
		if (L_2 == 2)
		{
			goto IL_0039;
		}
		if (L_2 == 3)
		{
			goto IL_0039;
		}
	}
	{
		uint8_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)230))))
		{
			goto IL_002d;
		}
	}
	{
		goto IL_0039;
	}

IL_002d:
	{
		OperationResponse_t5_43 * L_4 = ___operationResponse;
		ChatClient_HandleAuthResponse_m7_59(__this, L_4, /*hidden argument*/NULL);
		goto IL_00ae;
	}

IL_0039:
	{
		OperationResponse_t5_43 * L_5 = ___operationResponse;
		NullCheck(L_5);
		int16_t L_6 = (L_5->___ReturnCode_1);
		if (!L_6)
		{
			goto IL_00a9;
		}
	}
	{
		OperationResponse_t5_43 * L_7 = ___operationResponse;
		NullCheck(L_7);
		int16_t L_8 = (L_7->___ReturnCode_1);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0077;
		}
	}
	{
		Object_t * L_9 = (__this->___listener_5);
		OperationResponse_t5_43 * L_10 = ___operationResponse;
		NullCheck(L_10);
		uint8_t L_11 = (L_10->___OperationCode_0);
		uint8_t L_12 = L_11;
		Object_t * L_13 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m1_410(NULL /*static, unused*/, _stringLiteral2939, L_13, /*hidden argument*/NULL);
		NullCheck(L_9);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_9, 1, L_14);
		goto IL_00a9;
	}

IL_0077:
	{
		Object_t * L_15 = (__this->___listener_5);
		OperationResponse_t5_43 * L_16 = ___operationResponse;
		NullCheck(L_16);
		uint8_t L_17 = (L_16->___OperationCode_0);
		uint8_t L_18 = L_17;
		Object_t * L_19 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_18);
		OperationResponse_t5_43 * L_20 = ___operationResponse;
		NullCheck(L_20);
		int16_t L_21 = (L_20->___ReturnCode_1);
		int16_t L_22 = L_21;
		Object_t * L_23 = Box(Int16_t1_13_il2cpp_TypeInfo_var, &L_22);
		OperationResponse_t5_43 * L_24 = ___operationResponse;
		NullCheck(L_24);
		String_t* L_25 = (L_24->___DebugMessage_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Format_m1_412(NULL /*static, unused*/, _stringLiteral2940, L_19, L_23, L_25, /*hidden argument*/NULL);
		NullCheck(L_15);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_15, 1, L_26);
	}

IL_00a9:
	{
		goto IL_00ae;
	}

IL_00ae:
	{
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::ExitGames.Client.Photon.IPhotonPeerListener.OnStatusChanged(ExitGames.Client.Photon.StatusCode)
extern TypeInfo* ChatState_t7_13_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IPhotonPeerListener_t5_17_il2cpp_TypeInfo_var;
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2941;
extern Il2CppCodeGenString* _stringLiteral2942;
extern Il2CppCodeGenString* _stringLiteral2943;
extern "C" void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnStatusChanged_m7_12 (ChatClient_t7_2 * __this, int32_t ___statusCode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChatState_t7_13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1054);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		IPhotonPeerListener_t5_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(854);
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		_stringLiteral2941 = il2cpp_codegen_string_literal_from_index(2941);
		_stringLiteral2942 = il2cpp_codegen_string_literal_from_index(2942);
		_stringLiteral2943 = il2cpp_codegen_string_literal_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___statusCode;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)1024))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)1025))))
		{
			goto IL_017c;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)1048))))
		{
			goto IL_0104;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)1049))))
		{
			goto IL_0164;
		}
	}
	{
		goto IL_01b8;
	}

IL_0033:
	{
		ChatPeer_t7_3 * L_5 = (__this->___chatPeer_6);
		NullCheck(L_5);
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean ExitGames.Client.Photon.Chat.ChatPeer::get_IsProtocolSecure() */, L_5);
		if (L_6)
		{
			goto IL_005e;
		}
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral2941, /*hidden argument*/NULL);
		ChatPeer_t7_3 * L_7 = (__this->___chatPeer_6);
		NullCheck(L_7);
		PhotonPeer_EstablishEncryption_m5_240(L_7, /*hidden argument*/NULL);
		goto IL_00c3;
	}

IL_005e:
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral2942, /*hidden argument*/NULL);
		bool L_8 = (__this->___didAuthenticate_7);
		if (L_8)
		{
			goto IL_00c3;
		}
	}
	{
		ChatPeer_t7_3 * L_9 = (__this->___chatPeer_6);
		String_t* L_10 = ChatClient_get_AppId_m7_27(__this, /*hidden argument*/NULL);
		String_t* L_11 = ChatClient_get_AppVersion_m7_25(__this, /*hidden argument*/NULL);
		String_t* L_12 = (__this->___chatRegion_2);
		AuthenticationValues_t7_4 * L_13 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_14 = ChatPeer_AuthenticateOnNameServer_m7_72(L_9, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		__this->___didAuthenticate_7 = L_14;
		bool L_15 = (__this->___didAuthenticate_7);
		if (L_15)
		{
			goto IL_00c3;
		}
	}
	{
		int32_t L_16 = ChatClient_get_State_m7_19(__this, /*hidden argument*/NULL);
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(ChatState_t7_13_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral2943, L_18, /*hidden argument*/NULL);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.IPhotonPeerListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IPhotonPeerListener_t5_17_il2cpp_TypeInfo_var, __this, 1, L_19);
	}

IL_00c3:
	{
		int32_t L_20 = ChatClient_get_State_m7_19(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)1))))
		{
			goto IL_00ec;
		}
	}
	{
		ChatClient_set_State_m7_20(__this, 2, /*hidden argument*/NULL);
		Object_t * L_21 = (__this->___listener_5);
		int32_t L_22 = ChatClient_get_State_m7_19(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		InterfaceActionInvoker1< int32_t >::Invoke(3 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::OnChatStateChange(ExitGames.Client.Photon.Chat.ChatState) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_21, L_22);
		goto IL_00ff;
	}

IL_00ec:
	{
		int32_t L_23 = ChatClient_get_State_m7_19(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)6))))
		{
			goto IL_00ff;
		}
	}
	{
		ChatClient_AuthenticateOnFrontEnd_m7_62(__this, /*hidden argument*/NULL);
	}

IL_00ff:
	{
		goto IL_01b8;
	}

IL_0104:
	{
		bool L_24 = (__this->___didAuthenticate_7);
		if (L_24)
		{
			goto IL_015f;
		}
	}
	{
		ChatPeer_t7_3 * L_25 = (__this->___chatPeer_6);
		String_t* L_26 = ChatClient_get_AppId_m7_27(__this, /*hidden argument*/NULL);
		String_t* L_27 = ChatClient_get_AppVersion_m7_25(__this, /*hidden argument*/NULL);
		String_t* L_28 = (__this->___chatRegion_2);
		AuthenticationValues_t7_4 * L_29 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		bool L_30 = ChatPeer_AuthenticateOnNameServer_m7_72(L_25, L_26, L_27, L_28, L_29, /*hidden argument*/NULL);
		__this->___didAuthenticate_7 = L_30;
		bool L_31 = (__this->___didAuthenticate_7);
		if (L_31)
		{
			goto IL_015f;
		}
	}
	{
		int32_t L_32 = ChatClient_get_State_m7_19(__this, /*hidden argument*/NULL);
		int32_t L_33 = L_32;
		Object_t * L_34 = Box(ChatState_t7_13_il2cpp_TypeInfo_var, &L_33);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral2943, L_34, /*hidden argument*/NULL);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.IPhotonPeerListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IPhotonPeerListener_t5_17_il2cpp_TypeInfo_var, __this, 1, L_35);
	}

IL_015f:
	{
		goto IL_01b8;
	}

IL_0164:
	{
		ChatClient_set_State_m7_20(__this, ((int32_t)10), /*hidden argument*/NULL);
		ChatPeer_t7_3 * L_36 = (__this->___chatPeer_6);
		NullCheck(L_36);
		VirtActionInvoker0::Invoke(7 /* System.Void ExitGames.Client.Photon.PhotonPeer::Disconnect() */, L_36);
		goto IL_01b8;
	}

IL_017c:
	{
		int32_t L_37 = ChatClient_get_State_m7_19(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_37) == ((uint32_t)4))))
		{
			goto IL_0193;
		}
	}
	{
		ChatClient_ConnectToFrontEnd_m7_61(__this, /*hidden argument*/NULL);
		goto IL_01b3;
	}

IL_0193:
	{
		ChatClient_set_State_m7_20(__this, ((int32_t)11), /*hidden argument*/NULL);
		Object_t * L_38 = (__this->___listener_5);
		NullCheck(L_38);
		InterfaceActionInvoker1< int32_t >::Invoke(3 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::OnChatStateChange(ExitGames.Client.Photon.Chat.ChatState) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_38, ((int32_t)11));
		Object_t * L_39 = (__this->___listener_5);
		NullCheck(L_39);
		InterfaceActionInvoker0::Invoke(1 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::OnDisconnected() */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_39);
	}

IL_01b3:
	{
		goto IL_01b8;
	}

IL_01b8:
	{
		return;
	}
}
// System.String ExitGames.Client.Photon.Chat.ChatClient::get_NameServerAddress()
extern "C" String_t* ChatClient_get_NameServerAddress_m7_13 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CNameServerAddressU3Ek__BackingField_10);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_NameServerAddress(System.String)
extern "C" void ChatClient_set_NameServerAddress_m7_14 (ChatClient_t7_2 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CNameServerAddressU3Ek__BackingField_10 = L_0;
		return;
	}
}
// System.String ExitGames.Client.Photon.Chat.ChatClient::get_FrontendAddress()
extern "C" String_t* ChatClient_get_FrontendAddress_m7_15 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CFrontendAddressU3Ek__BackingField_11);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_FrontendAddress(System.String)
extern "C" void ChatClient_set_FrontendAddress_m7_16 (ChatClient_t7_2 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CFrontendAddressU3Ek__BackingField_11 = L_0;
		return;
	}
}
// System.String ExitGames.Client.Photon.Chat.ChatClient::get_ChatRegion()
extern "C" String_t* ChatClient_get_ChatRegion_m7_17 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___chatRegion_2);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_ChatRegion(System.String)
extern "C" void ChatClient_set_ChatRegion_m7_18 (ChatClient_t7_2 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___chatRegion_2 = L_0;
		return;
	}
}
// ExitGames.Client.Photon.Chat.ChatState ExitGames.Client.Photon.Chat.ChatClient::get_State()
extern "C" int32_t ChatClient_get_State_m7_19 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CStateU3Ek__BackingField_12);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_State(ExitGames.Client.Photon.Chat.ChatState)
extern "C" void ChatClient_set_State_m7_20 (ChatClient_t7_2 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CStateU3Ek__BackingField_12 = L_0;
		return;
	}
}
// ExitGames.Client.Photon.Chat.ChatDisconnectCause ExitGames.Client.Photon.Chat.ChatClient::get_DisconnectedCause()
extern "C" int32_t ChatClient_get_DisconnectedCause_m7_21 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CDisconnectedCauseU3Ek__BackingField_13);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_DisconnectedCause(ExitGames.Client.Photon.Chat.ChatDisconnectCause)
extern "C" void ChatClient_set_DisconnectedCause_m7_22 (ChatClient_t7_2 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CDisconnectedCauseU3Ek__BackingField_13 = L_0;
		return;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::get_CanChat()
extern "C" bool ChatClient_get_CanChat_m7_23 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ChatClient_get_State_m7_19(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)7))))
		{
			goto IL_0014;
		}
	}
	{
		bool L_1 = ChatClient_get_HasPeer_m7_24(__this, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 0;
	}

IL_0015:
	{
		return G_B3_0;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::get_HasPeer()
extern "C" bool ChatClient_get_HasPeer_m7_24 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		ChatPeer_t7_3 * L_0 = (__this->___chatPeer_6);
		return ((((int32_t)((((Object_t*)(ChatPeer_t7_3 *)L_0) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.String ExitGames.Client.Photon.Chat.ChatClient::get_AppVersion()
extern "C" String_t* ChatClient_get_AppVersion_m7_25 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CAppVersionU3Ek__BackingField_14);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_AppVersion(System.String)
extern "C" void ChatClient_set_AppVersion_m7_26 (ChatClient_t7_2 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CAppVersionU3Ek__BackingField_14 = L_0;
		return;
	}
}
// System.String ExitGames.Client.Photon.Chat.ChatClient::get_AppId()
extern "C" String_t* ChatClient_get_AppId_m7_27 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CAppIdU3Ek__BackingField_15);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_AppId(System.String)
extern "C" void ChatClient_set_AppId_m7_28 (ChatClient_t7_2 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CAppIdU3Ek__BackingField_15 = L_0;
		return;
	}
}
// ExitGames.Client.Photon.Chat.AuthenticationValues ExitGames.Client.Photon.Chat.ChatClient::get_AuthValues()
extern "C" AuthenticationValues_t7_4 * ChatClient_get_AuthValues_m7_29 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		AuthenticationValues_t7_4 * L_0 = (__this->___U3CAuthValuesU3Ek__BackingField_16);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_AuthValues(ExitGames.Client.Photon.Chat.AuthenticationValues)
extern "C" void ChatClient_set_AuthValues_m7_30 (ChatClient_t7_2 * __this, AuthenticationValues_t7_4 * ___value, const MethodInfo* method)
{
	{
		AuthenticationValues_t7_4 * L_0 = ___value;
		__this->___U3CAuthValuesU3Ek__BackingField_16 = L_0;
		return;
	}
}
// System.String ExitGames.Client.Photon.Chat.ChatClient::get_UserId()
extern "C" String_t* ChatClient_get_UserId_m7_31 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	String_t* G_B3_0 = {0};
	{
		AuthenticationValues_t7_4 * L_0 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		AuthenticationValues_t7_4 * L_1 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = AuthenticationValues_get_UserId_m7_83(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = ((String_t*)(NULL));
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_UserId(System.String)
extern TypeInfo* AuthenticationValues_t7_4_il2cpp_TypeInfo_var;
extern "C" void ChatClient_set_UserId_m7_32 (ChatClient_t7_2 * __this, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AuthenticationValues_t7_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1055);
		s_Il2CppMethodIntialized = true;
	}
	{
		AuthenticationValues_t7_4 * L_0 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		AuthenticationValues_t7_4 * L_1 = (AuthenticationValues_t7_4 *)il2cpp_codegen_object_new (AuthenticationValues_t7_4_il2cpp_TypeInfo_var);
		AuthenticationValues__ctor_m7_73(L_1, /*hidden argument*/NULL);
		ChatClient_set_AuthValues_m7_30(__this, L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		AuthenticationValues_t7_4 * L_2 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		String_t* L_3 = ___value;
		NullCheck(L_2);
		AuthenticationValues_set_UserId_m7_84(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::Connect(System.String,System.String,ExitGames.Client.Photon.Chat.AuthenticationValues)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2944;
extern Il2CppCodeGenString* _stringLiteral2945;
extern "C" bool ChatClient_Connect_m7_33 (ChatClient_t7_2 * __this, String_t* ___appId, String_t* ___appVersion, AuthenticationValues_t7_4 * ___authValues, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		_stringLiteral2944 = il2cpp_codegen_string_literal_from_index(2944);
		_stringLiteral2945 = il2cpp_codegen_string_literal_from_index(2945);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		ChatPeer_t7_3 * L_0 = (__this->___chatPeer_6);
		NullCheck(L_0);
		PhotonPeer_set_TimePingInterval_m5_220(L_0, ((int32_t)3000), /*hidden argument*/NULL);
		ChatClient_set_DisconnectedCause_m7_22(__this, 0, /*hidden argument*/NULL);
		AuthenticationValues_t7_4 * L_1 = ___authValues;
		if (!L_1)
		{
			goto IL_0066;
		}
	}
	{
		AuthenticationValues_t7_4 * L_2 = ___authValues;
		ChatClient_set_AuthValues_m7_30(__this, L_2, /*hidden argument*/NULL);
		AuthenticationValues_t7_4 * L_3 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = AuthenticationValues_get_UserId_m7_83(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004e;
		}
	}
	{
		AuthenticationValues_t7_4 * L_5 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = AuthenticationValues_get_UserId_m7_83(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_8 = String_op_Equality_m1_454(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0061;
		}
	}

IL_004e:
	{
		Object_t * L_9 = (__this->___listener_5);
		NullCheck(L_9);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_9, 1, _stringLiteral2944);
		return 0;
	}

IL_0061:
	{
		goto IL_0079;
	}

IL_0066:
	{
		Object_t * L_10 = (__this->___listener_5);
		NullCheck(L_10);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_10, 1, _stringLiteral2945);
		return 0;
	}

IL_0079:
	{
		String_t* L_11 = ___appId;
		ChatClient_set_AppId_m7_28(__this, L_11, /*hidden argument*/NULL);
		String_t* L_12 = ___appVersion;
		ChatClient_set_AppVersion_m7_26(__this, L_12, /*hidden argument*/NULL);
		__this->___didAuthenticate_7 = 0;
		__this->___msDeltaForServiceCalls_8 = ((int32_t)100);
		ChatPeer_t7_3 * L_13 = (__this->___chatPeer_6);
		NullCheck(L_13);
		PhotonPeer_set_QuickResendAttempts_m5_210(L_13, 2, /*hidden argument*/NULL);
		ChatPeer_t7_3 * L_14 = (__this->___chatPeer_6);
		NullCheck(L_14);
		PhotonPeer_set_SentCountAllowance_m5_219(L_14, 7, /*hidden argument*/NULL);
		Dictionary_2_t1_932 * L_15 = (__this->___PublicChannels_3);
		NullCheck(L_15);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::Clear() */, L_15);
		Dictionary_2_t1_932 * L_16 = (__this->___PrivateChannels_4);
		NullCheck(L_16);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::Clear() */, L_16);
		ChatPeer_t7_3 * L_17 = (__this->___chatPeer_6);
		NullCheck(L_17);
		String_t* L_18 = ChatPeer_get_NameServerAddress_m7_68(L_17, /*hidden argument*/NULL);
		ChatClient_set_NameServerAddress_m7_14(__this, L_18, /*hidden argument*/NULL);
		ChatPeer_t7_3 * L_19 = (__this->___chatPeer_6);
		NullCheck(L_19);
		bool L_20 = ChatPeer_Connect_m7_71(L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		bool L_21 = V_0;
		if (!L_21)
		{
			goto IL_00ee;
		}
	}
	{
		ChatClient_set_State_m7_20(__this, 1, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		bool L_22 = V_0;
		return L_22;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::Service()
extern "C" void ChatClient_Service_m7_34 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ChatClient_get_HasPeer_m7_24(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_1 = Environment_get_TickCount_m1_5041(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___msTimestampOfLastServiceCall_9);
		int32_t L_3 = (__this->___msDeltaForServiceCalls_8);
		if ((((int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2))) > ((int32_t)L_3)))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_4 = (__this->___msTimestampOfLastServiceCall_9);
		if (L_4)
		{
			goto IL_0043;
		}
	}

IL_002d:
	{
		int32_t L_5 = Environment_get_TickCount_m1_5041(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___msTimestampOfLastServiceCall_9 = L_5;
		ChatPeer_t7_3 * L_6 = (__this->___chatPeer_6);
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(10 /* System.Void ExitGames.Client.Photon.PhotonPeer::Service() */, L_6);
	}

IL_0043:
	{
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::Disconnect()
extern "C" void ChatClient_Disconnect_m7_35 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ChatClient_get_HasPeer_m7_24(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		ChatPeer_t7_3 * L_1 = (__this->___chatPeer_6);
		NullCheck(L_1);
		uint8_t L_2 = PhotonPeer_get_PeerState_m5_211(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		ChatPeer_t7_3 * L_3 = (__this->___chatPeer_6);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(7 /* System.Void ExitGames.Client.Photon.PhotonPeer::Disconnect() */, L_3);
	}

IL_0026:
	{
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::StopThread()
extern "C" void ChatClient_StopThread_m7_36 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ChatClient_get_HasPeer_m7_24(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ChatPeer_t7_3 * L_1 = (__this->___chatPeer_6);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(8 /* System.Void ExitGames.Client.Photon.PhotonPeer::StopThread() */, L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::Subscribe(System.String[])
extern "C" bool ChatClient_Subscribe_m7_37 (ChatClient_t7_2 * __this, StringU5BU5D_t1_202* ___channels, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_202* L_0 = ___channels;
		bool L_1 = ChatClient_Subscribe_m7_38(__this, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::Subscribe(System.String[],System.Int32)
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2946;
extern Il2CppCodeGenString* _stringLiteral2947;
extern "C" bool ChatClient_Subscribe_m7_38 (ChatClient_t7_2 * __this, StringU5BU5D_t1_202* ___channels, int32_t ___messagesFromHistory, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		_stringLiteral2946 = il2cpp_codegen_string_literal_from_index(2946);
		_stringLiteral2947 = il2cpp_codegen_string_literal_from_index(2947);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ChatClient_get_CanChat_m7_23(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Object_t * L_1 = (__this->___listener_5);
		NullCheck(L_1);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_1, 1, _stringLiteral2946);
		return 0;
	}

IL_001e:
	{
		StringU5BU5D_t1_202* L_2 = ___channels;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		StringU5BU5D_t1_202* L_3 = ___channels;
		NullCheck(L_3);
		if ((((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))))
		{
			goto IL_003f;
		}
	}

IL_002c:
	{
		Object_t * L_4 = (__this->___listener_5);
		NullCheck(L_4);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_4, 2, _stringLiteral2947);
		return 0;
	}

IL_003f:
	{
		StringU5BU5D_t1_202* L_5 = ___channels;
		int32_t L_6 = ___messagesFromHistory;
		bool L_7 = ChatClient_SendChannelOperation_m7_54(__this, L_5, 0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::Unsubscribe(System.String[])
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2948;
extern Il2CppCodeGenString* _stringLiteral2949;
extern "C" bool ChatClient_Unsubscribe_m7_39 (ChatClient_t7_2 * __this, StringU5BU5D_t1_202* ___channels, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		_stringLiteral2948 = il2cpp_codegen_string_literal_from_index(2948);
		_stringLiteral2949 = il2cpp_codegen_string_literal_from_index(2949);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ChatClient_get_CanChat_m7_23(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Object_t * L_1 = (__this->___listener_5);
		NullCheck(L_1);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_1, 1, _stringLiteral2948);
		return 0;
	}

IL_001e:
	{
		StringU5BU5D_t1_202* L_2 = ___channels;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		StringU5BU5D_t1_202* L_3 = ___channels;
		NullCheck(L_3);
		if ((((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))))
		{
			goto IL_003f;
		}
	}

IL_002c:
	{
		Object_t * L_4 = (__this->___listener_5);
		NullCheck(L_4);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_4, 2, _stringLiteral2949);
		return 0;
	}

IL_003f:
	{
		StringU5BU5D_t1_202* L_5 = ___channels;
		bool L_6 = ChatClient_SendChannelOperation_m7_54(__this, L_5, 1, 0, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::PublishMessage(System.String,System.Object)
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_888_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5588_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2950;
extern Il2CppCodeGenString* _stringLiteral2951;
extern "C" bool ChatClient_PublishMessage_m7_40 (ChatClient_t7_2 * __this, String_t* ___channelName, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Dictionary_2_t1_888_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(857);
		Dictionary_2__ctor_m1_5588_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483791);
		_stringLiteral2950 = il2cpp_codegen_string_literal_from_index(2950);
		_stringLiteral2951 = il2cpp_codegen_string_literal_from_index(2951);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_888 * V_0 = {0};
	Dictionary_2_t1_888 * V_1 = {0};
	{
		bool L_0 = ChatClient_get_CanChat_m7_23(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Object_t * L_1 = (__this->___listener_5);
		NullCheck(L_1);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_1, 1, _stringLiteral2950);
		return 0;
	}

IL_001e:
	{
		String_t* L_2 = ___channelName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002f;
		}
	}
	{
		Object_t * L_4 = ___message;
		if (L_4)
		{
			goto IL_0042;
		}
	}

IL_002f:
	{
		Object_t * L_5 = (__this->___listener_5);
		NullCheck(L_5);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_5, 2, _stringLiteral2951);
		return 0;
	}

IL_0042:
	{
		Dictionary_2_t1_888 * L_6 = (Dictionary_2_t1_888 *)il2cpp_codegen_object_new (Dictionary_2_t1_888_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5588(L_6, /*hidden argument*/Dictionary_2__ctor_m1_5588_MethodInfo_var);
		V_1 = L_6;
		Dictionary_2_t1_888 * L_7 = V_1;
		String_t* L_8 = ___channelName;
		NullCheck(L_7);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Add(!0,!1) */, L_7, 1, L_8);
		Dictionary_2_t1_888 * L_9 = V_1;
		Object_t * L_10 = ___message;
		NullCheck(L_9);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Add(!0,!1) */, L_9, 3, L_10);
		Dictionary_2_t1_888 * L_11 = V_1;
		V_0 = L_11;
		ChatPeer_t7_3 * L_12 = (__this->___chatPeer_6);
		Dictionary_2_t1_888 * L_13 = V_0;
		NullCheck(L_12);
		bool L_14 = (bool)VirtFuncInvoker3< bool, uint8_t, Dictionary_2_t1_888 *, bool >::Invoke(14 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::OpCustom(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean) */, L_12, 2, L_13, 1);
		return L_14;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::SendPrivateMessage(System.String,System.Object)
extern "C" bool ChatClient_SendPrivateMessage_m7_41 (ChatClient_t7_2 * __this, String_t* ___target, Object_t * ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___target;
		Object_t * L_1 = ___message;
		bool L_2 = ChatClient_SendPrivateMessage_m7_42(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::SendPrivateMessage(System.String,System.Object,System.Boolean)
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_888_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5588_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2952;
extern Il2CppCodeGenString* _stringLiteral2953;
extern "C" bool ChatClient_SendPrivateMessage_m7_42 (ChatClient_t7_2 * __this, String_t* ___target, Object_t * ___message, bool ___encrypt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Dictionary_2_t1_888_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(857);
		Dictionary_2__ctor_m1_5588_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483791);
		_stringLiteral2952 = il2cpp_codegen_string_literal_from_index(2952);
		_stringLiteral2953 = il2cpp_codegen_string_literal_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_888 * V_0 = {0};
	bool V_1 = false;
	Dictionary_2_t1_888 * V_2 = {0};
	{
		bool L_0 = ChatClient_get_CanChat_m7_23(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Object_t * L_1 = (__this->___listener_5);
		NullCheck(L_1);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_1, 1, _stringLiteral2952);
		return 0;
	}

IL_001e:
	{
		String_t* L_2 = ___target;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002f;
		}
	}
	{
		Object_t * L_4 = ___message;
		if (L_4)
		{
			goto IL_0042;
		}
	}

IL_002f:
	{
		Object_t * L_5 = (__this->___listener_5);
		NullCheck(L_5);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_5, 2, _stringLiteral2953);
		return 0;
	}

IL_0042:
	{
		Dictionary_2_t1_888 * L_6 = (Dictionary_2_t1_888 *)il2cpp_codegen_object_new (Dictionary_2_t1_888_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5588(L_6, /*hidden argument*/Dictionary_2__ctor_m1_5588_MethodInfo_var);
		V_2 = L_6;
		Dictionary_2_t1_888 * L_7 = V_2;
		String_t* L_8 = ___target;
		NullCheck(L_7);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Add(!0,!1) */, L_7, ((int32_t)225), L_8);
		Dictionary_2_t1_888 * L_9 = V_2;
		Object_t * L_10 = ___message;
		NullCheck(L_9);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Add(!0,!1) */, L_9, 3, L_10);
		Dictionary_2_t1_888 * L_11 = V_2;
		V_0 = L_11;
		ChatPeer_t7_3 * L_12 = (__this->___chatPeer_6);
		Dictionary_2_t1_888 * L_13 = V_0;
		bool L_14 = ___encrypt;
		NullCheck(L_12);
		bool L_15 = (bool)VirtFuncInvoker5< bool, uint8_t, Dictionary_2_t1_888 *, bool, uint8_t, bool >::Invoke(16 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::OpCustom(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean,System.Byte,System.Boolean) */, L_12, 3, L_13, 1, 0, L_14);
		V_1 = L_15;
		bool L_16 = V_1;
		return L_16;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::SetOnlineStatus(System.Int32,System.Object,System.Boolean)
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_888_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5588_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2954;
extern "C" bool ChatClient_SetOnlineStatus_m7_43 (ChatClient_t7_2 * __this, int32_t ___status, Object_t * ___message, bool ___skipMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		Dictionary_2_t1_888_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(857);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		Dictionary_2__ctor_m1_5588_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483791);
		_stringLiteral2954 = il2cpp_codegen_string_literal_from_index(2954);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_888 * V_0 = {0};
	Dictionary_2_t1_888 * V_1 = {0};
	{
		bool L_0 = ChatClient_get_CanChat_m7_23(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Object_t * L_1 = (__this->___listener_5);
		NullCheck(L_1);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_1, 1, _stringLiteral2954);
		return 0;
	}

IL_001e:
	{
		Dictionary_2_t1_888 * L_2 = (Dictionary_2_t1_888 *)il2cpp_codegen_object_new (Dictionary_2_t1_888_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5588(L_2, /*hidden argument*/Dictionary_2__ctor_m1_5588_MethodInfo_var);
		V_1 = L_2;
		Dictionary_2_t1_888 * L_3 = V_1;
		int32_t L_4 = ___status;
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Add(!0,!1) */, L_3, ((int32_t)10), L_6);
		Dictionary_2_t1_888 * L_7 = V_1;
		V_0 = L_7;
		bool L_8 = ___skipMessage;
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		Dictionary_2_t1_888 * L_9 = V_0;
		bool L_10 = 1;
		Object_t * L_11 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_9);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::set_Item(!0,!1) */, L_9, ((int32_t)12), L_11);
		goto IL_0055;
	}

IL_004d:
	{
		Dictionary_2_t1_888 * L_12 = V_0;
		Object_t * L_13 = ___message;
		NullCheck(L_12);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::set_Item(!0,!1) */, L_12, 3, L_13);
	}

IL_0055:
	{
		ChatPeer_t7_3 * L_14 = (__this->___chatPeer_6);
		Dictionary_2_t1_888 * L_15 = V_0;
		NullCheck(L_14);
		bool L_16 = (bool)VirtFuncInvoker3< bool, uint8_t, Dictionary_2_t1_888 *, bool >::Invoke(14 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::OpCustom(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean) */, L_14, 5, L_15, 1);
		return L_16;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::SetOnlineStatus(System.Int32)
extern "C" bool ChatClient_SetOnlineStatus_m7_44 (ChatClient_t7_2 * __this, int32_t ___status, const MethodInfo* method)
{
	{
		int32_t L_0 = ___status;
		bool L_1 = ChatClient_SetOnlineStatus_m7_43(__this, L_0, NULL, 1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::SetOnlineStatus(System.Int32,System.Object)
extern "C" bool ChatClient_SetOnlineStatus_m7_45 (ChatClient_t7_2 * __this, int32_t ___status, Object_t * ___message, const MethodInfo* method)
{
	{
		int32_t L_0 = ___status;
		Object_t * L_1 = ___message;
		bool L_2 = ChatClient_SetOnlineStatus_m7_43(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::AddFriends(System.String[])
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_888_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5588_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2955;
extern Il2CppCodeGenString* _stringLiteral2956;
extern Il2CppCodeGenString* _stringLiteral2957;
extern Il2CppCodeGenString* _stringLiteral2588;
extern "C" bool ChatClient_AddFriends_m7_46 (ChatClient_t7_2 * __this, StringU5BU5D_t1_202* ___friends, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Dictionary_2_t1_888_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(857);
		Dictionary_2__ctor_m1_5588_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483791);
		_stringLiteral2955 = il2cpp_codegen_string_literal_from_index(2955);
		_stringLiteral2956 = il2cpp_codegen_string_literal_from_index(2956);
		_stringLiteral2957 = il2cpp_codegen_string_literal_from_index(2957);
		_stringLiteral2588 = il2cpp_codegen_string_literal_from_index(2588);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_888 * V_0 = {0};
	Dictionary_2_t1_888 * V_1 = {0};
	{
		bool L_0 = ChatClient_get_CanChat_m7_23(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Object_t * L_1 = (__this->___listener_5);
		NullCheck(L_1);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_1, 1, _stringLiteral2955);
		return 0;
	}

IL_001e:
	{
		StringU5BU5D_t1_202* L_2 = ___friends;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		StringU5BU5D_t1_202* L_3 = ___friends;
		NullCheck(L_3);
		if ((((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))))
		{
			goto IL_003f;
		}
	}

IL_002c:
	{
		Object_t * L_4 = (__this->___listener_5);
		NullCheck(L_4);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_4, 2, _stringLiteral2956);
		return 0;
	}

IL_003f:
	{
		StringU5BU5D_t1_202* L_5 = ___friends;
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length))))) <= ((int32_t)((int32_t)1024))))
		{
			goto IL_008d;
		}
	}
	{
		Object_t * L_6 = (__this->___listener_5);
		ObjectU5BU5D_t1_157* L_7 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral2957);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral2957;
		ObjectU5BU5D_t1_157* L_8 = L_7;
		StringU5BU5D_t1_202* L_9 = ___friends;
		NullCheck(L_9);
		int32_t L_10 = (((int32_t)((int32_t)(((Array_t *)L_9)->max_length))));
		Object_t * L_11 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 1, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_157* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral2588);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2588;
		ObjectU5BU5D_t1_157* L_13 = L_12;
		int32_t L_14 = ((int32_t)1024);
		Object_t * L_15 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m1_421(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		NullCheck(L_6);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_6, 2, L_16);
		return 0;
	}

IL_008d:
	{
		Dictionary_2_t1_888 * L_17 = (Dictionary_2_t1_888 *)il2cpp_codegen_object_new (Dictionary_2_t1_888_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5588(L_17, /*hidden argument*/Dictionary_2__ctor_m1_5588_MethodInfo_var);
		V_1 = L_17;
		Dictionary_2_t1_888 * L_18 = V_1;
		StringU5BU5D_t1_202* L_19 = ___friends;
		NullCheck(L_18);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Add(!0,!1) */, L_18, ((int32_t)11), (Object_t *)(Object_t *)L_19);
		Dictionary_2_t1_888 * L_20 = V_1;
		V_0 = L_20;
		ChatPeer_t7_3 * L_21 = (__this->___chatPeer_6);
		Dictionary_2_t1_888 * L_22 = V_0;
		NullCheck(L_21);
		bool L_23 = (bool)VirtFuncInvoker3< bool, uint8_t, Dictionary_2_t1_888 *, bool >::Invoke(14 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::OpCustom(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean) */, L_21, 6, L_22, 1);
		return L_23;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::RemoveFriends(System.String[])
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_888_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5588_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2958;
extern Il2CppCodeGenString* _stringLiteral2959;
extern Il2CppCodeGenString* _stringLiteral2960;
extern Il2CppCodeGenString* _stringLiteral2588;
extern "C" bool ChatClient_RemoveFriends_m7_47 (ChatClient_t7_2 * __this, StringU5BU5D_t1_202* ___friends, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Dictionary_2_t1_888_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(857);
		Dictionary_2__ctor_m1_5588_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483791);
		_stringLiteral2958 = il2cpp_codegen_string_literal_from_index(2958);
		_stringLiteral2959 = il2cpp_codegen_string_literal_from_index(2959);
		_stringLiteral2960 = il2cpp_codegen_string_literal_from_index(2960);
		_stringLiteral2588 = il2cpp_codegen_string_literal_from_index(2588);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_888 * V_0 = {0};
	Dictionary_2_t1_888 * V_1 = {0};
	{
		bool L_0 = ChatClient_get_CanChat_m7_23(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Object_t * L_1 = (__this->___listener_5);
		NullCheck(L_1);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_1, 1, _stringLiteral2958);
		return 0;
	}

IL_001e:
	{
		StringU5BU5D_t1_202* L_2 = ___friends;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		StringU5BU5D_t1_202* L_3 = ___friends;
		NullCheck(L_3);
		if ((((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))))
		{
			goto IL_003f;
		}
	}

IL_002c:
	{
		Object_t * L_4 = (__this->___listener_5);
		NullCheck(L_4);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_4, 2, _stringLiteral2959);
		return 0;
	}

IL_003f:
	{
		StringU5BU5D_t1_202* L_5 = ___friends;
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length))))) <= ((int32_t)((int32_t)1024))))
		{
			goto IL_008d;
		}
	}
	{
		Object_t * L_6 = (__this->___listener_5);
		ObjectU5BU5D_t1_157* L_7 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral2960);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral2960;
		ObjectU5BU5D_t1_157* L_8 = L_7;
		StringU5BU5D_t1_202* L_9 = ___friends;
		NullCheck(L_9);
		int32_t L_10 = (((int32_t)((int32_t)(((Array_t *)L_9)->max_length))));
		Object_t * L_11 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 1, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_157* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral2588);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2588;
		ObjectU5BU5D_t1_157* L_13 = L_12;
		int32_t L_14 = ((int32_t)1024);
		Object_t * L_15 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m1_421(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		NullCheck(L_6);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_6, 2, L_16);
		return 0;
	}

IL_008d:
	{
		Dictionary_2_t1_888 * L_17 = (Dictionary_2_t1_888 *)il2cpp_codegen_object_new (Dictionary_2_t1_888_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5588(L_17, /*hidden argument*/Dictionary_2__ctor_m1_5588_MethodInfo_var);
		V_1 = L_17;
		Dictionary_2_t1_888 * L_18 = V_1;
		StringU5BU5D_t1_202* L_19 = ___friends;
		NullCheck(L_18);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Add(!0,!1) */, L_18, ((int32_t)11), (Object_t *)(Object_t *)L_19);
		Dictionary_2_t1_888 * L_20 = V_1;
		V_0 = L_20;
		ChatPeer_t7_3 * L_21 = (__this->___chatPeer_6);
		Dictionary_2_t1_888 * L_22 = V_0;
		NullCheck(L_21);
		bool L_23 = (bool)VirtFuncInvoker3< bool, uint8_t, Dictionary_2_t1_888 *, bool >::Invoke(14 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::OpCustom(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean) */, L_21, 7, L_22, 1);
		return L_23;
	}
}
// System.String ExitGames.Client.Photon.Chat.ChatClient::GetPrivateChannelNameByUser(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2961;
extern "C" String_t* ChatClient_GetPrivateChannelNameByUser_m7_48 (ChatClient_t7_2 * __this, String_t* ___userName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2961 = il2cpp_codegen_string_literal_from_index(2961);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ChatClient_get_UserId_m7_31(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___userName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1_411(NULL /*static, unused*/, _stringLiteral2961, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::TryGetChannel(System.String,System.Boolean,ExitGames.Client.Photon.Chat.ChatChannel&)
extern "C" bool ChatClient_TryGetChannel_m7_49 (ChatClient_t7_2 * __this, String_t* ___channelName, bool ___isPrivate, ChatChannel_t7_1 ** ___channel, const MethodInfo* method)
{
	{
		bool L_0 = ___isPrivate;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Dictionary_2_t1_932 * L_1 = (__this->___PublicChannels_3);
		String_t* L_2 = ___channelName;
		ChatChannel_t7_1 ** L_3 = ___channel;
		NullCheck(L_1);
		bool L_4 = (bool)VirtFuncInvoker2< bool, String_t*, ChatChannel_t7_1 ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::TryGetValue(!0,!1&) */, L_1, L_2, L_3);
		return L_4;
	}

IL_0014:
	{
		Dictionary_2_t1_932 * L_5 = (__this->___PrivateChannels_4);
		String_t* L_6 = ___channelName;
		ChatChannel_t7_1 ** L_7 = ___channel;
		NullCheck(L_5);
		bool L_8 = (bool)VirtFuncInvoker2< bool, String_t*, ChatChannel_t7_1 ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::TryGetValue(!0,!1&) */, L_5, L_6, L_7);
		return L_8;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::TryGetChannel(System.String,ExitGames.Client.Photon.Chat.ChatChannel&)
extern "C" bool ChatClient_TryGetChannel_m7_50 (ChatClient_t7_2 * __this, String_t* ___channelName, ChatChannel_t7_1 ** ___channel, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 0;
		Dictionary_2_t1_932 * L_0 = (__this->___PublicChannels_3);
		String_t* L_1 = ___channelName;
		ChatChannel_t7_1 ** L_2 = ___channel;
		NullCheck(L_0);
		bool L_3 = (bool)VirtFuncInvoker2< bool, String_t*, ChatChannel_t7_1 ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::TryGetValue(!0,!1&) */, L_0, L_1, L_2);
		V_0 = L_3;
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_0018;
		}
	}
	{
		return 1;
	}

IL_0018:
	{
		Dictionary_2_t1_932 * L_5 = (__this->___PrivateChannels_4);
		String_t* L_6 = ___channelName;
		ChatChannel_t7_1 ** L_7 = ___channel;
		NullCheck(L_5);
		bool L_8 = (bool)VirtFuncInvoker2< bool, String_t*, ChatChannel_t7_1 ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::TryGetValue(!0,!1&) */, L_5, L_6, L_7);
		V_0 = L_8;
		bool L_9 = V_0;
		return L_9;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::SendAcksOnly()
extern "C" void ChatClient_SendAcksOnly_m7_51 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		ChatPeer_t7_3 * L_0 = (__this->___chatPeer_6);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		ChatPeer_t7_3 * L_1 = (__this->___chatPeer_6);
		NullCheck(L_1);
		VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::SendAcksOnly() */, L_1);
	}

IL_0017:
	{
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::set_DebugOut(ExitGames.Client.Photon.DebugLevel)
extern "C" void ChatClient_set_DebugOut_m7_52 (ChatClient_t7_2 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		ChatPeer_t7_3 * L_0 = (__this->___chatPeer_6);
		uint8_t L_1 = ___value;
		NullCheck(L_0);
		PhotonPeer_set_DebugOut_m5_198(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.Chat.ChatClient::get_DebugOut()
extern "C" uint8_t ChatClient_get_DebugOut_m7_53 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	{
		ChatPeer_t7_3 * L_0 = (__this->___chatPeer_6);
		NullCheck(L_0);
		uint8_t L_1 = PhotonPeer_get_DebugOut_m5_199(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::SendChannelOperation(System.String[],System.Byte,System.Int32)
extern TypeInfo* Dictionary_2_t1_888_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5588_MethodInfo_var;
extern "C" bool ChatClient_SendChannelOperation_m7_54 (ChatClient_t7_2 * __this, StringU5BU5D_t1_202* ___channels, uint8_t ___operation, int32_t ___historyLength, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_888_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(857);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Dictionary_2__ctor_m1_5588_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483791);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_888 * V_0 = {0};
	Dictionary_2_t1_888 * V_1 = {0};
	{
		Dictionary_2_t1_888 * L_0 = (Dictionary_2_t1_888 *)il2cpp_codegen_object_new (Dictionary_2_t1_888_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5588(L_0, /*hidden argument*/Dictionary_2__ctor_m1_5588_MethodInfo_var);
		V_1 = L_0;
		Dictionary_2_t1_888 * L_1 = V_1;
		StringU5BU5D_t1_202* L_2 = ___channels;
		NullCheck(L_1);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Add(!0,!1) */, L_1, 0, (Object_t *)(Object_t *)L_2);
		Dictionary_2_t1_888 * L_3 = V_1;
		V_0 = L_3;
		int32_t L_4 = ___historyLength;
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		Dictionary_2_t1_888 * L_5 = V_0;
		int32_t L_6 = ___historyLength;
		int32_t L_7 = L_6;
		Object_t * L_8 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Add(!0,!1) */, L_5, ((int32_t)14), L_8);
	}

IL_0024:
	{
		ChatPeer_t7_3 * L_9 = (__this->___chatPeer_6);
		uint8_t L_10 = ___operation;
		Dictionary_2_t1_888 * L_11 = V_0;
		NullCheck(L_9);
		bool L_12 = (bool)VirtFuncInvoker3< bool, uint8_t, Dictionary_2_t1_888 *, bool >::Invoke(14 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::OpCustom(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean) */, L_9, L_10, L_11, 1);
		return L_12;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::HandlePrivateMessageEvent(ExitGames.Client.Photon.EventData)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ChatChannel_t7_1_il2cpp_TypeInfo_var;
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern "C" void ChatClient_HandlePrivateMessageEvent_m7_55 (ChatClient_t7_2 * __this, EventData_t5_44 * ___eventData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		ChatChannel_t7_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1050);
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	String_t* V_3 = {0};
	ChatChannel_t7_1 * V_4 = {0};
	{
		EventData_t5_44 * L_0 = ___eventData;
		NullCheck(L_0);
		Dictionary_2_t1_888 * L_1 = (L_0->___Parameters_1);
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(!0) */, L_1, 3);
		V_0 = L_2;
		EventData_t5_44 * L_3 = ___eventData;
		NullCheck(L_3);
		Dictionary_2_t1_888 * L_4 = (L_3->___Parameters_1);
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(!0) */, L_4, 5);
		V_1 = ((String_t*)CastclassSealed(L_5, String_t_il2cpp_TypeInfo_var));
		String_t* L_6 = ChatClient_get_UserId_m7_31(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		String_t* L_7 = ChatClient_get_UserId_m7_31(__this, /*hidden argument*/NULL);
		String_t* L_8 = V_1;
		NullCheck(L_7);
		bool L_9 = String_Equals_m1_340(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005e;
		}
	}
	{
		EventData_t5_44 * L_10 = ___eventData;
		NullCheck(L_10);
		Dictionary_2_t1_888 * L_11 = (L_10->___Parameters_1);
		NullCheck(L_11);
		Object_t * L_12 = (Object_t *)VirtFuncInvoker1< Object_t *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(!0) */, L_11, ((int32_t)225));
		V_3 = ((String_t*)CastclassSealed(L_12, String_t_il2cpp_TypeInfo_var));
		String_t* L_13 = V_3;
		String_t* L_14 = ChatClient_GetPrivateChannelNameByUser_m7_48(__this, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		goto IL_0066;
	}

IL_005e:
	{
		String_t* L_15 = V_1;
		String_t* L_16 = ChatClient_GetPrivateChannelNameByUser_m7_48(__this, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
	}

IL_0066:
	{
		Dictionary_2_t1_932 * L_17 = (__this->___PrivateChannels_4);
		String_t* L_18 = V_2;
		NullCheck(L_17);
		bool L_19 = (bool)VirtFuncInvoker2< bool, String_t*, ChatChannel_t7_1 ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::TryGetValue(!0,!1&) */, L_17, L_18, (&V_4));
		if (L_19)
		{
			goto IL_009d;
		}
	}
	{
		String_t* L_20 = V_2;
		ChatChannel_t7_1 * L_21 = (ChatChannel_t7_1 *)il2cpp_codegen_object_new (ChatChannel_t7_1_il2cpp_TypeInfo_var);
		ChatChannel__ctor_m7_0(L_21, L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		ChatChannel_t7_1 * L_22 = V_4;
		NullCheck(L_22);
		ChatChannel_set_IsPrivate_m7_2(L_22, 1, /*hidden argument*/NULL);
		Dictionary_2_t1_932 * L_23 = (__this->___PrivateChannels_4);
		ChatChannel_t7_1 * L_24 = V_4;
		NullCheck(L_24);
		String_t* L_25 = (L_24->___Name_0);
		ChatChannel_t7_1 * L_26 = V_4;
		NullCheck(L_23);
		VirtActionInvoker2< String_t*, ChatChannel_t7_1 * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::Add(!0,!1) */, L_23, L_25, L_26);
	}

IL_009d:
	{
		ChatChannel_t7_1 * L_27 = V_4;
		String_t* L_28 = V_1;
		Object_t * L_29 = V_0;
		NullCheck(L_27);
		ChatChannel_Add_m7_4(L_27, L_28, L_29, /*hidden argument*/NULL);
		Object_t * L_30 = (__this->___listener_5);
		String_t* L_31 = V_1;
		Object_t * L_32 = V_0;
		String_t* L_33 = V_2;
		NullCheck(L_30);
		InterfaceActionInvoker3< String_t*, Object_t *, String_t* >::Invoke(5 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::OnPrivateMessage(System.String,System.Object,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_30, L_31, L_32, L_33);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::HandleChatMessagesEvent(ExitGames.Client.Photon.EventData)
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t1_202_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1230;
extern Il2CppCodeGenString* _stringLiteral2962;
extern "C" void ChatClient_HandleChatMessagesEvent_m7_56 (ChatClient_t7_2 * __this, EventData_t5_44 * ___eventData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		StringU5BU5D_t1_202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(32);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		_stringLiteral1230 = il2cpp_codegen_string_literal_from_index(1230);
		_stringLiteral2962 = il2cpp_codegen_string_literal_from_index(2962);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1_157* V_0 = {0};
	StringU5BU5D_t1_202* V_1 = {0};
	String_t* V_2 = {0};
	ChatChannel_t7_1 * V_3 = {0};
	{
		EventData_t5_44 * L_0 = ___eventData;
		NullCheck(L_0);
		Dictionary_2_t1_888 * L_1 = (L_0->___Parameters_1);
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(!0) */, L_1, 2);
		V_0 = ((ObjectU5BU5D_t1_157*)Castclass(L_2, ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var));
		EventData_t5_44 * L_3 = ___eventData;
		NullCheck(L_3);
		Dictionary_2_t1_888 * L_4 = (L_3->___Parameters_1);
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(!0) */, L_4, 4);
		V_1 = ((StringU5BU5D_t1_202*)Castclass(L_5, StringU5BU5D_t1_202_il2cpp_TypeInfo_var));
		EventData_t5_44 * L_6 = ___eventData;
		NullCheck(L_6);
		Dictionary_2_t1_888 * L_7 = (L_6->___Parameters_1);
		NullCheck(L_7);
		Object_t * L_8 = (Object_t *)VirtFuncInvoker1< Object_t *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(!0) */, L_7, 1);
		V_2 = ((String_t*)CastclassSealed(L_8, String_t_il2cpp_TypeInfo_var));
		Dictionary_2_t1_932 * L_9 = (__this->___PublicChannels_3);
		String_t* L_10 = V_2;
		NullCheck(L_9);
		bool L_11 = (bool)VirtFuncInvoker2< bool, String_t*, ChatChannel_t7_1 ** >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::TryGetValue(!0,!1&) */, L_9, L_10, (&V_3));
		if (L_11)
		{
			goto IL_0066;
		}
	}
	{
		Object_t * L_12 = (__this->___listener_5);
		String_t* L_13 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m1_419(NULL /*static, unused*/, _stringLiteral1230, L_13, _stringLiteral2962, /*hidden argument*/NULL);
		NullCheck(L_12);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_12, 2, L_14);
		return;
	}

IL_0066:
	{
		ChatChannel_t7_1 * L_15 = V_3;
		StringU5BU5D_t1_202* L_16 = V_1;
		ObjectU5BU5D_t1_157* L_17 = V_0;
		NullCheck(L_15);
		ChatChannel_Add_m7_5(L_15, L_16, L_17, /*hidden argument*/NULL);
		Object_t * L_18 = (__this->___listener_5);
		String_t* L_19 = V_2;
		StringU5BU5D_t1_202* L_20 = V_1;
		ObjectU5BU5D_t1_157* L_21 = V_0;
		NullCheck(L_18);
		InterfaceActionInvoker3< String_t*, StringU5BU5D_t1_202*, ObjectU5BU5D_t1_157* >::Invoke(4 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::OnGetMessages(System.String,System.String[],System.Object[]) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_18, L_19, L_20, L_21);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::HandleSubscribeEvent(ExitGames.Client.Photon.EventData)
extern TypeInfo* StringU5BU5D_t1_202_il2cpp_TypeInfo_var;
extern TypeInfo* BooleanU5BU5D_t1_264_il2cpp_TypeInfo_var;
extern TypeInfo* ChatChannel_t7_1_il2cpp_TypeInfo_var;
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern "C" void ChatClient_HandleSubscribeEvent_m7_57 (ChatClient_t7_2 * __this, EventData_t5_44 * ___eventData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(32);
		BooleanU5BU5D_t1_264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		ChatChannel_t7_1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1050);
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1_202* V_0 = {0};
	BooleanU5BU5D_t1_264* V_1 = {0};
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	ChatChannel_t7_1 * V_4 = {0};
	{
		EventData_t5_44 * L_0 = ___eventData;
		NullCheck(L_0);
		Dictionary_2_t1_888 * L_1 = (L_0->___Parameters_1);
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(!0) */, L_1, 0);
		V_0 = ((StringU5BU5D_t1_202*)Castclass(L_2, StringU5BU5D_t1_202_il2cpp_TypeInfo_var));
		EventData_t5_44 * L_3 = ___eventData;
		NullCheck(L_3);
		Dictionary_2_t1_888 * L_4 = (L_3->___Parameters_1);
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(!0) */, L_4, ((int32_t)15));
		V_1 = ((BooleanU5BU5D_t1_264*)Castclass(L_5, BooleanU5BU5D_t1_264_il2cpp_TypeInfo_var));
		V_2 = 0;
		goto IL_0069;
	}

IL_002c:
	{
		BooleanU5BU5D_t1_264* L_6 = V_1;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		if (!(*(uint8_t*)(bool*)SZArrayLdElema(L_6, L_8, sizeof(bool))))
		{
			goto IL_0065;
		}
	}
	{
		StringU5BU5D_t1_202* L_9 = V_0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		V_3 = (*(String_t**)(String_t**)SZArrayLdElema(L_9, L_11, sizeof(String_t*)));
		Dictionary_2_t1_932 * L_12 = (__this->___PublicChannels_3);
		String_t* L_13 = V_3;
		NullCheck(L_12);
		bool L_14 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::ContainsKey(!0) */, L_12, L_13);
		if (L_14)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_15 = V_3;
		ChatChannel_t7_1 * L_16 = (ChatChannel_t7_1 *)il2cpp_codegen_object_new (ChatChannel_t7_1_il2cpp_TypeInfo_var);
		ChatChannel__ctor_m7_0(L_16, L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		Dictionary_2_t1_932 * L_17 = (__this->___PublicChannels_3);
		ChatChannel_t7_1 * L_18 = V_4;
		NullCheck(L_18);
		String_t* L_19 = (L_18->___Name_0);
		ChatChannel_t7_1 * L_20 = V_4;
		NullCheck(L_17);
		VirtActionInvoker2< String_t*, ChatChannel_t7_1 * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::Add(!0,!1) */, L_17, L_19, L_20);
	}

IL_0065:
	{
		int32_t L_21 = V_2;
		V_2 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0069:
	{
		int32_t L_22 = V_2;
		StringU5BU5D_t1_202* L_23 = V_0;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_23)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		Object_t * L_24 = (__this->___listener_5);
		StringU5BU5D_t1_202* L_25 = V_0;
		BooleanU5BU5D_t1_264* L_26 = V_1;
		NullCheck(L_24);
		InterfaceActionInvoker2< StringU5BU5D_t1_202*, BooleanU5BU5D_t1_264* >::Invoke(6 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::OnSubscribed(System.String[],System.Boolean[]) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_24, L_25, L_26);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::HandleUnsubscribeEvent(ExitGames.Client.Photon.EventData)
extern TypeInfo* StringU5BU5D_t1_202_il2cpp_TypeInfo_var;
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern "C" void ChatClient_HandleUnsubscribeEvent_m7_58 (ChatClient_t7_2 * __this, EventData_t5_44 * ___eventData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(32);
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1_202* V_0 = {0};
	int32_t V_1 = 0;
	String_t* V_2 = {0};
	{
		EventData_t5_44 * L_0 = ___eventData;
		NullCheck(L_0);
		Object_t * L_1 = EventData_get_Item_m5_265(L_0, 0, /*hidden argument*/NULL);
		V_0 = ((StringU5BU5D_t1_202*)Castclass(L_1, StringU5BU5D_t1_202_il2cpp_TypeInfo_var));
		V_1 = 0;
		goto IL_0029;
	}

IL_0014:
	{
		StringU5BU5D_t1_202* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_2 = (*(String_t**)(String_t**)SZArrayLdElema(L_2, L_4, sizeof(String_t*)));
		Dictionary_2_t1_932 * L_5 = (__this->___PublicChannels_3);
		String_t* L_6 = V_2;
		NullCheck(L_5);
		VirtFuncInvoker1< bool, String_t* >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::Remove(!0) */, L_5, L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_8 = V_1;
		StringU5BU5D_t1_202* L_9 = V_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		Object_t * L_10 = (__this->___listener_5);
		StringU5BU5D_t1_202* L_11 = V_0;
		NullCheck(L_10);
		InterfaceActionInvoker1< StringU5BU5D_t1_202* >::Invoke(7 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::OnUnsubscribed(System.String[]) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_10, L_11);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::HandleAuthResponse(ExitGames.Client.Photon.OperationResponse)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern TypeInfo* AuthenticationValues_t7_4_il2cpp_TypeInfo_var;
extern TypeInfo* Int16_t1_13_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2963;
extern Il2CppCodeGenString* _stringLiteral2964;
extern Il2CppCodeGenString* _stringLiteral2965;
extern Il2CppCodeGenString* _stringLiteral2966;
extern "C" void ChatClient_HandleAuthResponse_m7_59 (ChatClient_t7_2 * __this, OperationResponse_t5_43 * ___operationResponse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		AuthenticationValues_t7_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1055);
		Int16_t1_13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		_stringLiteral2963 = il2cpp_codegen_string_literal_from_index(2963);
		_stringLiteral2964 = il2cpp_codegen_string_literal_from_index(2964);
		_stringLiteral2965 = il2cpp_codegen_string_literal_from_index(2965);
		_stringLiteral2966 = il2cpp_codegen_string_literal_from_index(2966);
		s_Il2CppMethodIntialized = true;
	}
	int16_t V_0 = 0;
	{
		Object_t * L_0 = (__this->___listener_5);
		OperationResponse_t5_43 * L_1 = ___operationResponse;
		NullCheck(L_1);
		String_t* L_2 = OperationResponse_ToStringFull_m5_263(L_1, /*hidden argument*/NULL);
		ChatPeer_t7_3 * L_3 = (__this->___chatPeer_6);
		NullCheck(L_3);
		String_t* L_4 = ChatPeer_get_NameServerAddress_m7_68(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1_419(NULL /*static, unused*/, L_2, _stringLiteral2963, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_0, 3, L_5);
		OperationResponse_t5_43 * L_6 = ___operationResponse;
		NullCheck(L_6);
		int16_t L_7 = (L_6->___ReturnCode_1);
		if (L_7)
		{
			goto IL_011a;
		}
	}
	{
		int32_t L_8 = ChatClient_get_State_m7_19(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)2))))
		{
			goto IL_00d8;
		}
	}
	{
		ChatClient_set_State_m7_20(__this, 4, /*hidden argument*/NULL);
		Object_t * L_9 = (__this->___listener_5);
		int32_t L_10 = ChatClient_get_State_m7_19(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		InterfaceActionInvoker1< int32_t >::Invoke(3 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::OnChatStateChange(ExitGames.Client.Photon.Chat.ChatState) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_9, L_10);
		OperationResponse_t5_43 * L_11 = ___operationResponse;
		NullCheck(L_11);
		Dictionary_2_t1_888 * L_12 = (L_11->___Parameters_3);
		NullCheck(L_12);
		bool L_13 = (bool)VirtFuncInvoker1< bool, uint8_t >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::ContainsKey(!0) */, L_12, ((int32_t)221));
		if (!L_13)
		{
			goto IL_00c2;
		}
	}
	{
		AuthenticationValues_t7_4 * L_14 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0081;
		}
	}
	{
		AuthenticationValues_t7_4 * L_15 = (AuthenticationValues_t7_4 *)il2cpp_codegen_object_new (AuthenticationValues_t7_4_il2cpp_TypeInfo_var);
		AuthenticationValues__ctor_m7_73(L_15, /*hidden argument*/NULL);
		ChatClient_set_AuthValues_m7_30(__this, L_15, /*hidden argument*/NULL);
	}

IL_0081:
	{
		AuthenticationValues_t7_4 * L_16 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		OperationResponse_t5_43 * L_17 = ___operationResponse;
		NullCheck(L_17);
		Object_t * L_18 = OperationResponse_get_Item_m5_261(L_17, ((int32_t)221), /*hidden argument*/NULL);
		NullCheck(L_16);
		AuthenticationValues_set_Token_m7_82(L_16, ((String_t*)IsInstSealed(L_18, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		OperationResponse_t5_43 * L_19 = ___operationResponse;
		NullCheck(L_19);
		Object_t * L_20 = OperationResponse_get_Item_m5_261(L_19, ((int32_t)230), /*hidden argument*/NULL);
		ChatClient_set_FrontendAddress_m7_16(__this, ((String_t*)CastclassSealed(L_20, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		ChatPeer_t7_3 * L_21 = (__this->___chatPeer_6);
		NullCheck(L_21);
		VirtActionInvoker0::Invoke(7 /* System.Void ExitGames.Client.Photon.PhotonPeer::Disconnect() */, L_21);
		goto IL_00d3;
	}

IL_00c2:
	{
		Object_t * L_22 = (__this->___listener_5);
		NullCheck(L_22);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_22, 1, _stringLiteral2964);
	}

IL_00d3:
	{
		goto IL_0115;
	}

IL_00d8:
	{
		int32_t L_23 = ChatClient_get_State_m7_19(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)6))))
		{
			goto IL_0115;
		}
	}
	{
		int32_t L_24 = (__this->___msDeltaForServiceCalls_8);
		__this->___msDeltaForServiceCalls_8 = ((int32_t)((int32_t)L_24*(int32_t)4));
		ChatClient_set_State_m7_20(__this, 7, /*hidden argument*/NULL);
		Object_t * L_25 = (__this->___listener_5);
		int32_t L_26 = ChatClient_get_State_m7_19(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		InterfaceActionInvoker1< int32_t >::Invoke(3 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::OnChatStateChange(ExitGames.Client.Photon.Chat.ChatState) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_25, L_26);
		Object_t * L_27 = (__this->___listener_5);
		NullCheck(L_27);
		InterfaceActionInvoker0::Invoke(2 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::OnConnected() */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_27);
	}

IL_0115:
	{
		goto IL_01c8;
	}

IL_011a:
	{
		OperationResponse_t5_43 * L_28 = ___operationResponse;
		NullCheck(L_28);
		int16_t L_29 = (L_28->___ReturnCode_1);
		V_0 = L_29;
		int16_t L_30 = V_0;
		if (((int32_t)((int32_t)L_30-(int32_t)((int32_t)32755))) == 0)
		{
			goto IL_015d;
		}
		if (((int32_t)((int32_t)L_30-(int32_t)((int32_t)32755))) == 1)
		{
			goto IL_016a;
		}
		if (((int32_t)((int32_t)L_30-(int32_t)((int32_t)32755))) == 2)
		{
			goto IL_0176;
		}
	}
	{
		int16_t L_31 = V_0;
		if ((((int32_t)L_31) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0182;
		}
	}
	{
		int16_t L_32 = V_0;
		if ((((int32_t)L_32) == ((int32_t)((int32_t)32767))))
		{
			goto IL_0151;
		}
	}
	{
		goto IL_018f;
	}

IL_0151:
	{
		ChatClient_set_DisconnectedCause_m7_22(__this, 6, /*hidden argument*/NULL);
		goto IL_018f;
	}

IL_015d:
	{
		ChatClient_set_DisconnectedCause_m7_22(__this, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_018f;
	}

IL_016a:
	{
		ChatClient_set_DisconnectedCause_m7_22(__this, 8, /*hidden argument*/NULL);
		goto IL_018f;
	}

IL_0176:
	{
		ChatClient_set_DisconnectedCause_m7_22(__this, 7, /*hidden argument*/NULL);
		goto IL_018f;
	}

IL_0182:
	{
		ChatClient_set_DisconnectedCause_m7_22(__this, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_018f;
	}

IL_018f:
	{
		Object_t * L_33 = (__this->___listener_5);
		OperationResponse_t5_43 * L_34 = ___operationResponse;
		NullCheck(L_34);
		int16_t L_35 = (L_34->___ReturnCode_1);
		int16_t L_36 = L_35;
		Object_t * L_37 = Box(Int16_t1_13_il2cpp_TypeInfo_var, &L_36);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = String_Concat_m1_417(NULL /*static, unused*/, _stringLiteral2965, L_37, _stringLiteral2966, /*hidden argument*/NULL);
		NullCheck(L_33);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_33, 1, L_38);
		ChatClient_set_State_m7_20(__this, ((int32_t)10), /*hidden argument*/NULL);
		ChatPeer_t7_3 * L_39 = (__this->___chatPeer_6);
		NullCheck(L_39);
		VirtActionInvoker0::Invoke(7 /* System.Void ExitGames.Client.Photon.PhotonPeer::Disconnect() */, L_39);
	}

IL_01c8:
	{
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::HandleStatusUpdate(ExitGames.Client.Photon.EventData)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern "C" void ChatClient_HandleStatusUpdate_m7_60 (ChatClient_t7_2 * __this, EventData_t5_44 * ___eventData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	Object_t * V_2 = {0};
	bool V_3 = false;
	{
		EventData_t5_44 * L_0 = ___eventData;
		NullCheck(L_0);
		Dictionary_2_t1_888 * L_1 = (L_0->___Parameters_1);
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(!0) */, L_1, 5);
		V_0 = ((String_t*)CastclassSealed(L_2, String_t_il2cpp_TypeInfo_var));
		EventData_t5_44 * L_3 = ___eventData;
		NullCheck(L_3);
		Dictionary_2_t1_888 * L_4 = (L_3->___Parameters_1);
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(!0) */, L_4, ((int32_t)10));
		V_1 = ((*(int32_t*)((int32_t*)UnBox (L_5, Int32_t1_3_il2cpp_TypeInfo_var))));
		V_2 = NULL;
		EventData_t5_44 * L_6 = ___eventData;
		NullCheck(L_6);
		Dictionary_2_t1_888 * L_7 = (L_6->___Parameters_1);
		NullCheck(L_7);
		bool L_8 = (bool)VirtFuncInvoker1< bool, uint8_t >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::ContainsKey(!0) */, L_7, 3);
		V_3 = L_8;
		bool L_9 = V_3;
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		EventData_t5_44 * L_10 = ___eventData;
		NullCheck(L_10);
		Dictionary_2_t1_888 * L_11 = (L_10->___Parameters_1);
		NullCheck(L_11);
		Object_t * L_12 = (Object_t *)VirtFuncInvoker1< Object_t *, uint8_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(!0) */, L_11, 3);
		V_2 = L_12;
	}

IL_0047:
	{
		Object_t * L_13 = (__this->___listener_5);
		String_t* L_14 = V_0;
		int32_t L_15 = V_1;
		bool L_16 = V_3;
		Object_t * L_17 = V_2;
		NullCheck(L_13);
		InterfaceActionInvoker4< String_t*, int32_t, bool, Object_t * >::Invoke(8 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::OnStatusUpdate(System.String,System.Int32,System.Boolean,System.Object) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_13, L_14, L_15, L_16, L_17);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatClient::ConnectToFrontEnd()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2967;
extern Il2CppCodeGenString* _stringLiteral2968;
extern "C" void ChatClient_ConnectToFrontEnd_m7_61 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		_stringLiteral2967 = il2cpp_codegen_string_literal_from_index(2967);
		_stringLiteral2968 = il2cpp_codegen_string_literal_from_index(2968);
		s_Il2CppMethodIntialized = true;
	}
	{
		ChatClient_set_State_m7_20(__this, 6, /*hidden argument*/NULL);
		Object_t * L_0 = (__this->___listener_5);
		String_t* L_1 = ChatClient_get_FrontendAddress_m7_15(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1_418(NULL /*static, unused*/, _stringLiteral2967, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_0, 3, L_2);
		ChatPeer_t7_3 * L_3 = (__this->___chatPeer_6);
		String_t* L_4 = ChatClient_get_FrontendAddress_m7_15(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(6 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::Connect(System.String,System.String) */, L_3, L_4, _stringLiteral2968);
		return;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::AuthenticateOnFrontEnd()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IChatClientListener_t7_5_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_888_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5588_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2969;
extern Il2CppCodeGenString* _stringLiteral2970;
extern "C" bool ChatClient_AuthenticateOnFrontEnd_m7_62 (ChatClient_t7_2 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		IChatClientListener_t7_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		Dictionary_2_t1_888_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(857);
		Dictionary_2__ctor_m1_5588_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483791);
		_stringLiteral2969 = il2cpp_codegen_string_literal_from_index(2969);
		_stringLiteral2970 = il2cpp_codegen_string_literal_from_index(2970);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_888 * V_0 = {0};
	Dictionary_2_t1_888 * V_1 = {0};
	{
		AuthenticationValues_t7_4 * L_0 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0079;
		}
	}
	{
		AuthenticationValues_t7_4 * L_1 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = AuthenticationValues_get_Token_m7_81(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		AuthenticationValues_t7_4 * L_3 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = AuthenticationValues_get_Token_m7_81(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_6 = String_op_Equality_m1_454(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0048;
		}
	}

IL_0035:
	{
		Object_t * L_7 = (__this->___listener_5);
		NullCheck(L_7);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_7, 1, _stringLiteral2969);
		return 0;
	}

IL_0048:
	{
		Dictionary_2_t1_888 * L_8 = (Dictionary_2_t1_888 *)il2cpp_codegen_object_new (Dictionary_2_t1_888_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5588(L_8, /*hidden argument*/Dictionary_2__ctor_m1_5588_MethodInfo_var);
		V_1 = L_8;
		Dictionary_2_t1_888 * L_9 = V_1;
		AuthenticationValues_t7_4 * L_10 = ChatClient_get_AuthValues_m7_29(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = AuthenticationValues_get_Token_m7_81(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Add(!0,!1) */, L_9, ((int32_t)221), L_11);
		Dictionary_2_t1_888 * L_12 = V_1;
		V_0 = L_12;
		ChatPeer_t7_3 * L_13 = (__this->___chatPeer_6);
		Dictionary_2_t1_888 * L_14 = V_0;
		NullCheck(L_13);
		bool L_15 = (bool)VirtFuncInvoker3< bool, uint8_t, Dictionary_2_t1_888 *, bool >::Invoke(14 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::OpCustom(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean) */, L_13, ((int32_t)230), L_14, 1);
		return L_15;
	}

IL_0079:
	{
		Object_t * L_16 = (__this->___listener_5);
		NullCheck(L_16);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IChatClientListener_t7_5_il2cpp_TypeInfo_var, L_16, 1, _stringLiteral2970);
		return 0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatEventCode::.ctor()
extern "C" void ChatEventCode__ctor_m7_63 (ChatEventCode_t7_7 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatOperationCode::.ctor()
extern "C" void ChatOperationCode__ctor_m7_64 (ChatOperationCode_t7_8 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatParameterCode::.ctor()
extern "C" void ChatParameterCode__ctor_m7_65 (ChatParameterCode_t7_9 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatPeer::.ctor(ExitGames.Client.Photon.IPhotonPeerListener,ExitGames.Client.Photon.ConnectionProtocol)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2971;
extern Il2CppCodeGenString* _stringLiteral2972;
extern "C" void ChatPeer__ctor_m7_66 (ChatPeer_t7_3 * __this, Object_t * ___listener, uint8_t ___protocol, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral2971 = il2cpp_codegen_string_literal_from_index(2971);
		_stringLiteral2972 = il2cpp_codegen_string_literal_from_index(2972);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___listener;
		uint8_t L_1 = ___protocol;
		PhotonPeer__ctor_m5_235(__this, L_0, L_1, /*hidden argument*/NULL);
		uint8_t L_2 = ___protocol;
		if ((((int32_t)L_2) == ((int32_t)4)))
		{
			goto IL_0016;
		}
	}
	{
		uint8_t L_3 = ___protocol;
		if ((!(((uint32_t)L_3) == ((uint32_t)5))))
		{
			goto IL_0030;
		}
	}

IL_0016:
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral2971, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = il2cpp_codegen_get_type((methodPointerType)&Type_GetType_m1_889, _stringLiteral2972, "Assembly-CSharp-firstpass, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		PhotonPeer_set_SocketImplementation_m5_197(__this, L_4, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ChatPeer::.cctor()
extern TypeInfo* Dictionary_2_t1_933_il2cpp_TypeInfo_var;
extern TypeInfo* ChatPeer_t7_3_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5621_MethodInfo_var;
extern "C" void ChatPeer__cctor_m7_67 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_933_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1057);
		ChatPeer_t7_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1051);
		Dictionary_2__ctor_m1_5621_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483833);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_933 * V_0 = {0};
	{
		Dictionary_2_t1_933 * L_0 = (Dictionary_2_t1_933 *)il2cpp_codegen_object_new (Dictionary_2_t1_933_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5621(L_0, /*hidden argument*/Dictionary_2__ctor_m1_5621_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t1_933 * L_1 = V_0;
		NullCheck(L_1);
		VirtActionInvoker2< uint8_t, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::Add(!0,!1) */, L_1, 0, ((int32_t)5058));
		Dictionary_2_t1_933 * L_2 = V_0;
		NullCheck(L_2);
		VirtActionInvoker2< uint8_t, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::Add(!0,!1) */, L_2, 1, ((int32_t)4533));
		Dictionary_2_t1_933 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker2< uint8_t, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::Add(!0,!1) */, L_3, 4, ((int32_t)9093));
		Dictionary_2_t1_933 * L_4 = V_0;
		NullCheck(L_4);
		VirtActionInvoker2< uint8_t, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::Add(!0,!1) */, L_4, 5, ((int32_t)19093));
		Dictionary_2_t1_933 * L_5 = V_0;
		((ChatPeer_t7_3_StaticFields*)ChatPeer_t7_3_il2cpp_TypeInfo_var->static_fields)->___ProtocolToNameServerPort_7 = L_5;
		return;
	}
}
// System.String ExitGames.Client.Photon.Chat.ChatPeer::get_NameServerAddress()
extern "C" String_t* ChatPeer_get_NameServerAddress_m7_68 (ChatPeer_t7_3 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = ChatPeer_GetNameServerAddress_m7_70(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatPeer::get_IsProtocolSecure()
extern "C" bool ChatPeer_get_IsProtocolSecure_m7_69 (ChatPeer_t7_3 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = PhotonPeer_get_UsedProtocol_m5_228(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)5))? 1 : 0);
	}
}
// System.String ExitGames.Client.Photon.Chat.ChatPeer::GetNameServerAddress()
extern TypeInfo* ChatPeer_t7_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2973;
extern Il2CppCodeGenString* _stringLiteral2974;
extern Il2CppCodeGenString* _stringLiteral2975;
extern Il2CppCodeGenString* _stringLiteral2976;
extern "C" String_t* ChatPeer_GetNameServerAddress_m7_70 (ChatPeer_t7_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChatPeer_t7_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1051);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral2973 = il2cpp_codegen_string_literal_from_index(2973);
		_stringLiteral2974 = il2cpp_codegen_string_literal_from_index(2974);
		_stringLiteral2975 = il2cpp_codegen_string_literal_from_index(2975);
		_stringLiteral2976 = il2cpp_codegen_string_literal_from_index(2976);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = {0};
	int32_t V_1 = 0;
	String_t* V_2 = {0};
	{
		uint8_t L_0 = PhotonPeer_get_UsedProtocol_m5_228(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(ChatPeer_t7_3_il2cpp_TypeInfo_var);
		Dictionary_2_t1_933 * L_1 = ((ChatPeer_t7_3_StaticFields*)ChatPeer_t7_3_il2cpp_TypeInfo_var->static_fields)->___ProtocolToNameServerPort_7;
		uint8_t L_2 = V_0;
		NullCheck(L_1);
		VirtFuncInvoker2< bool, uint8_t, int32_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::TryGetValue(!0,!1&) */, L_1, L_2, (&V_1));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_2 = L_3;
		uint8_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)4))))
		{
			goto IL_002f;
		}
	}
	{
		V_2 = _stringLiteral2973;
		goto IL_003c;
	}

IL_002f:
	{
		uint8_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)5))))
		{
			goto IL_003c;
		}
	}
	{
		V_2 = _stringLiteral2974;
	}

IL_003c:
	{
		String_t* L_6 = V_2;
		int32_t L_7 = V_1;
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Format_m1_412(NULL /*static, unused*/, _stringLiteral2975, L_6, _stringLiteral2976, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatPeer::Connect()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IPhotonPeerListener_t5_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2977;
extern Il2CppCodeGenString* _stringLiteral2978;
extern "C" bool ChatPeer_Connect_m7_71 (ChatPeer_t7_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		IPhotonPeerListener_t5_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(854);
		_stringLiteral2977 = il2cpp_codegen_string_literal_from_index(2977);
		_stringLiteral2978 = il2cpp_codegen_string_literal_from_index(2978);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = PhotonPeer_get_Listener_m5_200(__this, /*hidden argument*/NULL);
		String_t* L_1 = ChatPeer_get_NameServerAddress_m7_68(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1_418(NULL /*static, unused*/, _stringLiteral2977, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.IPhotonPeerListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IPhotonPeerListener_t5_17_il2cpp_TypeInfo_var, L_0, 3, L_2);
		String_t* L_3 = ChatPeer_get_NameServerAddress_m7_68(__this, /*hidden argument*/NULL);
		bool L_4 = (bool)VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(6 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::Connect(System.String,System.String) */, __this, L_3, _stringLiteral2978);
		return L_4;
	}
}
// System.Boolean ExitGames.Client.Photon.Chat.ChatPeer::AuthenticateOnNameServer(System.String,System.String,System.String,ExitGames.Client.Photon.Chat.AuthenticationValues)
extern TypeInfo* IPhotonPeerListener_t5_17_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_888_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5588_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2979;
extern "C" bool ChatPeer_AuthenticateOnNameServer_m7_72 (ChatPeer_t7_3 * __this, String_t* ___appId, String_t* ___appVersion, String_t* ___region, AuthenticationValues_t7_4 * ___authValues, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IPhotonPeerListener_t5_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(854);
		Dictionary_2_t1_888_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(857);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Dictionary_2__ctor_m1_5588_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483791);
		_stringLiteral2979 = il2cpp_codegen_string_literal_from_index(2979);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_888 * V_0 = {0};
	{
		uint8_t L_0 = PhotonPeer_get_DebugOut_m5_199(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) < ((int32_t)3)))
		{
			goto IL_001d;
		}
	}
	{
		Object_t * L_1 = PhotonPeer_get_Listener_m5_200(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		InterfaceActionInvoker2< uint8_t, String_t* >::Invoke(0 /* System.Void ExitGames.Client.Photon.IPhotonPeerListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String) */, IPhotonPeerListener_t5_17_il2cpp_TypeInfo_var, L_1, 3, _stringLiteral2979);
	}

IL_001d:
	{
		Dictionary_2_t1_888 * L_2 = (Dictionary_2_t1_888 *)il2cpp_codegen_object_new (Dictionary_2_t1_888_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5588(L_2, /*hidden argument*/Dictionary_2__ctor_m1_5588_MethodInfo_var);
		V_0 = L_2;
		Dictionary_2_t1_888 * L_3 = V_0;
		String_t* L_4 = ___appVersion;
		NullCheck(L_3);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::set_Item(!0,!1) */, L_3, ((int32_t)220), L_4);
		Dictionary_2_t1_888 * L_5 = V_0;
		String_t* L_6 = ___appId;
		NullCheck(L_5);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::set_Item(!0,!1) */, L_5, ((int32_t)224), L_6);
		Dictionary_2_t1_888 * L_7 = V_0;
		String_t* L_8 = ___region;
		NullCheck(L_7);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::set_Item(!0,!1) */, L_7, ((int32_t)210), L_8);
		AuthenticationValues_t7_4 * L_9 = ___authValues;
		if (!L_9)
		{
			goto IL_0109;
		}
	}
	{
		AuthenticationValues_t7_4 * L_10 = ___authValues;
		NullCheck(L_10);
		String_t* L_11 = AuthenticationValues_get_UserId_m7_83(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0071;
		}
	}
	{
		Dictionary_2_t1_888 * L_13 = V_0;
		AuthenticationValues_t7_4 * L_14 = ___authValues;
		NullCheck(L_14);
		String_t* L_15 = AuthenticationValues_get_UserId_m7_83(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::set_Item(!0,!1) */, L_13, ((int32_t)225), L_15);
	}

IL_0071:
	{
		AuthenticationValues_t7_4 * L_16 = ___authValues;
		if (!L_16)
		{
			goto IL_0109;
		}
	}
	{
		AuthenticationValues_t7_4 * L_17 = ___authValues;
		NullCheck(L_17);
		uint8_t L_18 = AuthenticationValues_get_AuthType_m7_75(L_17, /*hidden argument*/NULL);
		if ((((int32_t)L_18) == ((int32_t)((int32_t)255))))
		{
			goto IL_0109;
		}
	}
	{
		Dictionary_2_t1_888 * L_19 = V_0;
		AuthenticationValues_t7_4 * L_20 = ___authValues;
		NullCheck(L_20);
		uint8_t L_21 = AuthenticationValues_get_AuthType_m7_75(L_20, /*hidden argument*/NULL);
		uint8_t L_22 = L_21;
		Object_t * L_23 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_19);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::set_Item(!0,!1) */, L_19, ((int32_t)217), L_23);
		AuthenticationValues_t7_4 * L_24 = ___authValues;
		NullCheck(L_24);
		String_t* L_25 = AuthenticationValues_get_Token_m7_81(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_26 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_00c8;
		}
	}
	{
		Dictionary_2_t1_888 * L_27 = V_0;
		AuthenticationValues_t7_4 * L_28 = ___authValues;
		NullCheck(L_28);
		String_t* L_29 = AuthenticationValues_get_Token_m7_81(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::set_Item(!0,!1) */, L_27, ((int32_t)221), L_29);
		goto IL_0109;
	}

IL_00c8:
	{
		AuthenticationValues_t7_4 * L_30 = ___authValues;
		NullCheck(L_30);
		String_t* L_31 = AuthenticationValues_get_AuthGetParameters_m7_77(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_32 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_00eb;
		}
	}
	{
		Dictionary_2_t1_888 * L_33 = V_0;
		AuthenticationValues_t7_4 * L_34 = ___authValues;
		NullCheck(L_34);
		String_t* L_35 = AuthenticationValues_get_AuthGetParameters_m7_77(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::set_Item(!0,!1) */, L_33, ((int32_t)216), L_35);
	}

IL_00eb:
	{
		AuthenticationValues_t7_4 * L_36 = ___authValues;
		NullCheck(L_36);
		Object_t * L_37 = AuthenticationValues_get_AuthPostData_m7_79(L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0109;
		}
	}
	{
		Dictionary_2_t1_888 * L_38 = V_0;
		AuthenticationValues_t7_4 * L_39 = ___authValues;
		NullCheck(L_39);
		Object_t * L_40 = AuthenticationValues_get_AuthPostData_m7_79(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		VirtActionInvoker2< uint8_t, Object_t * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::set_Item(!0,!1) */, L_38, ((int32_t)214), L_40);
	}

IL_0109:
	{
		Dictionary_2_t1_888 * L_41 = V_0;
		bool L_42 = PhotonPeer_get_IsEncryptionAvailable_m5_232(__this, /*hidden argument*/NULL);
		bool L_43 = (bool)VirtFuncInvoker5< bool, uint8_t, Dictionary_2_t1_888 *, bool, uint8_t, bool >::Invoke(16 /* System.Boolean ExitGames.Client.Photon.PhotonPeer::OpCustom(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean,System.Byte,System.Boolean) */, __this, ((int32_t)230), L_41, 1, 0, L_42);
		return L_43;
	}
}
// System.Void ExitGames.Client.Photon.Chat.AuthenticationValues::.ctor()
extern "C" void AuthenticationValues__ctor_m7_73 (AuthenticationValues_t7_4 * __this, const MethodInfo* method)
{
	{
		__this->___authType_0 = ((int32_t)255);
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.AuthenticationValues::.ctor(System.String)
extern "C" void AuthenticationValues__ctor_m7_74 (AuthenticationValues_t7_4 * __this, String_t* ___userId, const MethodInfo* method)
{
	{
		__this->___authType_0 = ((int32_t)255);
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___userId;
		AuthenticationValues_set_UserId_m7_84(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// ExitGames.Client.Photon.Chat.CustomAuthenticationType ExitGames.Client.Photon.Chat.AuthenticationValues::get_AuthType()
extern "C" uint8_t AuthenticationValues_get_AuthType_m7_75 (AuthenticationValues_t7_4 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (__this->___authType_0);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.AuthenticationValues::set_AuthType(ExitGames.Client.Photon.Chat.CustomAuthenticationType)
extern "C" void AuthenticationValues_set_AuthType_m7_76 (AuthenticationValues_t7_4 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___authType_0 = L_0;
		return;
	}
}
// System.String ExitGames.Client.Photon.Chat.AuthenticationValues::get_AuthGetParameters()
extern "C" String_t* AuthenticationValues_get_AuthGetParameters_m7_77 (AuthenticationValues_t7_4 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CAuthGetParametersU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.AuthenticationValues::set_AuthGetParameters(System.String)
extern "C" void AuthenticationValues_set_AuthGetParameters_m7_78 (AuthenticationValues_t7_4 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CAuthGetParametersU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Object ExitGames.Client.Photon.Chat.AuthenticationValues::get_AuthPostData()
extern "C" Object_t * AuthenticationValues_get_AuthPostData_m7_79 (AuthenticationValues_t7_4 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CAuthPostDataU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.AuthenticationValues::set_AuthPostData(System.Object)
extern "C" void AuthenticationValues_set_AuthPostData_m7_80 (AuthenticationValues_t7_4 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___U3CAuthPostDataU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.String ExitGames.Client.Photon.Chat.AuthenticationValues::get_Token()
extern "C" String_t* AuthenticationValues_get_Token_m7_81 (AuthenticationValues_t7_4 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CTokenU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.AuthenticationValues::set_Token(System.String)
extern "C" void AuthenticationValues_set_Token_m7_82 (AuthenticationValues_t7_4 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CTokenU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.String ExitGames.Client.Photon.Chat.AuthenticationValues::get_UserId()
extern "C" String_t* AuthenticationValues_get_UserId_m7_83 (AuthenticationValues_t7_4 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CUserIdU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void ExitGames.Client.Photon.Chat.AuthenticationValues::set_UserId(System.String)
extern "C" void AuthenticationValues_set_UserId_m7_84 (AuthenticationValues_t7_4 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CUserIdU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.AuthenticationValues::SetAuthPostData(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void AuthenticationValues_SetAuthPostData_m7_85 (AuthenticationValues_t7_4 * __this, String_t* ___stringData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	AuthenticationValues_t7_4 * G_B2_0 = {0};
	AuthenticationValues_t7_4 * G_B1_0 = {0};
	String_t* G_B3_0 = {0};
	AuthenticationValues_t7_4 * G_B3_1 = {0};
	{
		String_t* L_0 = ___stringData;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_1)
		{
			G_B2_0 = __this;
			goto IL_0012;
		}
	}
	{
		G_B3_0 = ((String_t*)(NULL));
		G_B3_1 = G_B1_0;
		goto IL_0013;
	}

IL_0012:
	{
		String_t* L_2 = ___stringData;
		G_B3_0 = L_2;
		G_B3_1 = G_B2_0;
	}

IL_0013:
	{
		NullCheck(G_B3_1);
		AuthenticationValues_set_AuthPostData_m7_80(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.AuthenticationValues::SetAuthPostData(System.Byte[])
extern "C" void AuthenticationValues_SetAuthPostData_m7_86 (AuthenticationValues_t7_4 * __this, ByteU5BU5D_t1_71* ___byteData, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_71* L_0 = ___byteData;
		AuthenticationValues_set_AuthPostData_m7_80(__this, (Object_t *)(Object_t *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.AuthenticationValues::AddAuthParameter(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t3_41_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1672;
extern Il2CppCodeGenString* _stringLiteral2980;
extern "C" void AuthenticationValues_AddAuthParameter_m7_87 (AuthenticationValues_t7_4 * __this, String_t* ___key, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Uri_t3_41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(643);
		_stringLiteral1672 = il2cpp_codegen_string_literal_from_index(1672);
		_stringLiteral2980 = il2cpp_codegen_string_literal_from_index(2980);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* G_B3_0 = {0};
	{
		String_t* L_0 = AuthenticationValues_get_AuthGetParameters_m7_77(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B3_0 = L_2;
		goto IL_001f;
	}

IL_001a:
	{
		G_B3_0 = _stringLiteral1672;
	}

IL_001f:
	{
		V_0 = G_B3_0;
		ObjectU5BU5D_t1_157* L_3 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		String_t* L_4 = AuthenticationValues_get_AuthGetParameters_m7_77(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 0, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t1_157* L_5 = L_3;
		String_t* L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t1_157* L_7 = L_5;
		String_t* L_8 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_41_il2cpp_TypeInfo_var);
		String_t* L_9 = Uri_EscapeDataString_m3_995(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_157* L_10 = L_7;
		String_t* L_11 = ___value;
		String_t* L_12 = Uri_EscapeDataString_m3_995(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 3, sizeof(Object_t *))) = (Object_t *)L_12;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Format_m1_413(NULL /*static, unused*/, _stringLiteral2980, L_10, /*hidden argument*/NULL);
		AuthenticationValues_set_AuthGetParameters_m7_78(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.String ExitGames.Client.Photon.Chat.AuthenticationValues::ToString()
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2981;
extern "C" String_t* AuthenticationValues_ToString_m7_88 (AuthenticationValues_t7_4 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2981 = il2cpp_codegen_string_literal_from_index(2981);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = AuthenticationValues_get_UserId_m7_83(__this, /*hidden argument*/NULL);
		String_t* L_1 = AuthenticationValues_get_AuthGetParameters_m7_77(__this, /*hidden argument*/NULL);
		String_t* L_2 = AuthenticationValues_get_Token_m7_81(__this, /*hidden argument*/NULL);
		bool L_3 = ((((int32_t)((((Object_t*)(String_t*)L_2) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Object_t * L_4 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1_412(NULL /*static, unused*/, _stringLiteral2981, L_0, L_1, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ParameterCode::.ctor()
extern "C" void ParameterCode__ctor_m7_89 (ParameterCode_t7_11 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExitGames.Client.Photon.Chat.ErrorCode::.ctor()
extern "C" void ErrorCode__ctor_m7_90 (ErrorCode_t7_12 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
