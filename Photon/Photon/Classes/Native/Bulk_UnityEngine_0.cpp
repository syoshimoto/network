﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t6_1;
// UnityEngine.AssetBundle
struct AssetBundle_t6_4;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t6_3;
// UnityEngine.Object
struct Object_t6_5;
struct Object_t6_5_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t6_252;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t6_11;
struct WaitForSeconds_t6_11_marshaled;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t6_13;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t6_14;
// UnityEngine.Coroutine
struct Coroutine_t6_15;
struct Coroutine_t6_15_marshaled;
// UnityEngine.ScriptableObject
struct ScriptableObject_t6_16;
struct ScriptableObject_t6_16_marshaled;
// UnityEngine.UnhandledExceptionHandler
struct UnhandledExceptionHandler_t6_17;
// System.Object
struct Object_t;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t1_772;
// System.Exception
struct Exception_t1_33;
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
struct GameCenterPlatform_t6_18;
// UnityEngine.SocialPlatforms.ILocalUser
struct ILocalUser_t6_255;
// System.Action`1<System.Boolean>
struct Action_1_t1_905;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.String[]
struct StringU5BU5D_t1_202;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t6_253;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t6_254;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
struct Action_1_t1_906;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t1_907;
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t1_908;
// UnityEngine.SocialPlatforms.ILeaderboard
struct ILeaderboard_t6_256;
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t1_909;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t6_20;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t6_257;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t6_22;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_t6_23;
// UnityEngine.Renderer
struct Renderer_t6_25;
// UnityEngine.Material
struct Material_t6_67;
// UnityEngine.GUILayer
struct GUILayer_t6_31;
// UnityEngine.GUIElement
struct GUIElement_t6_29;
// UnityEngine.Texture
struct Texture_t6_32;
// UnityEngine.RenderTexture
struct RenderTexture_t6_34;
// UnityEngine.CullingGroup/StateChanged
struct StateChanged_t6_37;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// UnityEngine.CullingGroup
struct CullingGroup_t6_38;
// UnityEngine.Gradient
struct Gradient_t6_42;
struct Gradient_t6_42_marshaled;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t6_45;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t6_59;
// UnityEngine.RectTransform
struct RectTransform_t6_60;
// UnityEngine.ResourceRequest
struct ResourceRequest_t6_62;
// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t6_64;
// UnityEngine.SerializeField
struct SerializeField_t6_65;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// UnityEngine.AsyncOperation
struct AsyncOperation_t6_2;
struct AsyncOperation_t6_2_marshaled;
// UnityEngine.Application/LogCallback
struct LogCallback_t6_72;
// UnityEngine.Behaviour
struct Behaviour_t6_30;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t6_74;
// UnityEngine.Camera
struct Camera_t6_75;
// UnityEngine.Camera[]
struct CameraU5BU5D_t6_224;
// UnityEngine.GameObject
struct GameObject_t6_85;
// UnityEngine.Display/DisplaysUpdatedDelegate
struct DisplaysUpdatedDelegate_t6_77;
// UnityEngine.Display
struct Display_t6_78;
// System.IntPtr[]
struct IntPtrU5BU5D_t1_34;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t6_80;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// UnityEngine.Component
struct Component_t6_26;
// UnityEngine.Transform
struct Transform_t6_61;
// System.Array
struct Array_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t6_258;
// UnityEngine.Transform/Enumerator
struct Enumerator_t6_86;
// UnityEngine.YieldInstruction
struct YieldInstruction_t6_12;
struct YieldInstruction_t6_12_marshaled;
// UnityEngine.PlayerPrefsException
struct PlayerPrefsException_t6_89;
// UnityEngine.Advertisements.UnityAdsInternal
struct UnityAdsInternal_t6_93;
// UnityEngine.Advertisements.UnityAdsDelegate
struct UnityAdsDelegate_t6_94;
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>
struct UnityAdsDelegate_2_t6_95;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t6_98;
// UnityEngine.Rigidbody
struct Rigidbody_t6_102;
// UnityEngine.Collider
struct Collider_t6_100;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_U3CModuleU3E.h"
#include "UnityEngine_U3CModuleU3EMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequestMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "UnityEngine_UnityEngine_AsyncOperationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation.h"
#include "UnityEngine_UnityEngine_AssetBundle.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest.h"
#include "UnityEngine_UnityEngine_AssetBundleRequestMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_AssetBundleMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Type.h"
#include "UnityEngine_ArrayTypes.h"
#include "mscorlib_System_NullReferenceExceptionMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_NullReferenceException.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_ArgumentException.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_SendMessageOptionsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Space.h"
#include "UnityEngine_UnityEngine_SpaceMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "UnityEngine_UnityEngine_RuntimePlatformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType.h"
#include "UnityEngine_UnityEngine_LogTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemInfo.h"
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_YieldInstructionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstruction.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrameMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "UnityEngine_UnityEngine_CoroutineMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandlerMethodDeclarations.h"
#include "mscorlib_System_AppDomainMethodDeclarations.h"
#include "mscorlib_System_UnhandledExceptionEventHandlerMethodDeclarations.h"
#include "mscorlib_System_AppDomain.h"
#include "mscorlib_System_UnhandledExceptionEventArgs.h"
#include "mscorlib_System_UnhandledExceptionEventHandler.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_UnhandledExceptionEventArgsMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCente.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCenteMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_4.h"
#include "mscorlib_System_Action_1_gen.h"
#include "mscorlib_System_Boolean.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_0MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_0.h"
#include "mscorlib_System_Action_1_genMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_1MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
#include "mscorlib_System_Action_1_gen_1.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
#include "mscorlib_System_Action_1_gen_2.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderbMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderb.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1.h"
#include "mscorlib_System_Action_1_gen_3MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_3.h"
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "UnityEngine_UnityEngine_BoneWeight.h"
#include "UnityEngine_UnityEngine_BoneWeightMethodDeclarations.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_Renderer.h"
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "UnityEngine_UnityEngine_Screen.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer.h"
#include "UnityEngine_UnityEngine_MeshRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIElement.h"
#include "UnityEngine_UnityEngine_GUIElementMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer.h"
#include "UnityEngine_UnityEngine_GUILayerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
#include "mscorlib_System_IntPtrMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureFormat.h"
#include "UnityEngine_UnityEngine_RenderTexture.h"
#include "UnityEngine_UnityEngine_RenderTextureMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ReflectionProbe.h"
#include "UnityEngine_UnityEngine_ReflectionProbeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent.h"
#include "UnityEngine_UnityEngine_CullingGroupEventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChangedMethodDeclarations.h"
#include "mscorlib_System_AsyncCallback.h"
#include "UnityEngine_UnityEngine_CullingGroup.h"
#include "UnityEngine_UnityEngine_CullingGroupMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GradientColorKey.h"
#include "UnityEngine_UnityEngine_GradientColorKeyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_GradientAlphaKey.h"
#include "UnityEngine_UnityEngine_GradientAlphaKeyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gradient.h"
#include "UnityEngine_UnityEngine_GradientMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstrucMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardMethodDeclarations.h"
#include "mscorlib_System_ConvertMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gizmos.h"
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
#include "UnityEngine_UnityEngine_LayerMask.h"
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
#include "mscorlib_System_Byte.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeExceptionMethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException.h"
#include "UnityEngine_UnityEngine_Bounds.h"
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal.h"
#include "UnityEngine_UnityEngineInternal_MathfInternalMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf.h"
#include "mscorlib_System_MathMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertie.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertieMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform.h"
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourceRequest.h"
#include "UnityEngine_UnityEngine_ResourceRequestMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariablesMethodDeclarations.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "UnityEngine_UnityEngine_SerializeField.h"
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader.h"
#include "UnityEngine_UnityEngine_ShaderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer.h"
#include "UnityEngine_UnityEngine_SpriteRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CacheIndex.h"
#include "UnityEngine_UnityEngine_CacheIndexMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityString.h"
#include "UnityEngine_UnityEngine_Application_LogCallback.h"
#include "UnityEngine_UnityEngine_Application_LogCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application.h"
#include "UnityEngine_UnityEngine_Behaviour.h"
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction.h"
#include "UnityEngine_UnityEngine_Debug.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegate.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Display.h"
#include "UnityEngine_UnityEngine_DisplayMethodDeclarations.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "mscorlib_System_Delegate.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase.h"
#include "UnityEngine_UnityEngine_TouchPhaseMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "UnityEngine_UnityEngine_HideFlags.h"
#include "UnityEngine_UnityEngine_HideFlagsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator.h"
#include "UnityEngine_UnityEngine_Transform_EnumeratorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time.h"
#include "UnityEngine_UnityEngine_Random.h"
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException.h"
#include "UnityEngine_UnityEngine_PlayerPrefsExceptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs.h"
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Motion.h"
#include "UnityEngine_UnityEngine_MotionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_DirectorPlayer.h"
#include "UnityEngine_UnityEngine_Experimental_Director_DirectorPlayerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsInternal.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsInternalMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate_2_ge.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate_2_geMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Particle.h"
#include "UnityEngine_UnityEngine_ParticleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ForceMode.h"
#include "UnityEngine_UnityEngine_ForceModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit.h"
#include "UnityEngine_UnityEngine_ControllerColliderHitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision.h"
#include "UnityEngine_UnityEngine_CollisionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CollisionFlags.h"
#include "UnityEngine_UnityEngine_CollisionFlagsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteractionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics.h"
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "UnityEngine_UnityEngine_ContactPoint.h"
#include "UnityEngine_UnityEngine_ContactPointMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody.h"
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider.h"
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C" void AssetBundleCreateRequest__ctor_m6_0 (AssetBundleCreateRequest_t6_1 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m6_438(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C" AssetBundle_t6_4 * AssetBundleCreateRequest_get_assetBundle_m6_1 (AssetBundleCreateRequest_t6_1 * __this, const MethodInfo* method)
{
	typedef AssetBundle_t6_4 * (*AssetBundleCreateRequest_get_assetBundle_m6_1_ftn) (AssetBundleCreateRequest_t6_1 *);
	static AssetBundleCreateRequest_get_assetBundle_m6_1_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_get_assetBundle_m6_1_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::get_assetBundle()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m6_2 (AssetBundleCreateRequest_t6_1 * __this, const MethodInfo* method)
{
	typedef void (*AssetBundleCreateRequest_DisableCompatibilityChecks_m6_2_ftn) (AssetBundleCreateRequest_t6_1 *);
	static AssetBundleCreateRequest_DisableCompatibilityChecks_m6_2_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_DisableCompatibilityChecks_m6_2_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m6_3 (AssetBundleRequest_t6_3 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m6_438(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t6_5 * AssetBundleRequest_get_asset_m6_4 (AssetBundleRequest_t6_3 * __this, const MethodInfo* method)
{
	{
		AssetBundle_t6_4 * L_0 = (__this->___m_AssetBundle_1);
		String_t* L_1 = (__this->___m_Path_2);
		Type_t * L_2 = (__this->___m_Type_3);
		NullCheck(L_0);
		Object_t6_5 * L_3 = AssetBundle_LoadAsset_m6_6(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C" ObjectU5BU5D_t6_252* AssetBundleRequest_get_allAssets_m6_5 (AssetBundleRequest_t6_3 * __this, const MethodInfo* method)
{
	{
		AssetBundle_t6_4 * L_0 = (__this->___m_AssetBundle_1);
		String_t* L_1 = (__this->___m_Path_2);
		Type_t * L_2 = (__this->___m_Type_3);
		NullCheck(L_0);
		ObjectU5BU5D_t6_252* L_3 = AssetBundle_LoadAssetWithSubAssets_Internal_m6_8(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String,System.Type)
extern TypeInfo* NullReferenceException_t1_750_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_646_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2681;
extern Il2CppCodeGenString* _stringLiteral2682;
extern Il2CppCodeGenString* _stringLiteral2683;
extern "C" Object_t6_5 * AssetBundle_LoadAsset_m6_6 (AssetBundle_t6_4 * __this, String_t* ___name, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t1_750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		ArgumentException_t1_646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral2681 = il2cpp_codegen_string_literal_from_index(2681);
		_stringLiteral2682 = il2cpp_codegen_string_literal_from_index(2682);
		_stringLiteral2683 = il2cpp_codegen_string_literal_from_index(2683);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullReferenceException_t1_750 * L_1 = (NullReferenceException_t1_750 *)il2cpp_codegen_object_new (NullReferenceException_t1_750_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m1_5209(L_1, _stringLiteral2681, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___name;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_428(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t1_646 * L_4 = (ArgumentException_t1_646 *)il2cpp_codegen_object_new (ArgumentException_t1_646_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_4605(L_4, _stringLiteral2682, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0027:
	{
		Type_t * L_5 = ___type;
		if (L_5)
		{
			goto IL_0038;
		}
	}
	{
		NullReferenceException_t1_750 * L_6 = (NullReferenceException_t1_750 *)il2cpp_codegen_object_new (NullReferenceException_t1_750_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m1_5209(L_6, _stringLiteral2683, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_0038:
	{
		String_t* L_7 = ___name;
		Type_t * L_8 = ___type;
		Object_t6_5 * L_9 = AssetBundle_LoadAsset_Internal_m6_7(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Object UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)
extern "C" Object_t6_5 * AssetBundle_LoadAsset_Internal_m6_7 (AssetBundle_t6_4 * __this, String_t* ___name, Type_t * ___type, const MethodInfo* method)
{
	typedef Object_t6_5 * (*AssetBundle_LoadAsset_Internal_m6_7_ftn) (AssetBundle_t6_4 *, String_t*, Type_t *);
	static AssetBundle_LoadAsset_Internal_m6_7_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundle_LoadAsset_Internal_m6_7_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)");
	return _il2cpp_icall_func(__this, ___name, ___type);
}
// UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)
extern "C" ObjectU5BU5D_t6_252* AssetBundle_LoadAssetWithSubAssets_Internal_m6_8 (AssetBundle_t6_4 * __this, String_t* ___name, Type_t * ___type, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t6_252* (*AssetBundle_LoadAssetWithSubAssets_Internal_m6_8_ftn) (AssetBundle_t6_4 *, String_t*, Type_t *);
	static AssetBundle_LoadAssetWithSubAssets_Internal_m6_8_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundle_LoadAssetWithSubAssets_Internal_m6_8_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)");
	return _il2cpp_icall_func(__this, ___name, ___type);
}
// System.String UnityEngine.SystemInfo::get_operatingSystem()
extern "C" String_t* SystemInfo_get_operatingSystem_m6_9 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_operatingSystem_m6_9_ftn) ();
	static SystemInfo_get_operatingSystem_m6_9_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_operatingSystem_m6_9_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_operatingSystem()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" void WaitForSeconds__ctor_m6_10 (WaitForSeconds_t6_11 * __this, float ___seconds, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m6_662(__this, /*hidden argument*/NULL);
		float L_0 = ___seconds;
		__this->___m_Seconds_0 = L_0;
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t6_11_marshal(const WaitForSeconds_t6_11& unmarshaled, WaitForSeconds_t6_11_marshaled& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.___m_Seconds_0;
}
extern "C" void WaitForSeconds_t6_11_marshal_back(const WaitForSeconds_t6_11_marshaled& marshaled, WaitForSeconds_t6_11& unmarshaled)
{
	unmarshaled.___m_Seconds_0 = marshaled.___m_Seconds_0;
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t6_11_marshal_cleanup(WaitForSeconds_t6_11_marshaled& marshaled)
{
}
// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C" void WaitForFixedUpdate__ctor_m6_11 (WaitForFixedUpdate_t6_13 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m6_662(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C" void WaitForEndOfFrame__ctor_m6_12 (WaitForEndOfFrame_t6_14 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m6_662(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::.ctor()
extern "C" void Coroutine__ctor_m6_13 (Coroutine_t6_15 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m6_662(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C" void Coroutine_ReleaseCoroutine_m6_14 (Coroutine_t6_15 * __this, const MethodInfo* method)
{
	typedef void (*Coroutine_ReleaseCoroutine_m6_14_ftn) (Coroutine_t6_15 *);
	static Coroutine_ReleaseCoroutine_m6_14_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Coroutine_ReleaseCoroutine_m6_14_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Coroutine::ReleaseCoroutine()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Coroutine::Finalize()
extern "C" void Coroutine_Finalize_m6_15 (Coroutine_t6_15 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Coroutine_ReleaseCoroutine_m6_14(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t6_15_marshal(const Coroutine_t6_15& unmarshaled, Coroutine_t6_15_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void Coroutine_t6_15_marshal_back(const Coroutine_t6_15_marshaled& marshaled, Coroutine_t6_15& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t6_15_marshal_cleanup(Coroutine_t6_15_marshaled& marshaled)
{
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C" void ScriptableObject__ctor_m6_16 (ScriptableObject_t6_16 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m6_553(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m6_17(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C" void ScriptableObject_Internal_CreateScriptableObject_m6_17 (Object_t * __this /* static, unused */, ScriptableObject_t6_16 * ___self, const MethodInfo* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m6_17_ftn) (ScriptableObject_t6_16 *);
	static ScriptableObject_Internal_CreateScriptableObject_m6_17_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m6_17_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C" ScriptableObject_t6_16 * ScriptableObject_CreateInstance_m6_18 (Object_t * __this /* static, unused */, String_t* ___className, const MethodInfo* method)
{
	typedef ScriptableObject_t6_16 * (*ScriptableObject_CreateInstance_m6_18_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m6_18_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m6_18_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	return _il2cpp_icall_func(___className);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C" ScriptableObject_t6_16 * ScriptableObject_CreateInstance_m6_19 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		ScriptableObject_t6_16 * L_1 = ScriptableObject_CreateInstanceFromType_m6_20(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C" ScriptableObject_t6_16 * ScriptableObject_CreateInstanceFromType_m6_20 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	typedef ScriptableObject_t6_16 * (*ScriptableObject_CreateInstanceFromType_m6_20_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m6_20_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m6_20_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	return _il2cpp_icall_func(___type);
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t6_16_marshal(const ScriptableObject_t6_16& unmarshaled, ScriptableObject_t6_16_marshaled& marshaled)
{
}
extern "C" void ScriptableObject_t6_16_marshal_back(const ScriptableObject_t6_16_marshaled& marshaled, ScriptableObject_t6_16& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t6_16_marshal_cleanup(ScriptableObject_t6_16_marshaled& marshaled)
{
}
// System.Void UnityEngine.UnhandledExceptionHandler::.ctor()
extern "C" void UnhandledExceptionHandler__ctor_m6_21 (UnhandledExceptionHandler_t6_17 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern TypeInfo* UnhandledExceptionEventHandler_t1_689_il2cpp_TypeInfo_var;
extern const MethodInfo* UnhandledExceptionHandler_HandleUnhandledException_m6_23_MethodInfo_var;
extern "C" void UnhandledExceptionHandler_RegisterUECatcher_m6_22 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnhandledExceptionEventHandler_t1_689_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(570);
		UnhandledExceptionHandler_HandleUnhandledException_m6_23_MethodInfo_var = il2cpp_codegen_method_info_from_index(146);
		s_Il2CppMethodIntialized = true;
	}
	{
		AppDomain_t1_634 * L_0 = AppDomain_get_CurrentDomain_m1_4587(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = { (void*)UnhandledExceptionHandler_HandleUnhandledException_m6_23_MethodInfo_var };
		UnhandledExceptionEventHandler_t1_689 * L_2 = (UnhandledExceptionEventHandler_t1_689 *)il2cpp_codegen_object_new (UnhandledExceptionEventHandler_t1_689_il2cpp_TypeInfo_var);
		UnhandledExceptionEventHandler__ctor_m1_5515(L_2, NULL, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		AppDomain_add_UnhandledException_m1_4583(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::HandleUnhandledException(System.Object,System.UnhandledExceptionEventArgs)
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2684;
extern "C" void UnhandledExceptionHandler_HandleUnhandledException_m6_23 (Object_t * __this /* static, unused */, Object_t * ___sender, UnhandledExceptionEventArgs_t1_772 * ___args, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		_stringLiteral2684 = il2cpp_codegen_string_literal_from_index(2684);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * V_0 = {0};
	{
		UnhandledExceptionEventArgs_t1_772 * L_0 = ___args;
		NullCheck(L_0);
		Object_t * L_1 = UnhandledExceptionEventArgs_get_ExceptionObject_m1_5430(L_0, /*hidden argument*/NULL);
		V_0 = ((Exception_t1_33 *)IsInstClass(L_1, Exception_t1_33_il2cpp_TypeInfo_var));
		Exception_t1_33 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Exception_t1_33 * L_3 = V_0;
		UnhandledExceptionHandler_PrintException_m6_24(NULL /*static, unused*/, _stringLiteral2684, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m6_25(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2685;
extern "C" void UnhandledExceptionHandler_PrintException_m6_24 (Object_t * __this /* static, unused */, String_t* ___title, Exception_t1_33 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2685 = il2cpp_codegen_string_literal_from_index(2685);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		Exception_t1_33 * L_1 = ___e;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_418(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		Debug_LogError_m6_490(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Exception_t1_33 * L_4 = ___e;
		NullCheck(L_4);
		Exception_t1_33 * L_5 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_4);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		Exception_t1_33 * L_6 = ___e;
		NullCheck(L_6);
		Exception_t1_33 * L_7 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_6);
		UnhandledExceptionHandler_PrintException_m6_24(NULL /*static, unused*/, _stringLiteral2685, L_7, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()
extern "C" void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m6_25 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m6_25_ftn) ();
	static UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m6_25_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m6_25_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.ctor()
extern "C" void GameCenterPlatform__ctor_m6_26 (GameCenterPlatform_t6_18 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.cctor()
extern TypeInfo* AchievementDescriptionU5BU5D_t6_19_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern TypeInfo* UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_910_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_5589_MethodInfo_var;
extern "C" void GameCenterPlatform__cctor_m6_27 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementDescriptionU5BU5D_t6_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(909);
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(912);
		List_1_t1_910_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(914);
		List_1__ctor_m1_5589_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483795);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9 = ((AchievementDescriptionU5BU5D_t6_19*)SZArrayNew(AchievementDescriptionU5BU5D_t6_19_il2cpp_TypeInfo_var, 0));
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_friends_10 = ((UserProfileU5BU5D_t6_20*)SZArrayNew(UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var, 0));
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_users_11 = ((UserProfileU5BU5D_t6_20*)SZArrayNew(UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var, 0));
		List_1_t1_910 * L_0 = (List_1_t1_910 *)il2cpp_codegen_object_new (List_1_t1_910_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5589(L_0, /*hidden argument*/List_1__ctor_m1_5589_MethodInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_GcBoards_14 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m6_28 (GameCenterPlatform_t6_18 * __this, Object_t * ___user, Action_1_t1_905 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1_905 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_FriendsCallback_1 = L_0;
		GameCenterPlatform_Internal_LoadFriends_m6_36(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m6_29 (GameCenterPlatform_t6_18 * __this, Object_t * ___user, Action_1_t1_905 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1_905 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_AuthenticateCallback_0 = L_0;
		GameCenterPlatform_Internal_Authenticate_m6_30(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
extern "C" void GameCenterPlatform_Internal_Authenticate_m6_30 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_Authenticate_m6_30_ftn) ();
	static GameCenterPlatform_Internal_Authenticate_m6_30_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticate_m6_30_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
extern "C" bool GameCenterPlatform_Internal_Authenticated_m6_31 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Authenticated_m6_31_ftn) ();
	static GameCenterPlatform_Internal_Authenticated_m6_31_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticated_m6_31_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
extern "C" String_t* GameCenterPlatform_Internal_UserName_m6_32 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserName_m6_32_ftn) ();
	static GameCenterPlatform_Internal_UserName_m6_32_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserName_m6_32_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
extern "C" String_t* GameCenterPlatform_Internal_UserID_m6_33 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserID_m6_33_ftn) ();
	static GameCenterPlatform_Internal_UserID_m6_33_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserID_m6_33_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
extern "C" bool GameCenterPlatform_Internal_Underage_m6_34 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Underage_m6_34_ftn) ();
	static GameCenterPlatform_Internal_Underage_m6_34_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Underage_m6_34_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()");
	return _il2cpp_icall_func();
}
// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
extern "C" Texture2D_t6_33 * GameCenterPlatform_Internal_UserImage_m6_35 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t6_33 * (*GameCenterPlatform_Internal_UserImage_m6_35_ftn) ();
	static GameCenterPlatform_Internal_UserImage_m6_35_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserImage_m6_35_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()
extern "C" void GameCenterPlatform_Internal_LoadFriends_m6_36 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadFriends_m6_36_ftn) ();
	static GameCenterPlatform_Internal_LoadFriends_m6_36_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadFriends_m6_36_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()
extern "C" void GameCenterPlatform_Internal_LoadAchievementDescriptions_m6_37 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievementDescriptions_m6_37_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievementDescriptions_m6_37_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievementDescriptions_m6_37_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()
extern "C" void GameCenterPlatform_Internal_LoadAchievements_m6_38 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievements_m6_38_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievements_m6_38_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievements_m6_38_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)
extern "C" void GameCenterPlatform_Internal_ReportProgress_m6_39 (Object_t * __this /* static, unused */, String_t* ___id, double ___progress, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportProgress_m6_39_ftn) (String_t*, double);
	static GameCenterPlatform_Internal_ReportProgress_m6_39_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportProgress_m6_39_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)");
	_il2cpp_icall_func(___id, ___progress);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)
extern "C" void GameCenterPlatform_Internal_ReportScore_m6_40 (Object_t * __this /* static, unused */, int64_t ___score, String_t* ___category, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportScore_m6_40_ftn) (int64_t, String_t*);
	static GameCenterPlatform_Internal_ReportScore_m6_40_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportScore_m6_40_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)");
	_il2cpp_icall_func(___score, ___category);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)
extern "C" void GameCenterPlatform_Internal_LoadScores_m6_41 (Object_t * __this /* static, unused */, String_t* ___category, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadScores_m6_41_ftn) (String_t*);
	static GameCenterPlatform_Internal_LoadScores_m6_41_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadScores_m6_41_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)");
	_il2cpp_icall_func(___category);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
extern "C" void GameCenterPlatform_Internal_ShowAchievementsUI_m6_42 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowAchievementsUI_m6_42_ftn) ();
	static GameCenterPlatform_Internal_ShowAchievementsUI_m6_42_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowAchievementsUI_m6_42_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
extern "C" void GameCenterPlatform_Internal_ShowLeaderboardUI_m6_43 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowLeaderboardUI_m6_43_ftn) ();
	static GameCenterPlatform_Internal_ShowLeaderboardUI_m6_43_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowLeaderboardUI_m6_43_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])
extern "C" void GameCenterPlatform_Internal_LoadUsers_m6_44 (Object_t * __this /* static, unused */, StringU5BU5D_t1_202* ___userIds, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadUsers_m6_44_ftn) (StringU5BU5D_t1_202*);
	static GameCenterPlatform_Internal_LoadUsers_m6_44_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadUsers_m6_44_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])");
	_il2cpp_icall_func(___userIds);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
extern "C" void GameCenterPlatform_Internal_ResetAllAchievements_m6_45 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ResetAllAchievements_m6_45_ftn) ();
	static GameCenterPlatform_Internal_ResetAllAchievements_m6_45_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ResetAllAchievements_m6_45_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
extern "C" void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6_46 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6_46_ftn) (bool);
	static GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6_46_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6_46_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ResetAllAchievements(System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ResetAllAchievements_m6_47 (Object_t * __this /* static, unused */, Action_1_t1_905 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1_905 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_ResetAchievements_12 = L_0;
		GameCenterPlatform_Internal_ResetAllAchievements_m6_45(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowDefaultAchievementCompletionBanner(System.Boolean)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m6_48 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6_46(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI(System.String,UnityEngine.SocialPlatforms.TimeScope)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m6_49 (Object_t * __this /* static, unused */, String_t* ___leaderboardID, int32_t ___timeScope, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID;
		int32_t L_1 = ___timeScope;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6_50(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
extern "C" void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6_50 (Object_t * __this /* static, unused */, String_t* ___leaderboardID, int32_t ___timeScope, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6_50_ftn) (String_t*, int32_t);
	static GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6_50_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6_50_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)");
	_il2cpp_icall_func(___leaderboardID, ___timeScope);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearAchievementDescriptions(System.Int32)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern TypeInfo* AchievementDescriptionU5BU5D_t6_19_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ClearAchievementDescriptions_m6_51 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		AchievementDescriptionU5BU5D_t6_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(909);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_19* L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_19* L_1 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_1);
		int32_t L_2 = ___size;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}

IL_0017:
	{
		int32_t L_3 = ___size;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9 = ((AchievementDescriptionU5BU5D_t6_19*)SZArrayNew(AchievementDescriptionU5BU5D_t6_19_il2cpp_TypeInfo_var, L_3));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescription(UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData,System.Int32)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetAchievementDescription_m6_52 (Object_t * __this /* static, unused */, GcAchievementDescriptionData_t6_204  ___data, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_19* L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		int32_t L_1 = ___number;
		AchievementDescription_t6_216 * L_2 = GcAchievementDescriptionData_ToAchievementDescription_m6_1413((&___data), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((AchievementDescription_t6_216 **)(AchievementDescription_t6_216 **)SZArrayLdElema(L_0, L_1, sizeof(AchievementDescription_t6_216 *))) = (AchievementDescription_t6_216 *)L_2;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescriptionImage(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2686;
extern "C" void GameCenterPlatform_SetAchievementDescriptionImage_m6_53 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		_stringLiteral2686 = il2cpp_codegen_string_literal_from_index(2686);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_19* L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_0);
		int32_t L_1 = ___number;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = ___number;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001f;
		}
	}

IL_0014:
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral2686, /*hidden argument*/NULL);
		return;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_19* L_3 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		int32_t L_4 = ___number;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Texture2D_t6_33 * L_6 = ___texture;
		NullCheck((*(AchievementDescription_t6_216 **)(AchievementDescription_t6_216 **)SZArrayLdElema(L_3, L_5, sizeof(AchievementDescription_t6_216 *))));
		AchievementDescription_SetImage_m6_1453((*(AchievementDescription_t6_216 **)(AchievementDescription_t6_216 **)SZArrayLdElema(L_3, L_5, sizeof(AchievementDescription_t6_216 *))), L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerAchievementDescriptionCallback()
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5590_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687;
extern "C" void GameCenterPlatform_TriggerAchievementDescriptionCallback_m6_54 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5590_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483796);
		_stringLiteral2687 = il2cpp_codegen_string_literal_from_index(2687);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_906 * L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_AchievementDescriptionLoaderCallback_2;
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_19* L_1 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_19* L_2 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))))
		{
			goto IL_002a;
		}
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral2687, /*hidden argument*/NULL);
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_906 * L_3 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_AchievementDescriptionLoaderCallback_2;
		AchievementDescriptionU5BU5D_t6_19* L_4 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_3);
		Action_1_Invoke_m1_5590(L_3, (IAchievementDescriptionU5BU5D_t6_266*)(IAchievementDescriptionU5BU5D_t6_266*)L_4, /*hidden argument*/Action_1_Invoke_m1_5590_MethodInfo_var);
	}

IL_0039:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AuthenticateCallbackWrapper(System.Int32)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5591_MethodInfo_var;
extern "C" void GameCenterPlatform_AuthenticateCallbackWrapper_m6_55 (Object_t * __this /* static, unused */, int32_t ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5591_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483797);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t1_905 * G_B3_0 = {0};
	Action_1_t1_905 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	Action_1_t1_905 * G_B4_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_905 * L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_AuthenticateCallback_0;
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m6_65(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t1_905 * L_1 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_AuthenticateCallback_0;
		int32_t L_2 = ___result;
		G_B2_0 = L_1;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B3_0 = L_1;
			goto IL_0021;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0022:
	{
		NullCheck(G_B4_1);
		Action_1_Invoke_m1_5591(G_B4_1, G_B4_0, /*hidden argument*/Action_1_Invoke_m1_5591_MethodInfo_var);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearFriends(System.Int32)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ClearFriends_m6_56 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size;
		GameCenterPlatform_SafeClearArray_m6_83(NULL /*static, unused*/, (&((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_friends_10), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriends(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetFriends_m6_57 (Object_t * __this /* static, unused */, GcUserProfileData_t6_203  ___data, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number;
		GcUserProfileData_AddToArray_m6_1412((&___data), (&((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_friends_10), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriendImage(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetFriendImage_m6_58 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Texture2D_t6_33 * L_0 = ___texture;
		int32_t L_1 = ___number;
		GameCenterPlatform_SafeSetUserImage_m6_82(NULL /*static, unused*/, (&((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_friends_10), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerFriendsCallbackWrapper(System.Int32)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5591_MethodInfo_var;
extern "C" void GameCenterPlatform_TriggerFriendsCallbackWrapper_m6_59 (Object_t * __this /* static, unused */, int32_t ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5591_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483797);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t1_905 * G_B5_0 = {0};
	Action_1_t1_905 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Action_1_t1_905 * G_B6_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		UserProfileU5BU5D_t6_20* L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_friends_10;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		LocalUser_t6_21 * L_1 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		UserProfileU5BU5D_t6_20* L_2 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_friends_10;
		NullCheck(L_1);
		LocalUser_SetFriends_m6_1426(L_1, (IUserProfileU5BU5D_t6_214*)(IUserProfileU5BU5D_t6_214*)L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_905 * L_3 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_FriendsCallback_1;
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_905 * L_4 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_FriendsCallback_1;
		int32_t L_5 = ___result;
		G_B4_0 = L_4;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			G_B5_0 = L_4;
			goto IL_0035;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0036:
	{
		NullCheck(G_B6_1);
		Action_1_Invoke_m1_5591(G_B6_1, G_B6_0, /*hidden argument*/Action_1_Invoke_m1_5591_MethodInfo_var);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AchievementCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[])
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern TypeInfo* AchievementU5BU5D_t6_267_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5592_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2688;
extern "C" void GameCenterPlatform_AchievementCallbackWrapper_m6_60 (Object_t * __this /* static, unused */, GcAchievementDataU5BU5D_t6_253* ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		AchievementU5BU5D_t6_267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(919);
		Action_1_Invoke_m1_5592_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483798);
		_stringLiteral2688 = il2cpp_codegen_string_literal_from_index(2688);
		s_Il2CppMethodIntialized = true;
	}
	AchievementU5BU5D_t6_267* V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_907 * L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_AchievementLoaderCallback_3;
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		GcAchievementDataU5BU5D_t6_253* L_1 = ___result;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))))
		{
			goto IL_001c;
		}
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral2688, /*hidden argument*/NULL);
	}

IL_001c:
	{
		GcAchievementDataU5BU5D_t6_253* L_2 = ___result;
		NullCheck(L_2);
		V_0 = ((AchievementU5BU5D_t6_267*)SZArrayNew(AchievementU5BU5D_t6_267_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_2)->max_length))))));
		V_1 = 0;
		goto IL_003f;
	}

IL_002c:
	{
		AchievementU5BU5D_t6_267* L_3 = V_0;
		int32_t L_4 = V_1;
		GcAchievementDataU5BU5D_t6_253* L_5 = ___result;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		Achievement_t6_215 * L_7 = GcAchievementData_ToAchievement_m6_1414(((GcAchievementData_t6_205 *)(GcAchievementData_t6_205 *)SZArrayLdElema(L_5, L_6, sizeof(GcAchievementData_t6_205 ))), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_7);
		*((Achievement_t6_215 **)(Achievement_t6_215 **)SZArrayLdElema(L_3, L_4, sizeof(Achievement_t6_215 *))) = (Achievement_t6_215 *)L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		GcAchievementDataU5BU5D_t6_253* L_10 = ___result;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_907 * L_11 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_AchievementLoaderCallback_3;
		AchievementU5BU5D_t6_267* L_12 = V_0;
		NullCheck(L_11);
		Action_1_Invoke_m1_5592(L_11, (IAchievementU5BU5D_t6_268*)(IAchievementU5BU5D_t6_268*)L_12, /*hidden argument*/Action_1_Invoke_m1_5592_MethodInfo_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ProgressCallbackWrapper(System.Boolean)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5591_MethodInfo_var;
extern "C" void GameCenterPlatform_ProgressCallbackWrapper_m6_61 (Object_t * __this /* static, unused */, bool ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5591_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483797);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_905 * L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_ProgressCallback_4;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_905 * L_1 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_ProgressCallback_4;
		bool L_2 = ___success;
		NullCheck(L_1);
		Action_1_Invoke_m1_5591(L_1, L_2, /*hidden argument*/Action_1_Invoke_m1_5591_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreCallbackWrapper(System.Boolean)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5591_MethodInfo_var;
extern "C" void GameCenterPlatform_ScoreCallbackWrapper_m6_62 (Object_t * __this /* static, unused */, bool ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5591_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483797);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_905 * L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_ScoreCallback_5;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_905 * L_1 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_ScoreCallback_5;
		bool L_2 = ___success;
		NullCheck(L_1);
		Action_1_Invoke_m1_5591(L_1, L_2, /*hidden argument*/Action_1_Invoke_m1_5591_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreLoaderCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern TypeInfo* ScoreU5BU5D_t6_269_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5593_MethodInfo_var;
extern "C" void GameCenterPlatform_ScoreLoaderCallbackWrapper_m6_63 (Object_t * __this /* static, unused */, GcScoreDataU5BU5D_t6_254* ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		ScoreU5BU5D_t6_269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(923);
		Action_1_Invoke_m1_5593_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483799);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t6_269* V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_908 * L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_ScoreLoaderCallback_6;
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		GcScoreDataU5BU5D_t6_254* L_1 = ___result;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t6_269*)SZArrayNew(ScoreU5BU5D_t6_269_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002d;
	}

IL_001a:
	{
		ScoreU5BU5D_t6_269* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t6_254* L_4 = ___result;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t6_217 * L_6 = GcScoreData_ToScore_m6_1415(((GcScoreData_t6_206 *)(GcScoreData_t6_206 *)SZArrayLdElema(L_4, L_5, sizeof(GcScoreData_t6_206 ))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		*((Score_t6_217 **)(Score_t6_217 **)SZArrayLdElema(L_2, L_3, sizeof(Score_t6_217 *))) = (Score_t6_217 *)L_6;
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t6_254* L_9 = ___result;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_908 * L_10 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_ScoreLoaderCallback_6;
		ScoreU5BU5D_t6_269* L_11 = V_0;
		NullCheck(L_10);
		Action_1_Invoke_m1_5593(L_10, (IScoreU5BU5D_t6_218*)(IScoreU5BU5D_t6_218*)L_11, /*hidden argument*/Action_1_Invoke_m1_5593_MethodInfo_var);
	}

IL_0041:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::get_localUser()
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern TypeInfo* LocalUser_t6_21_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral140;
extern "C" Object_t * GameCenterPlatform_get_localUser_m6_64 (GameCenterPlatform_t6_18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		LocalUser_t6_21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(925);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral140 = il2cpp_codegen_string_literal_from_index(140);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		LocalUser_t6_21 * L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		LocalUser_t6_21 * L_1 = (LocalUser_t6_21 *)il2cpp_codegen_object_new (LocalUser_t6_21_il2cpp_TypeInfo_var);
		LocalUser__ctor_m6_1425(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		bool L_2 = GameCenterPlatform_Internal_Authenticated_m6_31(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		LocalUser_t6_21 * L_3 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		NullCheck(L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1_454(NULL /*static, unused*/, L_4, _stringLiteral140, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m6_65(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		LocalUser_t6_21 * L_6 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		return L_6;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::PopulateLocalUser()
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_PopulateLocalUser_m6_65 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		LocalUser_t6_21 * L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		bool L_1 = GameCenterPlatform_Internal_Authenticated_m6_31(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocalUser_SetAuthenticated_m6_1427(L_0, L_1, /*hidden argument*/NULL);
		LocalUser_t6_21 * L_2 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		String_t* L_3 = GameCenterPlatform_Internal_UserName_m6_32(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		UserProfile_SetUserName_m6_1433(L_2, L_3, /*hidden argument*/NULL);
		LocalUser_t6_21 * L_4 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		String_t* L_5 = GameCenterPlatform_Internal_UserID_m6_33(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		UserProfile_SetUserID_m6_1434(L_4, L_5, /*hidden argument*/NULL);
		LocalUser_t6_21 * L_6 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		bool L_7 = GameCenterPlatform_Internal_Underage_m6_34(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		LocalUser_SetUnderage_m6_1428(L_6, L_7, /*hidden argument*/NULL);
		LocalUser_t6_21 * L_8 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		Texture2D_t6_33 * L_9 = GameCenterPlatform_Internal_UserImage_m6_35(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		UserProfile_SetImage_m6_1435(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern TypeInfo* AchievementDescriptionU5BU5D_t6_19_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5590_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadAchievementDescriptions_m6_66 (GameCenterPlatform_t6_18 * __this, Action_1_t1_906 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementDescriptionU5BU5D_t6_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(909);
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5590_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483796);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1_906 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_5590(L_1, (IAchievementDescriptionU5BU5D_t6_266*)(IAchievementDescriptionU5BU5D_t6_266*)((AchievementDescriptionU5BU5D_t6_19*)SZArrayNew(AchievementDescriptionU5BU5D_t6_19_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m1_5590_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t1_906 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_AchievementDescriptionLoaderCallback_2 = L_2;
		GameCenterPlatform_Internal_LoadAchievementDescriptions_m6_37(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5591_MethodInfo_var;
extern "C" void GameCenterPlatform_ReportProgress_m6_67 (GameCenterPlatform_t6_18 * __this, String_t* ___id, double ___progress, Action_1_t1_905 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5591_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483797);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t1_905 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_5591(L_1, 0, /*hidden argument*/Action_1_Invoke_m1_5591_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t1_905 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_ProgressCallback_4 = L_2;
		String_t* L_3 = ___id;
		double L_4 = ___progress;
		GameCenterPlatform_Internal_ReportProgress_m6_39(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern TypeInfo* AchievementU5BU5D_t6_267_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5592_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadAchievements_m6_68 (GameCenterPlatform_t6_18 * __this, Action_1_t1_907 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementU5BU5D_t6_267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(919);
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5592_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483798);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1_907 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_5592(L_1, (IAchievementU5BU5D_t6_268*)(IAchievementU5BU5D_t6_268*)((AchievementU5BU5D_t6_267*)SZArrayNew(AchievementU5BU5D_t6_267_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m1_5592_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t1_907 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_AchievementLoaderCallback_3 = L_2;
		GameCenterPlatform_Internal_LoadAchievements_m6_38(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5591_MethodInfo_var;
extern "C" void GameCenterPlatform_ReportScore_m6_69 (GameCenterPlatform_t6_18 * __this, int64_t ___score, String_t* ___board, Action_1_t1_905 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5591_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483797);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t1_905 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_5591(L_1, 0, /*hidden argument*/Action_1_Invoke_m1_5591_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t1_905 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_ScoreCallback_5 = L_2;
		int64_t L_3 = ___score;
		String_t* L_4 = ___board;
		GameCenterPlatform_Internal_ReportScore_m6_40(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern TypeInfo* ScoreU5BU5D_t6_269_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5593_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadScores_m6_70 (GameCenterPlatform_t6_18 * __this, String_t* ___category, Action_1_t1_908 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ScoreU5BU5D_t6_269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(923);
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5593_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483799);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1_908 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_5593(L_1, (IScoreU5BU5D_t6_218*)(IScoreU5BU5D_t6_218*)((ScoreU5BU5D_t6_269*)SZArrayNew(ScoreU5BU5D_t6_269_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m1_5593_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t1_908 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_ScoreLoaderCallback_6 = L_2;
		String_t* L_3 = ___category;
		GameCenterPlatform_Internal_LoadScores_m6_41(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern TypeInfo* Leaderboard_t6_23_il2cpp_TypeInfo_var;
extern TypeInfo* GcLeaderboard_t6_22_il2cpp_TypeInfo_var;
extern TypeInfo* ILeaderboard_t6_256_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5591_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadScores_m6_71 (GameCenterPlatform_t6_18 * __this, Object_t * ___board, Action_1_t1_905 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Leaderboard_t6_23_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(926);
		GcLeaderboard_t6_22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(908);
		ILeaderboard_t6_256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(927);
		Action_1_Invoke_m1_5591_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483797);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t6_23 * V_0 = {0};
	GcLeaderboard_t6_22 * V_1 = {0};
	Range_t6_219  V_2 = {0};
	Range_t6_219  V_3 = {0};
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t1_905 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_5591(L_1, 0, /*hidden argument*/Action_1_Invoke_m1_5591_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t1_905 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_LeaderboardCallback_7 = L_2;
		Object_t * L_3 = ___board;
		V_0 = ((Leaderboard_t6_23 *)CastclassClass(L_3, Leaderboard_t6_23_il2cpp_TypeInfo_var));
		Leaderboard_t6_23 * L_4 = V_0;
		GcLeaderboard_t6_22 * L_5 = (GcLeaderboard_t6_22 *)il2cpp_codegen_object_new (GcLeaderboard_t6_22_il2cpp_TypeInfo_var);
		GcLeaderboard__ctor_m6_87(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t1_910 * L_6 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_GcBoards_14;
		GcLeaderboard_t6_22 * L_7 = V_1;
		NullCheck(L_6);
		VirtActionInvoker1< GcLeaderboard_t6_22 * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Add(!0) */, L_6, L_7);
		Leaderboard_t6_23 * L_8 = V_0;
		NullCheck(L_8);
		StringU5BU5D_t1_202* L_9 = Leaderboard_GetUserFilter_m6_1474(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		GcLeaderboard_t6_22 * L_10 = V_1;
		Object_t * L_11 = ___board;
		NullCheck(L_11);
		String_t* L_12 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t6_256_il2cpp_TypeInfo_var, L_11);
		Object_t * L_13 = ___board;
		NullCheck(L_13);
		int32_t L_14 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t6_256_il2cpp_TypeInfo_var, L_13);
		Leaderboard_t6_23 * L_15 = V_0;
		NullCheck(L_15);
		StringU5BU5D_t1_202* L_16 = Leaderboard_GetUserFilter_m6_1474(L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		GcLeaderboard_Internal_LoadScoresWithUsers_m6_95(L_10, L_12, L_14, L_16, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_005d:
	{
		GcLeaderboard_t6_22 * L_17 = V_1;
		Object_t * L_18 = ___board;
		NullCheck(L_18);
		String_t* L_19 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t6_256_il2cpp_TypeInfo_var, L_18);
		Object_t * L_20 = ___board;
		NullCheck(L_20);
		Range_t6_219  L_21 = (Range_t6_219 )InterfaceFuncInvoker0< Range_t6_219  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t6_256_il2cpp_TypeInfo_var, L_20);
		V_2 = L_21;
		int32_t L_22 = ((&V_2)->___from_0);
		Object_t * L_23 = ___board;
		NullCheck(L_23);
		Range_t6_219  L_24 = (Range_t6_219 )InterfaceFuncInvoker0< Range_t6_219  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t6_256_il2cpp_TypeInfo_var, L_23);
		V_3 = L_24;
		int32_t L_25 = ((&V_3)->___count_1);
		Object_t * L_26 = ___board;
		NullCheck(L_26);
		int32_t L_27 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope() */, ILeaderboard_t6_256_il2cpp_TypeInfo_var, L_26);
		Object_t * L_28 = ___board;
		NullCheck(L_28);
		int32_t L_29 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t6_256_il2cpp_TypeInfo_var, L_28);
		NullCheck(L_17);
		GcLeaderboard_Internal_LoadScores_m6_94(L_17, L_19, L_22, L_25, L_27, L_29, /*hidden argument*/NULL);
	}

IL_0091:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LeaderboardCallbackWrapper(System.Boolean)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5591_MethodInfo_var;
extern "C" void GameCenterPlatform_LeaderboardCallbackWrapper_m6_72 (Object_t * __this /* static, unused */, bool ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5591_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483797);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_905 * L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_LeaderboardCallback_7;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_905 * L_1 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_LeaderboardCallback_7;
		bool L_2 = ___success;
		NullCheck(L_1);
		Action_1_Invoke_m1_5591(L_1, L_2, /*hidden argument*/Action_1_Invoke_m1_5591_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern TypeInfo* Leaderboard_t6_23_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_926_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_5594_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_5595_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_5596_MethodInfo_var;
extern "C" bool GameCenterPlatform_GetLoading_m6_73 (GameCenterPlatform_t6_18 * __this, Object_t * ___board, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Leaderboard_t6_23_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(926);
		Enumerator_t1_926_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(928);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		List_1_GetEnumerator_m1_5594_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483800);
		Enumerator_get_Current_m1_5595_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483801);
		Enumerator_MoveNext_m1_5596_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483802);
		s_Il2CppMethodIntialized = true;
	}
	GcLeaderboard_t6_22 * V_0 = {0};
	Enumerator_t1_926  V_1 = {0};
	bool V_2 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		List_1_t1_910 * L_1 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___m_GcBoards_14;
		NullCheck(L_1);
		Enumerator_t1_926  L_2 = List_1_GetEnumerator_m1_5594(L_1, /*hidden argument*/List_1_GetEnumerator_m1_5594_MethodInfo_var);
		V_1 = L_2;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_001d:
		{
			GcLeaderboard_t6_22 * L_3 = Enumerator_get_Current_m1_5595((&V_1), /*hidden argument*/Enumerator_get_Current_m1_5595_MethodInfo_var);
			V_0 = L_3;
			GcLeaderboard_t6_22 * L_4 = V_0;
			Object_t * L_5 = ___board;
			NullCheck(L_4);
			bool L_6 = GcLeaderboard_Contains_m6_89(L_4, ((Leaderboard_t6_23 *)CastclassClass(L_5, Leaderboard_t6_23_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0042;
			}
		}

IL_0036:
		{
			GcLeaderboard_t6_22 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = GcLeaderboard_Loading_m6_96(L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}

IL_0042:
		{
			bool L_9 = Enumerator_MoveNext_m1_5596((&V_1), /*hidden argument*/Enumerator_MoveNext_m1_5596_MethodInfo_var);
			if (L_9)
			{
				goto IL_001d;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x5F, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_t1_926  L_10 = V_1;
		Enumerator_t1_926  L_11 = L_10;
		Object_t * L_12 = Box(Enumerator_t1_926_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_12);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_005f:
	{
		return 0;
	}

IL_0061:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::VerifyAuthentication()
extern TypeInfo* ILocalUser_t6_255_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2689;
extern "C" bool GameCenterPlatform_VerifyAuthentication_m6_74 (GameCenterPlatform_t6_18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILocalUser_t6_255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(929);
		_stringLiteral2689 = il2cpp_codegen_string_literal_from_index(2689);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = GameCenterPlatform_get_localUser_m6_64(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t6_255_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral2689, /*hidden argument*/NULL);
		return 0;
	}

IL_001c:
	{
		return 1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowAchievementsUI()
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowAchievementsUI_m6_75 (GameCenterPlatform_t6_18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowAchievementsUI_m6_42(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI()
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m6_76 (GameCenterPlatform_t6_18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowLeaderboardUI_m6_43(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearUsers(System.Int32)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ClearUsers_m6_77 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size;
		GameCenterPlatform_SafeClearArray_m6_83(NULL /*static, unused*/, (&((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_users_11), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUser(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetUser_m6_78 (Object_t * __this /* static, unused */, GcUserProfileData_t6_203  ___data, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number;
		GcUserProfileData_AddToArray_m6_1412((&___data), (&((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_users_11), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUserImage(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetUserImage_m6_79 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Texture2D_t6_33 * L_0 = ___texture;
		int32_t L_1 = ___number;
		GameCenterPlatform_SafeSetUserImage_m6_82(NULL /*static, unused*/, (&((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_users_11), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerUsersCallbackWrapper()
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5597_MethodInfo_var;
extern "C" void GameCenterPlatform_TriggerUsersCallbackWrapper_m6_80 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5597_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483803);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_909 * L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_UsersCallback_8;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_909 * L_1 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_UsersCallback_8;
		UserProfileU5BU5D_t6_20* L_2 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_users_11;
		NullCheck(L_1);
		Action_1_Invoke_m1_5597(L_1, (IUserProfileU5BU5D_t6_214*)(IUserProfileU5BU5D_t6_214*)L_2, /*hidden argument*/Action_1_Invoke_m1_5597_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern TypeInfo* UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5597_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadUsers_m6_81 (GameCenterPlatform_t6_18 * __this, StringU5BU5D_t1_202* ___userIds, Action_1_t1_909 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(912);
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5597_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483803);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1_909 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_5597(L_1, (IUserProfileU5BU5D_t6_214*)(IUserProfileU5BU5D_t6_214*)((UserProfileU5BU5D_t6_20*)SZArrayNew(UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m1_5597_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t1_909 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_UsersCallback_8 = L_2;
		StringU5BU5D_t1_202* L_3 = ___userIds;
		GameCenterPlatform_Internal_LoadUsers_m6_44(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeSetUserImage(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,UnityEngine.Texture2D,System.Int32)
extern TypeInfo* Texture2D_t6_33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2690;
extern Il2CppCodeGenString* _stringLiteral2691;
extern "C" void GameCenterPlatform_SafeSetUserImage_m6_82 (Object_t * __this /* static, unused */, UserProfileU5BU5D_t6_20** ___array, Texture2D_t6_33 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t6_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(932);
		_stringLiteral2690 = il2cpp_codegen_string_literal_from_index(2690);
		_stringLiteral2691 = il2cpp_codegen_string_literal_from_index(2691);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t6_20** L_0 = ___array;
		NullCheck((*((UserProfileU5BU5D_t6_20**)L_0)));
		int32_t L_1 = ___number;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t6_20**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___number;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}

IL_0011:
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral2690, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_3 = (Texture2D_t6_33 *)il2cpp_codegen_object_new (Texture2D_t6_33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m6_130(L_3, ((int32_t)76), ((int32_t)76), /*hidden argument*/NULL);
		___texture = L_3;
	}

IL_0026:
	{
		UserProfileU5BU5D_t6_20** L_4 = ___array;
		NullCheck((*((UserProfileU5BU5D_t6_20**)L_4)));
		int32_t L_5 = ___number;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t6_20**)L_4)))->max_length))))) <= ((int32_t)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = ___number;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		UserProfileU5BU5D_t6_20** L_7 = ___array;
		int32_t L_8 = ___number;
		NullCheck((*((UserProfileU5BU5D_t6_20**)L_7)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t6_20**)L_7)), L_8);
		int32_t L_9 = L_8;
		Texture2D_t6_33 * L_10 = ___texture;
		NullCheck((*(UserProfile_t6_213 **)(UserProfile_t6_213 **)SZArrayLdElema((*((UserProfileU5BU5D_t6_20**)L_7)), L_9, sizeof(UserProfile_t6_213 *))));
		UserProfile_SetImage_m6_1435((*(UserProfile_t6_213 **)(UserProfile_t6_213 **)SZArrayLdElema((*((UserProfileU5BU5D_t6_20**)L_7)), L_9, sizeof(UserProfile_t6_213 *))), L_10, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0046:
	{
		Debug_Log_m6_489(NULL /*static, unused*/, _stringLiteral2691, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeClearArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern TypeInfo* UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SafeClearArray_m6_83 (Object_t * __this /* static, unused */, UserProfileU5BU5D_t6_20** ___array, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(912);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t6_20** L_0 = ___array;
		if (!(*((UserProfileU5BU5D_t6_20**)L_0)))
		{
			goto IL_0011;
		}
	}
	{
		UserProfileU5BU5D_t6_20** L_1 = ___array;
		NullCheck((*((UserProfileU5BU5D_t6_20**)L_1)));
		int32_t L_2 = ___size;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t6_20**)L_1)))->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0019;
		}
	}

IL_0011:
	{
		UserProfileU5BU5D_t6_20** L_3 = ___array;
		int32_t L_4 = ___size;
		*((Object_t **)(L_3)) = (Object_t *)((UserProfileU5BU5D_t6_20*)SZArrayNew(UserProfileU5BU5D_t6_20_il2cpp_TypeInfo_var, L_4));
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILeaderboard UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateLeaderboard()
extern TypeInfo* Leaderboard_t6_23_il2cpp_TypeInfo_var;
extern "C" Object_t * GameCenterPlatform_CreateLeaderboard_m6_84 (GameCenterPlatform_t6_18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Leaderboard_t6_23_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(926);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t6_23 * V_0 = {0};
	{
		Leaderboard_t6_23 * L_0 = (Leaderboard_t6_23 *)il2cpp_codegen_object_new (Leaderboard_t6_23_il2cpp_TypeInfo_var);
		Leaderboard__ctor_m6_1468(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Leaderboard_t6_23 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.SocialPlatforms.IAchievement UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateAchievement()
extern TypeInfo* Achievement_t6_215_il2cpp_TypeInfo_var;
extern "C" Object_t * GameCenterPlatform_CreateAchievement_m6_85 (GameCenterPlatform_t6_18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Achievement_t6_215_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(920);
		s_Il2CppMethodIntialized = true;
	}
	Achievement_t6_215 * V_0 = {0};
	{
		Achievement_t6_215 * L_0 = (Achievement_t6_215 *)il2cpp_codegen_object_new (Achievement_t6_215_il2cpp_TypeInfo_var);
		Achievement__ctor_m6_1442(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Achievement_t6_215 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerResetAchievementCallback(System.Boolean)
extern TypeInfo* GameCenterPlatform_t6_18_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_5591_MethodInfo_var;
extern "C" void GameCenterPlatform_TriggerResetAchievementCallback_m6_86 (Object_t * __this /* static, unused */, bool ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(911);
		Action_1_Invoke_m1_5591_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483797);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_905 * L_0 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_ResetAchievements_12;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_18_il2cpp_TypeInfo_var);
		Action_1_t1_905 * L_1 = ((GameCenterPlatform_t6_18_StaticFields*)GameCenterPlatform_t6_18_il2cpp_TypeInfo_var->static_fields)->___s_ResetAchievements_12;
		bool L_2 = ___result;
		NullCheck(L_1);
		Action_1_Invoke_m1_5591(L_1, L_2, /*hidden argument*/Action_1_Invoke_m1_5591_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::.ctor(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C" void GcLeaderboard__ctor_m6_87 (GcLeaderboard_t6_22 * __this, Leaderboard_t6_23 * ___board, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Leaderboard_t6_23 * L_0 = ___board;
		__this->___m_GenericLeaderboard_1 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Finalize()
extern "C" void GcLeaderboard_Finalize_m6_88 (GcLeaderboard_t6_22 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GcLeaderboard_Dispose_m6_97(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Contains(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C" bool GcLeaderboard_Contains_m6_89 (GcLeaderboard_t6_22 * __this, Leaderboard_t6_23 * ___board, const MethodInfo* method)
{
	{
		Leaderboard_t6_23 * L_0 = (__this->___m_GenericLeaderboard_1);
		Leaderboard_t6_23 * L_1 = ___board;
		return ((((Object_t*)(Leaderboard_t6_23 *)L_0) == ((Object_t*)(Leaderboard_t6_23 *)L_1))? 1 : 0);
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetScores(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern TypeInfo* ScoreU5BU5D_t6_269_il2cpp_TypeInfo_var;
extern "C" void GcLeaderboard_SetScores_m6_90 (GcLeaderboard_t6_22 * __this, GcScoreDataU5BU5D_t6_254* ___scoreDatas, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ScoreU5BU5D_t6_269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(923);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t6_269* V_0 = {0};
	int32_t V_1 = 0;
	{
		Leaderboard_t6_23 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		GcScoreDataU5BU5D_t6_254* L_1 = ___scoreDatas;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t6_269*)SZArrayNew(ScoreU5BU5D_t6_269_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002e;
	}

IL_001b:
	{
		ScoreU5BU5D_t6_269* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t6_254* L_4 = ___scoreDatas;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t6_217 * L_6 = GcScoreData_ToScore_m6_1415(((GcScoreData_t6_206 *)(GcScoreData_t6_206 *)SZArrayLdElema(L_4, L_5, sizeof(GcScoreData_t6_206 ))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		*((Score_t6_217 **)(Score_t6_217 **)SZArrayLdElema(L_2, L_3, sizeof(Score_t6_217 *))) = (Score_t6_217 *)L_6;
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t6_254* L_9 = ___scoreDatas;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		Leaderboard_t6_23 * L_10 = (__this->___m_GenericLeaderboard_1);
		ScoreU5BU5D_t6_269* L_11 = V_0;
		NullCheck(L_10);
		Leaderboard_SetScores_m6_1472(L_10, (IScoreU5BU5D_t6_218*)(IScoreU5BU5D_t6_218*)L_11, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetLocalScore(UnityEngine.SocialPlatforms.GameCenter.GcScoreData)
extern "C" void GcLeaderboard_SetLocalScore_m6_91 (GcLeaderboard_t6_22 * __this, GcScoreData_t6_206  ___scoreData, const MethodInfo* method)
{
	{
		Leaderboard_t6_23 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Leaderboard_t6_23 * L_1 = (__this->___m_GenericLeaderboard_1);
		Score_t6_217 * L_2 = GcScoreData_ToScore_m6_1415((&___scoreData), /*hidden argument*/NULL);
		NullCheck(L_1);
		Leaderboard_SetLocalUserScore_m6_1470(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetMaxRange(System.UInt32)
extern "C" void GcLeaderboard_SetMaxRange_m6_92 (GcLeaderboard_t6_22 * __this, uint32_t ___maxRange, const MethodInfo* method)
{
	{
		Leaderboard_t6_23 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t6_23 * L_1 = (__this->___m_GenericLeaderboard_1);
		uint32_t L_2 = ___maxRange;
		NullCheck(L_1);
		Leaderboard_SetMaxRange_m6_1471(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetTitle(System.String)
extern "C" void GcLeaderboard_SetTitle_m6_93 (GcLeaderboard_t6_22 * __this, String_t* ___title, const MethodInfo* method)
{
	{
		Leaderboard_t6_23 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t6_23 * L_1 = (__this->___m_GenericLeaderboard_1);
		String_t* L_2 = ___title;
		NullCheck(L_1);
		Leaderboard_SetTitle_m6_1473(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void GcLeaderboard_Internal_LoadScores_m6_94 (GcLeaderboard_t6_22 * __this, String_t* ___category, int32_t ___from, int32_t ___count, int32_t ___playerScope, int32_t ___timeScope, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScores_m6_94_ftn) (GcLeaderboard_t6_22 *, String_t*, int32_t, int32_t, int32_t, int32_t);
	static GcLeaderboard_Internal_LoadScores_m6_94_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScores_m6_94_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___category, ___from, ___count, ___playerScope, ___timeScope);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])
extern "C" void GcLeaderboard_Internal_LoadScoresWithUsers_m6_95 (GcLeaderboard_t6_22 * __this, String_t* ___category, int32_t ___timeScope, StringU5BU5D_t1_202* ___userIDs, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScoresWithUsers_m6_95_ftn) (GcLeaderboard_t6_22 *, String_t*, int32_t, StringU5BU5D_t1_202*);
	static GcLeaderboard_Internal_LoadScoresWithUsers_m6_95_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScoresWithUsers_m6_95_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])");
	_il2cpp_icall_func(__this, ___category, ___timeScope, ___userIDs);
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
extern "C" bool GcLeaderboard_Loading_m6_96 (GcLeaderboard_t6_22 * __this, const MethodInfo* method)
{
	typedef bool (*GcLeaderboard_Loading_m6_96_ftn) (GcLeaderboard_t6_22 *);
	static GcLeaderboard_Loading_m6_96_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Loading_m6_96_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
extern "C" void GcLeaderboard_Dispose_m6_97 (GcLeaderboard_t6_22 * __this, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Dispose_m6_97_ftn) (GcLeaderboard_t6_22 *);
	static GcLeaderboard_Dispose_m6_97_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Dispose_m6_97_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()");
	_il2cpp_icall_func(__this);
}
// System.Single UnityEngine.BoneWeight::get_weight0()
extern "C" float BoneWeight_get_weight0_m6_98 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight0_0);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight0(System.Single)
extern "C" void BoneWeight_set_weight0_m6_99 (BoneWeight_t6_24 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight0_0 = L_0;
		return;
	}
}
// System.Single UnityEngine.BoneWeight::get_weight1()
extern "C" float BoneWeight_get_weight1_m6_100 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight1_1);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight1(System.Single)
extern "C" void BoneWeight_set_weight1_m6_101 (BoneWeight_t6_24 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight1_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.BoneWeight::get_weight2()
extern "C" float BoneWeight_get_weight2_m6_102 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight2_2);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight2(System.Single)
extern "C" void BoneWeight_set_weight2_m6_103 (BoneWeight_t6_24 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight2_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.BoneWeight::get_weight3()
extern "C" float BoneWeight_get_weight3_m6_104 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight3_3);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight3(System.Single)
extern "C" void BoneWeight_set_weight3_m6_105 (BoneWeight_t6_24 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight3_3 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex0()
extern "C" int32_t BoneWeight_get_boneIndex0_m6_106 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex0_4);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex0(System.Int32)
extern "C" void BoneWeight_set_boneIndex0_m6_107 (BoneWeight_t6_24 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex0_4 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex1()
extern "C" int32_t BoneWeight_get_boneIndex1_m6_108 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex1_5);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex1(System.Int32)
extern "C" void BoneWeight_set_boneIndex1_m6_109 (BoneWeight_t6_24 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex1_5 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex2()
extern "C" int32_t BoneWeight_get_boneIndex2_m6_110 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex2_6);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex2(System.Int32)
extern "C" void BoneWeight_set_boneIndex2_m6_111 (BoneWeight_t6_24 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex2_6 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex3()
extern "C" int32_t BoneWeight_get_boneIndex3_m6_112 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex3_7);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex3(System.Int32)
extern "C" void BoneWeight_set_boneIndex3_m6_113 (BoneWeight_t6_24 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex3_7 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::GetHashCode()
extern "C" int32_t BoneWeight_GetHashCode_m6_114 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		int32_t L_0 = BoneWeight_get_boneIndex0_m6_106(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Int32_GetHashCode_m1_44((&V_0), /*hidden argument*/NULL);
		int32_t L_2 = BoneWeight_get_boneIndex1_m6_108(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Int32_GetHashCode_m1_44((&V_1), /*hidden argument*/NULL);
		int32_t L_4 = BoneWeight_get_boneIndex2_m6_110(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Int32_GetHashCode_m1_44((&V_2), /*hidden argument*/NULL);
		int32_t L_6 = BoneWeight_get_boneIndex3_m6_112(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Int32_GetHashCode_m1_44((&V_3), /*hidden argument*/NULL);
		float L_8 = BoneWeight_get_weight0_m6_98(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		int32_t L_9 = Single_GetHashCode_m1_475((&V_4), /*hidden argument*/NULL);
		float L_10 = BoneWeight_get_weight1_m6_100(__this, /*hidden argument*/NULL);
		V_5 = L_10;
		int32_t L_11 = Single_GetHashCode_m1_475((&V_5), /*hidden argument*/NULL);
		float L_12 = BoneWeight_get_weight2_m6_102(__this, /*hidden argument*/NULL);
		V_6 = L_12;
		int32_t L_13 = Single_GetHashCode_m1_475((&V_6), /*hidden argument*/NULL);
		float L_14 = BoneWeight_get_weight3_m6_104(__this, /*hidden argument*/NULL);
		V_7 = L_14;
		int32_t L_15 = Single_GetHashCode_m1_475((&V_7), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))))^(int32_t)((int32_t)((int32_t)L_9<<(int32_t)5))))^(int32_t)((int32_t)((int32_t)L_11<<(int32_t)4))))^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)4))))^(int32_t)((int32_t)((int32_t)L_15>>(int32_t)3))));
	}
}
// System.Boolean UnityEngine.BoneWeight::Equals(System.Object)
extern TypeInfo* BoneWeight_t6_24_il2cpp_TypeInfo_var;
extern TypeInfo* Vector4_t6_55_il2cpp_TypeInfo_var;
extern "C" bool BoneWeight_Equals_m6_115 (BoneWeight_t6_24 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BoneWeight_t6_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(933);
		Vector4_t6_55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(934);
		s_Il2CppMethodIntialized = true;
	}
	BoneWeight_t6_24  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector4_t6_55  V_5 = {0};
	int32_t G_B8_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, BoneWeight_t6_24_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(BoneWeight_t6_24 *)((BoneWeight_t6_24 *)UnBox (L_1, BoneWeight_t6_24_il2cpp_TypeInfo_var))));
		int32_t L_2 = BoneWeight_get_boneIndex0_m6_106(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = BoneWeight_get_boneIndex0_m6_106((&V_0), /*hidden argument*/NULL);
		bool L_4 = Int32_Equals_m1_46((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_5 = BoneWeight_get_boneIndex1_m6_108(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = BoneWeight_get_boneIndex1_m6_108((&V_0), /*hidden argument*/NULL);
		bool L_7 = Int32_Equals_m1_46((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_8 = BoneWeight_get_boneIndex2_m6_110(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		int32_t L_9 = BoneWeight_get_boneIndex2_m6_110((&V_0), /*hidden argument*/NULL);
		bool L_10 = Int32_Equals_m1_46((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_11 = BoneWeight_get_boneIndex3_m6_112(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		int32_t L_12 = BoneWeight_get_boneIndex3_m6_112((&V_0), /*hidden argument*/NULL);
		bool L_13 = Int32_Equals_m1_46((&V_4), L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00cb;
		}
	}
	{
		float L_14 = BoneWeight_get_weight0_m6_98(__this, /*hidden argument*/NULL);
		float L_15 = BoneWeight_get_weight1_m6_100(__this, /*hidden argument*/NULL);
		float L_16 = BoneWeight_get_weight2_m6_102(__this, /*hidden argument*/NULL);
		float L_17 = BoneWeight_get_weight3_m6_104(__this, /*hidden argument*/NULL);
		Vector4__ctor_m6_363((&V_5), L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		float L_18 = BoneWeight_get_weight0_m6_98((&V_0), /*hidden argument*/NULL);
		float L_19 = BoneWeight_get_weight1_m6_100((&V_0), /*hidden argument*/NULL);
		float L_20 = BoneWeight_get_weight2_m6_102((&V_0), /*hidden argument*/NULL);
		float L_21 = BoneWeight_get_weight3_m6_104((&V_0), /*hidden argument*/NULL);
		Vector4_t6_55  L_22 = {0};
		Vector4__ctor_m6_363(&L_22, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		Vector4_t6_55  L_23 = L_22;
		Object_t * L_24 = Box(Vector4_t6_55_il2cpp_TypeInfo_var, &L_23);
		bool L_25 = Vector4_Equals_m6_365((&V_5), L_24, /*hidden argument*/NULL);
		G_B8_0 = ((int32_t)(L_25));
		goto IL_00cc;
	}

IL_00cb:
	{
		G_B8_0 = 0;
	}

IL_00cc:
	{
		return G_B8_0;
	}
}
// System.Boolean UnityEngine.BoneWeight::op_Equality(UnityEngine.BoneWeight,UnityEngine.BoneWeight)
extern "C" bool BoneWeight_op_Equality_m6_116 (Object_t * __this /* static, unused */, BoneWeight_t6_24  ___lhs, BoneWeight_t6_24  ___rhs, const MethodInfo* method)
{
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = BoneWeight_get_boneIndex0_m6_106((&___lhs), /*hidden argument*/NULL);
		int32_t L_1 = BoneWeight_get_boneIndex0_m6_106((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_2 = BoneWeight_get_boneIndex1_m6_108((&___lhs), /*hidden argument*/NULL);
		int32_t L_3 = BoneWeight_get_boneIndex1_m6_108((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_4 = BoneWeight_get_boneIndex2_m6_110((&___lhs), /*hidden argument*/NULL);
		int32_t L_5 = BoneWeight_get_boneIndex2_m6_110((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_6 = BoneWeight_get_boneIndex3_m6_112((&___lhs), /*hidden argument*/NULL);
		int32_t L_7 = BoneWeight_get_boneIndex3_m6_112((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_0095;
		}
	}
	{
		float L_8 = BoneWeight_get_weight0_m6_98((&___lhs), /*hidden argument*/NULL);
		float L_9 = BoneWeight_get_weight1_m6_100((&___lhs), /*hidden argument*/NULL);
		float L_10 = BoneWeight_get_weight2_m6_102((&___lhs), /*hidden argument*/NULL);
		float L_11 = BoneWeight_get_weight3_m6_104((&___lhs), /*hidden argument*/NULL);
		Vector4_t6_55  L_12 = {0};
		Vector4__ctor_m6_363(&L_12, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		float L_13 = BoneWeight_get_weight0_m6_98((&___rhs), /*hidden argument*/NULL);
		float L_14 = BoneWeight_get_weight1_m6_100((&___rhs), /*hidden argument*/NULL);
		float L_15 = BoneWeight_get_weight2_m6_102((&___rhs), /*hidden argument*/NULL);
		float L_16 = BoneWeight_get_weight3_m6_104((&___rhs), /*hidden argument*/NULL);
		Vector4_t6_55  L_17 = {0};
		Vector4__ctor_m6_363(&L_17, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		bool L_18 = Vector4_op_Equality_m6_370(NULL /*static, unused*/, L_12, L_17, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_18));
		goto IL_0096;
	}

IL_0095:
	{
		G_B6_0 = 0;
	}

IL_0096:
	{
		return G_B6_0;
	}
}
// System.Boolean UnityEngine.BoneWeight::op_Inequality(UnityEngine.BoneWeight,UnityEngine.BoneWeight)
extern "C" bool BoneWeight_op_Inequality_m6_117 (Object_t * __this /* static, unused */, BoneWeight_t6_24  ___lhs, BoneWeight_t6_24  ___rhs, const MethodInfo* method)
{
	{
		BoneWeight_t6_24  L_0 = ___lhs;
		BoneWeight_t6_24  L_1 = ___rhs;
		bool L_2 = BoneWeight_op_Equality_m6_116(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C" void Renderer_set_enabled_m6_118 (Renderer_t6_25 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Renderer_set_enabled_m6_118_ftn) (Renderer_t6_25 *, bool);
	static Renderer_set_enabled_m6_118_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_enabled_m6_118_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C" Material_t6_67 * Renderer_get_material_m6_119 (Renderer_t6_25 * __this, const MethodInfo* method)
{
	typedef Material_t6_67 * (*Renderer_get_material_m6_119_ftn) (Renderer_t6_25 *);
	static Renderer_get_material_m6_119_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m6_119_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C" void Renderer_set_material_m6_120 (Renderer_t6_25 * __this, Material_t6_67 * ___value, const MethodInfo* method)
{
	typedef void (*Renderer_set_material_m6_120_ftn) (Renderer_t6_25 *, Material_t6_67 *);
	static Renderer_set_material_m6_120_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_material_m6_120_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C" int32_t Screen_get_width_m6_121 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_width_m6_121_ftn) ();
	static Screen_get_width_m6_121_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m6_121_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C" int32_t Screen_get_height_m6_122 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_height_m6_122_ftn) ();
	static Screen_get_height_m6_122_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m6_122_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	return _il2cpp_icall_func();
}
// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C" GUIElement_t6_29 * GUILayer_HitTest_m6_123 (GUILayer_t6_31 * __this, Vector3_t6_49  ___screenPosition, const MethodInfo* method)
{
	{
		GUIElement_t6_29 * L_0 = GUILayer_INTERNAL_CALL_HitTest_m6_124(NULL /*static, unused*/, __this, (&___screenPosition), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C" GUIElement_t6_29 * GUILayer_INTERNAL_CALL_HitTest_m6_124 (Object_t * __this /* static, unused */, GUILayer_t6_31 * ___self, Vector3_t6_49 * ___screenPosition, const MethodInfo* method)
{
	typedef GUIElement_t6_29 * (*GUILayer_INTERNAL_CALL_HitTest_m6_124_ftn) (GUILayer_t6_31 *, Vector3_t6_49 *);
	static GUILayer_INTERNAL_CALL_HitTest_m6_124_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayer_INTERNAL_CALL_HitTest_m6_124_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___screenPosition);
}
// System.Void UnityEngine.Texture::.ctor()
extern "C" void Texture__ctor_m6_125 (Texture_t6_32 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m6_553(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C" int32_t Texture_Internal_GetWidth_m6_126 (Object_t * __this /* static, unused */, Texture_t6_32 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetWidth_m6_126_ftn) (Texture_t6_32 *);
	static Texture_Internal_GetWidth_m6_126_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetWidth_m6_126_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)");
	return _il2cpp_icall_func(___mono);
}
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C" int32_t Texture_Internal_GetHeight_m6_127 (Object_t * __this /* static, unused */, Texture_t6_32 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetHeight_m6_127_ftn) (Texture_t6_32 *);
	static Texture_Internal_GetHeight_m6_127_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetHeight_m6_127_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)");
	return _il2cpp_icall_func(___mono);
}
// System.Int32 UnityEngine.Texture::get_width()
extern "C" int32_t Texture_get_width_m6_128 (Texture_t6_32 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetWidth_m6_126(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.Texture::get_height()
extern "C" int32_t Texture_get_height_m6_129 (Texture_t6_32 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetHeight_m6_127(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void Texture2D__ctor_m6_130 (Texture2D_t6_33 * __this, int32_t ___width, int32_t ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m6_125(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width;
		int32_t L_1 = ___height;
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		Texture2D_Internal_Create_m6_131(NULL /*static, unused*/, __this, L_0, L_1, 5, 1, 0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C" void Texture2D_Internal_Create_m6_131 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ___mono, int32_t ___width, int32_t ___height, int32_t ___format, bool ___mipmap, bool ___linear, IntPtr_t ___nativeTex, const MethodInfo* method)
{
	typedef void (*Texture2D_Internal_Create_m6_131_ftn) (Texture2D_t6_33 *, int32_t, int32_t, int32_t, bool, bool, IntPtr_t);
	static Texture2D_Internal_Create_m6_131_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Internal_Create_m6_131_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)");
	_il2cpp_icall_func(___mono, ___width, ___height, ___format, ___mipmap, ___linear, ___nativeTex);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C" int32_t RenderTexture_Internal_GetWidth_m6_132 (Object_t * __this /* static, unused */, RenderTexture_t6_34 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m6_132_ftn) (RenderTexture_t6_34 *);
	static RenderTexture_Internal_GetWidth_m6_132_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m6_132_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C" int32_t RenderTexture_Internal_GetHeight_m6_133 (Object_t * __this /* static, unused */, RenderTexture_t6_34 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m6_133_ftn) (RenderTexture_t6_34 *);
	static RenderTexture_Internal_GetHeight_m6_133_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m6_133_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C" int32_t RenderTexture_get_width_m6_134 (RenderTexture_t6_34 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m6_132(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C" int32_t RenderTexture_get_height_m6_135 (RenderTexture_t6_34 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m6_133(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.CullingGroup/StateChanged::.ctor(System.Object,System.IntPtr)
extern "C" void StateChanged__ctor_m6_136 (StateChanged_t6_37 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C" void StateChanged_Invoke_m6_137 (StateChanged_t6_37 * __this, CullingGroupEvent_t6_36  ___sphere, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		StateChanged_Invoke_m6_137((StateChanged_t6_37 *)__this->___prev_9,___sphere, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, CullingGroupEvent_t6_36  ___sphere, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___sphere,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, CullingGroupEvent_t6_36  ___sphere, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___sphere,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_StateChanged_t6_37(Il2CppObject* delegate, CullingGroupEvent_t6_36  ___sphere)
{
	typedef void (STDCALL *native_function_ptr_type)(CullingGroupEvent_t6_36 );
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___sphere' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___sphere);

	// Marshaling cleanup of parameter '___sphere' native representation

}
// System.IAsyncResult UnityEngine.CullingGroup/StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern TypeInfo* CullingGroupEvent_t6_36_il2cpp_TypeInfo_var;
extern "C" Object_t * StateChanged_BeginInvoke_m6_138 (StateChanged_t6_37 * __this, CullingGroupEvent_t6_36  ___sphere, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CullingGroupEvent_t6_36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(935);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CullingGroupEvent_t6_36_il2cpp_TypeInfo_var, &___sphere);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.CullingGroup/StateChanged::EndInvoke(System.IAsyncResult)
extern "C" void StateChanged_EndInvoke_m6_139 (StateChanged_t6_37 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.CullingGroup::Finalize()
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void CullingGroup_Finalize_m6_140 (CullingGroup_t6_38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = (__this->___m_Ptr_0);
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
			bool L_2 = IntPtr_op_Inequality_m1_641(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_001b;
			}
		}

IL_0015:
		{
			CullingGroup_FinalizerFailure_m6_143(__this, /*hidden argument*/NULL);
		}

IL_001b:
		{
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::Dispose()
extern "C" void CullingGroup_Dispose_m6_141 (CullingGroup_t6_38 * __this, const MethodInfo* method)
{
	typedef void (*CullingGroup_Dispose_m6_141_ftn) (CullingGroup_t6_38 *);
	static CullingGroup_Dispose_m6_141_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_Dispose_m6_141_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::Dispose()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern "C" void CullingGroup_SendEvents_m6_142 (Object_t * __this /* static, unused */, CullingGroup_t6_38 * ___cullingGroup, IntPtr_t ___eventsPtr, int32_t ___count, const MethodInfo* method)
{
	CullingGroupEvent_t6_36 * V_0 = {0};
	int32_t V_1 = 0;
	{
		void* L_0 = IntPtr_ToPointer_m1_637((&___eventsPtr), /*hidden argument*/NULL);
		V_0 = (CullingGroupEvent_t6_36 *)L_0;
		CullingGroup_t6_38 * L_1 = ___cullingGroup;
		NullCheck(L_1);
		StateChanged_t6_37 * L_2 = (L_1->___m_OnStateChanged_1);
		if (L_2)
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		V_1 = 0;
		goto IL_0039;
	}

IL_001b:
	{
		CullingGroup_t6_38 * L_3 = ___cullingGroup;
		NullCheck(L_3);
		StateChanged_t6_37 * L_4 = (L_3->___m_OnStateChanged_1);
		CullingGroupEvent_t6_36 * L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_4);
		StateChanged_Invoke_m6_137(L_4, (*(CullingGroupEvent_t6_36 *)((CullingGroupEvent_t6_36 *)((intptr_t)L_5+(int32_t)((int32_t)((int32_t)L_6*(int32_t)sizeof(CullingGroupEvent_t6_36 )))))), /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_8 = V_1;
		int32_t L_9 = ___count;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_001b;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C" void CullingGroup_FinalizerFailure_m6_143 (CullingGroup_t6_38 * __this, const MethodInfo* method)
{
	typedef void (*CullingGroup_FinalizerFailure_m6_143_ftn) (CullingGroup_t6_38 *);
	static CullingGroup_FinalizerFailure_m6_143_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_FinalizerFailure_m6_143_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::FinalizerFailure()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GradientColorKey::.ctor(UnityEngine.Color,System.Single)
extern "C" void GradientColorKey__ctor_m6_144 (GradientColorKey_t6_39 * __this, Color_t6_40  ___col, float ___time, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = ___col;
		__this->___color_0 = L_0;
		float L_1 = ___time;
		__this->___time_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.GradientAlphaKey::.ctor(System.Single,System.Single)
extern "C" void GradientAlphaKey__ctor_m6_145 (GradientAlphaKey_t6_41 * __this, float ___alpha, float ___time, const MethodInfo* method)
{
	{
		float L_0 = ___alpha;
		__this->___alpha_0 = L_0;
		float L_1 = ___time;
		__this->___time_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m6_146 (Gradient_t6_42 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Gradient_Init_m6_147(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m6_147 (Gradient_t6_42 * __this, const MethodInfo* method)
{
	typedef void (*Gradient_Init_m6_147_ftn) (Gradient_t6_42 *);
	static Gradient_Init_m6_147_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Init_m6_147_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m6_148 (Gradient_t6_42 * __this, const MethodInfo* method)
{
	typedef void (*Gradient_Cleanup_m6_148_ftn) (Gradient_t6_42 *);
	static Gradient_Cleanup_m6_148_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Cleanup_m6_148_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m6_149 (Gradient_t6_42 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Gradient_Cleanup_m6_148(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t6_42_marshal(const Gradient_t6_42& unmarshaled, Gradient_t6_42_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void Gradient_t6_42_marshal_back(const Gradient_t6_42_marshaled& marshaled, Gradient_t6_42& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t6_42_marshal_cleanup(Gradient_t6_42_marshaled& marshaled)
{
}
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern TypeInfo* TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43_il2cpp_TypeInfo_var;
extern TypeInfo* TouchScreenKeyboardType_t6_44_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1_703_il2cpp_TypeInfo_var;
extern "C" void TouchScreenKeyboard__ctor_m6_150 (TouchScreenKeyboard_t6_45 * __this, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, bool ___secure, bool ___alert, String_t* ___textPlaceholder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(936);
		TouchScreenKeyboardType_t6_44_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(937);
		Convert_t1_703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43  V_0 = {0};
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Initobj (TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = ___keyboardType;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(TouchScreenKeyboardType_t6_44_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_703_il2cpp_TypeInfo_var);
		uint32_t L_3 = Convert_ToUInt32_m1_4855(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		(&V_0)->___keyboardType_0 = L_3;
		bool L_4 = ___autocorrection;
		uint32_t L_5 = Convert_ToUInt32_m1_4842(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		(&V_0)->___autocorrection_1 = L_5;
		bool L_6 = ___multiline;
		uint32_t L_7 = Convert_ToUInt32_m1_4842(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		(&V_0)->___multiline_2 = L_7;
		bool L_8 = ___secure;
		uint32_t L_9 = Convert_ToUInt32_m1_4842(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		(&V_0)->___secure_3 = L_9;
		bool L_10 = ___alert;
		uint32_t L_11 = Convert_ToUInt32_m1_4842(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		(&V_0)->___alert_4 = L_11;
		String_t* L_12 = ___text;
		String_t* L_13 = ___textPlaceholder;
		TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6_153(__this, (&V_0), L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C" void TouchScreenKeyboard_Destroy_m6_151 (TouchScreenKeyboard_t6_45 * __this, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_Destroy_m6_151_ftn) (TouchScreenKeyboard_t6_45 *);
	static TouchScreenKeyboard_Destroy_m6_151_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_Destroy_m6_151_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern "C" void TouchScreenKeyboard_Finalize_m6_152 (TouchScreenKeyboard_t6_45 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		TouchScreenKeyboard_Destroy_m6_151(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C" void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6_153 (TouchScreenKeyboard_t6_45 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43 * ___arguments, String_t* ___text, String_t* ___textPlaceholder, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6_153_ftn) (TouchScreenKeyboard_t6_45 *, TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43 *, String_t*, String_t*);
	static TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6_153_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6_153_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)");
	_il2cpp_icall_func(__this, ___arguments, ___text, ___textPlaceholder);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_isSupported()
extern "C" bool TouchScreenKeyboard_get_isSupported_m6_154 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		int32_t L_0 = Application_get_platform_m6_452(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 0)
		{
			goto IL_0064;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 1)
		{
			goto IL_0064;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 2)
		{
			goto IL_0064;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 3)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 4)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 5)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 6)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 7)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 8)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 9)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 10)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 11)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 12)
		{
			goto IL_0062;
		}
	}

IL_0045:
	{
		int32_t L_3 = V_1;
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 0)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 1)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 2)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 3)
		{
			goto IL_0062;
		}
	}
	{
		goto IL_0066;
	}

IL_0062:
	{
		return 1;
	}

IL_0064:
	{
		return 0;
	}

IL_0066:
	{
		return 0;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" TouchScreenKeyboard_t6_45 * TouchScreenKeyboard_Open_m6_155 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, bool ___secure, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		V_1 = 0;
		String_t* L_1 = ___text;
		int32_t L_2 = ___keyboardType;
		bool L_3 = ___autocorrection;
		bool L_4 = ___multiline;
		bool L_5 = ___secure;
		bool L_6 = V_1;
		String_t* L_7 = V_0;
		TouchScreenKeyboard_t6_45 * L_8 = TouchScreenKeyboard_Open_m6_156(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern TypeInfo* TouchScreenKeyboard_t6_45_il2cpp_TypeInfo_var;
extern "C" TouchScreenKeyboard_t6_45 * TouchScreenKeyboard_Open_m6_156 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, bool ___secure, bool ___alert, String_t* ___textPlaceholder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TouchScreenKeyboard_t6_45_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(938);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		int32_t L_1 = ___keyboardType;
		bool L_2 = ___autocorrection;
		bool L_3 = ___multiline;
		bool L_4 = ___secure;
		bool L_5 = ___alert;
		String_t* L_6 = ___textPlaceholder;
		TouchScreenKeyboard_t6_45 * L_7 = (TouchScreenKeyboard_t6_45 *)il2cpp_codegen_object_new (TouchScreenKeyboard_t6_45_il2cpp_TypeInfo_var);
		TouchScreenKeyboard__ctor_m6_150(L_7, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.String UnityEngine.TouchScreenKeyboard::get_text()
extern "C" String_t* TouchScreenKeyboard_get_text_m6_157 (TouchScreenKeyboard_t6_45 * __this, const MethodInfo* method)
{
	typedef String_t* (*TouchScreenKeyboard_get_text_m6_157_ftn) (TouchScreenKeyboard_t6_45 *);
	static TouchScreenKeyboard_get_text_m6_157_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_text_m6_157_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_text()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
extern "C" bool TouchScreenKeyboard_get_done_m6_158 (TouchScreenKeyboard_t6_45 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_done_m6_158_ftn) (TouchScreenKeyboard_t6_45 *);
	static TouchScreenKeyboard_get_done_m6_158_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_done_m6_158_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_done()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gizmos::DrawWireSphere(UnityEngine.Vector3,System.Single)
extern "C" void Gizmos_DrawWireSphere_m6_159 (Object_t * __this /* static, unused */, Vector3_t6_49  ___center, float ___radius, const MethodInfo* method)
{
	{
		float L_0 = ___radius;
		Gizmos_INTERNAL_CALL_DrawWireSphere_m6_160(NULL /*static, unused*/, (&___center), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireSphere(UnityEngine.Vector3&,System.Single)
extern "C" void Gizmos_INTERNAL_CALL_DrawWireSphere_m6_160 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___center, float ___radius, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_CALL_DrawWireSphere_m6_160_ftn) (Vector3_t6_49 *, float);
	static Gizmos_INTERNAL_CALL_DrawWireSphere_m6_160_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_CALL_DrawWireSphere_m6_160_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_CALL_DrawWireSphere(UnityEngine.Vector3&,System.Single)");
	_il2cpp_icall_func(___center, ___radius);
}
// System.Void UnityEngine.Gizmos::DrawSphere(UnityEngine.Vector3,System.Single)
extern "C" void Gizmos_DrawSphere_m6_161 (Object_t * __this /* static, unused */, Vector3_t6_49  ___center, float ___radius, const MethodInfo* method)
{
	{
		float L_0 = ___radius;
		Gizmos_INTERNAL_CALL_DrawSphere_m6_162(NULL /*static, unused*/, (&___center), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawSphere(UnityEngine.Vector3&,System.Single)
extern "C" void Gizmos_INTERNAL_CALL_DrawSphere_m6_162 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___center, float ___radius, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_CALL_DrawSphere_m6_162_ftn) (Vector3_t6_49 *, float);
	static Gizmos_INTERNAL_CALL_DrawSphere_m6_162_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_CALL_DrawSphere_m6_162_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_CALL_DrawSphere(UnityEngine.Vector3&,System.Single)");
	_il2cpp_icall_func(___center, ___radius);
}
// System.Void UnityEngine.Gizmos::DrawWireCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Gizmos_DrawWireCube_m6_163 (Object_t * __this /* static, unused */, Vector3_t6_49  ___center, Vector3_t6_49  ___size, const MethodInfo* method)
{
	{
		Gizmos_INTERNAL_CALL_DrawWireCube_m6_164(NULL /*static, unused*/, (&___center), (&___size), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Gizmos_INTERNAL_CALL_DrawWireCube_m6_164 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___center, Vector3_t6_49 * ___size, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_CALL_DrawWireCube_m6_164_ftn) (Vector3_t6_49 *, Vector3_t6_49 *);
	static Gizmos_INTERNAL_CALL_DrawWireCube_m6_164_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_CALL_DrawWireCube_m6_164_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_CALL_DrawWireCube(UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___center, ___size);
}
// System.Void UnityEngine.Gizmos::DrawCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Gizmos_DrawCube_m6_165 (Object_t * __this /* static, unused */, Vector3_t6_49  ___center, Vector3_t6_49  ___size, const MethodInfo* method)
{
	{
		Gizmos_INTERNAL_CALL_DrawCube_m6_166(NULL /*static, unused*/, (&___center), (&___size), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Gizmos_INTERNAL_CALL_DrawCube_m6_166 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___center, Vector3_t6_49 * ___size, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_CALL_DrawCube_m6_166_ftn) (Vector3_t6_49 *, Vector3_t6_49 *);
	static Gizmos_INTERNAL_CALL_DrawCube_m6_166_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_CALL_DrawCube_m6_166_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_CALL_DrawCube(UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___center, ___size);
}
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C" void Gizmos_set_color_m6_167 (Object_t * __this /* static, unused */, Color_t6_40  ___value, const MethodInfo* method)
{
	{
		Gizmos_INTERNAL_set_color_m6_168(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void Gizmos_INTERNAL_set_color_m6_168 (Object_t * __this /* static, unused */, Color_t6_40 * ___value, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_set_color_m6_168_ftn) (Color_t6_40 *);
	static Gizmos_INTERNAL_set_color_m6_168_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_set_color_m6_168_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)");
	_il2cpp_icall_func(___value);
}
// System.Int32 UnityEngine.LayerMask::get_value()
extern "C" int32_t LayerMask_get_value_m6_169 (LayerMask_t6_47 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Mask_0);
		return L_0;
	}
}
// System.Void UnityEngine.LayerMask::set_value(System.Int32)
extern "C" void LayerMask_set_value_m6_170 (LayerMask_t6_47 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_Mask_0 = L_0;
		return;
	}
}
// System.String UnityEngine.LayerMask::LayerToName(System.Int32)
extern "C" String_t* LayerMask_LayerToName_m6_171 (Object_t * __this /* static, unused */, int32_t ___layer, const MethodInfo* method)
{
	typedef String_t* (*LayerMask_LayerToName_m6_171_ftn) (int32_t);
	static LayerMask_LayerToName_m6_171_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LayerMask_LayerToName_m6_171_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.LayerMask::LayerToName(System.Int32)");
	return _il2cpp_icall_func(___layer);
}
// System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern "C" int32_t LayerMask_NameToLayer_m6_172 (Object_t * __this /* static, unused */, String_t* ___layerName, const MethodInfo* method)
{
	typedef int32_t (*LayerMask_NameToLayer_m6_172_ftn) (String_t*);
	static LayerMask_NameToLayer_m6_172_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LayerMask_NameToLayer_m6_172_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.LayerMask::NameToLayer(System.String)");
	return _il2cpp_icall_func(___layerName);
}
// System.Int32 UnityEngine.LayerMask::GetMask(System.String[])
extern "C" int32_t LayerMask_GetMask_m6_173 (Object_t * __this /* static, unused */, StringU5BU5D_t1_202* ___layerNames, const MethodInfo* method)
{
	int32_t V_0 = 0;
	String_t* V_1 = {0};
	StringU5BU5D_t1_202* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = 0;
		StringU5BU5D_t1_202* L_0 = ___layerNames;
		V_2 = L_0;
		V_3 = 0;
		goto IL_002f;
	}

IL_000b:
	{
		StringU5BU5D_t1_202* L_1 = V_2;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_1 = (*(String_t**)(String_t**)SZArrayLdElema(L_1, L_3, sizeof(String_t*)));
		String_t* L_4 = V_1;
		int32_t L_5 = LayerMask_NameToLayer_m6_172(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_4 = L_5;
		int32_t L_6 = V_4;
		if (!L_6)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t L_8 = V_4;
		V_0 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8&(int32_t)((int32_t)31)))&(int32_t)((int32_t)31)))))));
	}

IL_002b:
	{
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_10 = V_3;
		StringU5BU5D_t1_202* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern "C" int32_t LayerMask_op_Implicit_m6_174 (Object_t * __this /* static, unused */, LayerMask_t6_47  ___mask, const MethodInfo* method)
{
	{
		int32_t L_0 = ((&___mask)->___m_Mask_0);
		return L_0;
	}
}
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C" LayerMask_t6_47  LayerMask_op_Implicit_m6_175 (Object_t * __this /* static, unused */, int32_t ___intVal, const MethodInfo* method)
{
	LayerMask_t6_47  V_0 = {0};
	{
		int32_t L_0 = ___intVal;
		(&V_0)->___m_Mask_0 = L_0;
		LayerMask_t6_47  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" void Vector2__ctor_m6_176 (Vector2_t6_48 * __this, float ___x, float ___y, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		return;
	}
}
// System.String UnityEngine.Vector2::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2692;
extern "C" String_t* Vector2_ToString_m6_177 (Vector2_t6_48 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		_stringLiteral2692 = il2cpp_codegen_string_literal_from_index(2692);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 2));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2692, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C" int32_t Vector2_GetHashCode_m6_178 (Vector2_t6_48 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m1_475(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m1_475(L_2, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern TypeInfo* Vector2_t6_48_il2cpp_TypeInfo_var;
extern "C" bool Vector2_Equals_m6_179 (Vector2_t6_48 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2_t6_48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(939);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6_48  V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Vector2_t6_48_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Vector2_t6_48 *)((Vector2_t6_48 *)UnBox (L_1, Vector2_t6_48_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m1_474(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m1_474(L_5, L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0040;
	}

IL_003f:
	{
		G_B5_0 = 0;
	}

IL_0040:
	{
		return G_B5_0;
	}
}
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C" float Vector2_get_sqrMagnitude_m6_180 (Vector2_t6_48 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___x_1);
		float L_1 = (__this->___x_1);
		float L_2 = (__this->___y_2);
		float L_3 = (__this->___y_2);
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// System.Single UnityEngine.Vector2::SqrMagnitude(UnityEngine.Vector2)
extern "C" float Vector2_SqrMagnitude_m6_181 (Object_t * __this /* static, unused */, Vector2_t6_48  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___a)->___y_2);
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C" Vector2_t6_48  Vector2_get_zero_m6_182 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t6_48  L_0 = {0};
		Vector2__ctor_m6_176(&L_0, (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern "C" Vector2_t6_48  Vector2_get_up_m6_183 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t6_48  L_0 = {0};
		Vector2__ctor_m6_176(&L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" Vector2_t6_48  Vector2_op_Addition_m6_184 (Object_t * __this /* static, unused */, Vector2_t6_48  ___a, Vector2_t6_48  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		Vector2_t6_48  L_4 = {0};
		Vector2__ctor_m6_176(&L_4, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" Vector2_t6_48  Vector2_op_Subtraction_m6_185 (Object_t * __this /* static, unused */, Vector2_t6_48  ___a, Vector2_t6_48  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		Vector2_t6_48  L_4 = {0};
		Vector2__ctor_m6_176(&L_4, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_UnaryNegation(UnityEngine.Vector2)
extern "C" Vector2_t6_48  Vector2_op_UnaryNegation_m6_186 (Object_t * __this /* static, unused */, Vector2_t6_48  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___y_2);
		Vector2_t6_48  L_2 = {0};
		Vector2__ctor_m6_176(&L_2, ((-L_0)), ((-L_1)), /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C" Vector2_t6_48  Vector2_op_Multiply_m6_187 (Object_t * __this /* static, unused */, Vector2_t6_48  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		Vector2_t6_48  L_4 = {0};
		Vector2__ctor_m6_176(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" bool Vector2_op_Equality_m6_188 (Object_t * __this /* static, unused */, Vector2_t6_48  ___lhs, Vector2_t6_48  ___rhs, const MethodInfo* method)
{
	{
		Vector2_t6_48  L_0 = ___lhs;
		Vector2_t6_48  L_1 = ___rhs;
		Vector2_t6_48  L_2 = Vector2_op_Subtraction_m6_185(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector2_SqrMagnitude_m6_181(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" Vector2_t6_48  Vector2_op_Implicit_m6_189 (Object_t * __this /* static, unused */, Vector3_t6_49  ___v, const MethodInfo* method)
{
	{
		float L_0 = ((&___v)->___x_1);
		float L_1 = ((&___v)->___y_2);
		Vector2_t6_48  L_2 = {0};
		Vector2__ctor_m6_176(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" Vector3_t6_49  Vector2_op_Implicit_m6_190 (Object_t * __this /* static, unused */, Vector2_t6_48  ___v, const MethodInfo* method)
{
	{
		float L_0 = ((&___v)->___x_1);
		float L_1 = ((&___v)->___y_2);
		Vector3_t6_49  L_2 = {0};
		Vector3__ctor_m6_191(&L_2, L_0, L_1, (0.0f), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" void Vector3__ctor_m6_191 (Vector3_t6_49 * __this, float ___x, float ___y, float ___z, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		float L_2 = ___z;
		__this->___z_3 = L_2;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Vector3_t6_49  Vector3_Lerp_m6_192 (Object_t * __this /* static, unused */, Vector3_t6_49  ___a, Vector3_t6_49  ___b, float ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m6_391(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t = L_1;
		float L_2 = ((&___a)->___x_1);
		float L_3 = ((&___b)->___x_1);
		float L_4 = ((&___a)->___x_1);
		float L_5 = ___t;
		float L_6 = ((&___a)->___y_2);
		float L_7 = ((&___b)->___y_2);
		float L_8 = ((&___a)->___y_2);
		float L_9 = ___t;
		float L_10 = ((&___a)->___z_3);
		float L_11 = ((&___b)->___z_3);
		float L_12 = ((&___a)->___z_3);
		float L_13 = ___t;
		Vector3_t6_49  L_14 = {0};
		Vector3__ctor_m6_191(&L_14, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), /*hidden argument*/NULL);
		return L_14;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Slerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" Vector3_t6_49  Vector3_Slerp_m6_193 (Object_t * __this /* static, unused */, Vector3_t6_49  ___a, Vector3_t6_49  ___b, float ___t, const MethodInfo* method)
{
	{
		float L_0 = ___t;
		Vector3_t6_49  L_1 = Vector3_INTERNAL_CALL_Slerp_m6_194(NULL /*static, unused*/, (&___a), (&___b), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::INTERNAL_CALL_Slerp(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single)
extern "C" Vector3_t6_49  Vector3_INTERNAL_CALL_Slerp_m6_194 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___a, Vector3_t6_49 * ___b, float ___t, const MethodInfo* method)
{
	typedef Vector3_t6_49  (*Vector3_INTERNAL_CALL_Slerp_m6_194_ftn) (Vector3_t6_49 *, Vector3_t6_49 *, float);
	static Vector3_INTERNAL_CALL_Slerp_m6_194_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Vector3_INTERNAL_CALL_Slerp_m6_194_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Vector3::INTERNAL_CALL_Slerp(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single)");
	return _il2cpp_icall_func(___a, ___b, ___t);
}
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" Vector3_t6_49  Vector3_MoveTowards_m6_195 (Object_t * __this /* static, unused */, Vector3_t6_49  ___current, Vector3_t6_49  ___target, float ___maxDistanceDelta, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	float V_1 = 0.0f;
	{
		Vector3_t6_49  L_0 = ___target;
		Vector3_t6_49  L_1 = ___current;
		Vector3_t6_49  L_2 = Vector3_op_Subtraction_m6_222(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector3_get_magnitude_m6_208((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = V_1;
		float L_5 = ___maxDistanceDelta;
		if ((((float)L_4) <= ((float)L_5)))
		{
			goto IL_0022;
		}
	}
	{
		float L_6 = V_1;
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0024;
		}
	}

IL_0022:
	{
		Vector3_t6_49  L_7 = ___target;
		return L_7;
	}

IL_0024:
	{
		Vector3_t6_49  L_8 = ___current;
		Vector3_t6_49  L_9 = V_0;
		float L_10 = V_1;
		Vector3_t6_49  L_11 = Vector3_op_Division_m6_226(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		float L_12 = ___maxDistanceDelta;
		Vector3_t6_49  L_13 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Vector3_t6_49  L_14 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_8, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::RotateTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern "C" Vector3_t6_49  Vector3_RotateTowards_m6_196 (Object_t * __this /* static, unused */, Vector3_t6_49  ___current, Vector3_t6_49  ___target, float ___maxRadiansDelta, float ___maxMagnitudeDelta, const MethodInfo* method)
{
	{
		float L_0 = ___maxRadiansDelta;
		float L_1 = ___maxMagnitudeDelta;
		Vector3_t6_49  L_2 = Vector3_INTERNAL_CALL_RotateTowards_m6_197(NULL /*static, unused*/, (&___current), (&___target), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::INTERNAL_CALL_RotateTowards(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Single)
extern "C" Vector3_t6_49  Vector3_INTERNAL_CALL_RotateTowards_m6_197 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___current, Vector3_t6_49 * ___target, float ___maxRadiansDelta, float ___maxMagnitudeDelta, const MethodInfo* method)
{
	typedef Vector3_t6_49  (*Vector3_INTERNAL_CALL_RotateTowards_m6_197_ftn) (Vector3_t6_49 *, Vector3_t6_49 *, float, float);
	static Vector3_INTERNAL_CALL_RotateTowards_m6_197_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Vector3_INTERNAL_CALL_RotateTowards_m6_197_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Vector3::INTERNAL_CALL_RotateTowards(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Single)");
	return _il2cpp_icall_func(___current, ___target, ___maxRadiansDelta, ___maxMagnitudeDelta);
}
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C" int32_t Vector3_GetHashCode_m6_198 (Vector3_t6_49 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m1_475(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m1_475(L_2, /*hidden argument*/NULL);
		float* L_4 = &(__this->___z_3);
		int32_t L_5 = Single_GetHashCode_m1_475(L_4, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))));
	}
}
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern "C" bool Vector3_Equals_m6_199 (Vector3_t6_49 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_49  V_0 = {0};
	int32_t G_B6_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Vector3_t6_49_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Vector3_t6_49 *)((Vector3_t6_49 *)UnBox (L_1, Vector3_t6_49_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m1_474(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0056;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m1_474(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		float* L_8 = &(__this->___z_3);
		float L_9 = ((&V_0)->___z_3);
		bool L_10 = Single_Equals_m1_474(L_8, L_9, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_10));
		goto IL_0057;
	}

IL_0056:
	{
		G_B6_0 = 0;
	}

IL_0057:
	{
		return G_B6_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C" Vector3_t6_49  Vector3_Normalize_m6_200 (Object_t * __this /* static, unused */, Vector3_t6_49  ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t6_49  L_0 = ___value;
		float L_1 = Vector3_Magnitude_m6_207(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) > ((float)(1.0E-05f)))))
		{
			goto IL_001a;
		}
	}
	{
		Vector3_t6_49  L_3 = ___value;
		float L_4 = V_0;
		Vector3_t6_49  L_5 = Vector3_op_Division_m6_226(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001a:
	{
		Vector3_t6_49  L_6 = Vector3_get_zero_m6_213(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C" Vector3_t6_49  Vector3_get_normalized_m6_201 (Vector3_t6_49 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Vector3_Normalize_m6_200(NULL /*static, unused*/, (*(Vector3_t6_49 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Vector3::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2693;
extern "C" String_t* Vector3_ToString_m6_202 (Vector3_t6_49 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		_stringLiteral2693 = il2cpp_codegen_string_literal_from_index(2693);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 3));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_4;
		float L_9 = (__this->___z_3);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		String_t* L_12 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2693, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String UnityEngine.Vector3::ToString(System.String)
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2694;
extern "C" String_t* Vector3_ToString_m6_203 (Vector3_t6_49 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral2694 = il2cpp_codegen_string_literal_from_index(2694);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 3));
		float* L_1 = &(__this->___x_1);
		String_t* L_2 = ___format;
		String_t* L_3 = Single_ToString_m1_483(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		float* L_5 = &(__this->___y_2);
		String_t* L_6 = ___format;
		String_t* L_7 = Single_ToString_m1_483(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_4;
		float* L_9 = &(__this->___z_3);
		String_t* L_10 = ___format;
		String_t* L_11 = Single_ToString_m1_483(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		String_t* L_12 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2694, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" float Vector3_Dot_m6_204 (Object_t * __this /* static, unused */, Vector3_t6_49  ___lhs, Vector3_t6_49  ___rhs, const MethodInfo* method)
{
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		float L_2 = ((&___lhs)->___y_2);
		float L_3 = ((&___rhs)->___y_2);
		float L_4 = ((&___lhs)->___z_3);
		float L_5 = ((&___rhs)->___z_3);
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Vector3_Angle_m6_205 (Object_t * __this /* static, unused */, Vector3_t6_49  ___from, Vector3_t6_49  ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t6_49  L_0 = Vector3_get_normalized_m6_201((&___from), /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = Vector3_get_normalized_m6_201((&___to), /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m6_204(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_2, (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = acosf(L_3);
		return ((float)((float)L_4*(float)(57.29578f)));
	}
}
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Vector3_Distance_m6_206 (Object_t * __this /* static, unused */, Vector3_t6_49  ___a, Vector3_t6_49  ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_49  V_0 = {0};
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3__ctor_m6_191((&V_0), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		float L_6 = ((&V_0)->___x_1);
		float L_7 = ((&V_0)->___x_1);
		float L_8 = ((&V_0)->___y_2);
		float L_9 = ((&V_0)->___y_2);
		float L_10 = ((&V_0)->___z_3);
		float L_11 = ((&V_0)->___z_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_12 = sqrtf(((float)((float)((float)((float)((float)((float)L_6*(float)L_7))+(float)((float)((float)L_8*(float)L_9))))+(float)((float)((float)L_10*(float)L_11)))));
		return L_12;
	}
}
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Vector3_Magnitude_m6_207 (Object_t * __this /* static, unused */, Vector3_t6_49  ___a, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___a)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___a)->___z_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::get_magnitude()
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Vector3_get_magnitude_m6_208 (Vector3_t6_49 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___x_1);
		float L_1 = (__this->___x_1);
		float L_2 = (__this->___y_2);
		float L_3 = (__this->___y_2);
		float L_4 = (__this->___z_3);
		float L_5 = (__this->___z_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
extern "C" float Vector3_SqrMagnitude_m6_209 (Object_t * __this /* static, unused */, Vector3_t6_49  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___a)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___a)->___z_3);
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C" float Vector3_get_sqrMagnitude_m6_210 (Vector3_t6_49 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___x_1);
		float L_1 = (__this->___x_1);
		float L_2 = (__this->___y_2);
		float L_3 = (__this->___y_2);
		float L_4 = (__this->___z_3);
		float L_5 = (__this->___z_3);
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Vector3_t6_49  Vector3_Min_m6_211 (Object_t * __this /* static, unused */, Vector3_t6_49  ___lhs, Vector3_t6_49  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m6_381(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = ((&___lhs)->___y_2);
		float L_4 = ((&___rhs)->___y_2);
		float L_5 = Mathf_Min_m6_381(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = ((&___lhs)->___z_3);
		float L_7 = ((&___rhs)->___z_3);
		float L_8 = Mathf_Min_m6_381(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t6_49  L_9 = {0};
		Vector3__ctor_m6_191(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Vector3_t6_49  Vector3_Max_m6_212 (Object_t * __this /* static, unused */, Vector3_t6_49  ___lhs, Vector3_t6_49  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m6_383(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = ((&___lhs)->___y_2);
		float L_4 = ((&___rhs)->___y_2);
		float L_5 = Mathf_Max_m6_383(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = ((&___lhs)->___z_3);
		float L_7 = ((&___rhs)->___z_3);
		float L_8 = Mathf_Max_m6_383(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t6_49  L_9 = {0};
		Vector3__ctor_m6_191(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" Vector3_t6_49  Vector3_get_zero_m6_213 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = {0};
		Vector3__ctor_m6_191(&L_0, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C" Vector3_t6_49  Vector3_get_one_m6_214 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = {0};
		Vector3__ctor_m6_191(&L_0, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C" Vector3_t6_49  Vector3_get_forward_m6_215 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = {0};
		Vector3__ctor_m6_191(&L_0, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C" Vector3_t6_49  Vector3_get_back_m6_216 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = {0};
		Vector3__ctor_m6_191(&L_0, (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C" Vector3_t6_49  Vector3_get_up_m6_217 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = {0};
		Vector3__ctor_m6_191(&L_0, (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C" Vector3_t6_49  Vector3_get_down_m6_218 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = {0};
		Vector3__ctor_m6_191(&L_0, (0.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
extern "C" Vector3_t6_49  Vector3_get_left_m6_219 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = {0};
		Vector3__ctor_m6_191(&L_0, (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C" Vector3_t6_49  Vector3_get_right_m6_220 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = {0};
		Vector3__ctor_m6_191(&L_0, (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t6_49  Vector3_op_Addition_m6_221 (Object_t * __this /* static, unused */, Vector3_t6_49  ___a, Vector3_t6_49  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3_t6_49  L_6 = {0};
		Vector3__ctor_m6_191(&L_6, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), ((float)((float)L_4+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t6_49  Vector3_op_Subtraction_m6_222 (Object_t * __this /* static, unused */, Vector3_t6_49  ___a, Vector3_t6_49  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3_t6_49  L_6 = {0};
		Vector3__ctor_m6_191(&L_6, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C" Vector3_t6_49  Vector3_op_UnaryNegation_m6_223 (Object_t * __this /* static, unused */, Vector3_t6_49  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___y_2);
		float L_2 = ((&___a)->___z_3);
		Vector3_t6_49  L_3 = {0};
		Vector3__ctor_m6_191(&L_3, ((-L_0)), ((-L_1)), ((-L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C" Vector3_t6_49  Vector3_op_Multiply_m6_224 (Object_t * __this /* static, unused */, Vector3_t6_49  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		Vector3_t6_49  L_6 = {0};
		Vector3__ctor_m6_191(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C" Vector3_t6_49  Vector3_op_Multiply_m6_225 (Object_t * __this /* static, unused */, float ___d, Vector3_t6_49  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		Vector3_t6_49  L_6 = {0};
		Vector3__ctor_m6_191(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C" Vector3_t6_49  Vector3_op_Division_m6_226 (Object_t * __this /* static, unused */, Vector3_t6_49  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		Vector3_t6_49  L_6 = {0};
		Vector3__ctor_m6_191(&L_6, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool Vector3_op_Equality_m6_227 (Object_t * __this /* static, unused */, Vector3_t6_49  ___lhs, Vector3_t6_49  ___rhs, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___lhs;
		Vector3_t6_49  L_1 = ___rhs;
		Vector3_t6_49  L_2 = Vector3_op_Subtraction_m6_222(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m6_209(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool Vector3_op_Inequality_m6_228 (Object_t * __this /* static, unused */, Vector3_t6_49  ___lhs, Vector3_t6_49  ___rhs, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___lhs;
		Vector3_t6_49  L_1 = ___rhs;
		Vector3_t6_49  L_2 = Vector3_op_Subtraction_m6_222(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m6_209(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Color__ctor_m6_229 (Color_t6_40 * __this, float ___r, float ___g, float ___b, float ___a, const MethodInfo* method)
{
	{
		float L_0 = ___r;
		__this->___r_0 = L_0;
		float L_1 = ___g;
		__this->___g_1 = L_1;
		float L_2 = ___b;
		__this->___b_2 = L_2;
		float L_3 = ___a;
		__this->___a_3 = L_3;
		return;
	}
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C" void Color__ctor_m6_230 (Color_t6_40 * __this, float ___r, float ___g, float ___b, const MethodInfo* method)
{
	{
		float L_0 = ___r;
		__this->___r_0 = L_0;
		float L_1 = ___g;
		__this->___g_1 = L_1;
		float L_2 = ___b;
		__this->___b_2 = L_2;
		__this->___a_3 = (1.0f);
		return;
	}
}
// System.String UnityEngine.Color::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2695;
extern "C" String_t* Color_ToString_m6_231 (Color_t6_40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		_stringLiteral2695 = il2cpp_codegen_string_literal_from_index(2695);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		float L_1 = (__this->___r_0);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		float L_5 = (__this->___g_1);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_4;
		float L_9 = (__this->___b_2);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_157* L_12 = L_8;
		float L_13 = (__this->___a_3);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2695, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C" int32_t Color_GetHashCode_m6_232 (Color_t6_40 * __this, const MethodInfo* method)
{
	Vector4_t6_55  V_0 = {0};
	{
		Vector4_t6_55  L_0 = Color_op_Implicit_m6_240(NULL /*static, unused*/, (*(Color_t6_40 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m6_364((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern TypeInfo* Color_t6_40_il2cpp_TypeInfo_var;
extern "C" bool Color_Equals_m6_233 (Color_t6_40 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Color_t6_40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(942);
		s_Il2CppMethodIntialized = true;
	}
	Color_t6_40  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Color_t6_40_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Color_t6_40 *)((Color_t6_40 *)UnBox (L_1, Color_t6_40_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___r_0);
		float L_3 = ((&V_0)->___r_0);
		bool L_4 = Single_Equals_m1_474(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = &(__this->___g_1);
		float L_6 = ((&V_0)->___g_1);
		bool L_7 = Single_Equals_m1_474(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = &(__this->___b_2);
		float L_9 = ((&V_0)->___b_2);
		bool L_10 = Single_Equals_m1_474(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = &(__this->___a_3);
		float L_12 = ((&V_0)->___a_3);
		bool L_13 = Single_Equals_m1_474(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C" Color_t6_40  Color_get_red_m6_234 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = {0};
		Color__ctor_m6_229(&L_0, (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C" Color_t6_40  Color_get_green_m6_235 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = {0};
		Color__ctor_m6_229(&L_0, (0.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C" Color_t6_40  Color_get_white_m6_236 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = {0};
		Color__ctor_m6_229(&L_0, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_yellow()
extern "C" Color_t6_40  Color_get_yellow_m6_237 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = {0};
		Color__ctor_m6_229(&L_0, (1.0f), (0.921568632f), (0.0156862754f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_cyan()
extern "C" Color_t6_40  Color_get_cyan_m6_238 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = {0};
		Color__ctor_m6_229(&L_0, (0.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern "C" Color_t6_40  Color_op_Multiply_m6_239 (Object_t * __this /* static, unused */, Color_t6_40  ___a, float ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___r_0);
		float L_1 = ___b;
		float L_2 = ((&___a)->___g_1);
		float L_3 = ___b;
		float L_4 = ((&___a)->___b_2);
		float L_5 = ___b;
		float L_6 = ((&___a)->___a_3);
		float L_7 = ___b;
		Color_t6_40  L_8 = {0};
		Color__ctor_m6_229(&L_8, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C" Vector4_t6_55  Color_op_Implicit_m6_240 (Object_t * __this /* static, unused */, Color_t6_40  ___c, const MethodInfo* method)
{
	{
		float L_0 = ((&___c)->___r_0);
		float L_1 = ((&___c)->___g_1);
		float L_2 = ((&___c)->___b_2);
		float L_3 = ((&___c)->___a_3);
		Vector4_t6_55  L_4 = {0};
		Vector4__ctor_m6_363(&L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C" void Color32__ctor_m6_241 (Color32_t6_50 * __this, uint8_t ___r, uint8_t ___g, uint8_t ___b, uint8_t ___a, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___r;
		__this->___r_0 = L_0;
		uint8_t L_1 = ___g;
		__this->___g_1 = L_1;
		uint8_t L_2 = ___b;
		__this->___b_2 = L_2;
		uint8_t L_3 = ___a;
		__this->___a_3 = L_3;
		return;
	}
}
// System.String UnityEngine.Color32::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2696;
extern "C" String_t* Color32_ToString_m6_242 (Color32_t6_50 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		_stringLiteral2696 = il2cpp_codegen_string_literal_from_index(2696);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		uint8_t L_1 = (__this->___r_0);
		uint8_t L_2 = L_1;
		Object_t * L_3 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		uint8_t L_5 = (__this->___g_1);
		uint8_t L_6 = L_5;
		Object_t * L_7 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_4;
		uint8_t L_9 = (__this->___b_2);
		uint8_t L_10 = L_9;
		Object_t * L_11 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_157* L_12 = L_8;
		uint8_t L_13 = (__this->___a_3);
		uint8_t L_14 = L_13;
		Object_t * L_15 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2696, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Quaternion__ctor_m6_243 (Quaternion_t6_51 * __this, float ___x, float ___y, float ___z, float ___w, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		float L_2 = ___z;
		__this->___z_3 = L_2;
		float L_3 = ___w;
		__this->___w_4 = L_3;
		return;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" Quaternion_t6_51  Quaternion_get_identity_m6_244 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Quaternion_t6_51  L_0 = {0};
		Quaternion__ctor_m6_243(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" float Quaternion_Dot_m6_245 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___a, Quaternion_t6_51  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		float L_6 = ((&___a)->___w_4);
		float L_7 = ((&___b)->___w_4);
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern "C" Quaternion_t6_51  Quaternion_LookRotation_m6_246 (Object_t * __this /* static, unused */, Vector3_t6_49  ___forward, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	{
		Vector3_t6_49  L_0 = Vector3_get_up_m6_217(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Quaternion_t6_51  L_1 = Quaternion_INTERNAL_CALL_LookRotation_m6_247(NULL /*static, unused*/, (&___forward), (&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" Quaternion_t6_51  Quaternion_INTERNAL_CALL_LookRotation_m6_247 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___forward, Vector3_t6_49 * ___upwards, const MethodInfo* method)
{
	typedef Quaternion_t6_51  (*Quaternion_INTERNAL_CALL_LookRotation_m6_247_ftn) (Vector3_t6_49 *, Vector3_t6_49 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m6_247_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m6_247_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___forward, ___upwards);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C" Quaternion_t6_51  Quaternion_Slerp_m6_248 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___a, Quaternion_t6_51  ___b, float ___t, const MethodInfo* method)
{
	{
		float L_0 = ___t;
		Quaternion_t6_51  L_1 = Quaternion_INTERNAL_CALL_Slerp_m6_249(NULL /*static, unused*/, (&___a), (&___b), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)
extern "C" Quaternion_t6_51  Quaternion_INTERNAL_CALL_Slerp_m6_249 (Object_t * __this /* static, unused */, Quaternion_t6_51 * ___a, Quaternion_t6_51 * ___b, float ___t, const MethodInfo* method)
{
	typedef Quaternion_t6_51  (*Quaternion_INTERNAL_CALL_Slerp_m6_249_ftn) (Quaternion_t6_51 *, Quaternion_t6_51 *, float);
	static Quaternion_INTERNAL_CALL_Slerp_m6_249_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Slerp_m6_249_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)");
	return _il2cpp_icall_func(___a, ___b, ___t);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::SlerpUnclamped(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C" Quaternion_t6_51  Quaternion_SlerpUnclamped_m6_250 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___a, Quaternion_t6_51  ___b, float ___t, const MethodInfo* method)
{
	{
		float L_0 = ___t;
		Quaternion_t6_51  L_1 = Quaternion_INTERNAL_CALL_SlerpUnclamped_m6_251(NULL /*static, unused*/, (&___a), (&___b), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)
extern "C" Quaternion_t6_51  Quaternion_INTERNAL_CALL_SlerpUnclamped_m6_251 (Object_t * __this /* static, unused */, Quaternion_t6_51 * ___a, Quaternion_t6_51 * ___b, float ___t, const MethodInfo* method)
{
	typedef Quaternion_t6_51  (*Quaternion_INTERNAL_CALL_SlerpUnclamped_m6_251_ftn) (Quaternion_t6_51 *, Quaternion_t6_51 *, float);
	static Quaternion_INTERNAL_CALL_SlerpUnclamped_m6_251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_SlerpUnclamped_m6_251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)");
	return _il2cpp_icall_func(___a, ___b, ___t);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C" Quaternion_t6_51  Quaternion_Lerp_m6_252 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___a, Quaternion_t6_51  ___b, float ___t, const MethodInfo* method)
{
	{
		float L_0 = ___t;
		Quaternion_t6_51  L_1 = Quaternion_INTERNAL_CALL_Lerp_m6_253(NULL /*static, unused*/, (&___a), (&___b), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)
extern "C" Quaternion_t6_51  Quaternion_INTERNAL_CALL_Lerp_m6_253 (Object_t * __this /* static, unused */, Quaternion_t6_51 * ___a, Quaternion_t6_51 * ___b, float ___t, const MethodInfo* method)
{
	typedef Quaternion_t6_51  (*Quaternion_INTERNAL_CALL_Lerp_m6_253_ftn) (Quaternion_t6_51 *, Quaternion_t6_51 *, float);
	static Quaternion_INTERNAL_CALL_Lerp_m6_253_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Lerp_m6_253_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)");
	return _il2cpp_icall_func(___a, ___b, ___t);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::RotateTowards(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Quaternion_t6_51  Quaternion_RotateTowards_m6_254 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___from, Quaternion_t6_51  ___to, float ___maxDegreesDelta, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Quaternion_t6_51  L_0 = ___from;
		Quaternion_t6_51  L_1 = ___to;
		float L_2 = Quaternion_Angle_m6_258(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		Quaternion_t6_51  L_4 = ___to;
		return L_4;
	}

IL_0015:
	{
		float L_5 = ___maxDegreesDelta;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Min_m6_381(NULL /*static, unused*/, (1.0f), ((float)((float)L_5/(float)L_6)), /*hidden argument*/NULL);
		V_1 = L_7;
		Quaternion_t6_51  L_8 = ___from;
		Quaternion_t6_51  L_9 = ___to;
		float L_10 = V_1;
		Quaternion_t6_51  L_11 = Quaternion_SlerpUnclamped_m6_250(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C" Quaternion_t6_51  Quaternion_Inverse_m6_255 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___rotation, const MethodInfo* method)
{
	{
		Quaternion_t6_51  L_0 = Quaternion_INTERNAL_CALL_Inverse_m6_256(NULL /*static, unused*/, (&___rotation), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&)
extern "C" Quaternion_t6_51  Quaternion_INTERNAL_CALL_Inverse_m6_256 (Object_t * __this /* static, unused */, Quaternion_t6_51 * ___rotation, const MethodInfo* method)
{
	typedef Quaternion_t6_51  (*Quaternion_INTERNAL_CALL_Inverse_m6_256_ftn) (Quaternion_t6_51 *);
	static Quaternion_INTERNAL_CALL_Inverse_m6_256_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m6_256_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___rotation);
}
// System.String UnityEngine.Quaternion::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2697;
extern "C" String_t* Quaternion_ToString_m6_257 (Quaternion_t6_51 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		_stringLiteral2697 = il2cpp_codegen_string_literal_from_index(2697);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_4;
		float L_9 = (__this->___z_3);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_157* L_12 = L_8;
		float L_13 = (__this->___w_4);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2697, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Quaternion_Angle_m6_258 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___a, Quaternion_t6_51  ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Quaternion_t6_51  L_0 = ___a;
		Quaternion_t6_51  L_1 = ___b;
		float L_2 = Quaternion_Dot_m6_245(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_4 = fabsf(L_3);
		float L_5 = Mathf_Min_m6_381(NULL /*static, unused*/, L_4, (1.0f), /*hidden argument*/NULL);
		float L_6 = acosf(L_5);
		return ((float)((float)((float)((float)L_6*(float)(2.0f)))*(float)(57.29578f)));
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C" Vector3_t6_49  Quaternion_get_eulerAngles_m6_259 (Quaternion_t6_51 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Quaternion_Internal_ToEulerRad_m6_261(NULL /*static, unused*/, (*(Quaternion_t6_51 *)__this), /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_0, (57.29578f), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C" Quaternion_t6_51  Quaternion_Euler_m6_260 (Object_t * __this /* static, unused */, float ___x, float ___y, float ___z, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		float L_1 = ___y;
		float L_2 = ___z;
		Vector3_t6_49  L_3 = {0};
		Vector3__ctor_m6_191(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t6_49  L_4 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t6_51  L_5 = Quaternion_Internal_FromEulerRad_m6_263(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C" Vector3_t6_49  Quaternion_Internal_ToEulerRad_m6_261 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___rotation, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m6_262(NULL /*static, unused*/, (&___rotation), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&)
extern "C" Vector3_t6_49  Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m6_262 (Object_t * __this /* static, unused */, Quaternion_t6_51 * ___rotation, const MethodInfo* method)
{
	typedef Vector3_t6_49  (*Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m6_262_ftn) (Quaternion_t6_51 *);
	static Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m6_262_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m6_262_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___rotation);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C" Quaternion_t6_51  Quaternion_Internal_FromEulerRad_m6_263 (Object_t * __this /* static, unused */, Vector3_t6_49  ___euler, const MethodInfo* method)
{
	{
		Quaternion_t6_51  L_0 = Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6_264(NULL /*static, unused*/, (&___euler), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&)
extern "C" Quaternion_t6_51  Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6_264 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___euler, const MethodInfo* method)
{
	typedef Quaternion_t6_51  (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6_264_ftn) (Vector3_t6_49 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6_264_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6_264_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___euler);
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C" int32_t Quaternion_GetHashCode_m6_265 (Quaternion_t6_51 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m1_475(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m1_475(L_2, /*hidden argument*/NULL);
		float* L_4 = &(__this->___z_3);
		int32_t L_5 = Single_GetHashCode_m1_475(L_4, /*hidden argument*/NULL);
		float* L_6 = &(__this->___w_4);
		int32_t L_7 = Single_GetHashCode_m1_475(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern TypeInfo* Quaternion_t6_51_il2cpp_TypeInfo_var;
extern "C" bool Quaternion_Equals_m6_266 (Quaternion_t6_51 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Quaternion_t6_51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(943);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t6_51  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Quaternion_t6_51_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Quaternion_t6_51 *)((Quaternion_t6_51 *)UnBox (L_1, Quaternion_t6_51_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m1_474(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m1_474(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = &(__this->___z_3);
		float L_9 = ((&V_0)->___z_3);
		bool L_10 = Single_Equals_m1_474(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = &(__this->___w_4);
		float L_12 = ((&V_0)->___w_4);
		bool L_13 = Single_Equals_m1_474(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" Quaternion_t6_51  Quaternion_op_Multiply_m6_267 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___lhs, Quaternion_t6_51  ___rhs, const MethodInfo* method)
{
	{
		float L_0 = ((&___lhs)->___w_4);
		float L_1 = ((&___rhs)->___x_1);
		float L_2 = ((&___lhs)->___x_1);
		float L_3 = ((&___rhs)->___w_4);
		float L_4 = ((&___lhs)->___y_2);
		float L_5 = ((&___rhs)->___z_3);
		float L_6 = ((&___lhs)->___z_3);
		float L_7 = ((&___rhs)->___y_2);
		float L_8 = ((&___lhs)->___w_4);
		float L_9 = ((&___rhs)->___y_2);
		float L_10 = ((&___lhs)->___y_2);
		float L_11 = ((&___rhs)->___w_4);
		float L_12 = ((&___lhs)->___z_3);
		float L_13 = ((&___rhs)->___x_1);
		float L_14 = ((&___lhs)->___x_1);
		float L_15 = ((&___rhs)->___z_3);
		float L_16 = ((&___lhs)->___w_4);
		float L_17 = ((&___rhs)->___z_3);
		float L_18 = ((&___lhs)->___z_3);
		float L_19 = ((&___rhs)->___w_4);
		float L_20 = ((&___lhs)->___x_1);
		float L_21 = ((&___rhs)->___y_2);
		float L_22 = ((&___lhs)->___y_2);
		float L_23 = ((&___rhs)->___x_1);
		float L_24 = ((&___lhs)->___w_4);
		float L_25 = ((&___rhs)->___w_4);
		float L_26 = ((&___lhs)->___x_1);
		float L_27 = ((&___rhs)->___x_1);
		float L_28 = ((&___lhs)->___y_2);
		float L_29 = ((&___rhs)->___y_2);
		float L_30 = ((&___lhs)->___z_3);
		float L_31 = ((&___rhs)->___z_3);
		Quaternion_t6_51  L_32 = {0};
		Quaternion__ctor_m6_243(&L_32, ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		return L_32;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" Vector3_t6_49  Quaternion_op_Multiply_m6_268 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___rotation, Vector3_t6_49  ___point, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t6_49  V_12 = {0};
	{
		float L_0 = ((&___rotation)->___x_1);
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = ((&___rotation)->___y_2);
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = ((&___rotation)->___z_3);
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = ((&___rotation)->___x_1);
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = ((&___rotation)->___y_2);
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = ((&___rotation)->___z_3);
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = ((&___rotation)->___x_1);
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = ((&___rotation)->___x_1);
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = ((&___rotation)->___y_2);
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = ((&___rotation)->___w_4);
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = ((&___rotation)->___w_4);
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = ((&___rotation)->___w_4);
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = ((&___point)->___x_1);
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = ((&___point)->___y_2);
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = ((&___point)->___z_3);
		(&V_12)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = ((&___point)->___x_1);
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = ((&___point)->___y_2);
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = ((&___point)->___z_3);
		(&V_12)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = ((&___point)->___x_1);
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = ((&___point)->___y_2);
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = ((&___point)->___z_3);
		(&V_12)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47))));
		Vector3_t6_49  L_48 = V_12;
		return L_48;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" bool Quaternion_op_Inequality_m6_269 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___lhs, Quaternion_t6_51  ___rhs, const MethodInfo* method)
{
	{
		Quaternion_t6_51  L_0 = ___lhs;
		Quaternion_t6_51  L_1 = ___rhs;
		float L_2 = Quaternion_Dot_m6_245(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)((!(((float)L_2) <= ((float)(0.999999f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Rect__ctor_m6_270 (Rect_t6_52 * __this, float ___x, float ___y, float ___width, float ___height, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___m_XMin_0 = L_0;
		float L_1 = ___y;
		__this->___m_YMin_1 = L_1;
		float L_2 = ___width;
		__this->___m_Width_2 = L_2;
		float L_3 = ___height;
		__this->___m_Height_3 = L_3;
		return;
	}
}
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C" void Rect__ctor_m6_271 (Rect_t6_52 * __this, Rect_t6_52  ___source, const MethodInfo* method)
{
	{
		float L_0 = ((&___source)->___m_XMin_0);
		__this->___m_XMin_0 = L_0;
		float L_1 = ((&___source)->___m_YMin_1);
		__this->___m_YMin_1 = L_1;
		float L_2 = ((&___source)->___m_Width_2);
		__this->___m_Width_2 = L_2;
		float L_3 = ((&___source)->___m_Height_3);
		__this->___m_Height_3 = L_3;
		return;
	}
}
// UnityEngine.Rect UnityEngine.Rect::MinMaxRect(System.Single,System.Single,System.Single,System.Single)
extern "C" Rect_t6_52  Rect_MinMaxRect_m6_272 (Object_t * __this /* static, unused */, float ___xmin, float ___ymin, float ___xmax, float ___ymax, const MethodInfo* method)
{
	{
		float L_0 = ___xmin;
		float L_1 = ___ymin;
		float L_2 = ___xmax;
		float L_3 = ___xmin;
		float L_4 = ___ymax;
		float L_5 = ___ymin;
		Rect_t6_52  L_6 = {0};
		Rect__ctor_m6_270(&L_6, L_0, L_1, ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Single UnityEngine.Rect::get_x()
extern "C" float Rect_get_x_m6_273 (Rect_t6_52 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_XMin_0);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C" void Rect_set_x_m6_274 (Rect_t6_52 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_XMin_0 = L_0;
		return;
	}
}
// System.Single UnityEngine.Rect::get_y()
extern "C" float Rect_get_y_m6_275 (Rect_t6_52 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_YMin_1);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C" void Rect_set_y_m6_276 (Rect_t6_52 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_YMin_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.Rect::get_width()
extern "C" float Rect_get_width_m6_277 (Rect_t6_52 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Width_2);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C" void Rect_set_width_m6_278 (Rect_t6_52 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Width_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.Rect::get_height()
extern "C" float Rect_get_height_m6_279 (Rect_t6_52 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Height_3);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C" void Rect_set_height_m6_280 (Rect_t6_52 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Height_3 = L_0;
		return;
	}
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C" float Rect_get_xMin_m6_281 (Rect_t6_52 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_XMin_0);
		return L_0;
	}
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C" float Rect_get_yMin_m6_282 (Rect_t6_52 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_YMin_1);
		return L_0;
	}
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C" float Rect_get_xMax_m6_283 (Rect_t6_52 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Width_2);
		float L_1 = (__this->___m_XMin_0);
		return ((float)((float)L_0+(float)L_1));
	}
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C" float Rect_get_yMax_m6_284 (Rect_t6_52 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Height_3);
		float L_1 = (__this->___m_YMin_1);
		return ((float)((float)L_0+(float)L_1));
	}
}
// System.String UnityEngine.Rect::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2698;
extern "C" String_t* Rect_ToString_m6_285 (Rect_t6_52 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		_stringLiteral2698 = il2cpp_codegen_string_literal_from_index(2698);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		float L_1 = Rect_get_x_m6_273(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		float L_5 = Rect_get_y_m6_275(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_4;
		float L_9 = Rect_get_width_m6_277(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_157* L_12 = L_8;
		float L_13 = Rect_get_height_m6_279(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2698, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C" bool Rect_Contains_m6_286 (Rect_t6_52 * __this, Vector2_t6_48  ___point, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = ((&___point)->___x_1);
		float L_1 = Rect_get_xMin_m6_281(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = ((&___point)->___x_1);
		float L_3 = Rect_get_xMax_m6_283(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = ((&___point)->___y_2);
		float L_5 = Rect_get_yMin_m6_282(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = ((&___point)->___y_2);
		float L_7 = Rect_get_yMax_m6_284(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return G_B5_0;
	}
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C" bool Rect_Contains_m6_287 (Rect_t6_52 * __this, Vector3_t6_49  ___point, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = ((&___point)->___x_1);
		float L_1 = Rect_get_xMin_m6_281(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = ((&___point)->___x_1);
		float L_3 = Rect_get_xMax_m6_283(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = ((&___point)->___y_2);
		float L_5 = Rect_get_yMin_m6_282(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = ((&___point)->___y_2);
		float L_7 = Rect_get_yMax_m6_284(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return G_B5_0;
	}
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C" int32_t Rect_GetHashCode_m6_288 (Rect_t6_52 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = Rect_get_x_m6_273(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m1_475((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m6_277(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m1_475((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m6_275(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m1_475((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m6_279(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m1_475((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern TypeInfo* Rect_t6_52_il2cpp_TypeInfo_var;
extern "C" bool Rect_Equals_m6_289 (Rect_t6_52 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Rect_t6_52_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(944);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t6_52  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Rect_t6_52_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Rect_t6_52 *)((Rect_t6_52 *)UnBox (L_1, Rect_t6_52_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m6_273(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Rect_get_x_m6_273((&V_0), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m1_474((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007a;
		}
	}
	{
		float L_5 = Rect_get_y_m6_275(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_y_m6_275((&V_0), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m1_474((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007a;
		}
	}
	{
		float L_8 = Rect_get_width_m6_277(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = Rect_get_width_m6_277((&V_0), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m1_474((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007a;
		}
	}
	{
		float L_11 = Rect_get_height_m6_279(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = Rect_get_height_m6_279((&V_0), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m1_474((&V_4), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_007b;
	}

IL_007a:
	{
		G_B7_0 = 0;
	}

IL_007b:
	{
		return G_B7_0;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C" bool Rect_op_Equality_m6_290 (Object_t * __this /* static, unused */, Rect_t6_52  ___lhs, Rect_t6_52  ___rhs, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m6_273((&___lhs), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m6_273((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004b;
		}
	}
	{
		float L_2 = Rect_get_y_m6_275((&___lhs), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m6_275((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004b;
		}
	}
	{
		float L_4 = Rect_get_width_m6_277((&___lhs), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m6_277((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004b;
		}
	}
	{
		float L_6 = Rect_get_height_m6_279((&___lhs), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m6_279((&___rhs), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 0;
	}

IL_004c:
	{
		return G_B5_0;
	}
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C" float Matrix4x4_get_Item_m6_291 (Matrix4x4_t6_53 * __this, int32_t ___row, int32_t ___column, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row;
		int32_t L_1 = ___column;
		float L_2 = Matrix4x4_get_Item_m6_293(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
extern "C" void Matrix4x4_set_Item_m6_292 (Matrix4x4_t6_53 * __this, int32_t ___row, int32_t ___column, float ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row;
		int32_t L_1 = ___column;
		float L_2 = ___value;
		Matrix4x4_set_Item_m6_294(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1_731_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2699;
extern "C" float Matrix4x4_get_Item_m6_293 (Matrix4x4_t6_53 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_731_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		_stringLiteral2699 = il2cpp_codegen_string_literal_from_index(2699);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0054;
		}
		if (L_1 == 2)
		{
			goto IL_005b;
		}
		if (L_1 == 3)
		{
			goto IL_0062;
		}
		if (L_1 == 4)
		{
			goto IL_0069;
		}
		if (L_1 == 5)
		{
			goto IL_0070;
		}
		if (L_1 == 6)
		{
			goto IL_0077;
		}
		if (L_1 == 7)
		{
			goto IL_007e;
		}
		if (L_1 == 8)
		{
			goto IL_0085;
		}
		if (L_1 == 9)
		{
			goto IL_008c;
		}
		if (L_1 == 10)
		{
			goto IL_0093;
		}
		if (L_1 == 11)
		{
			goto IL_009a;
		}
		if (L_1 == 12)
		{
			goto IL_00a1;
		}
		if (L_1 == 13)
		{
			goto IL_00a8;
		}
		if (L_1 == 14)
		{
			goto IL_00af;
		}
		if (L_1 == 15)
		{
			goto IL_00b6;
		}
	}
	{
		goto IL_00bd;
	}

IL_004d:
	{
		float L_2 = (__this->___m00_0);
		return L_2;
	}

IL_0054:
	{
		float L_3 = (__this->___m10_1);
		return L_3;
	}

IL_005b:
	{
		float L_4 = (__this->___m20_2);
		return L_4;
	}

IL_0062:
	{
		float L_5 = (__this->___m30_3);
		return L_5;
	}

IL_0069:
	{
		float L_6 = (__this->___m01_4);
		return L_6;
	}

IL_0070:
	{
		float L_7 = (__this->___m11_5);
		return L_7;
	}

IL_0077:
	{
		float L_8 = (__this->___m21_6);
		return L_8;
	}

IL_007e:
	{
		float L_9 = (__this->___m31_7);
		return L_9;
	}

IL_0085:
	{
		float L_10 = (__this->___m02_8);
		return L_10;
	}

IL_008c:
	{
		float L_11 = (__this->___m12_9);
		return L_11;
	}

IL_0093:
	{
		float L_12 = (__this->___m22_10);
		return L_12;
	}

IL_009a:
	{
		float L_13 = (__this->___m32_11);
		return L_13;
	}

IL_00a1:
	{
		float L_14 = (__this->___m03_12);
		return L_14;
	}

IL_00a8:
	{
		float L_15 = (__this->___m13_13);
		return L_15;
	}

IL_00af:
	{
		float L_16 = (__this->___m23_14);
		return L_16;
	}

IL_00b6:
	{
		float L_17 = (__this->___m33_15);
		return L_17;
	}

IL_00bd:
	{
		IndexOutOfRangeException_t1_731 * L_18 = (IndexOutOfRangeException_t1_731 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_731_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_5085(L_18, _stringLiteral2699, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1_731_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2699;
extern "C" void Matrix4x4_set_Item_m6_294 (Matrix4x4_t6_53 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_731_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		_stringLiteral2699 = il2cpp_codegen_string_literal_from_index(2699);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0059;
		}
		if (L_1 == 2)
		{
			goto IL_0065;
		}
		if (L_1 == 3)
		{
			goto IL_0071;
		}
		if (L_1 == 4)
		{
			goto IL_007d;
		}
		if (L_1 == 5)
		{
			goto IL_0089;
		}
		if (L_1 == 6)
		{
			goto IL_0095;
		}
		if (L_1 == 7)
		{
			goto IL_00a1;
		}
		if (L_1 == 8)
		{
			goto IL_00ad;
		}
		if (L_1 == 9)
		{
			goto IL_00b9;
		}
		if (L_1 == 10)
		{
			goto IL_00c5;
		}
		if (L_1 == 11)
		{
			goto IL_00d1;
		}
		if (L_1 == 12)
		{
			goto IL_00dd;
		}
		if (L_1 == 13)
		{
			goto IL_00e9;
		}
		if (L_1 == 14)
		{
			goto IL_00f5;
		}
		if (L_1 == 15)
		{
			goto IL_0101;
		}
	}
	{
		goto IL_010d;
	}

IL_004d:
	{
		float L_2 = ___value;
		__this->___m00_0 = L_2;
		goto IL_0118;
	}

IL_0059:
	{
		float L_3 = ___value;
		__this->___m10_1 = L_3;
		goto IL_0118;
	}

IL_0065:
	{
		float L_4 = ___value;
		__this->___m20_2 = L_4;
		goto IL_0118;
	}

IL_0071:
	{
		float L_5 = ___value;
		__this->___m30_3 = L_5;
		goto IL_0118;
	}

IL_007d:
	{
		float L_6 = ___value;
		__this->___m01_4 = L_6;
		goto IL_0118;
	}

IL_0089:
	{
		float L_7 = ___value;
		__this->___m11_5 = L_7;
		goto IL_0118;
	}

IL_0095:
	{
		float L_8 = ___value;
		__this->___m21_6 = L_8;
		goto IL_0118;
	}

IL_00a1:
	{
		float L_9 = ___value;
		__this->___m31_7 = L_9;
		goto IL_0118;
	}

IL_00ad:
	{
		float L_10 = ___value;
		__this->___m02_8 = L_10;
		goto IL_0118;
	}

IL_00b9:
	{
		float L_11 = ___value;
		__this->___m12_9 = L_11;
		goto IL_0118;
	}

IL_00c5:
	{
		float L_12 = ___value;
		__this->___m22_10 = L_12;
		goto IL_0118;
	}

IL_00d1:
	{
		float L_13 = ___value;
		__this->___m32_11 = L_13;
		goto IL_0118;
	}

IL_00dd:
	{
		float L_14 = ___value;
		__this->___m03_12 = L_14;
		goto IL_0118;
	}

IL_00e9:
	{
		float L_15 = ___value;
		__this->___m13_13 = L_15;
		goto IL_0118;
	}

IL_00f5:
	{
		float L_16 = ___value;
		__this->___m23_14 = L_16;
		goto IL_0118;
	}

IL_0101:
	{
		float L_17 = ___value;
		__this->___m33_15 = L_17;
		goto IL_0118;
	}

IL_010d:
	{
		IndexOutOfRangeException_t1_731 * L_18 = (IndexOutOfRangeException_t1_731 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_731_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_5085(L_18, _stringLiteral2699, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_0118:
	{
		return;
	}
}
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C" int32_t Matrix4x4_GetHashCode_m6_295 (Matrix4x4_t6_53 * __this, const MethodInfo* method)
{
	Vector4_t6_55  V_0 = {0};
	Vector4_t6_55  V_1 = {0};
	Vector4_t6_55  V_2 = {0};
	Vector4_t6_55  V_3 = {0};
	{
		Vector4_t6_55  L_0 = Matrix4x4_GetColumn_m6_306(__this, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m6_364((&V_0), /*hidden argument*/NULL);
		Vector4_t6_55  L_2 = Matrix4x4_GetColumn_m6_306(__this, 1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector4_GetHashCode_m6_364((&V_1), /*hidden argument*/NULL);
		Vector4_t6_55  L_4 = Matrix4x4_GetColumn_m6_306(__this, 2, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Vector4_GetHashCode_m6_364((&V_2), /*hidden argument*/NULL);
		Vector4_t6_55  L_6 = Matrix4x4_GetColumn_m6_306(__this, 3, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Vector4_GetHashCode_m6_364((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern TypeInfo* Matrix4x4_t6_53_il2cpp_TypeInfo_var;
extern TypeInfo* Vector4_t6_55_il2cpp_TypeInfo_var;
extern "C" bool Matrix4x4_Equals_m6_296 (Matrix4x4_t6_53 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t6_53_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(945);
		Vector4_t6_55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(934);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t6_53  V_0 = {0};
	Vector4_t6_55  V_1 = {0};
	Vector4_t6_55  V_2 = {0};
	Vector4_t6_55  V_3 = {0};
	Vector4_t6_55  V_4 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Matrix4x4_t6_53_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Matrix4x4_t6_53 *)((Matrix4x4_t6_53 *)UnBox (L_1, Matrix4x4_t6_53_il2cpp_TypeInfo_var))));
		Vector4_t6_55  L_2 = Matrix4x4_GetColumn_m6_306(__this, 0, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector4_t6_55  L_3 = Matrix4x4_GetColumn_m6_306((&V_0), 0, /*hidden argument*/NULL);
		Vector4_t6_55  L_4 = L_3;
		Object_t * L_5 = Box(Vector4_t6_55_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector4_Equals_m6_365((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t6_55  L_7 = Matrix4x4_GetColumn_m6_306(__this, 1, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector4_t6_55  L_8 = Matrix4x4_GetColumn_m6_306((&V_0), 1, /*hidden argument*/NULL);
		Vector4_t6_55  L_9 = L_8;
		Object_t * L_10 = Box(Vector4_t6_55_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector4_Equals_m6_365((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t6_55  L_12 = Matrix4x4_GetColumn_m6_306(__this, 2, /*hidden argument*/NULL);
		V_3 = L_12;
		Vector4_t6_55  L_13 = Matrix4x4_GetColumn_m6_306((&V_0), 2, /*hidden argument*/NULL);
		Vector4_t6_55  L_14 = L_13;
		Object_t * L_15 = Box(Vector4_t6_55_il2cpp_TypeInfo_var, &L_14);
		bool L_16 = Vector4_Equals_m6_365((&V_3), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t6_55  L_17 = Matrix4x4_GetColumn_m6_306(__this, 3, /*hidden argument*/NULL);
		V_4 = L_17;
		Vector4_t6_55  L_18 = Matrix4x4_GetColumn_m6_306((&V_0), 3, /*hidden argument*/NULL);
		Vector4_t6_55  L_19 = L_18;
		Object_t * L_20 = Box(Vector4_t6_55_il2cpp_TypeInfo_var, &L_19);
		bool L_21 = Vector4_Equals_m6_365((&V_4), L_20, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_21));
		goto IL_0097;
	}

IL_0096:
	{
		G_B7_0 = 0;
	}

IL_0097:
	{
		return G_B7_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Inverse(UnityEngine.Matrix4x4)
extern "C" Matrix4x4_t6_53  Matrix4x4_Inverse_m6_297 (Object_t * __this /* static, unused */, Matrix4x4_t6_53  ___m, const MethodInfo* method)
{
	{
		Matrix4x4_t6_53  L_0 = Matrix4x4_INTERNAL_CALL_Inverse_m6_298(NULL /*static, unused*/, (&___m), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&)
extern "C" Matrix4x4_t6_53  Matrix4x4_INTERNAL_CALL_Inverse_m6_298 (Object_t * __this /* static, unused */, Matrix4x4_t6_53 * ___m, const MethodInfo* method)
{
	typedef Matrix4x4_t6_53  (*Matrix4x4_INTERNAL_CALL_Inverse_m6_298_ftn) (Matrix4x4_t6_53 *);
	static Matrix4x4_INTERNAL_CALL_Inverse_m6_298_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Inverse_m6_298_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&)");
	return _il2cpp_icall_func(___m);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Transpose(UnityEngine.Matrix4x4)
extern "C" Matrix4x4_t6_53  Matrix4x4_Transpose_m6_299 (Object_t * __this /* static, unused */, Matrix4x4_t6_53  ___m, const MethodInfo* method)
{
	{
		Matrix4x4_t6_53  L_0 = Matrix4x4_INTERNAL_CALL_Transpose_m6_300(NULL /*static, unused*/, (&___m), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_Transpose(UnityEngine.Matrix4x4&)
extern "C" Matrix4x4_t6_53  Matrix4x4_INTERNAL_CALL_Transpose_m6_300 (Object_t * __this /* static, unused */, Matrix4x4_t6_53 * ___m, const MethodInfo* method)
{
	typedef Matrix4x4_t6_53  (*Matrix4x4_INTERNAL_CALL_Transpose_m6_300_ftn) (Matrix4x4_t6_53 *);
	static Matrix4x4_INTERNAL_CALL_Transpose_m6_300_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Transpose_m6_300_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Transpose(UnityEngine.Matrix4x4&)");
	return _il2cpp_icall_func(___m);
}
// System.Boolean UnityEngine.Matrix4x4::Invert(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4&)
extern "C" bool Matrix4x4_Invert_m6_301 (Object_t * __this /* static, unused */, Matrix4x4_t6_53  ___inMatrix, Matrix4x4_t6_53 * ___dest, const MethodInfo* method)
{
	{
		Matrix4x4_t6_53 * L_0 = ___dest;
		bool L_1 = Matrix4x4_INTERNAL_CALL_Invert_m6_302(NULL /*static, unused*/, (&___inMatrix), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Matrix4x4::INTERNAL_CALL_Invert(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
extern "C" bool Matrix4x4_INTERNAL_CALL_Invert_m6_302 (Object_t * __this /* static, unused */, Matrix4x4_t6_53 * ___inMatrix, Matrix4x4_t6_53 * ___dest, const MethodInfo* method)
{
	typedef bool (*Matrix4x4_INTERNAL_CALL_Invert_m6_302_ftn) (Matrix4x4_t6_53 *, Matrix4x4_t6_53 *);
	static Matrix4x4_INTERNAL_CALL_Invert_m6_302_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Invert_m6_302_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Invert(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)");
	return _il2cpp_icall_func(___inMatrix, ___dest);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C" Matrix4x4_t6_53  Matrix4x4_get_inverse_m6_303 (Matrix4x4_t6_53 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t6_53  L_0 = Matrix4x4_Inverse_m6_297(NULL /*static, unused*/, (*(Matrix4x4_t6_53 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_transpose()
extern "C" Matrix4x4_t6_53  Matrix4x4_get_transpose_m6_304 (Matrix4x4_t6_53 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t6_53  L_0 = Matrix4x4_Transpose_m6_299(NULL /*static, unused*/, (*(Matrix4x4_t6_53 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Matrix4x4::get_isIdentity()
extern "C" bool Matrix4x4_get_isIdentity_m6_305 (Matrix4x4_t6_53 * __this, const MethodInfo* method)
{
	typedef bool (*Matrix4x4_get_isIdentity_m6_305_ftn) (Matrix4x4_t6_53 *);
	static Matrix4x4_get_isIdentity_m6_305_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_get_isIdentity_m6_305_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::get_isIdentity()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C" Vector4_t6_55  Matrix4x4_GetColumn_m6_306 (Matrix4x4_t6_53 * __this, int32_t ___i, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = Matrix4x4_get_Item_m6_291(__this, 0, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = Matrix4x4_get_Item_m6_291(__this, 1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = Matrix4x4_get_Item_m6_291(__this, 2, L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = Matrix4x4_get_Item_m6_291(__this, 3, L_6, /*hidden argument*/NULL);
		Vector4_t6_55  L_8 = {0};
		Vector4__ctor_m6_363(&L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetRow(System.Int32)
extern "C" Vector4_t6_55  Matrix4x4_GetRow_m6_307 (Matrix4x4_t6_53 * __this, int32_t ___i, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = Matrix4x4_get_Item_m6_291(__this, L_0, 0, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = Matrix4x4_get_Item_m6_291(__this, L_2, 1, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = Matrix4x4_get_Item_m6_291(__this, L_4, 2, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = Matrix4x4_get_Item_m6_291(__this, L_6, 3, /*hidden argument*/NULL);
		Vector4_t6_55  L_8 = {0};
		Vector4__ctor_m6_363(&L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.Matrix4x4::SetColumn(System.Int32,UnityEngine.Vector4)
extern "C" void Matrix4x4_SetColumn_m6_308 (Matrix4x4_t6_53 * __this, int32_t ___i, Vector4_t6_55  ___v, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = ((&___v)->___x_0);
		Matrix4x4_set_Item_m6_292(__this, 0, L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = ((&___v)->___y_1);
		Matrix4x4_set_Item_m6_292(__this, 1, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = ((&___v)->___z_2);
		Matrix4x4_set_Item_m6_292(__this, 2, L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = ((&___v)->___w_3);
		Matrix4x4_set_Item_m6_292(__this, 3, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Matrix4x4::SetRow(System.Int32,UnityEngine.Vector4)
extern "C" void Matrix4x4_SetRow_m6_309 (Matrix4x4_t6_53 * __this, int32_t ___i, Vector4_t6_55  ___v, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = ((&___v)->___x_0);
		Matrix4x4_set_Item_m6_292(__this, L_0, 0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = ((&___v)->___y_1);
		Matrix4x4_set_Item_m6_292(__this, L_2, 1, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = ((&___v)->___z_2);
		Matrix4x4_set_Item_m6_292(__this, L_4, 2, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = ((&___v)->___w_3);
		Matrix4x4_set_Item_m6_292(__this, L_6, 3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
extern "C" Vector3_t6_49  Matrix4x4_MultiplyPoint_m6_310 (Matrix4x4_t6_53 * __this, Vector3_t6_49  ___v, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	float V_1 = 0.0f;
	{
		float L_0 = (__this->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = (__this->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = (__this->___m02_8);
		float L_5 = ((&___v)->___z_3);
		float L_6 = (__this->___m03_12);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)L_6));
		float L_7 = (__this->___m10_1);
		float L_8 = ((&___v)->___x_1);
		float L_9 = (__this->___m11_5);
		float L_10 = ((&___v)->___y_2);
		float L_11 = (__this->___m12_9);
		float L_12 = ((&___v)->___z_3);
		float L_13 = (__this->___m13_13);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)L_10))))+(float)((float)((float)L_11*(float)L_12))))+(float)L_13));
		float L_14 = (__this->___m20_2);
		float L_15 = ((&___v)->___x_1);
		float L_16 = (__this->___m21_6);
		float L_17 = ((&___v)->___y_2);
		float L_18 = (__this->___m22_10);
		float L_19 = ((&___v)->___z_3);
		float L_20 = (__this->___m23_14);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17))))+(float)((float)((float)L_18*(float)L_19))))+(float)L_20));
		float L_21 = (__this->___m30_3);
		float L_22 = ((&___v)->___x_1);
		float L_23 = (__this->___m31_7);
		float L_24 = ((&___v)->___y_2);
		float L_25 = (__this->___m32_11);
		float L_26 = ((&___v)->___z_3);
		float L_27 = (__this->___m33_15);
		V_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_21*(float)L_22))+(float)((float)((float)L_23*(float)L_24))))+(float)((float)((float)L_25*(float)L_26))))+(float)L_27));
		float L_28 = V_1;
		V_1 = ((float)((float)(1.0f)/(float)L_28));
		Vector3_t6_49 * L_29 = (&V_0);
		float L_30 = (L_29->___x_1);
		float L_31 = V_1;
		L_29->___x_1 = ((float)((float)L_30*(float)L_31));
		Vector3_t6_49 * L_32 = (&V_0);
		float L_33 = (L_32->___y_2);
		float L_34 = V_1;
		L_32->___y_2 = ((float)((float)L_33*(float)L_34));
		Vector3_t6_49 * L_35 = (&V_0);
		float L_36 = (L_35->___z_3);
		float L_37 = V_1;
		L_35->___z_3 = ((float)((float)L_36*(float)L_37));
		Vector3_t6_49  L_38 = V_0;
		return L_38;
	}
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C" Vector3_t6_49  Matrix4x4_MultiplyPoint3x4_m6_311 (Matrix4x4_t6_53 * __this, Vector3_t6_49  ___v, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	{
		float L_0 = (__this->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = (__this->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = (__this->___m02_8);
		float L_5 = ((&___v)->___z_3);
		float L_6 = (__this->___m03_12);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)L_6));
		float L_7 = (__this->___m10_1);
		float L_8 = ((&___v)->___x_1);
		float L_9 = (__this->___m11_5);
		float L_10 = ((&___v)->___y_2);
		float L_11 = (__this->___m12_9);
		float L_12 = ((&___v)->___z_3);
		float L_13 = (__this->___m13_13);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)L_10))))+(float)((float)((float)L_11*(float)L_12))))+(float)L_13));
		float L_14 = (__this->___m20_2);
		float L_15 = ((&___v)->___x_1);
		float L_16 = (__this->___m21_6);
		float L_17 = ((&___v)->___y_2);
		float L_18 = (__this->___m22_10);
		float L_19 = ((&___v)->___z_3);
		float L_20 = (__this->___m23_14);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17))))+(float)((float)((float)L_18*(float)L_19))))+(float)L_20));
		Vector3_t6_49  L_21 = V_0;
		return L_21;
	}
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
extern "C" Vector3_t6_49  Matrix4x4_MultiplyVector_m6_312 (Matrix4x4_t6_53 * __this, Vector3_t6_49  ___v, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	{
		float L_0 = (__this->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = (__this->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = (__this->___m02_8);
		float L_5 = ((&___v)->___z_3);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
		float L_6 = (__this->___m10_1);
		float L_7 = ((&___v)->___x_1);
		float L_8 = (__this->___m11_5);
		float L_9 = ((&___v)->___y_2);
		float L_10 = (__this->___m12_9);
		float L_11 = ((&___v)->___z_3);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)L_6*(float)L_7))+(float)((float)((float)L_8*(float)L_9))))+(float)((float)((float)L_10*(float)L_11))));
		float L_12 = (__this->___m20_2);
		float L_13 = ((&___v)->___x_1);
		float L_14 = (__this->___m21_6);
		float L_15 = ((&___v)->___y_2);
		float L_16 = (__this->___m22_10);
		float L_17 = ((&___v)->___z_3);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)L_12*(float)L_13))+(float)((float)((float)L_14*(float)L_15))))+(float)((float)((float)L_16*(float)L_17))));
		Vector3_t6_49  L_18 = V_0;
		return L_18;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Scale(UnityEngine.Vector3)
extern TypeInfo* Matrix4x4_t6_53_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t6_53  Matrix4x4_Scale_m6_313 (Object_t * __this /* static, unused */, Vector3_t6_49  ___v, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t6_53_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(945);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t6_53  V_0 = {0};
	{
		Initobj (Matrix4x4_t6_53_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___v)->___x_1);
		(&V_0)->___m00_0 = L_0;
		(&V_0)->___m01_4 = (0.0f);
		(&V_0)->___m02_8 = (0.0f);
		(&V_0)->___m03_12 = (0.0f);
		(&V_0)->___m10_1 = (0.0f);
		float L_1 = ((&___v)->___y_2);
		(&V_0)->___m11_5 = L_1;
		(&V_0)->___m12_9 = (0.0f);
		(&V_0)->___m13_13 = (0.0f);
		(&V_0)->___m20_2 = (0.0f);
		(&V_0)->___m21_6 = (0.0f);
		float L_2 = ((&___v)->___z_3);
		(&V_0)->___m22_10 = L_2;
		(&V_0)->___m23_14 = (0.0f);
		(&V_0)->___m30_3 = (0.0f);
		(&V_0)->___m31_7 = (0.0f);
		(&V_0)->___m32_11 = (0.0f);
		(&V_0)->___m33_15 = (1.0f);
		Matrix4x4_t6_53  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_zero()
extern TypeInfo* Matrix4x4_t6_53_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t6_53  Matrix4x4_get_zero_m6_314 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t6_53_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(945);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t6_53  V_0 = {0};
	{
		Initobj (Matrix4x4_t6_53_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->___m00_0 = (0.0f);
		(&V_0)->___m01_4 = (0.0f);
		(&V_0)->___m02_8 = (0.0f);
		(&V_0)->___m03_12 = (0.0f);
		(&V_0)->___m10_1 = (0.0f);
		(&V_0)->___m11_5 = (0.0f);
		(&V_0)->___m12_9 = (0.0f);
		(&V_0)->___m13_13 = (0.0f);
		(&V_0)->___m20_2 = (0.0f);
		(&V_0)->___m21_6 = (0.0f);
		(&V_0)->___m22_10 = (0.0f);
		(&V_0)->___m23_14 = (0.0f);
		(&V_0)->___m30_3 = (0.0f);
		(&V_0)->___m31_7 = (0.0f);
		(&V_0)->___m32_11 = (0.0f);
		(&V_0)->___m33_15 = (0.0f);
		Matrix4x4_t6_53  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern TypeInfo* Matrix4x4_t6_53_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t6_53  Matrix4x4_get_identity_m6_315 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t6_53_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(945);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t6_53  V_0 = {0};
	{
		Initobj (Matrix4x4_t6_53_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->___m00_0 = (1.0f);
		(&V_0)->___m01_4 = (0.0f);
		(&V_0)->___m02_8 = (0.0f);
		(&V_0)->___m03_12 = (0.0f);
		(&V_0)->___m10_1 = (0.0f);
		(&V_0)->___m11_5 = (1.0f);
		(&V_0)->___m12_9 = (0.0f);
		(&V_0)->___m13_13 = (0.0f);
		(&V_0)->___m20_2 = (0.0f);
		(&V_0)->___m21_6 = (0.0f);
		(&V_0)->___m22_10 = (1.0f);
		(&V_0)->___m23_14 = (0.0f);
		(&V_0)->___m30_3 = (0.0f);
		(&V_0)->___m31_7 = (0.0f);
		(&V_0)->___m32_11 = (0.0f);
		(&V_0)->___m33_15 = (1.0f);
		Matrix4x4_t6_53  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Matrix4x4::SetTRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" void Matrix4x4_SetTRS_m6_316 (Matrix4x4_t6_53 * __this, Vector3_t6_49  ___pos, Quaternion_t6_51  ___q, Vector3_t6_49  ___s, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___pos;
		Quaternion_t6_51  L_1 = ___q;
		Vector3_t6_49  L_2 = ___s;
		Matrix4x4_t6_53  L_3 = Matrix4x4_TRS_m6_317(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		(*(Matrix4x4_t6_53 *)__this) = L_3;
		return;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" Matrix4x4_t6_53  Matrix4x4_TRS_m6_317 (Object_t * __this /* static, unused */, Vector3_t6_49  ___pos, Quaternion_t6_51  ___q, Vector3_t6_49  ___s, const MethodInfo* method)
{
	{
		Matrix4x4_t6_53  L_0 = Matrix4x4_INTERNAL_CALL_TRS_m6_318(NULL /*static, unused*/, (&___pos), (&___q), (&___s), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C" Matrix4x4_t6_53  Matrix4x4_INTERNAL_CALL_TRS_m6_318 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___pos, Quaternion_t6_51 * ___q, Vector3_t6_49 * ___s, const MethodInfo* method)
{
	typedef Matrix4x4_t6_53  (*Matrix4x4_INTERNAL_CALL_TRS_m6_318_ftn) (Vector3_t6_49 *, Quaternion_t6_51 *, Vector3_t6_49 *);
	static Matrix4x4_INTERNAL_CALL_TRS_m6_318_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_TRS_m6_318_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___pos, ___q, ___s);
}
// System.String UnityEngine.Matrix4x4::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2700;
extern "C" String_t* Matrix4x4_ToString_m6_319 (Matrix4x4_t6_53 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		_stringLiteral2700 = il2cpp_codegen_string_literal_from_index(2700);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, ((int32_t)16)));
		float L_1 = (__this->___m00_0);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		float L_5 = (__this->___m01_4);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_4;
		float L_9 = (__this->___m02_8);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_157* L_12 = L_8;
		float L_13 = (__this->___m03_12);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t1_157* L_16 = L_12;
		float L_17 = (__this->___m10_1);
		float L_18 = L_17;
		Object_t * L_19 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 4, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t1_157* L_20 = L_16;
		float L_21 = (__this->___m11_5);
		float L_22 = L_21;
		Object_t * L_23 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5, sizeof(Object_t *))) = (Object_t *)L_23;
		ObjectU5BU5D_t1_157* L_24 = L_20;
		float L_25 = (__this->___m12_9);
		float L_26 = L_25;
		Object_t * L_27 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 6);
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 6, sizeof(Object_t *))) = (Object_t *)L_27;
		ObjectU5BU5D_t1_157* L_28 = L_24;
		float L_29 = (__this->___m13_13);
		float L_30 = L_29;
		Object_t * L_31 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 7);
		ArrayElementTypeCheck (L_28, L_31);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, 7, sizeof(Object_t *))) = (Object_t *)L_31;
		ObjectU5BU5D_t1_157* L_32 = L_28;
		float L_33 = (__this->___m20_2);
		float L_34 = L_33;
		Object_t * L_35 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		ArrayElementTypeCheck (L_32, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_32, 8, sizeof(Object_t *))) = (Object_t *)L_35;
		ObjectU5BU5D_t1_157* L_36 = L_32;
		float L_37 = (__this->___m21_6);
		float L_38 = L_37;
		Object_t * L_39 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)9));
		ArrayElementTypeCheck (L_36, L_39);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_39;
		ObjectU5BU5D_t1_157* L_40 = L_36;
		float L_41 = (__this->___m22_10);
		float L_42 = L_41;
		Object_t * L_43 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)10));
		ArrayElementTypeCheck (L_40, L_43);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_40, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_43;
		ObjectU5BU5D_t1_157* L_44 = L_40;
		float L_45 = (__this->___m23_14);
		float L_46 = L_45;
		Object_t * L_47 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)11));
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, ((int32_t)11), sizeof(Object_t *))) = (Object_t *)L_47;
		ObjectU5BU5D_t1_157* L_48 = L_44;
		float L_49 = (__this->___m30_3);
		float L_50 = L_49;
		Object_t * L_51 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)12));
		ArrayElementTypeCheck (L_48, L_51);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, ((int32_t)12), sizeof(Object_t *))) = (Object_t *)L_51;
		ObjectU5BU5D_t1_157* L_52 = L_48;
		float L_53 = (__this->___m31_7);
		float L_54 = L_53;
		Object_t * L_55 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)13));
		ArrayElementTypeCheck (L_52, L_55);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, ((int32_t)13), sizeof(Object_t *))) = (Object_t *)L_55;
		ObjectU5BU5D_t1_157* L_56 = L_52;
		float L_57 = (__this->___m32_11);
		float L_58 = L_57;
		Object_t * L_59 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)14));
		ArrayElementTypeCheck (L_56, L_59);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, ((int32_t)14), sizeof(Object_t *))) = (Object_t *)L_59;
		ObjectU5BU5D_t1_157* L_60 = L_56;
		float L_61 = (__this->___m33_15);
		float L_62 = L_61;
		Object_t * L_63 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)15));
		ArrayElementTypeCheck (L_60, L_63);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_60, ((int32_t)15), sizeof(Object_t *))) = (Object_t *)L_63;
		String_t* L_64 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2700, L_60, /*hidden argument*/NULL);
		return L_64;
	}
}
// System.String UnityEngine.Matrix4x4::ToString(System.String)
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2701;
extern "C" String_t* Matrix4x4_ToString_m6_320 (Matrix4x4_t6_53 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral2701 = il2cpp_codegen_string_literal_from_index(2701);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, ((int32_t)16)));
		float* L_1 = &(__this->___m00_0);
		String_t* L_2 = ___format;
		String_t* L_3 = Single_ToString_m1_483(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		float* L_5 = &(__this->___m01_4);
		String_t* L_6 = ___format;
		String_t* L_7 = Single_ToString_m1_483(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_4;
		float* L_9 = &(__this->___m02_8);
		String_t* L_10 = ___format;
		String_t* L_11 = Single_ToString_m1_483(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_157* L_12 = L_8;
		float* L_13 = &(__this->___m03_12);
		String_t* L_14 = ___format;
		String_t* L_15 = Single_ToString_m1_483(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t1_157* L_16 = L_12;
		float* L_17 = &(__this->___m10_1);
		String_t* L_18 = ___format;
		String_t* L_19 = Single_ToString_m1_483(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 4, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t1_157* L_20 = L_16;
		float* L_21 = &(__this->___m11_5);
		String_t* L_22 = ___format;
		String_t* L_23 = Single_ToString_m1_483(L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5, sizeof(Object_t *))) = (Object_t *)L_23;
		ObjectU5BU5D_t1_157* L_24 = L_20;
		float* L_25 = &(__this->___m12_9);
		String_t* L_26 = ___format;
		String_t* L_27 = Single_ToString_m1_483(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 6);
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 6, sizeof(Object_t *))) = (Object_t *)L_27;
		ObjectU5BU5D_t1_157* L_28 = L_24;
		float* L_29 = &(__this->___m13_13);
		String_t* L_30 = ___format;
		String_t* L_31 = Single_ToString_m1_483(L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 7);
		ArrayElementTypeCheck (L_28, L_31);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, 7, sizeof(Object_t *))) = (Object_t *)L_31;
		ObjectU5BU5D_t1_157* L_32 = L_28;
		float* L_33 = &(__this->___m20_2);
		String_t* L_34 = ___format;
		String_t* L_35 = Single_ToString_m1_483(L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		ArrayElementTypeCheck (L_32, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_32, 8, sizeof(Object_t *))) = (Object_t *)L_35;
		ObjectU5BU5D_t1_157* L_36 = L_32;
		float* L_37 = &(__this->___m21_6);
		String_t* L_38 = ___format;
		String_t* L_39 = Single_ToString_m1_483(L_37, L_38, /*hidden argument*/NULL);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)9));
		ArrayElementTypeCheck (L_36, L_39);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_39;
		ObjectU5BU5D_t1_157* L_40 = L_36;
		float* L_41 = &(__this->___m22_10);
		String_t* L_42 = ___format;
		String_t* L_43 = Single_ToString_m1_483(L_41, L_42, /*hidden argument*/NULL);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)10));
		ArrayElementTypeCheck (L_40, L_43);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_40, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_43;
		ObjectU5BU5D_t1_157* L_44 = L_40;
		float* L_45 = &(__this->___m23_14);
		String_t* L_46 = ___format;
		String_t* L_47 = Single_ToString_m1_483(L_45, L_46, /*hidden argument*/NULL);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)11));
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, ((int32_t)11), sizeof(Object_t *))) = (Object_t *)L_47;
		ObjectU5BU5D_t1_157* L_48 = L_44;
		float* L_49 = &(__this->___m30_3);
		String_t* L_50 = ___format;
		String_t* L_51 = Single_ToString_m1_483(L_49, L_50, /*hidden argument*/NULL);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)12));
		ArrayElementTypeCheck (L_48, L_51);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, ((int32_t)12), sizeof(Object_t *))) = (Object_t *)L_51;
		ObjectU5BU5D_t1_157* L_52 = L_48;
		float* L_53 = &(__this->___m31_7);
		String_t* L_54 = ___format;
		String_t* L_55 = Single_ToString_m1_483(L_53, L_54, /*hidden argument*/NULL);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)13));
		ArrayElementTypeCheck (L_52, L_55);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, ((int32_t)13), sizeof(Object_t *))) = (Object_t *)L_55;
		ObjectU5BU5D_t1_157* L_56 = L_52;
		float* L_57 = &(__this->___m32_11);
		String_t* L_58 = ___format;
		String_t* L_59 = Single_ToString_m1_483(L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)14));
		ArrayElementTypeCheck (L_56, L_59);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, ((int32_t)14), sizeof(Object_t *))) = (Object_t *)L_59;
		ObjectU5BU5D_t1_157* L_60 = L_56;
		float* L_61 = &(__this->___m33_15);
		String_t* L_62 = ___format;
		String_t* L_63 = Single_ToString_m1_483(L_61, L_62, /*hidden argument*/NULL);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)15));
		ArrayElementTypeCheck (L_60, L_63);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_60, ((int32_t)15), sizeof(Object_t *))) = (Object_t *)L_63;
		String_t* L_64 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2701, L_60, /*hidden argument*/NULL);
		return L_64;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" Matrix4x4_t6_53  Matrix4x4_Ortho_m6_321 (Object_t * __this /* static, unused */, float ___left, float ___right, float ___bottom, float ___top, float ___zNear, float ___zFar, const MethodInfo* method)
{
	typedef Matrix4x4_t6_53  (*Matrix4x4_Ortho_m6_321_ftn) (float, float, float, float, float, float);
	static Matrix4x4_Ortho_m6_321_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_Ortho_m6_321_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)");
	return _il2cpp_icall_func(___left, ___right, ___bottom, ___top, ___zNear, ___zFar);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Perspective(System.Single,System.Single,System.Single,System.Single)
extern "C" Matrix4x4_t6_53  Matrix4x4_Perspective_m6_322 (Object_t * __this /* static, unused */, float ___fov, float ___aspect, float ___zNear, float ___zFar, const MethodInfo* method)
{
	typedef Matrix4x4_t6_53  (*Matrix4x4_Perspective_m6_322_ftn) (float, float, float, float);
	static Matrix4x4_Perspective_m6_322_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_Perspective_m6_322_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::Perspective(System.Single,System.Single,System.Single,System.Single)");
	return _il2cpp_icall_func(___fov, ___aspect, ___zNear, ___zFar);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern TypeInfo* Matrix4x4_t6_53_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t6_53  Matrix4x4_op_Multiply_m6_323 (Object_t * __this /* static, unused */, Matrix4x4_t6_53  ___lhs, Matrix4x4_t6_53  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t6_53_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(945);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t6_53  V_0 = {0};
	{
		Initobj (Matrix4x4_t6_53_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___lhs)->___m00_0);
		float L_1 = ((&___rhs)->___m00_0);
		float L_2 = ((&___lhs)->___m01_4);
		float L_3 = ((&___rhs)->___m10_1);
		float L_4 = ((&___lhs)->___m02_8);
		float L_5 = ((&___rhs)->___m20_2);
		float L_6 = ((&___lhs)->___m03_12);
		float L_7 = ((&___rhs)->___m30_3);
		(&V_0)->___m00_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		float L_8 = ((&___lhs)->___m00_0);
		float L_9 = ((&___rhs)->___m01_4);
		float L_10 = ((&___lhs)->___m01_4);
		float L_11 = ((&___rhs)->___m11_5);
		float L_12 = ((&___lhs)->___m02_8);
		float L_13 = ((&___rhs)->___m21_6);
		float L_14 = ((&___lhs)->___m03_12);
		float L_15 = ((&___rhs)->___m31_7);
		(&V_0)->___m01_4 = ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15))));
		float L_16 = ((&___lhs)->___m00_0);
		float L_17 = ((&___rhs)->___m02_8);
		float L_18 = ((&___lhs)->___m01_4);
		float L_19 = ((&___rhs)->___m12_9);
		float L_20 = ((&___lhs)->___m02_8);
		float L_21 = ((&___rhs)->___m22_10);
		float L_22 = ((&___lhs)->___m03_12);
		float L_23 = ((&___rhs)->___m32_11);
		(&V_0)->___m02_8 = ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23))));
		float L_24 = ((&___lhs)->___m00_0);
		float L_25 = ((&___rhs)->___m03_12);
		float L_26 = ((&___lhs)->___m01_4);
		float L_27 = ((&___rhs)->___m13_13);
		float L_28 = ((&___lhs)->___m02_8);
		float L_29 = ((&___rhs)->___m23_14);
		float L_30 = ((&___lhs)->___m03_12);
		float L_31 = ((&___rhs)->___m33_15);
		(&V_0)->___m03_12 = ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31))));
		float L_32 = ((&___lhs)->___m10_1);
		float L_33 = ((&___rhs)->___m00_0);
		float L_34 = ((&___lhs)->___m11_5);
		float L_35 = ((&___rhs)->___m10_1);
		float L_36 = ((&___lhs)->___m12_9);
		float L_37 = ((&___rhs)->___m20_2);
		float L_38 = ((&___lhs)->___m13_13);
		float L_39 = ((&___rhs)->___m30_3);
		(&V_0)->___m10_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_32*(float)L_33))+(float)((float)((float)L_34*(float)L_35))))+(float)((float)((float)L_36*(float)L_37))))+(float)((float)((float)L_38*(float)L_39))));
		float L_40 = ((&___lhs)->___m10_1);
		float L_41 = ((&___rhs)->___m01_4);
		float L_42 = ((&___lhs)->___m11_5);
		float L_43 = ((&___rhs)->___m11_5);
		float L_44 = ((&___lhs)->___m12_9);
		float L_45 = ((&___rhs)->___m21_6);
		float L_46 = ((&___lhs)->___m13_13);
		float L_47 = ((&___rhs)->___m31_7);
		(&V_0)->___m11_5 = ((float)((float)((float)((float)((float)((float)((float)((float)L_40*(float)L_41))+(float)((float)((float)L_42*(float)L_43))))+(float)((float)((float)L_44*(float)L_45))))+(float)((float)((float)L_46*(float)L_47))));
		float L_48 = ((&___lhs)->___m10_1);
		float L_49 = ((&___rhs)->___m02_8);
		float L_50 = ((&___lhs)->___m11_5);
		float L_51 = ((&___rhs)->___m12_9);
		float L_52 = ((&___lhs)->___m12_9);
		float L_53 = ((&___rhs)->___m22_10);
		float L_54 = ((&___lhs)->___m13_13);
		float L_55 = ((&___rhs)->___m32_11);
		(&V_0)->___m12_9 = ((float)((float)((float)((float)((float)((float)((float)((float)L_48*(float)L_49))+(float)((float)((float)L_50*(float)L_51))))+(float)((float)((float)L_52*(float)L_53))))+(float)((float)((float)L_54*(float)L_55))));
		float L_56 = ((&___lhs)->___m10_1);
		float L_57 = ((&___rhs)->___m03_12);
		float L_58 = ((&___lhs)->___m11_5);
		float L_59 = ((&___rhs)->___m13_13);
		float L_60 = ((&___lhs)->___m12_9);
		float L_61 = ((&___rhs)->___m23_14);
		float L_62 = ((&___lhs)->___m13_13);
		float L_63 = ((&___rhs)->___m33_15);
		(&V_0)->___m13_13 = ((float)((float)((float)((float)((float)((float)((float)((float)L_56*(float)L_57))+(float)((float)((float)L_58*(float)L_59))))+(float)((float)((float)L_60*(float)L_61))))+(float)((float)((float)L_62*(float)L_63))));
		float L_64 = ((&___lhs)->___m20_2);
		float L_65 = ((&___rhs)->___m00_0);
		float L_66 = ((&___lhs)->___m21_6);
		float L_67 = ((&___rhs)->___m10_1);
		float L_68 = ((&___lhs)->___m22_10);
		float L_69 = ((&___rhs)->___m20_2);
		float L_70 = ((&___lhs)->___m23_14);
		float L_71 = ((&___rhs)->___m30_3);
		(&V_0)->___m20_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_64*(float)L_65))+(float)((float)((float)L_66*(float)L_67))))+(float)((float)((float)L_68*(float)L_69))))+(float)((float)((float)L_70*(float)L_71))));
		float L_72 = ((&___lhs)->___m20_2);
		float L_73 = ((&___rhs)->___m01_4);
		float L_74 = ((&___lhs)->___m21_6);
		float L_75 = ((&___rhs)->___m11_5);
		float L_76 = ((&___lhs)->___m22_10);
		float L_77 = ((&___rhs)->___m21_6);
		float L_78 = ((&___lhs)->___m23_14);
		float L_79 = ((&___rhs)->___m31_7);
		(&V_0)->___m21_6 = ((float)((float)((float)((float)((float)((float)((float)((float)L_72*(float)L_73))+(float)((float)((float)L_74*(float)L_75))))+(float)((float)((float)L_76*(float)L_77))))+(float)((float)((float)L_78*(float)L_79))));
		float L_80 = ((&___lhs)->___m20_2);
		float L_81 = ((&___rhs)->___m02_8);
		float L_82 = ((&___lhs)->___m21_6);
		float L_83 = ((&___rhs)->___m12_9);
		float L_84 = ((&___lhs)->___m22_10);
		float L_85 = ((&___rhs)->___m22_10);
		float L_86 = ((&___lhs)->___m23_14);
		float L_87 = ((&___rhs)->___m32_11);
		(&V_0)->___m22_10 = ((float)((float)((float)((float)((float)((float)((float)((float)L_80*(float)L_81))+(float)((float)((float)L_82*(float)L_83))))+(float)((float)((float)L_84*(float)L_85))))+(float)((float)((float)L_86*(float)L_87))));
		float L_88 = ((&___lhs)->___m20_2);
		float L_89 = ((&___rhs)->___m03_12);
		float L_90 = ((&___lhs)->___m21_6);
		float L_91 = ((&___rhs)->___m13_13);
		float L_92 = ((&___lhs)->___m22_10);
		float L_93 = ((&___rhs)->___m23_14);
		float L_94 = ((&___lhs)->___m23_14);
		float L_95 = ((&___rhs)->___m33_15);
		(&V_0)->___m23_14 = ((float)((float)((float)((float)((float)((float)((float)((float)L_88*(float)L_89))+(float)((float)((float)L_90*(float)L_91))))+(float)((float)((float)L_92*(float)L_93))))+(float)((float)((float)L_94*(float)L_95))));
		float L_96 = ((&___lhs)->___m30_3);
		float L_97 = ((&___rhs)->___m00_0);
		float L_98 = ((&___lhs)->___m31_7);
		float L_99 = ((&___rhs)->___m10_1);
		float L_100 = ((&___lhs)->___m32_11);
		float L_101 = ((&___rhs)->___m20_2);
		float L_102 = ((&___lhs)->___m33_15);
		float L_103 = ((&___rhs)->___m30_3);
		(&V_0)->___m30_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_96*(float)L_97))+(float)((float)((float)L_98*(float)L_99))))+(float)((float)((float)L_100*(float)L_101))))+(float)((float)((float)L_102*(float)L_103))));
		float L_104 = ((&___lhs)->___m30_3);
		float L_105 = ((&___rhs)->___m01_4);
		float L_106 = ((&___lhs)->___m31_7);
		float L_107 = ((&___rhs)->___m11_5);
		float L_108 = ((&___lhs)->___m32_11);
		float L_109 = ((&___rhs)->___m21_6);
		float L_110 = ((&___lhs)->___m33_15);
		float L_111 = ((&___rhs)->___m31_7);
		(&V_0)->___m31_7 = ((float)((float)((float)((float)((float)((float)((float)((float)L_104*(float)L_105))+(float)((float)((float)L_106*(float)L_107))))+(float)((float)((float)L_108*(float)L_109))))+(float)((float)((float)L_110*(float)L_111))));
		float L_112 = ((&___lhs)->___m30_3);
		float L_113 = ((&___rhs)->___m02_8);
		float L_114 = ((&___lhs)->___m31_7);
		float L_115 = ((&___rhs)->___m12_9);
		float L_116 = ((&___lhs)->___m32_11);
		float L_117 = ((&___rhs)->___m22_10);
		float L_118 = ((&___lhs)->___m33_15);
		float L_119 = ((&___rhs)->___m32_11);
		(&V_0)->___m32_11 = ((float)((float)((float)((float)((float)((float)((float)((float)L_112*(float)L_113))+(float)((float)((float)L_114*(float)L_115))))+(float)((float)((float)L_116*(float)L_117))))+(float)((float)((float)L_118*(float)L_119))));
		float L_120 = ((&___lhs)->___m30_3);
		float L_121 = ((&___rhs)->___m03_12);
		float L_122 = ((&___lhs)->___m31_7);
		float L_123 = ((&___rhs)->___m13_13);
		float L_124 = ((&___lhs)->___m32_11);
		float L_125 = ((&___rhs)->___m23_14);
		float L_126 = ((&___lhs)->___m33_15);
		float L_127 = ((&___rhs)->___m33_15);
		(&V_0)->___m33_15 = ((float)((float)((float)((float)((float)((float)((float)((float)L_120*(float)L_121))+(float)((float)((float)L_122*(float)L_123))))+(float)((float)((float)L_124*(float)L_125))))+(float)((float)((float)L_126*(float)L_127))));
		Matrix4x4_t6_53  L_128 = V_0;
		return L_128;
	}
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C" Vector4_t6_55  Matrix4x4_op_Multiply_m6_324 (Object_t * __this /* static, unused */, Matrix4x4_t6_53  ___lhs, Vector4_t6_55  ___v, const MethodInfo* method)
{
	Vector4_t6_55  V_0 = {0};
	{
		float L_0 = ((&___lhs)->___m00_0);
		float L_1 = ((&___v)->___x_0);
		float L_2 = ((&___lhs)->___m01_4);
		float L_3 = ((&___v)->___y_1);
		float L_4 = ((&___lhs)->___m02_8);
		float L_5 = ((&___v)->___z_2);
		float L_6 = ((&___lhs)->___m03_12);
		float L_7 = ((&___v)->___w_3);
		(&V_0)->___x_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		float L_8 = ((&___lhs)->___m10_1);
		float L_9 = ((&___v)->___x_0);
		float L_10 = ((&___lhs)->___m11_5);
		float L_11 = ((&___v)->___y_1);
		float L_12 = ((&___lhs)->___m12_9);
		float L_13 = ((&___v)->___z_2);
		float L_14 = ((&___lhs)->___m13_13);
		float L_15 = ((&___v)->___w_3);
		(&V_0)->___y_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15))));
		float L_16 = ((&___lhs)->___m20_2);
		float L_17 = ((&___v)->___x_0);
		float L_18 = ((&___lhs)->___m21_6);
		float L_19 = ((&___v)->___y_1);
		float L_20 = ((&___lhs)->___m22_10);
		float L_21 = ((&___v)->___z_2);
		float L_22 = ((&___lhs)->___m23_14);
		float L_23 = ((&___v)->___w_3);
		(&V_0)->___z_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23))));
		float L_24 = ((&___lhs)->___m30_3);
		float L_25 = ((&___v)->___x_0);
		float L_26 = ((&___lhs)->___m31_7);
		float L_27 = ((&___v)->___y_1);
		float L_28 = ((&___lhs)->___m32_11);
		float L_29 = ((&___v)->___z_2);
		float L_30 = ((&___lhs)->___m33_15);
		float L_31 = ((&___v)->___w_3);
		(&V_0)->___w_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31))));
		Vector4_t6_55  L_32 = V_0;
		return L_32;
	}
}
// System.Boolean UnityEngine.Matrix4x4::op_Equality(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C" bool Matrix4x4_op_Equality_m6_325 (Object_t * __this /* static, unused */, Matrix4x4_t6_53  ___lhs, Matrix4x4_t6_53  ___rhs, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		Vector4_t6_55  L_0 = Matrix4x4_GetColumn_m6_306((&___lhs), 0, /*hidden argument*/NULL);
		Vector4_t6_55  L_1 = Matrix4x4_GetColumn_m6_306((&___rhs), 0, /*hidden argument*/NULL);
		bool L_2 = Vector4_op_Equality_m6_370(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0065;
		}
	}
	{
		Vector4_t6_55  L_3 = Matrix4x4_GetColumn_m6_306((&___lhs), 1, /*hidden argument*/NULL);
		Vector4_t6_55  L_4 = Matrix4x4_GetColumn_m6_306((&___rhs), 1, /*hidden argument*/NULL);
		bool L_5 = Vector4_op_Equality_m6_370(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0065;
		}
	}
	{
		Vector4_t6_55  L_6 = Matrix4x4_GetColumn_m6_306((&___lhs), 2, /*hidden argument*/NULL);
		Vector4_t6_55  L_7 = Matrix4x4_GetColumn_m6_306((&___rhs), 2, /*hidden argument*/NULL);
		bool L_8 = Vector4_op_Equality_m6_370(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0065;
		}
	}
	{
		Vector4_t6_55  L_9 = Matrix4x4_GetColumn_m6_306((&___lhs), 3, /*hidden argument*/NULL);
		Vector4_t6_55  L_10 = Matrix4x4_GetColumn_m6_306((&___rhs), 3, /*hidden argument*/NULL);
		bool L_11 = Vector4_op_Equality_m6_370(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0066;
	}

IL_0065:
	{
		G_B5_0 = 0;
	}

IL_0066:
	{
		return G_B5_0;
	}
}
// System.Boolean UnityEngine.Matrix4x4::op_Inequality(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C" bool Matrix4x4_op_Inequality_m6_326 (Object_t * __this /* static, unused */, Matrix4x4_t6_53  ___lhs, Matrix4x4_t6_53  ___rhs, const MethodInfo* method)
{
	{
		Matrix4x4_t6_53  L_0 = ___lhs;
		Matrix4x4_t6_53  L_1 = ___rhs;
		bool L_2 = Matrix4x4_op_Equality_m6_325(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Bounds__ctor_m6_327 (Bounds_t6_54 * __this, Vector3_t6_49  ___center, Vector3_t6_49  ___size, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___center;
		__this->___m_Center_0 = L_0;
		Vector3_t6_49  L_1 = ___size;
		Vector3_t6_49  L_2 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		__this->___m_Extents_1 = L_2;
		return;
	}
}
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C" int32_t Bounds_GetHashCode_m6_328 (Bounds_t6_54 * __this, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	Vector3_t6_49  V_1 = {0};
	{
		Vector3_t6_49  L_0 = Bounds_get_center_m6_330(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector3_GetHashCode_m6_198((&V_0), /*hidden argument*/NULL);
		Vector3_t6_49  L_2 = Bounds_get_extents_m6_334(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector3_GetHashCode_m6_198((&V_1), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern TypeInfo* Bounds_t6_54_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern "C" bool Bounds_Equals_m6_329 (Bounds_t6_54 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Bounds_t6_54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(946);
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t6_54  V_0 = {0};
	Vector3_t6_49  V_1 = {0};
	Vector3_t6_49  V_2 = {0};
	int32_t G_B5_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Bounds_t6_54_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Bounds_t6_54 *)((Bounds_t6_54 *)UnBox (L_1, Bounds_t6_54_il2cpp_TypeInfo_var))));
		Vector3_t6_49  L_2 = Bounds_get_center_m6_330(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_t6_49  L_3 = Bounds_get_center_m6_330((&V_0), /*hidden argument*/NULL);
		Vector3_t6_49  L_4 = L_3;
		Object_t * L_5 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector3_Equals_m6_199((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		Vector3_t6_49  L_7 = Bounds_get_extents_m6_334(__this, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector3_t6_49  L_8 = Bounds_get_extents_m6_334((&V_0), /*hidden argument*/NULL);
		Vector3_t6_49  L_9 = L_8;
		Object_t * L_10 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector3_Equals_m6_199((&V_2), L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0050;
	}

IL_004f:
	{
		G_B5_0 = 0;
	}

IL_0050:
	{
		return G_B5_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C" Vector3_t6_49  Bounds_get_center_m6_330 (Bounds_t6_54 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = (__this->___m_Center_0);
		return L_0;
	}
}
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C" void Bounds_set_center_m6_331 (Bounds_t6_54 * __this, Vector3_t6_49  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___value;
		__this->___m_Center_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C" Vector3_t6_49  Bounds_get_size_m6_332 (Bounds_t6_54 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = (__this->___m_Extents_1);
		Vector3_t6_49  L_1 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_0, (2.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C" void Bounds_set_size_m6_333 (Bounds_t6_54 * __this, Vector3_t6_49  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___value;
		Vector3_t6_49  L_1 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_0, (0.5f), /*hidden argument*/NULL);
		__this->___m_Extents_1 = L_1;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C" Vector3_t6_49  Bounds_get_extents_m6_334 (Bounds_t6_54 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = (__this->___m_Extents_1);
		return L_0;
	}
}
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C" void Bounds_set_extents_m6_335 (Bounds_t6_54 * __this, Vector3_t6_49  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___value;
		__this->___m_Extents_1 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C" Vector3_t6_49  Bounds_get_min_m6_336 (Bounds_t6_54 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Bounds_get_center_m6_330(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = Bounds_get_extents_m6_334(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_2 = Vector3_op_Subtraction_m6_222(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Bounds::set_min(UnityEngine.Vector3)
extern "C" void Bounds_set_min_m6_337 (Bounds_t6_54 * __this, Vector3_t6_49  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___value;
		Vector3_t6_49  L_1 = Bounds_get_max_m6_338(__this, /*hidden argument*/NULL);
		Bounds_SetMinMax_m6_340(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C" Vector3_t6_49  Bounds_get_max_m6_338 (Bounds_t6_54 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Bounds_get_center_m6_330(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = Bounds_get_extents_m6_334(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_2 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Bounds::set_max(UnityEngine.Vector3)
extern "C" void Bounds_set_max_m6_339 (Bounds_t6_54 * __this, Vector3_t6_49  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Bounds_get_min_m6_336(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = ___value;
		Bounds_SetMinMax_m6_340(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Bounds_SetMinMax_m6_340 (Bounds_t6_54 * __this, Vector3_t6_49  ___min, Vector3_t6_49  ___max, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___max;
		Vector3_t6_49  L_1 = ___min;
		Vector3_t6_49  L_2 = Vector3_op_Subtraction_m6_222(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t6_49  L_3 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		Bounds_set_extents_m6_335(__this, L_3, /*hidden argument*/NULL);
		Vector3_t6_49  L_4 = ___min;
		Vector3_t6_49  L_5 = Bounds_get_extents_m6_334(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_6 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Bounds_set_center_m6_331(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C" void Bounds_Encapsulate_m6_341 (Bounds_t6_54 * __this, Vector3_t6_49  ___point, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Bounds_get_min_m6_336(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = ___point;
		Vector3_t6_49  L_2 = Vector3_Min_m6_211(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t6_49  L_3 = Bounds_get_max_m6_338(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_4 = ___point;
		Vector3_t6_49  L_5 = Vector3_Max_m6_212(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_SetMinMax_m6_340(__this, L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern "C" void Bounds_Encapsulate_m6_342 (Bounds_t6_54 * __this, Bounds_t6_54  ___bounds, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Bounds_get_center_m6_330((&___bounds), /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = Bounds_get_extents_m6_334((&___bounds), /*hidden argument*/NULL);
		Vector3_t6_49  L_2 = Vector3_op_Subtraction_m6_222(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Bounds_Encapsulate_m6_341(__this, L_2, /*hidden argument*/NULL);
		Vector3_t6_49  L_3 = Bounds_get_center_m6_330((&___bounds), /*hidden argument*/NULL);
		Vector3_t6_49  L_4 = Bounds_get_extents_m6_334((&___bounds), /*hidden argument*/NULL);
		Vector3_t6_49  L_5 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_Encapsulate_m6_341(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Expand(System.Single)
extern "C" void Bounds_Expand_m6_343 (Bounds_t6_54 * __this, float ___amount, const MethodInfo* method)
{
	{
		float L_0 = ___amount;
		___amount = ((float)((float)L_0*(float)(0.5f)));
		Vector3_t6_49  L_1 = Bounds_get_extents_m6_334(__this, /*hidden argument*/NULL);
		float L_2 = ___amount;
		float L_3 = ___amount;
		float L_4 = ___amount;
		Vector3_t6_49  L_5 = {0};
		Vector3__ctor_m6_191(&L_5, L_2, L_3, L_4, /*hidden argument*/NULL);
		Vector3_t6_49  L_6 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_1, L_5, /*hidden argument*/NULL);
		Bounds_set_extents_m6_335(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Expand(UnityEngine.Vector3)
extern "C" void Bounds_Expand_m6_344 (Bounds_t6_54 * __this, Vector3_t6_49  ___amount, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Bounds_get_extents_m6_334(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = ___amount;
		Vector3_t6_49  L_2 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		Vector3_t6_49  L_3 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		Bounds_set_extents_m6_335(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Bounds::Intersects(UnityEngine.Bounds)
extern "C" bool Bounds_Intersects_m6_345 (Bounds_t6_54 * __this, Bounds_t6_54  ___bounds, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	Vector3_t6_49  V_1 = {0};
	Vector3_t6_49  V_2 = {0};
	Vector3_t6_49  V_3 = {0};
	Vector3_t6_49  V_4 = {0};
	Vector3_t6_49  V_5 = {0};
	Vector3_t6_49  V_6 = {0};
	Vector3_t6_49  V_7 = {0};
	Vector3_t6_49  V_8 = {0};
	Vector3_t6_49  V_9 = {0};
	Vector3_t6_49  V_10 = {0};
	Vector3_t6_49  V_11 = {0};
	int32_t G_B7_0 = 0;
	{
		Vector3_t6_49  L_0 = Bounds_get_min_m6_336(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ((&V_0)->___x_1);
		Vector3_t6_49  L_2 = Bounds_get_max_m6_338((&___bounds), /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = ((&V_1)->___x_1);
		if ((!(((float)L_1) <= ((float)L_3))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6_49  L_4 = Bounds_get_max_m6_338(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = ((&V_2)->___x_1);
		Vector3_t6_49  L_6 = Bounds_get_min_m6_336((&___bounds), /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = ((&V_3)->___x_1);
		if ((!(((float)L_5) >= ((float)L_7))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6_49  L_8 = Bounds_get_min_m6_336(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = ((&V_4)->___y_2);
		Vector3_t6_49  L_10 = Bounds_get_max_m6_338((&___bounds), /*hidden argument*/NULL);
		V_5 = L_10;
		float L_11 = ((&V_5)->___y_2);
		if ((!(((float)L_9) <= ((float)L_11))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6_49  L_12 = Bounds_get_max_m6_338(__this, /*hidden argument*/NULL);
		V_6 = L_12;
		float L_13 = ((&V_6)->___y_2);
		Vector3_t6_49  L_14 = Bounds_get_min_m6_336((&___bounds), /*hidden argument*/NULL);
		V_7 = L_14;
		float L_15 = ((&V_7)->___y_2);
		if ((!(((float)L_13) >= ((float)L_15))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6_49  L_16 = Bounds_get_min_m6_336(__this, /*hidden argument*/NULL);
		V_8 = L_16;
		float L_17 = ((&V_8)->___z_3);
		Vector3_t6_49  L_18 = Bounds_get_max_m6_338((&___bounds), /*hidden argument*/NULL);
		V_9 = L_18;
		float L_19 = ((&V_9)->___z_3);
		if ((!(((float)L_17) <= ((float)L_19))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6_49  L_20 = Bounds_get_max_m6_338(__this, /*hidden argument*/NULL);
		V_10 = L_20;
		float L_21 = ((&V_10)->___z_3);
		Vector3_t6_49  L_22 = Bounds_get_min_m6_336((&___bounds), /*hidden argument*/NULL);
		V_11 = L_22;
		float L_23 = ((&V_11)->___z_3);
		G_B7_0 = ((((int32_t)((!(((float)L_21) >= ((float)L_23)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00d7;
	}

IL_00d6:
	{
		G_B7_0 = 0;
	}

IL_00d7:
	{
		return G_B7_0;
	}
}
// System.Boolean UnityEngine.Bounds::Internal_Contains(UnityEngine.Bounds,UnityEngine.Vector3)
extern "C" bool Bounds_Internal_Contains_m6_346 (Object_t * __this /* static, unused */, Bounds_t6_54  ___m, Vector3_t6_49  ___point, const MethodInfo* method)
{
	{
		bool L_0 = Bounds_INTERNAL_CALL_Internal_Contains_m6_347(NULL /*static, unused*/, (&___m), (&___point), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" bool Bounds_INTERNAL_CALL_Internal_Contains_m6_347 (Object_t * __this /* static, unused */, Bounds_t6_54 * ___m, Vector3_t6_49 * ___point, const MethodInfo* method)
{
	typedef bool (*Bounds_INTERNAL_CALL_Internal_Contains_m6_347_ftn) (Bounds_t6_54 *, Vector3_t6_49 *);
	static Bounds_INTERNAL_CALL_Internal_Contains_m6_347_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_Contains_m6_347_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___m, ___point);
}
// System.Boolean UnityEngine.Bounds::Contains(UnityEngine.Vector3)
extern "C" bool Bounds_Contains_m6_348 (Bounds_t6_54 * __this, Vector3_t6_49  ___point, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___point;
		bool L_1 = Bounds_Internal_Contains_m6_346(NULL /*static, unused*/, (*(Bounds_t6_54 *)__this), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityEngine.Bounds::Internal_SqrDistance(UnityEngine.Bounds,UnityEngine.Vector3)
extern "C" float Bounds_Internal_SqrDistance_m6_349 (Object_t * __this /* static, unused */, Bounds_t6_54  ___m, Vector3_t6_49  ___point, const MethodInfo* method)
{
	{
		float L_0 = Bounds_INTERNAL_CALL_Internal_SqrDistance_m6_350(NULL /*static, unused*/, (&___m), (&___point), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Bounds::INTERNAL_CALL_Internal_SqrDistance(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" float Bounds_INTERNAL_CALL_Internal_SqrDistance_m6_350 (Object_t * __this /* static, unused */, Bounds_t6_54 * ___m, Vector3_t6_49 * ___point, const MethodInfo* method)
{
	typedef float (*Bounds_INTERNAL_CALL_Internal_SqrDistance_m6_350_ftn) (Bounds_t6_54 *, Vector3_t6_49 *);
	static Bounds_INTERNAL_CALL_Internal_SqrDistance_m6_350_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_SqrDistance_m6_350_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_SqrDistance(UnityEngine.Bounds&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___m, ___point);
}
// System.Single UnityEngine.Bounds::SqrDistance(UnityEngine.Vector3)
extern "C" float Bounds_SqrDistance_m6_351 (Bounds_t6_54 * __this, Vector3_t6_49  ___point, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___point;
		float L_1 = Bounds_Internal_SqrDistance_m6_349(NULL /*static, unused*/, (*(Bounds_t6_54 *)__this), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Bounds::Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)
extern "C" bool Bounds_Internal_IntersectRay_m6_352 (Object_t * __this /* static, unused */, Ray_t6_56 * ___ray, Bounds_t6_54 * ___bounds, float* ___distance, const MethodInfo* method)
{
	{
		Ray_t6_56 * L_0 = ___ray;
		Bounds_t6_54 * L_1 = ___bounds;
		float* L_2 = ___distance;
		bool L_3 = Bounds_INTERNAL_CALL_Internal_IntersectRay_m6_353(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)
extern "C" bool Bounds_INTERNAL_CALL_Internal_IntersectRay_m6_353 (Object_t * __this /* static, unused */, Ray_t6_56 * ___ray, Bounds_t6_54 * ___bounds, float* ___distance, const MethodInfo* method)
{
	typedef bool (*Bounds_INTERNAL_CALL_Internal_IntersectRay_m6_353_ftn) (Ray_t6_56 *, Bounds_t6_54 *, float*);
	static Bounds_INTERNAL_CALL_Internal_IntersectRay_m6_353_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_IntersectRay_m6_353_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)");
	return _il2cpp_icall_func(___ray, ___bounds, ___distance);
}
// System.Boolean UnityEngine.Bounds::IntersectRay(UnityEngine.Ray)
extern "C" bool Bounds_IntersectRay_m6_354 (Bounds_t6_54 * __this, Ray_t6_56  ___ray, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		bool L_0 = Bounds_Internal_IntersectRay_m6_352(NULL /*static, unused*/, (&___ray), __this, (&V_0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Bounds::IntersectRay(UnityEngine.Ray,System.Single&)
extern "C" bool Bounds_IntersectRay_m6_355 (Bounds_t6_54 * __this, Ray_t6_56  ___ray, float* ___distance, const MethodInfo* method)
{
	{
		float* L_0 = ___distance;
		bool L_1 = Bounds_Internal_IntersectRay_m6_352(NULL /*static, unused*/, (&___ray), __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::Internal_GetClosestPoint(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" Vector3_t6_49  Bounds_Internal_GetClosestPoint_m6_356 (Object_t * __this /* static, unused */, Bounds_t6_54 * ___bounds, Vector3_t6_49 * ___point, const MethodInfo* method)
{
	{
		Bounds_t6_54 * L_0 = ___bounds;
		Vector3_t6_49 * L_1 = ___point;
		Vector3_t6_49  L_2 = Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6_357(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::INTERNAL_CALL_Internal_GetClosestPoint(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" Vector3_t6_49  Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6_357 (Object_t * __this /* static, unused */, Bounds_t6_54 * ___bounds, Vector3_t6_49 * ___point, const MethodInfo* method)
{
	typedef Vector3_t6_49  (*Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6_357_ftn) (Bounds_t6_54 *, Vector3_t6_49 *);
	static Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6_357_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6_357_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_GetClosestPoint(UnityEngine.Bounds&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___bounds, ___point);
}
// UnityEngine.Vector3 UnityEngine.Bounds::ClosestPoint(UnityEngine.Vector3)
extern "C" Vector3_t6_49  Bounds_ClosestPoint_m6_358 (Bounds_t6_54 * __this, Vector3_t6_49  ___point, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Bounds_Internal_GetClosestPoint_m6_356(NULL /*static, unused*/, __this, (&___point), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Bounds::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2702;
extern "C" String_t* Bounds_ToString_m6_359 (Bounds_t6_54 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		_stringLiteral2702 = il2cpp_codegen_string_literal_from_index(2702);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 2));
		Vector3_t6_49  L_1 = (__this->___m_Center_0);
		Vector3_t6_49  L_2 = L_1;
		Object_t * L_3 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		Vector3_t6_49  L_5 = (__this->___m_Extents_1);
		Vector3_t6_49  L_6 = L_5;
		Object_t * L_7 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2702, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.String UnityEngine.Bounds::ToString(System.String)
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2702;
extern "C" String_t* Bounds_ToString_m6_360 (Bounds_t6_54 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral2702 = il2cpp_codegen_string_literal_from_index(2702);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 2));
		Vector3_t6_49 * L_1 = &(__this->___m_Center_0);
		String_t* L_2 = ___format;
		String_t* L_3 = Vector3_ToString_m6_203(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		Vector3_t6_49 * L_5 = &(__this->___m_Extents_1);
		String_t* L_6 = ___format;
		String_t* L_7 = Vector3_ToString_m6_203(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2702, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C" bool Bounds_op_Equality_m6_361 (Object_t * __this /* static, unused */, Bounds_t6_54  ___lhs, Bounds_t6_54  ___rhs, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Vector3_t6_49  L_0 = Bounds_get_center_m6_330((&___lhs), /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = Bounds_get_center_m6_330((&___rhs), /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Equality_m6_227(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Vector3_t6_49  L_3 = Bounds_get_extents_m6_334((&___lhs), /*hidden argument*/NULL);
		Vector3_t6_49  L_4 = Bounds_get_extents_m6_334((&___rhs), /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Equality_m6_227(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C" bool Bounds_op_Inequality_m6_362 (Object_t * __this /* static, unused */, Bounds_t6_54  ___lhs, Bounds_t6_54  ___rhs, const MethodInfo* method)
{
	{
		Bounds_t6_54  L_0 = ___lhs;
		Bounds_t6_54  L_1 = ___rhs;
		bool L_2 = Bounds_op_Equality_m6_361(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Vector4__ctor_m6_363 (Vector4_t6_55 * __this, float ___x, float ___y, float ___z, float ___w, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_0 = L_0;
		float L_1 = ___y;
		__this->___y_1 = L_1;
		float L_2 = ___z;
		__this->___z_2 = L_2;
		float L_3 = ___w;
		__this->___w_3 = L_3;
		return;
	}
}
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C" int32_t Vector4_GetHashCode_m6_364 (Vector4_t6_55 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_0);
		int32_t L_1 = Single_GetHashCode_m1_475(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_1);
		int32_t L_3 = Single_GetHashCode_m1_475(L_2, /*hidden argument*/NULL);
		float* L_4 = &(__this->___z_2);
		int32_t L_5 = Single_GetHashCode_m1_475(L_4, /*hidden argument*/NULL);
		float* L_6 = &(__this->___w_3);
		int32_t L_7 = Single_GetHashCode_m1_475(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern TypeInfo* Vector4_t6_55_il2cpp_TypeInfo_var;
extern "C" bool Vector4_Equals_m6_365 (Vector4_t6_55 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector4_t6_55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(934);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t6_55  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Vector4_t6_55_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Vector4_t6_55 *)((Vector4_t6_55 *)UnBox (L_1, Vector4_t6_55_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_0);
		float L_3 = ((&V_0)->___x_0);
		bool L_4 = Single_Equals_m1_474(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = &(__this->___y_1);
		float L_6 = ((&V_0)->___y_1);
		bool L_7 = Single_Equals_m1_474(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = &(__this->___z_2);
		float L_9 = ((&V_0)->___z_2);
		bool L_10 = Single_Equals_m1_474(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = &(__this->___w_3);
		float L_12 = ((&V_0)->___w_3);
		bool L_13 = Single_Equals_m1_474(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// System.String UnityEngine.Vector4::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2697;
extern "C" String_t* Vector4_ToString_m6_366 (Vector4_t6_55 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		_stringLiteral2697 = il2cpp_codegen_string_literal_from_index(2697);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 4));
		float L_1 = (__this->___x_0);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		float L_5 = (__this->___y_1);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_157* L_8 = L_4;
		float L_9 = (__this->___z_2);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_157* L_12 = L_8;
		float L_13 = (__this->___w_3);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2697, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" float Vector4_Dot_m6_367 (Object_t * __this /* static, unused */, Vector4_t6_55  ___a, Vector4_t6_55  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_0);
		float L_1 = ((&___b)->___x_0);
		float L_2 = ((&___a)->___y_1);
		float L_3 = ((&___b)->___y_1);
		float L_4 = ((&___a)->___z_2);
		float L_5 = ((&___b)->___z_2);
		float L_6 = ((&___a)->___w_3);
		float L_7 = ((&___b)->___w_3);
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// System.Single UnityEngine.Vector4::SqrMagnitude(UnityEngine.Vector4)
extern "C" float Vector4_SqrMagnitude_m6_368 (Object_t * __this /* static, unused */, Vector4_t6_55  ___a, const MethodInfo* method)
{
	{
		Vector4_t6_55  L_0 = ___a;
		Vector4_t6_55  L_1 = ___a;
		float L_2 = Vector4_Dot_m6_367(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" Vector4_t6_55  Vector4_op_Subtraction_m6_369 (Object_t * __this /* static, unused */, Vector4_t6_55  ___a, Vector4_t6_55  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_0);
		float L_1 = ((&___b)->___x_0);
		float L_2 = ((&___a)->___y_1);
		float L_3 = ((&___b)->___y_1);
		float L_4 = ((&___a)->___z_2);
		float L_5 = ((&___b)->___z_2);
		float L_6 = ((&___a)->___w_3);
		float L_7 = ((&___b)->___w_3);
		Vector4_t6_55  L_8 = {0};
		Vector4__ctor_m6_363(&L_8, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), ((float)((float)L_6-(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" bool Vector4_op_Equality_m6_370 (Object_t * __this /* static, unused */, Vector4_t6_55  ___lhs, Vector4_t6_55  ___rhs, const MethodInfo* method)
{
	{
		Vector4_t6_55  L_0 = ___lhs;
		Vector4_t6_55  L_1 = ___rhs;
		Vector4_t6_55  L_2 = Vector4_op_Subtraction_m6_369(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector4_SqrMagnitude_m6_368(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Ray__ctor_m6_371 (Ray_t6_56 * __this, Vector3_t6_49  ___origin, Vector3_t6_49  ___direction, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___origin;
		__this->___m_Origin_0 = L_0;
		Vector3_t6_49  L_1 = Vector3_get_normalized_m6_201((&___direction), /*hidden argument*/NULL);
		__this->___m_Direction_1 = L_1;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C" Vector3_t6_49  Ray_get_origin_m6_372 (Ray_t6_56 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = (__this->___m_Origin_0);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C" Vector3_t6_49  Ray_get_direction_m6_373 (Ray_t6_56 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = (__this->___m_Direction_1);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C" Vector3_t6_49  Ray_GetPoint_m6_374 (Ray_t6_56 * __this, float ___distance, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = (__this->___m_Origin_0);
		Vector3_t6_49  L_1 = (__this->___m_Direction_1);
		float L_2 = ___distance;
		Vector3_t6_49  L_3 = Vector3_op_Multiply_m6_224(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t6_49  L_4 = Vector3_op_Addition_m6_221(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.Ray::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t6_49_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2703;
extern "C" String_t* Ray_ToString_m6_375 (Ray_t6_56 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Vector3_t6_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(941);
		_stringLiteral2703 = il2cpp_codegen_string_literal_from_index(2703);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 2));
		Vector3_t6_49  L_1 = (__this->___m_Origin_0);
		Vector3_t6_49  L_2 = L_1;
		Object_t * L_3 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_157* L_4 = L_0;
		Vector3_t6_49  L_5 = (__this->___m_Direction_1);
		Vector3_t6_49  L_6 = L_5;
		Object_t * L_7 = Box(Vector3_t6_49_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m6_437(NULL /*static, unused*/, _stringLiteral2703, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngineInternal.MathfInternal::.cctor()
extern TypeInfo* MathfInternal_t6_57_il2cpp_TypeInfo_var;
extern "C" void MathfInternal__cctor_m6_376 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MathfInternal_t6_57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(947);
		s_Il2CppMethodIntialized = true;
	}
	{
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t6_57_StaticFields*)MathfInternal_t6_57_il2cpp_TypeInfo_var->static_fields)->___FloatMinNormal_0 = (1.17549435E-38f);
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t6_57_StaticFields*)MathfInternal_t6_57_il2cpp_TypeInfo_var->static_fields)->___FloatMinDenormal_1 = (1.401298E-45f);
		((MathfInternal_t6_57_StaticFields*)MathfInternal_t6_57_il2cpp_TypeInfo_var->static_fields)->___IsFlushToZeroEnabled_2 = 1;
		return;
	}
}
// System.Void UnityEngine.Mathf::.cctor()
extern TypeInfo* MathfInternal_t6_57_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" void Mathf__cctor_m6_377 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MathfInternal_t6_57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(947);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	float G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t6_57_il2cpp_TypeInfo_var);
		bool L_0 = ((MathfInternal_t6_57_StaticFields*)MathfInternal_t6_57_il2cpp_TypeInfo_var->static_fields)->___IsFlushToZeroEnabled_2;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t6_57_il2cpp_TypeInfo_var);
		float L_1 = ((MathfInternal_t6_57_StaticFields*)MathfInternal_t6_57_il2cpp_TypeInfo_var->static_fields)->___FloatMinNormal_0;
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t6_57_il2cpp_TypeInfo_var);
		float L_2 = ((MathfInternal_t6_57_StaticFields*)MathfInternal_t6_57_il2cpp_TypeInfo_var->static_fields)->___FloatMinDenormal_1;
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		((Mathf_t6_58_StaticFields*)Mathf_t6_58_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0 = G_B3_0;
		return;
	}
}
// System.Single UnityEngine.Mathf::Acos(System.Single)
extern "C" float Mathf_Acos_m6_378 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = acos((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Sqrt(System.Single)
extern "C" float Mathf_Sqrt_m6_379 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = sqrt((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Abs(System.Single)
extern "C" float Mathf_Abs_m6_380 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		float L_1 = fabsf(L_0);
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C" float Mathf_Min_m6_381 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a;
		float L_1 = ___b;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C" int32_t Mathf_Min_m6_382 (Object_t * __this /* static, unused */, int32_t ___a, int32_t ___b, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a;
		int32_t L_1 = ___b;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C" float Mathf_Max_m6_383 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a;
		float L_1 = ___b;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C" int32_t Mathf_Max_m6_384 (Object_t * __this /* static, unused */, int32_t ___a, int32_t ___b, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a;
		int32_t L_1 = ___b;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Floor(System.Single)
extern "C" float Mathf_Floor_m6_385 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = floor((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Round(System.Single)
extern "C" float Mathf_Round_m6_386 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = bankers_round((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C" int32_t Mathf_FloorToInt_m6_387 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = floor((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C" float Mathf_Sign_m6_388 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___f;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_001a;
	}

IL_0015:
	{
		G_B3_0 = (-1.0f);
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C" float Mathf_Clamp_m6_389 (Object_t * __this /* static, unused */, float ___value, float ___min, float ___max, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		float L_1 = ___min;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000f;
		}
	}
	{
		float L_2 = ___min;
		___value = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		float L_3 = ___value;
		float L_4 = ___max;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0019;
		}
	}
	{
		float L_5 = ___max;
		___value = L_5;
	}

IL_0019:
	{
		float L_6 = ___value;
		return L_6;
	}
}
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C" int32_t Mathf_Clamp_m6_390 (Object_t * __this /* static, unused */, int32_t ___value, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		int32_t L_1 = ___min;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_2 = ___min;
		___value = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		int32_t L_3 = ___value;
		int32_t L_4 = ___max;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_5 = ___max;
		___value = L_5;
	}

IL_0019:
	{
		int32_t L_6 = ___value;
		return L_6;
	}
}
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C" float Mathf_Clamp01_m6_391 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		return (0.0f);
	}

IL_0011:
	{
		float L_1 = ___value;
		if ((!(((float)L_1) > ((float)(1.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		return (1.0f);
	}

IL_0022:
	{
		float L_2 = ___value;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Mathf_Lerp_m6_392 (Object_t * __this /* static, unused */, float ___a, float ___b, float ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___a;
		float L_1 = ___b;
		float L_2 = ___a;
		float L_3 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp01_m6_391(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return ((float)((float)L_0+(float)((float)((float)((float)((float)L_1-(float)L_2))*(float)L_4))));
	}
}
// System.Single UnityEngine.Mathf::MoveTowards(System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Mathf_MoveTowards_m6_393 (Object_t * __this /* static, unused */, float ___current, float ___target, float ___maxDelta, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___target;
		float L_1 = ___current;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___maxDelta;
		if ((!(((float)L_2) <= ((float)L_3))))
		{
			goto IL_0010;
		}
	}
	{
		float L_4 = ___target;
		return L_4;
	}

IL_0010:
	{
		float L_5 = ___current;
		float L_6 = ___target;
		float L_7 = ___current;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Sign_m6_388(NULL /*static, unused*/, ((float)((float)L_6-(float)L_7)), /*hidden argument*/NULL);
		float L_9 = ___maxDelta;
		return ((float)((float)L_5+(float)((float)((float)L_8*(float)L_9))));
	}
}
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" bool Mathf_Approximately_m6_394 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___b;
		float L_1 = ___a;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___a;
		float L_4 = fabsf(L_3);
		float L_5 = ___b;
		float L_6 = fabsf(L_5);
		float L_7 = Mathf_Max_m6_383(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		float L_8 = ((Mathf_t6_58_StaticFields*)Mathf_t6_58_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0;
		float L_9 = Mathf_Max_m6_383(NULL /*static, unused*/, ((float)((float)(1.0E-06f)*(float)L_7)), ((float)((float)L_8*(float)(8.0f))), /*hidden argument*/NULL);
		return ((((float)L_2) < ((float)L_9))? 1 : 0);
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Mathf_SmoothDamp_m6_395 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (std::numeric_limits<float>::infinity());
		float L_1 = ___current;
		float L_2 = ___target;
		float* L_3 = ___currentVelocity;
		float L_4 = ___smoothTime;
		float L_5 = V_1;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_7 = Mathf_SmoothDamp_m6_396(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Mathf_SmoothDamp_m6_396 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, float ___maxSpeed, float ___deltaTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		float L_0 = ___smoothTime;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Max_m6_383(NULL /*static, unused*/, (0.0001f), L_0, /*hidden argument*/NULL);
		___smoothTime = L_1;
		float L_2 = ___smoothTime;
		V_0 = ((float)((float)(2.0f)/(float)L_2));
		float L_3 = V_0;
		float L_4 = ___deltaTime;
		V_1 = ((float)((float)L_3*(float)L_4));
		float L_5 = V_1;
		float L_6 = V_1;
		float L_7 = V_1;
		float L_8 = V_1;
		float L_9 = V_1;
		float L_10 = V_1;
		V_2 = ((float)((float)(1.0f)/(float)((float)((float)((float)((float)((float)((float)(1.0f)+(float)L_5))+(float)((float)((float)((float)((float)(0.48f)*(float)L_6))*(float)L_7))))+(float)((float)((float)((float)((float)((float)((float)(0.235f)*(float)L_8))*(float)L_9))*(float)L_10))))));
		float L_11 = ___current;
		float L_12 = ___target;
		V_3 = ((float)((float)L_11-(float)L_12));
		float L_13 = ___target;
		V_4 = L_13;
		float L_14 = ___maxSpeed;
		float L_15 = ___smoothTime;
		V_5 = ((float)((float)L_14*(float)L_15));
		float L_16 = V_3;
		float L_17 = V_5;
		float L_18 = V_5;
		float L_19 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_16, ((-L_17)), L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20 = ___current;
		float L_21 = V_3;
		___target = ((float)((float)L_20-(float)L_21));
		float* L_22 = ___currentVelocity;
		float L_23 = V_0;
		float L_24 = V_3;
		float L_25 = ___deltaTime;
		V_6 = ((float)((float)((float)((float)(*((float*)L_22))+(float)((float)((float)L_23*(float)L_24))))*(float)L_25));
		float* L_26 = ___currentVelocity;
		float* L_27 = ___currentVelocity;
		float L_28 = V_0;
		float L_29 = V_6;
		float L_30 = V_2;
		*((float*)(L_26)) = (float)((float)((float)((float)((float)(*((float*)L_27))-(float)((float)((float)L_28*(float)L_29))))*(float)L_30));
		float L_31 = ___target;
		float L_32 = V_3;
		float L_33 = V_6;
		float L_34 = V_2;
		V_7 = ((float)((float)L_31+(float)((float)((float)((float)((float)L_32+(float)L_33))*(float)L_34))));
		float L_35 = V_4;
		float L_36 = ___current;
		float L_37 = V_7;
		float L_38 = V_4;
		if ((!(((uint32_t)((((float)((float)((float)L_35-(float)L_36))) > ((float)(0.0f)))? 1 : 0)) == ((uint32_t)((((float)L_37) > ((float)L_38))? 1 : 0)))))
		{
			goto IL_00a0;
		}
	}
	{
		float L_39 = V_4;
		V_7 = L_39;
		float* L_40 = ___currentVelocity;
		float L_41 = V_7;
		float L_42 = V_4;
		float L_43 = ___deltaTime;
		*((float*)(L_40)) = (float)((float)((float)((float)((float)L_41-(float)L_42))/(float)L_43));
	}

IL_00a0:
	{
		float L_44 = V_7;
		return L_44;
	}
}
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Mathf_SmoothDampAngle_m6_397 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, float ___maxSpeed, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m6_654(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___current;
		float L_2 = ___target;
		float* L_3 = ___currentVelocity;
		float L_4 = ___smoothTime;
		float L_5 = ___maxSpeed;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_7 = Mathf_SmoothDampAngle_m6_398(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Mathf_SmoothDampAngle_m6_398 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, float ___maxSpeed, float ___deltaTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___current;
		float L_1 = ___current;
		float L_2 = ___target;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_3 = Mathf_DeltaAngle_m6_400(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		___target = ((float)((float)L_0+(float)L_3));
		float L_4 = ___current;
		float L_5 = ___target;
		float* L_6 = ___currentVelocity;
		float L_7 = ___smoothTime;
		float L_8 = ___maxSpeed;
		float L_9 = ___deltaTime;
		float L_10 = Mathf_SmoothDamp_m6_396(NULL /*static, unused*/, L_4, L_5, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Mathf_Repeat_m6_399 (Object_t * __this /* static, unused */, float ___t, float ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t;
		float L_1 = ___t;
		float L_2 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_3 = floorf(((float)((float)L_1/(float)L_2)));
		float L_4 = ___length;
		return ((float)((float)L_0-(float)((float)((float)L_3*(float)L_4))));
	}
}
// System.Single UnityEngine.Mathf::DeltaAngle(System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Mathf_DeltaAngle_m6_400 (Object_t * __this /* static, unused */, float ___current, float ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___target;
		float L_1 = ___current;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Repeat_m6_399(NULL /*static, unused*/, ((float)((float)L_0-(float)L_1)), (360.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) > ((float)(180.0f)))))
		{
			goto IL_0021;
		}
	}
	{
		float L_4 = V_0;
		V_0 = ((float)((float)L_4-(float)(360.0f)));
	}

IL_0021:
	{
		float L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C" void ReapplyDrivenProperties__ctor_m6_401 (ReapplyDrivenProperties_t6_59 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C" void ReapplyDrivenProperties_Invoke_m6_402 (ReapplyDrivenProperties_t6_59 * __this, RectTransform_t6_60 * ___driven, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ReapplyDrivenProperties_Invoke_m6_402((ReapplyDrivenProperties_t6_59 *)__this->___prev_9,___driven, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, RectTransform_t6_60 * ___driven, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___driven,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, RectTransform_t6_60 * ___driven, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___driven,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___driven,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t6_59(Il2CppObject* delegate, RectTransform_t6_60 * ___driven)
{
	// Marshaling of parameter '___driven' to native representation
	RectTransform_t6_60 * ____driven_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.RectTransform'."));
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C" Object_t * ReapplyDrivenProperties_BeginInvoke_m6_403 (ReapplyDrivenProperties_t6_59 * __this, RectTransform_t6_60 * ___driven, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C" void ReapplyDrivenProperties_EndInvoke_m6_404 (ReapplyDrivenProperties_t6_59 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern TypeInfo* RectTransform_t6_60_il2cpp_TypeInfo_var;
extern "C" void RectTransform_SendReapplyDrivenProperties_m6_405 (Object_t * __this /* static, unused */, RectTransform_t6_60 * ___driven, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t6_60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(948);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t6_59 * L_0 = ((RectTransform_t6_60_StaticFields*)RectTransform_t6_60_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ReapplyDrivenProperties_t6_59 * L_1 = ((RectTransform_t6_60_StaticFields*)RectTransform_t6_60_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2;
		RectTransform_t6_60 * L_2 = ___driven;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m6_402(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C" void ResourceRequest__ctor_m6_406 (ResourceRequest_t6_62 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m6_438(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C" Object_t6_5 * ResourceRequest_get_asset_m6_407 (ResourceRequest_t6_62 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Path_1);
		Type_t * L_1 = (__this->___m_Type_2);
		Object_t6_5 * L_2 = Resources_Load_m6_409(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object[] UnityEngine.Resources::FindObjectsOfTypeAll(System.Type)
extern "C" ObjectU5BU5D_t6_252* Resources_FindObjectsOfTypeAll_m6_408 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t6_252* (*Resources_FindObjectsOfTypeAll_m6_408_ftn) (Type_t *);
	static Resources_FindObjectsOfTypeAll_m6_408_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_FindObjectsOfTypeAll_m6_408_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::FindObjectsOfTypeAll(System.Type)");
	return _il2cpp_icall_func(___type);
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C" Object_t6_5 * Resources_Load_m6_409 (Object_t * __this /* static, unused */, String_t* ___path, Type_t * ___systemTypeInstance, const MethodInfo* method)
{
	typedef Object_t6_5 * (*Resources_Load_m6_409_ftn) (String_t*, Type_t *);
	static Resources_Load_m6_409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m6_409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	return _il2cpp_icall_func(___path, ___systemTypeInstance);
}
// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C" void SerializePrivateVariables__ctor_m6_410 (SerializePrivateVariables_t6_64 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C" void SerializeField__ctor_m6_411 (SerializeField_t6_65 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C" int32_t Shader_PropertyToID_m6_412 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef int32_t (*Shader_PropertyToID_m6_412_ftn) (String_t*);
	static Shader_PropertyToID_m6_412_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m6_412_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern "C" void Material__ctor_m6_413 (Material_t6_67 * __this, Material_t6_67 * ___source, const MethodInfo* method)
{
	{
		Object__ctor_m6_553(__this, /*hidden argument*/NULL);
		Material_t6_67 * L_0 = ___source;
		Material_Internal_CreateWithMaterial_m6_418(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern Il2CppCodeGenString* _stringLiteral2704;
extern "C" void Material_set_color_m6_414 (Material_t6_67 * __this, Color_t6_40  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2704 = il2cpp_codegen_string_literal_from_index(2704);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t6_40  L_0 = ___value;
		Material_SetColor_m6_415(__this, _stringLiteral2704, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C" void Material_SetColor_m6_415 (Material_t6_67 * __this, String_t* ___propertyName, Color_t6_40  ___color, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m6_412(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t6_40  L_2 = ___color;
		Material_SetColor_m6_416(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern "C" void Material_SetColor_m6_416 (Material_t6_67 * __this, int32_t ___nameID, Color_t6_40  ___color, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID;
		Material_INTERNAL_CALL_SetColor_m6_417(NULL /*static, unused*/, __this, L_0, (&___color), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)
extern "C" void Material_INTERNAL_CALL_SetColor_m6_417 (Object_t * __this /* static, unused */, Material_t6_67 * ___self, int32_t ___nameID, Color_t6_40 * ___color, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_SetColor_m6_417_ftn) (Material_t6_67 *, int32_t, Color_t6_40 *);
	static Material_INTERNAL_CALL_SetColor_m6_417_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetColor_m6_417_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self, ___nameID, ___color);
}
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern "C" void Material_Internal_CreateWithMaterial_m6_418 (Object_t * __this /* static, unused */, Material_t6_67 * ___mono, Material_t6_67 * ___source, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithMaterial_m6_418_ftn) (Material_t6_67 *, Material_t6_67 *);
	static Material_Internal_CreateWithMaterial_m6_418_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithMaterial_m6_418_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)");
	_il2cpp_icall_func(___mono, ___source);
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::Clear()
extern "C" void SphericalHarmonicsL2_Clear_m6_419 (SphericalHarmonicsL2_t6_68 * __this, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_ClearInternal_m6_420(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::ClearInternal(UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_ClearInternal_m6_420 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t6_68 * ___sh, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_t6_68 * L_0 = ___sh;
		SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6_421(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_ClearInternal(UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6_421 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t6_68 * ___sh, const MethodInfo* method)
{
	typedef void (*SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6_421_ftn) (SphericalHarmonicsL2_t6_68 *);
	static SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6_421_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6_421_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_ClearInternal(UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___sh);
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::AddAmbientLight(UnityEngine.Color)
extern "C" void SphericalHarmonicsL2_AddAmbientLight_m6_422 (SphericalHarmonicsL2_t6_68 * __this, Color_t6_40  ___color, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = ___color;
		SphericalHarmonicsL2_AddAmbientLightInternal_m6_423(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::AddAmbientLightInternal(UnityEngine.Color,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_AddAmbientLightInternal_m6_423 (Object_t * __this /* static, unused */, Color_t6_40  ___color, SphericalHarmonicsL2_t6_68 * ___sh, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_t6_68 * L_0 = ___sh;
		SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6_424(NULL /*static, unused*/, (&___color), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddAmbientLightInternal(UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6_424 (Object_t * __this /* static, unused */, Color_t6_40 * ___color, SphericalHarmonicsL2_t6_68 * ___sh, const MethodInfo* method)
{
	typedef void (*SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6_424_ftn) (Color_t6_40 *, SphericalHarmonicsL2_t6_68 *);
	static SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6_424_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6_424_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddAmbientLightInternal(UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___color, ___sh);
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::AddDirectionalLight(UnityEngine.Vector3,UnityEngine.Color,System.Single)
extern "C" void SphericalHarmonicsL2_AddDirectionalLight_m6_425 (SphericalHarmonicsL2_t6_68 * __this, Vector3_t6_49  ___direction, Color_t6_40  ___color, float ___intensity, const MethodInfo* method)
{
	Color_t6_40  V_0 = {0};
	{
		Color_t6_40  L_0 = ___color;
		float L_1 = ___intensity;
		Color_t6_40  L_2 = Color_op_Multiply_m6_239(NULL /*static, unused*/, L_0, ((float)((float)(2.0f)*(float)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t6_49  L_3 = ___direction;
		Color_t6_40  L_4 = V_0;
		SphericalHarmonicsL2_AddDirectionalLightInternal_m6_426(NULL /*static, unused*/, L_3, L_4, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::AddDirectionalLightInternal(UnityEngine.Vector3,UnityEngine.Color,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_AddDirectionalLightInternal_m6_426 (Object_t * __this /* static, unused */, Vector3_t6_49  ___direction, Color_t6_40  ___color, SphericalHarmonicsL2_t6_68 * ___sh, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_t6_68 * L_0 = ___sh;
		SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6_427(NULL /*static, unused*/, (&___direction), (&___color), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddDirectionalLightInternal(UnityEngine.Vector3&,UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6_427 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___direction, Color_t6_40 * ___color, SphericalHarmonicsL2_t6_68 * ___sh, const MethodInfo* method)
{
	typedef void (*SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6_427_ftn) (Vector3_t6_49 *, Color_t6_40 *, SphericalHarmonicsL2_t6_68 *);
	static SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6_427_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6_427_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddDirectionalLightInternal(UnityEngine.Vector3&,UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___direction, ___color, ___sh);
}
// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::get_Item(System.Int32,System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1_731_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2705;
extern "C" float SphericalHarmonicsL2_get_Item_m6_428 (SphericalHarmonicsL2_t6_68 * __this, int32_t ___rgb, int32_t ___coefficient, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_731_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		_stringLiteral2705 = il2cpp_codegen_string_literal_from_index(2705);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___rgb;
		int32_t L_1 = ___coefficient;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)9)))+(int32_t)L_1));
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (L_3 == 0)
		{
			goto IL_0080;
		}
		if (L_3 == 1)
		{
			goto IL_0087;
		}
		if (L_3 == 2)
		{
			goto IL_008e;
		}
		if (L_3 == 3)
		{
			goto IL_0095;
		}
		if (L_3 == 4)
		{
			goto IL_009c;
		}
		if (L_3 == 5)
		{
			goto IL_00a3;
		}
		if (L_3 == 6)
		{
			goto IL_00aa;
		}
		if (L_3 == 7)
		{
			goto IL_00b1;
		}
		if (L_3 == 8)
		{
			goto IL_00b8;
		}
		if (L_3 == 9)
		{
			goto IL_00bf;
		}
		if (L_3 == 10)
		{
			goto IL_00c6;
		}
		if (L_3 == 11)
		{
			goto IL_00cd;
		}
		if (L_3 == 12)
		{
			goto IL_00d4;
		}
		if (L_3 == 13)
		{
			goto IL_00db;
		}
		if (L_3 == 14)
		{
			goto IL_00e2;
		}
		if (L_3 == 15)
		{
			goto IL_00e9;
		}
		if (L_3 == 16)
		{
			goto IL_00f0;
		}
		if (L_3 == 17)
		{
			goto IL_00f7;
		}
		if (L_3 == 18)
		{
			goto IL_00fe;
		}
		if (L_3 == 19)
		{
			goto IL_0105;
		}
		if (L_3 == 20)
		{
			goto IL_010c;
		}
		if (L_3 == 21)
		{
			goto IL_0113;
		}
		if (L_3 == 22)
		{
			goto IL_011a;
		}
		if (L_3 == 23)
		{
			goto IL_0121;
		}
		if (L_3 == 24)
		{
			goto IL_0128;
		}
		if (L_3 == 25)
		{
			goto IL_012f;
		}
		if (L_3 == 26)
		{
			goto IL_0136;
		}
	}
	{
		goto IL_013d;
	}

IL_0080:
	{
		float L_4 = (__this->___shr0_0);
		return L_4;
	}

IL_0087:
	{
		float L_5 = (__this->___shr1_1);
		return L_5;
	}

IL_008e:
	{
		float L_6 = (__this->___shr2_2);
		return L_6;
	}

IL_0095:
	{
		float L_7 = (__this->___shr3_3);
		return L_7;
	}

IL_009c:
	{
		float L_8 = (__this->___shr4_4);
		return L_8;
	}

IL_00a3:
	{
		float L_9 = (__this->___shr5_5);
		return L_9;
	}

IL_00aa:
	{
		float L_10 = (__this->___shr6_6);
		return L_10;
	}

IL_00b1:
	{
		float L_11 = (__this->___shr7_7);
		return L_11;
	}

IL_00b8:
	{
		float L_12 = (__this->___shr8_8);
		return L_12;
	}

IL_00bf:
	{
		float L_13 = (__this->___shg0_9);
		return L_13;
	}

IL_00c6:
	{
		float L_14 = (__this->___shg1_10);
		return L_14;
	}

IL_00cd:
	{
		float L_15 = (__this->___shg2_11);
		return L_15;
	}

IL_00d4:
	{
		float L_16 = (__this->___shg3_12);
		return L_16;
	}

IL_00db:
	{
		float L_17 = (__this->___shg4_13);
		return L_17;
	}

IL_00e2:
	{
		float L_18 = (__this->___shg5_14);
		return L_18;
	}

IL_00e9:
	{
		float L_19 = (__this->___shg6_15);
		return L_19;
	}

IL_00f0:
	{
		float L_20 = (__this->___shg7_16);
		return L_20;
	}

IL_00f7:
	{
		float L_21 = (__this->___shg8_17);
		return L_21;
	}

IL_00fe:
	{
		float L_22 = (__this->___shb0_18);
		return L_22;
	}

IL_0105:
	{
		float L_23 = (__this->___shb1_19);
		return L_23;
	}

IL_010c:
	{
		float L_24 = (__this->___shb2_20);
		return L_24;
	}

IL_0113:
	{
		float L_25 = (__this->___shb3_21);
		return L_25;
	}

IL_011a:
	{
		float L_26 = (__this->___shb4_22);
		return L_26;
	}

IL_0121:
	{
		float L_27 = (__this->___shb5_23);
		return L_27;
	}

IL_0128:
	{
		float L_28 = (__this->___shb6_24);
		return L_28;
	}

IL_012f:
	{
		float L_29 = (__this->___shb7_25);
		return L_29;
	}

IL_0136:
	{
		float L_30 = (__this->___shb8_26);
		return L_30;
	}

IL_013d:
	{
		IndexOutOfRangeException_t1_731 * L_31 = (IndexOutOfRangeException_t1_731 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_731_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_5085(L_31, _stringLiteral2705, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_31);
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::set_Item(System.Int32,System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1_731_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2705;
extern "C" void SphericalHarmonicsL2_set_Item_m6_429 (SphericalHarmonicsL2_t6_68 * __this, int32_t ___rgb, int32_t ___coefficient, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_731_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		_stringLiteral2705 = il2cpp_codegen_string_literal_from_index(2705);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___rgb;
		int32_t L_1 = ___coefficient;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)9)))+(int32_t)L_1));
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (L_3 == 0)
		{
			goto IL_0080;
		}
		if (L_3 == 1)
		{
			goto IL_008c;
		}
		if (L_3 == 2)
		{
			goto IL_0098;
		}
		if (L_3 == 3)
		{
			goto IL_00a4;
		}
		if (L_3 == 4)
		{
			goto IL_00b0;
		}
		if (L_3 == 5)
		{
			goto IL_00bc;
		}
		if (L_3 == 6)
		{
			goto IL_00c8;
		}
		if (L_3 == 7)
		{
			goto IL_00d4;
		}
		if (L_3 == 8)
		{
			goto IL_00e0;
		}
		if (L_3 == 9)
		{
			goto IL_00ec;
		}
		if (L_3 == 10)
		{
			goto IL_00f8;
		}
		if (L_3 == 11)
		{
			goto IL_0104;
		}
		if (L_3 == 12)
		{
			goto IL_0110;
		}
		if (L_3 == 13)
		{
			goto IL_011c;
		}
		if (L_3 == 14)
		{
			goto IL_0128;
		}
		if (L_3 == 15)
		{
			goto IL_0134;
		}
		if (L_3 == 16)
		{
			goto IL_0140;
		}
		if (L_3 == 17)
		{
			goto IL_014c;
		}
		if (L_3 == 18)
		{
			goto IL_0158;
		}
		if (L_3 == 19)
		{
			goto IL_0164;
		}
		if (L_3 == 20)
		{
			goto IL_0170;
		}
		if (L_3 == 21)
		{
			goto IL_017c;
		}
		if (L_3 == 22)
		{
			goto IL_0188;
		}
		if (L_3 == 23)
		{
			goto IL_0194;
		}
		if (L_3 == 24)
		{
			goto IL_01a0;
		}
		if (L_3 == 25)
		{
			goto IL_01ac;
		}
		if (L_3 == 26)
		{
			goto IL_01b8;
		}
	}
	{
		goto IL_01c4;
	}

IL_0080:
	{
		float L_4 = ___value;
		__this->___shr0_0 = L_4;
		goto IL_01cf;
	}

IL_008c:
	{
		float L_5 = ___value;
		__this->___shr1_1 = L_5;
		goto IL_01cf;
	}

IL_0098:
	{
		float L_6 = ___value;
		__this->___shr2_2 = L_6;
		goto IL_01cf;
	}

IL_00a4:
	{
		float L_7 = ___value;
		__this->___shr3_3 = L_7;
		goto IL_01cf;
	}

IL_00b0:
	{
		float L_8 = ___value;
		__this->___shr4_4 = L_8;
		goto IL_01cf;
	}

IL_00bc:
	{
		float L_9 = ___value;
		__this->___shr5_5 = L_9;
		goto IL_01cf;
	}

IL_00c8:
	{
		float L_10 = ___value;
		__this->___shr6_6 = L_10;
		goto IL_01cf;
	}

IL_00d4:
	{
		float L_11 = ___value;
		__this->___shr7_7 = L_11;
		goto IL_01cf;
	}

IL_00e0:
	{
		float L_12 = ___value;
		__this->___shr8_8 = L_12;
		goto IL_01cf;
	}

IL_00ec:
	{
		float L_13 = ___value;
		__this->___shg0_9 = L_13;
		goto IL_01cf;
	}

IL_00f8:
	{
		float L_14 = ___value;
		__this->___shg1_10 = L_14;
		goto IL_01cf;
	}

IL_0104:
	{
		float L_15 = ___value;
		__this->___shg2_11 = L_15;
		goto IL_01cf;
	}

IL_0110:
	{
		float L_16 = ___value;
		__this->___shg3_12 = L_16;
		goto IL_01cf;
	}

IL_011c:
	{
		float L_17 = ___value;
		__this->___shg4_13 = L_17;
		goto IL_01cf;
	}

IL_0128:
	{
		float L_18 = ___value;
		__this->___shg5_14 = L_18;
		goto IL_01cf;
	}

IL_0134:
	{
		float L_19 = ___value;
		__this->___shg6_15 = L_19;
		goto IL_01cf;
	}

IL_0140:
	{
		float L_20 = ___value;
		__this->___shg7_16 = L_20;
		goto IL_01cf;
	}

IL_014c:
	{
		float L_21 = ___value;
		__this->___shg8_17 = L_21;
		goto IL_01cf;
	}

IL_0158:
	{
		float L_22 = ___value;
		__this->___shb0_18 = L_22;
		goto IL_01cf;
	}

IL_0164:
	{
		float L_23 = ___value;
		__this->___shb1_19 = L_23;
		goto IL_01cf;
	}

IL_0170:
	{
		float L_24 = ___value;
		__this->___shb2_20 = L_24;
		goto IL_01cf;
	}

IL_017c:
	{
		float L_25 = ___value;
		__this->___shb3_21 = L_25;
		goto IL_01cf;
	}

IL_0188:
	{
		float L_26 = ___value;
		__this->___shb4_22 = L_26;
		goto IL_01cf;
	}

IL_0194:
	{
		float L_27 = ___value;
		__this->___shb5_23 = L_27;
		goto IL_01cf;
	}

IL_01a0:
	{
		float L_28 = ___value;
		__this->___shb6_24 = L_28;
		goto IL_01cf;
	}

IL_01ac:
	{
		float L_29 = ___value;
		__this->___shb7_25 = L_29;
		goto IL_01cf;
	}

IL_01b8:
	{
		float L_30 = ___value;
		__this->___shb8_26 = L_30;
		goto IL_01cf;
	}

IL_01c4:
	{
		IndexOutOfRangeException_t1_731 * L_31 = (IndexOutOfRangeException_t1_731 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_731_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_5085(L_31, _stringLiteral2705, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_31);
	}

IL_01cf:
	{
		return;
	}
}
// System.Int32 UnityEngine.Rendering.SphericalHarmonicsL2::GetHashCode()
extern "C" int32_t SphericalHarmonicsL2_GetHashCode_m6_430 (SphericalHarmonicsL2_t6_68 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)17);
		int32_t L_0 = V_0;
		float* L_1 = &(__this->___shr0_0);
		int32_t L_2 = Single_GetHashCode_m1_475(L_1, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)23)))+(int32_t)L_2));
		int32_t L_3 = V_0;
		float* L_4 = &(__this->___shr1_1);
		int32_t L_5 = Single_GetHashCode_m1_475(L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_3*(int32_t)((int32_t)23)))+(int32_t)L_5));
		int32_t L_6 = V_0;
		float* L_7 = &(__this->___shr2_2);
		int32_t L_8 = Single_GetHashCode_m1_475(L_7, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6*(int32_t)((int32_t)23)))+(int32_t)L_8));
		int32_t L_9 = V_0;
		float* L_10 = &(__this->___shr3_3);
		int32_t L_11 = Single_GetHashCode_m1_475(L_10, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9*(int32_t)((int32_t)23)))+(int32_t)L_11));
		int32_t L_12 = V_0;
		float* L_13 = &(__this->___shr4_4);
		int32_t L_14 = Single_GetHashCode_m1_475(L_13, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_12*(int32_t)((int32_t)23)))+(int32_t)L_14));
		int32_t L_15 = V_0;
		float* L_16 = &(__this->___shr5_5);
		int32_t L_17 = Single_GetHashCode_m1_475(L_16, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_15*(int32_t)((int32_t)23)))+(int32_t)L_17));
		int32_t L_18 = V_0;
		float* L_19 = &(__this->___shr6_6);
		int32_t L_20 = Single_GetHashCode_m1_475(L_19, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_18*(int32_t)((int32_t)23)))+(int32_t)L_20));
		int32_t L_21 = V_0;
		float* L_22 = &(__this->___shr7_7);
		int32_t L_23 = Single_GetHashCode_m1_475(L_22, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_21*(int32_t)((int32_t)23)))+(int32_t)L_23));
		int32_t L_24 = V_0;
		float* L_25 = &(__this->___shr8_8);
		int32_t L_26 = Single_GetHashCode_m1_475(L_25, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_24*(int32_t)((int32_t)23)))+(int32_t)L_26));
		int32_t L_27 = V_0;
		float* L_28 = &(__this->___shg0_9);
		int32_t L_29 = Single_GetHashCode_m1_475(L_28, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_27*(int32_t)((int32_t)23)))+(int32_t)L_29));
		int32_t L_30 = V_0;
		float* L_31 = &(__this->___shg1_10);
		int32_t L_32 = Single_GetHashCode_m1_475(L_31, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_30*(int32_t)((int32_t)23)))+(int32_t)L_32));
		int32_t L_33 = V_0;
		float* L_34 = &(__this->___shg2_11);
		int32_t L_35 = Single_GetHashCode_m1_475(L_34, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_33*(int32_t)((int32_t)23)))+(int32_t)L_35));
		int32_t L_36 = V_0;
		float* L_37 = &(__this->___shg3_12);
		int32_t L_38 = Single_GetHashCode_m1_475(L_37, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_36*(int32_t)((int32_t)23)))+(int32_t)L_38));
		int32_t L_39 = V_0;
		float* L_40 = &(__this->___shg4_13);
		int32_t L_41 = Single_GetHashCode_m1_475(L_40, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_39*(int32_t)((int32_t)23)))+(int32_t)L_41));
		int32_t L_42 = V_0;
		float* L_43 = &(__this->___shg5_14);
		int32_t L_44 = Single_GetHashCode_m1_475(L_43, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_42*(int32_t)((int32_t)23)))+(int32_t)L_44));
		int32_t L_45 = V_0;
		float* L_46 = &(__this->___shg6_15);
		int32_t L_47 = Single_GetHashCode_m1_475(L_46, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_45*(int32_t)((int32_t)23)))+(int32_t)L_47));
		int32_t L_48 = V_0;
		float* L_49 = &(__this->___shg7_16);
		int32_t L_50 = Single_GetHashCode_m1_475(L_49, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_48*(int32_t)((int32_t)23)))+(int32_t)L_50));
		int32_t L_51 = V_0;
		float* L_52 = &(__this->___shg8_17);
		int32_t L_53 = Single_GetHashCode_m1_475(L_52, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_51*(int32_t)((int32_t)23)))+(int32_t)L_53));
		int32_t L_54 = V_0;
		float* L_55 = &(__this->___shb0_18);
		int32_t L_56 = Single_GetHashCode_m1_475(L_55, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_54*(int32_t)((int32_t)23)))+(int32_t)L_56));
		int32_t L_57 = V_0;
		float* L_58 = &(__this->___shb1_19);
		int32_t L_59 = Single_GetHashCode_m1_475(L_58, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_57*(int32_t)((int32_t)23)))+(int32_t)L_59));
		int32_t L_60 = V_0;
		float* L_61 = &(__this->___shb2_20);
		int32_t L_62 = Single_GetHashCode_m1_475(L_61, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_60*(int32_t)((int32_t)23)))+(int32_t)L_62));
		int32_t L_63 = V_0;
		float* L_64 = &(__this->___shb3_21);
		int32_t L_65 = Single_GetHashCode_m1_475(L_64, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_63*(int32_t)((int32_t)23)))+(int32_t)L_65));
		int32_t L_66 = V_0;
		float* L_67 = &(__this->___shb4_22);
		int32_t L_68 = Single_GetHashCode_m1_475(L_67, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_66*(int32_t)((int32_t)23)))+(int32_t)L_68));
		int32_t L_69 = V_0;
		float* L_70 = &(__this->___shb5_23);
		int32_t L_71 = Single_GetHashCode_m1_475(L_70, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_69*(int32_t)((int32_t)23)))+(int32_t)L_71));
		int32_t L_72 = V_0;
		float* L_73 = &(__this->___shb6_24);
		int32_t L_74 = Single_GetHashCode_m1_475(L_73, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_72*(int32_t)((int32_t)23)))+(int32_t)L_74));
		int32_t L_75 = V_0;
		float* L_76 = &(__this->___shb7_25);
		int32_t L_77 = Single_GetHashCode_m1_475(L_76, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_75*(int32_t)((int32_t)23)))+(int32_t)L_77));
		int32_t L_78 = V_0;
		float* L_79 = &(__this->___shb8_26);
		int32_t L_80 = Single_GetHashCode_m1_475(L_79, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_78*(int32_t)((int32_t)23)))+(int32_t)L_80));
		int32_t L_81 = V_0;
		return L_81;
	}
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::Equals(System.Object)
extern TypeInfo* SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var;
extern "C" bool SphericalHarmonicsL2_Equals_m6_431 (SphericalHarmonicsL2_t6_68 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(949);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t6_68  V_0 = {0};
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(SphericalHarmonicsL2_t6_68 *)((SphericalHarmonicsL2_t6_68 *)UnBox (L_1, SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var))));
		SphericalHarmonicsL2_t6_68  L_2 = V_0;
		bool L_3 = SphericalHarmonicsL2_op_Equality_m6_435(NULL /*static, unused*/, (*(SphericalHarmonicsL2_t6_68 *)__this), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Rendering.SphericalHarmonicsL2 UnityEngine.Rendering.SphericalHarmonicsL2::op_Multiply(UnityEngine.Rendering.SphericalHarmonicsL2,System.Single)
extern TypeInfo* SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var;
extern "C" SphericalHarmonicsL2_t6_68  SphericalHarmonicsL2_op_Multiply_m6_432 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t6_68  ___lhs, float ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(949);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t6_68  V_0 = {0};
	{
		Initobj (SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___lhs)->___shr0_0);
		float L_1 = ___rhs;
		(&V_0)->___shr0_0 = ((float)((float)L_0*(float)L_1));
		float L_2 = ((&___lhs)->___shr1_1);
		float L_3 = ___rhs;
		(&V_0)->___shr1_1 = ((float)((float)L_2*(float)L_3));
		float L_4 = ((&___lhs)->___shr2_2);
		float L_5 = ___rhs;
		(&V_0)->___shr2_2 = ((float)((float)L_4*(float)L_5));
		float L_6 = ((&___lhs)->___shr3_3);
		float L_7 = ___rhs;
		(&V_0)->___shr3_3 = ((float)((float)L_6*(float)L_7));
		float L_8 = ((&___lhs)->___shr4_4);
		float L_9 = ___rhs;
		(&V_0)->___shr4_4 = ((float)((float)L_8*(float)L_9));
		float L_10 = ((&___lhs)->___shr5_5);
		float L_11 = ___rhs;
		(&V_0)->___shr5_5 = ((float)((float)L_10*(float)L_11));
		float L_12 = ((&___lhs)->___shr6_6);
		float L_13 = ___rhs;
		(&V_0)->___shr6_6 = ((float)((float)L_12*(float)L_13));
		float L_14 = ((&___lhs)->___shr7_7);
		float L_15 = ___rhs;
		(&V_0)->___shr7_7 = ((float)((float)L_14*(float)L_15));
		float L_16 = ((&___lhs)->___shr8_8);
		float L_17 = ___rhs;
		(&V_0)->___shr8_8 = ((float)((float)L_16*(float)L_17));
		float L_18 = ((&___lhs)->___shg0_9);
		float L_19 = ___rhs;
		(&V_0)->___shg0_9 = ((float)((float)L_18*(float)L_19));
		float L_20 = ((&___lhs)->___shg1_10);
		float L_21 = ___rhs;
		(&V_0)->___shg1_10 = ((float)((float)L_20*(float)L_21));
		float L_22 = ((&___lhs)->___shg2_11);
		float L_23 = ___rhs;
		(&V_0)->___shg2_11 = ((float)((float)L_22*(float)L_23));
		float L_24 = ((&___lhs)->___shg3_12);
		float L_25 = ___rhs;
		(&V_0)->___shg3_12 = ((float)((float)L_24*(float)L_25));
		float L_26 = ((&___lhs)->___shg4_13);
		float L_27 = ___rhs;
		(&V_0)->___shg4_13 = ((float)((float)L_26*(float)L_27));
		float L_28 = ((&___lhs)->___shg5_14);
		float L_29 = ___rhs;
		(&V_0)->___shg5_14 = ((float)((float)L_28*(float)L_29));
		float L_30 = ((&___lhs)->___shg6_15);
		float L_31 = ___rhs;
		(&V_0)->___shg6_15 = ((float)((float)L_30*(float)L_31));
		float L_32 = ((&___lhs)->___shg7_16);
		float L_33 = ___rhs;
		(&V_0)->___shg7_16 = ((float)((float)L_32*(float)L_33));
		float L_34 = ((&___lhs)->___shg8_17);
		float L_35 = ___rhs;
		(&V_0)->___shg8_17 = ((float)((float)L_34*(float)L_35));
		float L_36 = ((&___lhs)->___shb0_18);
		float L_37 = ___rhs;
		(&V_0)->___shb0_18 = ((float)((float)L_36*(float)L_37));
		float L_38 = ((&___lhs)->___shb1_19);
		float L_39 = ___rhs;
		(&V_0)->___shb1_19 = ((float)((float)L_38*(float)L_39));
		float L_40 = ((&___lhs)->___shb2_20);
		float L_41 = ___rhs;
		(&V_0)->___shb2_20 = ((float)((float)L_40*(float)L_41));
		float L_42 = ((&___lhs)->___shb3_21);
		float L_43 = ___rhs;
		(&V_0)->___shb3_21 = ((float)((float)L_42*(float)L_43));
		float L_44 = ((&___lhs)->___shb4_22);
		float L_45 = ___rhs;
		(&V_0)->___shb4_22 = ((float)((float)L_44*(float)L_45));
		float L_46 = ((&___lhs)->___shb5_23);
		float L_47 = ___rhs;
		(&V_0)->___shb5_23 = ((float)((float)L_46*(float)L_47));
		float L_48 = ((&___lhs)->___shb6_24);
		float L_49 = ___rhs;
		(&V_0)->___shb6_24 = ((float)((float)L_48*(float)L_49));
		float L_50 = ((&___lhs)->___shb7_25);
		float L_51 = ___rhs;
		(&V_0)->___shb7_25 = ((float)((float)L_50*(float)L_51));
		float L_52 = ((&___lhs)->___shb8_26);
		float L_53 = ___rhs;
		(&V_0)->___shb8_26 = ((float)((float)L_52*(float)L_53));
		SphericalHarmonicsL2_t6_68  L_54 = V_0;
		return L_54;
	}
}
// UnityEngine.Rendering.SphericalHarmonicsL2 UnityEngine.Rendering.SphericalHarmonicsL2::op_Multiply(System.Single,UnityEngine.Rendering.SphericalHarmonicsL2)
extern TypeInfo* SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var;
extern "C" SphericalHarmonicsL2_t6_68  SphericalHarmonicsL2_op_Multiply_m6_433 (Object_t * __this /* static, unused */, float ___lhs, SphericalHarmonicsL2_t6_68  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(949);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t6_68  V_0 = {0};
	{
		Initobj (SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___rhs)->___shr0_0);
		float L_1 = ___lhs;
		(&V_0)->___shr0_0 = ((float)((float)L_0*(float)L_1));
		float L_2 = ((&___rhs)->___shr1_1);
		float L_3 = ___lhs;
		(&V_0)->___shr1_1 = ((float)((float)L_2*(float)L_3));
		float L_4 = ((&___rhs)->___shr2_2);
		float L_5 = ___lhs;
		(&V_0)->___shr2_2 = ((float)((float)L_4*(float)L_5));
		float L_6 = ((&___rhs)->___shr3_3);
		float L_7 = ___lhs;
		(&V_0)->___shr3_3 = ((float)((float)L_6*(float)L_7));
		float L_8 = ((&___rhs)->___shr4_4);
		float L_9 = ___lhs;
		(&V_0)->___shr4_4 = ((float)((float)L_8*(float)L_9));
		float L_10 = ((&___rhs)->___shr5_5);
		float L_11 = ___lhs;
		(&V_0)->___shr5_5 = ((float)((float)L_10*(float)L_11));
		float L_12 = ((&___rhs)->___shr6_6);
		float L_13 = ___lhs;
		(&V_0)->___shr6_6 = ((float)((float)L_12*(float)L_13));
		float L_14 = ((&___rhs)->___shr7_7);
		float L_15 = ___lhs;
		(&V_0)->___shr7_7 = ((float)((float)L_14*(float)L_15));
		float L_16 = ((&___rhs)->___shr8_8);
		float L_17 = ___lhs;
		(&V_0)->___shr8_8 = ((float)((float)L_16*(float)L_17));
		float L_18 = ((&___rhs)->___shg0_9);
		float L_19 = ___lhs;
		(&V_0)->___shg0_9 = ((float)((float)L_18*(float)L_19));
		float L_20 = ((&___rhs)->___shg1_10);
		float L_21 = ___lhs;
		(&V_0)->___shg1_10 = ((float)((float)L_20*(float)L_21));
		float L_22 = ((&___rhs)->___shg2_11);
		float L_23 = ___lhs;
		(&V_0)->___shg2_11 = ((float)((float)L_22*(float)L_23));
		float L_24 = ((&___rhs)->___shg3_12);
		float L_25 = ___lhs;
		(&V_0)->___shg3_12 = ((float)((float)L_24*(float)L_25));
		float L_26 = ((&___rhs)->___shg4_13);
		float L_27 = ___lhs;
		(&V_0)->___shg4_13 = ((float)((float)L_26*(float)L_27));
		float L_28 = ((&___rhs)->___shg5_14);
		float L_29 = ___lhs;
		(&V_0)->___shg5_14 = ((float)((float)L_28*(float)L_29));
		float L_30 = ((&___rhs)->___shg6_15);
		float L_31 = ___lhs;
		(&V_0)->___shg6_15 = ((float)((float)L_30*(float)L_31));
		float L_32 = ((&___rhs)->___shg7_16);
		float L_33 = ___lhs;
		(&V_0)->___shg7_16 = ((float)((float)L_32*(float)L_33));
		float L_34 = ((&___rhs)->___shg8_17);
		float L_35 = ___lhs;
		(&V_0)->___shg8_17 = ((float)((float)L_34*(float)L_35));
		float L_36 = ((&___rhs)->___shb0_18);
		float L_37 = ___lhs;
		(&V_0)->___shb0_18 = ((float)((float)L_36*(float)L_37));
		float L_38 = ((&___rhs)->___shb1_19);
		float L_39 = ___lhs;
		(&V_0)->___shb1_19 = ((float)((float)L_38*(float)L_39));
		float L_40 = ((&___rhs)->___shb2_20);
		float L_41 = ___lhs;
		(&V_0)->___shb2_20 = ((float)((float)L_40*(float)L_41));
		float L_42 = ((&___rhs)->___shb3_21);
		float L_43 = ___lhs;
		(&V_0)->___shb3_21 = ((float)((float)L_42*(float)L_43));
		float L_44 = ((&___rhs)->___shb4_22);
		float L_45 = ___lhs;
		(&V_0)->___shb4_22 = ((float)((float)L_44*(float)L_45));
		float L_46 = ((&___rhs)->___shb5_23);
		float L_47 = ___lhs;
		(&V_0)->___shb5_23 = ((float)((float)L_46*(float)L_47));
		float L_48 = ((&___rhs)->___shb6_24);
		float L_49 = ___lhs;
		(&V_0)->___shb6_24 = ((float)((float)L_48*(float)L_49));
		float L_50 = ((&___rhs)->___shb7_25);
		float L_51 = ___lhs;
		(&V_0)->___shb7_25 = ((float)((float)L_50*(float)L_51));
		float L_52 = ((&___rhs)->___shb8_26);
		float L_53 = ___lhs;
		(&V_0)->___shb8_26 = ((float)((float)L_52*(float)L_53));
		SphericalHarmonicsL2_t6_68  L_54 = V_0;
		return L_54;
	}
}
// UnityEngine.Rendering.SphericalHarmonicsL2 UnityEngine.Rendering.SphericalHarmonicsL2::op_Addition(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern TypeInfo* SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var;
extern "C" SphericalHarmonicsL2_t6_68  SphericalHarmonicsL2_op_Addition_m6_434 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t6_68  ___lhs, SphericalHarmonicsL2_t6_68  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(949);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t6_68  V_0 = {0};
	{
		Initobj (SphericalHarmonicsL2_t6_68_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___lhs)->___shr0_0);
		float L_1 = ((&___rhs)->___shr0_0);
		(&V_0)->___shr0_0 = ((float)((float)L_0+(float)L_1));
		float L_2 = ((&___lhs)->___shr1_1);
		float L_3 = ((&___rhs)->___shr1_1);
		(&V_0)->___shr1_1 = ((float)((float)L_2+(float)L_3));
		float L_4 = ((&___lhs)->___shr2_2);
		float L_5 = ((&___rhs)->___shr2_2);
		(&V_0)->___shr2_2 = ((float)((float)L_4+(float)L_5));
		float L_6 = ((&___lhs)->___shr3_3);
		float L_7 = ((&___rhs)->___shr3_3);
		(&V_0)->___shr3_3 = ((float)((float)L_6+(float)L_7));
		float L_8 = ((&___lhs)->___shr4_4);
		float L_9 = ((&___rhs)->___shr4_4);
		(&V_0)->___shr4_4 = ((float)((float)L_8+(float)L_9));
		float L_10 = ((&___lhs)->___shr5_5);
		float L_11 = ((&___rhs)->___shr5_5);
		(&V_0)->___shr5_5 = ((float)((float)L_10+(float)L_11));
		float L_12 = ((&___lhs)->___shr6_6);
		float L_13 = ((&___rhs)->___shr6_6);
		(&V_0)->___shr6_6 = ((float)((float)L_12+(float)L_13));
		float L_14 = ((&___lhs)->___shr7_7);
		float L_15 = ((&___rhs)->___shr7_7);
		(&V_0)->___shr7_7 = ((float)((float)L_14+(float)L_15));
		float L_16 = ((&___lhs)->___shr8_8);
		float L_17 = ((&___rhs)->___shr8_8);
		(&V_0)->___shr8_8 = ((float)((float)L_16+(float)L_17));
		float L_18 = ((&___lhs)->___shg0_9);
		float L_19 = ((&___rhs)->___shg0_9);
		(&V_0)->___shg0_9 = ((float)((float)L_18+(float)L_19));
		float L_20 = ((&___lhs)->___shg1_10);
		float L_21 = ((&___rhs)->___shg1_10);
		(&V_0)->___shg1_10 = ((float)((float)L_20+(float)L_21));
		float L_22 = ((&___lhs)->___shg2_11);
		float L_23 = ((&___rhs)->___shg2_11);
		(&V_0)->___shg2_11 = ((float)((float)L_22+(float)L_23));
		float L_24 = ((&___lhs)->___shg3_12);
		float L_25 = ((&___rhs)->___shg3_12);
		(&V_0)->___shg3_12 = ((float)((float)L_24+(float)L_25));
		float L_26 = ((&___lhs)->___shg4_13);
		float L_27 = ((&___rhs)->___shg4_13);
		(&V_0)->___shg4_13 = ((float)((float)L_26+(float)L_27));
		float L_28 = ((&___lhs)->___shg5_14);
		float L_29 = ((&___rhs)->___shg5_14);
		(&V_0)->___shg5_14 = ((float)((float)L_28+(float)L_29));
		float L_30 = ((&___lhs)->___shg6_15);
		float L_31 = ((&___rhs)->___shg6_15);
		(&V_0)->___shg6_15 = ((float)((float)L_30+(float)L_31));
		float L_32 = ((&___lhs)->___shg7_16);
		float L_33 = ((&___rhs)->___shg7_16);
		(&V_0)->___shg7_16 = ((float)((float)L_32+(float)L_33));
		float L_34 = ((&___lhs)->___shg8_17);
		float L_35 = ((&___rhs)->___shg8_17);
		(&V_0)->___shg8_17 = ((float)((float)L_34+(float)L_35));
		float L_36 = ((&___lhs)->___shb0_18);
		float L_37 = ((&___rhs)->___shb0_18);
		(&V_0)->___shb0_18 = ((float)((float)L_36+(float)L_37));
		float L_38 = ((&___lhs)->___shb1_19);
		float L_39 = ((&___rhs)->___shb1_19);
		(&V_0)->___shb1_19 = ((float)((float)L_38+(float)L_39));
		float L_40 = ((&___lhs)->___shb2_20);
		float L_41 = ((&___rhs)->___shb2_20);
		(&V_0)->___shb2_20 = ((float)((float)L_40+(float)L_41));
		float L_42 = ((&___lhs)->___shb3_21);
		float L_43 = ((&___rhs)->___shb3_21);
		(&V_0)->___shb3_21 = ((float)((float)L_42+(float)L_43));
		float L_44 = ((&___lhs)->___shb4_22);
		float L_45 = ((&___rhs)->___shb4_22);
		(&V_0)->___shb4_22 = ((float)((float)L_44+(float)L_45));
		float L_46 = ((&___lhs)->___shb5_23);
		float L_47 = ((&___rhs)->___shb5_23);
		(&V_0)->___shb5_23 = ((float)((float)L_46+(float)L_47));
		float L_48 = ((&___lhs)->___shb6_24);
		float L_49 = ((&___rhs)->___shb6_24);
		(&V_0)->___shb6_24 = ((float)((float)L_48+(float)L_49));
		float L_50 = ((&___lhs)->___shb7_25);
		float L_51 = ((&___rhs)->___shb7_25);
		(&V_0)->___shb7_25 = ((float)((float)L_50+(float)L_51));
		float L_52 = ((&___lhs)->___shb8_26);
		float L_53 = ((&___rhs)->___shb8_26);
		(&V_0)->___shb8_26 = ((float)((float)L_52+(float)L_53));
		SphericalHarmonicsL2_t6_68  L_54 = V_0;
		return L_54;
	}
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::op_Equality(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C" bool SphericalHarmonicsL2_op_Equality_m6_435 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t6_68  ___lhs, SphericalHarmonicsL2_t6_68  ___rhs, const MethodInfo* method)
{
	int32_t G_B28_0 = 0;
	{
		float L_0 = ((&___lhs)->___shr0_0);
		float L_1 = ((&___rhs)->___shr0_0);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_0200;
		}
	}
	{
		float L_2 = ((&___lhs)->___shr1_1);
		float L_3 = ((&___rhs)->___shr1_1);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_0200;
		}
	}
	{
		float L_4 = ((&___lhs)->___shr2_2);
		float L_5 = ((&___rhs)->___shr2_2);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_0200;
		}
	}
	{
		float L_6 = ((&___lhs)->___shr3_3);
		float L_7 = ((&___rhs)->___shr3_3);
		if ((!(((float)L_6) == ((float)L_7))))
		{
			goto IL_0200;
		}
	}
	{
		float L_8 = ((&___lhs)->___shr4_4);
		float L_9 = ((&___rhs)->___shr4_4);
		if ((!(((float)L_8) == ((float)L_9))))
		{
			goto IL_0200;
		}
	}
	{
		float L_10 = ((&___lhs)->___shr5_5);
		float L_11 = ((&___rhs)->___shr5_5);
		if ((!(((float)L_10) == ((float)L_11))))
		{
			goto IL_0200;
		}
	}
	{
		float L_12 = ((&___lhs)->___shr6_6);
		float L_13 = ((&___rhs)->___shr6_6);
		if ((!(((float)L_12) == ((float)L_13))))
		{
			goto IL_0200;
		}
	}
	{
		float L_14 = ((&___lhs)->___shr7_7);
		float L_15 = ((&___rhs)->___shr7_7);
		if ((!(((float)L_14) == ((float)L_15))))
		{
			goto IL_0200;
		}
	}
	{
		float L_16 = ((&___lhs)->___shr8_8);
		float L_17 = ((&___rhs)->___shr8_8);
		if ((!(((float)L_16) == ((float)L_17))))
		{
			goto IL_0200;
		}
	}
	{
		float L_18 = ((&___lhs)->___shg0_9);
		float L_19 = ((&___rhs)->___shg0_9);
		if ((!(((float)L_18) == ((float)L_19))))
		{
			goto IL_0200;
		}
	}
	{
		float L_20 = ((&___lhs)->___shg1_10);
		float L_21 = ((&___rhs)->___shg1_10);
		if ((!(((float)L_20) == ((float)L_21))))
		{
			goto IL_0200;
		}
	}
	{
		float L_22 = ((&___lhs)->___shg2_11);
		float L_23 = ((&___rhs)->___shg2_11);
		if ((!(((float)L_22) == ((float)L_23))))
		{
			goto IL_0200;
		}
	}
	{
		float L_24 = ((&___lhs)->___shg3_12);
		float L_25 = ((&___rhs)->___shg3_12);
		if ((!(((float)L_24) == ((float)L_25))))
		{
			goto IL_0200;
		}
	}
	{
		float L_26 = ((&___lhs)->___shg4_13);
		float L_27 = ((&___rhs)->___shg4_13);
		if ((!(((float)L_26) == ((float)L_27))))
		{
			goto IL_0200;
		}
	}
	{
		float L_28 = ((&___lhs)->___shg5_14);
		float L_29 = ((&___rhs)->___shg5_14);
		if ((!(((float)L_28) == ((float)L_29))))
		{
			goto IL_0200;
		}
	}
	{
		float L_30 = ((&___lhs)->___shg6_15);
		float L_31 = ((&___rhs)->___shg6_15);
		if ((!(((float)L_30) == ((float)L_31))))
		{
			goto IL_0200;
		}
	}
	{
		float L_32 = ((&___lhs)->___shg7_16);
		float L_33 = ((&___rhs)->___shg7_16);
		if ((!(((float)L_32) == ((float)L_33))))
		{
			goto IL_0200;
		}
	}
	{
		float L_34 = ((&___lhs)->___shg8_17);
		float L_35 = ((&___rhs)->___shg8_17);
		if ((!(((float)L_34) == ((float)L_35))))
		{
			goto IL_0200;
		}
	}
	{
		float L_36 = ((&___lhs)->___shb0_18);
		float L_37 = ((&___rhs)->___shb0_18);
		if ((!(((float)L_36) == ((float)L_37))))
		{
			goto IL_0200;
		}
	}
	{
		float L_38 = ((&___lhs)->___shb1_19);
		float L_39 = ((&___rhs)->___shb1_19);
		if ((!(((float)L_38) == ((float)L_39))))
		{
			goto IL_0200;
		}
	}
	{
		float L_40 = ((&___lhs)->___shb2_20);
		float L_41 = ((&___rhs)->___shb2_20);
		if ((!(((float)L_40) == ((float)L_41))))
		{
			goto IL_0200;
		}
	}
	{
		float L_42 = ((&___lhs)->___shb3_21);
		float L_43 = ((&___rhs)->___shb3_21);
		if ((!(((float)L_42) == ((float)L_43))))
		{
			goto IL_0200;
		}
	}
	{
		float L_44 = ((&___lhs)->___shb4_22);
		float L_45 = ((&___rhs)->___shb4_22);
		if ((!(((float)L_44) == ((float)L_45))))
		{
			goto IL_0200;
		}
	}
	{
		float L_46 = ((&___lhs)->___shb5_23);
		float L_47 = ((&___rhs)->___shb5_23);
		if ((!(((float)L_46) == ((float)L_47))))
		{
			goto IL_0200;
		}
	}
	{
		float L_48 = ((&___lhs)->___shb6_24);
		float L_49 = ((&___rhs)->___shb6_24);
		if ((!(((float)L_48) == ((float)L_49))))
		{
			goto IL_0200;
		}
	}
	{
		float L_50 = ((&___lhs)->___shb7_25);
		float L_51 = ((&___rhs)->___shb7_25);
		if ((!(((float)L_50) == ((float)L_51))))
		{
			goto IL_0200;
		}
	}
	{
		float L_52 = ((&___lhs)->___shb8_26);
		float L_53 = ((&___rhs)->___shb8_26);
		G_B28_0 = ((((float)L_52) == ((float)L_53))? 1 : 0);
		goto IL_0201;
	}

IL_0200:
	{
		G_B28_0 = 0;
	}

IL_0201:
	{
		return G_B28_0;
	}
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::op_Inequality(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C" bool SphericalHarmonicsL2_op_Inequality_m6_436 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t6_68  ___lhs, SphericalHarmonicsL2_t6_68  ___rhs, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_t6_68  L_0 = ___lhs;
		SphericalHarmonicsL2_t6_68  L_1 = ___rhs;
		bool L_2 = SphericalHarmonicsL2_op_Equality_m6_435(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.CacheIndex
extern "C" void CacheIndex_t6_70_marshal(const CacheIndex_t6_70& unmarshaled, CacheIndex_t6_70_marshaled& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.___name_0);
	marshaled.___bytesUsed_1 = unmarshaled.___bytesUsed_1;
	marshaled.___expires_2 = unmarshaled.___expires_2;
}
extern "C" void CacheIndex_t6_70_marshal_back(const CacheIndex_t6_70_marshaled& marshaled, CacheIndex_t6_70& unmarshaled)
{
	unmarshaled.___name_0 = il2cpp_codegen_marshal_string_result(marshaled.___name_0);
	unmarshaled.___bytesUsed_1 = marshaled.___bytesUsed_1;
	unmarshaled.___expires_2 = marshaled.___expires_2;
}
// Conversion method for clean up from marshalling of: UnityEngine.CacheIndex
extern "C" void CacheIndex_t6_70_marshal_cleanup(CacheIndex_t6_70_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* UnityString_Format_m6_437 (Object_t * __this /* static, unused */, String_t* ___fmt, ObjectU5BU5D_t1_157* ___args, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___fmt;
		ObjectU5BU5D_t1_157* L_1 = ___args;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1_413(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C" void AsyncOperation__ctor_m6_438 (AsyncOperation_t6_2 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m6_662(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C" void AsyncOperation_InternalDestroy_m6_439 (AsyncOperation_t6_2 * __this, const MethodInfo* method)
{
	typedef void (*AsyncOperation_InternalDestroy_m6_439_ftn) (AsyncOperation_t6_2 *);
	static AsyncOperation_InternalDestroy_m6_439_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_InternalDestroy_m6_439_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C" void AsyncOperation_Finalize_m6_440 (AsyncOperation_t6_2 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AsyncOperation_InternalDestroy_m6_439(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t6_2_marshal(const AsyncOperation_t6_2& unmarshaled, AsyncOperation_t6_2_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void AsyncOperation_t6_2_marshal_back(const AsyncOperation_t6_2_marshaled& marshaled, AsyncOperation_t6_2& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t6_2_marshal_cleanup(AsyncOperation_t6_2_marshaled& marshaled)
{
}
// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C" void LogCallback__ctor_m6_441 (LogCallback_t6_72 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C" void LogCallback_Invoke_m6_442 (LogCallback_t6_72 * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		LogCallback_Invoke_m6_442((LogCallback_t6_72 *)__this->___prev_9,___condition, ___stackTrace, ___type, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_LogCallback_t6_72(Il2CppObject* delegate, String_t* ___condition, String_t* ___stackTrace, int32_t ___type)
{
	typedef void (STDCALL *native_function_ptr_type)(char*, char*, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___condition' to native representation
	char* ____condition_marshaled = { 0 };
	____condition_marshaled = il2cpp_codegen_marshal_string(___condition);

	// Marshaling of parameter '___stackTrace' to native representation
	char* ____stackTrace_marshaled = { 0 };
	____stackTrace_marshaled = il2cpp_codegen_marshal_string(___stackTrace);

	// Marshaling of parameter '___type' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____condition_marshaled, ____stackTrace_marshaled, ___type);

	// Marshaling cleanup of parameter '___condition' native representation
	il2cpp_codegen_marshal_free(____condition_marshaled);
	____condition_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace' native representation
	il2cpp_codegen_marshal_free(____stackTrace_marshaled);
	____stackTrace_marshaled = NULL;

	// Marshaling cleanup of parameter '___type' native representation

}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern TypeInfo* LogType_t6_9_il2cpp_TypeInfo_var;
extern "C" Object_t * LogCallback_BeginInvoke_m6_443 (LogCallback_t6_72 * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LogType_t6_9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(950);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition;
	__d_args[1] = ___stackTrace;
	__d_args[2] = Box(LogType_t6_9_il2cpp_TypeInfo_var, &___type);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C" void LogCallback_EndInvoke_m6_444 (LogCallback_t6_72 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Application::Quit()
extern "C" void Application_Quit_m6_445 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*Application_Quit_m6_445_ftn) ();
	static Application_Quit_m6_445_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_Quit_m6_445_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::Quit()");
	_il2cpp_icall_func();
}
// System.Int32 UnityEngine.Application::get_loadedLevel()
extern "C" int32_t Application_get_loadedLevel_m6_446 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Application_get_loadedLevel_m6_446_ftn) ();
	static Application_get_loadedLevel_m6_446_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_loadedLevel_m6_446_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_loadedLevel()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_loadedLevelName()
extern "C" String_t* Application_get_loadedLevelName_m6_447 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_loadedLevelName_m6_447_ftn) ();
	static Application_get_loadedLevelName_m6_447_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_loadedLevelName_m6_447_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_loadedLevelName()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::LoadLevel(System.Int32)
extern "C" void Application_LoadLevel_m6_448 (Object_t * __this /* static, unused */, int32_t ___index, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		Application_LoadLevelAsync_m6_450(NULL /*static, unused*/, (String_t*)NULL, L_0, 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Application::LoadLevel(System.String)
extern "C" void Application_LoadLevel_m6_449 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Application_LoadLevelAsync_m6_450(NULL /*static, unused*/, L_0, (-1), 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.Application::LoadLevelAsync(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C" AsyncOperation_t6_2 * Application_LoadLevelAsync_m6_450 (Object_t * __this /* static, unused */, String_t* ___monoLevelName, int32_t ___index, bool ___additive, bool ___mustCompleteNextFrame, const MethodInfo* method)
{
	typedef AsyncOperation_t6_2 * (*Application_LoadLevelAsync_m6_450_ftn) (String_t*, int32_t, bool, bool);
	static Application_LoadLevelAsync_m6_450_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_LoadLevelAsync_m6_450_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::LoadLevelAsync(System.String,System.Int32,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___monoLevelName, ___index, ___additive, ___mustCompleteNextFrame);
}
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C" bool Application_get_isPlaying_m6_451 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isPlaying_m6_451_ftn) ();
	static Application_get_isPlaying_m6_451_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isPlaying_m6_451_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isPlaying()");
	return _il2cpp_icall_func();
}
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C" int32_t Application_get_platform_m6_452 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Application_get_platform_m6_452_ftn) ();
	static Application_get_platform_m6_452_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_platform_m6_452_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_platform()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
extern "C" void Application_set_runInBackground_m6_453 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*Application_set_runInBackground_m6_453_ftn) (bool);
	static Application_set_runInBackground_m6_453_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_runInBackground_m6_453_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_runInBackground(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.Application::OpenURL(System.String)
extern "C" void Application_OpenURL_m6_454 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method)
{
	typedef void (*Application_OpenURL_m6_454_ftn) (String_t*);
	static Application_OpenURL_m6_454_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_OpenURL_m6_454_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::OpenURL(System.String)");
	_il2cpp_icall_func(___url);
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern TypeInfo* Application_t6_73_il2cpp_TypeInfo_var;
extern "C" void Application_CallLogCallback_m6_455 (Object_t * __this /* static, unused */, String_t* ___logString, String_t* ___stackTrace, int32_t ___type, bool ___invokedOnMainThread, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Application_t6_73_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(951);
		s_Il2CppMethodIntialized = true;
	}
	LogCallback_t6_72 * V_0 = {0};
	LogCallback_t6_72 * V_1 = {0};
	{
		bool L_0 = ___invokedOnMainThread;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t6_72 * L_1 = ((Application_t6_73_StaticFields*)Application_t6_73_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandler_0;
		V_0 = L_1;
		LogCallback_t6_72 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t6_72 * L_3 = V_0;
		String_t* L_4 = ___logString;
		String_t* L_5 = ___stackTrace;
		int32_t L_6 = ___type;
		NullCheck(L_3);
		LogCallback_Invoke_m6_442(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_001b:
	{
		LogCallback_t6_72 * L_7 = ((Application_t6_73_StaticFields*)Application_t6_73_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandlerThreaded_1;
		V_1 = L_7;
		LogCallback_t6_72 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0030;
		}
	}
	{
		LogCallback_t6_72 * L_9 = V_1;
		String_t* L_10 = ___logString;
		String_t* L_11 = ___stackTrace;
		int32_t L_12 = ___type;
		NullCheck(L_9);
		LogCallback_Invoke_m6_442(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void UnityEngine.Behaviour::.ctor()
extern "C" void Behaviour__ctor_m6_456 (Behaviour_t6_30 * __this, const MethodInfo* method)
{
	{
		Component__ctor_m6_581(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C" bool Behaviour_get_enabled_m6_457 (Behaviour_t6_30 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_enabled_m6_457_ftn) (Behaviour_t6_30 *);
	static Behaviour_get_enabled_m6_457_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_enabled_m6_457_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C" void Behaviour_set_enabled_m6_458 (Behaviour_t6_30 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Behaviour_set_enabled_m6_458_ftn) (Behaviour_t6_30 *, bool);
	static Behaviour_set_enabled_m6_458_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_set_enabled_m6_458_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern "C" void CameraCallback__ctor_m6_459 (CameraCallback_t6_74 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C" void CameraCallback_Invoke_m6_460 (CameraCallback_t6_74 * __this, Camera_t6_75 * ___cam, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		CameraCallback_Invoke_m6_460((CameraCallback_t6_74 *)__this->___prev_9,___cam, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Camera_t6_75 * ___cam, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___cam,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Camera_t6_75 * ___cam, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___cam,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___cam,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t6_74(Il2CppObject* delegate, Camera_t6_75 * ___cam)
{
	// Marshaling of parameter '___cam' to native representation
	Camera_t6_75 * ____cam_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.Camera'."));
}
// System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern "C" Object_t * CameraCallback_BeginInvoke_m6_461 (CameraCallback_t6_74 * __this, Camera_t6_75 * ___cam, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___cam;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern "C" void CameraCallback_EndInvoke_m6_462 (CameraCallback_t6_74 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C" float Camera_get_nearClipPlane_m6_463 (Camera_t6_75 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_nearClipPlane_m6_463_ftn) (Camera_t6_75 *);
	static Camera_get_nearClipPlane_m6_463_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m6_463_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C" float Camera_get_farClipPlane_m6_464 (Camera_t6_75 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_farClipPlane_m6_464_ftn) (Camera_t6_75 *);
	static Camera_get_farClipPlane_m6_464_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m6_464_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C" int32_t Camera_get_cullingMask_m6_465 (Camera_t6_75 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_cullingMask_m6_465_ftn) (Camera_t6_75 *);
	static Camera_get_cullingMask_m6_465_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m6_465_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C" int32_t Camera_get_eventMask_m6_466 (Camera_t6_75 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_eventMask_m6_466_ftn) (Camera_t6_75 *);
	static Camera_get_eventMask_m6_466_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m6_466_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C" Rect_t6_52  Camera_get_pixelRect_m6_467 (Camera_t6_75 * __this, const MethodInfo* method)
{
	Rect_t6_52  V_0 = {0};
	{
		Camera_INTERNAL_get_pixelRect_m6_468(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t6_52  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C" void Camera_INTERNAL_get_pixelRect_m6_468 (Camera_t6_75 * __this, Rect_t6_52 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m6_468_ftn) (Camera_t6_75 *, Rect_t6_52 *);
	static Camera_INTERNAL_get_pixelRect_m6_468_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m6_468_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C" RenderTexture_t6_34 * Camera_get_targetTexture_m6_469 (Camera_t6_75 * __this, const MethodInfo* method)
{
	typedef RenderTexture_t6_34 * (*Camera_get_targetTexture_m6_469_ftn) (Camera_t6_75 *);
	static Camera_get_targetTexture_m6_469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m6_469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C" int32_t Camera_get_clearFlags_m6_470 (Camera_t6_75 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_clearFlags_m6_470_ftn) (Camera_t6_75 *);
	static Camera_get_clearFlags_m6_470_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m6_470_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Ray UnityEngine.Camera::ViewportPointToRay(UnityEngine.Vector3)
extern "C" Ray_t6_56  Camera_ViewportPointToRay_m6_471 (Camera_t6_75 * __this, Vector3_t6_49  ___position, const MethodInfo* method)
{
	{
		Ray_t6_56  L_0 = Camera_INTERNAL_CALL_ViewportPointToRay_m6_472(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Ray UnityEngine.Camera::INTERNAL_CALL_ViewportPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Ray_t6_56  Camera_INTERNAL_CALL_ViewportPointToRay_m6_472 (Object_t * __this /* static, unused */, Camera_t6_75 * ___self, Vector3_t6_49 * ___position, const MethodInfo* method)
{
	typedef Ray_t6_56  (*Camera_INTERNAL_CALL_ViewportPointToRay_m6_472_ftn) (Camera_t6_75 *, Vector3_t6_49 *);
	static Camera_INTERNAL_CALL_ViewportPointToRay_m6_472_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ViewportPointToRay_m6_472_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ViewportPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C" Ray_t6_56  Camera_ScreenPointToRay_m6_473 (Camera_t6_75 * __this, Vector3_t6_49  ___position, const MethodInfo* method)
{
	{
		Ray_t6_56  L_0 = Camera_INTERNAL_CALL_ScreenPointToRay_m6_474(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Ray UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Ray_t6_56  Camera_INTERNAL_CALL_ScreenPointToRay_m6_474 (Object_t * __this /* static, unused */, Camera_t6_75 * ___self, Vector3_t6_49 * ___position, const MethodInfo* method)
{
	typedef Ray_t6_56  (*Camera_INTERNAL_CALL_ScreenPointToRay_m6_474_ftn) (Camera_t6_75 *, Vector3_t6_49 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m6_474_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m6_474_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" Camera_t6_75 * Camera_get_main_m6_475 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Camera_t6_75 * (*Camera_get_main_m6_475_ftn) ();
	static Camera_get_main_m6_475_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_main_m6_475_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_main()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C" int32_t Camera_get_allCamerasCount_m6_476 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m6_476_ftn) ();
	static Camera_get_allCamerasCount_m6_476_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m6_476_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C" int32_t Camera_GetAllCameras_m6_477 (Object_t * __this /* static, unused */, CameraU5BU5D_t6_224* ___cameras, const MethodInfo* method)
{
	typedef int32_t (*Camera_GetAllCameras_m6_477_ftn) (CameraU5BU5D_t6_224*);
	static Camera_GetAllCameras_m6_477_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m6_477_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	return _il2cpp_icall_func(___cameras);
}
// System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern TypeInfo* Camera_t6_75_il2cpp_TypeInfo_var;
extern "C" void Camera_FireOnPreCull_m6_478 (Object_t * __this /* static, unused */, Camera_t6_75 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t6_75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(952);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t6_74 * L_0 = ((Camera_t6_75_StaticFields*)Camera_t6_75_il2cpp_TypeInfo_var->static_fields)->___onPreCull_2;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t6_74 * L_1 = ((Camera_t6_75_StaticFields*)Camera_t6_75_il2cpp_TypeInfo_var->static_fields)->___onPreCull_2;
		Camera_t6_75 * L_2 = ___cam;
		NullCheck(L_1);
		CameraCallback_Invoke_m6_460(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern TypeInfo* Camera_t6_75_il2cpp_TypeInfo_var;
extern "C" void Camera_FireOnPreRender_m6_479 (Object_t * __this /* static, unused */, Camera_t6_75 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t6_75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(952);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t6_74 * L_0 = ((Camera_t6_75_StaticFields*)Camera_t6_75_il2cpp_TypeInfo_var->static_fields)->___onPreRender_3;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t6_74 * L_1 = ((Camera_t6_75_StaticFields*)Camera_t6_75_il2cpp_TypeInfo_var->static_fields)->___onPreRender_3;
		Camera_t6_75 * L_2 = ___cam;
		NullCheck(L_1);
		CameraCallback_Invoke_m6_460(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern TypeInfo* Camera_t6_75_il2cpp_TypeInfo_var;
extern "C" void Camera_FireOnPostRender_m6_480 (Object_t * __this /* static, unused */, Camera_t6_75 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t6_75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(952);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t6_74 * L_0 = ((Camera_t6_75_StaticFields*)Camera_t6_75_il2cpp_TypeInfo_var->static_fields)->___onPostRender_4;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t6_74 * L_1 = ((Camera_t6_75_StaticFields*)Camera_t6_75_il2cpp_TypeInfo_var->static_fields)->___onPostRender_4;
		Camera_t6_75 * L_2 = ___cam;
		NullCheck(L_1);
		CameraCallback_Invoke_m6_460(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C" GameObject_t6_85 * Camera_RaycastTry_m6_481 (Camera_t6_75 * __this, Ray_t6_56  ___ray, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		int32_t L_2 = V_0;
		GameObject_t6_85 * L_3 = Camera_INTERNAL_CALL_RaycastTry_m6_482(NULL /*static, unused*/, __this, (&___ray), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" GameObject_t6_85 * Camera_INTERNAL_CALL_RaycastTry_m6_482 (Object_t * __this /* static, unused */, Camera_t6_75 * ___self, Ray_t6_56 * ___ray, float ___distance, int32_t ___layerMask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	typedef GameObject_t6_85 * (*Camera_INTERNAL_CALL_RaycastTry_m6_482_ftn) (Camera_t6_75 *, Ray_t6_56 *, float, int32_t, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry_m6_482_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry_m6_482_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___self, ___ray, ___distance, ___layerMask, ___queryTriggerInteraction);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C" GameObject_t6_85 * Camera_RaycastTry2D_m6_483 (Camera_t6_75 * __this, Ray_t6_56  ___ray, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		GameObject_t6_85 * L_2 = Camera_INTERNAL_CALL_RaycastTry2D_m6_484(NULL /*static, unused*/, __this, (&___ray), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C" GameObject_t6_85 * Camera_INTERNAL_CALL_RaycastTry2D_m6_484 (Object_t * __this /* static, unused */, Camera_t6_75 * ___self, Ray_t6_56 * ___ray, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	typedef GameObject_t6_85 * (*Camera_INTERNAL_CALL_RaycastTry2D_m6_484_ftn) (Camera_t6_75 *, Ray_t6_56 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry2D_m6_484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry2D_m6_484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___self, ___ray, ___distance, ___layerMask);
}
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single)
extern "C" void Debug_DrawLine_m6_485 (Object_t * __this /* static, unused */, Vector3_t6_49  ___start, Vector3_t6_49  ___end, Color_t6_40  ___color, float ___duration, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 1;
		float L_0 = ___duration;
		bool L_1 = V_0;
		Debug_INTERNAL_CALL_DrawLine_m6_487(NULL /*static, unused*/, (&___start), (&___end), (&___color), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Debug_DrawLine_m6_486 (Object_t * __this /* static, unused */, Vector3_t6_49  ___start, Vector3_t6_49  ___end, const MethodInfo* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	Color_t6_40  V_2 = {0};
	{
		V_0 = 1;
		V_1 = (0.0f);
		Color_t6_40  L_0 = Color_get_white_m6_236(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_0;
		float L_1 = V_1;
		bool L_2 = V_0;
		Debug_INTERNAL_CALL_DrawLine_m6_487(NULL /*static, unused*/, (&___start), (&___end), (&V_2), L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)
extern "C" void Debug_INTERNAL_CALL_DrawLine_m6_487 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___start, Vector3_t6_49 * ___end, Color_t6_40 * ___color, float ___duration, bool ___depthTest, const MethodInfo* method)
{
	typedef void (*Debug_INTERNAL_CALL_DrawLine_m6_487_ftn) (Vector3_t6_49 *, Vector3_t6_49 *, Color_t6_40 *, float, bool);
	static Debug_INTERNAL_CALL_DrawLine_m6_487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_INTERNAL_CALL_DrawLine_m6_487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)");
	_il2cpp_icall_func(___start, ___end, ___color, ___duration, ___depthTest);
}
// System.Void UnityEngine.Debug::Internal_Log(System.Int32,System.String,UnityEngine.Object)
extern "C" void Debug_Internal_Log_m6_488 (Object_t * __this /* static, unused */, int32_t ___level, String_t* ___msg, Object_t6_5 * ___obj, const MethodInfo* method)
{
	typedef void (*Debug_Internal_Log_m6_488_ftn) (int32_t, String_t*, Object_t6_5 *);
	static Debug_Internal_Log_m6_488_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_Internal_Log_m6_488_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::Internal_Log(System.Int32,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level, ___msg, ___obj);
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern Il2CppCodeGenString* _stringLiteral2706;
extern "C" void Debug_Log_m6_489 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2706 = il2cpp_codegen_string_literal_from_index(2706);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	{
		Object_t * L_0 = ___message;
		G_B1_0 = 0;
		if (!L_0)
		{
			G_B2_0 = 0;
			goto IL_0012;
		}
	}
	{
		Object_t * L_1 = ___message;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = _stringLiteral2706;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		Debug_Internal_Log_m6_488(NULL /*static, unused*/, G_B3_1, G_B3_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern Il2CppCodeGenString* _stringLiteral2706;
extern "C" void Debug_LogError_m6_490 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2706 = il2cpp_codegen_string_literal_from_index(2706);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	{
		Object_t * L_0 = ___message;
		G_B1_0 = 2;
		if (!L_0)
		{
			G_B2_0 = 2;
			goto IL_0012;
		}
	}
	{
		Object_t * L_1 = ___message;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = _stringLiteral2706;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		Debug_Internal_Log_m6_488(NULL /*static, unused*/, G_B3_1, G_B3_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" void Debug_LogWarning_m6_491 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Debug_Internal_Log_m6_488(NULL /*static, unused*/, 1, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C" void Debug_LogWarning_m6_492 (Object_t * __this /* static, unused */, Object_t * ___message, Object_t6_5 * ___context, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Object_t6_5 * L_2 = ___context;
		Debug_Internal_Log_m6_488(NULL /*static, unused*/, 1, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void DisplaysUpdatedDelegate__ctor_m6_493 (DisplaysUpdatedDelegate_t6_77 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C" void DisplaysUpdatedDelegate_Invoke_m6_494 (DisplaysUpdatedDelegate_t6_77 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m6_494((DisplaysUpdatedDelegate_t6_77 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t6_77(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DisplaysUpdatedDelegate_BeginInvoke_m6_495 (DisplaysUpdatedDelegate_t6_77 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void DisplaysUpdatedDelegate_EndInvoke_m6_496 (DisplaysUpdatedDelegate_t6_77 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Display::.ctor()
extern "C" void Display__ctor_m6_497 (Display_t6_78 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = {0};
		IntPtr__ctor_m1_628(&L_0, 0, /*hidden argument*/NULL);
		__this->___nativeDisplay_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C" void Display__ctor_m6_498 (Display_t6_78 * __this, IntPtr_t ___nativeDisplay, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___nativeDisplay;
		__this->___nativeDisplay_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Display::.cctor()
extern TypeInfo* DisplayU5BU5D_t6_79_il2cpp_TypeInfo_var;
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" void Display__cctor_m6_499 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisplayU5BU5D_t6_79_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(953);
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	{
		DisplayU5BU5D_t6_79* L_0 = ((DisplayU5BU5D_t6_79*)SZArrayNew(DisplayU5BU5D_t6_79_il2cpp_TypeInfo_var, 1));
		Display_t6_78 * L_1 = (Display_t6_78 *)il2cpp_codegen_object_new (Display_t6_78_il2cpp_TypeInfo_var);
		Display__ctor_m6_497(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Display_t6_78 **)(Display_t6_78 **)SZArrayLdElema(L_0, 0, sizeof(Display_t6_78 *))) = (Display_t6_78 *)L_1;
		((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->___displays_1 = L_0;
		DisplayU5BU5D_t6_79* L_2 = ((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2 = (*(Display_t6_78 **)(Display_t6_78 **)SZArrayLdElema(L_2, L_3, sizeof(Display_t6_78 *)));
		((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = (DisplaysUpdatedDelegate_t6_77 *)NULL;
		return;
	}
}
// System.Void UnityEngine.Display::add_onDisplaysUpdated(UnityEngine.Display/DisplaysUpdatedDelegate)
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern TypeInfo* DisplaysUpdatedDelegate_t6_77_il2cpp_TypeInfo_var;
extern "C" void Display_add_onDisplaysUpdated_m6_500 (Object_t * __this /* static, unused */, DisplaysUpdatedDelegate_t6_77 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		DisplaysUpdatedDelegate_t6_77_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(955);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t6_77 * L_0 = ((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		DisplaysUpdatedDelegate_t6_77 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_695(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = ((DisplaysUpdatedDelegate_t6_77 *)CastclassSealed(L_2, DisplaysUpdatedDelegate_t6_77_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Display::remove_onDisplaysUpdated(UnityEngine.Display/DisplaysUpdatedDelegate)
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern TypeInfo* DisplaysUpdatedDelegate_t6_77_il2cpp_TypeInfo_var;
extern "C" void Display_remove_onDisplaysUpdated_m6_501 (Object_t * __this /* static, unused */, DisplaysUpdatedDelegate_t6_77 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		DisplaysUpdatedDelegate_t6_77_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(955);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t6_77 * L_0 = ((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		DisplaysUpdatedDelegate_t6_77 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_698(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = ((DisplaysUpdatedDelegate_t6_77 *)CastclassSealed(L_2, DisplaysUpdatedDelegate_t6_77_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Int32 UnityEngine.Display::get_renderingWidth()
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_renderingWidth_m6_502 (Display_t6_78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m6_518(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_renderingHeight()
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_renderingHeight_m6_503 (Display_t6_78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m6_518(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_systemWidth()
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_systemWidth_m6_504 (Display_t6_78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m6_517(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_systemHeight()
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_systemHeight_m6_505 (Display_t6_78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m6_517(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.Display::get_colorBuffer()
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" RenderBuffer_t6_208  Display_get_colorBuffer_m6_506 (Display_t6_78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	RenderBuffer_t6_208  V_0 = {0};
	RenderBuffer_t6_208  V_1 = {0};
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		Display_GetRenderingBuffersImpl_m6_519(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		RenderBuffer_t6_208  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.Display::get_depthBuffer()
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" RenderBuffer_t6_208  Display_get_depthBuffer_m6_507 (Display_t6_78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	RenderBuffer_t6_208  V_0 = {0};
	RenderBuffer_t6_208  V_1 = {0};
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		Display_GetRenderingBuffersImpl_m6_519(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		RenderBuffer_t6_208  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Display::Activate()
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" void Display_Activate_m6_508 (Display_t6_78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		Display_ActivateDisplayImpl_m6_521(NULL /*static, unused*/, L_0, 0, 0, ((int32_t)60), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::Activate(System.Int32,System.Int32,System.Int32)
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" void Display_Activate_m6_509 (Display_t6_78 * __this, int32_t ___width, int32_t ___height, int32_t ___refreshRate, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___width;
		int32_t L_2 = ___height;
		int32_t L_3 = ___refreshRate;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		Display_ActivateDisplayImpl_m6_521(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::SetParams(System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" void Display_SetParams_m6_510 (Display_t6_78 * __this, int32_t ___width, int32_t ___height, int32_t ___x, int32_t ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___width;
		int32_t L_2 = ___height;
		int32_t L_3 = ___x;
		int32_t L_4 = ___y;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		Display_SetParamsImpl_m6_522(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::SetRenderingResolution(System.Int32,System.Int32)
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" void Display_SetRenderingResolution_m6_511 (Display_t6_78 * __this, int32_t ___w, int32_t ___h, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___w;
		int32_t L_2 = ___h;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		Display_SetRenderingResolutionImpl_m6_520(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Display::MultiDisplayLicense()
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" bool Display_MultiDisplayLicense_m6_512 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		bool L_0 = Display_MultiDisplayLicenseImpl_m6_523(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Display::RelativeMouseAt(UnityEngine.Vector3)
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" Vector3_t6_49  Display_RelativeMouseAt_m6_513 (Object_t * __this /* static, unused */, Vector3_t6_49  ___inputMouseCoordinates, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_49  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_1 = 0;
		V_2 = 0;
		float L_0 = ((&___inputMouseCoordinates)->___x_1);
		V_3 = (((int32_t)((int32_t)L_0)));
		float L_1 = ((&___inputMouseCoordinates)->___y_2);
		V_4 = (((int32_t)((int32_t)L_1)));
		int32_t L_2 = V_3;
		int32_t L_3 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		int32_t L_4 = Display_RelativeMouseAtImpl_m6_524(NULL /*static, unused*/, L_2, L_3, (&V_1), (&V_2), /*hidden argument*/NULL);
		(&V_0)->___z_3 = (((float)((float)L_4)));
		int32_t L_5 = V_1;
		(&V_0)->___x_1 = (((float)((float)L_5)));
		int32_t L_6 = V_2;
		(&V_0)->___y_2 = (((float)((float)L_6)));
		Vector3_t6_49  L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Display UnityEngine.Display::get_main()
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" Display_t6_78 * Display_get_main_m6_514 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		Display_t6_78 * L_0 = ((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2;
		return L_0;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern TypeInfo* DisplayU5BU5D_t6_79_il2cpp_TypeInfo_var;
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" void Display_RecreateDisplayList_m6_515 (Object_t * __this /* static, unused */, IntPtrU5BU5D_t1_34* ___nativeDisplay, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisplayU5BU5D_t6_79_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(953);
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t1_34* L_0 = ___nativeDisplay;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->___displays_1 = ((DisplayU5BU5D_t6_79*)SZArrayNew(DisplayU5BU5D_t6_79_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))));
		V_0 = 0;
		goto IL_0027;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t6_79* L_1 = ((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t1_34* L_3 = ___nativeDisplay;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Display_t6_78 * L_6 = (Display_t6_78 *)il2cpp_codegen_object_new (Display_t6_78_il2cpp_TypeInfo_var);
		Display__ctor_m6_498(L_6, (*(IntPtr_t*)(IntPtr_t*)SZArrayLdElema(L_3, L_5, sizeof(IntPtr_t))), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		ArrayElementTypeCheck (L_1, L_6);
		*((Display_t6_78 **)(Display_t6_78 **)SZArrayLdElema(L_1, L_2, sizeof(Display_t6_78 *))) = (Display_t6_78 *)L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_8 = V_0;
		IntPtrU5BU5D_t1_34* L_9 = ___nativeDisplay;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t6_79* L_10 = ((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2 = (*(Display_t6_78 **)(Display_t6_78 **)SZArrayLdElema(L_10, L_11, sizeof(Display_t6_78 *)));
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern TypeInfo* Display_t6_78_il2cpp_TypeInfo_var;
extern "C" void Display_FireDisplaysUpdated_m6_516 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t6_77 * L_0 = ((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_78_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t6_77 * L_1 = ((Display_t6_78_StaticFields*)Display_t6_78_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		NullCheck(L_1);
		DisplaysUpdatedDelegate_Invoke_m6_494(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C" void Display_GetSystemExtImpl_m6_517 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t* ___w, int32_t* ___h, const MethodInfo* method)
{
	typedef void (*Display_GetSystemExtImpl_m6_517_ftn) (IntPtr_t, int32_t*, int32_t*);
	static Display_GetSystemExtImpl_m6_517_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetSystemExtImpl_m6_517_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C" void Display_GetRenderingExtImpl_m6_518 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t* ___w, int32_t* ___h, const MethodInfo* method)
{
	typedef void (*Display_GetRenderingExtImpl_m6_518_ftn) (IntPtr_t, int32_t*, int32_t*);
	static Display_GetRenderingExtImpl_m6_518_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingExtImpl_m6_518_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)
extern "C" void Display_GetRenderingBuffersImpl_m6_519 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, RenderBuffer_t6_208 * ___color, RenderBuffer_t6_208 * ___depth, const MethodInfo* method)
{
	typedef void (*Display_GetRenderingBuffersImpl_m6_519_ftn) (IntPtr_t, RenderBuffer_t6_208 *, RenderBuffer_t6_208 *);
	static Display_GetRenderingBuffersImpl_m6_519_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingBuffersImpl_m6_519_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(___nativeDisplay, ___color, ___depth);
}
// System.Void UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)
extern "C" void Display_SetRenderingResolutionImpl_m6_520 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___w, int32_t ___h, const MethodInfo* method)
{
	typedef void (*Display_SetRenderingResolutionImpl_m6_520_ftn) (IntPtr_t, int32_t, int32_t);
	static Display_SetRenderingResolutionImpl_m6_520_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_SetRenderingResolutionImpl_m6_520_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::ActivateDisplayImpl(System.IntPtr,System.Int32,System.Int32,System.Int32)
extern "C" void Display_ActivateDisplayImpl_m6_521 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___width, int32_t ___height, int32_t ___refreshRate, const MethodInfo* method)
{
	typedef void (*Display_ActivateDisplayImpl_m6_521_ftn) (IntPtr_t, int32_t, int32_t, int32_t);
	static Display_ActivateDisplayImpl_m6_521_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_ActivateDisplayImpl_m6_521_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::ActivateDisplayImpl(System.IntPtr,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___width, ___height, ___refreshRate);
}
// System.Void UnityEngine.Display::SetParamsImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void Display_SetParamsImpl_m6_522 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___width, int32_t ___height, int32_t ___x, int32_t ___y, const MethodInfo* method)
{
	typedef void (*Display_SetParamsImpl_m6_522_ftn) (IntPtr_t, int32_t, int32_t, int32_t, int32_t);
	static Display_SetParamsImpl_m6_522_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_SetParamsImpl_m6_522_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::SetParamsImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___width, ___height, ___x, ___y);
}
// System.Boolean UnityEngine.Display::MultiDisplayLicenseImpl()
extern "C" bool Display_MultiDisplayLicenseImpl_m6_523 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Display_MultiDisplayLicenseImpl_m6_523_ftn) ();
	static Display_MultiDisplayLicenseImpl_m6_523_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_MultiDisplayLicenseImpl_m6_523_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::MultiDisplayLicenseImpl()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern "C" int32_t Display_RelativeMouseAtImpl_m6_524 (Object_t * __this /* static, unused */, int32_t ___x, int32_t ___y, int32_t* ___rx, int32_t* ___ry, const MethodInfo* method)
{
	typedef int32_t (*Display_RelativeMouseAtImpl_m6_524_ftn) (int32_t, int32_t, int32_t*, int32_t*);
	static Display_RelativeMouseAtImpl_m6_524_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_RelativeMouseAtImpl_m6_524_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)");
	return _il2cpp_icall_func(___x, ___y, ___rx, ___ry);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" void MonoBehaviour__ctor_m6_525 (MonoBehaviour_t6_80 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m6_456(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()
extern "C" void MonoBehaviour_Internal_CancelInvokeAll_m6_526 (MonoBehaviour_t6_80 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Internal_CancelInvokeAll_m6_526_ftn) (MonoBehaviour_t6_80 *);
	static MonoBehaviour_Internal_CancelInvokeAll_m6_526_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_CancelInvokeAll_m6_526_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C" void MonoBehaviour_Invoke_m6_527 (MonoBehaviour_t6_80 * __this, String_t* ___methodName, float ___time, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Invoke_m6_527_ftn) (MonoBehaviour_t6_80 *, String_t*, float);
	static MonoBehaviour_Invoke_m6_527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Invoke_m6_527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___methodName, ___time);
}
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C" void MonoBehaviour_InvokeRepeating_m6_528 (MonoBehaviour_t6_80 * __this, String_t* ___methodName, float ___time, float ___repeatRate, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_InvokeRepeating_m6_528_ftn) (MonoBehaviour_t6_80 *, String_t*, float, float);
	static MonoBehaviour_InvokeRepeating_m6_528_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_InvokeRepeating_m6_528_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___methodName, ___time, ___repeatRate);
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke()
extern "C" void MonoBehaviour_CancelInvoke_m6_529 (MonoBehaviour_t6_80 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_Internal_CancelInvokeAll_m6_526(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" Coroutine_t6_15 * MonoBehaviour_StartCoroutine_m6_530 (MonoBehaviour_t6_80 * __this, Object_t * ___routine, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___routine;
		Coroutine_t6_15 * L_1 = MonoBehaviour_StartCoroutine_Auto_m6_531(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C" Coroutine_t6_15 * MonoBehaviour_StartCoroutine_Auto_m6_531 (MonoBehaviour_t6_80 * __this, Object_t * ___routine, const MethodInfo* method)
{
	typedef Coroutine_t6_15 * (*MonoBehaviour_StartCoroutine_Auto_m6_531_ftn) (MonoBehaviour_t6_80 *, Object_t *);
	static MonoBehaviour_StartCoroutine_Auto_m6_531_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_m6_531_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine);
}
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C" Vector2_t6_48  Touch_get_position_m6_532 (Touch_t6_82 * __this, const MethodInfo* method)
{
	{
		Vector2_t6_48  L_0 = (__this->___m_Position_1);
		return L_0;
	}
}
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C" int32_t Touch_get_phase_m6_533 (Touch_t6_82 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Phase_6);
		return L_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Touch
extern "C" void Touch_t6_82_marshal(const Touch_t6_82& unmarshaled, Touch_t6_82_marshaled& marshaled)
{
	marshaled.___m_FingerId_0 = unmarshaled.___m_FingerId_0;
	marshaled.___m_Position_1 = unmarshaled.___m_Position_1;
	marshaled.___m_RawPosition_2 = unmarshaled.___m_RawPosition_2;
	marshaled.___m_PositionDelta_3 = unmarshaled.___m_PositionDelta_3;
	marshaled.___m_TimeDelta_4 = unmarshaled.___m_TimeDelta_4;
	marshaled.___m_TapCount_5 = unmarshaled.___m_TapCount_5;
	marshaled.___m_Phase_6 = unmarshaled.___m_Phase_6;
	marshaled.___m_Pressure_7 = unmarshaled.___m_Pressure_7;
	marshaled.___m_maximumPossiblePressure_8 = unmarshaled.___m_maximumPossiblePressure_8;
}
extern "C" void Touch_t6_82_marshal_back(const Touch_t6_82_marshaled& marshaled, Touch_t6_82& unmarshaled)
{
	unmarshaled.___m_FingerId_0 = marshaled.___m_FingerId_0;
	unmarshaled.___m_Position_1 = marshaled.___m_Position_1;
	unmarshaled.___m_RawPosition_2 = marshaled.___m_RawPosition_2;
	unmarshaled.___m_PositionDelta_3 = marshaled.___m_PositionDelta_3;
	unmarshaled.___m_TimeDelta_4 = marshaled.___m_TimeDelta_4;
	unmarshaled.___m_TapCount_5 = marshaled.___m_TapCount_5;
	unmarshaled.___m_Phase_6 = marshaled.___m_Phase_6;
	unmarshaled.___m_Pressure_7 = marshaled.___m_Pressure_7;
	unmarshaled.___m_maximumPossiblePressure_8 = marshaled.___m_maximumPossiblePressure_8;
}
// Conversion method for clean up from marshalling of: UnityEngine.Touch
extern "C" void Touch_t6_82_marshal_cleanup(Touch_t6_82_marshaled& marshaled)
{
}
// System.Void UnityEngine.Input::.cctor()
extern "C" void Input__cctor_m6_534 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.Input::GetKeyInt(System.Int32)
extern "C" bool Input_GetKeyInt_m6_535 (Object_t * __this /* static, unused */, int32_t ___key, const MethodInfo* method)
{
	typedef bool (*Input_GetKeyInt_m6_535_ftn) (int32_t);
	static Input_GetKeyInt_m6_535_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyInt_m6_535_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyInt(System.Int32)");
	return _il2cpp_icall_func(___key);
}
// System.Boolean UnityEngine.Input::GetKeyDownInt(System.Int32)
extern "C" bool Input_GetKeyDownInt_m6_536 (Object_t * __this /* static, unused */, int32_t ___key, const MethodInfo* method)
{
	typedef bool (*Input_GetKeyDownInt_m6_536_ftn) (int32_t);
	static Input_GetKeyDownInt_m6_536_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyDownInt_m6_536_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyDownInt(System.Int32)");
	return _il2cpp_icall_func(___key);
}
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C" float Input_GetAxis_m6_537 (Object_t * __this /* static, unused */, String_t* ___axisName, const MethodInfo* method)
{
	typedef float (*Input_GetAxis_m6_537_ftn) (String_t*);
	static Input_GetAxis_m6_537_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetAxis_m6_537_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetAxis(System.String)");
	return _il2cpp_icall_func(___axisName);
}
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C" float Input_GetAxisRaw_m6_538 (Object_t * __this /* static, unused */, String_t* ___axisName, const MethodInfo* method)
{
	typedef float (*Input_GetAxisRaw_m6_538_ftn) (String_t*);
	static Input_GetAxisRaw_m6_538_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetAxisRaw_m6_538_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetAxisRaw(System.String)");
	return _il2cpp_icall_func(___axisName);
}
// System.Boolean UnityEngine.Input::GetButton(System.String)
extern "C" bool Input_GetButton_m6_539 (Object_t * __this /* static, unused */, String_t* ___buttonName, const MethodInfo* method)
{
	typedef bool (*Input_GetButton_m6_539_ftn) (String_t*);
	static Input_GetButton_m6_539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetButton_m6_539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetButton(System.String)");
	return _il2cpp_icall_func(___buttonName);
}
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C" bool Input_GetButtonDown_m6_540 (Object_t * __this /* static, unused */, String_t* ___buttonName, const MethodInfo* method)
{
	typedef bool (*Input_GetButtonDown_m6_540_ftn) (String_t*);
	static Input_GetButtonDown_m6_540_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetButtonDown_m6_540_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetButtonDown(System.String)");
	return _il2cpp_icall_func(___buttonName);
}
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern "C" bool Input_GetKey_m6_541 (Object_t * __this /* static, unused */, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyInt_m6_535(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern "C" bool Input_GetKeyDown_m6_542 (Object_t * __this /* static, unused */, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDownInt_m6_536(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C" bool Input_GetMouseButton_m6_543 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButton_m6_543_ftn) (int32_t);
	static Input_GetMouseButton_m6_543_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButton_m6_543_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButton(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C" bool Input_GetMouseButtonDown_m6_544 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonDown_m6_544_ftn) (int32_t);
	static Input_GetMouseButtonDown_m6_544_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonDown_m6_544_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonDown(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C" bool Input_GetMouseButtonUp_m6_545 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonUp_m6_545_ftn) (int32_t);
	static Input_GetMouseButtonUp_m6_545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonUp_m6_545_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonUp(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern "C" Vector3_t6_49  Input_get_mousePosition_m6_546 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_49  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mousePosition_m6_547(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t6_49  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C" void Input_INTERNAL_get_mousePosition_m6_547 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_get_mousePosition_m6_547_ftn) (Vector3_t6_49 *);
	static Input_INTERNAL_get_mousePosition_m6_547_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mousePosition_m6_547_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C" Touch_t6_82  Input_GetTouch_m6_548 (Object_t * __this /* static, unused */, int32_t ___index, const MethodInfo* method)
{
	typedef Touch_t6_82  (*Input_GetTouch_m6_548_ftn) (int32_t);
	static Input_GetTouch_m6_548_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetTouch_m6_548_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetTouch(System.Int32)");
	return _il2cpp_icall_func(___index);
}
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C" int32_t Input_get_touchCount_m6_549 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Input_get_touchCount_m6_549_ftn) ();
	static Input_get_touchCount_m6_549_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchCount_m6_549_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchCount()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Input::get_compositionString()
extern "C" String_t* Input_get_compositionString_m6_550 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Input_get_compositionString_m6_550_ftn) ();
	static Input_get_compositionString_m6_550_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_compositionString_m6_550_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_compositionString()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Input::set_compositionCursorPos(UnityEngine.Vector2)
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern "C" void Input_set_compositionCursorPos_m6_551 (Object_t * __this /* static, unused */, Vector2_t6_48  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		Input_INTERNAL_set_compositionCursorPos_m6_552(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
extern "C" void Input_INTERNAL_set_compositionCursorPos_m6_552 (Object_t * __this /* static, unused */, Vector2_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_set_compositionCursorPos_m6_552_ftn) (Vector2_t6_48 *);
	static Input_INTERNAL_set_compositionCursorPos_m6_552_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_set_compositionCursorPos_m6_552_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.Object::.ctor()
extern "C" void Object__ctor_m6_553 (Object_t6_5 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C" Object_t6_5 * Object_Internal_CloneSingle_m6_554 (Object_t * __this /* static, unused */, Object_t6_5 * ___data, const MethodInfo* method)
{
	typedef Object_t6_5 * (*Object_Internal_CloneSingle_m6_554_ftn) (Object_t6_5 *);
	static Object_Internal_CloneSingle_m6_554_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m6_554_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" Object_t6_5 * Object_Internal_InstantiateSingle_m6_555 (Object_t * __this /* static, unused */, Object_t6_5 * ___data, Vector3_t6_49  ___pos, Quaternion_t6_51  ___rot, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = ___data;
		Object_t6_5 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m6_556(NULL /*static, unused*/, L_0, (&___pos), (&___rot), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C" Object_t6_5 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m6_556 (Object_t * __this /* static, unused */, Object_t6_5 * ___data, Vector3_t6_49 * ___pos, Quaternion_t6_51 * ___rot, const MethodInfo* method)
{
	typedef Object_t6_5 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m6_556_ftn) (Object_t6_5 *, Vector3_t6_49 *, Quaternion_t6_51 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m6_556_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m6_556_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data, ___pos, ___rot);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C" void Object_Destroy_m6_557 (Object_t * __this /* static, unused */, Object_t6_5 * ___obj, float ___t, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m6_557_ftn) (Object_t6_5 *, float);
	static Object_Destroy_m6_557_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m6_557_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj, ___t);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" void Object_Destroy_m6_558 (Object_t * __this /* static, unused */, Object_t6_5 * ___obj, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t6_5 * L_0 = ___obj;
		float L_1 = V_0;
		Object_Destroy_m6_557(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C" void Object_DestroyImmediate_m6_559 (Object_t * __this /* static, unused */, Object_t6_5 * ___obj, bool ___allowDestroyingAssets, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m6_559_ftn) (Object_t6_5 *, bool);
	static Object_DestroyImmediate_m6_559_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m6_559_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj, ___allowDestroyingAssets);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C" void Object_DestroyImmediate_m6_560 (Object_t * __this /* static, unused */, Object_t6_5 * ___obj, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 0;
		Object_t6_5 * L_0 = ___obj;
		bool L_1 = V_0;
		Object_DestroyImmediate_m6_559(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C" ObjectU5BU5D_t6_252* Object_FindObjectsOfType_m6_561 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t6_252* (*Object_FindObjectsOfType_m6_561_ftn) (Type_t *);
	static Object_FindObjectsOfType_m6_561_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m6_561_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type);
}
// System.String UnityEngine.Object::get_name()
extern "C" String_t* Object_get_name_m6_562 (Object_t6_5 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m6_562_ftn) (Object_t6_5 *);
	static Object_get_name_m6_562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m6_562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" void Object_set_name_m6_563 (Object_t6_5 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*Object_set_name_m6_563_ftn) (Object_t6_5 *, String_t*);
	static Object_set_name_m6_563_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m6_563_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C" void Object_DontDestroyOnLoad_m6_564 (Object_t * __this /* static, unused */, Object_t6_5 * ___target, const MethodInfo* method)
{
	typedef void (*Object_DontDestroyOnLoad_m6_564_ftn) (Object_t6_5 *);
	static Object_DontDestroyOnLoad_m6_564_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m6_564_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C" void Object_set_hideFlags_m6_565 (Object_t6_5 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m6_565_ftn) (Object_t6_5 *, int32_t);
	static Object_set_hideFlags_m6_565_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m6_565_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C" void Object_DestroyObject_m6_566 (Object_t * __this /* static, unused */, Object_t6_5 * ___obj, float ___t, const MethodInfo* method)
{
	typedef void (*Object_DestroyObject_m6_566_ftn) (Object_t6_5 *, float);
	static Object_DestroyObject_m6_566_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyObject_m6_566_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj, ___t);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object)
extern "C" void Object_DestroyObject_m6_567 (Object_t * __this /* static, unused */, Object_t6_5 * ___obj, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t6_5 * L_0 = ___obj;
		float L_1 = V_0;
		Object_DestroyObject_m6_566(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Object::ToString()
extern "C" String_t* Object_ToString_m6_568 (Object_t6_5 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m6_568_ftn) (Object_t6_5 *);
	static Object_ToString_m6_568_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m6_568_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern TypeInfo* Object_t6_5_il2cpp_TypeInfo_var;
extern "C" bool Object_Equals_m6_569 (Object_t6_5 * __this, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t6_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(957);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___o;
		bool L_1 = Object_CompareBaseObjects_m6_571(NULL /*static, unused*/, __this, ((Object_t6_5 *)IsInstClass(L_0, Object_t6_5_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C" int32_t Object_GetHashCode_m6_570 (Object_t6_5 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetInstanceID_m6_573(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_CompareBaseObjects_m6_571 (Object_t * __this /* static, unused */, Object_t6_5 * ___lhs, Object_t6_5 * ___rhs, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		Object_t6_5 * L_0 = ___lhs;
		V_0 = ((((Object_t*)(Object_t6_5 *)L_0) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
		Object_t6_5 * L_1 = ___rhs;
		V_1 = ((((Object_t*)(Object_t6_5 *)L_1) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return 1;
	}

IL_0018:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		Object_t6_5 * L_5 = ___lhs;
		bool L_6 = Object_IsNativeObjectAlive_m6_572(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return ((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
	}

IL_0028:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		Object_t6_5 * L_8 = ___rhs;
		bool L_9 = Object_IsNativeObjectAlive_m6_572(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return ((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
	}

IL_0038:
	{
		Object_t6_5 * L_10 = ___lhs;
		NullCheck(L_10);
		int32_t L_11 = (L_10->___m_InstanceID_0);
		Object_t6_5 * L_12 = ___rhs;
		NullCheck(L_12);
		int32_t L_13 = (L_12->___m_InstanceID_0);
		return ((((int32_t)L_11) == ((int32_t)L_13))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" bool Object_IsNativeObjectAlive_m6_572 (Object_t * __this /* static, unused */, Object_t6_5 * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t6_5 * L_0 = ___o;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m6_574(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_3 = IntPtr_op_Inequality_m1_641(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C" int32_t Object_GetInstanceID_m6_573 (Object_t6_5 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_InstanceID_0);
		return L_0;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C" IntPtr_t Object_GetCachedPtr_m6_574 (Object_t6_5 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___m_CachedPtr_1);
		return L_0;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppCodeGenString* _stringLiteral2707;
extern "C" Object_t6_5 * Object_Instantiate_m6_575 (Object_t * __this /* static, unused */, Object_t6_5 * ___original, Vector3_t6_49  ___position, Quaternion_t6_51  ___rotation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2707 = il2cpp_codegen_string_literal_from_index(2707);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t6_5 * L_0 = ___original;
		Object_CheckNullArgument_m6_576(NULL /*static, unused*/, L_0, _stringLiteral2707, /*hidden argument*/NULL);
		Object_t6_5 * L_1 = ___original;
		Vector3_t6_49  L_2 = ___position;
		Quaternion_t6_51  L_3 = ___rotation;
		Object_t6_5 * L_4 = Object_Internal_InstantiateSingle_m6_555(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern TypeInfo* ArgumentException_t1_646_il2cpp_TypeInfo_var;
extern "C" void Object_CheckNullArgument_m6_576 (Object_t * __this /* static, unused */, Object_t * ___arg, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___arg;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___message;
		ArgumentException_t1_646 * L_2 = (ArgumentException_t1_646 *)il2cpp_codegen_object_new (ArgumentException_t1_646_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_4605(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_000d:
	{
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C" Object_t6_5 * Object_FindObjectOfType_m6_577 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	ObjectU5BU5D_t6_252* V_0 = {0};
	{
		Type_t * L_0 = ___type;
		ObjectU5BU5D_t6_252* L_1 = Object_FindObjectsOfType_m6_561(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t6_252* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		ObjectU5BU5D_t6_252* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		return (*(Object_t6_5 **)(Object_t6_5 **)SZArrayLdElema(L_3, L_4, sizeof(Object_t6_5 *)));
	}

IL_0014:
	{
		return (Object_t6_5 *)NULL;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" bool Object_op_Implicit_m6_578 (Object_t * __this /* static, unused */, Object_t6_5 * ___exists, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = ___exists;
		bool L_1 = Object_CompareBaseObjects_m6_571(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_op_Equality_m6_579 (Object_t * __this /* static, unused */, Object_t6_5 * ___x, Object_t6_5 * ___y, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = ___x;
		Object_t6_5 * L_1 = ___y;
		bool L_2 = Object_CompareBaseObjects_m6_571(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_op_Inequality_m6_580 (Object_t * __this /* static, unused */, Object_t6_5 * ___x, Object_t6_5 * ___y, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = ___x;
		Object_t6_5 * L_1 = ___y;
		bool L_2 = Object_CompareBaseObjects_m6_571(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t6_5_marshal(const Object_t6_5& unmarshaled, Object_t6_5_marshaled& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.___m_InstanceID_0;
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.___m_CachedPtr_1).___m_value_0);
}
extern "C" void Object_t6_5_marshal_back(const Object_t6_5_marshaled& marshaled, Object_t6_5& unmarshaled)
{
	unmarshaled.___m_InstanceID_0 = marshaled.___m_InstanceID_0;
	(unmarshaled.___m_CachedPtr_1).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_CachedPtr_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t6_5_marshal_cleanup(Object_t6_5_marshaled& marshaled)
{
}
// System.Void UnityEngine.Component::.ctor()
extern "C" void Component__ctor_m6_581 (Component_t6_26 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m6_553(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" Transform_t6_61 * Component_get_transform_m6_582 (Component_t6_26 * __this, const MethodInfo* method)
{
	typedef Transform_t6_61 * (*Component_get_transform_m6_582_ftn) (Component_t6_26 *);
	static Component_get_transform_m6_582_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m6_582_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" GameObject_t6_85 * Component_get_gameObject_m6_583 (Component_t6_26 * __this, const MethodInfo* method)
{
	typedef GameObject_t6_85 * (*Component_get_gameObject_m6_583_ftn) (Component_t6_26 *);
	static Component_get_gameObject_m6_583_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m6_583_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C" void Component_GetComponentFastPath_m6_584 (Component_t6_26 * __this, Type_t * ___type, IntPtr_t ___oneFurtherThanResultValue, const MethodInfo* method)
{
	typedef void (*Component_GetComponentFastPath_m6_584_ftn) (Component_t6_26 *, Type_t *, IntPtr_t);
	static Component_GetComponentFastPath_m6_584_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m6_584_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type, ___oneFurtherThanResultValue);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type)
extern "C" Component_t6_26 * Component_GetComponentInChildren_m6_585 (Component_t6_26 * __this, Type_t * ___t, const MethodInfo* method)
{
	{
		GameObject_t6_85 * L_0 = Component_get_gameObject_m6_583(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t;
		NullCheck(L_0);
		Component_t6_26 * L_2 = GameObject_GetComponentInChildren_m6_593(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C" void Component_SendMessage_m6_586 (Component_t6_26 * __this, String_t* ___methodName, Object_t * ___value, int32_t ___options, const MethodInfo* method)
{
	typedef void (*Component_SendMessage_m6_586_ftn) (Component_t6_26 *, String_t*, Object_t *, int32_t);
	static Component_SendMessage_m6_586_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_SendMessage_m6_586_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName, ___value, ___options);
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object)
extern "C" void Component_SendMessage_m6_587 (Component_t6_26 * __this, String_t* ___methodName, Object_t * ___value, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		String_t* L_0 = ___methodName;
		Object_t * L_1 = ___value;
		int32_t L_2 = V_0;
		Component_SendMessage_m6_586(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern "C" void Component_SendMessage_m6_588 (Component_t6_26 * __this, String_t* ___methodName, int32_t ___options, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName;
		int32_t L_1 = ___options;
		Component_SendMessage_m6_586(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" void GameObject__ctor_m6_589 (GameObject_t6_85 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Object__ctor_m6_553(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		GameObject_Internal_CreateGameObject_m6_605(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor()
extern "C" void GameObject__ctor_m6_590 (GameObject_t6_85 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m6_553(__this, /*hidden argument*/NULL);
		GameObject_Internal_CreateGameObject_m6_605(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C" Component_t6_26 * GameObject_GetComponent_m6_591 (GameObject_t6_85 * __this, Type_t * ___type, const MethodInfo* method)
{
	typedef Component_t6_26 * (*GameObject_GetComponent_m6_591_ftn) (GameObject_t6_85 *, Type_t *);
	static GameObject_GetComponent_m6_591_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponent_m6_591_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponent(System.Type)");
	return _il2cpp_icall_func(__this, ___type);
}
// System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
extern "C" void GameObject_GetComponentFastPath_m6_592 (GameObject_t6_85 * __this, Type_t * ___type, IntPtr_t ___oneFurtherThanResultValue, const MethodInfo* method)
{
	typedef void (*GameObject_GetComponentFastPath_m6_592_ftn) (GameObject_t6_85 *, Type_t *, IntPtr_t);
	static GameObject_GetComponentFastPath_m6_592_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentFastPath_m6_592_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type, ___oneFurtherThanResultValue);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type)
extern TypeInfo* IEnumerator_t1_130_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t6_61_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_835_il2cpp_TypeInfo_var;
extern "C" Component_t6_26 * GameObject_GetComponentInChildren_m6_593 (GameObject_t6_85 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(159);
		Transform_t6_61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(958);
		IDisposable_t1_835_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		s_Il2CppMethodIntialized = true;
	}
	Component_t6_26 * V_0 = {0};
	Transform_t6_61 * V_1 = {0};
	Transform_t6_61 * V_2 = {0};
	Object_t * V_3 = {0};
	Component_t6_26 * V_4 = {0};
	Component_t6_26 * V_5 = {0};
	Object_t * V_6 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameObject_get_activeInHierarchy_m6_597(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Type_t * L_1 = ___type;
		Component_t6_26 * L_2 = GameObject_GetComponent_m6_591(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Component_t6_26 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		Component_t6_26 * L_5 = V_0;
		return L_5;
	}

IL_0021:
	{
		Transform_t6_61 * L_6 = GameObject_get_transform_m6_595(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		Transform_t6_61 * L_7 = V_1;
		bool L_8 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_7, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0095;
		}
	}
	{
		Transform_t6_61 * L_9 = V_1;
		NullCheck(L_9);
		Object_t * L_10 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_9);
		V_3 = L_10;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0070;
		}

IL_0040:
		{
			Object_t * L_11 = V_3;
			NullCheck(L_11);
			Object_t * L_12 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_130_il2cpp_TypeInfo_var, L_11);
			V_2 = ((Transform_t6_61 *)CastclassClass(L_12, Transform_t6_61_il2cpp_TypeInfo_var));
			Transform_t6_61 * L_13 = V_2;
			NullCheck(L_13);
			GameObject_t6_85 * L_14 = Component_get_gameObject_m6_583(L_13, /*hidden argument*/NULL);
			Type_t * L_15 = ___type;
			NullCheck(L_14);
			Component_t6_26 * L_16 = GameObject_GetComponentInChildren_m6_593(L_14, L_15, /*hidden argument*/NULL);
			V_4 = L_16;
			Component_t6_26 * L_17 = V_4;
			bool L_18 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_17, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_0070;
			}
		}

IL_0067:
		{
			Component_t6_26 * L_19 = V_4;
			V_5 = L_19;
			IL2CPP_LEAVE(0x97, FINALLY_0080);
		}

IL_0070:
		{
			Object_t * L_20 = V_3;
			NullCheck(L_20);
			bool L_21 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_130_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_0040;
			}
		}

IL_007b:
		{
			IL2CPP_LEAVE(0x95, FINALLY_0080);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0080;
	}

FINALLY_0080:
	{ // begin finally (depth: 1)
		{
			Object_t * L_22 = V_3;
			V_6 = ((Object_t *)IsInst(L_22, IDisposable_t1_835_il2cpp_TypeInfo_var));
			Object_t * L_23 = V_6;
			if (L_23)
			{
				goto IL_008d;
			}
		}

IL_008c:
		{
			IL2CPP_END_FINALLY(128)
		}

IL_008d:
		{
			Object_t * L_24 = V_6;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_835_il2cpp_TypeInfo_var, L_24);
			IL2CPP_END_FINALLY(128)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(128)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0095:
	{
		return (Component_t6_26 *)NULL;
	}

IL_0097:
	{
		Component_t6_26 * L_25 = V_5;
		return L_25;
	}
}
// System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern "C" Array_t * GameObject_GetComponentsInternal_m6_594 (GameObject_t6_85 * __this, Type_t * ___type, bool ___useSearchTypeAsArrayReturnType, bool ___recursive, bool ___includeInactive, bool ___reverse, Object_t * ___resultList, const MethodInfo* method)
{
	typedef Array_t * (*GameObject_GetComponentsInternal_m6_594_ftn) (GameObject_t6_85 *, Type_t *, bool, bool, bool, bool, Object_t *);
	static GameObject_GetComponentsInternal_m6_594_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentsInternal_m6_594_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)");
	return _il2cpp_icall_func(__this, ___type, ___useSearchTypeAsArrayReturnType, ___recursive, ___includeInactive, ___reverse, ___resultList);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" Transform_t6_61 * GameObject_get_transform_m6_595 (GameObject_t6_85 * __this, const MethodInfo* method)
{
	typedef Transform_t6_61 * (*GameObject_get_transform_m6_595_ftn) (GameObject_t6_85 *);
	static GameObject_get_transform_m6_595_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_transform_m6_595_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_transform()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" void GameObject_SetActive_m6_596 (GameObject_t6_85 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GameObject_SetActive_m6_596_ftn) (GameObject_t6_85 *, bool);
	static GameObject_SetActive_m6_596_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SetActive_m6_596_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SetActive(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C" bool GameObject_get_activeInHierarchy_m6_597 (GameObject_t6_85 * __this, const MethodInfo* method)
{
	typedef bool (*GameObject_get_activeInHierarchy_m6_597_ftn) (GameObject_t6_85 *);
	static GameObject_get_activeInHierarchy_m6_597_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeInHierarchy_m6_597_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeInHierarchy()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::set_tag(System.String)
extern "C" void GameObject_set_tag_m6_598 (GameObject_t6_85 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*GameObject_set_tag_m6_598_ftn) (GameObject_t6_85 *, String_t*);
	static GameObject_set_tag_m6_598_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_set_tag_m6_598_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_tag(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C" GameObjectU5BU5D_t6_258* GameObject_FindGameObjectsWithTag_m6_599 (Object_t * __this /* static, unused */, String_t* ___tag, const MethodInfo* method)
{
	typedef GameObjectU5BU5D_t6_258* (*GameObject_FindGameObjectsWithTag_m6_599_ftn) (String_t*);
	static GameObject_FindGameObjectsWithTag_m6_599_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_FindGameObjectsWithTag_m6_599_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::FindGameObjectsWithTag(System.String)");
	return _il2cpp_icall_func(___tag);
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C" void GameObject_SendMessage_m6_600 (GameObject_t6_85 * __this, String_t* ___methodName, Object_t * ___value, int32_t ___options, const MethodInfo* method)
{
	typedef void (*GameObject_SendMessage_m6_600_ftn) (GameObject_t6_85 *, String_t*, Object_t *, int32_t);
	static GameObject_SendMessage_m6_600_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SendMessage_m6_600_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName, ___value, ___options);
}
// System.Void UnityEngine.GameObject::SendMessage(System.String)
extern "C" void GameObject_SendMessage_m6_601 (GameObject_t6_85 * __this, String_t* ___methodName, const MethodInfo* method)
{
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName;
		Object_t * L_1 = V_1;
		int32_t L_2 = V_0;
		GameObject_SendMessage_m6_600(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern "C" void GameObject_SendMessage_m6_602 (GameObject_t6_85 * __this, String_t* ___methodName, int32_t ___options, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName;
		int32_t L_1 = ___options;
		GameObject_SendMessage_m6_600(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C" Component_t6_26 * GameObject_Internal_AddComponentWithType_m6_603 (GameObject_t6_85 * __this, Type_t * ___componentType, const MethodInfo* method)
{
	typedef Component_t6_26 * (*GameObject_Internal_AddComponentWithType_m6_603_ftn) (GameObject_t6_85 *, Type_t *);
	static GameObject_Internal_AddComponentWithType_m6_603_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_AddComponentWithType_m6_603_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)");
	return _il2cpp_icall_func(__this, ___componentType);
}
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C" Component_t6_26 * GameObject_AddComponent_m6_604 (GameObject_t6_85 * __this, Type_t * ___componentType, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___componentType;
		Component_t6_26 * L_1 = GameObject_Internal_AddComponentWithType_m6_603(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C" void GameObject_Internal_CreateGameObject_m6_605 (Object_t * __this /* static, unused */, GameObject_t6_85 * ___mono, String_t* ___name, const MethodInfo* method)
{
	typedef void (*GameObject_Internal_CreateGameObject_m6_605_ftn) (GameObject_t6_85 *, String_t*);
	static GameObject_Internal_CreateGameObject_m6_605_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_CreateGameObject_m6_605_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)");
	_il2cpp_icall_func(___mono, ___name);
}
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C" GameObject_t6_85 * GameObject_Find_m6_606 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef GameObject_t6_85 * (*GameObject_Find_m6_606_ftn) (String_t*);
	static GameObject_Find_m6_606_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Find_m6_606_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Find(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C" void Enumerator__ctor_m6_607 (Enumerator_t6_86 * __this, Transform_t6_61 * ___outer, const MethodInfo* method)
{
	{
		__this->___currentIndex_1 = (-1);
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Transform_t6_61 * L_0 = ___outer;
		__this->___outer_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C" Object_t * Enumerator_get_Current_m6_608 (Enumerator_t6_86 * __this, const MethodInfo* method)
{
	{
		Transform_t6_61 * L_0 = (__this->___outer_0);
		int32_t L_1 = (__this->___currentIndex_1);
		NullCheck(L_0);
		Transform_t6_61 * L_2 = Transform_GetChild_m6_651(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C" bool Enumerator_MoveNext_m6_609 (Enumerator_t6_86 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Transform_t6_61 * L_0 = (__this->___outer_0);
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m6_649(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (__this->___currentIndex_1);
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->___currentIndex_1 = L_3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		return ((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
	}
}
// System.Void UnityEngine.Transform/Enumerator::Reset()
extern "C" void Enumerator_Reset_m6_610 (Enumerator_t6_86 * __this, const MethodInfo* method)
{
	{
		__this->___currentIndex_1 = (-1);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" Vector3_t6_49  Transform_get_position_m6_611 (Transform_t6_61 * __this, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	{
		Transform_INTERNAL_get_position_m6_613(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6_49  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" void Transform_set_position_m6_612 (Transform_t6_61 * __this, Vector3_t6_49  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_position_m6_614(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_position_m6_613 (Transform_t6_61 * __this, Vector3_t6_49 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_position_m6_613_ftn) (Transform_t6_61 *, Vector3_t6_49 *);
	static Transform_INTERNAL_get_position_m6_613_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m6_613_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_position_m6_614 (Transform_t6_61 * __this, Vector3_t6_49 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_position_m6_614_ftn) (Transform_t6_61 *, Vector3_t6_49 *);
	static Transform_INTERNAL_set_position_m6_614_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m6_614_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C" Vector3_t6_49  Transform_get_localPosition_m6_615 (Transform_t6_61 * __this, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	{
		Transform_INTERNAL_get_localPosition_m6_617(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6_49  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" void Transform_set_localPosition_m6_616 (Transform_t6_61 * __this, Vector3_t6_49  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localPosition_m6_618(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localPosition_m6_617 (Transform_t6_61 * __this, Vector3_t6_49 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m6_617_ftn) (Transform_t6_61 *, Vector3_t6_49 *);
	static Transform_INTERNAL_get_localPosition_m6_617_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m6_617_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localPosition_m6_618 (Transform_t6_61 * __this, Vector3_t6_49 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m6_618_ftn) (Transform_t6_61 *, Vector3_t6_49 *);
	static Transform_INTERNAL_set_localPosition_m6_618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m6_618_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C" Vector3_t6_49  Transform_get_eulerAngles_m6_619 (Transform_t6_61 * __this, const MethodInfo* method)
{
	Quaternion_t6_51  V_0 = {0};
	{
		Quaternion_t6_51  L_0 = Transform_get_rotation_m6_623(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t6_49  L_1 = Quaternion_get_eulerAngles_m6_259((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern "C" Vector3_t6_49  Transform_get_right_m6_620 (Transform_t6_61 * __this, const MethodInfo* method)
{
	{
		Quaternion_t6_51  L_0 = Transform_get_rotation_m6_623(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = Vector3_get_right_m6_220(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_2 = Quaternion_op_Multiply_m6_268(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C" Vector3_t6_49  Transform_get_up_m6_621 (Transform_t6_61 * __this, const MethodInfo* method)
{
	{
		Quaternion_t6_51  L_0 = Transform_get_rotation_m6_623(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = Vector3_get_up_m6_217(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_2 = Quaternion_op_Multiply_m6_268(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C" Vector3_t6_49  Transform_get_forward_m6_622 (Transform_t6_61 * __this, const MethodInfo* method)
{
	{
		Quaternion_t6_51  L_0 = Transform_get_rotation_m6_623(__this, /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = Vector3_get_forward_m6_215(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_49  L_2 = Quaternion_op_Multiply_m6_268(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" Quaternion_t6_51  Transform_get_rotation_m6_623 (Transform_t6_61 * __this, const MethodInfo* method)
{
	Quaternion_t6_51  V_0 = {0};
	{
		Transform_INTERNAL_get_rotation_m6_625(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t6_51  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C" void Transform_set_rotation_m6_624 (Transform_t6_61 * __this, Quaternion_t6_51  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_rotation_m6_626(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_rotation_m6_625 (Transform_t6_61 * __this, Quaternion_t6_51 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m6_625_ftn) (Transform_t6_61 *, Quaternion_t6_51 *);
	static Transform_INTERNAL_get_rotation_m6_625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m6_625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_set_rotation_m6_626 (Transform_t6_61 * __this, Quaternion_t6_51 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_rotation_m6_626_ftn) (Transform_t6_61 *, Quaternion_t6_51 *);
	static Transform_INTERNAL_set_rotation_m6_626_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_rotation_m6_626_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C" Quaternion_t6_51  Transform_get_localRotation_m6_627 (Transform_t6_61 * __this, const MethodInfo* method)
{
	Quaternion_t6_51  V_0 = {0};
	{
		Transform_INTERNAL_get_localRotation_m6_629(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t6_51  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" void Transform_set_localRotation_m6_628 (Transform_t6_61 * __this, Quaternion_t6_51  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localRotation_m6_630(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_localRotation_m6_629 (Transform_t6_61 * __this, Quaternion_t6_51 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m6_629_ftn) (Transform_t6_61 *, Quaternion_t6_51 *);
	static Transform_INTERNAL_get_localRotation_m6_629_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m6_629_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_set_localRotation_m6_630 (Transform_t6_61 * __this, Quaternion_t6_51 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m6_630_ftn) (Transform_t6_61 *, Quaternion_t6_51 *);
	static Transform_INTERNAL_set_localRotation_m6_630_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m6_630_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C" Vector3_t6_49  Transform_get_localScale_m6_631 (Transform_t6_61 * __this, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	{
		Transform_INTERNAL_get_localScale_m6_633(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6_49  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" void Transform_set_localScale_m6_632 (Transform_t6_61 * __this, Vector3_t6_49  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localScale_m6_634(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localScale_m6_633 (Transform_t6_61 * __this, Vector3_t6_49 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m6_633_ftn) (Transform_t6_61 *, Vector3_t6_49 *);
	static Transform_INTERNAL_get_localScale_m6_633_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m6_633_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localScale_m6_634 (Transform_t6_61 * __this, Vector3_t6_49 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m6_634_ftn) (Transform_t6_61 *, Vector3_t6_49 *);
	static Transform_INTERNAL_set_localScale_m6_634_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m6_634_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C" Transform_t6_61 * Transform_get_parent_m6_635 (Transform_t6_61 * __this, const MethodInfo* method)
{
	{
		Transform_t6_61 * L_0 = Transform_get_parentInternal_m6_637(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern TypeInfo* RectTransform_t6_60_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2708;
extern "C" void Transform_set_parent_m6_636 (Transform_t6_61 * __this, Transform_t6_61 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t6_60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(948);
		_stringLiteral2708 = il2cpp_codegen_string_literal_from_index(2708);
		s_Il2CppMethodIntialized = true;
	}
	{
		if (!((RectTransform_t6_60 *)IsInstSealed(__this, RectTransform_t6_60_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogWarning_m6_492(NULL /*static, unused*/, _stringLiteral2708, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Transform_t6_61 * L_0 = ___value;
		Transform_set_parentInternal_m6_638(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C" Transform_t6_61 * Transform_get_parentInternal_m6_637 (Transform_t6_61 * __this, const MethodInfo* method)
{
	typedef Transform_t6_61 * (*Transform_get_parentInternal_m6_637_ftn) (Transform_t6_61 *);
	static Transform_get_parentInternal_m6_637_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m6_637_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C" void Transform_set_parentInternal_m6_638 (Transform_t6_61 * __this, Transform_t6_61 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_set_parentInternal_m6_638_ftn) (Transform_t6_61 *, Transform_t6_61 *);
	static Transform_set_parentInternal_m6_638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_set_parentInternal_m6_638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern "C" void Transform_Rotate_m6_639 (Transform_t6_61 * __this, Vector3_t6_49  ___eulerAngles, int32_t ___relativeTo, const MethodInfo* method)
{
	Quaternion_t6_51  V_0 = {0};
	{
		float L_0 = ((&___eulerAngles)->___x_1);
		float L_1 = ((&___eulerAngles)->___y_2);
		float L_2 = ((&___eulerAngles)->___z_3);
		Quaternion_t6_51  L_3 = Quaternion_Euler_m6_260(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = ___relativeTo;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0039;
		}
	}
	{
		Quaternion_t6_51  L_5 = Transform_get_localRotation_m6_627(__this, /*hidden argument*/NULL);
		Quaternion_t6_51  L_6 = V_0;
		Quaternion_t6_51  L_7 = Quaternion_op_Multiply_m6_267(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Transform_set_localRotation_m6_628(__this, L_7, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_0039:
	{
		Quaternion_t6_51  L_8 = Transform_get_rotation_m6_623(__this, /*hidden argument*/NULL);
		Quaternion_t6_51  L_9 = Transform_get_rotation_m6_623(__this, /*hidden argument*/NULL);
		Quaternion_t6_51  L_10 = Quaternion_Inverse_m6_255(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Quaternion_t6_51  L_11 = V_0;
		Quaternion_t6_51  L_12 = Quaternion_op_Multiply_m6_267(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Quaternion_t6_51  L_13 = Transform_get_rotation_m6_623(__this, /*hidden argument*/NULL);
		Quaternion_t6_51  L_14 = Quaternion_op_Multiply_m6_267(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Quaternion_t6_51  L_15 = Quaternion_op_Multiply_m6_267(NULL /*static, unused*/, L_8, L_14, /*hidden argument*/NULL);
		Transform_set_rotation_m6_624(__this, L_15, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
extern "C" void Transform_Rotate_m6_640 (Transform_t6_61 * __this, float ___xAngle, float ___yAngle, float ___zAngle, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 1;
		float L_0 = ___xAngle;
		float L_1 = ___yAngle;
		float L_2 = ___zAngle;
		int32_t L_3 = V_0;
		Transform_Rotate_m6_641(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern "C" void Transform_Rotate_m6_641 (Transform_t6_61 * __this, float ___xAngle, float ___yAngle, float ___zAngle, int32_t ___relativeTo, const MethodInfo* method)
{
	{
		float L_0 = ___xAngle;
		float L_1 = ___yAngle;
		float L_2 = ___zAngle;
		Vector3_t6_49  L_3 = {0};
		Vector3__ctor_m6_191(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___relativeTo;
		Transform_Rotate_m6_639(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform)
extern "C" void Transform_LookAt_m6_642 (Transform_t6_61 * __this, Transform_t6_61 * ___target, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	{
		Vector3_t6_49  L_0 = Vector3_get_up_m6_217(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_t6_61 * L_1 = ___target;
		Vector3_t6_49  L_2 = V_0;
		Transform_LookAt_m6_643(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform,UnityEngine.Vector3)
extern "C" void Transform_LookAt_m6_643 (Transform_t6_61 * __this, Transform_t6_61 * ___target, Vector3_t6_49  ___worldUp, const MethodInfo* method)
{
	{
		Transform_t6_61 * L_0 = ___target;
		bool L_1 = Object_op_Implicit_m6_578(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Transform_t6_61 * L_2 = ___target;
		NullCheck(L_2);
		Vector3_t6_49  L_3 = Transform_get_position_m6_611(L_2, /*hidden argument*/NULL);
		Vector3_t6_49  L_4 = ___worldUp;
		Transform_LookAt_m6_644(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Transform_LookAt_m6_644 (Transform_t6_61 * __this, Vector3_t6_49  ___worldPosition, Vector3_t6_49  ___worldUp, const MethodInfo* method)
{
	{
		Transform_INTERNAL_CALL_LookAt_m6_645(NULL /*static, unused*/, __this, (&___worldPosition), (&___worldUp), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_CALL_LookAt_m6_645 (Object_t * __this /* static, unused */, Transform_t6_61 * ___self, Vector3_t6_49 * ___worldPosition, Vector3_t6_49 * ___worldUp, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_LookAt_m6_645_ftn) (Transform_t6_61 *, Vector3_t6_49 *, Vector3_t6_49 *);
	static Transform_INTERNAL_CALL_LookAt_m6_645_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_LookAt_m6_645_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self, ___worldPosition, ___worldUp);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C" Vector3_t6_49  Transform_TransformDirection_m6_646 (Transform_t6_61 * __this, Vector3_t6_49  ___direction, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Transform_INTERNAL_CALL_TransformDirection_m6_647(NULL /*static, unused*/, __this, (&___direction), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t6_49  Transform_INTERNAL_CALL_TransformDirection_m6_647 (Object_t * __this /* static, unused */, Transform_t6_61 * ___self, Vector3_t6_49 * ___direction, const MethodInfo* method)
{
	typedef Vector3_t6_49  (*Transform_INTERNAL_CALL_TransformDirection_m6_647_ftn) (Transform_t6_61 *, Vector3_t6_49 *);
	static Transform_INTERNAL_CALL_TransformDirection_m6_647_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformDirection_m6_647_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___direction);
}
// UnityEngine.Transform UnityEngine.Transform::get_root()
extern "C" Transform_t6_61 * Transform_get_root_m6_648 (Transform_t6_61 * __this, const MethodInfo* method)
{
	typedef Transform_t6_61 * (*Transform_get_root_m6_648_ftn) (Transform_t6_61 *);
	static Transform_get_root_m6_648_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_root_m6_648_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_root()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" int32_t Transform_get_childCount_m6_649 (Transform_t6_61 * __this, const MethodInfo* method)
{
	typedef int32_t (*Transform_get_childCount_m6_649_ftn) (Transform_t6_61 *);
	static Transform_get_childCount_m6_649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m6_649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	return _il2cpp_icall_func(__this);
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern TypeInfo* Enumerator_t6_86_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_GetEnumerator_m6_650 (Transform_t6_61 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t6_86_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(959);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t6_86 * L_0 = (Enumerator_t6_86 *)il2cpp_codegen_object_new (Enumerator_t6_86_il2cpp_TypeInfo_var);
		Enumerator__ctor_m6_607(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C" Transform_t6_61 * Transform_GetChild_m6_651 (Transform_t6_61 * __this, int32_t ___index, const MethodInfo* method)
{
	typedef Transform_t6_61 * (*Transform_GetChild_m6_651_ftn) (Transform_t6_61 *, int32_t);
	static Transform_GetChild_m6_651_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m6_651_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	return _il2cpp_icall_func(__this, ___index);
}
// System.Single UnityEngine.Time::get_time()
extern "C" float Time_get_time_m6_652 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_time_m6_652_ftn) ();
	static Time_get_time_m6_652_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_time_m6_652_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_time()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_timeSinceLevelLoad()
extern "C" float Time_get_timeSinceLevelLoad_m6_653 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_timeSinceLevelLoad_m6_653_ftn) ();
	static Time_get_timeSinceLevelLoad_m6_653_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_timeSinceLevelLoad_m6_653_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_timeSinceLevelLoad()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" float Time_get_deltaTime_m6_654 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_deltaTime_m6_654_ftn) ();
	static Time_get_deltaTime_m6_654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m6_654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Time::get_frameCount()
extern "C" int32_t Time_get_frameCount_m6_655 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Time_get_frameCount_m6_655_ftn) ();
	static Time_get_frameCount_m6_655_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_frameCount_m6_655_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_frameCount()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C" float Time_get_realtimeSinceStartup_m6_656 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m6_656_ftn) ();
	static Time_get_realtimeSinceStartup_m6_656_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m6_656_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C" float Random_Range_m6_657 (Object_t * __this /* static, unused */, float ___min, float ___max, const MethodInfo* method)
{
	typedef float (*Random_Range_m6_657_ftn) (float, float);
	static Random_Range_m6_657_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m6_657_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	return _il2cpp_icall_func(___min, ___max);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" int32_t Random_Range_m6_658 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	{
		int32_t L_0 = ___min;
		int32_t L_1 = ___max;
		int32_t L_2 = Random_RandomRangeInt_m6_659(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C" int32_t Random_RandomRangeInt_m6_659 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m6_659_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m6_659_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m6_659_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min, ___max);
}
// UnityEngine.Vector3 UnityEngine.Random::get_insideUnitSphere()
extern "C" Vector3_t6_49  Random_get_insideUnitSphere_m6_660 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	{
		Random_INTERNAL_get_insideUnitSphere_m6_661(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t6_49  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Random::INTERNAL_get_insideUnitSphere(UnityEngine.Vector3&)
extern "C" void Random_INTERNAL_get_insideUnitSphere_m6_661 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___value, const MethodInfo* method)
{
	typedef void (*Random_INTERNAL_get_insideUnitSphere_m6_661_ftn) (Vector3_t6_49 *);
	static Random_INTERNAL_get_insideUnitSphere_m6_661_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_INTERNAL_get_insideUnitSphere_m6_661_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::INTERNAL_get_insideUnitSphere(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m6_662 (YieldInstruction_t6_12 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t6_12_marshal(const YieldInstruction_t6_12& unmarshaled, YieldInstruction_t6_12_marshaled& marshaled)
{
}
extern "C" void YieldInstruction_t6_12_marshal_back(const YieldInstruction_t6_12_marshaled& marshaled, YieldInstruction_t6_12& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t6_12_marshal_cleanup(YieldInstruction_t6_12_marshaled& marshaled)
{
}
// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern "C" void PlayerPrefsException__ctor_m6_663 (PlayerPrefsException_t6_89 * __this, String_t* ___error, const MethodInfo* method)
{
	{
		String_t* L_0 = ___error;
		Exception__ctor_m1_932(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
extern "C" bool PlayerPrefs_TrySetSetString_m6_664 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___value, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetSetString_m6_664_ftn) (String_t*, String_t*);
	static PlayerPrefs_TrySetSetString_m6_664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetSetString_m6_664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)");
	return _il2cpp_icall_func(___key, ___value);
}
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern TypeInfo* PlayerPrefsException_t6_89_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2709;
extern "C" void PlayerPrefs_SetString_m6_665 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerPrefsException_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(960);
		_stringLiteral2709 = il2cpp_codegen_string_literal_from_index(2709);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key;
		String_t* L_1 = ___value;
		bool L_2 = PlayerPrefs_TrySetSetString_m6_664(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t6_89 * L_3 = (PlayerPrefsException_t6_89 *)il2cpp_codegen_object_new (PlayerPrefsException_t6_89_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m6_663(L_3, _stringLiteral2709, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern "C" String_t* PlayerPrefs_GetString_m6_666 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___defaultValue, const MethodInfo* method)
{
	typedef String_t* (*PlayerPrefs_GetString_m6_666_ftn) (String_t*, String_t*);
	static PlayerPrefs_GetString_m6_666_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetString_m6_666_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetString(System.String,System.String)");
	return _il2cpp_icall_func(___key, ___defaultValue);
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* PlayerPrefs_GetString_m6_667 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ___key;
		String_t* L_2 = V_0;
		String_t* L_3 = PlayerPrefs_GetString_m6_666(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.PlayerPrefs::DeleteKey(System.String)
extern "C" void PlayerPrefs_DeleteKey_m6_668 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_DeleteKey_m6_668_ftn) (String_t*);
	static PlayerPrefs_DeleteKey_m6_668_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_DeleteKey_m6_668_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::DeleteKey(System.String)");
	_il2cpp_icall_func(___key);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::.ctor()
extern "C" void UnityAdsInternal__ctor_m6_669 (UnityAdsInternal_t6_93 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onCampaignsAvailable(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onCampaignsAvailable_m6_670 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_94 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(962);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0;
		UnityAdsDelegate_t6_94 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_695(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0 = ((UnityAdsDelegate_t6_94 *)CastclassSealed(L_2, UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onCampaignsAvailable(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onCampaignsAvailable_m6_671 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_94 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(962);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0;
		UnityAdsDelegate_t6_94 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_698(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0 = ((UnityAdsDelegate_t6_94 *)CastclassSealed(L_2, UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onCampaignsFetchFailed(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onCampaignsFetchFailed_m6_672 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_94 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(962);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1;
		UnityAdsDelegate_t6_94 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_695(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1 = ((UnityAdsDelegate_t6_94 *)CastclassSealed(L_2, UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onCampaignsFetchFailed(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onCampaignsFetchFailed_m6_673 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_94 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(962);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1;
		UnityAdsDelegate_t6_94 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_698(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1 = ((UnityAdsDelegate_t6_94 *)CastclassSealed(L_2, UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onShow(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onShow_m6_674 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_94 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(962);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onShow_2;
		UnityAdsDelegate_t6_94 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_695(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onShow_2 = ((UnityAdsDelegate_t6_94 *)CastclassSealed(L_2, UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onShow(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onShow_m6_675 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_94 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(962);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onShow_2;
		UnityAdsDelegate_t6_94 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_698(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onShow_2 = ((UnityAdsDelegate_t6_94 *)CastclassSealed(L_2, UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onHide(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onHide_m6_676 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_94 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(962);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onHide_3;
		UnityAdsDelegate_t6_94 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_695(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onHide_3 = ((UnityAdsDelegate_t6_94 *)CastclassSealed(L_2, UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onHide(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onHide_m6_677 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_94 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(962);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onHide_3;
		UnityAdsDelegate_t6_94 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_698(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onHide_3 = ((UnityAdsDelegate_t6_94 *)CastclassSealed(L_2, UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onVideoCompleted(UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_2_t6_95_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onVideoCompleted_m6_678 (Object_t * __this /* static, unused */, UnityAdsDelegate_2_t6_95 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_2_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(963);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_2_t6_95 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4;
		UnityAdsDelegate_2_t6_95 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_695(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4 = ((UnityAdsDelegate_2_t6_95 *)CastclassSealed(L_2, UnityAdsDelegate_2_t6_95_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onVideoCompleted(UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_2_t6_95_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onVideoCompleted_m6_679 (Object_t * __this /* static, unused */, UnityAdsDelegate_2_t6_95 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_2_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(963);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_2_t6_95 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4;
		UnityAdsDelegate_2_t6_95 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_698(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4 = ((UnityAdsDelegate_2_t6_95 *)CastclassSealed(L_2, UnityAdsDelegate_2_t6_95_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onVideoStarted(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onVideoStarted_m6_680 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_94 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(962);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5;
		UnityAdsDelegate_t6_94 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_695(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5 = ((UnityAdsDelegate_t6_94 *)CastclassSealed(L_2, UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onVideoStarted(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onVideoStarted_m6_681 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_94 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(962);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5;
		UnityAdsDelegate_t6_94 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_698(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5 = ((UnityAdsDelegate_t6_94 *)CastclassSealed(L_2, UnityAdsDelegate_t6_94_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::RegisterNative()
extern "C" void UnityAdsInternal_RegisterNative_m6_682 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_RegisterNative_m6_682_ftn) ();
	static UnityAdsInternal_RegisterNative_m6_682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_RegisterNative_m6_682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::RegisterNative()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::Init(System.String,System.Boolean,System.Boolean,System.String)
extern "C" void UnityAdsInternal_Init_m6_683 (Object_t * __this /* static, unused */, String_t* ___gameId, bool ___testModeEnabled, bool ___debugModeEnabled, String_t* ___unityVersion, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_Init_m6_683_ftn) (String_t*, bool, bool, String_t*);
	static UnityAdsInternal_Init_m6_683_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_Init_m6_683_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::Init(System.String,System.Boolean,System.Boolean,System.String)");
	_il2cpp_icall_func(___gameId, ___testModeEnabled, ___debugModeEnabled, ___unityVersion);
}
// System.Boolean UnityEngine.Advertisements.UnityAdsInternal::Show(System.String,System.String,System.String)
extern "C" bool UnityAdsInternal_Show_m6_684 (Object_t * __this /* static, unused */, String_t* ___zoneId, String_t* ___rewardItemKey, String_t* ___options, const MethodInfo* method)
{
	typedef bool (*UnityAdsInternal_Show_m6_684_ftn) (String_t*, String_t*, String_t*);
	static UnityAdsInternal_Show_m6_684_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_Show_m6_684_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::Show(System.String,System.String,System.String)");
	return _il2cpp_icall_func(___zoneId, ___rewardItemKey, ___options);
}
// System.Boolean UnityEngine.Advertisements.UnityAdsInternal::CanShowAds(System.String)
extern "C" bool UnityAdsInternal_CanShowAds_m6_685 (Object_t * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	typedef bool (*UnityAdsInternal_CanShowAds_m6_685_ftn) (String_t*);
	static UnityAdsInternal_CanShowAds_m6_685_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_CanShowAds_m6_685_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::CanShowAds(System.String)");
	return _il2cpp_icall_func(___zoneId);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::SetLogLevel(System.Int32)
extern "C" void UnityAdsInternal_SetLogLevel_m6_686 (Object_t * __this /* static, unused */, int32_t ___logLevel, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_SetLogLevel_m6_686_ftn) (int32_t);
	static UnityAdsInternal_SetLogLevel_m6_686_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_SetLogLevel_m6_686_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::SetLogLevel(System.Int32)");
	_il2cpp_icall_func(___logLevel);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::SetCampaignDataURL(System.String)
extern "C" void UnityAdsInternal_SetCampaignDataURL_m6_687 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_SetCampaignDataURL_m6_687_ftn) (String_t*);
	static UnityAdsInternal_SetCampaignDataURL_m6_687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_SetCampaignDataURL_m6_687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::SetCampaignDataURL(System.String)");
	_il2cpp_icall_func(___url);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::RemoveAllEventHandlers()
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_RemoveAllEventHandlers_m6_688 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		s_Il2CppMethodIntialized = true;
	}
	{
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0 = (UnityAdsDelegate_t6_94 *)NULL;
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1 = (UnityAdsDelegate_t6_94 *)NULL;
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onShow_2 = (UnityAdsDelegate_t6_94 *)NULL;
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onHide_3 = (UnityAdsDelegate_t6_94 *)NULL;
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4 = (UnityAdsDelegate_2_t6_95 *)NULL;
		((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5 = (UnityAdsDelegate_t6_94 *)NULL;
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsCampaignsAvailable()
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsAvailable_m6_689 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t6_94 * V_0 = {0};
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0;
		V_0 = L_0;
		UnityAdsDelegate_t6_94 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t6_94 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m6_1650(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsCampaignsFetchFailed()
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m6_690 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t6_94 * V_0 = {0};
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1;
		V_0 = L_0;
		UnityAdsDelegate_t6_94 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t6_94 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m6_1650(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsShow()
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsShow_m6_691 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t6_94 * V_0 = {0};
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onShow_2;
		V_0 = L_0;
		UnityAdsDelegate_t6_94 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t6_94 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m6_1650(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsHide()
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsHide_m6_692 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t6_94 * V_0 = {0};
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onHide_3;
		V_0 = L_0;
		UnityAdsDelegate_t6_94 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t6_94 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m6_1650(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsVideoCompleted(System.String,System.Boolean)
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAdsDelegate_2_Invoke_m6_1653_MethodInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsVideoCompleted_m6_693 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, bool ___skipped, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		UnityAdsDelegate_2_Invoke_m6_1653_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483804);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_2_t6_95 * V_0 = {0};
	{
		UnityAdsDelegate_2_t6_95 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4;
		V_0 = L_0;
		UnityAdsDelegate_2_t6_95 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		UnityAdsDelegate_2_t6_95 * L_2 = V_0;
		String_t* L_3 = ___rewardItemKey;
		bool L_4 = ___skipped;
		NullCheck(L_2);
		UnityAdsDelegate_2_Invoke_m6_1653(L_2, L_3, L_4, /*hidden argument*/UnityAdsDelegate_2_Invoke_m6_1653_MethodInfo_var);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsVideoStarted()
extern TypeInfo* UnityAdsInternal_t6_93_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsVideoStarted_m6_694 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(961);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t6_94 * V_0 = {0};
	{
		UnityAdsDelegate_t6_94 * L_0 = ((UnityAdsInternal_t6_93_StaticFields*)UnityAdsInternal_t6_93_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5;
		V_0 = L_0;
		UnityAdsDelegate_t6_94 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t6_94 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m6_1650(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Particle::get_position()
extern "C" Vector3_t6_49  Particle_get_position_m6_695 (Particle_t6_96 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = (__this->___m_Position_0);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_position(UnityEngine.Vector3)
extern "C" void Particle_set_position_m6_696 (Particle_t6_96 * __this, Vector3_t6_49  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___value;
		__this->___m_Position_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Particle::get_velocity()
extern "C" Vector3_t6_49  Particle_get_velocity_m6_697 (Particle_t6_96 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = (__this->___m_Velocity_1);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_velocity(UnityEngine.Vector3)
extern "C" void Particle_set_velocity_m6_698 (Particle_t6_96 * __this, Vector3_t6_49  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___value;
		__this->___m_Velocity_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_energy()
extern "C" float Particle_get_energy_m6_699 (Particle_t6_96 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Energy_5);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_energy(System.Single)
extern "C" void Particle_set_energy_m6_700 (Particle_t6_96 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Energy_5 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_startEnergy()
extern "C" float Particle_get_startEnergy_m6_701 (Particle_t6_96 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_StartEnergy_6);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_startEnergy(System.Single)
extern "C" void Particle_set_startEnergy_m6_702 (Particle_t6_96 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_StartEnergy_6 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_size()
extern "C" float Particle_get_size_m6_703 (Particle_t6_96 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Size_2);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_size(System.Single)
extern "C" void Particle_set_size_m6_704 (Particle_t6_96 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Size_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_rotation()
extern "C" float Particle_get_rotation_m6_705 (Particle_t6_96 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Rotation_3);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_rotation(System.Single)
extern "C" void Particle_set_rotation_m6_706 (Particle_t6_96 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Rotation_3 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_angularVelocity()
extern "C" float Particle_get_angularVelocity_m6_707 (Particle_t6_96 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_AngularVelocity_4);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_angularVelocity(System.Single)
extern "C" void Particle_set_angularVelocity_m6_708 (Particle_t6_96 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_AngularVelocity_4 = L_0;
		return;
	}
}
// UnityEngine.Color UnityEngine.Particle::get_color()
extern "C" Color_t6_40  Particle_get_color_m6_709 (Particle_t6_96 * __this, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = (__this->___m_Color_7);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_color(UnityEngine.Color)
extern "C" void Particle_set_color_m6_710 (Particle_t6_96 * __this, Color_t6_40  ___value, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = ___value;
		__this->___m_Color_7 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_moveDirection()
extern "C" Vector3_t6_49  ControllerColliderHit_get_moveDirection_m6_711 (ControllerColliderHit_t6_98 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = (__this->___m_MoveDirection_4);
		return L_0;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" bool Physics_Raycast_m6_712 (Object_t * __this /* static, unused */, Vector3_t6_49  ___origin, Vector3_t6_49  ___direction, RaycastHit_t6_108 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = ___origin;
		Vector3_t6_49  L_1 = ___direction;
		RaycastHit_t6_108 * L_2 = ___hitInfo;
		float L_3 = ___maxDistance;
		int32_t L_4 = ___layerMask;
		int32_t L_5 = ___queryTriggerInteraction;
		bool L_6 = Physics_Internal_Raycast_m6_715(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C" bool Physics_Raycast_m6_713 (Object_t * __this /* static, unused */, Ray_t6_56  ___ray, RaycastHit_t6_108 * ___hitInfo, float ___maxDistance, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t6_56  L_0 = ___ray;
		RaycastHit_t6_108 * L_1 = ___hitInfo;
		float L_2 = ___maxDistance;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m6_714(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" bool Physics_Raycast_m6_714 (Object_t * __this /* static, unused */, Ray_t6_56  ___ray, RaycastHit_t6_108 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = Ray_get_origin_m6_372((&___ray), /*hidden argument*/NULL);
		Vector3_t6_49  L_1 = Ray_get_direction_m6_373((&___ray), /*hidden argument*/NULL);
		RaycastHit_t6_108 * L_2 = ___hitInfo;
		float L_3 = ___maxDistance;
		int32_t L_4 = ___layerMask;
		int32_t L_5 = ___queryTriggerInteraction;
		bool L_6 = Physics_Raycast_m6_712(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" bool Physics_Internal_Raycast_m6_715 (Object_t * __this /* static, unused */, Vector3_t6_49  ___origin, Vector3_t6_49  ___direction, RaycastHit_t6_108 * ___hitInfo, float ___maxDistance, int32_t ___layermask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		RaycastHit_t6_108 * L_0 = ___hitInfo;
		float L_1 = ___maxDistance;
		int32_t L_2 = ___layermask;
		int32_t L_3 = ___queryTriggerInteraction;
		bool L_4 = Physics_INTERNAL_CALL_Internal_Raycast_m6_716(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" bool Physics_INTERNAL_CALL_Internal_Raycast_m6_716 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___origin, Vector3_t6_49 * ___direction, RaycastHit_t6_108 * ___hitInfo, float ___maxDistance, int32_t ___layermask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m6_716_ftn) (Vector3_t6_49 *, Vector3_t6_49 *, RaycastHit_t6_108 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m6_716_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m6_716_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin, ___direction, ___hitInfo, ___maxDistance, ___layermask, ___queryTriggerInteraction);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern "C" Vector3_t6_49  Rigidbody_get_velocity_m6_717 (Rigidbody_t6_102 * __this, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	{
		Rigidbody_INTERNAL_get_velocity_m6_719(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6_49  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C" void Rigidbody_set_velocity_m6_718 (Rigidbody_t6_102 * __this, Vector3_t6_49  ___value, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_velocity_m6_720(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C" void Rigidbody_INTERNAL_get_velocity_m6_719 (Rigidbody_t6_102 * __this, Vector3_t6_49 * ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_velocity_m6_719_ftn) (Rigidbody_t6_102 *, Vector3_t6_49 *);
	static Rigidbody_INTERNAL_get_velocity_m6_719_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_velocity_m6_719_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C" void Rigidbody_INTERNAL_set_velocity_m6_720 (Rigidbody_t6_102 * __this, Vector3_t6_49 * ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_velocity_m6_720_ftn) (Rigidbody_t6_102 *, Vector3_t6_49 *);
	static Rigidbody_INTERNAL_set_velocity_m6_720_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_velocity_m6_720_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_angularVelocity()
extern "C" Vector3_t6_49  Rigidbody_get_angularVelocity_m6_721 (Rigidbody_t6_102 * __this, const MethodInfo* method)
{
	Vector3_t6_49  V_0 = {0};
	{
		Rigidbody_INTERNAL_get_angularVelocity_m6_723(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6_49  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
extern "C" void Rigidbody_set_angularVelocity_m6_722 (Rigidbody_t6_102 * __this, Vector3_t6_49  ___value, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_angularVelocity_m6_724(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_angularVelocity(UnityEngine.Vector3&)
extern "C" void Rigidbody_INTERNAL_get_angularVelocity_m6_723 (Rigidbody_t6_102 * __this, Vector3_t6_49 * ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_angularVelocity_m6_723_ftn) (Rigidbody_t6_102 *, Vector3_t6_49 *);
	static Rigidbody_INTERNAL_get_angularVelocity_m6_723_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_angularVelocity_m6_723_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_angularVelocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_angularVelocity(UnityEngine.Vector3&)
extern "C" void Rigidbody_INTERNAL_set_angularVelocity_m6_724 (Rigidbody_t6_102 * __this, Vector3_t6_49 * ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_angularVelocity_m6_724_ftn) (Rigidbody_t6_102 *, Vector3_t6_49 *);
	static Rigidbody_INTERNAL_set_angularVelocity_m6_724_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_angularVelocity_m6_724_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_angularVelocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern "C" void Rigidbody_set_isKinematic_m6_725 (Rigidbody_t6_102 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_isKinematic_m6_725_ftn) (Rigidbody_t6_102 *, bool);
	static Rigidbody_set_isKinematic_m6_725_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_isKinematic_m6_725_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern "C" void Rigidbody_AddForce_m6_726 (Rigidbody_t6_102 * __this, Vector3_t6_49  ___force, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddForce_m6_727(NULL /*static, unused*/, __this, (&___force), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C" void Rigidbody_INTERNAL_CALL_AddForce_m6_727 (Object_t * __this /* static, unused */, Rigidbody_t6_102 * ___self, Vector3_t6_49 * ___force, int32_t ___mode, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForce_m6_727_ftn) (Rigidbody_t6_102 *, Vector3_t6_49 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForce_m6_727_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForce_m6_727_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self, ___force, ___mode);
}
// UnityEngine.Bounds UnityEngine.Collider::get_bounds()
extern "C" Bounds_t6_54  Collider_get_bounds_m6_728 (Collider_t6_100 * __this, const MethodInfo* method)
{
	Bounds_t6_54  V_0 = {0};
	{
		Collider_INTERNAL_get_bounds_m6_729(__this, (&V_0), /*hidden argument*/NULL);
		Bounds_t6_54  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Collider::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C" void Collider_INTERNAL_get_bounds_m6_729 (Collider_t6_100 * __this, Bounds_t6_54 * ___value, const MethodInfo* method)
{
	typedef void (*Collider_INTERNAL_get_bounds_m6_729_ftn) (Collider_t6_100 *, Bounds_t6_54 *);
	static Collider_INTERNAL_get_bounds_m6_729_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_INTERNAL_get_bounds_m6_729_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::INTERNAL_get_bounds(UnityEngine.Bounds&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C" Vector3_t6_49  RaycastHit_get_point_m6_730 (RaycastHit_t6_108 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_49  L_0 = (__this->___m_Point_0);
		return L_0;
	}
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C" Collider_t6_100 * RaycastHit_get_collider_m6_731 (RaycastHit_t6_108 * __this, const MethodInfo* method)
{
	{
		Collider_t6_100 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
