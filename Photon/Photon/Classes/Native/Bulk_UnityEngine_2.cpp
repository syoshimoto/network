﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t6_217;
// System.String
struct String_t;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_t6_23;
// UnityEngine.SocialPlatforms.IScore
struct IScore_t6_220;
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t6_218;
// System.String[]
struct StringU5BU5D_t1_202;
// UnityEngine.GUILayer
struct GUILayer_t6_31;
// System.Object
struct Object_t;
// UnityEngine.SliderState
struct SliderState_t6_228;
// UnityEngine.GUIStyle
struct GUIStyle_t6_166;
// UnityEngine.Event
struct Event_t6_154;
struct Event_t6_154_marshaled;
// UnityEngine.StackTraceUtility
struct StackTraceUtility_t6_230;
// System.Diagnostics.StackTrace
struct StackTrace_t1_199;
// UnityEngine.UnityException
struct UnityException_t6_231;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_177;
// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t6_232;
// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_t6_233;
// UnityEngine.TextEditor
struct TextEditor_t6_238;
// UnityEngine.TrackedReference
struct TrackedReference_t6_136;
struct TrackedReference_t6_136_marshaled;
// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t6_239;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t6_242;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6_243;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t6_244;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t6_245;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t6_246;
// UnityEngine.Internal.DefaultValueAttribute
struct DefaultValueAttribute_t6_247;
// UnityEngine.Internal.ExcludeFromDocsAttribute
struct ExcludeFromDocsAttribute_t6_248;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t6_249;
// UnityEngineInternal.TypeInferenceRuleAttribute
struct TypeInferenceRuleAttribute_t6_251;
// UnityEngineInternal.GenericStack
struct GenericStack_t6_162;
// UnityEngine.Advertisements.UnityAdsDelegate
struct UnityAdsDelegate_t6_94;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_RangeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_UInt32.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_SendMouseEvents.h"
#include "UnityEngine_UnityEngine_SendMouseEventsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_GUILayer.h"
#include "UnityEngine_UnityEngine_GUIElement.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_RenderTexture.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScopeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScopeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderState.h"
#include "UnityEngine_UnityEngine_SliderStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderHandler.h"
#include "UnityEngine_UnityEngine_SliderHandlerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemClockMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "mscorlib_System_Double.h"
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent.h"
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset.h"
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
#include "UnityEngine_UnityEngine_StackTraceUtilityMethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTraceMethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackFrame.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_Reflection_ParameterInfo.h"
#include "mscorlib_System_Diagnostics_StackFrameMethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityException.h"
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttributeMethodDeclarations.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "UnityEngine_UnityEngine_SystemClock.h"
#include "mscorlib_System_DateTimeKind.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappingMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterType.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOpMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor.h"
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventModifiers.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_10.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_10MethodDeclarations.h"
#include "mscorlib_System_CharMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "UnityEngine_UnityEngine_Font.h"
#include "UnityEngine_UnityEngine_TrackedReference.h"
#include "UnityEngine_UnityEngine_TrackedReferenceMethodDeclarations.h"
#include "mscorlib_System_IntPtrMethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCacheMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroupMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_11MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_11.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallListMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_12MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_12.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRulesMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
#include "mscorlib_System_Enum.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
#include "mscorlib_System_Collections_StackMethodDeclarations.h"
#include "mscorlib_System_Collections_Stack.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegateMethodDeclarations.h"
#include "mscorlib_System_AsyncCallback.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m6_1655_gshared (Component_t6_26 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m6_1655(__this, method) (( Object_t * (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponent_TisObject_t_m6_1655_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t6_31_m6_1654(__this, method) (( GUILayer_t6_31 * (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponent_TisObject_t_m6_1655_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
extern TypeInfo* DateTime_t1_127_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral140;
extern "C" void Score__ctor_m6_1461 (Score_t6_217 * __this, String_t* ___leaderboardID, int64_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral140 = il2cpp_codegen_string_literal_from_index(140);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID;
		int64_t L_1 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_127_il2cpp_TypeInfo_var);
		DateTime_t1_127  L_2 = DateTime_get_Now_m1_4932(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Score__ctor_m6_1462(__this, L_0, L_1, _stringLiteral140, L_2, L_3, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
extern "C" void Score__ctor_m6_1462 (Score_t6_217 * __this, String_t* ___leaderboardID, int64_t ___value, String_t* ___userID, DateTime_t1_127  ___date, String_t* ___formattedValue, int32_t ___rank, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___leaderboardID;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String) */, __this, L_0);
		int64_t L_1 = ___value;
		VirtActionInvoker1< int64_t >::Invoke(7 /* System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64) */, __this, L_1);
		String_t* L_2 = ___userID;
		__this->___m_UserID_2 = L_2;
		DateTime_t1_127  L_3 = ___date;
		__this->___m_Date_0 = L_3;
		String_t* L_4 = ___formattedValue;
		__this->___m_FormattedValue_1 = L_4;
		int32_t L_5 = ___rank;
		__this->___m_Rank_3 = L_5;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t1_7_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_127_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2835;
extern Il2CppCodeGenString* _stringLiteral2836;
extern Il2CppCodeGenString* _stringLiteral2837;
extern Il2CppCodeGenString* _stringLiteral2838;
extern Il2CppCodeGenString* _stringLiteral2839;
extern "C" String_t* Score_ToString_m6_1463 (Score_t6_217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Int64_t1_7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		DateTime_t1_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2835 = il2cpp_codegen_string_literal_from_index(2835);
		_stringLiteral2836 = il2cpp_codegen_string_literal_from_index(2836);
		_stringLiteral2837 = il2cpp_codegen_string_literal_from_index(2837);
		_stringLiteral2838 = il2cpp_codegen_string_literal_from_index(2838);
		_stringLiteral2839 = il2cpp_codegen_string_literal_from_index(2839);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, ((int32_t)10)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2835);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral2835;
		ObjectU5BU5D_t1_157* L_1 = L_0;
		int32_t L_2 = (__this->___m_Rank_3);
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t1_157* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral2836);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2836;
		ObjectU5BU5D_t1_157* L_6 = L_5;
		int64_t L_7 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(6 /* System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value() */, __this);
		int64_t L_8 = L_7;
		Object_t * L_9 = Box(Int64_t1_7_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_157* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral2837);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral2837;
		ObjectU5BU5D_t1_157* L_11 = L_10;
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID() */, __this);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 5, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t1_157* L_13 = L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, _stringLiteral2838);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral2838;
		ObjectU5BU5D_t1_157* L_14 = L_13;
		String_t* L_15 = (__this->___m_UserID_2);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		ArrayElementTypeCheck (L_14, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 7, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t1_157* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 8);
		ArrayElementTypeCheck (L_16, _stringLiteral2839);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 8, sizeof(Object_t *))) = (Object_t *)_stringLiteral2839;
		ObjectU5BU5D_t1_157* L_17 = L_16;
		DateTime_t1_127  L_18 = (__this->___m_Date_0);
		DateTime_t1_127  L_19 = L_18;
		Object_t * L_20 = Box(DateTime_t1_127_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)9));
		ArrayElementTypeCheck (L_17, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1_421(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
extern "C" String_t* Score_get_leaderboardID_m6_1464 (Score_t6_217 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CleaderboardIDU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
extern "C" void Score_set_leaderboardID_m6_1465 (Score_t6_217 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CleaderboardIDU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
extern "C" int64_t Score_get_value_m6_1466 (Score_t6_217 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->___U3CvalueU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
extern "C" void Score_set_value_m6_1467 (Score_t6_217 * __this, int64_t ___value, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value;
		__this->___U3CvalueU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern TypeInfo* Score_t6_217_il2cpp_TypeInfo_var;
extern TypeInfo* ScoreU5BU5D_t6_269_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t1_202_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2840;
extern "C" void Leaderboard__ctor_m6_1468 (Leaderboard_t6_23 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Score_t6_217_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(924);
		ScoreU5BU5D_t6_269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(923);
		StringU5BU5D_t1_202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(32);
		_stringLiteral2840 = il2cpp_codegen_string_literal_from_index(2840);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(8 /* System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String) */, __this, _stringLiteral2840);
		Range_t6_219  L_0 = {0};
		Range__ctor_m6_1490(&L_0, 1, ((int32_t)10), /*hidden argument*/NULL);
		VirtActionInvoker1< Range_t6_219  >::Invoke(10 /* System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range) */, __this, L_0);
		VirtActionInvoker1< int32_t >::Invoke(9 /* System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope) */, __this, 0);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope) */, __this, 2);
		__this->___m_Loading_0 = 0;
		Score_t6_217 * L_1 = (Score_t6_217 *)il2cpp_codegen_object_new (Score_t6_217_il2cpp_TypeInfo_var);
		Score__ctor_m6_1461(L_1, _stringLiteral2840, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		__this->___m_LocalUserScore_1 = L_1;
		__this->___m_MaxRange_2 = 0;
		__this->___m_Scores_3 = (IScoreU5BU5D_t6_218*)((ScoreU5BU5D_t6_269*)SZArrayNew(ScoreU5BU5D_t6_269_il2cpp_TypeInfo_var, 0));
		__this->___m_Title_4 = _stringLiteral2840;
		__this->___m_UserIDs_5 = ((StringU5BU5D_t1_202*)SZArrayNew(StringU5BU5D_t1_202_il2cpp_TypeInfo_var, 0));
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t1_8_il2cpp_TypeInfo_var;
extern TypeInfo* UserScope_t6_226_il2cpp_TypeInfo_var;
extern TypeInfo* TimeScope_t6_227_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2841;
extern Il2CppCodeGenString* _stringLiteral2842;
extern Il2CppCodeGenString* _stringLiteral2843;
extern Il2CppCodeGenString* _stringLiteral2844;
extern Il2CppCodeGenString* _stringLiteral229;
extern Il2CppCodeGenString* _stringLiteral2845;
extern Il2CppCodeGenString* _stringLiteral2846;
extern Il2CppCodeGenString* _stringLiteral2847;
extern Il2CppCodeGenString* _stringLiteral2848;
extern Il2CppCodeGenString* _stringLiteral2849;
extern "C" String_t* Leaderboard_ToString_m6_1469 (Leaderboard_t6_23 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		UInt32_t1_8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		UserScope_t6_226_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1027);
		TimeScope_t6_227_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2841 = il2cpp_codegen_string_literal_from_index(2841);
		_stringLiteral2842 = il2cpp_codegen_string_literal_from_index(2842);
		_stringLiteral2843 = il2cpp_codegen_string_literal_from_index(2843);
		_stringLiteral2844 = il2cpp_codegen_string_literal_from_index(2844);
		_stringLiteral229 = il2cpp_codegen_string_literal_from_index(229);
		_stringLiteral2845 = il2cpp_codegen_string_literal_from_index(2845);
		_stringLiteral2846 = il2cpp_codegen_string_literal_from_index(2846);
		_stringLiteral2847 = il2cpp_codegen_string_literal_from_index(2847);
		_stringLiteral2848 = il2cpp_codegen_string_literal_from_index(2848);
		_stringLiteral2849 = il2cpp_codegen_string_literal_from_index(2849);
		s_Il2CppMethodIntialized = true;
	}
	Range_t6_219  V_0 = {0};
	Range_t6_219  V_1 = {0};
	{
		ObjectU5BU5D_t1_157* L_0 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, ((int32_t)20)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2841);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral2841;
		ObjectU5BU5D_t1_157* L_1 = L_0;
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id() */, __this);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_2;
		ObjectU5BU5D_t1_157* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral2842);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2842;
		ObjectU5BU5D_t1_157* L_4 = L_3;
		String_t* L_5 = (__this->___m_Title_4);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 3, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_157* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral2843);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral2843;
		ObjectU5BU5D_t1_157* L_7 = L_6;
		bool L_8 = (__this->___m_Loading_0);
		bool L_9 = L_8;
		Object_t * L_10 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 5, sizeof(Object_t *))) = (Object_t *)L_10;
		ObjectU5BU5D_t1_157* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, _stringLiteral2844);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral2844;
		ObjectU5BU5D_t1_157* L_12 = L_11;
		Range_t6_219  L_13 = (Range_t6_219 )VirtFuncInvoker0< Range_t6_219  >::Invoke(6 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range() */, __this);
		V_0 = L_13;
		int32_t L_14 = ((&V_0)->___from_0);
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 7);
		ArrayElementTypeCheck (L_12, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 7, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t1_157* L_17 = L_12;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 8);
		ArrayElementTypeCheck (L_17, _stringLiteral229);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 8, sizeof(Object_t *))) = (Object_t *)_stringLiteral229;
		ObjectU5BU5D_t1_157* L_18 = L_17;
		Range_t6_219  L_19 = (Range_t6_219 )VirtFuncInvoker0< Range_t6_219  >::Invoke(6 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range() */, __this);
		V_1 = L_19;
		int32_t L_20 = ((&V_1)->___count_1);
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		ArrayElementTypeCheck (L_18, L_22);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_22;
		ObjectU5BU5D_t1_157* L_23 = L_18;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)10));
		ArrayElementTypeCheck (L_23, _stringLiteral2845);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)_stringLiteral2845;
		ObjectU5BU5D_t1_157* L_24 = L_23;
		uint32_t L_25 = (__this->___m_MaxRange_2);
		uint32_t L_26 = L_25;
		Object_t * L_27 = Box(UInt32_t1_8_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)11));
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, ((int32_t)11), sizeof(Object_t *))) = (Object_t *)L_27;
		ObjectU5BU5D_t1_157* L_28 = L_24;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)12));
		ArrayElementTypeCheck (L_28, _stringLiteral2846);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, ((int32_t)12), sizeof(Object_t *))) = (Object_t *)_stringLiteral2846;
		ObjectU5BU5D_t1_157* L_29 = L_28;
		IScoreU5BU5D_t6_218* L_30 = (__this->___m_Scores_3);
		NullCheck(L_30);
		int32_t L_31 = (((int32_t)((int32_t)(((Array_t *)L_30)->max_length))));
		Object_t * L_32 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)13));
		ArrayElementTypeCheck (L_29, L_32);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_29, ((int32_t)13), sizeof(Object_t *))) = (Object_t *)L_32;
		ObjectU5BU5D_t1_157* L_33 = L_29;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)14));
		ArrayElementTypeCheck (L_33, _stringLiteral2847);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_33, ((int32_t)14), sizeof(Object_t *))) = (Object_t *)_stringLiteral2847;
		ObjectU5BU5D_t1_157* L_34 = L_33;
		int32_t L_35 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope() */, __this);
		int32_t L_36 = L_35;
		Object_t * L_37 = Box(UserScope_t6_226_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)15));
		ArrayElementTypeCheck (L_34, L_37);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_34, ((int32_t)15), sizeof(Object_t *))) = (Object_t *)L_37;
		ObjectU5BU5D_t1_157* L_38 = L_34;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)16));
		ArrayElementTypeCheck (L_38, _stringLiteral2848);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_38, ((int32_t)16), sizeof(Object_t *))) = (Object_t *)_stringLiteral2848;
		ObjectU5BU5D_t1_157* L_39 = L_38;
		int32_t L_40 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(7 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope() */, __this);
		int32_t L_41 = L_40;
		Object_t * L_42 = Box(TimeScope_t6_227_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)17));
		ArrayElementTypeCheck (L_39, L_42);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_39, ((int32_t)17), sizeof(Object_t *))) = (Object_t *)L_42;
		ObjectU5BU5D_t1_157* L_43 = L_39;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)18));
		ArrayElementTypeCheck (L_43, _stringLiteral2849);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_43, ((int32_t)18), sizeof(Object_t *))) = (Object_t *)_stringLiteral2849;
		ObjectU5BU5D_t1_157* L_44 = L_43;
		StringU5BU5D_t1_202* L_45 = (__this->___m_UserIDs_5);
		NullCheck(L_45);
		int32_t L_46 = (((int32_t)((int32_t)(((Array_t *)L_45)->max_length))));
		Object_t * L_47 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)19));
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, ((int32_t)19), sizeof(Object_t *))) = (Object_t *)L_47;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m1_421(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return L_48;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern "C" void Leaderboard_SetLocalUserScore_m6_1470 (Leaderboard_t6_23 * __this, Object_t * ___score, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___score;
		__this->___m_LocalUserScore_1 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern "C" void Leaderboard_SetMaxRange_m6_1471 (Leaderboard_t6_23 * __this, uint32_t ___maxRange, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___maxRange;
		__this->___m_MaxRange_2 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern "C" void Leaderboard_SetScores_m6_1472 (Leaderboard_t6_23 * __this, IScoreU5BU5D_t6_218* ___scores, const MethodInfo* method)
{
	{
		IScoreU5BU5D_t6_218* L_0 = ___scores;
		__this->___m_Scores_3 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern "C" void Leaderboard_SetTitle_m6_1473 (Leaderboard_t6_23 * __this, String_t* ___title, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title;
		__this->___m_Title_4 = L_0;
		return;
	}
}
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern "C" StringU5BU5D_t1_202* Leaderboard_GetUserFilter_m6_1474 (Leaderboard_t6_23 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_202* L_0 = (__this->___m_UserIDs_5);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern "C" String_t* Leaderboard_get_id_m6_1475 (Leaderboard_t6_23 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CidU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
extern "C" void Leaderboard_set_id_m6_1476 (Leaderboard_t6_23 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CidU3Ek__BackingField_6 = L_0;
		return;
	}
}
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
extern "C" int32_t Leaderboard_get_userScope_m6_1477 (Leaderboard_t6_23 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CuserScopeU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern "C" void Leaderboard_set_userScope_m6_1478 (Leaderboard_t6_23 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CuserScopeU3Ek__BackingField_7 = L_0;
		return;
	}
}
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
extern "C" Range_t6_219  Leaderboard_get_range_m6_1479 (Leaderboard_t6_23 * __this, const MethodInfo* method)
{
	{
		Range_t6_219  L_0 = (__this->___U3CrangeU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern "C" void Leaderboard_set_range_m6_1480 (Leaderboard_t6_23 * __this, Range_t6_219  ___value, const MethodInfo* method)
{
	{
		Range_t6_219  L_0 = ___value;
		__this->___U3CrangeU3Ek__BackingField_8 = L_0;
		return;
	}
}
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
extern "C" int32_t Leaderboard_get_timeScope_m6_1481 (Leaderboard_t6_23 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CtimeScopeU3Ek__BackingField_9);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern "C" void Leaderboard_set_timeScope_m6_1482 (Leaderboard_t6_23 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CtimeScopeU3Ek__BackingField_9 = L_0;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C" void HitInfo_SendMessage_m6_1483 (HitInfo_t6_221 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		GameObject_t6_85 * L_0 = (__this->___target_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		GameObject_SendMessage_m6_600(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C" bool HitInfo_Compare_m6_1484 (Object_t * __this /* static, unused */, HitInfo_t6_221  ___lhs, HitInfo_t6_221  ___rhs, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		GameObject_t6_85 * L_0 = ((&___lhs)->___target_0);
		GameObject_t6_85 * L_1 = ((&___rhs)->___target_0);
		bool L_2 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Camera_t6_75 * L_3 = ((&___lhs)->___camera_1);
		Camera_t6_75 * L_4 = ((&___rhs)->___camera_1);
		bool L_5 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C" bool HitInfo_op_Implicit_m6_1485 (Object_t * __this /* static, unused */, HitInfo_t6_221  ___exists, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		GameObject_t6_85 * L_0 = ((&___exists)->___target_0);
		bool L_1 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Camera_t6_75 * L_2 = ((&___exists)->___camera_1);
		bool L_3 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern TypeInfo* SendMouseEvents_t6_222_il2cpp_TypeInfo_var;
extern TypeInfo* HitInfoU5BU5D_t6_223_il2cpp_TypeInfo_var;
extern TypeInfo* HitInfo_t6_221_il2cpp_TypeInfo_var;
extern "C" void SendMouseEvents__cctor_m6_1486 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SendMouseEvents_t6_222_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		HitInfoU5BU5D_t6_223_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		HitInfo_t6_221_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1031);
		s_Il2CppMethodIntialized = true;
	}
	HitInfo_t6_221  V_0 = {0};
	HitInfo_t6_221  V_1 = {0};
	HitInfo_t6_221  V_2 = {0};
	HitInfo_t6_221  V_3 = {0};
	HitInfo_t6_221  V_4 = {0};
	HitInfo_t6_221  V_5 = {0};
	HitInfo_t6_221  V_6 = {0};
	HitInfo_t6_221  V_7 = {0};
	HitInfo_t6_221  V_8 = {0};
	{
		((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___s_MouseUsed_3 = 0;
		HitInfoU5BU5D_t6_223* L_0 = ((HitInfoU5BU5D_t6_223*)SZArrayNew(HitInfoU5BU5D_t6_223_il2cpp_TypeInfo_var, 3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Initobj (HitInfo_t6_221_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t6_221  L_1 = V_0;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_0, 0, sizeof(HitInfo_t6_221 )))) = L_1;
		HitInfoU5BU5D_t6_223* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Initobj (HitInfo_t6_221_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t6_221  L_3 = V_1;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_2, 1, sizeof(HitInfo_t6_221 )))) = L_3;
		HitInfoU5BU5D_t6_223* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Initobj (HitInfo_t6_221_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t6_221  L_5 = V_2;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_4, 2, sizeof(HitInfo_t6_221 )))) = L_5;
		((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_4 = L_4;
		HitInfoU5BU5D_t6_223* L_6 = ((HitInfoU5BU5D_t6_223*)SZArrayNew(HitInfoU5BU5D_t6_223_il2cpp_TypeInfo_var, 3));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		Initobj (HitInfo_t6_221_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t6_221  L_7 = V_3;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_6, 0, sizeof(HitInfo_t6_221 )))) = L_7;
		HitInfoU5BU5D_t6_223* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		Initobj (HitInfo_t6_221_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t6_221  L_9 = V_4;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_8, 1, sizeof(HitInfo_t6_221 )))) = L_9;
		HitInfoU5BU5D_t6_223* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		Initobj (HitInfo_t6_221_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t6_221  L_11 = V_5;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_10, 2, sizeof(HitInfo_t6_221 )))) = L_11;
		((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5 = L_10;
		HitInfoU5BU5D_t6_223* L_12 = ((HitInfoU5BU5D_t6_223*)SZArrayNew(HitInfoU5BU5D_t6_223_il2cpp_TypeInfo_var, 3));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		Initobj (HitInfo_t6_221_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t6_221  L_13 = V_6;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_12, 0, sizeof(HitInfo_t6_221 )))) = L_13;
		HitInfoU5BU5D_t6_223* L_14 = L_12;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		Initobj (HitInfo_t6_221_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t6_221  L_15 = V_7;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_14, 1, sizeof(HitInfo_t6_221 )))) = L_15;
		HitInfoU5BU5D_t6_223* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		Initobj (HitInfo_t6_221_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t6_221  L_17 = V_8;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_16, 2, sizeof(HitInfo_t6_221 )))) = L_17;
		((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6 = L_16;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern TypeInfo* SendMouseEvents_t6_222_il2cpp_TypeInfo_var;
extern "C" void SendMouseEvents_SetMouseMoved_m6_1487 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SendMouseEvents_t6_222_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___s_MouseUsed_3 = 1;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern TypeInfo* SendMouseEvents_t6_222_il2cpp_TypeInfo_var;
extern TypeInfo* CameraU5BU5D_t6_224_il2cpp_TypeInfo_var;
extern TypeInfo* HitInfo_t6_221_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUILayer_t6_31_m6_1654_MethodInfo_var;
extern "C" void SendMouseEvents_DoSendMouseEvents_m6_1488 (Object_t * __this /* static, unused */, int32_t ___skipRTCameras, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		SendMouseEvents_t6_222_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		CameraU5BU5D_t6_224_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1033);
		HitInfo_t6_221_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1031);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		Component_GetComponent_TisGUILayer_t6_31_m6_1654_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_49  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Camera_t6_75 * V_3 = {0};
	CameraU5BU5D_t6_224* V_4 = {0};
	int32_t V_5 = 0;
	Rect_t6_52  V_6 = {0};
	GUILayer_t6_31 * V_7 = {0};
	GUIElement_t6_29 * V_8 = {0};
	Ray_t6_56  V_9 = {0};
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	GameObject_t6_85 * V_12 = {0};
	GameObject_t6_85 * V_13 = {0};
	int32_t V_14 = 0;
	HitInfo_t6_221  V_15 = {0};
	Vector3_t6_49  V_16 = {0};
	float G_B23_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		Vector3_t6_49  L_0 = Input_get_mousePosition_m6_546(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m6_476(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		CameraU5BU5D_t6_224* L_2 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		CameraU5BU5D_t6_224* L_3 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7;
		NullCheck(L_3);
		int32_t L_4 = V_1;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_002e;
		}
	}

IL_0023:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7 = ((CameraU5BU5D_t6_224*)SZArrayNew(CameraU5BU5D_t6_224_il2cpp_TypeInfo_var, L_5));
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		CameraU5BU5D_t6_224* L_6 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7;
		Camera_GetAllCameras_m6_477(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_005e;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_7 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Initobj (HitInfo_t6_221_il2cpp_TypeInfo_var, (&V_15));
		HitInfo_t6_221  L_9 = V_15;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_7, L_8, sizeof(HitInfo_t6_221 )))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_12 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		bool L_13 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___s_MouseUsed_3;
		if (L_13)
		{
			goto IL_02c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		CameraU5BU5D_t6_224* L_14 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7;
		V_4 = L_14;
		V_5 = 0;
		goto IL_02b8;
	}

IL_0084:
	{
		CameraU5BU5D_t6_224* L_15 = V_4;
		int32_t L_16 = V_5;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		V_3 = (*(Camera_t6_75 **)(Camera_t6_75 **)SZArrayLdElema(L_15, L_17, sizeof(Camera_t6_75 *)));
		Camera_t6_75 * L_18 = V_3;
		bool L_19 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_18, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_20 = ___skipRTCameras;
		if (!L_20)
		{
			goto IL_00b2;
		}
	}
	{
		Camera_t6_75 * L_21 = V_3;
		NullCheck(L_21);
		RenderTexture_t6_34 * L_22 = Camera_get_targetTexture_m6_469(L_21, /*hidden argument*/NULL);
		bool L_23 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_22, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00b2;
		}
	}

IL_00ad:
	{
		goto IL_02b2;
	}

IL_00b2:
	{
		Camera_t6_75 * L_24 = V_3;
		NullCheck(L_24);
		Rect_t6_52  L_25 = Camera_get_pixelRect_m6_467(L_24, /*hidden argument*/NULL);
		V_6 = L_25;
		Vector3_t6_49  L_26 = V_0;
		bool L_27 = Rect_Contains_m6_287((&V_6), L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00cc;
		}
	}
	{
		goto IL_02b2;
	}

IL_00cc:
	{
		Camera_t6_75 * L_28 = V_3;
		NullCheck(L_28);
		GUILayer_t6_31 * L_29 = Component_GetComponent_TisGUILayer_t6_31_m6_1654(L_28, /*hidden argument*/Component_GetComponent_TisGUILayer_t6_31_m6_1654_MethodInfo_var);
		V_7 = L_29;
		GUILayer_t6_31 * L_30 = V_7;
		bool L_31 = Object_op_Implicit_m6_578(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0145;
		}
	}
	{
		GUILayer_t6_31 * L_32 = V_7;
		Vector3_t6_49  L_33 = V_0;
		NullCheck(L_32);
		GUIElement_t6_29 * L_34 = GUILayer_HitTest_m6_123(L_32, L_33, /*hidden argument*/NULL);
		V_8 = L_34;
		GUIElement_t6_29 * L_35 = V_8;
		bool L_36 = Object_op_Implicit_m6_578(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0123;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_37 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 0);
		GUIElement_t6_29 * L_38 = V_8;
		NullCheck(L_38);
		GameObject_t6_85 * L_39 = Component_get_gameObject_m6_583(L_38, /*hidden argument*/NULL);
		((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_37, 0, sizeof(HitInfo_t6_221 )))->___target_0 = L_39;
		HitInfoU5BU5D_t6_223* L_40 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 0);
		Camera_t6_75 * L_41 = V_3;
		((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_40, 0, sizeof(HitInfo_t6_221 )))->___camera_1 = L_41;
		goto IL_0145;
	}

IL_0123:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_42 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 0);
		((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_42, 0, sizeof(HitInfo_t6_221 )))->___target_0 = (GameObject_t6_85 *)NULL;
		HitInfoU5BU5D_t6_223* L_43 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_43, 0, sizeof(HitInfo_t6_221 )))->___camera_1 = (Camera_t6_75 *)NULL;
	}

IL_0145:
	{
		Camera_t6_75 * L_44 = V_3;
		NullCheck(L_44);
		int32_t L_45 = Camera_get_eventMask_m6_466(L_44, /*hidden argument*/NULL);
		if (L_45)
		{
			goto IL_0155;
		}
	}
	{
		goto IL_02b2;
	}

IL_0155:
	{
		Camera_t6_75 * L_46 = V_3;
		Vector3_t6_49  L_47 = V_0;
		NullCheck(L_46);
		Ray_t6_56  L_48 = Camera_ScreenPointToRay_m6_473(L_46, L_47, /*hidden argument*/NULL);
		V_9 = L_48;
		Vector3_t6_49  L_49 = Ray_get_direction_m6_373((&V_9), /*hidden argument*/NULL);
		V_16 = L_49;
		float L_50 = ((&V_16)->___z_3);
		V_10 = L_50;
		float L_51 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_52 = Mathf_Approximately_m6_394(NULL /*static, unused*/, (0.0f), L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_018b;
		}
	}
	{
		G_B23_0 = (std::numeric_limits<float>::infinity());
		goto IL_01a0;
	}

IL_018b:
	{
		Camera_t6_75 * L_53 = V_3;
		NullCheck(L_53);
		float L_54 = Camera_get_farClipPlane_m6_464(L_53, /*hidden argument*/NULL);
		Camera_t6_75 * L_55 = V_3;
		NullCheck(L_55);
		float L_56 = Camera_get_nearClipPlane_m6_463(L_55, /*hidden argument*/NULL);
		float L_57 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_58 = fabsf(((float)((float)((float)((float)L_54-(float)L_56))/(float)L_57)));
		G_B23_0 = L_58;
	}

IL_01a0:
	{
		V_11 = G_B23_0;
		Camera_t6_75 * L_59 = V_3;
		Ray_t6_56  L_60 = V_9;
		float L_61 = V_11;
		Camera_t6_75 * L_62 = V_3;
		NullCheck(L_62);
		int32_t L_63 = Camera_get_cullingMask_m6_465(L_62, /*hidden argument*/NULL);
		Camera_t6_75 * L_64 = V_3;
		NullCheck(L_64);
		int32_t L_65 = Camera_get_eventMask_m6_466(L_64, /*hidden argument*/NULL);
		NullCheck(L_59);
		GameObject_t6_85 * L_66 = Camera_RaycastTry_m6_481(L_59, L_60, L_61, ((int32_t)((int32_t)L_63&(int32_t)L_65)), /*hidden argument*/NULL);
		V_12 = L_66;
		GameObject_t6_85 * L_67 = V_12;
		bool L_68 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_67, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_01f0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_69 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, 1);
		GameObject_t6_85 * L_70 = V_12;
		((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_69, 1, sizeof(HitInfo_t6_221 )))->___target_0 = L_70;
		HitInfoU5BU5D_t6_223* L_71 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, 1);
		Camera_t6_75 * L_72 = V_3;
		((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_71, 1, sizeof(HitInfo_t6_221 )))->___camera_1 = L_72;
		goto IL_022a;
	}

IL_01f0:
	{
		Camera_t6_75 * L_73 = V_3;
		NullCheck(L_73);
		int32_t L_74 = Camera_get_clearFlags_m6_470(L_73, /*hidden argument*/NULL);
		if ((((int32_t)L_74) == ((int32_t)1)))
		{
			goto IL_0208;
		}
	}
	{
		Camera_t6_75 * L_75 = V_3;
		NullCheck(L_75);
		int32_t L_76 = Camera_get_clearFlags_m6_470(L_75, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_76) == ((uint32_t)2))))
		{
			goto IL_022a;
		}
	}

IL_0208:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_77 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, 1);
		((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_77, 1, sizeof(HitInfo_t6_221 )))->___target_0 = (GameObject_t6_85 *)NULL;
		HitInfoU5BU5D_t6_223* L_78 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, 1);
		((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_78, 1, sizeof(HitInfo_t6_221 )))->___camera_1 = (Camera_t6_75 *)NULL;
	}

IL_022a:
	{
		Camera_t6_75 * L_79 = V_3;
		Ray_t6_56  L_80 = V_9;
		float L_81 = V_11;
		Camera_t6_75 * L_82 = V_3;
		NullCheck(L_82);
		int32_t L_83 = Camera_get_cullingMask_m6_465(L_82, /*hidden argument*/NULL);
		Camera_t6_75 * L_84 = V_3;
		NullCheck(L_84);
		int32_t L_85 = Camera_get_eventMask_m6_466(L_84, /*hidden argument*/NULL);
		NullCheck(L_79);
		GameObject_t6_85 * L_86 = Camera_RaycastTry2D_m6_483(L_79, L_80, L_81, ((int32_t)((int32_t)L_83&(int32_t)L_85)), /*hidden argument*/NULL);
		V_13 = L_86;
		GameObject_t6_85 * L_87 = V_13;
		bool L_88 = Object_op_Inequality_m6_580(NULL /*static, unused*/, L_87, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_88)
		{
			goto IL_0278;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_89 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, 2);
		GameObject_t6_85 * L_90 = V_13;
		((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_89, 2, sizeof(HitInfo_t6_221 )))->___target_0 = L_90;
		HitInfoU5BU5D_t6_223* L_91 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, 2);
		Camera_t6_75 * L_92 = V_3;
		((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_91, 2, sizeof(HitInfo_t6_221 )))->___camera_1 = L_92;
		goto IL_02b2;
	}

IL_0278:
	{
		Camera_t6_75 * L_93 = V_3;
		NullCheck(L_93);
		int32_t L_94 = Camera_get_clearFlags_m6_470(L_93, /*hidden argument*/NULL);
		if ((((int32_t)L_94) == ((int32_t)1)))
		{
			goto IL_0290;
		}
	}
	{
		Camera_t6_75 * L_95 = V_3;
		NullCheck(L_95);
		int32_t L_96 = Camera_get_clearFlags_m6_470(L_95, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_96) == ((uint32_t)2))))
		{
			goto IL_02b2;
		}
	}

IL_0290:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_97 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, 2);
		((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_97, 2, sizeof(HitInfo_t6_221 )))->___target_0 = (GameObject_t6_85 *)NULL;
		HitInfoU5BU5D_t6_223* L_98 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, 2);
		((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_98, 2, sizeof(HitInfo_t6_221 )))->___camera_1 = (Camera_t6_75 *)NULL;
	}

IL_02b2:
	{
		int32_t L_99 = V_5;
		V_5 = ((int32_t)((int32_t)L_99+(int32_t)1));
	}

IL_02b8:
	{
		int32_t L_100 = V_5;
		CameraU5BU5D_t6_224* L_101 = V_4;
		NullCheck(L_101);
		if ((((int32_t)L_100) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_101)->max_length)))))))
		{
			goto IL_0084;
		}
	}

IL_02c3:
	{
		V_14 = 0;
		goto IL_02e9;
	}

IL_02cb:
	{
		int32_t L_102 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_103 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		int32_t L_104 = V_14;
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, L_104);
		SendMouseEvents_SendEvents_m6_1489(NULL /*static, unused*/, L_102, (*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_103, L_104, sizeof(HitInfo_t6_221 )))), /*hidden argument*/NULL);
		int32_t L_105 = V_14;
		V_14 = ((int32_t)((int32_t)L_105+(int32_t)1));
	}

IL_02e9:
	{
		int32_t L_106 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_107 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_107);
		if ((((int32_t)L_106) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_107)->max_length)))))))
		{
			goto IL_02cb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___s_MouseUsed_3 = 0;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern TypeInfo* SendMouseEvents_t6_222_il2cpp_TypeInfo_var;
extern TypeInfo* HitInfo_t6_221_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2850;
extern Il2CppCodeGenString* _stringLiteral2851;
extern Il2CppCodeGenString* _stringLiteral2852;
extern Il2CppCodeGenString* _stringLiteral2853;
extern Il2CppCodeGenString* _stringLiteral2854;
extern Il2CppCodeGenString* _stringLiteral2855;
extern Il2CppCodeGenString* _stringLiteral2856;
extern "C" void SendMouseEvents_SendEvents_m6_1489 (Object_t * __this /* static, unused */, int32_t ___i, HitInfo_t6_221  ___hit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		SendMouseEvents_t6_222_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		HitInfo_t6_221_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1031);
		_stringLiteral2850 = il2cpp_codegen_string_literal_from_index(2850);
		_stringLiteral2851 = il2cpp_codegen_string_literal_from_index(2851);
		_stringLiteral2852 = il2cpp_codegen_string_literal_from_index(2852);
		_stringLiteral2853 = il2cpp_codegen_string_literal_from_index(2853);
		_stringLiteral2854 = il2cpp_codegen_string_literal_from_index(2854);
		_stringLiteral2855 = il2cpp_codegen_string_literal_from_index(2855);
		_stringLiteral2856 = il2cpp_codegen_string_literal_from_index(2856);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t6_221  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m6_544(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m6_543(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		HitInfo_t6_221  L_3 = ___hit;
		bool L_4 = HitInfo_op_Implicit_m6_1485(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_5 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_6 = ___i;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		HitInfo_t6_221  L_7 = ___hit;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_5, L_6, sizeof(HitInfo_t6_221 )))) = L_7;
		HitInfoU5BU5D_t6_223* L_8 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_9 = ___i;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		HitInfo_SendMessage_m6_1483(((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_8, L_9, sizeof(HitInfo_t6_221 ))), _stringLiteral2850, /*hidden argument*/NULL);
	}

IL_0045:
	{
		goto IL_00fc;
	}

IL_004a:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00cd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_11 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_12 = ___i;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		bool L_13 = HitInfo_op_Implicit_m6_1485(NULL /*static, unused*/, (*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_11, L_12, sizeof(HitInfo_t6_221 )))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00c8;
		}
	}
	{
		HitInfo_t6_221  L_14 = ___hit;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_15 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_16 = ___i;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		bool L_17 = HitInfo_Compare_m6_1484(NULL /*static, unused*/, L_14, (*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_15, L_16, sizeof(HitInfo_t6_221 )))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_18 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_19 = ___i;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		HitInfo_SendMessage_m6_1483(((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_18, L_19, sizeof(HitInfo_t6_221 ))), _stringLiteral2851, /*hidden argument*/NULL);
	}

IL_009a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_20 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_21 = ___i;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		HitInfo_SendMessage_m6_1483(((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_20, L_21, sizeof(HitInfo_t6_221 ))), _stringLiteral2852, /*hidden argument*/NULL);
		HitInfoU5BU5D_t6_223* L_22 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_23 = ___i;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		Initobj (HitInfo_t6_221_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t6_221  L_24 = V_2;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_22, L_23, sizeof(HitInfo_t6_221 )))) = L_24;
	}

IL_00c8:
	{
		goto IL_00fc;
	}

IL_00cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_25 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_26 = ___i;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		bool L_27 = HitInfo_op_Implicit_m6_1485(NULL /*static, unused*/, (*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_25, L_26, sizeof(HitInfo_t6_221 )))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_28 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_29 = ___i;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		HitInfo_SendMessage_m6_1483(((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_28, L_29, sizeof(HitInfo_t6_221 ))), _stringLiteral2853, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		HitInfo_t6_221  L_30 = ___hit;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_31 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_4;
		int32_t L_32 = ___i;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		bool L_33 = HitInfo_Compare_m6_1484(NULL /*static, unused*/, L_30, (*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_31, L_32, sizeof(HitInfo_t6_221 )))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0133;
		}
	}
	{
		HitInfo_t6_221  L_34 = ___hit;
		bool L_35 = HitInfo_op_Implicit_m6_1485(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_012e;
		}
	}
	{
		HitInfo_SendMessage_m6_1483((&___hit), _stringLiteral2854, /*hidden argument*/NULL);
	}

IL_012e:
	{
		goto IL_0185;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_36 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_4;
		int32_t L_37 = ___i;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		bool L_38 = HitInfo_op_Implicit_m6_1485(NULL /*static, unused*/, (*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_36, L_37, sizeof(HitInfo_t6_221 )))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0162;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_39 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_4;
		int32_t L_40 = ___i;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		HitInfo_SendMessage_m6_1483(((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_39, L_40, sizeof(HitInfo_t6_221 ))), _stringLiteral2855, /*hidden argument*/NULL);
	}

IL_0162:
	{
		HitInfo_t6_221  L_41 = ___hit;
		bool L_42 = HitInfo_op_Implicit_m6_1485(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0185;
		}
	}
	{
		HitInfo_SendMessage_m6_1483((&___hit), _stringLiteral2856, /*hidden argument*/NULL);
		HitInfo_SendMessage_m6_1483((&___hit), _stringLiteral2854, /*hidden argument*/NULL);
	}

IL_0185:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_222_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_223* L_43 = ((SendMouseEvents_t6_222_StaticFields*)SendMouseEvents_t6_222_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_4;
		int32_t L_44 = ___i;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		HitInfo_t6_221  L_45 = ___hit;
		(*(HitInfo_t6_221 *)((HitInfo_t6_221 *)(HitInfo_t6_221 *)SZArrayLdElema(L_43, L_44, sizeof(HitInfo_t6_221 )))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
extern "C" void Range__ctor_m6_1490 (Range_t6_219 * __this, int32_t ___fromValue, int32_t ___valueCount, const MethodInfo* method)
{
	{
		int32_t L_0 = ___fromValue;
		__this->___from_0 = L_0;
		int32_t L_1 = ___valueCount;
		__this->___count_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.SliderState::.ctor()
extern "C" void SliderState__ctor_m6_1491 (SliderState_t6_228 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SliderHandler::.ctor(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean,System.Int32)
extern "C" void SliderHandler__ctor_m6_1492 (SliderHandler_t6_229 * __this, Rect_t6_52  ___position, float ___currentValue, float ___size, float ___start, float ___end, GUIStyle_t6_166 * ___slider, GUIStyle_t6_166 * ___thumb, bool ___horiz, int32_t ___id, const MethodInfo* method)
{
	{
		Rect_t6_52  L_0 = ___position;
		__this->___position_0 = L_0;
		float L_1 = ___currentValue;
		__this->___currentValue_1 = L_1;
		float L_2 = ___size;
		__this->___size_2 = L_2;
		float L_3 = ___start;
		__this->___start_3 = L_3;
		float L_4 = ___end;
		__this->___end_4 = L_4;
		GUIStyle_t6_166 * L_5 = ___slider;
		__this->___slider_5 = L_5;
		GUIStyle_t6_166 * L_6 = ___thumb;
		__this->___thumb_6 = L_6;
		bool L_7 = ___horiz;
		__this->___horiz_7 = L_7;
		int32_t L_8 = ___id;
		__this->___id_8 = L_8;
		return;
	}
}
// System.Single UnityEngine.SliderHandler::Handle()
extern "C" float SliderHandler_Handle_m6_1493 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		GUIStyle_t6_166 * L_0 = (__this->___slider_5);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GUIStyle_t6_166 * L_1 = (__this->___thumb_6);
		if (L_1)
		{
			goto IL_001d;
		}
	}

IL_0016:
	{
		float L_2 = (__this->___currentValue_1);
		return L_2;
	}

IL_001d:
	{
		int32_t L_3 = SliderHandler_CurrentEventType_m6_1498(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4 == 0)
		{
			goto IL_004f;
		}
		if (L_4 == 1)
		{
			goto IL_005d;
		}
		if (L_4 == 2)
		{
			goto IL_006b;
		}
		if (L_4 == 3)
		{
			goto IL_0056;
		}
		if (L_4 == 4)
		{
			goto IL_006b;
		}
		if (L_4 == 5)
		{
			goto IL_006b;
		}
		if (L_4 == 6)
		{
			goto IL_006b;
		}
		if (L_4 == 7)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_006b;
	}

IL_004f:
	{
		float L_5 = SliderHandler_OnMouseDown_m6_1494(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0056:
	{
		float L_6 = SliderHandler_OnMouseDrag_m6_1495(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_005d:
	{
		float L_7 = SliderHandler_OnMouseUp_m6_1496(__this, /*hidden argument*/NULL);
		return L_7;
	}

IL_0064:
	{
		float L_8 = SliderHandler_OnRepaint_m6_1497(__this, /*hidden argument*/NULL);
		return L_8;
	}

IL_006b:
	{
		float L_9 = (__this->___currentValue_1);
		return L_9;
	}
}
// System.Single UnityEngine.SliderHandler::OnMouseDown()
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* SystemClock_t6_234_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_OnMouseDown_m6_1494 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		SystemClock_t6_234_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1034);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Rect_t6_52  V_1 = {0};
	Rect_t6_52  V_2 = {0};
	DateTime_t1_127  V_3 = {0};
	{
		Rect_t6_52  L_0 = (__this->___position_0);
		V_1 = L_0;
		Event_t6_154 * L_1 = SliderHandler_CurrentEvent_m6_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t6_48  L_2 = Event_get_mousePosition_m6_970(L_1, /*hidden argument*/NULL);
		bool L_3 = Rect_Contains_m6_286((&V_1), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		bool L_4 = SliderHandler_IsEmptySlider_m6_1500(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}

IL_0029:
	{
		float L_5 = (__this->___currentValue_1);
		return L_5;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_scrollTroughSide_m6_1039(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		int32_t L_6 = (__this->___id_8);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1371(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Event_t6_154 * L_7 = SliderHandler_CurrentEvent_m6_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m6_1027(L_7, /*hidden argument*/NULL);
		Rect_t6_52  L_8 = SliderHandler_ThumbSelectionRect_m6_1507(__this, /*hidden argument*/NULL);
		V_2 = L_8;
		Event_t6_154 * L_9 = SliderHandler_CurrentEvent_m6_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector2_t6_48  L_10 = Event_get_mousePosition_m6_970(L_9, /*hidden argument*/NULL);
		bool L_11 = Rect_Contains_m6_286((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_007d;
		}
	}
	{
		float L_12 = SliderHandler_ClampedCurrentValue_m6_1513(__this, /*hidden argument*/NULL);
		SliderHandler_StartDraggingWithValue_m6_1508(__this, L_12, /*hidden argument*/NULL);
		float L_13 = (__this->___currentValue_1);
		return L_13;
	}

IL_007d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_changed_m6_1081(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		bool L_14 = SliderHandler_SupportsPageMovements_m6_1501(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00c7;
		}
	}
	{
		SliderState_t6_228 * L_15 = SliderHandler_SliderState_m6_1509(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->___isDragging_2 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t6_234_il2cpp_TypeInfo_var);
		DateTime_t1_127  L_16 = SystemClock_get_now_m6_1535(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_16;
		DateTime_t1_127  L_17 = DateTime_AddMilliseconds_m1_4940((&V_3), (250.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m6_1037(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		int32_t L_18 = SliderHandler_CurrentScrollTroughSide_m6_1499(__this, /*hidden argument*/NULL);
		GUI_set_scrollTroughSide_m6_1039(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		float L_19 = SliderHandler_PageMovementValue_m6_1502(__this, /*hidden argument*/NULL);
		return L_19;
	}

IL_00c7:
	{
		float L_20 = SliderHandler_ValueForCurrentMousePosition_m6_1505(__this, /*hidden argument*/NULL);
		V_0 = L_20;
		float L_21 = V_0;
		SliderHandler_StartDraggingWithValue_m6_1508(__this, L_21, /*hidden argument*/NULL);
		float L_22 = V_0;
		float L_23 = SliderHandler_Clamp_m6_1506(__this, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Single UnityEngine.SliderHandler::OnMouseDrag()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_OnMouseDrag_m6_1495 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	SliderState_t6_228 * V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___id_8);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0017;
		}
	}
	{
		float L_2 = (__this->___currentValue_1);
		return L_2;
	}

IL_0017:
	{
		SliderState_t6_228 * L_3 = SliderHandler_SliderState_m6_1509(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		SliderState_t6_228 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = (L_4->___isDragging_2);
		if (L_5)
		{
			goto IL_0030;
		}
	}
	{
		float L_6 = (__this->___currentValue_1);
		return L_6;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_changed_m6_1081(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		Event_t6_154 * L_7 = SliderHandler_CurrentEvent_m6_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m6_1027(L_7, /*hidden argument*/NULL);
		float L_8 = SliderHandler_MousePosition_m6_1514(__this, /*hidden argument*/NULL);
		SliderState_t6_228 * L_9 = V_0;
		NullCheck(L_9);
		float L_10 = (L_9->___dragStartPos_0);
		V_1 = ((float)((float)L_8-(float)L_10));
		SliderState_t6_228 * L_11 = V_0;
		NullCheck(L_11);
		float L_12 = (L_11->___dragStartValue_1);
		float L_13 = V_1;
		float L_14 = SliderHandler_ValuesPerPixel_m6_1515(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_12+(float)((float)((float)L_13/(float)L_14))));
		float L_15 = V_2;
		float L_16 = SliderHandler_Clamp_m6_1506(__this, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Single UnityEngine.SliderHandler::OnMouseUp()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_OnMouseUp_m6_1496 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___id_8);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0021;
		}
	}
	{
		Event_t6_154 * L_2 = SliderHandler_CurrentEvent_m6_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Event_Use_m6_1027(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1371(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0021:
	{
		float L_3 = (__this->___currentValue_1);
		return L_3;
	}
}
// System.Single UnityEngine.SliderHandler::OnRepaint()
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern TypeInfo* SystemClock_t6_234_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_127_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_OnRepaint_m6_1497 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		SystemClock_t6_234_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1034);
		DateTime_t1_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t6_52  V_0 = {0};
	Rect_t6_52  V_1 = {0};
	DateTime_t1_127  V_2 = {0};
	{
		GUIStyle_t6_166 * L_0 = (__this->___slider_5);
		Rect_t6_52  L_1 = (__this->___position_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_2 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___none_6;
		int32_t L_3 = (__this->___id_8);
		NullCheck(L_0);
		GUIStyle_Draw_m6_1311(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		bool L_4 = SliderHandler_IsEmptySlider_m6_1500(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		GUIStyle_t6_166 * L_5 = (__this->___thumb_6);
		Rect_t6_52  L_6 = SliderHandler_ThumbRect_m6_1510(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent_t6_163 * L_7 = ((GUIContent_t6_163_StaticFields*)GUIContent_t6_163_il2cpp_TypeInfo_var->static_fields)->___none_6;
		int32_t L_8 = (__this->___id_8);
		NullCheck(L_5);
		GUIStyle_Draw_m6_1311(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_9 = GUIUtility_get_hotControl_m6_1370(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = (__this->___id_8);
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_007c;
		}
	}
	{
		Rect_t6_52  L_11 = (__this->___position_0);
		V_0 = L_11;
		Event_t6_154 * L_12 = SliderHandler_CurrentEvent_m6_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector2_t6_48  L_13 = Event_get_mousePosition_m6_970(L_12, /*hidden argument*/NULL);
		bool L_14 = Rect_Contains_m6_286((&V_0), L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007c;
		}
	}
	{
		bool L_15 = SliderHandler_IsEmptySlider_m6_1500(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0083;
		}
	}

IL_007c:
	{
		float L_16 = (__this->___currentValue_1);
		return L_16;
	}

IL_0083:
	{
		Rect_t6_52  L_17 = SliderHandler_ThumbRect_m6_1510(__this, /*hidden argument*/NULL);
		V_1 = L_17;
		Event_t6_154 * L_18 = SliderHandler_CurrentEvent_m6_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector2_t6_48  L_19 = Event_get_mousePosition_m6_970(L_18, /*hidden argument*/NULL);
		bool L_20 = Rect_Contains_m6_286((&V_1), L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_21 = GUI_get_scrollTroughSide_m6_1038(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1371(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		float L_22 = (__this->___currentValue_1);
		return L_22;
	}

IL_00b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_InternalRepaintEditorWindow_m6_1094(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t6_234_il2cpp_TypeInfo_var);
		DateTime_t1_127  L_23 = SystemClock_get_now_m6_1535(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t1_127  L_24 = GUI_get_nextScrollStepTime_m6_1036(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_127_il2cpp_TypeInfo_var);
		bool L_25 = DateTime_op_LessThan_m1_4980(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d8;
		}
	}
	{
		float L_26 = (__this->___currentValue_1);
		return L_26;
	}

IL_00d8:
	{
		int32_t L_27 = SliderHandler_CurrentScrollTroughSide_m6_1499(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		int32_t L_28 = GUI_get_scrollTroughSide_m6_1038(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_27) == ((int32_t)L_28)))
		{
			goto IL_00ef;
		}
	}
	{
		float L_29 = (__this->___currentValue_1);
		return L_29;
	}

IL_00ef:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t6_234_il2cpp_TypeInfo_var);
		DateTime_t1_127  L_30 = SystemClock_get_now_m6_1535(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_30;
		DateTime_t1_127  L_31 = DateTime_AddMilliseconds_m1_4940((&V_2), (30.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m6_1037(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		bool L_32 = SliderHandler_SupportsPageMovements_m6_1501(__this, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_012e;
		}
	}
	{
		SliderState_t6_228 * L_33 = SliderHandler_SliderState_m6_1509(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		L_33->___isDragging_2 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		GUI_set_changed_m6_1081(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		float L_34 = SliderHandler_PageMovementValue_m6_1502(__this, /*hidden argument*/NULL);
		return L_34;
	}

IL_012e:
	{
		float L_35 = SliderHandler_ClampedCurrentValue_m6_1513(__this, /*hidden argument*/NULL);
		return L_35;
	}
}
// UnityEngine.EventType UnityEngine.SliderHandler::CurrentEventType()
extern "C" int32_t SliderHandler_CurrentEventType_m6_1498 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	{
		Event_t6_154 * L_0 = SliderHandler_CurrentEvent_m6_1504(__this, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___id_8);
		NullCheck(L_0);
		int32_t L_2 = Event_GetTypeForControl_m6_1005(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.SliderHandler::CurrentScrollTroughSide()
extern "C" int32_t SliderHandler_CurrentScrollTroughSide_m6_1499 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector2_t6_48  V_2 = {0};
	Vector2_t6_48  V_3 = {0};
	Rect_t6_52  V_4 = {0};
	Rect_t6_52  V_5 = {0};
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	int32_t G_B9_0 = 0;
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		Event_t6_154 * L_1 = SliderHandler_CurrentEvent_m6_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t6_48  L_2 = Event_get_mousePosition_m6_970(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = ((&V_2)->___x_1);
		G_B3_0 = L_3;
		goto IL_0036;
	}

IL_0023:
	{
		Event_t6_154 * L_4 = SliderHandler_CurrentEvent_m6_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector2_t6_48  L_5 = Event_get_mousePosition_m6_970(L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = ((&V_3)->___y_2);
		G_B3_0 = L_6;
	}

IL_0036:
	{
		V_0 = G_B3_0;
		bool L_7 = (__this->___horiz_7);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		Rect_t6_52  L_8 = SliderHandler_ThumbRect_m6_1510(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_x_m6_273((&V_4), /*hidden argument*/NULL);
		G_B6_0 = L_9;
		goto IL_0065;
	}

IL_0056:
	{
		Rect_t6_52  L_10 = SliderHandler_ThumbRect_m6_1510(__this, /*hidden argument*/NULL);
		V_5 = L_10;
		float L_11 = Rect_get_y_m6_275((&V_5), /*hidden argument*/NULL);
		G_B6_0 = L_11;
	}

IL_0065:
	{
		V_1 = G_B6_0;
		float L_12 = V_0;
		float L_13 = V_1;
		if ((!(((float)L_12) > ((float)L_13))))
		{
			goto IL_0073;
		}
	}
	{
		G_B9_0 = 1;
		goto IL_0074;
	}

IL_0073:
	{
		G_B9_0 = (-1);
	}

IL_0074:
	{
		return G_B9_0;
	}
}
// System.Boolean UnityEngine.SliderHandler::IsEmptySlider()
extern "C" bool SliderHandler_IsEmptySlider_m6_1500 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___start_3);
		float L_1 = (__this->___end_4);
		return ((((float)L_0) == ((float)L_1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.SliderHandler::SupportsPageMovements()
extern TypeInfo* GUI_t6_160_il2cpp_TypeInfo_var;
extern "C" bool SliderHandler_SupportsPageMovements_m6_1501 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		float L_0 = (__this->___size_2);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_160_il2cpp_TypeInfo_var);
		bool L_1 = GUI_get_usePageScrollbars_m6_1093(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.SliderHandler::PageMovementValue()
extern "C" float SliderHandler_PageMovementValue_m6_1502 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		float L_0 = (__this->___currentValue_1);
		V_0 = L_0;
		float L_1 = (__this->___start_3);
		float L_2 = (__this->___end_4);
		if ((!(((float)L_1) > ((float)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		V_1 = G_B3_0;
		float L_3 = SliderHandler_MousePosition_m6_1514(__this, /*hidden argument*/NULL);
		float L_4 = SliderHandler_PageUpMovementBound_m6_1503(__this, /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0048;
		}
	}
	{
		float L_5 = V_0;
		float L_6 = (__this->___size_2);
		int32_t L_7 = V_1;
		V_0 = ((float)((float)L_5+(float)((float)((float)((float)((float)L_6*(float)(((float)((float)L_7)))))*(float)(0.9f)))));
		goto IL_005a;
	}

IL_0048:
	{
		float L_8 = V_0;
		float L_9 = (__this->___size_2);
		int32_t L_10 = V_1;
		V_0 = ((float)((float)L_8-(float)((float)((float)((float)((float)L_9*(float)(((float)((float)L_10)))))*(float)(0.9f)))));
	}

IL_005a:
	{
		float L_11 = V_0;
		float L_12 = SliderHandler_Clamp_m6_1506(__this, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.SliderHandler::PageUpMovementBound()
extern "C" float SliderHandler_PageUpMovementBound_m6_1503 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	Rect_t6_52  V_0 = {0};
	Rect_t6_52  V_1 = {0};
	Rect_t6_52  V_2 = {0};
	Rect_t6_52  V_3 = {0};
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		Rect_t6_52  L_1 = SliderHandler_ThumbRect_m6_1510(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_xMax_m6_283((&V_0), /*hidden argument*/NULL);
		Rect_t6_52  L_3 = (__this->___position_0);
		V_1 = L_3;
		float L_4 = Rect_get_x_m6_273((&V_1), /*hidden argument*/NULL);
		return ((float)((float)L_2-(float)L_4));
	}

IL_0029:
	{
		Rect_t6_52  L_5 = SliderHandler_ThumbRect_m6_1510(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_yMax_m6_284((&V_2), /*hidden argument*/NULL);
		Rect_t6_52  L_7 = (__this->___position_0);
		V_3 = L_7;
		float L_8 = Rect_get_y_m6_275((&V_3), /*hidden argument*/NULL);
		return ((float)((float)L_6-(float)L_8));
	}
}
// UnityEngine.Event UnityEngine.SliderHandler::CurrentEvent()
extern "C" Event_t6_154 * SliderHandler_CurrentEvent_m6_1504 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	{
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.SliderHandler::ValueForCurrentMousePosition()
extern "C" float SliderHandler_ValueForCurrentMousePosition_m6_1505 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	Rect_t6_52  V_0 = {0};
	Rect_t6_52  V_1 = {0};
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		float L_1 = SliderHandler_MousePosition_m6_1514(__this, /*hidden argument*/NULL);
		Rect_t6_52  L_2 = SliderHandler_ThumbRect_m6_1510(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m6_277((&V_0), /*hidden argument*/NULL);
		float L_4 = SliderHandler_ValuesPerPixel_m6_1515(__this, /*hidden argument*/NULL);
		float L_5 = (__this->___start_3);
		float L_6 = (__this->___size_2);
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_1-(float)((float)((float)L_3*(float)(0.5f)))))/(float)L_4))+(float)L_5))-(float)((float)((float)L_6*(float)(0.5f)))));
	}

IL_0042:
	{
		float L_7 = SliderHandler_MousePosition_m6_1514(__this, /*hidden argument*/NULL);
		Rect_t6_52  L_8 = SliderHandler_ThumbRect_m6_1510(__this, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = Rect_get_height_m6_279((&V_1), /*hidden argument*/NULL);
		float L_10 = SliderHandler_ValuesPerPixel_m6_1515(__this, /*hidden argument*/NULL);
		float L_11 = (__this->___start_3);
		float L_12 = (__this->___size_2);
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_7-(float)((float)((float)L_9*(float)(0.5f)))))/(float)L_10))+(float)L_11))-(float)((float)((float)L_12*(float)(0.5f)))));
	}
}
// System.Single UnityEngine.SliderHandler::Clamp(System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_Clamp_m6_1506 (SliderHandler_t6_229 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value;
		float L_1 = SliderHandler_MinValue_m6_1518(__this, /*hidden argument*/NULL);
		float L_2 = SliderHandler_MaxValue_m6_1517(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m6_389(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbSelectionRect()
extern "C" Rect_t6_52  SliderHandler_ThumbSelectionRect_m6_1507 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	Rect_t6_52  V_0 = {0};
	int32_t V_1 = 0;
	{
		Rect_t6_52  L_0 = SliderHandler_ThumbRect_m6_1510(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = ((int32_t)12);
		float L_1 = Rect_get_width_m6_277((&V_0), /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		if ((!(((float)L_1) < ((float)(((float)((float)L_2)))))))
		{
			goto IL_003f;
		}
	}
	{
		Rect_t6_52 * L_3 = (&V_0);
		float L_4 = Rect_get_x_m6_273(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		float L_6 = Rect_get_width_m6_277((&V_0), /*hidden argument*/NULL);
		Rect_set_x_m6_274(L_3, ((float)((float)L_4-(float)((float)((float)((float)((float)(((float)((float)L_5)))-(float)L_6))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		Rect_set_width_m6_278((&V_0), (((float)((float)L_7))), /*hidden argument*/NULL);
	}

IL_003f:
	{
		float L_8 = Rect_get_height_m6_279((&V_0), /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		if ((!(((float)L_8) < ((float)(((float)((float)L_9)))))))
		{
			goto IL_0074;
		}
	}
	{
		Rect_t6_52 * L_10 = (&V_0);
		float L_11 = Rect_get_y_m6_275(L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		float L_13 = Rect_get_height_m6_279((&V_0), /*hidden argument*/NULL);
		Rect_set_y_m6_276(L_10, ((float)((float)L_11-(float)((float)((float)((float)((float)(((float)((float)L_12)))-(float)L_13))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		Rect_set_height_m6_280((&V_0), (((float)((float)L_14))), /*hidden argument*/NULL);
	}

IL_0074:
	{
		Rect_t6_52  L_15 = V_0;
		return L_15;
	}
}
// System.Void UnityEngine.SliderHandler::StartDraggingWithValue(System.Single)
extern "C" void SliderHandler_StartDraggingWithValue_m6_1508 (SliderHandler_t6_229 * __this, float ___dragStartValue, const MethodInfo* method)
{
	SliderState_t6_228 * V_0 = {0};
	{
		SliderState_t6_228 * L_0 = SliderHandler_SliderState_m6_1509(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		SliderState_t6_228 * L_1 = V_0;
		float L_2 = SliderHandler_MousePosition_m6_1514(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->___dragStartPos_0 = L_2;
		SliderState_t6_228 * L_3 = V_0;
		float L_4 = ___dragStartValue;
		NullCheck(L_3);
		L_3->___dragStartValue_1 = L_4;
		SliderState_t6_228 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___isDragging_2 = 1;
		return;
	}
}
// UnityEngine.SliderState UnityEngine.SliderHandler::SliderState()
extern const Il2CppType* SliderState_t6_228_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* SliderState_t6_228_il2cpp_TypeInfo_var;
extern "C" SliderState_t6_228 * SliderHandler_SliderState_m6_1509 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SliderState_t6_228_0_0_0_var = il2cpp_codegen_type_from_index(1035);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		SliderState_t6_228_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1035);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_893(NULL /*static, unused*/, LoadTypeToken(SliderState_t6_228_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_1 = (__this->___id_8);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		Object_t * L_2 = GUIUtility_GetStateObject_m6_1369(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((SliderState_t6_228 *)CastclassClass(L_2, SliderState_t6_228_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbRect()
extern "C" Rect_t6_52  SliderHandler_ThumbRect_m6_1510 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	Rect_t6_52  G_B3_0 = {0};
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Rect_t6_52  L_1 = SliderHandler_HorizontalThumbRect_m6_1512(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Rect_t6_52  L_2 = SliderHandler_VerticalThumbRect_m6_1511(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.Rect UnityEngine.SliderHandler::VerticalThumbRect()
extern "C" Rect_t6_52  SliderHandler_VerticalThumbRect_m6_1511 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t6_52  V_1 = {0};
	Rect_t6_52  V_2 = {0};
	Rect_t6_52  V_3 = {0};
	Rect_t6_52  V_4 = {0};
	Rect_t6_52  V_5 = {0};
	Rect_t6_52  V_6 = {0};
	{
		float L_0 = SliderHandler_ValuesPerPixel_m6_1515(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (__this->___start_3);
		float L_2 = (__this->___end_4);
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		Rect_t6_52  L_3 = (__this->___position_0);
		V_1 = L_3;
		float L_4 = Rect_get_x_m6_273((&V_1), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_5 = (__this->___slider_5);
		NullCheck(L_5);
		RectOffset_t6_172 * L_6 = GUIStyle_get_padding_m6_1304(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_left_m6_1284(L_6, /*hidden argument*/NULL);
		float L_8 = SliderHandler_ClampedCurrentValue_m6_1513(__this, /*hidden argument*/NULL);
		float L_9 = (__this->___start_3);
		float L_10 = V_0;
		Rect_t6_52  L_11 = (__this->___position_0);
		V_2 = L_11;
		float L_12 = Rect_get_y_m6_275((&V_2), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_13 = (__this->___slider_5);
		NullCheck(L_13);
		RectOffset_t6_172 * L_14 = GUIStyle_get_padding_m6_1304(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m6_1288(L_14, /*hidden argument*/NULL);
		Rect_t6_52  L_16 = (__this->___position_0);
		V_3 = L_16;
		float L_17 = Rect_get_width_m6_277((&V_3), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_18 = (__this->___slider_5);
		NullCheck(L_18);
		RectOffset_t6_172 * L_19 = GUIStyle_get_padding_m6_1304(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_horizontal_m6_1292(L_19, /*hidden argument*/NULL);
		float L_21 = (__this->___size_2);
		float L_22 = V_0;
		float L_23 = SliderHandler_ThumbSize_m6_1516(__this, /*hidden argument*/NULL);
		Rect_t6_52  L_24 = {0};
		Rect__ctor_m6_270(&L_24, ((float)((float)L_4+(float)(((float)((float)L_7))))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8-(float)L_9))*(float)L_10))+(float)L_12))+(float)(((float)((float)L_15))))), ((float)((float)L_17-(float)(((float)((float)L_20))))), ((float)((float)((float)((float)L_21*(float)L_22))+(float)L_23)), /*hidden argument*/NULL);
		return L_24;
	}

IL_009d:
	{
		Rect_t6_52  L_25 = (__this->___position_0);
		V_4 = L_25;
		float L_26 = Rect_get_x_m6_273((&V_4), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_27 = (__this->___slider_5);
		NullCheck(L_27);
		RectOffset_t6_172 * L_28 = GUIStyle_get_padding_m6_1304(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = RectOffset_get_left_m6_1284(L_28, /*hidden argument*/NULL);
		float L_30 = SliderHandler_ClampedCurrentValue_m6_1513(__this, /*hidden argument*/NULL);
		float L_31 = (__this->___size_2);
		float L_32 = (__this->___start_3);
		float L_33 = V_0;
		Rect_t6_52  L_34 = (__this->___position_0);
		V_5 = L_34;
		float L_35 = Rect_get_y_m6_275((&V_5), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_36 = (__this->___slider_5);
		NullCheck(L_36);
		RectOffset_t6_172 * L_37 = GUIStyle_get_padding_m6_1304(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = RectOffset_get_top_m6_1288(L_37, /*hidden argument*/NULL);
		Rect_t6_52  L_39 = (__this->___position_0);
		V_6 = L_39;
		float L_40 = Rect_get_width_m6_277((&V_6), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_41 = (__this->___slider_5);
		NullCheck(L_41);
		RectOffset_t6_172 * L_42 = GUIStyle_get_padding_m6_1304(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_horizontal_m6_1292(L_42, /*hidden argument*/NULL);
		float L_44 = (__this->___size_2);
		float L_45 = V_0;
		float L_46 = SliderHandler_ThumbSize_m6_1516(__this, /*hidden argument*/NULL);
		Rect_t6_52  L_47 = {0};
		Rect__ctor_m6_270(&L_47, ((float)((float)L_26+(float)(((float)((float)L_29))))), ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))-(float)L_32))*(float)L_33))+(float)L_35))+(float)(((float)((float)L_38))))), ((float)((float)L_40-(float)(((float)((float)L_43))))), ((float)((float)((float)((float)L_44*(float)((-L_45))))+(float)L_46)), /*hidden argument*/NULL);
		return L_47;
	}
}
// UnityEngine.Rect UnityEngine.SliderHandler::HorizontalThumbRect()
extern "C" Rect_t6_52  SliderHandler_HorizontalThumbRect_m6_1512 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t6_52  V_1 = {0};
	Rect_t6_52  V_2 = {0};
	Rect_t6_52  V_3 = {0};
	Rect_t6_52  V_4 = {0};
	Rect_t6_52  V_5 = {0};
	Rect_t6_52  V_6 = {0};
	{
		float L_0 = SliderHandler_ValuesPerPixel_m6_1515(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (__this->___start_3);
		float L_2 = (__this->___end_4);
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		float L_3 = SliderHandler_ClampedCurrentValue_m6_1513(__this, /*hidden argument*/NULL);
		float L_4 = (__this->___start_3);
		float L_5 = V_0;
		Rect_t6_52  L_6 = (__this->___position_0);
		V_1 = L_6;
		float L_7 = Rect_get_x_m6_273((&V_1), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_8 = (__this->___slider_5);
		NullCheck(L_8);
		RectOffset_t6_172 * L_9 = GUIStyle_get_padding_m6_1304(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_left_m6_1284(L_9, /*hidden argument*/NULL);
		Rect_t6_52  L_11 = (__this->___position_0);
		V_2 = L_11;
		float L_12 = Rect_get_y_m6_275((&V_2), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_13 = (__this->___slider_5);
		NullCheck(L_13);
		RectOffset_t6_172 * L_14 = GUIStyle_get_padding_m6_1304(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m6_1288(L_14, /*hidden argument*/NULL);
		float L_16 = (__this->___size_2);
		float L_17 = V_0;
		float L_18 = SliderHandler_ThumbSize_m6_1516(__this, /*hidden argument*/NULL);
		Rect_t6_52  L_19 = (__this->___position_0);
		V_3 = L_19;
		float L_20 = Rect_get_height_m6_279((&V_3), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_21 = (__this->___slider_5);
		NullCheck(L_21);
		RectOffset_t6_172 * L_22 = GUIStyle_get_padding_m6_1304(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = RectOffset_get_vertical_m6_1293(L_22, /*hidden argument*/NULL);
		Rect_t6_52  L_24 = {0};
		Rect__ctor_m6_270(&L_24, ((float)((float)((float)((float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5))+(float)L_7))+(float)(((float)((float)L_10))))), ((float)((float)L_12+(float)(((float)((float)L_15))))), ((float)((float)((float)((float)L_16*(float)L_17))+(float)L_18)), ((float)((float)L_20-(float)(((float)((float)L_23))))), /*hidden argument*/NULL);
		return L_24;
	}

IL_009d:
	{
		float L_25 = SliderHandler_ClampedCurrentValue_m6_1513(__this, /*hidden argument*/NULL);
		float L_26 = (__this->___size_2);
		float L_27 = (__this->___start_3);
		float L_28 = V_0;
		Rect_t6_52  L_29 = (__this->___position_0);
		V_4 = L_29;
		float L_30 = Rect_get_x_m6_273((&V_4), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_31 = (__this->___slider_5);
		NullCheck(L_31);
		RectOffset_t6_172 * L_32 = GUIStyle_get_padding_m6_1304(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		int32_t L_33 = RectOffset_get_left_m6_1284(L_32, /*hidden argument*/NULL);
		Rect_t6_52  L_34 = (__this->___position_0);
		V_5 = L_34;
		float L_35 = Rect_get_y_m6_275((&V_5), /*hidden argument*/NULL);
		float L_36 = (__this->___size_2);
		float L_37 = V_0;
		float L_38 = SliderHandler_ThumbSize_m6_1516(__this, /*hidden argument*/NULL);
		Rect_t6_52  L_39 = (__this->___position_0);
		V_6 = L_39;
		float L_40 = Rect_get_height_m6_279((&V_6), /*hidden argument*/NULL);
		Rect_t6_52  L_41 = {0};
		Rect__ctor_m6_270(&L_41, ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_25+(float)L_26))-(float)L_27))*(float)L_28))+(float)L_30))+(float)(((float)((float)L_33))))), L_35, ((float)((float)((float)((float)L_36*(float)((-L_37))))+(float)L_38)), L_40, /*hidden argument*/NULL);
		return L_41;
	}
}
// System.Single UnityEngine.SliderHandler::ClampedCurrentValue()
extern "C" float SliderHandler_ClampedCurrentValue_m6_1513 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___currentValue_1);
		float L_1 = SliderHandler_Clamp_m6_1506(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityEngine.SliderHandler::MousePosition()
extern "C" float SliderHandler_MousePosition_m6_1514 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	Vector2_t6_48  V_0 = {0};
	Rect_t6_52  V_1 = {0};
	Vector2_t6_48  V_2 = {0};
	Rect_t6_52  V_3 = {0};
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Event_t6_154 * L_1 = SliderHandler_CurrentEvent_m6_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t6_48  L_2 = Event_get_mousePosition_m6_970(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = ((&V_0)->___x_1);
		Rect_t6_52  L_4 = (__this->___position_0);
		V_1 = L_4;
		float L_5 = Rect_get_x_m6_273((&V_1), /*hidden argument*/NULL);
		return ((float)((float)L_3-(float)L_5));
	}

IL_002e:
	{
		Event_t6_154 * L_6 = SliderHandler_CurrentEvent_m6_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_t6_48  L_7 = Event_get_mousePosition_m6_970(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = ((&V_2)->___y_2);
		Rect_t6_52  L_9 = (__this->___position_0);
		V_3 = L_9;
		float L_10 = Rect_get_y_m6_275((&V_3), /*hidden argument*/NULL);
		return ((float)((float)L_8-(float)L_10));
	}
}
// System.Single UnityEngine.SliderHandler::ValuesPerPixel()
extern "C" float SliderHandler_ValuesPerPixel_m6_1515 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	Rect_t6_52  V_0 = {0};
	Rect_t6_52  V_1 = {0};
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		Rect_t6_52  L_1 = (__this->___position_0);
		V_0 = L_1;
		float L_2 = Rect_get_width_m6_277((&V_0), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_3 = (__this->___slider_5);
		NullCheck(L_3);
		RectOffset_t6_172 * L_4 = GUIStyle_get_padding_m6_1304(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_horizontal_m6_1292(L_4, /*hidden argument*/NULL);
		float L_6 = SliderHandler_ThumbSize_m6_1516(__this, /*hidden argument*/NULL);
		float L_7 = (__this->___end_4);
		float L_8 = (__this->___start_3);
		return ((float)((float)((float)((float)((float)((float)L_2-(float)(((float)((float)L_5)))))-(float)L_6))/(float)((float)((float)L_7-(float)L_8))));
	}

IL_0041:
	{
		Rect_t6_52  L_9 = (__this->___position_0);
		V_1 = L_9;
		float L_10 = Rect_get_height_m6_279((&V_1), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_11 = (__this->___slider_5);
		NullCheck(L_11);
		RectOffset_t6_172 * L_12 = GUIStyle_get_padding_m6_1304(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_vertical_m6_1293(L_12, /*hidden argument*/NULL);
		float L_14 = SliderHandler_ThumbSize_m6_1516(__this, /*hidden argument*/NULL);
		float L_15 = (__this->___end_4);
		float L_16 = (__this->___start_3);
		return ((float)((float)((float)((float)((float)((float)L_10-(float)(((float)((float)L_13)))))-(float)L_14))/(float)((float)((float)L_15-(float)L_16))));
	}
}
// System.Single UnityEngine.SliderHandler::ThumbSize()
extern "C" float SliderHandler_ThumbSize_m6_1516 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	float G_B4_0 = 0.0f;
	float G_B8_0 = 0.0f;
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		GUIStyle_t6_166 * L_1 = (__this->___thumb_6);
		NullCheck(L_1);
		float L_2 = GUIStyle_get_fixedWidth_m6_1342(L_1, /*hidden argument*/NULL);
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_0030;
		}
	}
	{
		GUIStyle_t6_166 * L_3 = (__this->___thumb_6);
		NullCheck(L_3);
		float L_4 = GUIStyle_get_fixedWidth_m6_1342(L_3, /*hidden argument*/NULL);
		G_B4_0 = L_4;
		goto IL_0041;
	}

IL_0030:
	{
		GUIStyle_t6_166 * L_5 = (__this->___thumb_6);
		NullCheck(L_5);
		RectOffset_t6_172 * L_6 = GUIStyle_get_padding_m6_1304(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_horizontal_m6_1292(L_6, /*hidden argument*/NULL);
		G_B4_0 = (((float)((float)L_7)));
	}

IL_0041:
	{
		return G_B4_0;
	}

IL_0042:
	{
		GUIStyle_t6_166 * L_8 = (__this->___thumb_6);
		NullCheck(L_8);
		float L_9 = GUIStyle_get_fixedHeight_m6_1343(L_8, /*hidden argument*/NULL);
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		GUIStyle_t6_166 * L_10 = (__this->___thumb_6);
		NullCheck(L_10);
		float L_11 = GUIStyle_get_fixedHeight_m6_1343(L_10, /*hidden argument*/NULL);
		G_B8_0 = L_11;
		goto IL_0078;
	}

IL_0067:
	{
		GUIStyle_t6_166 * L_12 = (__this->___thumb_6);
		NullCheck(L_12);
		RectOffset_t6_172 * L_13 = GUIStyle_get_padding_m6_1304(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_vertical_m6_1293(L_13, /*hidden argument*/NULL);
		G_B8_0 = (((float)((float)L_14)));
	}

IL_0078:
	{
		return G_B8_0;
	}
}
// System.Single UnityEngine.SliderHandler::MaxValue()
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_MaxValue_m6_1517 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___start_3);
		float L_1 = (__this->___end_4);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m6_383(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = (__this->___size_2);
		return ((float)((float)L_2-(float)L_3));
	}
}
// System.Single UnityEngine.SliderHandler::MinValue()
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_MinValue_m6_1518 (SliderHandler_t6_229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___start_3);
		float L_1 = (__this->___end_4);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m6_381(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.StackTraceUtility::.ctor()
extern "C" void StackTraceUtility__ctor_m6_1519 (StackTraceUtility_t6_230 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t6_230_il2cpp_TypeInfo_var;
extern "C" void StackTraceUtility__cctor_m6_1520 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		StackTraceUtility_t6_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1036);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		((StackTraceUtility_t6_230_StaticFields*)StackTraceUtility_t6_230_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern TypeInfo* StackTraceUtility_t6_230_il2cpp_TypeInfo_var;
extern "C" void StackTraceUtility_SetProjectFolder_m6_1521 (Object_t * __this /* static, unused */, String_t* ___folder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StackTraceUtility_t6_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1036);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___folder;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_230_il2cpp_TypeInfo_var);
		((StackTraceUtility_t6_230_StaticFields*)StackTraceUtility_t6_230_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0 = L_0;
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern TypeInfo* StackTrace_t1_199_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t6_230_il2cpp_TypeInfo_var;
extern "C" String_t* StackTraceUtility_ExtractStackTrace_m6_1522 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StackTrace_t1_199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(100);
		StackTraceUtility_t6_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1036);
		s_Il2CppMethodIntialized = true;
	}
	StackTrace_t1_199 * V_0 = {0};
	String_t* V_1 = {0};
	{
		StackTrace_t1_199 * L_0 = (StackTrace_t1_199 *)il2cpp_codegen_object_new (StackTrace_t1_199_il2cpp_TypeInfo_var);
		StackTrace__ctor_m1_1875(L_0, 1, 1, /*hidden argument*/NULL);
		V_0 = L_0;
		StackTrace_t1_199 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_230_il2cpp_TypeInfo_var);
		String_t* L_2 = StackTraceUtility_ExtractFormattedStackTrace_m6_1527(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = String_ToString_m1_408(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2857;
extern Il2CppCodeGenString* _stringLiteral2858;
extern Il2CppCodeGenString* _stringLiteral2859;
extern Il2CppCodeGenString* _stringLiteral2860;
extern Il2CppCodeGenString* _stringLiteral2861;
extern Il2CppCodeGenString* _stringLiteral2862;
extern "C" bool StackTraceUtility_IsSystemStacktraceType_m6_1523 (Object_t * __this /* static, unused */, Object_t * ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2857 = il2cpp_codegen_string_literal_from_index(2857);
		_stringLiteral2858 = il2cpp_codegen_string_literal_from_index(2858);
		_stringLiteral2859 = il2cpp_codegen_string_literal_from_index(2859);
		_stringLiteral2860 = il2cpp_codegen_string_literal_from_index(2860);
		_stringLiteral2861 = il2cpp_codegen_string_literal_from_index(2861);
		_stringLiteral2862 = il2cpp_codegen_string_literal_from_index(2862);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___name;
		V_0 = ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m1_399(L_1, _stringLiteral2857, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m1_399(L_3, _stringLiteral2858, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = String_StartsWith_m1_399(L_5, _stringLiteral2859, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = String_StartsWith_m1_399(L_7, _stringLiteral2860, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = String_StartsWith_m1_399(L_9, _stringLiteral2861, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = String_StartsWith_m1_399(L_11, _stringLiteral2862, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_12));
		goto IL_0065;
	}

IL_0064:
	{
		G_B7_0 = 1;
	}

IL_0065:
	{
		return G_B7_0;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStringFromException(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t6_230_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2802;
extern "C" String_t* StackTraceUtility_ExtractStringFromException_m6_1524 (Object_t * __this /* static, unused */, Object_t * ___exception, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		StackTraceUtility_t6_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1036);
		_stringLiteral2802 = il2cpp_codegen_string_literal_from_index(2802);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_1 = L_1;
		Object_t * L_2 = ___exception;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_230_il2cpp_TypeInfo_var);
		StackTraceUtility_ExtractStringFromExceptionInternal_m6_1525(NULL /*static, unused*/, L_2, (&V_0), (&V_1), /*hidden argument*/NULL);
		String_t* L_3 = V_0;
		String_t* L_4 = V_1;
		String_t* L_5 = String_Concat_m1_419(NULL /*static, unused*/, L_3, _stringLiteral2802, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern TypeInfo* ArgumentException_t1_646_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t1_145_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTrace_t1_199_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t6_230_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2863;
extern Il2CppCodeGenString* _stringLiteral2864;
extern Il2CppCodeGenString* _stringLiteral2802;
extern Il2CppCodeGenString* _stringLiteral225;
extern Il2CppCodeGenString* _stringLiteral2865;
extern "C" void StackTraceUtility_ExtractStringFromExceptionInternal_m6_1525 (Object_t * __this /* static, unused */, Object_t * ___exceptiono, String_t** ___message, String_t** ___stackTrace, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		StringBuilder_t1_145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		StackTrace_t1_199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(100);
		StackTraceUtility_t6_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1036);
		_stringLiteral2863 = il2cpp_codegen_string_literal_from_index(2863);
		_stringLiteral2864 = il2cpp_codegen_string_literal_from_index(2864);
		_stringLiteral2802 = il2cpp_codegen_string_literal_from_index(2802);
		_stringLiteral225 = il2cpp_codegen_string_literal_from_index(225);
		_stringLiteral2865 = il2cpp_codegen_string_literal_from_index(2865);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * V_0 = {0};
	StringBuilder_t1_145 * V_1 = {0};
	String_t* V_2 = {0};
	String_t* V_3 = {0};
	String_t* V_4 = {0};
	StackTrace_t1_199 * V_5 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___exceptiono;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t1_646 * L_1 = (ArgumentException_t1_646 *)il2cpp_codegen_object_new (ArgumentException_t1_646_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_4605(L_1, _stringLiteral2863, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___exceptiono;
		V_0 = ((Exception_t1_33 *)IsInstClass(L_2, Exception_t1_33_il2cpp_TypeInfo_var));
		Exception_t1_33 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentException_t1_646 * L_4 = (ArgumentException_t1_646 *)il2cpp_codegen_object_new (ArgumentException_t1_646_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_4605(L_4, _stringLiteral2864, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0029:
	{
		Exception_t1_33 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_5);
		if (L_6)
		{
			goto IL_003e;
		}
	}
	{
		G_B7_0 = ((int32_t)512);
		goto IL_004b;
	}

IL_003e:
	{
		Exception_t1_33 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_7);
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m1_428(L_8, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)((int32_t)L_9*(int32_t)2));
	}

IL_004b:
	{
		StringBuilder_t1_145 * L_10 = (StringBuilder_t1_145 *)il2cpp_codegen_object_new (StringBuilder_t1_145_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_4330(L_10, G_B7_0, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t** L_11 = ___message;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		*((Object_t **)(L_11)) = (Object_t *)L_12;
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_2 = L_13;
		goto IL_00ff;
	}

IL_0063:
	{
		String_t* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1_428(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_007a;
		}
	}
	{
		Exception_t1_33 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_16);
		V_2 = L_17;
		goto IL_008c;
	}

IL_007a:
	{
		Exception_t1_33 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_18);
		String_t* L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1_419(NULL /*static, unused*/, L_19, _stringLiteral2802, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
	}

IL_008c:
	{
		Exception_t1_33 * L_22 = V_0;
		NullCheck(L_22);
		Type_t * L_23 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(10 /* System.Type System.Exception::GetType() */, L_22);
		NullCheck(L_23);
		String_t* L_24 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		V_3 = L_24;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_4 = L_25;
		Exception_t1_33 * L_26 = V_0;
		NullCheck(L_26);
		String_t* L_27 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_26);
		if (!L_27)
		{
			goto IL_00b2;
		}
	}
	{
		Exception_t1_33 * L_28 = V_0;
		NullCheck(L_28);
		String_t* L_29 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_28);
		V_4 = L_29;
	}

IL_00b2:
	{
		String_t* L_30 = V_4;
		NullCheck(L_30);
		String_t* L_31 = String_Trim_m1_354(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		int32_t L_32 = String_get_Length_m1_428(L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00d8;
		}
	}
	{
		String_t* L_33 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m1_418(NULL /*static, unused*/, L_33, _stringLiteral225, /*hidden argument*/NULL);
		V_3 = L_34;
		String_t* L_35 = V_3;
		String_t* L_36 = V_4;
		String_t* L_37 = String_Concat_m1_418(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_3 = L_37;
	}

IL_00d8:
	{
		String_t** L_38 = ___message;
		String_t* L_39 = V_3;
		*((Object_t **)(L_38)) = (Object_t *)L_39;
		Exception_t1_33 * L_40 = V_0;
		NullCheck(L_40);
		Exception_t1_33 * L_41 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_40);
		if (!L_41)
		{
			goto IL_00f8;
		}
	}
	{
		String_t* L_42 = V_3;
		String_t* L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m1_420(NULL /*static, unused*/, _stringLiteral2865, L_42, _stringLiteral2802, L_43, /*hidden argument*/NULL);
		V_2 = L_44;
	}

IL_00f8:
	{
		Exception_t1_33 * L_45 = V_0;
		NullCheck(L_45);
		Exception_t1_33 * L_46 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_45);
		V_0 = L_46;
	}

IL_00ff:
	{
		Exception_t1_33 * L_47 = V_0;
		if (L_47)
		{
			goto IL_0063;
		}
	}
	{
		StringBuilder_t1_145 * L_48 = V_1;
		String_t* L_49 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m1_418(NULL /*static, unused*/, L_49, _stringLiteral2802, /*hidden argument*/NULL);
		NullCheck(L_48);
		StringBuilder_Append_m1_4346(L_48, L_50, /*hidden argument*/NULL);
		StackTrace_t1_199 * L_51 = (StackTrace_t1_199 *)il2cpp_codegen_object_new (StackTrace_t1_199_il2cpp_TypeInfo_var);
		StackTrace__ctor_m1_1875(L_51, 1, 1, /*hidden argument*/NULL);
		V_5 = L_51;
		StringBuilder_t1_145 * L_52 = V_1;
		StackTrace_t1_199 * L_53 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_230_il2cpp_TypeInfo_var);
		String_t* L_54 = StackTraceUtility_ExtractFormattedStackTrace_m6_1527(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		NullCheck(L_52);
		StringBuilder_Append_m1_4346(L_52, L_54, /*hidden argument*/NULL);
		String_t** L_55 = ___stackTrace;
		StringBuilder_t1_145 * L_56 = V_1;
		NullCheck(L_56);
		String_t* L_57 = StringBuilder_ToString_m1_4341(L_56, /*hidden argument*/NULL);
		*((Object_t **)(L_55)) = (Object_t *)L_57;
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t1_145_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t6_230_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2866;
extern Il2CppCodeGenString* _stringLiteral2867;
extern Il2CppCodeGenString* _stringLiteral2868;
extern Il2CppCodeGenString* _stringLiteral2869;
extern Il2CppCodeGenString* _stringLiteral2870;
extern Il2CppCodeGenString* _stringLiteral2871;
extern Il2CppCodeGenString* _stringLiteral228;
extern Il2CppCodeGenString* _stringLiteral230;
extern Il2CppCodeGenString* _stringLiteral2872;
extern Il2CppCodeGenString* _stringLiteral2873;
extern Il2CppCodeGenString* _stringLiteral2874;
extern Il2CppCodeGenString* _stringLiteral2875;
extern Il2CppCodeGenString* _stringLiteral2876;
extern Il2CppCodeGenString* _stringLiteral93;
extern Il2CppCodeGenString* _stringLiteral2802;
extern "C" String_t* StackTraceUtility_PostprocessStacktrace_m6_1526 (Object_t * __this /* static, unused */, String_t* ___oldString, bool ___stripEngineInternalInformation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		StringBuilder_t1_145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		StackTraceUtility_t6_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1036);
		_stringLiteral2866 = il2cpp_codegen_string_literal_from_index(2866);
		_stringLiteral2867 = il2cpp_codegen_string_literal_from_index(2867);
		_stringLiteral2868 = il2cpp_codegen_string_literal_from_index(2868);
		_stringLiteral2869 = il2cpp_codegen_string_literal_from_index(2869);
		_stringLiteral2870 = il2cpp_codegen_string_literal_from_index(2870);
		_stringLiteral2871 = il2cpp_codegen_string_literal_from_index(2871);
		_stringLiteral228 = il2cpp_codegen_string_literal_from_index(228);
		_stringLiteral230 = il2cpp_codegen_string_literal_from_index(230);
		_stringLiteral2872 = il2cpp_codegen_string_literal_from_index(2872);
		_stringLiteral2873 = il2cpp_codegen_string_literal_from_index(2873);
		_stringLiteral2874 = il2cpp_codegen_string_literal_from_index(2874);
		_stringLiteral2875 = il2cpp_codegen_string_literal_from_index(2875);
		_stringLiteral2876 = il2cpp_codegen_string_literal_from_index(2876);
		_stringLiteral93 = il2cpp_codegen_string_literal_from_index(93);
		_stringLiteral2802 = il2cpp_codegen_string_literal_from_index(2802);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1_202* V_0 = {0};
	StringBuilder_t1_145 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		String_t* L_0 = ___oldString;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_1;
	}

IL_000c:
	{
		String_t* L_2 = ___oldString;
		CharU5BU5D_t1_16* L_3 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)10);
		NullCheck(L_2);
		StringU5BU5D_t1_202* L_4 = String_Split_m1_346(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = ___oldString;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1_428(L_5, /*hidden argument*/NULL);
		StringBuilder_t1_145 * L_7 = (StringBuilder_t1_145 *)il2cpp_codegen_object_new (StringBuilder_t1_145_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_4330(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = 0;
		goto IL_0040;
	}

IL_0031:
	{
		StringU5BU5D_t1_202* L_8 = V_0;
		int32_t L_9 = V_2;
		StringU5BU5D_t1_202* L_10 = V_0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck((*(String_t**)(String_t**)SZArrayLdElema(L_10, L_12, sizeof(String_t*))));
		String_t* L_13 = String_Trim_m1_354((*(String_t**)(String_t**)SZArrayLdElema(L_10, L_12, sizeof(String_t*))), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_13);
		*((String_t**)(String_t**)SZArrayLdElema(L_8, L_9, sizeof(String_t*))) = (String_t*)L_13;
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_15 = V_2;
		StringU5BU5D_t1_202* L_16 = V_0;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_16)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		V_3 = 0;
		goto IL_0265;
	}

IL_0050:
	{
		StringU5BU5D_t1_202* L_17 = V_0;
		int32_t L_18 = V_3;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		V_4 = (*(String_t**)(String_t**)SZArrayLdElema(L_17, L_19, sizeof(String_t*)));
		String_t* L_20 = V_4;
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_m1_428(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0070;
		}
	}
	{
		String_t* L_22 = V_4;
		NullCheck(L_22);
		uint16_t L_23 = String_get_Chars_m1_341(L_22, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0075;
		}
	}

IL_0070:
	{
		goto IL_0261;
	}

IL_0075:
	{
		String_t* L_24 = V_4;
		NullCheck(L_24);
		bool L_25 = String_StartsWith_m1_399(L_24, _stringLiteral2866, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0261;
	}

IL_008b:
	{
		bool L_26 = ___stripEngineInternalInformation;
		if (!L_26)
		{
			goto IL_00a7;
		}
	}
	{
		String_t* L_27 = V_4;
		NullCheck(L_27);
		bool L_28 = String_StartsWith_m1_399(L_27, _stringLiteral2867, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00a7;
		}
	}
	{
		goto IL_026e;
	}

IL_00a7:
	{
		bool L_29 = ___stripEngineInternalInformation;
		if (!L_29)
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_30 = V_3;
		StringU5BU5D_t1_202* L_31 = V_0;
		NullCheck(L_31);
		if ((((int32_t)L_30) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_31)->max_length))))-(int32_t)1)))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_32 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_230_il2cpp_TypeInfo_var);
		bool L_33 = StackTraceUtility_IsSystemStacktraceType_m6_1523(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00fa;
		}
	}
	{
		StringU5BU5D_t1_202* L_34 = V_0;
		int32_t L_35 = V_3;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)((int32_t)L_35+(int32_t)1)));
		int32_t L_36 = ((int32_t)((int32_t)L_35+(int32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_230_il2cpp_TypeInfo_var);
		bool L_37 = StackTraceUtility_IsSystemStacktraceType_m6_1523(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_34, L_36, sizeof(String_t*))), /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_00d8;
		}
	}
	{
		goto IL_0261;
	}

IL_00d8:
	{
		String_t* L_38 = V_4;
		NullCheck(L_38);
		int32_t L_39 = String_IndexOf_m1_385(L_38, _stringLiteral2868, /*hidden argument*/NULL);
		V_5 = L_39;
		int32_t L_40 = V_5;
		if ((((int32_t)L_40) == ((int32_t)(-1))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_41 = V_4;
		int32_t L_42 = V_5;
		NullCheck(L_41);
		String_t* L_43 = String_Substring_m1_352(L_41, 0, L_42, /*hidden argument*/NULL);
		V_4 = L_43;
	}

IL_00fa:
	{
		String_t* L_44 = V_4;
		NullCheck(L_44);
		int32_t L_45 = String_IndexOf_m1_385(L_44, _stringLiteral2869, /*hidden argument*/NULL);
		if ((((int32_t)L_45) == ((int32_t)(-1))))
		{
			goto IL_0111;
		}
	}
	{
		goto IL_0261;
	}

IL_0111:
	{
		String_t* L_46 = V_4;
		NullCheck(L_46);
		int32_t L_47 = String_IndexOf_m1_385(L_46, _stringLiteral2870, /*hidden argument*/NULL);
		if ((((int32_t)L_47) == ((int32_t)(-1))))
		{
			goto IL_0128;
		}
	}
	{
		goto IL_0261;
	}

IL_0128:
	{
		String_t* L_48 = V_4;
		NullCheck(L_48);
		int32_t L_49 = String_IndexOf_m1_385(L_48, _stringLiteral2871, /*hidden argument*/NULL);
		if ((((int32_t)L_49) == ((int32_t)(-1))))
		{
			goto IL_013f;
		}
	}
	{
		goto IL_0261;
	}

IL_013f:
	{
		bool L_50 = ___stripEngineInternalInformation;
		if (!L_50)
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_51 = V_4;
		NullCheck(L_51);
		bool L_52 = String_StartsWith_m1_399(L_51, _stringLiteral228, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_53 = V_4;
		NullCheck(L_53);
		bool L_54 = String_EndsWith_m1_371(L_53, _stringLiteral230, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_016c;
		}
	}
	{
		goto IL_0261;
	}

IL_016c:
	{
		String_t* L_55 = V_4;
		NullCheck(L_55);
		bool L_56 = String_StartsWith_m1_399(L_55, _stringLiteral2872, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_0188;
		}
	}
	{
		String_t* L_57 = V_4;
		NullCheck(L_57);
		String_t* L_58 = String_Remove_m1_404(L_57, 0, 3, /*hidden argument*/NULL);
		V_4 = L_58;
	}

IL_0188:
	{
		String_t* L_59 = V_4;
		NullCheck(L_59);
		int32_t L_60 = String_IndexOf_m1_385(L_59, _stringLiteral2873, /*hidden argument*/NULL);
		V_6 = L_60;
		V_7 = (-1);
		int32_t L_61 = V_6;
		if ((((int32_t)L_61) == ((int32_t)(-1))))
		{
			goto IL_01b1;
		}
	}
	{
		String_t* L_62 = V_4;
		int32_t L_63 = V_6;
		NullCheck(L_62);
		int32_t L_64 = String_IndexOf_m1_386(L_62, _stringLiteral230, L_63, /*hidden argument*/NULL);
		V_7 = L_64;
	}

IL_01b1:
	{
		int32_t L_65 = V_6;
		if ((((int32_t)L_65) == ((int32_t)(-1))))
		{
			goto IL_01d4;
		}
	}
	{
		int32_t L_66 = V_7;
		int32_t L_67 = V_6;
		if ((((int32_t)L_66) <= ((int32_t)L_67)))
		{
			goto IL_01d4;
		}
	}
	{
		String_t* L_68 = V_4;
		int32_t L_69 = V_6;
		int32_t L_70 = V_7;
		int32_t L_71 = V_6;
		NullCheck(L_68);
		String_t* L_72 = String_Remove_m1_404(L_68, L_69, ((int32_t)((int32_t)((int32_t)((int32_t)L_70-(int32_t)L_71))+(int32_t)1)), /*hidden argument*/NULL);
		V_4 = L_72;
	}

IL_01d4:
	{
		String_t* L_73 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_74 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_73);
		String_t* L_75 = String_Replace_m1_401(L_73, _stringLiteral2874, L_74, /*hidden argument*/NULL);
		V_4 = L_75;
		String_t* L_76 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_230_il2cpp_TypeInfo_var);
		String_t* L_77 = ((StackTraceUtility_t6_230_StaticFields*)StackTraceUtility_t6_230_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0;
		String_t* L_78 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_76);
		String_t* L_79 = String_Replace_m1_401(L_76, L_77, L_78, /*hidden argument*/NULL);
		V_4 = L_79;
		String_t* L_80 = V_4;
		NullCheck(L_80);
		String_t* L_81 = String_Replace_m1_400(L_80, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		V_4 = L_81;
		String_t* L_82 = V_4;
		NullCheck(L_82);
		int32_t L_83 = String_LastIndexOf_m1_394(L_82, _stringLiteral2875, /*hidden argument*/NULL);
		V_8 = L_83;
		int32_t L_84 = V_8;
		if ((((int32_t)L_84) == ((int32_t)(-1))))
		{
			goto IL_024e;
		}
	}
	{
		String_t* L_85 = V_4;
		int32_t L_86 = V_8;
		NullCheck(L_85);
		String_t* L_87 = String_Remove_m1_404(L_85, L_86, 5, /*hidden argument*/NULL);
		V_4 = L_87;
		String_t* L_88 = V_4;
		int32_t L_89 = V_8;
		NullCheck(L_88);
		String_t* L_90 = String_Insert_m1_424(L_88, L_89, _stringLiteral2876, /*hidden argument*/NULL);
		V_4 = L_90;
		String_t* L_91 = V_4;
		String_t* L_92 = V_4;
		NullCheck(L_92);
		int32_t L_93 = String_get_Length_m1_428(L_92, /*hidden argument*/NULL);
		NullCheck(L_91);
		String_t* L_94 = String_Insert_m1_424(L_91, L_93, _stringLiteral93, /*hidden argument*/NULL);
		V_4 = L_94;
	}

IL_024e:
	{
		StringBuilder_t1_145 * L_95 = V_1;
		String_t* L_96 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_97 = String_Concat_m1_418(NULL /*static, unused*/, L_96, _stringLiteral2802, /*hidden argument*/NULL);
		NullCheck(L_95);
		StringBuilder_Append_m1_4346(L_95, L_97, /*hidden argument*/NULL);
	}

IL_0261:
	{
		int32_t L_98 = V_3;
		V_3 = ((int32_t)((int32_t)L_98+(int32_t)1));
	}

IL_0265:
	{
		int32_t L_99 = V_3;
		StringU5BU5D_t1_202* L_100 = V_0;
		NullCheck(L_100);
		if ((((int32_t)L_99) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_100)->max_length)))))))
		{
			goto IL_0050;
		}
	}

IL_026e:
	{
		StringBuilder_t1_145 * L_101 = V_1;
		NullCheck(L_101);
		String_t* L_102 = StringBuilder_ToString_m1_4341(L_101, /*hidden argument*/NULL);
		return L_102;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern TypeInfo* StringBuilder_t1_145_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t6_230_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral49;
extern Il2CppCodeGenString* _stringLiteral633;
extern Il2CppCodeGenString* _stringLiteral596;
extern Il2CppCodeGenString* _stringLiteral141;
extern Il2CppCodeGenString* _stringLiteral93;
extern Il2CppCodeGenString* _stringLiteral2877;
extern Il2CppCodeGenString* _stringLiteral2878;
extern Il2CppCodeGenString* _stringLiteral2876;
extern Il2CppCodeGenString* _stringLiteral2802;
extern "C" String_t* StackTraceUtility_ExtractFormattedStackTrace_m6_1527 (Object_t * __this /* static, unused */, StackTrace_t1_199 * ___stackTrace, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		StackTraceUtility_t6_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1036);
		_stringLiteral49 = il2cpp_codegen_string_literal_from_index(49);
		_stringLiteral633 = il2cpp_codegen_string_literal_from_index(633);
		_stringLiteral596 = il2cpp_codegen_string_literal_from_index(596);
		_stringLiteral141 = il2cpp_codegen_string_literal_from_index(141);
		_stringLiteral93 = il2cpp_codegen_string_literal_from_index(93);
		_stringLiteral2877 = il2cpp_codegen_string_literal_from_index(2877);
		_stringLiteral2878 = il2cpp_codegen_string_literal_from_index(2878);
		_stringLiteral2876 = il2cpp_codegen_string_literal_from_index(2876);
		_stringLiteral2802 = il2cpp_codegen_string_literal_from_index(2802);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_145 * V_0 = {0};
	int32_t V_1 = 0;
	StackFrame_t1_197 * V_2 = {0};
	MethodBase_t1_198 * V_3 = {0};
	Type_t * V_4 = {0};
	String_t* V_5 = {0};
	int32_t V_6 = 0;
	ParameterInfoU5BU5D_t1_812* V_7 = {0};
	bool V_8 = false;
	String_t* V_9 = {0};
	int32_t V_10 = 0;
	{
		StringBuilder_t1_145 * L_0 = (StringBuilder_t1_145 *)il2cpp_codegen_object_new (StringBuilder_t1_145_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_4330(L_0, ((int32_t)255), /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_01c9;
	}

IL_0012:
	{
		StackTrace_t1_199 * L_1 = ___stackTrace;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		StackFrame_t1_197 * L_3 = (StackFrame_t1_197 *)VirtFuncInvoker1< StackFrame_t1_197 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_1, L_2);
		V_2 = L_3;
		StackFrame_t1_197 * L_4 = V_2;
		NullCheck(L_4);
		MethodBase_t1_198 * L_5 = (MethodBase_t1_198 *)VirtFuncInvoker0< MethodBase_t1_198 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_4);
		V_3 = L_5;
		MethodBase_t1_198 * L_6 = V_3;
		if (L_6)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_01c5;
	}

IL_002c:
	{
		MethodBase_t1_198 * L_7 = V_3;
		NullCheck(L_7);
		Type_t * L_8 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_7);
		V_4 = L_8;
		Type_t * L_9 = V_4;
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		goto IL_01c5;
	}

IL_0040:
	{
		Type_t * L_10 = V_4;
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_10);
		V_5 = L_11;
		String_t* L_12 = V_5;
		if (!L_12)
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_13 = V_5;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m1_428(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0071;
		}
	}
	{
		StringBuilder_t1_145 * L_15 = V_0;
		String_t* L_16 = V_5;
		NullCheck(L_15);
		StringBuilder_Append_m1_4346(L_15, L_16, /*hidden argument*/NULL);
		StringBuilder_t1_145 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m1_4346(L_17, _stringLiteral49, /*hidden argument*/NULL);
	}

IL_0071:
	{
		StringBuilder_t1_145 * L_18 = V_0;
		Type_t * L_19 = V_4;
		NullCheck(L_19);
		String_t* L_20 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		NullCheck(L_18);
		StringBuilder_Append_m1_4346(L_18, L_20, /*hidden argument*/NULL);
		StringBuilder_t1_145 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m1_4346(L_21, _stringLiteral633, /*hidden argument*/NULL);
		StringBuilder_t1_145 * L_22 = V_0;
		MethodBase_t1_198 * L_23 = V_3;
		NullCheck(L_23);
		String_t* L_24 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		NullCheck(L_22);
		StringBuilder_Append_m1_4346(L_22, L_24, /*hidden argument*/NULL);
		StringBuilder_t1_145 * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m1_4346(L_25, _stringLiteral596, /*hidden argument*/NULL);
		V_6 = 0;
		MethodBase_t1_198 * L_26 = V_3;
		NullCheck(L_26);
		ParameterInfoU5BU5D_t1_812* L_27 = (ParameterInfoU5BU5D_t1_812*)VirtFuncInvoker0< ParameterInfoU5BU5D_t1_812* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_26);
		V_7 = L_27;
		V_8 = 1;
		goto IL_00ee;
	}

IL_00b7:
	{
		bool L_28 = V_8;
		if (L_28)
		{
			goto IL_00cf;
		}
	}
	{
		StringBuilder_t1_145 * L_29 = V_0;
		NullCheck(L_29);
		StringBuilder_Append_m1_4346(L_29, _stringLiteral141, /*hidden argument*/NULL);
		goto IL_00d2;
	}

IL_00cf:
	{
		V_8 = 0;
	}

IL_00d2:
	{
		StringBuilder_t1_145 * L_30 = V_0;
		ParameterInfoU5BU5D_t1_812* L_31 = V_7;
		int32_t L_32 = V_6;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		NullCheck((*(ParameterInfo_t1_351 **)(ParameterInfo_t1_351 **)SZArrayLdElema(L_31, L_33, sizeof(ParameterInfo_t1_351 *))));
		Type_t * L_34 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, (*(ParameterInfo_t1_351 **)(ParameterInfo_t1_351 **)SZArrayLdElema(L_31, L_33, sizeof(ParameterInfo_t1_351 *))));
		NullCheck(L_34);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_34);
		NullCheck(L_30);
		StringBuilder_Append_m1_4346(L_30, L_35, /*hidden argument*/NULL);
		int32_t L_36 = V_6;
		V_6 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00ee:
	{
		int32_t L_37 = V_6;
		ParameterInfoU5BU5D_t1_812* L_38 = V_7;
		NullCheck(L_38);
		if ((((int32_t)L_37) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_38)->max_length)))))))
		{
			goto IL_00b7;
		}
	}
	{
		StringBuilder_t1_145 * L_39 = V_0;
		NullCheck(L_39);
		StringBuilder_Append_m1_4346(L_39, _stringLiteral93, /*hidden argument*/NULL);
		StackFrame_t1_197 * L_40 = V_2;
		NullCheck(L_40);
		String_t* L_41 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_40);
		V_9 = L_41;
		String_t* L_42 = V_9;
		if (!L_42)
		{
			goto IL_01b9;
		}
	}
	{
		Type_t * L_43 = V_4;
		NullCheck(L_43);
		String_t* L_44 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_43);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_45 = String_op_Equality_m1_454(NULL /*static, unused*/, L_44, _stringLiteral2877, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_0140;
		}
	}
	{
		Type_t * L_46 = V_4;
		NullCheck(L_46);
		String_t* L_47 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_46);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_48 = String_op_Equality_m1_454(NULL /*static, unused*/, L_47, _stringLiteral2878, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_01b9;
		}
	}

IL_0140:
	{
		StringBuilder_t1_145 * L_49 = V_0;
		NullCheck(L_49);
		StringBuilder_Append_m1_4346(L_49, _stringLiteral2876, /*hidden argument*/NULL);
		String_t* L_50 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_230_il2cpp_TypeInfo_var);
		String_t* L_51 = ((StackTraceUtility_t6_230_StaticFields*)StackTraceUtility_t6_230_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0;
		NullCheck(L_50);
		bool L_52 = String_StartsWith_m1_399(L_50, L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0182;
		}
	}
	{
		String_t* L_53 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_230_il2cpp_TypeInfo_var);
		String_t* L_54 = ((StackTraceUtility_t6_230_StaticFields*)StackTraceUtility_t6_230_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0;
		NullCheck(L_54);
		int32_t L_55 = String_get_Length_m1_428(L_54, /*hidden argument*/NULL);
		String_t* L_56 = V_9;
		NullCheck(L_56);
		int32_t L_57 = String_get_Length_m1_428(L_56, /*hidden argument*/NULL);
		String_t* L_58 = ((StackTraceUtility_t6_230_StaticFields*)StackTraceUtility_t6_230_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0;
		NullCheck(L_58);
		int32_t L_59 = String_get_Length_m1_428(L_58, /*hidden argument*/NULL);
		NullCheck(L_53);
		String_t* L_60 = String_Substring_m1_352(L_53, L_55, ((int32_t)((int32_t)L_57-(int32_t)L_59)), /*hidden argument*/NULL);
		V_9 = L_60;
	}

IL_0182:
	{
		StringBuilder_t1_145 * L_61 = V_0;
		String_t* L_62 = V_9;
		NullCheck(L_61);
		StringBuilder_Append_m1_4346(L_61, L_62, /*hidden argument*/NULL);
		StringBuilder_t1_145 * L_63 = V_0;
		NullCheck(L_63);
		StringBuilder_Append_m1_4346(L_63, _stringLiteral633, /*hidden argument*/NULL);
		StringBuilder_t1_145 * L_64 = V_0;
		StackFrame_t1_197 * L_65 = V_2;
		NullCheck(L_65);
		int32_t L_66 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber() */, L_65);
		V_10 = L_66;
		String_t* L_67 = Int32_ToString_m1_63((&V_10), /*hidden argument*/NULL);
		NullCheck(L_64);
		StringBuilder_Append_m1_4346(L_64, L_67, /*hidden argument*/NULL);
		StringBuilder_t1_145 * L_68 = V_0;
		NullCheck(L_68);
		StringBuilder_Append_m1_4346(L_68, _stringLiteral93, /*hidden argument*/NULL);
	}

IL_01b9:
	{
		StringBuilder_t1_145 * L_69 = V_0;
		NullCheck(L_69);
		StringBuilder_Append_m1_4346(L_69, _stringLiteral2802, /*hidden argument*/NULL);
	}

IL_01c5:
	{
		int32_t L_70 = V_1;
		V_1 = ((int32_t)((int32_t)L_70+(int32_t)1));
	}

IL_01c9:
	{
		int32_t L_71 = V_1;
		StackTrace_t1_199 * L_72 = ___stackTrace;
		NullCheck(L_72);
		int32_t L_73 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_72);
		if ((((int32_t)L_71) < ((int32_t)L_73)))
		{
			goto IL_0012;
		}
	}
	{
		StringBuilder_t1_145 * L_74 = V_0;
		NullCheck(L_74);
		String_t* L_75 = StringBuilder_ToString_m1_4341(L_74, /*hidden argument*/NULL);
		return L_75;
	}
}
// System.Void UnityEngine.UnityException::.ctor()
extern Il2CppCodeGenString* _stringLiteral2879;
extern "C" void UnityException__ctor_m6_1528 (UnityException_t6_231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2879 = il2cpp_codegen_string_literal_from_index(2879);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception__ctor_m1_932(__this, _stringLiteral2879, /*hidden argument*/NULL);
		Exception_set_HResult_m1_936(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern "C" void UnityException__ctor_m6_1529 (UnityException_t6_231 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception__ctor_m1_932(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_936(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern "C" void UnityException__ctor_m6_1530 (UnityException_t6_231 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___innerException;
		Exception__ctor_m1_934(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_936(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnityException__ctor_m6_1531 (UnityException_t6_231 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_177 * L_0 = ___info;
		StreamingContext_t1_514  L_1 = ___context;
		Exception__ctor_m1_933(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern "C" void SharedBetweenAnimatorsAttribute__ctor_m6_1532 (SharedBetweenAnimatorsAttribute_t6_232 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
extern "C" void StateMachineBehaviour__ctor_m6_1533 (StateMachineBehaviour_t6_233 * __this, const MethodInfo* method)
{
	{
		ScriptableObject__ctor_m6_16(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SystemClock::.cctor()
extern TypeInfo* SystemClock_t6_234_il2cpp_TypeInfo_var;
extern "C" void SystemClock__cctor_m6_1534 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SystemClock_t6_234_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1034);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t1_127  L_0 = {0};
		DateTime__ctor_m1_4905(&L_0, ((int32_t)1970), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		((SystemClock_t6_234_StaticFields*)SystemClock_t6_234_il2cpp_TypeInfo_var->static_fields)->___s_Epoch_0 = L_0;
		return;
	}
}
// System.DateTime UnityEngine.SystemClock::get_now()
extern TypeInfo* DateTime_t1_127_il2cpp_TypeInfo_var;
extern "C" DateTime_t1_127  SystemClock_get_now_m6_1535 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_127_il2cpp_TypeInfo_var);
		DateTime_t1_127  L_0 = DateTime_get_Now_m1_4932(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::.ctor()
extern TypeInfo* GUIContent_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_166_il2cpp_TypeInfo_var;
extern "C" void TextEditor__ctor_m6_1536 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(985);
		GUIStyle_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_163 * L_0 = (GUIContent_t6_163 *)il2cpp_codegen_object_new (GUIContent_t6_163_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1099(L_0, /*hidden argument*/NULL);
		__this->___content_2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_166_il2cpp_TypeInfo_var);
		GUIStyle_t6_166 * L_1 = GUIStyle_get_none_m6_1316(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___style_3 = L_1;
		Vector2_t6_48  L_2 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___scrollOffset_8 = L_2;
		__this->___m_iAltCursorPos_19 = (-1);
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect UnityEngine.TextEditor::get_position()
extern "C" Rect_t6_52  TextEditor_get_position_m6_1537 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		Rect_t6_52  L_0 = (__this->___m_Position_9);
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::set_position(UnityEngine.Rect)
extern "C" void TextEditor_set_position_m6_1538 (TextEditor_t6_238 * __this, Rect_t6_52  ___value, const MethodInfo* method)
{
	{
		Rect_t6_52  L_0 = (__this->___m_Position_9);
		Rect_t6_52  L_1 = ___value;
		bool L_2 = Rect_op_Equality_m6_290(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Rect_t6_52  L_3 = ___value;
		__this->___m_Position_9 = L_3;
		TextEditor_UpdateScrollOffset_m6_1608(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::get_cursorIndex()
extern "C" int32_t TextEditor_get_cursorIndex_m6_1539 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_CursorIndex_10);
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::set_cursorIndex(System.Int32)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" void TextEditor_set_cursorIndex_m6_1540 (TextEditor_t6_238 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___m_CursorIndex_10);
		V_0 = L_0;
		int32_t L_1 = ___value;
		GUIContent_t6_163 * L_2 = (__this->___content_2);
		NullCheck(L_2);
		String_t* L_3 = GUIContent_get_text_m6_1103(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1_428(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Clamp_m6_390(NULL /*static, unused*/, L_1, 0, L_4, /*hidden argument*/NULL);
		__this->___m_CursorIndex_10 = L_5;
		int32_t L_6 = (__this->___m_CursorIndex_10);
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_0037;
		}
	}
	{
		__this->___m_RevealCursor_12 = 1;
	}

IL_0037:
	{
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::get_selectIndex()
extern "C" int32_t TextEditor_get_selectIndex_m6_1541 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_SelectIndex_11);
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::set_selectIndex(System.Int32)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" void TextEditor_set_selectIndex_m6_1542 (TextEditor_t6_238 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		GUIContent_t6_163 * L_1 = (__this->___content_2);
		NullCheck(L_1);
		String_t* L_2 = GUIContent_get_text_m6_1103(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_428(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_Clamp_m6_390(NULL /*static, unused*/, L_0, 0, L_3, /*hidden argument*/NULL);
		__this->___m_SelectIndex_11 = L_4;
		return;
	}
}
// System.Void UnityEngine.TextEditor::ClearCursorPos()
extern "C" void TextEditor_ClearCursorPos_m6_1543 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		__this->___hasHorizontalCursorPos_5 = 0;
		__this->___m_iAltCursorPos_19 = (-1);
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnFocus()
extern "C" void TextEditor_OnFocus_m6_1544 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = (__this->___multiline_4);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		V_0 = 0;
		int32_t L_1 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_2, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		TextEditor_SelectAll_m6_1553(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		__this->___m_HasFocus_7 = 1;
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnLostFocus()
extern "C" void TextEditor_OnLostFocus_m6_1545 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		__this->___m_HasFocus_7 = 0;
		Vector2_t6_48  L_0 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___scrollOffset_8 = L_0;
		return;
	}
}
// System.Void UnityEngine.TextEditor::GrabGraphicalCursorPos()
extern "C" void TextEditor_GrabGraphicalCursorPos_m6_1546 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___hasHorizontalCursorPos_5);
		if (L_0)
		{
			goto IL_0058;
		}
	}
	{
		GUIStyle_t6_166 * L_1 = (__this->___style_3);
		Rect_t6_52  L_2 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_3 = (__this->___content_2);
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t6_48  L_5 = GUIStyle_GetCursorPixelPosition_m6_1317(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		__this->___graphicalCursorPos_13 = L_5;
		GUIStyle_t6_166 * L_6 = (__this->___style_3);
		Rect_t6_52  L_7 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_8 = (__this->___content_2);
		int32_t L_9 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_t6_48  L_10 = GUIStyle_GetCursorPixelPosition_m6_1317(L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->___graphicalSelectCursorPos_14 = L_10;
		__this->___hasHorizontalCursorPos_5 = 0;
	}

IL_0058:
	{
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::HandleKeyEvent(UnityEngine.Event)
extern TypeInfo* TextEditor_t6_238_il2cpp_TypeInfo_var;
extern "C" bool TextEditor_HandleKeyEvent_m6_1547 (TextEditor_t6_238 * __this, Event_t6_154 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditor_t6_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(986);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		TextEditor_InitKeyActions_m6_1617(__this, /*hidden argument*/NULL);
		Event_t6_154 * L_0 = ___e;
		NullCheck(L_0);
		int32_t L_1 = Event_get_modifiers_m6_1014(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t6_154 * L_2 = ___e;
		Event_t6_154 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = Event_get_modifiers_m6_1014(L_3, /*hidden argument*/NULL);
		NullCheck(L_3);
		Event_set_modifiers_m6_1015(L_3, ((int32_t)((int32_t)L_4&(int32_t)((int32_t)-33))), /*hidden argument*/NULL);
		Dictionary_2_t1_920 * L_5 = ((TextEditor_t6_238_StaticFields*)TextEditor_t6_238_il2cpp_TypeInfo_var->static_fields)->___s_Keyactions_23;
		Event_t6_154 * L_6 = ___e;
		NullCheck(L_5);
		bool L_7 = (bool)VirtFuncInvoker1< bool, Event_t6_154 * >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKey(!0) */, L_5, L_6);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		Dictionary_2_t1_920 * L_8 = ((TextEditor_t6_238_StaticFields*)TextEditor_t6_238_il2cpp_TypeInfo_var->static_fields)->___s_Keyactions_23;
		Event_t6_154 * L_9 = ___e;
		NullCheck(L_8);
		int32_t L_10 = (int32_t)VirtFuncInvoker1< int32_t, Event_t6_154 * >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Item(!0) */, L_8, L_9);
		V_1 = L_10;
		int32_t L_11 = V_1;
		TextEditor_PerformOperation_m6_1610(__this, L_11, /*hidden argument*/NULL);
		Event_t6_154 * L_12 = ___e;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		Event_set_modifiers_m6_1015(L_12, L_13, /*hidden argument*/NULL);
		return 1;
	}

IL_0049:
	{
		Event_t6_154 * L_14 = ___e;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		Event_set_modifiers_m6_1015(L_14, L_15, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteLineBack()
extern "C" bool TextEditor_DeleteLineBack_m6_1548 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m6_1555(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m6_1556(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = L_2;
		goto IL_0043;
	}

IL_0022:
	{
		GUIContent_t6_163 * L_3 = (__this->___content_2);
		NullCheck(L_3);
		String_t* L_4 = GUIContent_get_text_m6_1103(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		uint16_t L_6 = String_get_Chars_m1_341(L_4, L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_7 = V_1;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		goto IL_004d;
	}

IL_0043:
	{
		int32_t L_8 = V_1;
		int32_t L_9 = L_8;
		V_1 = ((int32_t)((int32_t)L_9-(int32_t)1));
		if (L_9)
		{
			goto IL_0022;
		}
	}

IL_004d:
	{
		int32_t L_10 = V_1;
		if ((!(((uint32_t)L_10) == ((uint32_t)(-1)))))
		{
			goto IL_0056;
		}
	}
	{
		V_0 = 0;
	}

IL_0056:
	{
		int32_t L_11 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) == ((int32_t)L_12)))
		{
			goto IL_0098;
		}
	}
	{
		GUIContent_t6_163 * L_13 = (__this->___content_2);
		GUIContent_t6_163 * L_14 = (__this->___content_2);
		NullCheck(L_14);
		String_t* L_15 = GUIContent_get_text_m6_1103(L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		int32_t L_17 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_18 = V_0;
		NullCheck(L_15);
		String_t* L_19 = String_Remove_m1_404(L_15, L_16, ((int32_t)((int32_t)L_17-(int32_t)L_18)), /*hidden argument*/NULL);
		NullCheck(L_13);
		GUIContent_set_text_m6_1104(L_13, L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_0;
		V_2 = L_20;
		int32_t L_21 = V_2;
		TextEditor_set_cursorIndex_m6_1540(__this, L_21, /*hidden argument*/NULL);
		int32_t L_22 = V_2;
		TextEditor_set_selectIndex_m6_1542(__this, L_22, /*hidden argument*/NULL);
		return 1;
	}

IL_0098:
	{
		return 0;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteWordBack()
extern "C" bool TextEditor_DeleteWordBack_m6_1549 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m6_1555(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m6_1556(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_FindEndOfPreviousWord_m6_1594(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0063;
		}
	}
	{
		GUIContent_t6_163 * L_5 = (__this->___content_2);
		GUIContent_t6_163 * L_6 = (__this->___content_2);
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m6_1103(L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		int32_t L_9 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		NullCheck(L_7);
		String_t* L_11 = String_Remove_m1_404(L_7, L_8, ((int32_t)((int32_t)L_9-(int32_t)L_10)), /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIContent_set_text_m6_1104(L_5, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		V_1 = L_12;
		int32_t L_13 = V_1;
		TextEditor_set_cursorIndex_m6_1540(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		TextEditor_set_selectIndex_m6_1542(__this, L_14, /*hidden argument*/NULL);
		return 1;
	}

IL_0063:
	{
		return 0;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteWordForward()
extern "C" bool TextEditor_DeleteWordForward_m6_1550 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m6_1555(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m6_1556(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_FindStartOfNextWord_m6_1593(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_4 = (__this->___content_2);
		NullCheck(L_4);
		String_t* L_5 = GUIContent_get_text_m6_1103(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1_428(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)L_6)))
		{
			goto IL_0067;
		}
	}
	{
		GUIContent_t6_163 * L_7 = (__this->___content_2);
		GUIContent_t6_163 * L_8 = (__this->___content_2);
		NullCheck(L_8);
		String_t* L_9 = GUIContent_get_text_m6_1103(L_8, /*hidden argument*/NULL);
		int32_t L_10 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		int32_t L_12 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_13 = String_Remove_m1_404(L_9, L_10, ((int32_t)((int32_t)L_11-(int32_t)L_12)), /*hidden argument*/NULL);
		NullCheck(L_7);
		GUIContent_set_text_m6_1104(L_7, L_13, /*hidden argument*/NULL);
		return 1;
	}

IL_0067:
	{
		return 0;
	}
}
// System.Boolean UnityEngine.TextEditor::Delete()
extern "C" bool TextEditor_Delete_m6_1551 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		bool L_0 = TextEditor_get_hasSelection_m6_1555(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m6_1556(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_2 = (__this->___content_2);
		NullCheck(L_2);
		String_t* L_3 = GUIContent_get_text_m6_1103(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1_428(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_4)))
		{
			goto IL_0053;
		}
	}
	{
		GUIContent_t6_163 * L_5 = (__this->___content_2);
		GUIContent_t6_163 * L_6 = (__this->___content_2);
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m6_1103(L_6, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_9 = String_Remove_m1_404(L_7, L_8, 1, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIContent_set_text_m6_1104(L_5, L_9, /*hidden argument*/NULL);
		return 1;
	}

IL_0053:
	{
		return 0;
	}
}
// System.Boolean UnityEngine.TextEditor::Backspace()
extern "C" bool TextEditor_Backspace_m6_1552 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m6_1555(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m6_1556(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0063;
		}
	}
	{
		GUIContent_t6_163 * L_2 = (__this->___content_2);
		GUIContent_t6_163 * L_3 = (__this->___content_2);
		NullCheck(L_3);
		String_t* L_4 = GUIContent_get_text_m6_1103(L_3, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_6 = String_Remove_m1_404(L_4, ((int32_t)((int32_t)L_5-(int32_t)1)), 1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIContent_set_text_m6_1104(L_2, L_6, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_7-(int32_t)1));
		int32_t L_8 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_9, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0063:
	{
		return 0;
	}
}
// System.Void UnityEngine.TextEditor::SelectAll()
extern "C" void TextEditor_SelectAll_m6_1553 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		TextEditor_set_cursorIndex_m6_1540(__this, 0, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1103(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_428(L_1, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_2, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectNone()
extern "C" void TextEditor_SelectNone_m6_1554 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_0, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::get_hasSelection()
extern "C" bool TextEditor_get_hasSelection_m6_1555 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextEditor_DeleteSelection_m6_1556 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1103(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_428(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_5 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_5, /*hidden argument*/NULL);
	}

IL_0024:
	{
		int32_t L_6 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) <= ((int32_t)L_7)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_8 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_8, /*hidden argument*/NULL);
	}

IL_0037:
	{
		int32_t L_9 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_10 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_004a;
		}
	}
	{
		return 0;
	}

IL_004a:
	{
		int32_t L_11 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_12 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_00c0;
		}
	}
	{
		GUIContent_t6_163 * L_13 = (__this->___content_2);
		GUIContent_t6_163 * L_14 = (__this->___content_2);
		NullCheck(L_14);
		String_t* L_15 = GUIContent_get_text_m6_1103(L_14, /*hidden argument*/NULL);
		int32_t L_16 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_17 = String_Substring_m1_352(L_15, 0, L_16, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_18 = (__this->___content_2);
		NullCheck(L_18);
		String_t* L_19 = GUIContent_get_text_m6_1103(L_18, /*hidden argument*/NULL);
		int32_t L_20 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_21 = (__this->___content_2);
		NullCheck(L_21);
		String_t* L_22 = GUIContent_get_text_m6_1103(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m1_428(L_22, /*hidden argument*/NULL);
		int32_t L_24 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_25 = String_Substring_m1_352(L_19, L_20, ((int32_t)((int32_t)L_23-(int32_t)L_24)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1_418(NULL /*static, unused*/, L_17, L_25, /*hidden argument*/NULL);
		NullCheck(L_13);
		GUIContent_set_text_m6_1104(L_13, L_26, /*hidden argument*/NULL);
		int32_t L_27 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_27, /*hidden argument*/NULL);
		goto IL_0120;
	}

IL_00c0:
	{
		GUIContent_t6_163 * L_28 = (__this->___content_2);
		GUIContent_t6_163 * L_29 = (__this->___content_2);
		NullCheck(L_29);
		String_t* L_30 = GUIContent_get_text_m6_1103(L_29, /*hidden argument*/NULL);
		int32_t L_31 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		NullCheck(L_30);
		String_t* L_32 = String_Substring_m1_352(L_30, 0, L_31, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_33 = (__this->___content_2);
		NullCheck(L_33);
		String_t* L_34 = GUIContent_get_text_m6_1103(L_33, /*hidden argument*/NULL);
		int32_t L_35 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_36 = (__this->___content_2);
		NullCheck(L_36);
		String_t* L_37 = GUIContent_get_text_m6_1103(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = String_get_Length_m1_428(L_37, /*hidden argument*/NULL);
		int32_t L_39 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		String_t* L_40 = String_Substring_m1_352(L_34, L_35, ((int32_t)((int32_t)L_38-(int32_t)L_39)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m1_418(NULL /*static, unused*/, L_32, L_40, /*hidden argument*/NULL);
		NullCheck(L_28);
		GUIContent_set_text_m6_1104(L_28, L_41, /*hidden argument*/NULL);
		int32_t L_42 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_42, /*hidden argument*/NULL);
	}

IL_0120:
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
extern "C" void TextEditor_ReplaceSelection_m6_1557 (TextEditor_t6_238 * __this, String_t* ___replace, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_DeleteSelection_m6_1556(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		GUIContent_t6_163 * L_1 = (__this->___content_2);
		NullCheck(L_1);
		String_t* L_2 = GUIContent_get_text_m6_1103(L_1, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		String_t* L_4 = ___replace;
		NullCheck(L_2);
		String_t* L_5 = String_Insert_m1_424(L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIContent_set_text_m6_1104(L_0, L_5, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		String_t* L_7 = ___replace;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m1_428(L_7, /*hidden argument*/NULL);
		int32_t L_9 = ((int32_t)((int32_t)L_6+(int32_t)L_8));
		V_0 = L_9;
		TextEditor_set_cursorIndex_m6_1540(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_10, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::Insert(System.Char)
extern "C" void TextEditor_Insert_m6_1558 (TextEditor_t6_238 * __this, uint16_t ___c, const MethodInfo* method)
{
	{
		String_t* L_0 = Char_ToString_m1_314((&___c), /*hidden argument*/NULL);
		TextEditor_ReplaceSelection_m6_1557(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveRight()
extern "C" void TextEditor_MoveRight_m6_1559 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/NULL);
		TextEditor_DetectFocusChange_m6_1618(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_3, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_003c:
	{
		int32_t L_4 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_5)))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_6 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_6, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_005e:
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_7, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLeft()
extern "C" void TextEditor_MoveLeft_m6_1560 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_3, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0030:
	{
		int32_t L_4 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_5)))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_6 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_6, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0052:
	{
		int32_t L_7 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_7, /*hidden argument*/NULL);
	}

IL_005e:
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveUp()
extern "C" void TextEditor_MoveUp_m6_1561 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_2, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0022:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		TextEditor_GrabGraphicalCursorPos_m6_1546(__this, /*hidden argument*/NULL);
		Vector2_t6_48 * L_4 = &(__this->___graphicalCursorPos_13);
		Vector2_t6_48 * L_5 = L_4;
		float L_6 = (L_5->___y_2);
		L_5->___y_2 = ((float)((float)L_6-(float)(1.0f)));
		GUIStyle_t6_166 * L_7 = (__this->___style_3);
		Rect_t6_52  L_8 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_9 = (__this->___content_2);
		Vector2_t6_48  L_10 = (__this->___graphicalCursorPos_13);
		NullCheck(L_7);
		int32_t L_11 = GUIStyle_GetCursorStringIndex_m6_1318(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		int32_t L_12 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_14) > ((int32_t)0)))
		{
			goto IL_0089;
		}
	}
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveDown()
extern "C" void TextEditor_MoveDown_m6_1562 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_2, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0022:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		TextEditor_GrabGraphicalCursorPos_m6_1546(__this, /*hidden argument*/NULL);
		Vector2_t6_48 * L_4 = &(__this->___graphicalCursorPos_13);
		Vector2_t6_48 * L_5 = L_4;
		float L_6 = (L_5->___y_2);
		GUIStyle_t6_166 * L_7 = (__this->___style_3);
		NullCheck(L_7);
		float L_8 = GUIStyle_get_lineHeight_m6_1307(L_7, /*hidden argument*/NULL);
		L_5->___y_2 = ((float)((float)L_6+(float)((float)((float)L_8+(float)(5.0f)))));
		GUIStyle_t6_166 * L_9 = (__this->___style_3);
		Rect_t6_52  L_10 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_11 = (__this->___content_2);
		Vector2_t6_48  L_12 = (__this->___graphicalCursorPos_13);
		NullCheck(L_9);
		int32_t L_13 = GUIStyle_GetCursorStringIndex_m6_1318(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_15, /*hidden argument*/NULL);
		int32_t L_16 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_17 = (__this->___content_2);
		NullCheck(L_17);
		String_t* L_18 = GUIContent_get_text_m6_1103(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = String_get_Length_m1_428(L_18, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)L_19))))
		{
			goto IL_00a4;
		}
	}
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLineStart()
extern "C" void TextEditor_MoveLineStart_m6_1563 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0022;
	}

IL_001c:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0022:
	{
		V_0 = G_B3_0;
		int32_t L_4 = V_0;
		V_1 = L_4;
		goto IL_0055;
	}

IL_002a:
	{
		GUIContent_t6_163 * L_5 = (__this->___content_2);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m6_1103(L_5, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		NullCheck(L_6);
		uint16_t L_8 = String_get_Chars_m1_341(L_6, L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_9 = V_1;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
		int32_t L_10 = V_2;
		TextEditor_set_cursorIndex_m6_1540(__this, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_2;
		TextEditor_set_selectIndex_m6_1542(__this, L_11, /*hidden argument*/NULL);
		return;
	}

IL_0055:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = L_12;
		V_1 = ((int32_t)((int32_t)L_13-(int32_t)1));
		if (L_13)
		{
			goto IL_002a;
		}
	}
	{
		V_2 = 0;
		int32_t L_14 = V_2;
		TextEditor_set_cursorIndex_m6_1540(__this, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_2;
		TextEditor_set_selectIndex_m6_1542(__this, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLineEnd()
extern "C" void TextEditor_MoveLineEnd_m6_1564 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0022;
	}

IL_001c:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0022:
	{
		V_0 = G_B3_0;
		int32_t L_4 = V_0;
		V_1 = L_4;
		GUIContent_t6_163 * L_5 = (__this->___content_2);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m6_1103(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1_428(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_0068;
	}

IL_003b:
	{
		GUIContent_t6_163 * L_8 = (__this->___content_2);
		NullCheck(L_8);
		String_t* L_9 = GUIContent_get_text_m6_1103(L_8, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		uint16_t L_11 = String_get_Chars_m1_341(L_9, L_10, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = V_1;
		V_3 = L_12;
		int32_t L_13 = V_3;
		TextEditor_set_cursorIndex_m6_1540(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_3;
		TextEditor_set_selectIndex_m6_1542(__this, L_14, /*hidden argument*/NULL);
		return;
	}

IL_0064:
	{
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = V_2;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_18 = V_2;
		V_3 = L_18;
		int32_t L_19 = V_3;
		TextEditor_set_cursorIndex_m6_1540(__this, L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_3;
		TextEditor_set_selectIndex_m6_1542(__this, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveGraphicalLineStart()
extern "C" void TextEditor_MoveGraphicalLineStart_m6_1565 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_238 * G_B2_0 = {0};
	TextEditor_t6_238 * G_B2_1 = {0};
	TextEditor_t6_238 * G_B1_0 = {0};
	TextEditor_t6_238 * G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	TextEditor_t6_238 * G_B3_1 = {0};
	TextEditor_t6_238 * G_B3_2 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		G_B1_1 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			G_B2_1 = __this;
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0024;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		int32_t L_4 = TextEditor_GetGraphicalLineStart_m6_1582(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(G_B3_2);
		TextEditor_set_cursorIndex_m6_1540(G_B3_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveGraphicalLineEnd()
extern "C" void TextEditor_MoveGraphicalLineEnd_m6_1566 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_238 * G_B2_0 = {0};
	TextEditor_t6_238 * G_B2_1 = {0};
	TextEditor_t6_238 * G_B1_0 = {0};
	TextEditor_t6_238 * G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	TextEditor_t6_238 * G_B3_1 = {0};
	TextEditor_t6_238 * G_B3_2 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		G_B1_1 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			G_B2_1 = __this;
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0024;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		int32_t L_4 = TextEditor_GetGraphicalLineEnd_m6_1583(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(G_B3_2);
		TextEditor_set_cursorIndex_m6_1540(G_B3_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveTextStart()
extern "C" void TextEditor_MoveTextStart_m6_1567 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveTextEnd()
extern "C" void TextEditor_MoveTextEnd_m6_1568 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1103(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_428(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::IndexOfEndOfLine(System.Int32)
extern "C" int32_t TextEditor_IndexOfEndOfLine_m6_1569 (TextEditor_t6_238 * __this, int32_t ___startIndex, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1103(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___startIndex;
		NullCheck(L_1);
		int32_t L_3 = String_IndexOf_m1_382(L_1, ((int32_t)10), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_5 = V_0;
		G_B3_0 = L_5;
		goto IL_0031;
	}

IL_0021:
	{
		GUIContent_t6_163 * L_6 = (__this->___content_2);
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m6_1103(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m1_428(L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0031:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextEditor::MoveParagraphForward()
extern "C" void TextEditor_MoveParagraphForward_m6_1570 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_238 * G_B2_0 = {0};
	TextEditor_t6_238 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	TextEditor_t6_238 * G_B3_1 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m6_1540(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_5 = (__this->___content_2);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m6_1103(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1_428(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_4) >= ((int32_t)L_7)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_8 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_IndexOfEndOfLine_m6_1569(__this, ((int32_t)((int32_t)L_8+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_9;
		int32_t L_10 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_11, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveParagraphBackward()
extern "C" void TextEditor_MoveParagraphBackward_m6_1571 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_238 * G_B2_0 = {0};
	TextEditor_t6_238 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	TextEditor_t6_238 * G_B3_1 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m6_1540(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)1)))
		{
			goto IL_0064;
		}
	}
	{
		GUIContent_t6_163 * L_5 = (__this->___content_2);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m6_1103(L_5, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_8 = String_LastIndexOf_m1_391(L_6, ((int32_t)10), ((int32_t)((int32_t)L_7-(int32_t)2)), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
		int32_t L_9 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_10, /*hidden argument*/NULL);
		goto IL_0074;
	}

IL_0064:
	{
		V_0 = 0;
		int32_t L_11 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_12, /*hidden argument*/NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveCursorToPosition(UnityEngine.Vector2)
extern "C" void TextEditor_MoveCursorToPosition_m6_1572 (TextEditor_t6_238 * __this, Vector2_t6_48  ___cursorPosition, const MethodInfo* method)
{
	{
		GUIStyle_t6_166 * L_0 = (__this->___style_3);
		Rect_t6_52  L_1 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_2 = (__this->___content_2);
		Vector2_t6_48  L_3 = ___cursorPosition;
		Vector2_t6_48  L_4 = (__this->___scrollOffset_8);
		Vector2_t6_48  L_5 = Vector2_op_Addition_m6_184(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_6 = GUIStyle_GetCursorStringIndex_m6_1318(L_0, L_1, L_2, L_5, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_6, /*hidden argument*/NULL);
		Event_t6_154 * L_7 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Event_get_shift_m6_976(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_9 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_9, /*hidden argument*/NULL);
	}

IL_0044:
	{
		TextEditor_DetectFocusChange_m6_1618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToPosition(UnityEngine.Vector2)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" void TextEditor_SelectToPosition_m6_1573 (TextEditor_t6_238 * __this, Vector2_t6_48  ___cursorPosition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = (__this->___m_MouseDragSelectsWholeWords_15);
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		GUIStyle_t6_166 * L_1 = (__this->___style_3);
		Rect_t6_52  L_2 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_3 = (__this->___content_2);
		Vector2_t6_48  L_4 = ___cursorPosition;
		Vector2_t6_48  L_5 = (__this->___scrollOffset_8);
		Vector2_t6_48  L_6 = Vector2_op_Addition_m6_184(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_7 = GUIStyle_GetCursorStringIndex_m6_1318(L_1, L_2, L_3, L_6, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_7, /*hidden argument*/NULL);
		goto IL_01b6;
	}

IL_0039:
	{
		GUIStyle_t6_166 * L_8 = (__this->___style_3);
		Rect_t6_52  L_9 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_10 = (__this->___content_2);
		Vector2_t6_48  L_11 = ___cursorPosition;
		Vector2_t6_48  L_12 = (__this->___scrollOffset_8);
		Vector2_t6_48  L_13 = Vector2_op_Addition_m6_184(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_14 = GUIStyle_GetCursorStringIndex_m6_1318(L_8, L_9, L_10, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		uint8_t L_15 = (__this->___m_DblClickSnap_17);
		if (L_15)
		{
			goto IL_00eb;
		}
	}
	{
		int32_t L_16 = V_0;
		int32_t L_17 = (__this->___m_DblClickInitPos_16);
		if ((((int32_t)L_16) >= ((int32_t)L_17)))
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_18 = V_0;
		int32_t L_19 = TextEditor_FindEndOfClassification_m6_1605(__this, L_18, (-1), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_19, /*hidden argument*/NULL);
		int32_t L_20 = (__this->___m_DblClickInitPos_16);
		int32_t L_21 = TextEditor_FindEndOfClassification_m6_1605(__this, L_20, 1, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_21, /*hidden argument*/NULL);
		goto IL_00e6;
	}

IL_009a:
	{
		int32_t L_22 = V_0;
		GUIContent_t6_163 * L_23 = (__this->___content_2);
		NullCheck(L_23);
		String_t* L_24 = GUIContent_get_text_m6_1103(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		int32_t L_25 = String_get_Length_m1_428(L_24, /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)L_25)))
		{
			goto IL_00c3;
		}
	}
	{
		GUIContent_t6_163 * L_26 = (__this->___content_2);
		NullCheck(L_26);
		String_t* L_27 = GUIContent_get_text_m6_1103(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		int32_t L_28 = String_get_Length_m1_428(L_27, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_28-(int32_t)1));
	}

IL_00c3:
	{
		int32_t L_29 = V_0;
		int32_t L_30 = TextEditor_FindEndOfClassification_m6_1605(__this, L_29, 1, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_30, /*hidden argument*/NULL);
		int32_t L_31 = (__this->___m_DblClickInitPos_16);
		int32_t L_32 = TextEditor_FindEndOfClassification_m6_1605(__this, ((int32_t)((int32_t)L_31-(int32_t)1)), (-1), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_32, /*hidden argument*/NULL);
	}

IL_00e6:
	{
		goto IL_01b6;
	}

IL_00eb:
	{
		int32_t L_33 = V_0;
		int32_t L_34 = (__this->___m_DblClickInitPos_16);
		if ((((int32_t)L_33) >= ((int32_t)L_34)))
		{
			goto IL_0150;
		}
	}
	{
		int32_t L_35 = V_0;
		if ((((int32_t)L_35) <= ((int32_t)0)))
		{
			goto IL_0126;
		}
	}
	{
		GUIContent_t6_163 * L_36 = (__this->___content_2);
		NullCheck(L_36);
		String_t* L_37 = GUIContent_get_text_m6_1103(L_36, /*hidden argument*/NULL);
		int32_t L_38 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_39 = Mathf_Max_m6_384(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_38-(int32_t)2)), /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_40 = String_LastIndexOf_m1_391(L_37, ((int32_t)10), L_39, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, ((int32_t)((int32_t)L_40+(int32_t)1)), /*hidden argument*/NULL);
		goto IL_012d;
	}

IL_0126:
	{
		TextEditor_set_cursorIndex_m6_1540(__this, 0, /*hidden argument*/NULL);
	}

IL_012d:
	{
		GUIContent_t6_163 * L_41 = (__this->___content_2);
		NullCheck(L_41);
		String_t* L_42 = GUIContent_get_text_m6_1103(L_41, /*hidden argument*/NULL);
		int32_t L_43 = (__this->___m_DblClickInitPos_16);
		NullCheck(L_42);
		int32_t L_44 = String_LastIndexOf_m1_391(L_42, ((int32_t)10), L_43, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_44, /*hidden argument*/NULL);
		goto IL_01b6;
	}

IL_0150:
	{
		int32_t L_45 = V_0;
		GUIContent_t6_163 * L_46 = (__this->___content_2);
		NullCheck(L_46);
		String_t* L_47 = GUIContent_get_text_m6_1103(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		int32_t L_48 = String_get_Length_m1_428(L_47, /*hidden argument*/NULL);
		if ((((int32_t)L_45) >= ((int32_t)L_48)))
		{
			goto IL_0178;
		}
	}
	{
		int32_t L_49 = V_0;
		int32_t L_50 = TextEditor_IndexOfEndOfLine_m6_1569(__this, L_49, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_50, /*hidden argument*/NULL);
		goto IL_018e;
	}

IL_0178:
	{
		GUIContent_t6_163 * L_51 = (__this->___content_2);
		NullCheck(L_51);
		String_t* L_52 = GUIContent_get_text_m6_1103(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		int32_t L_53 = String_get_Length_m1_428(L_52, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_53, /*hidden argument*/NULL);
	}

IL_018e:
	{
		GUIContent_t6_163 * L_54 = (__this->___content_2);
		NullCheck(L_54);
		String_t* L_55 = GUIContent_get_text_m6_1103(L_54, /*hidden argument*/NULL);
		int32_t L_56 = (__this->___m_DblClickInitPos_16);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_57 = Mathf_Max_m6_384(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_56-(int32_t)2)), /*hidden argument*/NULL);
		NullCheck(L_55);
		int32_t L_58 = String_LastIndexOf_m1_391(L_55, ((int32_t)10), L_57, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, ((int32_t)((int32_t)L_58+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_01b6:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectLeft()
extern "C" void TextEditor_SelectLeft_m6_1574 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = (__this->___m_bJustSelected_18);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		__this->___m_bJustSelected_18 = 0;
		int32_t L_6 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectRight()
extern "C" void TextEditor_SelectRight_m6_1575 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = (__this->___m_bJustSelected_18);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		__this->___m_bJustSelected_18 = 0;
		int32_t L_6 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		GUIContent_t6_163 * L_7 = (__this->___content_2);
		NullCheck(L_7);
		String_t* L_8 = GUIContent_get_text_m6_1103(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m1_428(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) <= ((int32_t)L_11)))
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_12 = V_1;
		TextEditor_set_cursorIndex_m6_1540(__this, L_12, /*hidden argument*/NULL);
	}

IL_006f:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectUp()
extern "C" void TextEditor_SelectUp_m6_1576 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		TextEditor_GrabGraphicalCursorPos_m6_1546(__this, /*hidden argument*/NULL);
		Vector2_t6_48 * L_0 = &(__this->___graphicalCursorPos_13);
		Vector2_t6_48 * L_1 = L_0;
		float L_2 = (L_1->___y_2);
		L_1->___y_2 = ((float)((float)L_2-(float)(1.0f)));
		GUIStyle_t6_166 * L_3 = (__this->___style_3);
		Rect_t6_52  L_4 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_5 = (__this->___content_2);
		Vector2_t6_48  L_6 = (__this->___graphicalCursorPos_13);
		NullCheck(L_3);
		int32_t L_7 = GUIStyle_GetCursorStringIndex_m6_1318(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectDown()
extern "C" void TextEditor_SelectDown_m6_1577 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		TextEditor_GrabGraphicalCursorPos_m6_1546(__this, /*hidden argument*/NULL);
		Vector2_t6_48 * L_0 = &(__this->___graphicalCursorPos_13);
		Vector2_t6_48 * L_1 = L_0;
		float L_2 = (L_1->___y_2);
		GUIStyle_t6_166 * L_3 = (__this->___style_3);
		NullCheck(L_3);
		float L_4 = GUIStyle_get_lineHeight_m6_1307(L_3, /*hidden argument*/NULL);
		L_1->___y_2 = ((float)((float)L_2+(float)((float)((float)L_4+(float)(5.0f)))));
		GUIStyle_t6_166 * L_5 = (__this->___style_3);
		Rect_t6_52  L_6 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_7 = (__this->___content_2);
		Vector2_t6_48  L_8 = (__this->___graphicalCursorPos_13);
		NullCheck(L_5);
		int32_t L_9 = GUIStyle_GetCursorStringIndex_m6_1318(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectTextEnd()
extern "C" void TextEditor_SelectTextEnd_m6_1578 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1103(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_428(L_1, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectTextStart()
extern "C" void TextEditor_SelectTextStart_m6_1579 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		TextEditor_set_cursorIndex_m6_1540(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MouseDragSelectsWholeWords(System.Boolean)
extern "C" void TextEditor_MouseDragSelectsWholeWords_m6_1580 (TextEditor_t6_238 * __this, bool ___on, const MethodInfo* method)
{
	{
		bool L_0 = ___on;
		__this->___m_MouseDragSelectsWholeWords_15 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		__this->___m_DblClickInitPos_16 = L_1;
		return;
	}
}
// System.Void UnityEngine.TextEditor::DblClickSnap(UnityEngine.TextEditor/DblClickSnapping)
extern "C" void TextEditor_DblClickSnap_m6_1581 (TextEditor_t6_238 * __this, uint8_t ___snapping, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___snapping;
		__this->___m_DblClickSnap_17 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::GetGraphicalLineStart(System.Int32)
extern "C" int32_t TextEditor_GetGraphicalLineStart_m6_1582 (TextEditor_t6_238 * __this, int32_t ___p, const MethodInfo* method)
{
	Vector2_t6_48  V_0 = {0};
	{
		GUIStyle_t6_166 * L_0 = (__this->___style_3);
		Rect_t6_52  L_1 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_2 = (__this->___content_2);
		int32_t L_3 = ___p;
		NullCheck(L_0);
		Vector2_t6_48  L_4 = GUIStyle_GetCursorPixelPosition_m6_1317(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		(&V_0)->___x_1 = (0.0f);
		GUIStyle_t6_166 * L_5 = (__this->___style_3);
		Rect_t6_52  L_6 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_7 = (__this->___content_2);
		Vector2_t6_48  L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = GUIStyle_GetCursorStringIndex_m6_1318(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Int32 UnityEngine.TextEditor::GetGraphicalLineEnd(System.Int32)
extern "C" int32_t TextEditor_GetGraphicalLineEnd_m6_1583 (TextEditor_t6_238 * __this, int32_t ___p, const MethodInfo* method)
{
	Vector2_t6_48  V_0 = {0};
	{
		GUIStyle_t6_166 * L_0 = (__this->___style_3);
		Rect_t6_52  L_1 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_2 = (__this->___content_2);
		int32_t L_3 = ___p;
		NullCheck(L_0);
		Vector2_t6_48  L_4 = GUIStyle_GetCursorPixelPosition_m6_1317(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector2_t6_48 * L_5 = (&V_0);
		float L_6 = (L_5->___x_1);
		L_5->___x_1 = ((float)((float)L_6+(float)(5000.0f)));
		GUIStyle_t6_166 * L_7 = (__this->___style_3);
		Rect_t6_52  L_8 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_9 = (__this->___content_2);
		Vector2_t6_48  L_10 = V_0;
		NullCheck(L_7);
		int32_t L_11 = GUIStyle_GetCursorStringIndex_m6_1318(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Int32 UnityEngine.TextEditor::FindNextSeperator(System.Int32)
extern "C" int32_t TextEditor_FindNextSeperator_m6_1584 (TextEditor_t6_238 * __this, int32_t ___startPos, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1103(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_428(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001b;
	}

IL_0016:
	{
		int32_t L_3 = ___startPos;
		___startPos = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001b:
	{
		int32_t L_4 = ___startPos;
		int32_t L_5 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_003d;
		}
	}
	{
		GUIContent_t6_163 * L_6 = (__this->___content_2);
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m6_1103(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ___startPos;
		NullCheck(L_7);
		uint16_t L_9 = String_get_Chars_m1_341(L_7, L_8, /*hidden argument*/NULL);
		bool L_10 = TextEditor_isLetterLikeChar_m6_1585(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0016;
		}
	}

IL_003d:
	{
		goto IL_0047;
	}

IL_0042:
	{
		int32_t L_11 = ___startPos;
		___startPos = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_12 = ___startPos;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) >= ((int32_t)L_13)))
		{
			goto IL_0069;
		}
	}
	{
		GUIContent_t6_163 * L_14 = (__this->___content_2);
		NullCheck(L_14);
		String_t* L_15 = GUIContent_get_text_m6_1103(L_14, /*hidden argument*/NULL);
		int32_t L_16 = ___startPos;
		NullCheck(L_15);
		uint16_t L_17 = String_get_Chars_m1_341(L_15, L_16, /*hidden argument*/NULL);
		bool L_18 = TextEditor_isLetterLikeChar_m6_1585(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0042;
		}
	}

IL_0069:
	{
		int32_t L_19 = ___startPos;
		return L_19;
	}
}
// System.Boolean UnityEngine.TextEditor::isLetterLikeChar(System.Char)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" bool TextEditor_isLetterLikeChar_m6_1585 (Object_t * __this /* static, unused */, uint16_t ___c, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		uint16_t L_0 = ___c;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsLetterOrDigit_m1_302(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		uint16_t L_2 = ___c;
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)((int32_t)39)))? 1 : 0);
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 1;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.TextEditor::FindPrevSeperator(System.Int32)
extern "C" int32_t TextEditor_FindPrevSeperator_m6_1586 (TextEditor_t6_238 * __this, int32_t ___startPos, const MethodInfo* method)
{
	{
		int32_t L_0 = ___startPos;
		___startPos = ((int32_t)((int32_t)L_0-(int32_t)1));
		goto IL_000f;
	}

IL_000a:
	{
		int32_t L_1 = ___startPos;
		___startPos = ((int32_t)((int32_t)L_1-(int32_t)1));
	}

IL_000f:
	{
		int32_t L_2 = ___startPos;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0031;
		}
	}
	{
		GUIContent_t6_163 * L_3 = (__this->___content_2);
		NullCheck(L_3);
		String_t* L_4 = GUIContent_get_text_m6_1103(L_3, /*hidden argument*/NULL);
		int32_t L_5 = ___startPos;
		NullCheck(L_4);
		uint16_t L_6 = String_get_Chars_m1_341(L_4, L_5, /*hidden argument*/NULL);
		bool L_7 = TextEditor_isLetterLikeChar_m6_1585(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_000a;
		}
	}

IL_0031:
	{
		goto IL_003b;
	}

IL_0036:
	{
		int32_t L_8 = ___startPos;
		___startPos = ((int32_t)((int32_t)L_8-(int32_t)1));
	}

IL_003b:
	{
		int32_t L_9 = ___startPos;
		if ((((int32_t)L_9) < ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		GUIContent_t6_163 * L_10 = (__this->___content_2);
		NullCheck(L_10);
		String_t* L_11 = GUIContent_get_text_m6_1103(L_10, /*hidden argument*/NULL);
		int32_t L_12 = ___startPos;
		NullCheck(L_11);
		uint16_t L_13 = String_get_Chars_m1_341(L_11, L_12, /*hidden argument*/NULL);
		bool L_14 = TextEditor_isLetterLikeChar_m6_1585(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0036;
		}
	}

IL_005d:
	{
		int32_t L_15 = ___startPos;
		return ((int32_t)((int32_t)L_15+(int32_t)1));
	}
}
// System.Void UnityEngine.TextEditor::MoveWordRight()
extern "C" void TextEditor_MoveWordRight_m6_1587 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_238 * G_B2_0 = {0};
	TextEditor_t6_238 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	TextEditor_t6_238 * G_B3_1 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m6_1540(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_FindNextSeperator_m6_1584(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_7, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveToStartOfNextWord()
extern "C" void TextEditor_MoveToStartOfNextWord_m6_1588 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001e;
		}
	}
	{
		TextEditor_MoveRight_m6_1559(__this, /*hidden argument*/NULL);
		return;
	}

IL_001e:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_FindStartOfNextWord_m6_1593(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveToEndOfPreviousWord()
extern "C" void TextEditor_MoveToEndOfPreviousWord_m6_1589 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001e;
		}
	}
	{
		TextEditor_MoveLeft_m6_1560(__this, /*hidden argument*/NULL);
		return;
	}

IL_001e:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_FindEndOfPreviousWord_m6_1594(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToStartOfNextWord()
extern "C" void TextEditor_SelectToStartOfNextWord_m6_1590 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_FindStartOfNextWord_m6_1593(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToEndOfPreviousWord()
extern "C" void TextEditor_SelectToEndOfPreviousWord_m6_1591 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_FindEndOfPreviousWord_m6_1594(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.TextEditor/CharacterType UnityEngine.TextEditor::ClassifyChar(System.Char)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" int32_t TextEditor_ClassifyChar_m6_1592 (TextEditor_t6_238 * __this, uint16_t ___c, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint16_t L_0 = ___c;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsWhiteSpace_m1_305(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (int32_t)(3);
	}

IL_000d:
	{
		uint16_t L_2 = ___c;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_3 = Char_IsLetterOrDigit_m1_302(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0020;
		}
	}
	{
		uint16_t L_4 = ___c;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)39)))))
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (int32_t)(0);
	}

IL_0022:
	{
		return (int32_t)(1);
	}
}
// System.Int32 UnityEngine.TextEditor::FindStartOfNextWord(System.Int32)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" int32_t TextEditor_FindStartOfNextWord_m6_1593 (TextEditor_t6_238 * __this, int32_t ___p, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	uint16_t V_1 = 0x0;
	int32_t V_2 = {0};
	{
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1103(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_428(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = ___p;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_5 = ___p;
		return L_5;
	}

IL_001a:
	{
		GUIContent_t6_163 * L_6 = (__this->___content_2);
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m6_1103(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ___p;
		NullCheck(L_7);
		uint16_t L_9 = String_get_Chars_m1_341(L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		uint16_t L_10 = V_1;
		int32_t L_11 = TextEditor_ClassifyChar_m6_1592(__this, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		int32_t L_12 = V_2;
		if ((((int32_t)L_12) == ((int32_t)3)))
		{
			goto IL_0073;
		}
	}
	{
		int32_t L_13 = ___p;
		___p = ((int32_t)((int32_t)L_13+(int32_t)1));
		goto IL_004a;
	}

IL_0045:
	{
		int32_t L_14 = ___p;
		___p = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_15 = ___p;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) >= ((int32_t)L_16)))
		{
			goto IL_006e;
		}
	}
	{
		GUIContent_t6_163 * L_17 = (__this->___content_2);
		NullCheck(L_17);
		String_t* L_18 = GUIContent_get_text_m6_1103(L_17, /*hidden argument*/NULL);
		int32_t L_19 = ___p;
		NullCheck(L_18);
		uint16_t L_20 = String_get_Chars_m1_341(L_18, L_19, /*hidden argument*/NULL);
		int32_t L_21 = TextEditor_ClassifyChar_m6_1592(__this, L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_2;
		if ((((int32_t)L_21) == ((int32_t)L_22)))
		{
			goto IL_0045;
		}
	}

IL_006e:
	{
		goto IL_0087;
	}

IL_0073:
	{
		uint16_t L_23 = V_1;
		if ((((int32_t)L_23) == ((int32_t)((int32_t)9))))
		{
			goto IL_0083;
		}
	}
	{
		uint16_t L_24 = V_1;
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0087;
		}
	}

IL_0083:
	{
		int32_t L_25 = ___p;
		return ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_0087:
	{
		int32_t L_26 = ___p;
		int32_t L_27 = V_0;
		if ((!(((uint32_t)L_26) == ((uint32_t)L_27))))
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_28 = ___p;
		return L_28;
	}

IL_0090:
	{
		GUIContent_t6_163 * L_29 = (__this->___content_2);
		NullCheck(L_29);
		String_t* L_30 = GUIContent_get_text_m6_1103(L_29, /*hidden argument*/NULL);
		int32_t L_31 = ___p;
		NullCheck(L_30);
		uint16_t L_32 = String_get_Chars_m1_341(L_30, L_31, /*hidden argument*/NULL);
		V_1 = L_32;
		uint16_t L_33 = V_1;
		if ((!(((uint32_t)L_33) == ((uint32_t)((int32_t)32)))))
		{
			goto IL_00db;
		}
	}
	{
		goto IL_00b4;
	}

IL_00af:
	{
		int32_t L_34 = ___p;
		___p = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00b4:
	{
		int32_t L_35 = ___p;
		int32_t L_36 = V_0;
		if ((((int32_t)L_35) >= ((int32_t)L_36)))
		{
			goto IL_00d6;
		}
	}
	{
		GUIContent_t6_163 * L_37 = (__this->___content_2);
		NullCheck(L_37);
		String_t* L_38 = GUIContent_get_text_m6_1103(L_37, /*hidden argument*/NULL);
		int32_t L_39 = ___p;
		NullCheck(L_38);
		uint16_t L_40 = String_get_Chars_m1_341(L_38, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_41 = Char_IsWhiteSpace_m1_305(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_00af;
		}
	}

IL_00d6:
	{
		goto IL_00ed;
	}

IL_00db:
	{
		uint16_t L_42 = V_1;
		if ((((int32_t)L_42) == ((int32_t)((int32_t)9))))
		{
			goto IL_00eb;
		}
	}
	{
		uint16_t L_43 = V_1;
		if ((!(((uint32_t)L_43) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00ed;
		}
	}

IL_00eb:
	{
		int32_t L_44 = ___p;
		return L_44;
	}

IL_00ed:
	{
		int32_t L_45 = ___p;
		return L_45;
	}
}
// System.Int32 UnityEngine.TextEditor::FindEndOfPreviousWord(System.Int32)
extern "C" int32_t TextEditor_FindEndOfPreviousWord_m6_1594 (TextEditor_t6_238 * __this, int32_t ___p, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___p;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_1 = ___p;
		return L_1;
	}

IL_0008:
	{
		int32_t L_2 = ___p;
		___p = ((int32_t)((int32_t)L_2-(int32_t)1));
		goto IL_0017;
	}

IL_0012:
	{
		int32_t L_3 = ___p;
		___p = ((int32_t)((int32_t)L_3-(int32_t)1));
	}

IL_0017:
	{
		int32_t L_4 = ___p;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		GUIContent_t6_163 * L_5 = (__this->___content_2);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m6_1103(L_5, /*hidden argument*/NULL);
		int32_t L_7 = ___p;
		NullCheck(L_6);
		uint16_t L_8 = String_get_Chars_m1_341(L_6, L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)((int32_t)32))))
		{
			goto IL_0012;
		}
	}

IL_0036:
	{
		GUIContent_t6_163 * L_9 = (__this->___content_2);
		NullCheck(L_9);
		String_t* L_10 = GUIContent_get_text_m6_1103(L_9, /*hidden argument*/NULL);
		int32_t L_11 = ___p;
		NullCheck(L_10);
		uint16_t L_12 = String_get_Chars_m1_341(L_10, L_11, /*hidden argument*/NULL);
		int32_t L_13 = TextEditor_ClassifyChar_m6_1592(__this, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) == ((int32_t)3)))
		{
			goto IL_0085;
		}
	}
	{
		goto IL_005f;
	}

IL_005a:
	{
		int32_t L_15 = ___p;
		___p = ((int32_t)((int32_t)L_15-(int32_t)1));
	}

IL_005f:
	{
		int32_t L_16 = ___p;
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_0085;
		}
	}
	{
		GUIContent_t6_163 * L_17 = (__this->___content_2);
		NullCheck(L_17);
		String_t* L_18 = GUIContent_get_text_m6_1103(L_17, /*hidden argument*/NULL);
		int32_t L_19 = ___p;
		NullCheck(L_18);
		uint16_t L_20 = String_get_Chars_m1_341(L_18, ((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_21 = TextEditor_ClassifyChar_m6_1592(__this, L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) == ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}

IL_0085:
	{
		int32_t L_23 = ___p;
		return L_23;
	}
}
// System.Void UnityEngine.TextEditor::MoveWordLeft()
extern "C" void TextEditor_MoveWordLeft_m6_1595 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	TextEditor_t6_238 * G_B2_0 = {0};
	TextEditor_t6_238 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	TextEditor_t6_238 * G_B3_1 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m6_1540(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_FindPrevSeperator_m6_1586(__this, L_4, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectWordRight()
extern "C" void TextEditor_SelectWordRight_m6_1596 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_238 * G_B3_0 = {0};
	TextEditor_t6_238 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	TextEditor_t6_238 * G_B4_1 = {0};
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_3, /*hidden argument*/NULL);
		TextEditor_MoveWordRight_m6_1587(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			G_B3_0 = __this;
			goto IL_0054;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		goto IL_005a;
	}

IL_0054:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_005a:
	{
		NullCheck(G_B4_1);
		TextEditor_set_cursorIndex_m6_1540(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		return;
	}

IL_0060:
	{
		int32_t L_9 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_9, /*hidden argument*/NULL);
		TextEditor_MoveWordRight_m6_1587(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectWordLeft()
extern "C" void TextEditor_SelectWordLeft_m6_1597 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_238 * G_B3_0 = {0};
	TextEditor_t6_238 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	TextEditor_t6_238 * G_B4_1 = {0};
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_3, /*hidden argument*/NULL);
		TextEditor_MoveWordLeft_m6_1595(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			G_B3_0 = __this;
			goto IL_0054;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		goto IL_005a;
	}

IL_0054:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_005a:
	{
		NullCheck(G_B4_1);
		TextEditor_set_cursorIndex_m6_1540(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		return;
	}

IL_0060:
	{
		int32_t L_9 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_9, /*hidden argument*/NULL);
		TextEditor_MoveWordLeft_m6_1595(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::ExpandSelectGraphicalLineStart()
extern "C" void TextEditor_ExpandSelectGraphicalLineStart_m6_1598 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_GetGraphicalLineStart_m6_1582(__this, L_2, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_3, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_002e:
	{
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_GetGraphicalLineStart_m6_1582(__this, L_5, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_7, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::ExpandSelectGraphicalLineEnd()
extern "C" void TextEditor_ExpandSelectGraphicalLineEnd_m6_1599 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_GetGraphicalLineEnd_m6_1583(__this, L_2, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_3, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_002e:
	{
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_GetGraphicalLineEnd_m6_1583(__this, L_5, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_selectIndex_m6_1542(__this, L_7, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectGraphicalLineStart()
extern "C" void TextEditor_SelectGraphicalLineStart_m6_1600 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_GetGraphicalLineStart_m6_1582(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectGraphicalLineEnd()
extern "C" void TextEditor_SelectGraphicalLineEnd_m6_1601 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_GetGraphicalLineEnd_m6_1583(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectParagraphForward()
extern "C" void TextEditor_SelectParagraphForward_m6_1602 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		V_0 = ((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0);
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_3 = (__this->___content_2);
		NullCheck(L_3);
		String_t* L_4 = GUIContent_get_text_m6_1103(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1_428(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)L_5)))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_6 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_IndexOfEndOfLine_m6_1569(__this, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_7, /*hidden argument*/NULL);
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_9 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_10 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_9) <= ((int32_t)L_10)))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_11 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_11, /*hidden argument*/NULL);
	}

IL_0067:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectParagraphBackward()
extern "C" void TextEditor_SelectParagraphBackward_m6_1603 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		V_0 = ((((int32_t)L_0) > ((int32_t)L_1))? 1 : 0);
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_006b;
		}
	}
	{
		GUIContent_t6_163 * L_3 = (__this->___content_2);
		NullCheck(L_3);
		String_t* L_4 = GUIContent_get_text_m6_1103(L_3, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_6 = String_LastIndexOf_m1_391(L_4, ((int32_t)10), ((int32_t)((int32_t)L_5-(int32_t)2)), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_8 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) >= ((int32_t)L_9)))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_10 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_10, /*hidden argument*/NULL);
	}

IL_0066:
	{
		goto IL_007b;
	}

IL_006b:
	{
		V_1 = 0;
		int32_t L_11 = V_1;
		TextEditor_set_cursorIndex_m6_1540(__this, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		TextEditor_set_selectIndex_m6_1542(__this, L_12, /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectCurrentWord()
extern "C" void TextEditor_SelectCurrentWord_m6_1604 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1103(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_428(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if (L_4)
		{
			goto IL_002a;
		}
	}
	{
		return;
	}

IL_002a:
	{
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_7 = V_0;
		TextEditor_set_cursorIndex_m6_1540(__this, ((int32_t)((int32_t)L_7-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_003f:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_10 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, ((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0059:
	{
		int32_t L_11 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_12 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_13 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_14 = TextEditor_FindEndOfClassification_m6_1605(__this, L_13, (-1), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_14, /*hidden argument*/NULL);
		int32_t L_15 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_16 = TextEditor_FindEndOfClassification_m6_1605(__this, L_15, 1, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_16, /*hidden argument*/NULL);
		goto IL_00bb;
	}

IL_0095:
	{
		int32_t L_17 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_18 = TextEditor_FindEndOfClassification_m6_1605(__this, L_17, 1, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, L_18, /*hidden argument*/NULL);
		int32_t L_19 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_20 = TextEditor_FindEndOfClassification_m6_1605(__this, L_19, (-1), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, L_20, /*hidden argument*/NULL);
	}

IL_00bb:
	{
		__this->___m_bJustSelected_18 = 1;
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::FindEndOfClassification(System.Int32,System.Int32)
extern "C" int32_t TextEditor_FindEndOfClassification_m6_1605 (TextEditor_t6_238 * __this, int32_t ___p, int32_t ___dir, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	{
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1103(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_428(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = ___p;
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_5 = ___p;
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_0021;
		}
	}

IL_001f:
	{
		int32_t L_6 = ___p;
		return L_6;
	}

IL_0021:
	{
		GUIContent_t6_163 * L_7 = (__this->___content_2);
		NullCheck(L_7);
		String_t* L_8 = GUIContent_get_text_m6_1103(L_7, /*hidden argument*/NULL);
		int32_t L_9 = ___p;
		NullCheck(L_8);
		uint16_t L_10 = String_get_Chars_m1_341(L_8, L_9, /*hidden argument*/NULL);
		int32_t L_11 = TextEditor_ClassifyChar_m6_1592(__this, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
	}

IL_0039:
	{
		int32_t L_12 = ___p;
		int32_t L_13 = ___dir;
		___p = ((int32_t)((int32_t)L_12+(int32_t)L_13));
		int32_t L_14 = ___p;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_0047;
		}
	}
	{
		return 0;
	}

IL_0047:
	{
		int32_t L_15 = ___p;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_17 = V_0;
		return L_17;
	}

IL_0050:
	{
		GUIContent_t6_163 * L_18 = (__this->___content_2);
		NullCheck(L_18);
		String_t* L_19 = GUIContent_get_text_m6_1103(L_18, /*hidden argument*/NULL);
		int32_t L_20 = ___p;
		NullCheck(L_19);
		uint16_t L_21 = String_get_Chars_m1_341(L_19, L_20, /*hidden argument*/NULL);
		int32_t L_22 = TextEditor_ClassifyChar_m6_1592(__this, L_21, /*hidden argument*/NULL);
		int32_t L_23 = V_1;
		if ((((int32_t)L_22) == ((int32_t)L_23)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_24 = ___dir;
		if ((!(((uint32_t)L_24) == ((uint32_t)1))))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_25 = ___p;
		return L_25;
	}

IL_0076:
	{
		int32_t L_26 = ___p;
		return ((int32_t)((int32_t)L_26+(int32_t)1));
	}
}
// System.Void UnityEngine.TextEditor::SelectCurrentParagraph()
extern "C" void TextEditor_SelectCurrentParagraph_m6_1606 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m6_1543(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1103(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_428(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_IndexOfEndOfLine_m6_1569(__this, L_5, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1540(__this, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0037:
	{
		int32_t L_7 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0064;
		}
	}
	{
		GUIContent_t6_163 * L_8 = (__this->___content_2);
		NullCheck(L_8);
		String_t* L_9 = GUIContent_get_text_m6_1103(L_8, /*hidden argument*/NULL);
		int32_t L_10 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_11 = String_LastIndexOf_m1_391(L_9, ((int32_t)10), ((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1542(__this, ((int32_t)((int32_t)L_11+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffsetIfNeeded()
extern "C" void TextEditor_UpdateScrollOffsetIfNeeded_m6_1607 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		Event_t6_154 * L_0 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1003(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)7)))
		{
			goto IL_0026;
		}
	}
	{
		Event_t6_154 * L_2 = Event_get_current_m6_989(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1003(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)8)))
		{
			goto IL_0026;
		}
	}
	{
		TextEditor_UpdateScrollOffset_m6_1608(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
extern "C" void TextEditor_UpdateScrollOffset_m6_1608 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Rect_t6_52  V_1 = {0};
	Vector2_t6_48  V_2 = {0};
	Rect_t6_52  V_3 = {0};
	Rect_t6_52  V_4 = {0};
	Vector2_t6_48  V_5 = {0};
	Rect_t6_52  V_6 = {0};
	Rect_t6_52  V_7 = {0};
	Vector2_t6_48 * G_B19_0 = {0};
	Vector2_t6_48 * G_B18_0 = {0};
	float G_B20_0 = 0.0f;
	Vector2_t6_48 * G_B20_1 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		GUIStyle_t6_166 * L_1 = (__this->___style_3);
		Rect_t6_52  L_2 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		V_3 = L_2;
		float L_3 = Rect_get_width_m6_277((&V_3), /*hidden argument*/NULL);
		Rect_t6_52  L_4 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		V_4 = L_4;
		float L_5 = Rect_get_height_m6_279((&V_4), /*hidden argument*/NULL);
		Rect_t6_52  L_6 = {0};
		Rect__ctor_m6_270(&L_6, (0.0f), (0.0f), L_3, L_5, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_7 = (__this->___content_2);
		int32_t L_8 = V_0;
		NullCheck(L_1);
		Vector2_t6_48  L_9 = GUIStyle_GetCursorPixelPosition_m6_1317(L_1, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->___graphicalCursorPos_13 = L_9;
		GUIStyle_t6_166 * L_10 = (__this->___style_3);
		NullCheck(L_10);
		RectOffset_t6_172 * L_11 = GUIStyle_get_padding_m6_1304(L_10, /*hidden argument*/NULL);
		Rect_t6_52  L_12 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Rect_t6_52  L_13 = RectOffset_Remove_m6_1296(L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		GUIStyle_t6_166 * L_14 = (__this->___style_3);
		GUIContent_t6_163 * L_15 = (__this->___content_2);
		NullCheck(L_14);
		Vector2_t6_48  L_16 = GUIStyle_CalcSize_m6_1319(L_14, L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		float L_17 = ((&V_5)->___x_1);
		GUIStyle_t6_166 * L_18 = (__this->___style_3);
		GUIContent_t6_163 * L_19 = (__this->___content_2);
		Rect_t6_52  L_20 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		V_6 = L_20;
		float L_21 = Rect_get_width_m6_277((&V_6), /*hidden argument*/NULL);
		NullCheck(L_18);
		float L_22 = GUIStyle_CalcHeight_m6_1320(L_18, L_19, L_21, /*hidden argument*/NULL);
		Vector2__ctor_m6_176((&V_2), L_17, L_22, /*hidden argument*/NULL);
		float L_23 = ((&V_2)->___x_1);
		Rect_t6_52  L_24 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		V_7 = L_24;
		float L_25 = Rect_get_width_m6_277((&V_7), /*hidden argument*/NULL);
		if ((!(((float)L_23) < ((float)L_25))))
		{
			goto IL_00d3;
		}
	}
	{
		Vector2_t6_48 * L_26 = &(__this->___scrollOffset_8);
		L_26->___x_1 = (0.0f);
		goto IL_017a;
	}

IL_00d3:
	{
		bool L_27 = (__this->___m_RevealCursor_12);
		if (!L_27)
		{
			goto IL_017a;
		}
	}
	{
		Vector2_t6_48 * L_28 = &(__this->___graphicalCursorPos_13);
		float L_29 = (L_28->___x_1);
		Vector2_t6_48 * L_30 = &(__this->___scrollOffset_8);
		float L_31 = (L_30->___x_1);
		float L_32 = Rect_get_width_m6_277((&V_1), /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_29+(float)(1.0f)))) > ((float)((float)((float)L_31+(float)L_32))))))
		{
			goto IL_0125;
		}
	}
	{
		Vector2_t6_48 * L_33 = &(__this->___scrollOffset_8);
		Vector2_t6_48 * L_34 = &(__this->___graphicalCursorPos_13);
		float L_35 = (L_34->___x_1);
		float L_36 = Rect_get_width_m6_277((&V_1), /*hidden argument*/NULL);
		L_33->___x_1 = ((float)((float)L_35-(float)L_36));
	}

IL_0125:
	{
		Vector2_t6_48 * L_37 = &(__this->___graphicalCursorPos_13);
		float L_38 = (L_37->___x_1);
		Vector2_t6_48 * L_39 = &(__this->___scrollOffset_8);
		float L_40 = (L_39->___x_1);
		GUIStyle_t6_166 * L_41 = (__this->___style_3);
		NullCheck(L_41);
		RectOffset_t6_172 * L_42 = GUIStyle_get_padding_m6_1304(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_left_m6_1284(L_42, /*hidden argument*/NULL);
		if ((!(((float)L_38) < ((float)((float)((float)L_40+(float)(((float)((float)L_43)))))))))
		{
			goto IL_017a;
		}
	}
	{
		Vector2_t6_48 * L_44 = &(__this->___scrollOffset_8);
		Vector2_t6_48 * L_45 = &(__this->___graphicalCursorPos_13);
		float L_46 = (L_45->___x_1);
		GUIStyle_t6_166 * L_47 = (__this->___style_3);
		NullCheck(L_47);
		RectOffset_t6_172 * L_48 = GUIStyle_get_padding_m6_1304(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_49 = RectOffset_get_left_m6_1284(L_48, /*hidden argument*/NULL);
		L_44->___x_1 = ((float)((float)L_46-(float)(((float)((float)L_49)))));
	}

IL_017a:
	{
		float L_50 = ((&V_2)->___y_2);
		float L_51 = Rect_get_height_m6_279((&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_50) < ((float)L_51))))
		{
			goto IL_01a2;
		}
	}
	{
		Vector2_t6_48 * L_52 = &(__this->___scrollOffset_8);
		L_52->___y_2 = (0.0f);
		goto IL_027f;
	}

IL_01a2:
	{
		bool L_53 = (__this->___m_RevealCursor_12);
		if (!L_53)
		{
			goto IL_027f;
		}
	}
	{
		Vector2_t6_48 * L_54 = &(__this->___graphicalCursorPos_13);
		float L_55 = (L_54->___y_2);
		GUIStyle_t6_166 * L_56 = (__this->___style_3);
		NullCheck(L_56);
		float L_57 = GUIStyle_get_lineHeight_m6_1307(L_56, /*hidden argument*/NULL);
		Vector2_t6_48 * L_58 = &(__this->___scrollOffset_8);
		float L_59 = (L_58->___y_2);
		float L_60 = Rect_get_height_m6_279((&V_1), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_61 = (__this->___style_3);
		NullCheck(L_61);
		RectOffset_t6_172 * L_62 = GUIStyle_get_padding_m6_1304(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = RectOffset_get_top_m6_1288(L_62, /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_55+(float)L_57))) > ((float)((float)((float)((float)((float)L_59+(float)L_60))+(float)(((float)((float)L_63)))))))))
		{
			goto IL_022a;
		}
	}
	{
		Vector2_t6_48 * L_64 = &(__this->___scrollOffset_8);
		Vector2_t6_48 * L_65 = &(__this->___graphicalCursorPos_13);
		float L_66 = (L_65->___y_2);
		float L_67 = Rect_get_height_m6_279((&V_1), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_68 = (__this->___style_3);
		NullCheck(L_68);
		RectOffset_t6_172 * L_69 = GUIStyle_get_padding_m6_1304(L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		int32_t L_70 = RectOffset_get_top_m6_1288(L_69, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_71 = (__this->___style_3);
		NullCheck(L_71);
		float L_72 = GUIStyle_get_lineHeight_m6_1307(L_71, /*hidden argument*/NULL);
		L_64->___y_2 = ((float)((float)((float)((float)((float)((float)L_66-(float)L_67))-(float)(((float)((float)L_70)))))+(float)L_72));
	}

IL_022a:
	{
		Vector2_t6_48 * L_73 = &(__this->___graphicalCursorPos_13);
		float L_74 = (L_73->___y_2);
		Vector2_t6_48 * L_75 = &(__this->___scrollOffset_8);
		float L_76 = (L_75->___y_2);
		GUIStyle_t6_166 * L_77 = (__this->___style_3);
		NullCheck(L_77);
		RectOffset_t6_172 * L_78 = GUIStyle_get_padding_m6_1304(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		int32_t L_79 = RectOffset_get_top_m6_1288(L_78, /*hidden argument*/NULL);
		if ((!(((float)L_74) < ((float)((float)((float)L_76+(float)(((float)((float)L_79)))))))))
		{
			goto IL_027f;
		}
	}
	{
		Vector2_t6_48 * L_80 = &(__this->___scrollOffset_8);
		Vector2_t6_48 * L_81 = &(__this->___graphicalCursorPos_13);
		float L_82 = (L_81->___y_2);
		GUIStyle_t6_166 * L_83 = (__this->___style_3);
		NullCheck(L_83);
		RectOffset_t6_172 * L_84 = GUIStyle_get_padding_m6_1304(L_83, /*hidden argument*/NULL);
		NullCheck(L_84);
		int32_t L_85 = RectOffset_get_top_m6_1288(L_84, /*hidden argument*/NULL);
		L_80->___y_2 = ((float)((float)L_82-(float)(((float)((float)L_85)))));
	}

IL_027f:
	{
		Vector2_t6_48 * L_86 = &(__this->___scrollOffset_8);
		float L_87 = (L_86->___y_2);
		if ((!(((float)L_87) > ((float)(0.0f)))))
		{
			goto IL_02f1;
		}
	}
	{
		float L_88 = ((&V_2)->___y_2);
		Vector2_t6_48 * L_89 = &(__this->___scrollOffset_8);
		float L_90 = (L_89->___y_2);
		float L_91 = Rect_get_height_m6_279((&V_1), /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_88-(float)L_90))) < ((float)L_91))))
		{
			goto IL_02f1;
		}
	}
	{
		Vector2_t6_48 * L_92 = &(__this->___scrollOffset_8);
		float L_93 = ((&V_2)->___y_2);
		float L_94 = Rect_get_height_m6_279((&V_1), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_95 = (__this->___style_3);
		NullCheck(L_95);
		RectOffset_t6_172 * L_96 = GUIStyle_get_padding_m6_1304(L_95, /*hidden argument*/NULL);
		NullCheck(L_96);
		int32_t L_97 = RectOffset_get_top_m6_1288(L_96, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_98 = (__this->___style_3);
		NullCheck(L_98);
		RectOffset_t6_172 * L_99 = GUIStyle_get_padding_m6_1304(L_98, /*hidden argument*/NULL);
		NullCheck(L_99);
		int32_t L_100 = RectOffset_get_bottom_m6_1290(L_99, /*hidden argument*/NULL);
		L_92->___y_2 = ((float)((float)((float)((float)((float)((float)L_93-(float)L_94))-(float)(((float)((float)L_97)))))-(float)(((float)((float)L_100)))));
	}

IL_02f1:
	{
		Vector2_t6_48 * L_101 = &(__this->___scrollOffset_8);
		Vector2_t6_48 * L_102 = &(__this->___scrollOffset_8);
		float L_103 = (L_102->___y_2);
		G_B18_0 = L_101;
		if ((!(((float)L_103) < ((float)(0.0f)))))
		{
			G_B19_0 = L_101;
			goto IL_0316;
		}
	}
	{
		G_B20_0 = (0.0f);
		G_B20_1 = G_B18_0;
		goto IL_0321;
	}

IL_0316:
	{
		Vector2_t6_48 * L_104 = &(__this->___scrollOffset_8);
		float L_105 = (L_104->___y_2);
		G_B20_0 = L_105;
		G_B20_1 = G_B19_0;
	}

IL_0321:
	{
		G_B20_1->___y_2 = G_B20_0;
		__this->___m_RevealCursor_12 = 0;
		return;
	}
}
// System.Void UnityEngine.TextEditor::DrawCursor(System.String)
extern TypeInfo* Input_t6_83_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void TextEditor_DrawCursor_m6_1609 (TextEditor_t6_238 * __this, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(956);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	Vector2_t6_48  V_2 = {0};
	Rect_t6_52  V_3 = {0};
	Rect_t6_52  V_4 = {0};
	Rect_t6_52  V_5 = {0};
	Rect_t6_52  V_6 = {0};
	{
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1103(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		String_t* L_3 = Input_get_compositionString_m6_550(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1_428(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0063;
		}
	}
	{
		GUIContent_t6_163 * L_5 = (__this->___content_2);
		String_t* L_6 = ___text;
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_8 = String_Substring_m1_352(L_6, 0, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		String_t* L_9 = Input_get_compositionString_m6_550(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = ___text;
		int32_t L_11 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_12 = String_Substring_m1_351(L_10, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1_419(NULL /*static, unused*/, L_8, L_9, L_12, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIContent_set_text_m6_1104(L_5, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		String_t* L_15 = Input_get_compositionString_m6_550(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = String_get_Length_m1_428(L_15, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)L_16));
		goto IL_006f;
	}

IL_0063:
	{
		GUIContent_t6_163 * L_17 = (__this->___content_2);
		String_t* L_18 = ___text;
		NullCheck(L_17);
		GUIContent_set_text_m6_1104(L_17, L_18, /*hidden argument*/NULL);
	}

IL_006f:
	{
		GUIStyle_t6_166 * L_19 = (__this->___style_3);
		Rect_t6_52  L_20 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		V_3 = L_20;
		float L_21 = Rect_get_width_m6_277((&V_3), /*hidden argument*/NULL);
		Rect_t6_52  L_22 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		V_4 = L_22;
		float L_23 = Rect_get_height_m6_279((&V_4), /*hidden argument*/NULL);
		Rect_t6_52  L_24 = {0};
		Rect__ctor_m6_270(&L_24, (0.0f), (0.0f), L_21, L_23, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_25 = (__this->___content_2);
		int32_t L_26 = V_1;
		NullCheck(L_19);
		Vector2_t6_48  L_27 = GUIStyle_GetCursorPixelPosition_m6_1317(L_19, L_24, L_25, L_26, /*hidden argument*/NULL);
		__this->___graphicalCursorPos_13 = L_27;
		GUIStyle_t6_166 * L_28 = (__this->___style_3);
		NullCheck(L_28);
		Vector2_t6_48  L_29 = GUIStyle_get_contentOffset_m6_1336(L_28, /*hidden argument*/NULL);
		V_2 = L_29;
		GUIStyle_t6_166 * L_30 = (__this->___style_3);
		GUIStyle_t6_166 * L_31 = L_30;
		NullCheck(L_31);
		Vector2_t6_48  L_32 = GUIStyle_get_contentOffset_m6_1336(L_31, /*hidden argument*/NULL);
		Vector2_t6_48  L_33 = (__this->___scrollOffset_8);
		Vector2_t6_48  L_34 = Vector2_op_Subtraction_m6_185(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		GUIStyle_set_contentOffset_m6_1337(L_31, L_34, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_35 = (__this->___style_3);
		Vector2_t6_48  L_36 = (__this->___scrollOffset_8);
		NullCheck(L_35);
		GUIStyle_set_Internal_clipOffset_m6_1340(L_35, L_36, /*hidden argument*/NULL);
		Vector2_t6_48  L_37 = (__this->___graphicalCursorPos_13);
		Rect_t6_52  L_38 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		V_5 = L_38;
		float L_39 = Rect_get_x_m6_273((&V_5), /*hidden argument*/NULL);
		Rect_t6_52  L_40 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		V_6 = L_40;
		float L_41 = Rect_get_y_m6_275((&V_6), /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_42 = (__this->___style_3);
		NullCheck(L_42);
		float L_43 = GUIStyle_get_lineHeight_m6_1307(L_42, /*hidden argument*/NULL);
		Vector2_t6_48  L_44 = {0};
		Vector2__ctor_m6_176(&L_44, L_39, ((float)((float)L_41+(float)L_43)), /*hidden argument*/NULL);
		Vector2_t6_48  L_45 = Vector2_op_Addition_m6_184(NULL /*static, unused*/, L_37, L_44, /*hidden argument*/NULL);
		Vector2_t6_48  L_46 = (__this->___scrollOffset_8);
		Vector2_t6_48  L_47 = Vector2_op_Subtraction_m6_185(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		Input_set_compositionCursorPos_m6_551(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		String_t* L_48 = Input_get_compositionString_m6_550(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_49 = String_get_Length_m1_428(L_48, /*hidden argument*/NULL);
		if ((((int32_t)L_49) <= ((int32_t)0)))
		{
			goto IL_0180;
		}
	}
	{
		GUIStyle_t6_166 * L_50 = (__this->___style_3);
		Rect_t6_52  L_51 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_52 = (__this->___content_2);
		int32_t L_53 = (__this->___controlID_1);
		int32_t L_54 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_55 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_83_il2cpp_TypeInfo_var);
		String_t* L_56 = Input_get_compositionString_m6_550(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_56);
		int32_t L_57 = String_get_Length_m1_428(L_56, /*hidden argument*/NULL);
		NullCheck(L_50);
		GUIStyle_DrawWithTextSelection_m6_1314(L_50, L_51, L_52, L_53, L_54, ((int32_t)((int32_t)L_55+(int32_t)L_57)), 1, /*hidden argument*/NULL);
		goto IL_01a9;
	}

IL_0180:
	{
		GUIStyle_t6_166 * L_58 = (__this->___style_3);
		Rect_t6_52  L_59 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_60 = (__this->___content_2);
		int32_t L_61 = (__this->___controlID_1);
		int32_t L_62 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_63 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		NullCheck(L_58);
		GUIStyle_DrawWithTextSelection_m6_1315(L_58, L_59, L_60, L_61, L_62, L_63, /*hidden argument*/NULL);
	}

IL_01a9:
	{
		int32_t L_64 = (__this->___m_iAltCursorPos_19);
		if ((((int32_t)L_64) == ((int32_t)(-1))))
		{
			goto IL_01d8;
		}
	}
	{
		GUIStyle_t6_166 * L_65 = (__this->___style_3);
		Rect_t6_52  L_66 = TextEditor_get_position_m6_1537(__this, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_67 = (__this->___content_2);
		int32_t L_68 = (__this->___controlID_1);
		int32_t L_69 = (__this->___m_iAltCursorPos_19);
		NullCheck(L_65);
		GUIStyle_DrawCursor_m6_1313(L_65, L_66, L_67, L_68, L_69, /*hidden argument*/NULL);
	}

IL_01d8:
	{
		GUIStyle_t6_166 * L_70 = (__this->___style_3);
		Vector2_t6_48  L_71 = V_2;
		NullCheck(L_70);
		GUIStyle_set_contentOffset_m6_1337(L_70, L_71, /*hidden argument*/NULL);
		GUIStyle_t6_166 * L_72 = (__this->___style_3);
		Vector2_t6_48  L_73 = Vector2_get_zero_m6_182(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_72);
		GUIStyle_set_Internal_clipOffset_m6_1340(L_72, L_73, /*hidden argument*/NULL);
		GUIContent_t6_163 * L_74 = (__this->___content_2);
		String_t* L_75 = V_0;
		NullCheck(L_74);
		GUIContent_set_text_m6_1104(L_74, L_75, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::PerformOperation(UnityEngine.TextEditor/TextEditOp)
extern TypeInfo* TextEditOp_t6_237_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2880;
extern "C" bool TextEditor_PerformOperation_m6_1610 (TextEditor_t6_238 * __this, int32_t ___operation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditOp_t6_237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1037);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2880 = il2cpp_codegen_string_literal_from_index(2880);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		__this->___m_RevealCursor_12 = 1;
		int32_t L_0 = ___operation;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_00cc;
		}
		if (L_1 == 1)
		{
			goto IL_00d7;
		}
		if (L_1 == 2)
		{
			goto IL_00e2;
		}
		if (L_1 == 3)
		{
			goto IL_00ed;
		}
		if (L_1 == 4)
		{
			goto IL_00f8;
		}
		if (L_1 == 5)
		{
			goto IL_0103;
		}
		if (L_1 == 6)
		{
			goto IL_013a;
		}
		if (L_1 == 7)
		{
			goto IL_0145;
		}
		if (L_1 == 8)
		{
			goto IL_027e;
		}
		if (L_1 == 9)
		{
			goto IL_027e;
		}
		if (L_1 == 10)
		{
			goto IL_0166;
		}
		if (L_1 == 11)
		{
			goto IL_0171;
		}
		if (L_1 == 12)
		{
			goto IL_012f;
		}
		if (L_1 == 13)
		{
			goto IL_010e;
		}
		if (L_1 == 14)
		{
			goto IL_0150;
		}
		if (L_1 == 15)
		{
			goto IL_015b;
		}
		if (L_1 == 16)
		{
			goto IL_0119;
		}
		if (L_1 == 17)
		{
			goto IL_0124;
		}
		if (L_1 == 18)
		{
			goto IL_017c;
		}
		if (L_1 == 19)
		{
			goto IL_0187;
		}
		if (L_1 == 20)
		{
			goto IL_0192;
		}
		if (L_1 == 21)
		{
			goto IL_019d;
		}
		if (L_1 == 22)
		{
			goto IL_01d4;
		}
		if (L_1 == 23)
		{
			goto IL_01df;
		}
		if (L_1 == 24)
		{
			goto IL_027e;
		}
		if (L_1 == 25)
		{
			goto IL_027e;
		}
		if (L_1 == 26)
		{
			goto IL_01ea;
		}
		if (L_1 == 27)
		{
			goto IL_01f5;
		}
		if (L_1 == 28)
		{
			goto IL_0216;
		}
		if (L_1 == 29)
		{
			goto IL_0221;
		}
		if (L_1 == 30)
		{
			goto IL_01b3;
		}
		if (L_1 == 31)
		{
			goto IL_01a8;
		}
		if (L_1 == 32)
		{
			goto IL_01be;
		}
		if (L_1 == 33)
		{
			goto IL_01c9;
		}
		if (L_1 == 34)
		{
			goto IL_020b;
		}
		if (L_1 == 35)
		{
			goto IL_0200;
		}
		if (L_1 == 36)
		{
			goto IL_022c;
		}
		if (L_1 == 37)
		{
			goto IL_0233;
		}
		if (L_1 == 38)
		{
			goto IL_0269;
		}
		if (L_1 == 39)
		{
			goto IL_0277;
		}
		if (L_1 == 40)
		{
			goto IL_0270;
		}
		if (L_1 == 41)
		{
			goto IL_023a;
		}
		if (L_1 == 42)
		{
			goto IL_0241;
		}
		if (L_1 == 43)
		{
			goto IL_024c;
		}
		if (L_1 == 44)
		{
			goto IL_0253;
		}
		if (L_1 == 45)
		{
			goto IL_025e;
		}
	}
	{
		goto IL_027e;
	}

IL_00cc:
	{
		TextEditor_MoveLeft_m6_1560(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00d7:
	{
		TextEditor_MoveRight_m6_1559(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00e2:
	{
		TextEditor_MoveUp_m6_1561(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00ed:
	{
		TextEditor_MoveDown_m6_1562(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00f8:
	{
		TextEditor_MoveLineStart_m6_1563(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0103:
	{
		TextEditor_MoveLineEnd_m6_1564(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_010e:
	{
		TextEditor_MoveWordRight_m6_1587(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0119:
	{
		TextEditor_MoveToStartOfNextWord_m6_1588(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0124:
	{
		TextEditor_MoveToEndOfPreviousWord_m6_1589(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_012f:
	{
		TextEditor_MoveWordLeft_m6_1595(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_013a:
	{
		TextEditor_MoveTextStart_m6_1567(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0145:
	{
		TextEditor_MoveTextEnd_m6_1568(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0150:
	{
		TextEditor_MoveParagraphForward_m6_1570(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_015b:
	{
		TextEditor_MoveParagraphBackward_m6_1571(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0166:
	{
		TextEditor_MoveGraphicalLineStart_m6_1565(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0171:
	{
		TextEditor_MoveGraphicalLineEnd_m6_1566(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_017c:
	{
		TextEditor_SelectLeft_m6_1574(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0187:
	{
		TextEditor_SelectRight_m6_1575(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0192:
	{
		TextEditor_SelectUp_m6_1576(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_019d:
	{
		TextEditor_SelectDown_m6_1577(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01a8:
	{
		TextEditor_SelectWordRight_m6_1596(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01b3:
	{
		TextEditor_SelectWordLeft_m6_1597(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01be:
	{
		TextEditor_SelectToEndOfPreviousWord_m6_1591(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01c9:
	{
		TextEditor_SelectToStartOfNextWord_m6_1590(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01d4:
	{
		TextEditor_SelectTextStart_m6_1579(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01df:
	{
		TextEditor_SelectTextEnd_m6_1578(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01ea:
	{
		TextEditor_ExpandSelectGraphicalLineStart_m6_1598(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01f5:
	{
		TextEditor_ExpandSelectGraphicalLineEnd_m6_1599(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0200:
	{
		TextEditor_SelectParagraphForward_m6_1602(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_020b:
	{
		TextEditor_SelectParagraphBackward_m6_1603(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0216:
	{
		TextEditor_SelectGraphicalLineStart_m6_1600(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0221:
	{
		TextEditor_SelectGraphicalLineEnd_m6_1601(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_022c:
	{
		bool L_2 = TextEditor_Delete_m6_1551(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_0233:
	{
		bool L_3 = TextEditor_Backspace_m6_1552(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_023a:
	{
		bool L_4 = TextEditor_Cut_m6_1612(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_0241:
	{
		TextEditor_Copy_m6_1613(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_024c:
	{
		bool L_5 = TextEditor_Paste_m6_1615(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0253:
	{
		TextEditor_SelectAll_m6_1553(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_025e:
	{
		TextEditor_SelectNone_m6_1554(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0269:
	{
		bool L_6 = TextEditor_DeleteWordBack_m6_1549(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_0270:
	{
		bool L_7 = TextEditor_DeleteLineBack_m6_1548(__this, /*hidden argument*/NULL);
		return L_7;
	}

IL_0277:
	{
		bool L_8 = TextEditor_DeleteWordForward_m6_1550(__this, /*hidden argument*/NULL);
		return L_8;
	}

IL_027e:
	{
		int32_t L_9 = ___operation;
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(TextEditOp_t6_237_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m1_416(NULL /*static, unused*/, _stringLiteral2880, L_11, /*hidden argument*/NULL);
		Debug_Log_m6_489(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0298:
	{
		return 0;
	}
}
// System.Void UnityEngine.TextEditor::SaveBackup()
extern "C" void TextEditor_SaveBackup_m6_1611 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		GUIContent_t6_163 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1103(L_0, /*hidden argument*/NULL);
		__this->___oldText_20 = L_1;
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		__this->___oldPos_21 = L_2;
		int32_t L_3 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		__this->___oldSelectPos_22 = L_3;
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::Cut()
extern "C" bool TextEditor_Cut_m6_1612 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isPasswordField_6);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		TextEditor_Copy_m6_1613(__this, /*hidden argument*/NULL);
		bool L_1 = TextEditor_DeleteSelection_m6_1556(__this, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.TextEditor::Copy()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" void TextEditor_Copy_m6_1613 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		int32_t L_0 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = (__this->___isPasswordField_6);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0058;
		}
	}
	{
		GUIContent_t6_163 * L_5 = (__this->___content_2);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m6_1103(L_5, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_10 = String_Substring_m1_352(L_6, L_7, ((int32_t)((int32_t)L_8-(int32_t)L_9)), /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_007c;
	}

IL_0058:
	{
		GUIContent_t6_163 * L_11 = (__this->___content_2);
		NullCheck(L_11);
		String_t* L_12 = GUIContent_get_text_m6_1103(L_11, /*hidden argument*/NULL);
		int32_t L_13 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		int32_t L_14 = TextEditor_get_cursorIndex_m6_1539(__this, /*hidden argument*/NULL);
		int32_t L_15 = TextEditor_get_selectIndex_m6_1541(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_16 = String_Substring_m1_352(L_12, L_13, ((int32_t)((int32_t)L_14-(int32_t)L_15)), /*hidden argument*/NULL);
		V_0 = L_16;
	}

IL_007c:
	{
		String_t* L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		GUIUtility_set_systemCopyBuffer_m6_1386(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
extern Il2CppCodeGenString* _stringLiteral2218;
extern Il2CppCodeGenString* _stringLiteral232;
extern "C" String_t* TextEditor_ReplaceNewlinesWithSpaces_m6_1614 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2218 = il2cpp_codegen_string_literal_from_index(2218);
		_stringLiteral232 = il2cpp_codegen_string_literal_from_index(232);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value;
		NullCheck(L_0);
		String_t* L_1 = String_Replace_m1_401(L_0, _stringLiteral2218, _stringLiteral232, /*hidden argument*/NULL);
		___value = L_1;
		String_t* L_2 = ___value;
		NullCheck(L_2);
		String_t* L_3 = String_Replace_m1_400(L_2, ((int32_t)10), ((int32_t)32), /*hidden argument*/NULL);
		___value = L_3;
		String_t* L_4 = ___value;
		NullCheck(L_4);
		String_t* L_5 = String_Replace_m1_400(L_4, ((int32_t)13), ((int32_t)32), /*hidden argument*/NULL);
		___value = L_5;
		String_t* L_6 = ___value;
		return L_6;
	}
}
// System.Boolean UnityEngine.TextEditor::Paste()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextEditor_Paste_m6_1615 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		String_t* L_0 = GUIUtility_get_systemCopyBuffer_m6_1385(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_3 = String_op_Inequality_m1_455(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		bool L_4 = (__this->___multiline_4);
		if (L_4)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_5 = V_0;
		String_t* L_6 = TextEditor_ReplaceNewlinesWithSpaces_m6_1614(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0028:
	{
		String_t* L_7 = V_0;
		TextEditor_ReplaceSelection_m6_1557(__this, L_7, /*hidden argument*/NULL);
		return 1;
	}

IL_0031:
	{
		return 0;
	}
}
// System.Void UnityEngine.TextEditor::MapKey(System.String,UnityEngine.TextEditor/TextEditOp)
extern TypeInfo* TextEditor_t6_238_il2cpp_TypeInfo_var;
extern "C" void TextEditor_MapKey_m6_1616 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditor_t6_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(986);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_920 * L_0 = ((TextEditor_t6_238_StaticFields*)TextEditor_t6_238_il2cpp_TypeInfo_var->static_fields)->___s_Keyactions_23;
		String_t* L_1 = ___key;
		Event_t6_154 * L_2 = Event_KeyboardEvent_m6_994(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___action;
		NullCheck(L_0);
		VirtActionInvoker2< Event_t6_154 *, int32_t >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Item(!0,!1) */, L_0, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.TextEditor::InitKeyActions()
extern TypeInfo* TextEditor_t6_238_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_920_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_5614_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2735;
extern Il2CppCodeGenString* _stringLiteral2736;
extern Il2CppCodeGenString* _stringLiteral2733;
extern Il2CppCodeGenString* _stringLiteral2734;
extern Il2CppCodeGenString* _stringLiteral2881;
extern Il2CppCodeGenString* _stringLiteral2882;
extern Il2CppCodeGenString* _stringLiteral2883;
extern Il2CppCodeGenString* _stringLiteral2884;
extern Il2CppCodeGenString* _stringLiteral2745;
extern Il2CppCodeGenString* _stringLiteral2744;
extern Il2CppCodeGenString* _stringLiteral2885;
extern Il2CppCodeGenString* _stringLiteral2886;
extern Il2CppCodeGenString* _stringLiteral2887;
extern Il2CppCodeGenString* _stringLiteral2888;
extern Il2CppCodeGenString* _stringLiteral2889;
extern Il2CppCodeGenString* _stringLiteral2890;
extern Il2CppCodeGenString* _stringLiteral2891;
extern Il2CppCodeGenString* _stringLiteral2892;
extern Il2CppCodeGenString* _stringLiteral2893;
extern Il2CppCodeGenString* _stringLiteral2894;
extern Il2CppCodeGenString* _stringLiteral2895;
extern Il2CppCodeGenString* _stringLiteral2896;
extern Il2CppCodeGenString* _stringLiteral2897;
extern Il2CppCodeGenString* _stringLiteral2898;
extern Il2CppCodeGenString* _stringLiteral2899;
extern Il2CppCodeGenString* _stringLiteral2900;
extern Il2CppCodeGenString* _stringLiteral2901;
extern Il2CppCodeGenString* _stringLiteral2902;
extern Il2CppCodeGenString* _stringLiteral2903;
extern Il2CppCodeGenString* _stringLiteral2904;
extern Il2CppCodeGenString* _stringLiteral2905;
extern Il2CppCodeGenString* _stringLiteral2906;
extern Il2CppCodeGenString* _stringLiteral2907;
extern Il2CppCodeGenString* _stringLiteral2908;
extern Il2CppCodeGenString* _stringLiteral2909;
extern Il2CppCodeGenString* _stringLiteral2910;
extern Il2CppCodeGenString* _stringLiteral2911;
extern Il2CppCodeGenString* _stringLiteral2912;
extern Il2CppCodeGenString* _stringLiteral2913;
extern Il2CppCodeGenString* _stringLiteral2914;
extern Il2CppCodeGenString* _stringLiteral2915;
extern Il2CppCodeGenString* _stringLiteral2916;
extern Il2CppCodeGenString* _stringLiteral2917;
extern Il2CppCodeGenString* _stringLiteral2918;
extern Il2CppCodeGenString* _stringLiteral2919;
extern Il2CppCodeGenString* _stringLiteral2920;
extern Il2CppCodeGenString* _stringLiteral2921;
extern Il2CppCodeGenString* _stringLiteral2922;
extern Il2CppCodeGenString* _stringLiteral2923;
extern Il2CppCodeGenString* _stringLiteral2738;
extern Il2CppCodeGenString* _stringLiteral2739;
extern Il2CppCodeGenString* _stringLiteral2924;
extern Il2CppCodeGenString* _stringLiteral2925;
extern Il2CppCodeGenString* _stringLiteral2926;
extern Il2CppCodeGenString* _stringLiteral2927;
extern Il2CppCodeGenString* _stringLiteral2928;
extern Il2CppCodeGenString* _stringLiteral2929;
extern Il2CppCodeGenString* _stringLiteral2930;
extern Il2CppCodeGenString* _stringLiteral2931;
extern Il2CppCodeGenString* _stringLiteral2932;
extern Il2CppCodeGenString* _stringLiteral2933;
extern "C" void TextEditor_InitKeyActions_m6_1617 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditor_t6_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(986);
		Dictionary_2_t1_920_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		Dictionary_2__ctor_m1_5614_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483826);
		_stringLiteral2735 = il2cpp_codegen_string_literal_from_index(2735);
		_stringLiteral2736 = il2cpp_codegen_string_literal_from_index(2736);
		_stringLiteral2733 = il2cpp_codegen_string_literal_from_index(2733);
		_stringLiteral2734 = il2cpp_codegen_string_literal_from_index(2734);
		_stringLiteral2881 = il2cpp_codegen_string_literal_from_index(2881);
		_stringLiteral2882 = il2cpp_codegen_string_literal_from_index(2882);
		_stringLiteral2883 = il2cpp_codegen_string_literal_from_index(2883);
		_stringLiteral2884 = il2cpp_codegen_string_literal_from_index(2884);
		_stringLiteral2745 = il2cpp_codegen_string_literal_from_index(2745);
		_stringLiteral2744 = il2cpp_codegen_string_literal_from_index(2744);
		_stringLiteral2885 = il2cpp_codegen_string_literal_from_index(2885);
		_stringLiteral2886 = il2cpp_codegen_string_literal_from_index(2886);
		_stringLiteral2887 = il2cpp_codegen_string_literal_from_index(2887);
		_stringLiteral2888 = il2cpp_codegen_string_literal_from_index(2888);
		_stringLiteral2889 = il2cpp_codegen_string_literal_from_index(2889);
		_stringLiteral2890 = il2cpp_codegen_string_literal_from_index(2890);
		_stringLiteral2891 = il2cpp_codegen_string_literal_from_index(2891);
		_stringLiteral2892 = il2cpp_codegen_string_literal_from_index(2892);
		_stringLiteral2893 = il2cpp_codegen_string_literal_from_index(2893);
		_stringLiteral2894 = il2cpp_codegen_string_literal_from_index(2894);
		_stringLiteral2895 = il2cpp_codegen_string_literal_from_index(2895);
		_stringLiteral2896 = il2cpp_codegen_string_literal_from_index(2896);
		_stringLiteral2897 = il2cpp_codegen_string_literal_from_index(2897);
		_stringLiteral2898 = il2cpp_codegen_string_literal_from_index(2898);
		_stringLiteral2899 = il2cpp_codegen_string_literal_from_index(2899);
		_stringLiteral2900 = il2cpp_codegen_string_literal_from_index(2900);
		_stringLiteral2901 = il2cpp_codegen_string_literal_from_index(2901);
		_stringLiteral2902 = il2cpp_codegen_string_literal_from_index(2902);
		_stringLiteral2903 = il2cpp_codegen_string_literal_from_index(2903);
		_stringLiteral2904 = il2cpp_codegen_string_literal_from_index(2904);
		_stringLiteral2905 = il2cpp_codegen_string_literal_from_index(2905);
		_stringLiteral2906 = il2cpp_codegen_string_literal_from_index(2906);
		_stringLiteral2907 = il2cpp_codegen_string_literal_from_index(2907);
		_stringLiteral2908 = il2cpp_codegen_string_literal_from_index(2908);
		_stringLiteral2909 = il2cpp_codegen_string_literal_from_index(2909);
		_stringLiteral2910 = il2cpp_codegen_string_literal_from_index(2910);
		_stringLiteral2911 = il2cpp_codegen_string_literal_from_index(2911);
		_stringLiteral2912 = il2cpp_codegen_string_literal_from_index(2912);
		_stringLiteral2913 = il2cpp_codegen_string_literal_from_index(2913);
		_stringLiteral2914 = il2cpp_codegen_string_literal_from_index(2914);
		_stringLiteral2915 = il2cpp_codegen_string_literal_from_index(2915);
		_stringLiteral2916 = il2cpp_codegen_string_literal_from_index(2916);
		_stringLiteral2917 = il2cpp_codegen_string_literal_from_index(2917);
		_stringLiteral2918 = il2cpp_codegen_string_literal_from_index(2918);
		_stringLiteral2919 = il2cpp_codegen_string_literal_from_index(2919);
		_stringLiteral2920 = il2cpp_codegen_string_literal_from_index(2920);
		_stringLiteral2921 = il2cpp_codegen_string_literal_from_index(2921);
		_stringLiteral2922 = il2cpp_codegen_string_literal_from_index(2922);
		_stringLiteral2923 = il2cpp_codegen_string_literal_from_index(2923);
		_stringLiteral2738 = il2cpp_codegen_string_literal_from_index(2738);
		_stringLiteral2739 = il2cpp_codegen_string_literal_from_index(2739);
		_stringLiteral2924 = il2cpp_codegen_string_literal_from_index(2924);
		_stringLiteral2925 = il2cpp_codegen_string_literal_from_index(2925);
		_stringLiteral2926 = il2cpp_codegen_string_literal_from_index(2926);
		_stringLiteral2927 = il2cpp_codegen_string_literal_from_index(2927);
		_stringLiteral2928 = il2cpp_codegen_string_literal_from_index(2928);
		_stringLiteral2929 = il2cpp_codegen_string_literal_from_index(2929);
		_stringLiteral2930 = il2cpp_codegen_string_literal_from_index(2930);
		_stringLiteral2931 = il2cpp_codegen_string_literal_from_index(2931);
		_stringLiteral2932 = il2cpp_codegen_string_literal_from_index(2932);
		_stringLiteral2933 = il2cpp_codegen_string_literal_from_index(2933);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_920 * L_0 = ((TextEditor_t6_238_StaticFields*)TextEditor_t6_238_il2cpp_TypeInfo_var->static_fields)->___s_Keyactions_23;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		Dictionary_2_t1_920 * L_1 = (Dictionary_2_t1_920 *)il2cpp_codegen_object_new (Dictionary_2_t1_920_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_5614(L_1, /*hidden argument*/Dictionary_2__ctor_m1_5614_MethodInfo_var);
		((TextEditor_t6_238_StaticFields*)TextEditor_t6_238_il2cpp_TypeInfo_var->static_fields)->___s_Keyactions_23 = L_1;
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2735, 0, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2736, 1, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2733, 2, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2734, 3, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2881, ((int32_t)18), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2882, ((int32_t)19), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2883, ((int32_t)20), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2884, ((int32_t)21), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2745, ((int32_t)36), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2744, ((int32_t)37), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2885, ((int32_t)37), /*hidden argument*/NULL);
		int32_t L_2 = Application_get_platform_m6_452(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_3 = Application_get_platform_m6_452(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)3)))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_4 = Application_get_platform_m6_452(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)4)))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_5 = Application_get_platform_m6_452(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_6 = Application_get_platform_m6_452(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)17)))))
		{
			goto IL_029b;
		}
	}
	{
		String_t* L_7 = SystemInfo_get_operatingSystem_m6_9(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = String_StartsWith_m1_399(L_7, _stringLiteral2886, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_029b;
		}
	}

IL_00e0:
	{
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2887, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2888, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2889, ((int32_t)12), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2890, ((int32_t)13), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2891, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2892, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2893, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2894, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2895, 6, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2896, 7, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2897, ((int32_t)22), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2898, ((int32_t)23), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2899, ((int32_t)26), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2900, ((int32_t)27), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2901, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2902, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2903, ((int32_t)30), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2904, ((int32_t)31), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2905, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2906, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2907, ((int32_t)26), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2908, ((int32_t)27), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2909, ((int32_t)22), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2910, ((int32_t)23), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2911, ((int32_t)44), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2912, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2913, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2914, ((int32_t)43), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2915, ((int32_t)36), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2916, ((int32_t)37), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2917, 0, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2918, 1, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2919, 4, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2920, 5, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2921, ((int32_t)39), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2922, ((int32_t)38), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2923, ((int32_t)40), /*hidden argument*/NULL);
		goto IL_03d3;
	}

IL_029b:
	{
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2738, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2739, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2893, ((int32_t)12), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2894, ((int32_t)13), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2895, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2896, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2887, ((int32_t)17), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2888, ((int32_t)16), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2924, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2925, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2899, ((int32_t)32), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2900, ((int32_t)33), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2901, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2902, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2897, ((int32_t)28), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2898, ((int32_t)29), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2926, ((int32_t)39), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2927, ((int32_t)38), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2923, ((int32_t)40), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2919, ((int32_t)44), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2928, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2929, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2930, ((int32_t)43), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2931, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2932, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1616(NULL /*static, unused*/, _stringLiteral2933, ((int32_t)43), /*hidden argument*/NULL);
	}

IL_03d3:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::DetectFocusChange()
extern TypeInfo* GUIUtility_t6_185_il2cpp_TypeInfo_var;
extern "C" void TextEditor_DetectFocusChange_m6_1618 (TextEditor_t6_238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_HasFocus_7);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_1 = (__this->___controlID_1);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_2 = GUIUtility_get_keyboardControl_m6_1383(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		TextEditor_OnLostFocus_m6_1545(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		bool L_3 = (__this->___m_HasFocus_7);
		if (L_3)
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_4 = (__this->___controlID_1);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_185_il2cpp_TypeInfo_var);
		int32_t L_5 = GUIUtility_get_keyboardControl_m6_1383(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0042;
		}
	}
	{
		TextEditor_OnFocus_m6_1544(__this, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" bool TextGenerationSettings_CompareColors_m6_1619 (TextGenerationSettings_t6_152 * __this, Color_t6_40  ___left, Color_t6_40  ___right, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		float L_0 = ((&___left)->___r_0);
		float L_1 = ((&___right)->___r_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m6_394(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005d;
		}
	}
	{
		float L_3 = ((&___left)->___g_1);
		float L_4 = ((&___right)->___g_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_5 = Mathf_Approximately_m6_394(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005d;
		}
	}
	{
		float L_6 = ((&___left)->___b_2);
		float L_7 = ((&___right)->___b_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m6_394(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005d;
		}
	}
	{
		float L_9 = ((&___left)->___a_3);
		float L_10 = ((&___right)->___a_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_11 = Mathf_Approximately_m6_394(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_005e;
	}

IL_005d:
	{
		G_B5_0 = 0;
	}

IL_005e:
	{
		return G_B5_0;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" bool TextGenerationSettings_CompareVector2_m6_1620 (TextGenerationSettings_t6_152 * __this, Vector2_t6_48  ___left, Vector2_t6_48  ___right, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		float L_0 = ((&___left)->___x_1);
		float L_1 = ((&___right)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m6_394(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		float L_3 = ((&___left)->___y_2);
		float L_4 = ((&___right)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_5 = Mathf_Approximately_m6_394(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" bool TextGenerationSettings_Equals_m6_1621 (TextGenerationSettings_t6_152 * __this, TextGenerationSettings_t6_152  ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B20_0 = 0;
	{
		Color_t6_40  L_0 = (__this->___color_1);
		Color_t6_40  L_1 = ((&___other)->___color_1);
		bool L_2 = TextGenerationSettings_CompareColors_m6_1619(__this, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_3 = (__this->___fontSize_2);
		int32_t L_4 = ((&___other)->___fontSize_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_0174;
		}
	}
	{
		float L_5 = (__this->___scaleFactor_5);
		float L_6 = ((&___other)->___scaleFactor_5);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_7 = Mathf_Approximately_m6_394(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_8 = (__this->___resizeTextMinSize_9);
		int32_t L_9 = ((&___other)->___resizeTextMinSize_9);
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_10 = (__this->___resizeTextMaxSize_10);
		int32_t L_11 = ((&___other)->___resizeTextMaxSize_10);
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_0174;
		}
	}
	{
		float L_12 = (__this->___lineSpacing_3);
		float L_13 = ((&___other)->___lineSpacing_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_14 = Mathf_Approximately_m6_394(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_15 = (__this->___fontStyle_6);
		int32_t L_16 = ((&___other)->___fontStyle_6);
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_17 = (__this->___richText_4);
		bool L_18 = ((&___other)->___richText_4);
		if ((!(((uint32_t)L_17) == ((uint32_t)L_18))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_19 = (__this->___textAnchor_7);
		int32_t L_20 = ((&___other)->___textAnchor_7);
		if ((!(((uint32_t)L_19) == ((uint32_t)L_20))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_21 = (__this->___resizeTextForBestFit_8);
		bool L_22 = ((&___other)->___resizeTextForBestFit_8);
		if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_23 = (__this->___resizeTextMinSize_9);
		int32_t L_24 = ((&___other)->___resizeTextMinSize_9);
		if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_25 = (__this->___resizeTextMaxSize_10);
		int32_t L_26 = ((&___other)->___resizeTextMaxSize_10);
		if ((!(((uint32_t)L_25) == ((uint32_t)L_26))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_27 = (__this->___resizeTextForBestFit_8);
		bool L_28 = ((&___other)->___resizeTextForBestFit_8);
		if ((!(((uint32_t)L_27) == ((uint32_t)L_28))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_29 = (__this->___updateBounds_11);
		bool L_30 = ((&___other)->___updateBounds_11);
		if ((!(((uint32_t)L_29) == ((uint32_t)L_30))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_31 = (__this->___horizontalOverflow_13);
		int32_t L_32 = ((&___other)->___horizontalOverflow_13);
		if ((!(((uint32_t)L_31) == ((uint32_t)L_32))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_33 = (__this->___verticalOverflow_12);
		int32_t L_34 = ((&___other)->___verticalOverflow_12);
		if ((!(((uint32_t)L_33) == ((uint32_t)L_34))))
		{
			goto IL_0174;
		}
	}
	{
		Vector2_t6_48  L_35 = (__this->___generationExtents_14);
		Vector2_t6_48  L_36 = ((&___other)->___generationExtents_14);
		bool L_37 = TextGenerationSettings_CompareVector2_m6_1620(__this, L_35, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0174;
		}
	}
	{
		Vector2_t6_48  L_38 = (__this->___pivot_15);
		Vector2_t6_48  L_39 = ((&___other)->___pivot_15);
		bool L_40 = TextGenerationSettings_CompareVector2_m6_1620(__this, L_38, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0174;
		}
	}
	{
		Font_t6_148 * L_41 = (__this->___font_0);
		Font_t6_148 * L_42 = ((&___other)->___font_0);
		bool L_43 = Object_op_Equality_m6_579(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		G_B20_0 = ((int32_t)(L_43));
		goto IL_0175;
	}

IL_0174:
	{
		G_B20_0 = 0;
	}

IL_0175:
	{
		return G_B20_0;
	}
}
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern TypeInfo* TrackedReference_t6_136_il2cpp_TypeInfo_var;
extern "C" bool TrackedReference_Equals_m6_1622 (TrackedReference_t6_136 * __this, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackedReference_t6_136_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1039);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___o;
		bool L_1 = TrackedReference_op_Equality_m6_1624(NULL /*static, unused*/, ((TrackedReference_t6_136 *)IsInstClass(L_0, TrackedReference_t6_136_il2cpp_TypeInfo_var)), __this, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern "C" int32_t TrackedReference_GetHashCode_m6_1623 (TrackedReference_t6_136 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		int32_t L_1 = IntPtr_op_Explicit_m1_645(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" bool TrackedReference_op_Equality_m6_1624 (Object_t * __this /* static, unused */, TrackedReference_t6_136 * ___x, TrackedReference_t6_136 * ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	{
		TrackedReference_t6_136 * L_0 = ___x;
		V_0 = L_0;
		TrackedReference_t6_136 * L_1 = ___y;
		V_1 = L_1;
		Object_t * L_2 = V_1;
		if (L_2)
		{
			goto IL_0012;
		}
	}
	{
		Object_t * L_3 = V_0;
		if (L_3)
		{
			goto IL_0012;
		}
	}
	{
		return 1;
	}

IL_0012:
	{
		Object_t * L_4 = V_1;
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		TrackedReference_t6_136 * L_5 = ___x;
		NullCheck(L_5);
		IntPtr_t L_6 = (L_5->___m_Ptr_0);
		IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_8 = IntPtr_op_Equality_m1_640(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0029:
	{
		Object_t * L_9 = V_0;
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		TrackedReference_t6_136 * L_10 = ___y;
		NullCheck(L_10);
		IntPtr_t L_11 = (L_10->___m_Ptr_0);
		IntPtr_t L_12 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_13 = IntPtr_op_Equality_m1_640(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0040:
	{
		TrackedReference_t6_136 * L_14 = ___x;
		NullCheck(L_14);
		IntPtr_t L_15 = (L_14->___m_Ptr_0);
		TrackedReference_t6_136 * L_16 = ___y;
		NullCheck(L_16);
		IntPtr_t L_17 = (L_16->___m_Ptr_0);
		bool L_18 = IntPtr_op_Equality_m1_640(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		return L_18;
	}
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t6_136_marshal(const TrackedReference_t6_136& unmarshaled, TrackedReference_t6_136_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void TrackedReference_t6_136_marshal_back(const TrackedReference_t6_136_marshaled& marshaled, TrackedReference_t6_136& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t6_136_marshal_cleanup(TrackedReference_t6_136_marshaled& marshaled)
{
}
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C" void ArgumentCache__ctor_m6_1625 (ArgumentCache_t6_239 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Regex_t3_113_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2934;
extern Il2CppCodeGenString* _stringLiteral2935;
extern Il2CppCodeGenString* _stringLiteral2936;
extern "C" void ArgumentCache_TidyAssemblyTypeName_m6_1626 (ArgumentCache_t6_239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Regex_t3_113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(660);
		_stringLiteral2934 = il2cpp_codegen_string_literal_from_index(2934);
		_stringLiteral2935 = il2cpp_codegen_string_literal_from_index(2935);
		_stringLiteral2936 = il2cpp_codegen_string_literal_from_index(2936);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___m_ObjectArgumentAssemblyTypeName_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_397(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		String_t* L_2 = (__this->___m_ObjectArgumentAssemblyTypeName_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t3_113_il2cpp_TypeInfo_var);
		String_t* L_4 = Regex_Replace_m3_620(NULL /*static, unused*/, L_2, _stringLiteral2934, L_3, /*hidden argument*/NULL);
		__this->___m_ObjectArgumentAssemblyTypeName_0 = L_4;
		String_t* L_5 = (__this->___m_ObjectArgumentAssemblyTypeName_0);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		String_t* L_7 = Regex_Replace_m3_620(NULL /*static, unused*/, L_5, _stringLiteral2935, L_6, /*hidden argument*/NULL);
		__this->___m_ObjectArgumentAssemblyTypeName_0 = L_7;
		String_t* L_8 = (__this->___m_ObjectArgumentAssemblyTypeName_0);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		String_t* L_10 = Regex_Replace_m3_620(NULL /*static, unused*/, L_8, _stringLiteral2936, L_9, /*hidden argument*/NULL);
		__this->___m_ObjectArgumentAssemblyTypeName_0 = L_10;
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern "C" void ArgumentCache_OnBeforeSerialize_m6_1627 (ArgumentCache_t6_239 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m6_1626(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern "C" void ArgumentCache_OnAfterDeserialize_m6_1628 (ArgumentCache_t6_239 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m6_1626(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern TypeInfo* ArgumentCache_t6_239_il2cpp_TypeInfo_var;
extern "C" void PersistentCall__ctor_m6_1629 (PersistentCall_t6_242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentCache_t6_239_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1040);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArgumentCache_t6_239 * L_0 = (ArgumentCache_t6_239 *)il2cpp_codegen_object_new (ArgumentCache_t6_239_il2cpp_TypeInfo_var);
		ArgumentCache__ctor_m6_1625(L_0, /*hidden argument*/NULL);
		__this->___m_Arguments_0 = L_0;
		__this->___m_CallState_1 = 2;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern TypeInfo* List_1_t1_921_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_5615_MethodInfo_var;
extern "C" void PersistentCallGroup__ctor_m6_1630 (PersistentCallGroup_t6_243 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_921_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1042);
		List_1__ctor_m1_5615_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483827);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		List_1_t1_921 * L_0 = (List_1_t1_921 *)il2cpp_codegen_object_new (List_1_t1_921_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5615(L_0, /*hidden argument*/List_1__ctor_m1_5615_MethodInfo_var);
		__this->___m_Calls_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern TypeInfo* List_1_t1_922_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_5616_MethodInfo_var;
extern "C" void InvokableCallList__ctor_m6_1631 (InvokableCallList_t6_244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_922_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1044);
		List_1__ctor_m1_5616_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483828);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_922 * L_0 = (List_1_t1_922 *)il2cpp_codegen_object_new (List_1_t1_922_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5616(L_0, /*hidden argument*/List_1__ctor_m1_5616_MethodInfo_var);
		__this->___m_PersistentCalls_0 = L_0;
		List_1_t1_922 * L_1 = (List_1_t1_922 *)il2cpp_codegen_object_new (List_1_t1_922_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5616(L_1, /*hidden argument*/List_1__ctor_m1_5616_MethodInfo_var);
		__this->___m_RuntimeCalls_1 = L_1;
		List_1_t1_922 * L_2 = (List_1_t1_922 *)il2cpp_codegen_object_new (List_1_t1_922_il2cpp_TypeInfo_var);
		List_1__ctor_m1_5616(L_2, /*hidden argument*/List_1__ctor_m1_5616_MethodInfo_var);
		__this->___m_ExecutingCalls_2 = L_2;
		__this->___m_NeedsUpdate_3 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C" void InvokableCallList_ClearPersistent_m6_1632 (InvokableCallList_t6_244 * __this, const MethodInfo* method)
{
	{
		List_1_t1_922 * L_0 = (__this->___m_PersistentCalls_0);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear() */, L_0);
		__this->___m_NeedsUpdate_3 = 1;
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern TypeInfo* InvokableCallList_t6_244_il2cpp_TypeInfo_var;
extern TypeInfo* PersistentCallGroup_t6_243_il2cpp_TypeInfo_var;
extern "C" void UnityEventBase__ctor_m6_1633 (UnityEventBase_t6_245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvokableCallList_t6_244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1045);
		PersistentCallGroup_t6_243_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1046);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_CallsDirty_3 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		InvokableCallList_t6_244 * L_0 = (InvokableCallList_t6_244 *)il2cpp_codegen_object_new (InvokableCallList_t6_244_il2cpp_TypeInfo_var);
		InvokableCallList__ctor_m6_1631(L_0, /*hidden argument*/NULL);
		__this->___m_Calls_0 = L_0;
		PersistentCallGroup_t6_243 * L_1 = (PersistentCallGroup_t6_243 *)il2cpp_codegen_object_new (PersistentCallGroup_t6_243_il2cpp_TypeInfo_var);
		PersistentCallGroup__ctor_m6_1630(L_1, /*hidden argument*/NULL);
		__this->___m_PersistentCalls_1 = L_1;
		Type_t * L_2 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_2);
		__this->___m_TypeName_2 = L_3;
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6_1634 (UnityEventBase_t6_245 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6_1635 (UnityEventBase_t6_245 * __this, const MethodInfo* method)
{
	{
		UnityEventBase_DirtyPersistentCalls_m6_1636(__this, /*hidden argument*/NULL);
		Type_t * L_0 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_0);
		__this->___m_TypeName_2 = L_1;
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C" void UnityEventBase_DirtyPersistentCalls_m6_1636 (UnityEventBase_t6_245 * __this, const MethodInfo* method)
{
	{
		InvokableCallList_t6_244 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		InvokableCallList_ClearPersistent_m6_1632(L_0, /*hidden argument*/NULL);
		__this->___m_CallsDirty_3 = 1;
		return;
	}
}
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral232;
extern "C" String_t* UnityEventBase_ToString_m6_1637 (UnityEventBase_t6_245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral232 = il2cpp_codegen_string_literal_from_index(232);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Object_ToString_m1_7(__this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_419(NULL /*static, unused*/, L_0, _stringLiteral232, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern TypeInfo* ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var;
extern "C" void UnityEvent__ctor_m6_1638 (UnityEvent_t6_246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_InvokeArray_4 = ((ObjectU5BU5D_t1_157*)SZArrayNew(ObjectU5BU5D_t1_157_il2cpp_TypeInfo_var, 0));
		UnityEventBase__ctor_m6_1633(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern "C" void DefaultValueAttribute__ctor_m6_1639 (DefaultValueAttribute_t6_247 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value;
		__this->___DefaultValue_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern "C" Object_t * DefaultValueAttribute_get_Value_m6_1640 (DefaultValueAttribute_t6_247 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___DefaultValue_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern TypeInfo* DefaultValueAttribute_t6_247_il2cpp_TypeInfo_var;
extern "C" bool DefaultValueAttribute_Equals_m6_1641 (DefaultValueAttribute_t6_247 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t6_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1047);
		s_Il2CppMethodIntialized = true;
	}
	DefaultValueAttribute_t6_247 * V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		V_0 = ((DefaultValueAttribute_t6_247 *)IsInstClass(L_0, DefaultValueAttribute_t6_247_il2cpp_TypeInfo_var));
		DefaultValueAttribute_t6_247 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		Object_t * L_2 = (__this->___DefaultValue_0);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		DefaultValueAttribute_t6_247 * L_3 = V_0;
		NullCheck(L_3);
		Object_t * L_4 = DefaultValueAttribute_get_Value_m6_1640(L_3, /*hidden argument*/NULL);
		return ((((Object_t*)(Object_t *)L_4) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}

IL_0024:
	{
		Object_t * L_5 = (__this->___DefaultValue_0);
		DefaultValueAttribute_t6_247 * L_6 = V_0;
		NullCheck(L_6);
		Object_t * L_7 = DefaultValueAttribute_get_Value_m6_1640(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_8 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_5, L_7);
		return L_8;
	}
}
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern "C" int32_t DefaultValueAttribute_GetHashCode_m6_1642 (DefaultValueAttribute_t6_247 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___DefaultValue_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = Attribute_GetHashCode_m1_21(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_0012:
	{
		Object_t * L_2 = (__this->___DefaultValue_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		return L_3;
	}
}
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern "C" void ExcludeFromDocsAttribute__ctor_m6_1643 (ExcludeFromDocsAttribute_t6_248 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C" void FormerlySerializedAsAttribute__ctor_m6_1644 (FormerlySerializedAsAttribute_t6_249 * __this, String_t* ___oldName, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName;
		__this->___m_oldName_0 = L_0;
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern TypeInfo* TypeInferenceRules_t6_250_il2cpp_TypeInfo_var;
extern "C" void TypeInferenceRuleAttribute__ctor_m6_1645 (TypeInferenceRuleAttribute_t6_251 * __this, int32_t ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRules_t6_250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1048);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___rule;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(TypeInferenceRules_t6_250_il2cpp_TypeInfo_var, &L_1);
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_2);
		TypeInferenceRuleAttribute__ctor_m6_1646(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern "C" void TypeInferenceRuleAttribute__ctor_m6_1646 (TypeInferenceRuleAttribute_t6_251 * __this, String_t* ___rule, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_17(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___rule;
		__this->____rule_0 = L_0;
		return;
	}
}
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern "C" String_t* TypeInferenceRuleAttribute_ToString_m6_1647 (TypeInferenceRuleAttribute_t6_251 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->____rule_0);
		return L_0;
	}
}
// System.Void UnityEngineInternal.GenericStack::.ctor()
extern "C" void GenericStack__ctor_m6_1648 (GenericStack_t6_162 * __this, const MethodInfo* method)
{
	{
		Stack__ctor_m1_1845(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void UnityAdsDelegate__ctor_m6_1649 (UnityAdsDelegate_t6_94 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate::Invoke()
extern "C" void UnityAdsDelegate_Invoke_m6_1650 (UnityAdsDelegate_t6_94 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		UnityAdsDelegate_Invoke_m6_1650((UnityAdsDelegate_t6_94 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_UnityAdsDelegate_t6_94(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Advertisements.UnityAdsDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * UnityAdsDelegate_BeginInvoke_m6_1651 (UnityAdsDelegate_t6_94 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate::EndInvoke(System.IAsyncResult)
extern "C" void UnityAdsDelegate_EndInvoke_m6_1652 (UnityAdsDelegate_t6_94 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
