﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void pinvoke_delegate_wrapper_Swapper_t1_26 ();
extern "C" void pinvoke_delegate_wrapper_AsyncCallback_t1_28 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1_103 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1_116 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t1_238 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t1_239 ();
extern "C" void pinvoke_delegate_wrapper_AddEventAdapter_t1_334 ();
extern "C" void pinvoke_delegate_wrapper_GetterAdapter_t1_349 ();
extern "C" void pinvoke_delegate_wrapper_RenewalDelegate_t1_431 ();
extern "C" void pinvoke_delegate_wrapper_CallbackHandler_t1_538 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t1_775 ();
extern "C" void pinvoke_delegate_wrapper_MemberFilter_t1_32 ();
extern "C" void pinvoke_delegate_wrapper_TypeFilter_t1_341 ();
extern "C" void pinvoke_delegate_wrapper_CrossContextDelegate_t1_776 ();
extern "C" void pinvoke_delegate_wrapper_HeaderHandler_t1_777 ();
extern "C" void pinvoke_delegate_wrapper_ThreadStart_t1_779 ();
extern "C" void pinvoke_delegate_wrapper_TimerCallback_t1_681 ();
extern "C" void pinvoke_delegate_wrapper_WaitCallback_t1_780 ();
extern "C" void pinvoke_delegate_wrapper_AppDomainInitializer_t1_691 ();
extern "C" void pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1_687 ();
extern "C" void pinvoke_delegate_wrapper_EventHandler_t1_267 ();
extern "C" void pinvoke_delegate_wrapper_ResolveEventHandler_t1_688 ();
extern "C" void pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t1_689 ();
extern "C" void pinvoke_delegate_wrapper_MatchAppendEvaluator_t3_104 ();
extern "C" void pinvoke_delegate_wrapper_CostDelegate_t3_143 ();
extern "C" void pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t3_47 ();
extern "C" void pinvoke_delegate_wrapper_MatchEvaluator_t3_176 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t4_39 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t4_111 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback_t4_90 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback2_t4_91 ();
extern "C" void pinvoke_delegate_wrapper_CertificateSelectionCallback_t4_78 ();
extern "C" void pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t4_79 ();
extern "C" void pinvoke_delegate_wrapper_MyAction_t5_6 ();
extern "C" void pinvoke_delegate_wrapper_SerializeMethod_t5_46 ();
extern "C" void pinvoke_delegate_wrapper_SerializeStreamMethod_t5_47 ();
extern "C" void pinvoke_delegate_wrapper_DeserializeMethod_t5_48 ();
extern "C" void pinvoke_delegate_wrapper_DeserializeStreamMethod_t5_49 ();
extern "C" void pinvoke_delegate_wrapper_IntegerMillisecondsDelegate_t5_54 ();
extern "C" void pinvoke_delegate_wrapper_StateChanged_t6_37 ();
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t6_59 ();
extern "C" void pinvoke_delegate_wrapper_LogCallback_t6_72 ();
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t6_74 ();
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t6_77 ();
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t6_117 ();
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t6_119 ();
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t6_120 ();
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t6_147 ();
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t6_159 ();
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t6_178 ();
extern "C" void pinvoke_delegate_wrapper_UnityAdsDelegate_t6_94 ();
extern "C" void pinvoke_delegate_wrapper_EventCallback_t8_113 ();
extern const methodPointerType g_DelegateWrappersManagedToNative[52] = 
{
	pinvoke_delegate_wrapper_Swapper_t1_26,
	pinvoke_delegate_wrapper_AsyncCallback_t1_28,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1_103,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1_116,
	pinvoke_delegate_wrapper_ReadDelegate_t1_238,
	pinvoke_delegate_wrapper_WriteDelegate_t1_239,
	pinvoke_delegate_wrapper_AddEventAdapter_t1_334,
	pinvoke_delegate_wrapper_GetterAdapter_t1_349,
	pinvoke_delegate_wrapper_RenewalDelegate_t1_431,
	pinvoke_delegate_wrapper_CallbackHandler_t1_538,
	pinvoke_delegate_wrapper_PrimalityTest_t1_775,
	pinvoke_delegate_wrapper_MemberFilter_t1_32,
	pinvoke_delegate_wrapper_TypeFilter_t1_341,
	pinvoke_delegate_wrapper_CrossContextDelegate_t1_776,
	pinvoke_delegate_wrapper_HeaderHandler_t1_777,
	pinvoke_delegate_wrapper_ThreadStart_t1_779,
	pinvoke_delegate_wrapper_TimerCallback_t1_681,
	pinvoke_delegate_wrapper_WaitCallback_t1_780,
	pinvoke_delegate_wrapper_AppDomainInitializer_t1_691,
	pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1_687,
	pinvoke_delegate_wrapper_EventHandler_t1_267,
	pinvoke_delegate_wrapper_ResolveEventHandler_t1_688,
	pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t1_689,
	pinvoke_delegate_wrapper_MatchAppendEvaluator_t3_104,
	pinvoke_delegate_wrapper_CostDelegate_t3_143,
	pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t3_47,
	pinvoke_delegate_wrapper_MatchEvaluator_t3_176,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t4_39,
	pinvoke_delegate_wrapper_PrimalityTest_t4_111,
	pinvoke_delegate_wrapper_CertificateValidationCallback_t4_90,
	pinvoke_delegate_wrapper_CertificateValidationCallback2_t4_91,
	pinvoke_delegate_wrapper_CertificateSelectionCallback_t4_78,
	pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t4_79,
	pinvoke_delegate_wrapper_MyAction_t5_6,
	pinvoke_delegate_wrapper_SerializeMethod_t5_46,
	pinvoke_delegate_wrapper_SerializeStreamMethod_t5_47,
	pinvoke_delegate_wrapper_DeserializeMethod_t5_48,
	pinvoke_delegate_wrapper_DeserializeStreamMethod_t5_49,
	pinvoke_delegate_wrapper_IntegerMillisecondsDelegate_t5_54,
	pinvoke_delegate_wrapper_StateChanged_t6_37,
	pinvoke_delegate_wrapper_ReapplyDrivenProperties_t6_59,
	pinvoke_delegate_wrapper_LogCallback_t6_72,
	pinvoke_delegate_wrapper_CameraCallback_t6_74,
	pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t6_77,
	pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t6_117,
	pinvoke_delegate_wrapper_PCMReaderCallback_t6_119,
	pinvoke_delegate_wrapper_PCMSetPositionCallback_t6_120,
	pinvoke_delegate_wrapper_FontTextureRebuildCallback_t6_147,
	pinvoke_delegate_wrapper_WindowFunction_t6_159,
	pinvoke_delegate_wrapper_SkinChangedDelegate_t6_178,
	pinvoke_delegate_wrapper_UnityAdsDelegate_t6_94,
	pinvoke_delegate_wrapper_EventCallback_t8_113,
};
