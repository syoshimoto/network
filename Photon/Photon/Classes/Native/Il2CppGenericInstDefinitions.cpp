﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Object_t_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Types[] = { &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0 = { 1, GenInst_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 4, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType Int32_t1_3_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t1_3_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t1_3_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t1_3_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1_332_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1_332_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1_332_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1_332_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1_332_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t1_331_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t1_331_0_0_0_Types[] = { &CustomAttributeNamedArgument_t1_331_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t1_331_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t1_331_0_0_0_Types };
extern const Il2CppType StrongName_t1_623_0_0_0;
static const Il2CppType* GenInst_StrongName_t1_623_0_0_0_Types[] = { &StrongName_t1_623_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t1_623_0_0_0 = { 1, GenInst_StrongName_t1_623_0_0_0_Types };
extern const Il2CppType DateTime_t1_127_0_0_0;
static const Il2CppType* GenInst_DateTime_t1_127_0_0_0_Types[] = { &DateTime_t1_127_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t1_127_0_0_0 = { 1, GenInst_DateTime_t1_127_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1_707_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1_707_0_0_0_Types[] = { &DateTimeOffset_t1_707_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1_707_0_0_0 = { 1, GenInst_DateTimeOffset_t1_707_0_0_0_Types };
extern const Il2CppType TimeSpan_t1_213_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t1_213_0_0_0_Types[] = { &TimeSpan_t1_213_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t1_213_0_0_0 = { 1, GenInst_TimeSpan_t1_213_0_0_0_Types };
extern const Il2CppType Guid_t1_730_0_0_0;
static const Il2CppType* GenInst_Guid_t1_730_0_0_0_Types[] = { &Guid_t1_730_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t1_730_0_0_0 = { 1, GenInst_Guid_t1_730_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t1_328_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t1_328_0_0_0_Types[] = { &CustomAttributeData_t1_328_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t1_328_0_0_0 = { 1, GenInst_CustomAttributeData_t1_328_0_0_0_Types };
extern const Il2CppType Boolean_t1_20_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Types[] = { &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0 = { 1, GenInst_Int32_t1_3_0_0_0_Types };
extern const Il2CppType NCommand_t5_13_0_0_0;
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_NCommand_t5_13_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &NCommand_t5_13_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_NCommand_t5_13_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_NCommand_t5_13_0_0_0_Types };
static const Il2CppType* GenInst_NCommand_t5_13_0_0_0_Types[] = { &NCommand_t5_13_0_0_0 };
extern const Il2CppGenericInst GenInst_NCommand_t5_13_0_0_0 = { 1, GenInst_NCommand_t5_13_0_0_0_Types };
extern const Il2CppType CmdLogItem_t5_31_0_0_0;
static const Il2CppType* GenInst_CmdLogItem_t5_31_0_0_0_Types[] = { &CmdLogItem_t5_31_0_0_0 };
extern const Il2CppGenericInst GenInst_CmdLogItem_t5_31_0_0_0 = { 1, GenInst_CmdLogItem_t5_31_0_0_0_Types };
extern const Il2CppType Byte_t1_11_0_0_0;
static const Il2CppType* GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_Types[] = { &Byte_t1_11_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0 = { 2, GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType MyAction_t5_6_0_0_0;
static const Il2CppType* GenInst_MyAction_t5_6_0_0_0_Types[] = { &MyAction_t5_6_0_0_0 };
extern const Il2CppGenericInst GenInst_MyAction_t5_6_0_0_0 = { 1, GenInst_MyAction_t5_6_0_0_0_Types };
extern const Il2CppType SimulationItem_t5_28_0_0_0;
static const Il2CppType* GenInst_SimulationItem_t5_28_0_0_0_Types[] = { &SimulationItem_t5_28_0_0_0 };
extern const Il2CppGenericInst GenInst_SimulationItem_t5_28_0_0_0 = { 1, GenInst_SimulationItem_t5_28_0_0_0_Types };
extern const Il2CppType EnetChannel_t5_5_0_0_0;
static const Il2CppType* GenInst_Byte_t1_11_0_0_0_EnetChannel_t5_5_0_0_0_Types[] = { &Byte_t1_11_0_0_0, &EnetChannel_t5_5_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1_11_0_0_0_EnetChannel_t5_5_0_0_0 = { 2, GenInst_Byte_t1_11_0_0_0_EnetChannel_t5_5_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t1_167_0_0_0;
static const Il2CppType* GenInst_DictionaryEntry_t1_167_0_0_0_Types[] = { &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1_167_0_0_0 = { 1, GenInst_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType CustomType_t5_50_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_CustomType_t5_50_0_0_0_Types[] = { &Type_t_0_0_0, &CustomType_t5_50_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_CustomType_t5_50_0_0_0 = { 2, GenInst_Type_t_0_0_0_CustomType_t5_50_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t1_11_0_0_0_CustomType_t5_50_0_0_0_Types[] = { &Byte_t1_11_0_0_0, &CustomType_t5_50_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1_11_0_0_0_CustomType_t5_50_0_0_0 = { 2, GenInst_Byte_t1_11_0_0_0_CustomType_t5_50_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t1_20_0_0_0_Types[] = { &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t1_20_0_0_0 = { 1, GenInst_Boolean_t1_20_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t1_71_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t1_71_0_0_0_Types[] = { &ByteU5BU5D_t1_71_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t1_71_0_0_0 = { 1, GenInst_ByteU5BU5D_t1_71_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t6_22_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t6_22_0_0_0_Types[] = { &GcLeaderboard_t6_22_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t6_22_0_0_0 = { 1, GenInst_GcLeaderboard_t6_22_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t6_266_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t6_266_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t6_266_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t6_266_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t6_266_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t6_268_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t6_268_0_0_0_Types[] = { &IAchievementU5BU5D_t6_268_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t6_268_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t6_268_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t6_218_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t6_218_0_0_0_Types[] = { &IScoreU5BU5D_t6_218_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t6_218_0_0_0 = { 1, GenInst_IScoreU5BU5D_t6_218_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t6_214_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t6_214_0_0_0_Types[] = { &IUserProfileU5BU5D_t6_214_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t6_214_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t6_214_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t6_113_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t6_113_0_0_0_Types[] = { &Rigidbody2D_t6_113_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t6_113_0_0_0 = { 1, GenInst_Rigidbody2D_t6_113_0_0_0_Types };
extern const Il2CppType Font_t6_148_0_0_0;
static const Il2CppType* GenInst_Font_t6_148_0_0_0_Types[] = { &Font_t6_148_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t6_148_0_0_0 = { 1, GenInst_Font_t6_148_0_0_0_Types };
extern const Il2CppType UIVertex_t6_153_0_0_0;
static const Il2CppType* GenInst_UIVertex_t6_153_0_0_0_Types[] = { &UIVertex_t6_153_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t6_153_0_0_0 = { 1, GenInst_UIVertex_t6_153_0_0_0_Types };
extern const Il2CppType UICharInfo_t6_149_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t6_149_0_0_0_Types[] = { &UICharInfo_t6_149_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t6_149_0_0_0 = { 1, GenInst_UICharInfo_t6_149_0_0_0_Types };
extern const Il2CppType UILineInfo_t6_150_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t6_150_0_0_0_Types[] = { &UILineInfo_t6_150_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t6_150_0_0_0 = { 1, GenInst_UILineInfo_t6_150_0_0_0_Types };
extern const Il2CppType LayoutCache_t6_168_0_0_0;
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_168_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &LayoutCache_t6_168_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_168_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_168_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t6_171_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t6_171_0_0_0_Types[] = { &GUILayoutEntry_t6_171_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t6_171_0_0_0 = { 1, GenInst_GUILayoutEntry_t6_171_0_0_0_Types };
extern const Il2CppType GUIStyle_t6_166_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t6_166_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t6_166_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t6_166_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t6_166_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType GUILayer_t6_31_0_0_0;
static const Il2CppType* GenInst_GUILayer_t6_31_0_0_0_Types[] = { &GUILayer_t6_31_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t6_31_0_0_0 = { 1, GenInst_GUILayer_t6_31_0_0_0_Types };
extern const Il2CppType Event_t6_154_0_0_0;
extern const Il2CppType TextEditOp_t6_237_0_0_0;
static const Il2CppType* GenInst_Event_t6_154_0_0_0_TextEditOp_t6_237_0_0_0_Types[] = { &Event_t6_154_0_0_0, &TextEditOp_t6_237_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t6_154_0_0_0_TextEditOp_t6_237_0_0_0 = { 2, GenInst_Event_t6_154_0_0_0_TextEditOp_t6_237_0_0_0_Types };
extern const Il2CppType PersistentCall_t6_242_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t6_242_0_0_0_Types[] = { &PersistentCall_t6_242_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t6_242_0_0_0 = { 1, GenInst_PersistentCall_t6_242_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t6_240_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t6_240_0_0_0_Types[] = { &BaseInvokableCall_t6_240_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t6_240_0_0_0 = { 1, GenInst_BaseInvokableCall_t6_240_0_0_0_Types };
extern const Il2CppType ChatChannel_t7_1_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ChatChannel_t7_1_0_0_0_Types[] = { &String_t_0_0_0, &ChatChannel_t7_1_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ChatChannel_t7_1_0_0_0 = { 2, GenInst_String_t_0_0_0_ChatChannel_t7_1_0_0_0_Types };
extern const Il2CppType ConnectionProtocol_t5_36_0_0_0;
static const Il2CppType* GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_Types[] = { &ConnectionProtocol_t5_36_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0 = { 2, GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_Types };
extern const Il2CppType Animator_t6_138_0_0_0;
static const Il2CppType* GenInst_Animator_t6_138_0_0_0_Types[] = { &Animator_t6_138_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t6_138_0_0_0 = { 1, GenInst_Animator_t6_138_0_0_0_Types };
extern const Il2CppType PhotonView_t8_3_0_0_0;
static const Il2CppType* GenInst_PhotonView_t8_3_0_0_0_Types[] = { &PhotonView_t8_3_0_0_0 };
extern const Il2CppGenericInst GenInst_PhotonView_t8_3_0_0_0 = { 1, GenInst_PhotonView_t8_3_0_0_0_Types };
extern const Il2CppType Rigidbody_t6_102_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t6_102_0_0_0_Types[] = { &Rigidbody_t6_102_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t6_102_0_0_0 = { 1, GenInst_Rigidbody_t6_102_0_0_0_Types };
extern const Il2CppType InputToEvent_t8_152_0_0_0;
static const Il2CppType* GenInst_InputToEvent_t8_152_0_0_0_Types[] = { &InputToEvent_t8_152_0_0_0 };
extern const Il2CppGenericInst GenInst_InputToEvent_t8_152_0_0_0 = { 1, GenInst_InputToEvent_t8_152_0_0_0_Types };
extern const Il2CppType Renderer_t6_25_0_0_0;
static const Il2CppType* GenInst_Renderer_t6_25_0_0_0_Types[] = { &Renderer_t6_25_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t6_25_0_0_0 = { 1, GenInst_Renderer_t6_25_0_0_0_Types };
extern const Il2CppType ChatGui_t8_14_0_0_0;
static const Il2CppType* GenInst_ChatGui_t8_14_0_0_0_Types[] = { &ChatGui_t8_14_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatGui_t8_14_0_0_0 = { 1, GenInst_ChatGui_t8_14_0_0_0_Types };
extern const Il2CppType FriendInfo_t8_74_0_0_0;
static const Il2CppType* GenInst_FriendInfo_t8_74_0_0_0_Types[] = { &FriendInfo_t8_74_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendInfo_t8_74_0_0_0 = { 1, GenInst_FriendInfo_t8_74_0_0_0_Types };
extern const Il2CppType Camera_t6_75_0_0_0;
static const Il2CppType* GenInst_Camera_t6_75_0_0_0_Types[] = { &Camera_t6_75_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t6_75_0_0_0 = { 1, GenInst_Camera_t6_75_0_0_0_Types };
extern const Il2CppType PhotonAnimatorView_t8_27_0_0_0;
static const Il2CppType* GenInst_PhotonAnimatorView_t8_27_0_0_0_Types[] = { &PhotonAnimatorView_t8_27_0_0_0 };
extern const Il2CppGenericInst GenInst_PhotonAnimatorView_t8_27_0_0_0 = { 1, GenInst_PhotonAnimatorView_t8_27_0_0_0_Types };
extern const Il2CppType PickupController_t8_32_0_0_0;
static const Il2CppType* GenInst_PickupController_t8_32_0_0_0_Types[] = { &PickupController_t8_32_0_0_0 };
extern const Il2CppGenericInst GenInst_PickupController_t8_32_0_0_0 = { 1, GenInst_PickupController_t8_32_0_0_0_Types };
extern const Il2CppType Collider_t6_100_0_0_0;
static const Il2CppType* GenInst_Collider_t6_100_0_0_0_Types[] = { &Collider_t6_100_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t6_100_0_0_0 = { 1, GenInst_Collider_t6_100_0_0_0_Types };
extern const Il2CppType Animation_t6_135_0_0_0;
static const Il2CppType* GenInst_Animation_t6_135_0_0_0_Types[] = { &Animation_t6_135_0_0_0 };
extern const Il2CppGenericInst GenInst_Animation_t6_135_0_0_0 = { 1, GenInst_Animation_t6_135_0_0_0_Types };
extern const Il2CppType CharacterController_t6_99_0_0_0;
static const Il2CppType* GenInst_CharacterController_t6_99_0_0_0_Types[] = { &CharacterController_t6_99_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterController_t6_99_0_0_0 = { 1, GenInst_CharacterController_t6_99_0_0_0_Types };
extern const Il2CppType PickupItem_t8_163_0_0_0;
static const Il2CppType* GenInst_PickupItem_t8_163_0_0_0_Types[] = { &PickupItem_t8_163_0_0_0 };
extern const Il2CppGenericInst GenInst_PickupItem_t8_163_0_0_0 = { 1, GenInst_PickupItem_t8_163_0_0_0_Types };
extern const Il2CppType Team_t8_169_0_0_0;
extern const Il2CppType List_1_t1_955_0_0_0;
static const Il2CppType* GenInst_Team_t8_169_0_0_0_List_1_t1_955_0_0_0_Types[] = { &Team_t8_169_0_0_0, &List_1_t1_955_0_0_0 };
extern const Il2CppGenericInst GenInst_Team_t8_169_0_0_0_List_1_t1_955_0_0_0 = { 2, GenInst_Team_t8_169_0_0_0_List_1_t1_955_0_0_0_Types };
extern const Il2CppType PhotonPlayer_t8_102_0_0_0;
static const Il2CppType* GenInst_PhotonPlayer_t8_102_0_0_0_Types[] = { &PhotonPlayer_t8_102_0_0_0 };
extern const Il2CppGenericInst GenInst_PhotonPlayer_t8_102_0_0_0 = { 1, GenInst_PhotonPlayer_t8_102_0_0_0_Types };
extern const Il2CppType PhotonTransformView_t8_39_0_0_0;
static const Il2CppType* GenInst_PhotonTransformView_t8_39_0_0_0_Types[] = { &PhotonTransformView_t8_39_0_0_0 };
extern const Il2CppGenericInst GenInst_PhotonTransformView_t8_39_0_0_0 = { 1, GenInst_PhotonTransformView_t8_39_0_0_0_Types };
extern const Il2CppType ThirdPersonController_t8_47_0_0_0;
static const Il2CppType* GenInst_ThirdPersonController_t8_47_0_0_0_Types[] = { &ThirdPersonController_t8_47_0_0_0 };
extern const Il2CppGenericInst GenInst_ThirdPersonController_t8_47_0_0_0 = { 1, GenInst_ThirdPersonController_t8_47_0_0_0_Types };
extern const Il2CppType ThirdPersonCamera_t8_46_0_0_0;
static const Il2CppType* GenInst_ThirdPersonCamera_t8_46_0_0_0_Types[] = { &ThirdPersonCamera_t8_46_0_0_0 };
extern const Il2CppGenericInst GenInst_ThirdPersonCamera_t8_46_0_0_0 = { 1, GenInst_ThirdPersonCamera_t8_46_0_0_0_Types };
extern const Il2CppType InRoomChat_t8_150_0_0_0;
static const Il2CppType* GenInst_InRoomChat_t8_150_0_0_0_Types[] = { &InRoomChat_t8_150_0_0_0 };
extern const Il2CppGenericInst GenInst_InRoomChat_t8_150_0_0_0 = { 1, GenInst_InRoomChat_t8_150_0_0_0_Types };
extern const Il2CppType AudioSource_t6_122_0_0_0;
static const Il2CppType* GenInst_AudioSource_t6_122_0_0_0_Types[] = { &AudioSource_t6_122_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t6_122_0_0_0 = { 1, GenInst_AudioSource_t6_122_0_0_0_Types };
extern const Il2CppType myThirdPersonController_t8_55_0_0_0;
static const Il2CppType* GenInst_myThirdPersonController_t8_55_0_0_0_Types[] = { &myThirdPersonController_t8_55_0_0_0 };
extern const Il2CppGenericInst GenInst_myThirdPersonController_t8_55_0_0_0 = { 1, GenInst_myThirdPersonController_t8_55_0_0_0_Types };
extern const Il2CppType TypedLobbyInfo_t8_95_0_0_0;
static const Il2CppType* GenInst_TypedLobbyInfo_t8_95_0_0_0_Types[] = { &TypedLobbyInfo_t8_95_0_0_0 };
extern const Il2CppGenericInst GenInst_TypedLobbyInfo_t8_95_0_0_0 = { 1, GenInst_TypedLobbyInfo_t8_95_0_0_0_Types };
extern const Il2CppType RoomInfo_t8_124_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_RoomInfo_t8_124_0_0_0_Types[] = { &String_t_0_0_0, &RoomInfo_t8_124_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RoomInfo_t8_124_0_0_0 = { 2, GenInst_String_t_0_0_0_RoomInfo_t8_124_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_PhotonPlayer_t8_102_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &PhotonPlayer_t8_102_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_PhotonPlayer_t8_102_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_PhotonPlayer_t8_102_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_PhotonView_t8_3_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &PhotonView_t8_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_PhotonView_t8_3_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_PhotonView_t8_3_0_0_0_Types };
extern const Il2CppType Hashtable_t5_1_0_0_0;
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Hashtable_t5_1_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Hashtable_t5_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Hashtable_t5_1_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_Hashtable_t5_1_0_0_0_Types };
extern const Il2CppType List_1_t1_893_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_List_1_t1_893_0_0_0_Types[] = { &Type_t_0_0_0, &List_1_t1_893_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_List_1_t1_893_0_0_0 = { 2, GenInst_Type_t_0_0_0_List_1_t1_893_0_0_0_Types };
extern const Il2CppType ObjectU5BU5D_t1_157_0_0_0;
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_ObjectU5BU5D_t1_157_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &ObjectU5BU5D_t1_157_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_ObjectU5BU5D_t1_157_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_ObjectU5BU5D_t1_157_0_0_0_Types };
extern const Il2CppType GameObject_t6_85_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GameObject_t6_85_0_0_0_Types[] = { &String_t_0_0_0, &GameObject_t6_85_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t6_85_0_0_0 = { 2, GenInst_String_t_0_0_0_GameObject_t6_85_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t6_85_0_0_0_Types[] = { &GameObject_t6_85_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t6_85_0_0_0 = { 1, GenInst_GameObject_t6_85_0_0_0_Types };
extern const Il2CppType Region_t8_110_0_0_0;
static const Il2CppType* GenInst_Region_t8_110_0_0_0_Types[] = { &Region_t8_110_0_0_0 };
extern const Il2CppGenericInst GenInst_Region_t8_110_0_0_0 = { 1, GenInst_Region_t8_110_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t1_11_0_0_0_Types[] = { &Byte_t1_11_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1_11_0_0_0 = { 1, GenInst_Byte_t1_11_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType PhotonHandler_t8_111_0_0_0;
static const Il2CppType* GenInst_PhotonHandler_t8_111_0_0_0_Types[] = { &PhotonHandler_t8_111_0_0_0 };
extern const Il2CppGenericInst GenInst_PhotonHandler_t8_111_0_0_0 = { 1, GenInst_PhotonHandler_t8_111_0_0_0_Types };
extern const Il2CppType Component_t6_26_0_0_0;
static const Il2CppType* GenInst_Component_t6_26_0_0_0_MethodInfo_t_0_0_0_Types[] = { &Component_t6_26_0_0_0, &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t6_26_0_0_0_MethodInfo_t_0_0_0 = { 2, GenInst_Component_t6_26_0_0_0_MethodInfo_t_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t6_80_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t6_80_0_0_0_Types[] = { &MonoBehaviour_t6_80_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t6_80_0_0_0 = { 1, GenInst_MonoBehaviour_t6_80_0_0_0_Types };
extern const Il2CppType SynchronizedParameter_t8_128_0_0_0;
static const Il2CppType* GenInst_SynchronizedParameter_t8_128_0_0_0_Types[] = { &SynchronizedParameter_t8_128_0_0_0 };
extern const Il2CppGenericInst GenInst_SynchronizedParameter_t8_128_0_0_0 = { 1, GenInst_SynchronizedParameter_t8_128_0_0_0_Types };
extern const Il2CppType SynchronizedLayer_t8_129_0_0_0;
static const Il2CppType* GenInst_SynchronizedLayer_t8_129_0_0_0_Types[] = { &SynchronizedLayer_t8_129_0_0_0 };
extern const Il2CppGenericInst GenInst_SynchronizedLayer_t8_129_0_0_0 = { 1, GenInst_SynchronizedLayer_t8_129_0_0_0_Types };
extern const Il2CppType Vector3_t6_49_0_0_0;
static const Il2CppType* GenInst_Vector3_t6_49_0_0_0_Types[] = { &Vector3_t6_49_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t6_49_0_0_0 = { 1, GenInst_Vector3_t6_49_0_0_0_Types };
extern const Il2CppType OnClickDestroy_t8_157_0_0_0;
static const Il2CppType* GenInst_OnClickDestroy_t8_157_0_0_0_Types[] = { &OnClickDestroy_t8_157_0_0_0 };
extern const Il2CppGenericInst GenInst_OnClickDestroy_t8_157_0_0_0 = { 1, GenInst_OnClickDestroy_t8_157_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t6_69_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t6_69_0_0_0_Types[] = { &SpriteRenderer_t6_69_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t6_69_0_0_0 = { 1, GenInst_SpriteRenderer_t6_69_0_0_0_Types };
extern const Il2CppType Single_t1_17_0_0_0;
static const Il2CppType* GenInst_Single_t1_17_0_0_0_Types[] = { &Single_t1_17_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t1_17_0_0_0 = { 1, GenInst_Single_t1_17_0_0_0_Types };
extern const Il2CppType MeshRenderer_t6_28_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t6_28_0_0_0_Types[] = { &MeshRenderer_t6_28_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t6_28_0_0_0 = { 1, GenInst_MeshRenderer_t6_28_0_0_0_Types };
extern const Il2CppType TextMesh_t6_145_0_0_0;
static const Il2CppType* GenInst_TextMesh_t6_145_0_0_0_Types[] = { &TextMesh_t6_145_0_0_0 };
extern const Il2CppGenericInst GenInst_TextMesh_t6_145_0_0_0 = { 1, GenInst_TextMesh_t6_145_0_0_0_Types };
extern const Il2CppType SupportLogging_t8_178_0_0_0;
static const Il2CppType* GenInst_SupportLogging_t8_178_0_0_0_Types[] = { &SupportLogging_t8_178_0_0_0 };
extern const Il2CppGenericInst GenInst_SupportLogging_t8_178_0_0_0 = { 1, GenInst_SupportLogging_t8_178_0_0_0_Types };
extern const Il2CppType Char_t1_15_0_0_0;
static const Il2CppType* GenInst_Char_t1_15_0_0_0_Types[] = { &Char_t1_15_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t1_15_0_0_0 = { 1, GenInst_Char_t1_15_0_0_0_Types };
extern const Il2CppType IConvertible_t1_834_0_0_0;
static const Il2CppType* GenInst_IConvertible_t1_834_0_0_0_Types[] = { &IConvertible_t1_834_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t1_834_0_0_0 = { 1, GenInst_IConvertible_t1_834_0_0_0_Types };
extern const Il2CppType IComparable_t1_833_0_0_0;
static const Il2CppType* GenInst_IComparable_t1_833_0_0_0_Types[] = { &IComparable_t1_833_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1_833_0_0_0 = { 1, GenInst_IComparable_t1_833_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1747_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1747_0_0_0_Types[] = { &IComparable_1_t1_1747_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1747_0_0_0 = { 1, GenInst_IComparable_1_t1_1747_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1752_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1752_0_0_0_Types[] = { &IEquatable_1_t1_1752_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1752_0_0_0 = { 1, GenInst_IEquatable_1_t1_1752_0_0_0_Types };
extern const Il2CppType ValueType_t1_1_0_0_0;
static const Il2CppType* GenInst_ValueType_t1_1_0_0_0_Types[] = { &ValueType_t1_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t1_1_0_0_0 = { 1, GenInst_ValueType_t1_1_0_0_0_Types };
extern const Il2CppType Int64_t1_7_0_0_0;
static const Il2CppType* GenInst_Int64_t1_7_0_0_0_Types[] = { &Int64_t1_7_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t1_7_0_0_0 = { 1, GenInst_Int64_t1_7_0_0_0_Types };
extern const Il2CppType UInt32_t1_8_0_0_0;
static const Il2CppType* GenInst_UInt32_t1_8_0_0_0_Types[] = { &UInt32_t1_8_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t1_8_0_0_0 = { 1, GenInst_UInt32_t1_8_0_0_0_Types };
extern const Il2CppType UInt64_t1_10_0_0_0;
static const Il2CppType* GenInst_UInt64_t1_10_0_0_0_Types[] = { &UInt64_t1_10_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t1_10_0_0_0 = { 1, GenInst_UInt64_t1_10_0_0_0_Types };
extern const Il2CppType SByte_t1_12_0_0_0;
static const Il2CppType* GenInst_SByte_t1_12_0_0_0_Types[] = { &SByte_t1_12_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t1_12_0_0_0 = { 1, GenInst_SByte_t1_12_0_0_0_Types };
extern const Il2CppType Int16_t1_13_0_0_0;
static const Il2CppType* GenInst_Int16_t1_13_0_0_0_Types[] = { &Int16_t1_13_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t1_13_0_0_0 = { 1, GenInst_Int16_t1_13_0_0_0_Types };
extern const Il2CppType UInt16_t1_14_0_0_0;
static const Il2CppType* GenInst_UInt16_t1_14_0_0_0_Types[] = { &UInt16_t1_14_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t1_14_0_0_0 = { 1, GenInst_UInt16_t1_14_0_0_0_Types };
extern const Il2CppType IEnumerable_t1_836_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t1_836_0_0_0_Types[] = { &IEnumerable_t1_836_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t1_836_0_0_0 = { 1, GenInst_IEnumerable_t1_836_0_0_0_Types };
extern const Il2CppType ICloneable_t1_846_0_0_0;
static const Il2CppType* GenInst_ICloneable_t1_846_0_0_0_Types[] = { &ICloneable_t1_846_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t1_846_0_0_0 = { 1, GenInst_ICloneable_t1_846_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1776_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1776_0_0_0_Types[] = { &IComparable_1_t1_1776_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1776_0_0_0 = { 1, GenInst_IComparable_1_t1_1776_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1777_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1777_0_0_0_Types[] = { &IEquatable_1_t1_1777_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1777_0_0_0 = { 1, GenInst_IEquatable_1_t1_1777_0_0_0_Types };
extern const Il2CppType IReflect_t1_1618_0_0_0;
static const Il2CppType* GenInst_IReflect_t1_1618_0_0_0_Types[] = { &IReflect_t1_1618_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t1_1618_0_0_0 = { 1, GenInst_IReflect_t1_1618_0_0_0_Types };
extern const Il2CppType _Type_t1_1616_0_0_0;
static const Il2CppType* GenInst__Type_t1_1616_0_0_0_Types[] = { &_Type_t1_1616_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t1_1616_0_0_0 = { 1, GenInst__Type_t1_1616_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t1_828_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t1_828_0_0_0_Types[] = { &ICustomAttributeProvider_t1_828_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1_828_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t1_828_0_0_0_Types };
extern const Il2CppType _MemberInfo_t1_1617_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t1_1617_0_0_0_Types[] = { &_MemberInfo_t1_1617_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t1_1617_0_0_0 = { 1, GenInst__MemberInfo_t1_1617_0_0_0_Types };
extern const Il2CppType IFormattable_t1_831_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1_831_0_0_0_Types[] = { &IFormattable_t1_831_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1_831_0_0_0 = { 1, GenInst_IFormattable_t1_831_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1768_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1768_0_0_0_Types[] = { &IComparable_1_t1_1768_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1768_0_0_0 = { 1, GenInst_IComparable_1_t1_1768_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1769_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1769_0_0_0_Types[] = { &IEquatable_1_t1_1769_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1769_0_0_0 = { 1, GenInst_IEquatable_1_t1_1769_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1837_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1837_0_0_0_Types[] = { &IComparable_1_t1_1837_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1837_0_0_0 = { 1, GenInst_IComparable_1_t1_1837_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1842_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1842_0_0_0_Types[] = { &IEquatable_1_t1_1842_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1842_0_0_0 = { 1, GenInst_IEquatable_1_t1_1842_0_0_0_Types };
extern const Il2CppType Double_t1_18_0_0_0;
static const Il2CppType* GenInst_Double_t1_18_0_0_0_Types[] = { &Double_t1_18_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t1_18_0_0_0 = { 1, GenInst_Double_t1_18_0_0_0_Types };
extern const Il2CppType Decimal_t1_19_0_0_0;
static const Il2CppType* GenInst_Decimal_t1_19_0_0_0_Types[] = { &Decimal_t1_19_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t1_19_0_0_0 = { 1, GenInst_Decimal_t1_19_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1733_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1733_0_0_0_Types[] = { &IComparable_1_t1_1733_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1733_0_0_0 = { 1, GenInst_IComparable_1_t1_1733_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1734_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1734_0_0_0_Types[] = { &IEquatable_1_t1_1734_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1734_0_0_0 = { 1, GenInst_IEquatable_1_t1_1734_0_0_0_Types };
extern const Il2CppType Delegate_t1_22_0_0_0;
static const Il2CppType* GenInst_Delegate_t1_22_0_0_0_Types[] = { &Delegate_t1_22_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t1_22_0_0_0 = { 1, GenInst_Delegate_t1_22_0_0_0_Types };
extern const Il2CppType ISerializable_t1_866_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1_866_0_0_0_Types[] = { &ISerializable_t1_866_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1_866_0_0_0 = { 1, GenInst_ISerializable_t1_866_0_0_0_Types };
extern const Il2CppType ParameterInfo_t1_351_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t1_351_0_0_0_Types[] = { &ParameterInfo_t1_351_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t1_351_0_0_0 = { 1, GenInst_ParameterInfo_t1_351_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t1_1658_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t1_1658_0_0_0_Types[] = { &_ParameterInfo_t1_1658_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t1_1658_0_0_0 = { 1, GenInst__ParameterInfo_t1_1658_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1_352_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1_352_0_0_0_Types[] = { &ParameterModifier_t1_352_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1_352_0_0_0 = { 1, GenInst_ParameterModifier_t1_352_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1774_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1774_0_0_0_Types[] = { &IComparable_1_t1_1774_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1774_0_0_0 = { 1, GenInst_IComparable_1_t1_1774_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1775_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1775_0_0_0_Types[] = { &IEquatable_1_t1_1775_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1775_0_0_0 = { 1, GenInst_IEquatable_1_t1_1775_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1764_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1764_0_0_0_Types[] = { &IComparable_1_t1_1764_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1764_0_0_0 = { 1, GenInst_IComparable_1_t1_1764_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1765_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1765_0_0_0_Types[] = { &IEquatable_1_t1_1765_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1765_0_0_0 = { 1, GenInst_IEquatable_1_t1_1765_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1766_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1766_0_0_0_Types[] = { &IComparable_1_t1_1766_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1766_0_0_0 = { 1, GenInst_IComparable_1_t1_1766_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1767_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1767_0_0_0_Types[] = { &IEquatable_1_t1_1767_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1767_0_0_0 = { 1, GenInst_IEquatable_1_t1_1767_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1772_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1772_0_0_0_Types[] = { &IComparable_1_t1_1772_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1772_0_0_0 = { 1, GenInst_IComparable_1_t1_1772_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1773_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1773_0_0_0_Types[] = { &IEquatable_1_t1_1773_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1773_0_0_0 = { 1, GenInst_IEquatable_1_t1_1773_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1770_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1770_0_0_0_Types[] = { &IComparable_1_t1_1770_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1770_0_0_0 = { 1, GenInst_IComparable_1_t1_1770_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1771_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1771_0_0_0_Types[] = { &IEquatable_1_t1_1771_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1771_0_0_0 = { 1, GenInst_IEquatable_1_t1_1771_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1762_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1762_0_0_0_Types[] = { &IComparable_1_t1_1762_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1762_0_0_0 = { 1, GenInst_IComparable_1_t1_1762_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1763_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1763_0_0_0_Types[] = { &IEquatable_1_t1_1763_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1763_0_0_0 = { 1, GenInst_IEquatable_1_t1_1763_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1846_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1846_0_0_0_Types[] = { &IComparable_1_t1_1846_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1846_0_0_0 = { 1, GenInst_IComparable_1_t1_1846_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1847_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1847_0_0_0_Types[] = { &IEquatable_1_t1_1847_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1847_0_0_0 = { 1, GenInst_IEquatable_1_t1_1847_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t1_1654_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t1_1654_0_0_0_Types[] = { &_FieldInfo_t1_1654_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t1_1654_0_0_0 = { 1, GenInst__FieldInfo_t1_1654_0_0_0_Types };
extern const Il2CppType _MethodInfo_t1_1656_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t1_1656_0_0_0_Types[] = { &_MethodInfo_t1_1656_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t1_1656_0_0_0 = { 1, GenInst__MethodInfo_t1_1656_0_0_0_Types };
extern const Il2CppType MethodBase_t1_198_0_0_0;
static const Il2CppType* GenInst_MethodBase_t1_198_0_0_0_Types[] = { &MethodBase_t1_198_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t1_198_0_0_0 = { 1, GenInst_MethodBase_t1_198_0_0_0_Types };
extern const Il2CppType _MethodBase_t1_1655_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1_1655_0_0_0_Types[] = { &_MethodBase_t1_1655_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1_1655_0_0_0 = { 1, GenInst__MethodBase_t1_1655_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t1_273_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t1_273_0_0_0_Types[] = { &ConstructorInfo_t1_273_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t1_273_0_0_0 = { 1, GenInst_ConstructorInfo_t1_273_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t1_1652_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t1_1652_0_0_0_Types[] = { &_ConstructorInfo_t1_1652_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t1_1652_0_0_0 = { 1, GenInst__ConstructorInfo_t1_1652_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType TableRange_t1_66_0_0_0;
static const Il2CppType* GenInst_TableRange_t1_66_0_0_0_Types[] = { &TableRange_t1_66_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t1_66_0_0_0 = { 1, GenInst_TableRange_t1_66_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1_69_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1_69_0_0_0_Types[] = { &TailoringInfo_t1_69_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1_69_0_0_0 = { 1, GenInst_TailoringInfo_t1_69_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1016_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1016_0_0_0_Types[] = { &KeyValuePair_2_t1_1016_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1016_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1016_0_0_0_Types };
extern const Il2CppType Link_t1_150_0_0_0;
static const Il2CppType* GenInst_Link_t1_150_0_0_0_Types[] = { &Link_t1_150_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1_150_0_0_0 = { 1, GenInst_Link_t1_150_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t1_3_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t1_3_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t1_3_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_KeyValuePair_2_t1_1016_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t1_3_0_0_0, &KeyValuePair_2_t1_1016_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_KeyValuePair_2_t1_1016_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_KeyValuePair_2_t1_1016_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t1_3_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1032_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1032_0_0_0_Types[] = { &KeyValuePair_2_t1_1032_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1032_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1032_0_0_0_Types };
extern const Il2CppType Contraction_t1_70_0_0_0;
static const Il2CppType* GenInst_Contraction_t1_70_0_0_0_Types[] = { &Contraction_t1_70_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1_70_0_0_0 = { 1, GenInst_Contraction_t1_70_0_0_0_Types };
extern const Il2CppType Level2Map_t1_73_0_0_0;
static const Il2CppType* GenInst_Level2Map_t1_73_0_0_0_Types[] = { &Level2Map_t1_73_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t1_73_0_0_0 = { 1, GenInst_Level2Map_t1_73_0_0_0_Types };
extern const Il2CppType BigInteger_t1_95_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1_95_0_0_0_Types[] = { &BigInteger_t1_95_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1_95_0_0_0 = { 1, GenInst_BigInteger_t1_95_0_0_0_Types };
extern const Il2CppType KeySizes_t1_575_0_0_0;
static const Il2CppType* GenInst_KeySizes_t1_575_0_0_0_Types[] = { &KeySizes_t1_575_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t1_575_0_0_0 = { 1, GenInst_KeySizes_t1_575_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1044_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1044_0_0_0_Types[] = { &KeyValuePair_2_t1_1044_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1044_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1044_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_1044_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1_1044_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_1044_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_1044_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1859_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1859_0_0_0_Types[] = { &IComparable_1_t1_1859_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1859_0_0_0 = { 1, GenInst_IComparable_1_t1_1859_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1860_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1860_0_0_0_Types[] = { &IEquatable_1_t1_1860_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1860_0_0_0 = { 1, GenInst_IEquatable_1_t1_1860_0_0_0_Types };
extern const Il2CppType Slot_t1_168_0_0_0;
static const Il2CppType* GenInst_Slot_t1_168_0_0_0_Types[] = { &Slot_t1_168_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1_168_0_0_0 = { 1, GenInst_Slot_t1_168_0_0_0_Types };
extern const Il2CppType Slot_t1_183_0_0_0;
static const Il2CppType* GenInst_Slot_t1_183_0_0_0_Types[] = { &Slot_t1_183_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1_183_0_0_0 = { 1, GenInst_Slot_t1_183_0_0_0_Types };
extern const Il2CppType StackFrame_t1_197_0_0_0;
static const Il2CppType* GenInst_StackFrame_t1_197_0_0_0_Types[] = { &StackFrame_t1_197_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t1_197_0_0_0 = { 1, GenInst_StackFrame_t1_197_0_0_0_Types };
extern const Il2CppType Calendar_t1_201_0_0_0;
static const Il2CppType* GenInst_Calendar_t1_201_0_0_0_Types[] = { &Calendar_t1_201_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t1_201_0_0_0 = { 1, GenInst_Calendar_t1_201_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t1_293_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t1_293_0_0_0_Types[] = { &ModuleBuilder_t1_293_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t1_293_0_0_0 = { 1, GenInst_ModuleBuilder_t1_293_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1_1647_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1_1647_0_0_0_Types[] = { &_ModuleBuilder_t1_1647_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1_1647_0_0_0 = { 1, GenInst__ModuleBuilder_t1_1647_0_0_0_Types };
extern const Il2CppType Module_t1_289_0_0_0;
static const Il2CppType* GenInst_Module_t1_289_0_0_0_Types[] = { &Module_t1_289_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t1_289_0_0_0 = { 1, GenInst_Module_t1_289_0_0_0_Types };
extern const Il2CppType _Module_t1_1657_0_0_0;
static const Il2CppType* GenInst__Module_t1_1657_0_0_0_Types[] = { &_Module_t1_1657_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t1_1657_0_0_0 = { 1, GenInst__Module_t1_1657_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t1_299_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t1_299_0_0_0_Types[] = { &ParameterBuilder_t1_299_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1_299_0_0_0 = { 1, GenInst_ParameterBuilder_t1_299_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t1_1648_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t1_1648_0_0_0_Types[] = { &_ParameterBuilder_t1_1648_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t1_1648_0_0_0 = { 1, GenInst__ParameterBuilder_t1_1648_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t1_31_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t1_31_0_0_0_Types[] = { &TypeU5BU5D_t1_31_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1_31_0_0_0 = { 1, GenInst_TypeU5BU5D_t1_31_0_0_0_Types };
extern const Il2CppType Array_t_0_0_0;
static const Il2CppType* GenInst_Array_t_0_0_0_Types[] = { &Array_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_t_0_0_0 = { 1, GenInst_Array_t_0_0_0_Types };
extern const Il2CppType ICollection_t1_811_0_0_0;
static const Il2CppType* GenInst_ICollection_t1_811_0_0_0_Types[] = { &ICollection_t1_811_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t1_811_0_0_0 = { 1, GenInst_ICollection_t1_811_0_0_0_Types };
extern const Il2CppType IList_t1_413_0_0_0;
static const Il2CppType* GenInst_IList_t1_413_0_0_0_Types[] = { &IList_t1_413_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t1_413_0_0_0 = { 1, GenInst_IList_t1_413_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t1_283_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t1_283_0_0_0_Types[] = { &ILTokenInfo_t1_283_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1_283_0_0_0 = { 1, GenInst_ILTokenInfo_t1_283_0_0_0_Types };
extern const Il2CppType LabelData_t1_285_0_0_0;
static const Il2CppType* GenInst_LabelData_t1_285_0_0_0_Types[] = { &LabelData_t1_285_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t1_285_0_0_0 = { 1, GenInst_LabelData_t1_285_0_0_0_Types };
extern const Il2CppType LabelFixup_t1_284_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t1_284_0_0_0_Types[] = { &LabelFixup_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t1_284_0_0_0 = { 1, GenInst_LabelFixup_t1_284_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1_281_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1_281_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1_281_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1_281_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1_281_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1_275_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1_275_0_0_0_Types[] = { &TypeBuilder_t1_275_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1_275_0_0_0 = { 1, GenInst_TypeBuilder_t1_275_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t1_1649_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t1_1649_0_0_0_Types[] = { &_TypeBuilder_t1_1649_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t1_1649_0_0_0 = { 1, GenInst__TypeBuilder_t1_1649_0_0_0_Types };
extern const Il2CppType MethodBuilder_t1_282_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t1_282_0_0_0_Types[] = { &MethodBuilder_t1_282_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t1_282_0_0_0 = { 1, GenInst_MethodBuilder_t1_282_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t1_1646_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t1_1646_0_0_0_Types[] = { &_MethodBuilder_t1_1646_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t1_1646_0_0_0 = { 1, GenInst__MethodBuilder_t1_1646_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t1_272_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t1_272_0_0_0_Types[] = { &ConstructorBuilder_t1_272_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t1_272_0_0_0 = { 1, GenInst_ConstructorBuilder_t1_272_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t1_1642_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t1_1642_0_0_0_Types[] = { &_ConstructorBuilder_t1_1642_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1_1642_0_0_0 = { 1, GenInst__ConstructorBuilder_t1_1642_0_0_0_Types };
extern const Il2CppType FieldBuilder_t1_279_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t1_279_0_0_0_Types[] = { &FieldBuilder_t1_279_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1_279_0_0_0 = { 1, GenInst_FieldBuilder_t1_279_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t1_1644_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t1_1644_0_0_0_Types[] = { &_FieldBuilder_t1_1644_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1_1644_0_0_0 = { 1, GenInst__FieldBuilder_t1_1644_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t1_1659_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t1_1659_0_0_0_Types[] = { &_PropertyInfo_t1_1659_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1_1659_0_0_0 = { 1, GenInst__PropertyInfo_t1_1659_0_0_0_Types };
extern const Il2CppType ResourceInfo_t1_363_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t1_363_0_0_0_Types[] = { &ResourceInfo_t1_363_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t1_363_0_0_0 = { 1, GenInst_ResourceInfo_t1_363_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t1_364_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t1_364_0_0_0_Types[] = { &ResourceCacheItem_t1_364_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t1_364_0_0_0 = { 1, GenInst_ResourceCacheItem_t1_364_0_0_0_Types };
extern const Il2CppType IContextProperty_t1_822_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t1_822_0_0_0_Types[] = { &IContextProperty_t1_822_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t1_822_0_0_0 = { 1, GenInst_IContextProperty_t1_822_0_0_0_Types };
extern const Il2CppType Header_t1_453_0_0_0;
static const Il2CppType* GenInst_Header_t1_453_0_0_0_Types[] = { &Header_t1_453_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t1_453_0_0_0 = { 1, GenInst_Header_t1_453_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t1_860_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t1_860_0_0_0_Types[] = { &ITrackingHandler_t1_860_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t1_860_0_0_0 = { 1, GenInst_ITrackingHandler_t1_860_0_0_0_Types };
extern const Il2CppType IContextAttribute_t1_847_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t1_847_0_0_0_Types[] = { &IContextAttribute_t1_847_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t1_847_0_0_0 = { 1, GenInst_IContextAttribute_t1_847_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_2178_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_2178_0_0_0_Types[] = { &IComparable_1_t1_2178_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_2178_0_0_0 = { 1, GenInst_IComparable_1_t1_2178_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_2183_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_2183_0_0_0_Types[] = { &IEquatable_1_t1_2183_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_2183_0_0_0 = { 1, GenInst_IEquatable_1_t1_2183_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_1848_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_1848_0_0_0_Types[] = { &IComparable_1_t1_1848_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_1848_0_0_0 = { 1, GenInst_IComparable_1_t1_1848_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_1849_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_1849_0_0_0_Types[] = { &IEquatable_1_t1_1849_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_1849_0_0_0 = { 1, GenInst_IEquatable_1_t1_1849_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_2202_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_2202_0_0_0_Types[] = { &IComparable_1_t1_2202_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_2202_0_0_0 = { 1, GenInst_IComparable_1_t1_2202_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_2207_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_2207_0_0_0_Types[] = { &IEquatable_1_t1_2207_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_2207_0_0_0 = { 1, GenInst_IEquatable_1_t1_2207_0_0_0_Types };
extern const Il2CppType TypeTag_t1_510_0_0_0;
static const Il2CppType* GenInst_TypeTag_t1_510_0_0_0_Types[] = { &TypeTag_t1_510_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t1_510_0_0_0 = { 1, GenInst_TypeTag_t1_510_0_0_0_Types };
extern const Il2CppType Enum_t1_24_0_0_0;
static const Il2CppType* GenInst_Enum_t1_24_0_0_0_Types[] = { &Enum_t1_24_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t1_24_0_0_0 = { 1, GenInst_Enum_t1_24_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType Version_t1_319_0_0_0;
static const Il2CppType* GenInst_Version_t1_319_0_0_0_Types[] = { &Version_t1_319_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1_319_0_0_0 = { 1, GenInst_Version_t1_319_0_0_0_Types };
extern const Il2CppType Link_t2_22_0_0_0;
static const Il2CppType* GenInst_Link_t2_22_0_0_0_Types[] = { &Link_t2_22_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2_22_0_0_0 = { 1, GenInst_Link_t2_22_0_0_0_Types };
extern const Il2CppType IPAddress_t3_54_0_0_0;
static const Il2CppType* GenInst_IPAddress_t3_54_0_0_0_Types[] = { &IPAddress_t3_54_0_0_0 };
extern const Il2CppGenericInst GenInst_IPAddress_t3_54_0_0_0 = { 1, GenInst_IPAddress_t3_54_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1126_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1126_0_0_0_Types[] = { &KeyValuePair_2_t1_1126_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1126_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1126_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t1_20_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t1_20_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t1_20_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_KeyValuePair_2_t1_1126_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t1_20_0_0_0, &KeyValuePair_2_t1_1126_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_KeyValuePair_2_t1_1126_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_KeyValuePair_2_t1_1126_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t1_20_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1140_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1140_0_0_0_Types[] = { &KeyValuePair_2_t1_1140_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1140_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1140_0_0_0_Types };
extern const Il2CppType X509Certificate_t1_545_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t1_545_0_0_0_Types[] = { &X509Certificate_t1_545_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t1_545_0_0_0 = { 1, GenInst_X509Certificate_t1_545_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t1_869_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t1_869_0_0_0_Types[] = { &IDeserializationCallback_t1_869_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1_869_0_0_0 = { 1, GenInst_IDeserializationCallback_t1_869_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t3_87_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t3_87_0_0_0_Types[] = { &X509ChainStatus_t3_87_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t3_87_0_0_0 = { 1, GenInst_X509ChainStatus_t3_87_0_0_0_Types };
extern const Il2CppType Capture_t3_107_0_0_0;
static const Il2CppType* GenInst_Capture_t3_107_0_0_0_Types[] = { &Capture_t3_107_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t3_107_0_0_0 = { 1, GenInst_Capture_t3_107_0_0_0_Types };
extern const Il2CppType Group_t3_110_0_0_0;
static const Il2CppType* GenInst_Group_t3_110_0_0_0_Types[] = { &Group_t3_110_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t3_110_0_0_0 = { 1, GenInst_Group_t3_110_0_0_0_Types };
extern const Il2CppType Mark_t3_134_0_0_0;
static const Il2CppType* GenInst_Mark_t3_134_0_0_0_Types[] = { &Mark_t3_134_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t3_134_0_0_0 = { 1, GenInst_Mark_t3_134_0_0_0_Types };
extern const Il2CppType UriScheme_t3_169_0_0_0;
static const Il2CppType* GenInst_UriScheme_t3_169_0_0_0_Types[] = { &UriScheme_t3_169_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t3_169_0_0_0 = { 1, GenInst_UriScheme_t3_169_0_0_0_Types };
extern const Il2CppType BigInteger_t4_18_0_0_0;
static const Il2CppType* GenInst_BigInteger_t4_18_0_0_0_Types[] = { &BigInteger_t4_18_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t4_18_0_0_0 = { 1, GenInst_BigInteger_t4_18_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t4_98_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t4_98_0_0_0_Types[] = { &ClientCertificateType_t4_98_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t4_98_0_0_0 = { 1, GenInst_ClientCertificateType_t4_98_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1159_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1159_0_0_0_Types[] = { &KeyValuePair_2_t1_1159_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1159_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1159_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Int32_t1_3_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Object_t_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Int32_t1_3_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Int32_t1_3_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_1159_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1_1159_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_1159_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_1159_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_NCommand_t5_13_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &NCommand_t5_13_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_NCommand_t5_13_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_NCommand_t5_13_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1170_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1170_0_0_0_Types[] = { &KeyValuePair_2_t1_1170_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1170_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1170_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_902_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_902_0_0_0_Types[] = { &KeyValuePair_2_t1_902_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_902_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_902_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_Byte_t1_11_0_0_0_Types[] = { &Byte_t1_11_0_0_0, &Object_t_0_0_0, &Byte_t1_11_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_Byte_t1_11_0_0_0 = { 3, GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_Byte_t1_11_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Byte_t1_11_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Byte_t1_11_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_902_0_0_0_Types[] = { &Byte_t1_11_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1_902_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_902_0_0_0 = { 3, GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_902_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t1_11_0_0_0_EnetChannel_t5_5_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Byte_t1_11_0_0_0, &EnetChannel_t5_5_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1_11_0_0_0_EnetChannel_t5_5_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Byte_t1_11_0_0_0_EnetChannel_t5_5_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1188_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1188_0_0_0_Types[] = { &KeyValuePair_2_t1_1188_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1188_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1188_0_0_0_Types };
static const Il2CppType* GenInst_EnetChannel_t5_5_0_0_0_Types[] = { &EnetChannel_t5_5_0_0_0 };
extern const Il2CppGenericInst GenInst_EnetChannel_t5_5_0_0_0 = { 1, GenInst_EnetChannel_t5_5_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_CustomType_t5_50_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Type_t_0_0_0, &CustomType_t5_50_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_CustomType_t5_50_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Type_t_0_0_0_CustomType_t5_50_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1194_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1194_0_0_0_Types[] = { &KeyValuePair_2_t1_1194_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1194_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1194_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t1_11_0_0_0_CustomType_t5_50_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Byte_t1_11_0_0_0, &CustomType_t5_50_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1_11_0_0_0_CustomType_t5_50_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Byte_t1_11_0_0_0_CustomType_t5_50_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1199_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1199_0_0_0_Types[] = { &KeyValuePair_2_t1_1199_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1199_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1199_0_0_0_Types };
extern const Il2CppType Object_t6_5_0_0_0;
static const Il2CppType* GenInst_Object_t6_5_0_0_0_Types[] = { &Object_t6_5_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t6_5_0_0_0 = { 1, GenInst_Object_t6_5_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t6_297_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t6_297_0_0_0_Types[] = { &IAchievementDescription_t6_297_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t6_297_0_0_0 = { 1, GenInst_IAchievementDescription_t6_297_0_0_0_Types };
extern const Il2CppType IAchievement_t6_257_0_0_0;
static const Il2CppType* GenInst_IAchievement_t6_257_0_0_0_Types[] = { &IAchievement_t6_257_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t6_257_0_0_0 = { 1, GenInst_IAchievement_t6_257_0_0_0_Types };
extern const Il2CppType IScore_t6_220_0_0_0;
static const Il2CppType* GenInst_IScore_t6_220_0_0_0_Types[] = { &IScore_t6_220_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t6_220_0_0_0 = { 1, GenInst_IScore_t6_220_0_0_0_Types };
extern const Il2CppType IUserProfile_t6_296_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t6_296_0_0_0_Types[] = { &IUserProfile_t6_296_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t6_296_0_0_0 = { 1, GenInst_IUserProfile_t6_296_0_0_0_Types };
extern const Il2CppType AchievementDescription_t6_216_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t6_216_0_0_0_Types[] = { &AchievementDescription_t6_216_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t6_216_0_0_0 = { 1, GenInst_AchievementDescription_t6_216_0_0_0_Types };
extern const Il2CppType UserProfile_t6_213_0_0_0;
static const Il2CppType* GenInst_UserProfile_t6_213_0_0_0_Types[] = { &UserProfile_t6_213_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t6_213_0_0_0 = { 1, GenInst_UserProfile_t6_213_0_0_0_Types };
extern const Il2CppType GcAchievementData_t6_205_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t6_205_0_0_0_Types[] = { &GcAchievementData_t6_205_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t6_205_0_0_0 = { 1, GenInst_GcAchievementData_t6_205_0_0_0_Types };
extern const Il2CppType Achievement_t6_215_0_0_0;
static const Il2CppType* GenInst_Achievement_t6_215_0_0_0_Types[] = { &Achievement_t6_215_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t6_215_0_0_0 = { 1, GenInst_Achievement_t6_215_0_0_0_Types };
extern const Il2CppType GcScoreData_t6_206_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t6_206_0_0_0_Types[] = { &GcScoreData_t6_206_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t6_206_0_0_0 = { 1, GenInst_GcScoreData_t6_206_0_0_0_Types };
extern const Il2CppType Score_t6_217_0_0_0;
static const Il2CppType* GenInst_Score_t6_217_0_0_0_Types[] = { &Score_t6_217_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t6_217_0_0_0 = { 1, GenInst_Score_t6_217_0_0_0_Types };
extern const Il2CppType Behaviour_t6_30_0_0_0;
static const Il2CppType* GenInst_Behaviour_t6_30_0_0_0_Types[] = { &Behaviour_t6_30_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t6_30_0_0_0 = { 1, GenInst_Behaviour_t6_30_0_0_0_Types };
static const Il2CppType* GenInst_Component_t6_26_0_0_0_Types[] = { &Component_t6_26_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t6_26_0_0_0 = { 1, GenInst_Component_t6_26_0_0_0_Types };
extern const Il2CppType Display_t6_78_0_0_0;
static const Il2CppType* GenInst_Display_t6_78_0_0_0_Types[] = { &Display_t6_78_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t6_78_0_0_0 = { 1, GenInst_Display_t6_78_0_0_0_Types };
extern const Il2CppType ContactPoint_t6_104_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t6_104_0_0_0_Types[] = { &ContactPoint_t6_104_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t6_104_0_0_0 = { 1, GenInst_ContactPoint_t6_104_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t6_114_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t6_114_0_0_0_Types[] = { &ContactPoint2D_t6_114_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t6_114_0_0_0 = { 1, GenInst_ContactPoint2D_t6_114_0_0_0_Types };
extern const Il2CppType Keyframe_t6_131_0_0_0;
static const Il2CppType* GenInst_Keyframe_t6_131_0_0_0_Types[] = { &Keyframe_t6_131_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t6_131_0_0_0 = { 1, GenInst_Keyframe_t6_131_0_0_0_Types };
extern const Il2CppType CharacterInfo_t6_146_0_0_0;
static const Il2CppType* GenInst_CharacterInfo_t6_146_0_0_0_Types[] = { &CharacterInfo_t6_146_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterInfo_t6_146_0_0_0 = { 1, GenInst_CharacterInfo_t6_146_0_0_0_Types };
extern const Il2CppType GUIContent_t6_163_0_0_0;
static const Il2CppType* GenInst_GUIContent_t6_163_0_0_0_Types[] = { &GUIContent_t6_163_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIContent_t6_163_0_0_0 = { 1, GenInst_GUIContent_t6_163_0_0_0_Types };
extern const Il2CppType Rect_t6_52_0_0_0;
static const Il2CppType* GenInst_Rect_t6_52_0_0_0_Types[] = { &Rect_t6_52_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t6_52_0_0_0 = { 1, GenInst_Rect_t6_52_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t6_176_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t6_176_0_0_0_Types[] = { &GUILayoutOption_t6_176_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t6_176_0_0_0 = { 1, GenInst_GUILayoutOption_t6_176_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_168_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &LayoutCache_t6_168_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_168_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_168_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1246_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1246_0_0_0_Types[] = { &KeyValuePair_2_t1_1246_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1246_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1246_0_0_0_Types };
static const Il2CppType* GenInst_GUIStyle_t6_166_0_0_0_Types[] = { &GUIStyle_t6_166_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t6_166_0_0_0 = { 1, GenInst_GUIStyle_t6_166_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t6_166_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t6_166_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t6_166_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t6_166_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1253_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1253_0_0_0_Types[] = { &KeyValuePair_2_t1_1253_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1253_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1253_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t6_195_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t6_195_0_0_0_Types[] = { &DisallowMultipleComponent_t6_195_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t6_195_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t6_195_0_0_0_Types };
extern const Il2CppType Attribute_t1_2_0_0_0;
static const Il2CppType* GenInst_Attribute_t1_2_0_0_0_Types[] = { &Attribute_t1_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t1_2_0_0_0 = { 1, GenInst_Attribute_t1_2_0_0_0_Types };
extern const Il2CppType _Attribute_t1_1606_0_0_0;
static const Il2CppType* GenInst__Attribute_t1_1606_0_0_0_Types[] = { &_Attribute_t1_1606_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t1_1606_0_0_0 = { 1, GenInst__Attribute_t1_1606_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t6_198_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t6_198_0_0_0_Types[] = { &ExecuteInEditMode_t6_198_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t6_198_0_0_0 = { 1, GenInst_ExecuteInEditMode_t6_198_0_0_0_Types };
extern const Il2CppType RequireComponent_t6_196_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t6_196_0_0_0_Types[] = { &RequireComponent_t6_196_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t6_196_0_0_0 = { 1, GenInst_RequireComponent_t6_196_0_0_0_Types };
extern const Il2CppType HitInfo_t6_221_0_0_0;
static const Il2CppType* GenInst_HitInfo_t6_221_0_0_0_Types[] = { &HitInfo_t6_221_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t6_221_0_0_0 = { 1, GenInst_HitInfo_t6_221_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t6_237_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0 = { 2, GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1266_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1266_0_0_0_Types[] = { &KeyValuePair_2_t1_1266_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1266_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1266_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t6_237_0_0_0_Types[] = { &TextEditOp_t6_237_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t6_237_0_0_0 = { 1, GenInst_TextEditOp_t6_237_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t6_237_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_TextEditOp_t6_237_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t6_237_0_0_0, &TextEditOp_t6_237_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_TextEditOp_t6_237_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_TextEditOp_t6_237_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t6_237_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_KeyValuePair_2_t1_1266_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t6_237_0_0_0, &KeyValuePair_2_t1_1266_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_KeyValuePair_2_t1_1266_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_KeyValuePair_2_t1_1266_0_0_0_Types };
static const Il2CppType* GenInst_Event_t6_154_0_0_0_Types[] = { &Event_t6_154_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t6_154_0_0_0 = { 1, GenInst_Event_t6_154_0_0_0_Types };
static const Il2CppType* GenInst_Event_t6_154_0_0_0_TextEditOp_t6_237_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Event_t6_154_0_0_0, &TextEditOp_t6_237_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t6_154_0_0_0_TextEditOp_t6_237_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Event_t6_154_0_0_0_TextEditOp_t6_237_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1280_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1280_0_0_0_Types[] = { &KeyValuePair_2_t1_1280_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1280_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1280_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ChatChannel_t7_1_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &String_t_0_0_0, &ChatChannel_t7_1_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ChatChannel_t7_1_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_String_t_0_0_0_ChatChannel_t7_1_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1289_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1289_0_0_0_Types[] = { &KeyValuePair_2_t1_1289_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1289_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1289_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1294_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1294_0_0_0_Types[] = { &KeyValuePair_2_t1_1294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1294_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1294_0_0_0_Types };
static const Il2CppType* GenInst_ConnectionProtocol_t5_36_0_0_0_Types[] = { &ConnectionProtocol_t5_36_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionProtocol_t5_36_0_0_0 = { 1, GenInst_ConnectionProtocol_t5_36_0_0_0_Types };
static const Il2CppType* GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_ConnectionProtocol_t5_36_0_0_0_Types[] = { &ConnectionProtocol_t5_36_0_0_0, &Int32_t1_3_0_0_0, &ConnectionProtocol_t5_36_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_ConnectionProtocol_t5_36_0_0_0 = { 3, GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_ConnectionProtocol_t5_36_0_0_0_Types };
static const Il2CppType* GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0_Types[] = { &ConnectionProtocol_t5_36_0_0_0, &Int32_t1_3_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0 = { 3, GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0_Types };
static const Il2CppType* GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &ConnectionProtocol_t5_36_0_0_0, &Int32_t1_3_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
static const Il2CppType* GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_KeyValuePair_2_t1_1294_0_0_0_Types[] = { &ConnectionProtocol_t5_36_0_0_0, &Int32_t1_3_0_0_0, &KeyValuePair_2_t1_1294_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_KeyValuePair_2_t1_1294_0_0_0 = { 3, GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_KeyValuePair_2_t1_1294_0_0_0_Types };
static const Il2CppType* GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_Types[] = { &Team_t8_169_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Team_t8_169_0_0_0_Object_t_0_0_0 = { 2, GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Team_t8_169_0_0_0_Types[] = { &Team_t8_169_0_0_0 };
extern const Il2CppGenericInst GenInst_Team_t8_169_0_0_0 = { 1, GenInst_Team_t8_169_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1317_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1317_0_0_0_Types[] = { &KeyValuePair_2_t1_1317_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1317_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1317_0_0_0_Types };
static const Il2CppType* GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_Team_t8_169_0_0_0_Types[] = { &Team_t8_169_0_0_0, &Object_t_0_0_0, &Team_t8_169_0_0_0 };
extern const Il2CppGenericInst GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_Team_t8_169_0_0_0 = { 3, GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_Team_t8_169_0_0_0_Types };
static const Il2CppType* GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Team_t8_169_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Team_t8_169_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
static const Il2CppType* GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_1317_0_0_0_Types[] = { &Team_t8_169_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1_1317_0_0_0 };
extern const Il2CppGenericInst GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_1317_0_0_0 = { 3, GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_1317_0_0_0_Types };
static const Il2CppType* GenInst_Team_t8_169_0_0_0_List_1_t1_955_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Team_t8_169_0_0_0, &List_1_t1_955_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Team_t8_169_0_0_0_List_1_t1_955_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Team_t8_169_0_0_0_List_1_t1_955_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1331_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1331_0_0_0_Types[] = { &KeyValuePair_2_t1_1331_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1331_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1331_0_0_0_Types };
extern const Il2CppType State_t8_41_0_0_0;
static const Il2CppType* GenInst_State_t8_41_0_0_0_Types[] = { &State_t8_41_0_0_0 };
extern const Il2CppGenericInst GenInst_State_t8_41_0_0_0 = { 1, GenInst_State_t8_41_0_0_0_Types };
extern const Il2CppType Transform_t6_61_0_0_0;
static const Il2CppType* GenInst_Transform_t6_61_0_0_0_Types[] = { &Transform_t6_61_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t6_61_0_0_0 = { 1, GenInst_Transform_t6_61_0_0_0_Types };
static const Il2CppType* GenInst_RoomInfo_t8_124_0_0_0_Types[] = { &RoomInfo_t8_124_0_0_0 };
extern const Il2CppGenericInst GenInst_RoomInfo_t8_124_0_0_0 = { 1, GenInst_RoomInfo_t8_124_0_0_0_Types };
extern const Il2CppType Color_t6_40_0_0_0;
static const Il2CppType* GenInst_Color_t6_40_0_0_0_Types[] = { &Color_t6_40_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t6_40_0_0_0 = { 1, GenInst_Color_t6_40_0_0_0_Types };
extern const Il2CppType Material_t6_67_0_0_0;
static const Il2CppType* GenInst_Material_t6_67_0_0_0_Types[] = { &Material_t6_67_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t6_67_0_0_0 = { 1, GenInst_Material_t6_67_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_PhotonPlayer_t8_102_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &PhotonPlayer_t8_102_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_PhotonPlayer_t8_102_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_PhotonPlayer_t8_102_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1341_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1341_0_0_0_Types[] = { &KeyValuePair_2_t1_1341_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1341_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1341_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t8_6_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t8_6_0_0_0_Types[] = { &MonoBehaviour_t8_6_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t8_6_0_0_0 = { 1, GenInst_MonoBehaviour_t8_6_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_RoomInfo_t8_124_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &String_t_0_0_0, &RoomInfo_t8_124_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RoomInfo_t8_124_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_String_t_0_0_0_RoomInfo_t8_124_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1348_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1348_0_0_0_Types[] = { &KeyValuePair_2_t1_1348_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1348_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1348_0_0_0_Types };
extern const Il2CppType Link_t2_29_0_0_0;
static const Il2CppType* GenInst_Link_t2_29_0_0_0_Types[] = { &Link_t2_29_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2_29_0_0_0 = { 1, GenInst_Link_t2_29_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_PhotonView_t8_3_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &PhotonView_t8_3_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_PhotonView_t8_3_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_PhotonView_t8_3_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_964_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_964_0_0_0_Types[] = { &KeyValuePair_2_t1_964_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_964_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_964_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Hashtable_t5_1_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Hashtable_t5_1_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Hashtable_t5_1_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_Hashtable_t5_1_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_967_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_967_0_0_0_Types[] = { &KeyValuePair_2_t1_967_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_967_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_967_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GameObject_t6_85_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &String_t_0_0_0, &GameObject_t6_85_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t6_85_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_String_t_0_0_0_GameObject_t6_85_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1358_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1358_0_0_0_Types[] = { &KeyValuePair_2_t1_1358_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1358_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1358_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_List_1_t1_893_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Type_t_0_0_0, &List_1_t1_893_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_List_1_t1_893_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Type_t_0_0_0_List_1_t1_893_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1364_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1364_0_0_0_Types[] = { &KeyValuePair_2_t1_1364_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1364_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1364_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_ObjectU5BU5D_t1_157_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &ObjectU5BU5D_t1_157_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_ObjectU5BU5D_t1_157_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_ObjectU5BU5D_t1_157_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1370_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1370_0_0_0_Types[] = { &KeyValuePair_2_t1_1370_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1370_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1370_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1382_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1382_0_0_0_Types[] = { &KeyValuePair_2_t1_1382_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1382_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1382_0_0_0_Types };
static const Il2CppType* GenInst_Component_t6_26_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Component_t6_26_0_0_0, &MethodInfo_t_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t6_26_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Component_t6_26_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1390_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1390_0_0_0_Types[] = { &KeyValuePair_2_t1_1390_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1390_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1390_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1_167_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &DictionaryEntry_t1_167_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1_167_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 2, GenInst_DictionaryEntry_t1_167_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1016_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_1016_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1016_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1016_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1016_0_0_0_KeyValuePair_2_t1_1016_0_0_0_Types[] = { &KeyValuePair_2_t1_1016_0_0_0, &KeyValuePair_2_t1_1016_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1016_0_0_0_KeyValuePair_2_t1_1016_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1016_0_0_0_KeyValuePair_2_t1_1016_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1044_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_1044_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1044_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1044_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1044_0_0_0_KeyValuePair_2_t1_1044_0_0_0_Types[] = { &KeyValuePair_2_t1_1044_0_0_0, &KeyValuePair_2_t1_1044_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1044_0_0_0_KeyValuePair_2_t1_1044_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1044_0_0_0_KeyValuePair_2_t1_1044_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t1_20_0_0_0_Object_t_0_0_0_Types[] = { &Boolean_t1_20_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t1_20_0_0_0_Object_t_0_0_0 = { 2, GenInst_Boolean_t1_20_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &Boolean_t1_20_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1126_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_1126_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1126_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1126_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1126_0_0_0_KeyValuePair_2_t1_1126_0_0_0_Types[] = { &KeyValuePair_2_t1_1126_0_0_0, &KeyValuePair_2_t1_1126_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1126_0_0_0_KeyValuePair_2_t1_1126_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1126_0_0_0_KeyValuePair_2_t1_1126_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1159_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_1159_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1159_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1159_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1159_0_0_0_KeyValuePair_2_t1_1159_0_0_0_Types[] = { &KeyValuePair_2_t1_1159_0_0_0, &KeyValuePair_2_t1_1159_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1159_0_0_0_KeyValuePair_2_t1_1159_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1159_0_0_0_KeyValuePair_2_t1_1159_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t1_11_0_0_0_Byte_t1_11_0_0_0_Types[] = { &Byte_t1_11_0_0_0, &Byte_t1_11_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1_11_0_0_0_Byte_t1_11_0_0_0 = { 2, GenInst_Byte_t1_11_0_0_0_Byte_t1_11_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_902_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_902_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_902_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_902_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_902_0_0_0_KeyValuePair_2_t1_902_0_0_0_Types[] = { &KeyValuePair_2_t1_902_0_0_0, &KeyValuePair_2_t1_902_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_902_0_0_0_KeyValuePair_2_t1_902_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_902_0_0_0_KeyValuePair_2_t1_902_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t6_237_0_0_0_Object_t_0_0_0_Types[] = { &TextEditOp_t6_237_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t6_237_0_0_0_Object_t_0_0_0 = { 2, GenInst_TextEditOp_t6_237_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t6_237_0_0_0_TextEditOp_t6_237_0_0_0_Types[] = { &TextEditOp_t6_237_0_0_0, &TextEditOp_t6_237_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t6_237_0_0_0_TextEditOp_t6_237_0_0_0 = { 2, GenInst_TextEditOp_t6_237_0_0_0_TextEditOp_t6_237_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1266_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_1266_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1266_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1266_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1266_0_0_0_KeyValuePair_2_t1_1266_0_0_0_Types[] = { &KeyValuePair_2_t1_1266_0_0_0, &KeyValuePair_2_t1_1266_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1266_0_0_0_KeyValuePair_2_t1_1266_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1266_0_0_0_KeyValuePair_2_t1_1266_0_0_0_Types };
static const Il2CppType* GenInst_ConnectionProtocol_t5_36_0_0_0_Object_t_0_0_0_Types[] = { &ConnectionProtocol_t5_36_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionProtocol_t5_36_0_0_0_Object_t_0_0_0 = { 2, GenInst_ConnectionProtocol_t5_36_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_ConnectionProtocol_t5_36_0_0_0_ConnectionProtocol_t5_36_0_0_0_Types[] = { &ConnectionProtocol_t5_36_0_0_0, &ConnectionProtocol_t5_36_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionProtocol_t5_36_0_0_0_ConnectionProtocol_t5_36_0_0_0 = { 2, GenInst_ConnectionProtocol_t5_36_0_0_0_ConnectionProtocol_t5_36_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1294_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_1294_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1294_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1294_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1294_0_0_0_KeyValuePair_2_t1_1294_0_0_0_Types[] = { &KeyValuePair_2_t1_1294_0_0_0, &KeyValuePair_2_t1_1294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1294_0_0_0_KeyValuePair_2_t1_1294_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1294_0_0_0_KeyValuePair_2_t1_1294_0_0_0_Types };
static const Il2CppType* GenInst_Team_t8_169_0_0_0_Team_t8_169_0_0_0_Types[] = { &Team_t8_169_0_0_0, &Team_t8_169_0_0_0 };
extern const Il2CppGenericInst GenInst_Team_t8_169_0_0_0_Team_t8_169_0_0_0 = { 2, GenInst_Team_t8_169_0_0_0_Team_t8_169_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1317_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_1317_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1317_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1317_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1317_0_0_0_KeyValuePair_2_t1_1317_0_0_0_Types[] = { &KeyValuePair_2_t1_1317_0_0_0, &KeyValuePair_2_t1_1317_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1317_0_0_0_KeyValuePair_2_t1_1317_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1317_0_0_0_KeyValuePair_2_t1_1317_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1_1610_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1_1610_gp_0_0_0_0_Types[] = { &IEnumerable_1_t1_1610_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1_1610_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t1_1610_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m1_12012_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m1_12012_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m1_12012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m1_12012_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m1_12012_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_12024_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_12024_gp_0_0_0_0_Array_Sort_m1_12024_gp_0_0_0_0_Types[] = { &Array_Sort_m1_12024_gp_0_0_0_0, &Array_Sort_m1_12024_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12024_gp_0_0_0_0_Array_Sort_m1_12024_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1_12024_gp_0_0_0_0_Array_Sort_m1_12024_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_12025_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1_12025_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_12025_gp_0_0_0_0_Array_Sort_m1_12025_gp_1_0_0_0_Types[] = { &Array_Sort_m1_12025_gp_0_0_0_0, &Array_Sort_m1_12025_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12025_gp_0_0_0_0_Array_Sort_m1_12025_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1_12025_gp_0_0_0_0_Array_Sort_m1_12025_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_12026_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_12026_gp_0_0_0_0_Types[] = { &Array_Sort_m1_12026_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12026_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1_12026_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1_12026_gp_0_0_0_0_Array_Sort_m1_12026_gp_0_0_0_0_Types[] = { &Array_Sort_m1_12026_gp_0_0_0_0, &Array_Sort_m1_12026_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12026_gp_0_0_0_0_Array_Sort_m1_12026_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1_12026_gp_0_0_0_0_Array_Sort_m1_12026_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_12027_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_12027_gp_0_0_0_0_Types[] = { &Array_Sort_m1_12027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12027_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1_12027_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_12027_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_12027_gp_0_0_0_0_Array_Sort_m1_12027_gp_1_0_0_0_Types[] = { &Array_Sort_m1_12027_gp_0_0_0_0, &Array_Sort_m1_12027_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12027_gp_0_0_0_0_Array_Sort_m1_12027_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1_12027_gp_0_0_0_0_Array_Sort_m1_12027_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_12028_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_12028_gp_0_0_0_0_Array_Sort_m1_12028_gp_0_0_0_0_Types[] = { &Array_Sort_m1_12028_gp_0_0_0_0, &Array_Sort_m1_12028_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12028_gp_0_0_0_0_Array_Sort_m1_12028_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1_12028_gp_0_0_0_0_Array_Sort_m1_12028_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_12029_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1_12029_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_12029_gp_0_0_0_0_Array_Sort_m1_12029_gp_1_0_0_0_Types[] = { &Array_Sort_m1_12029_gp_0_0_0_0, &Array_Sort_m1_12029_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12029_gp_0_0_0_0_Array_Sort_m1_12029_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1_12029_gp_0_0_0_0_Array_Sort_m1_12029_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_12030_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_12030_gp_0_0_0_0_Types[] = { &Array_Sort_m1_12030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12030_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1_12030_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1_12030_gp_0_0_0_0_Array_Sort_m1_12030_gp_0_0_0_0_Types[] = { &Array_Sort_m1_12030_gp_0_0_0_0, &Array_Sort_m1_12030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12030_gp_0_0_0_0_Array_Sort_m1_12030_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1_12030_gp_0_0_0_0_Array_Sort_m1_12030_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_12031_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_12031_gp_0_0_0_0_Types[] = { &Array_Sort_m1_12031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12031_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1_12031_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_12031_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_12031_gp_1_0_0_0_Types[] = { &Array_Sort_m1_12031_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12031_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m1_12031_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1_12031_gp_0_0_0_0_Array_Sort_m1_12031_gp_1_0_0_0_Types[] = { &Array_Sort_m1_12031_gp_0_0_0_0, &Array_Sort_m1_12031_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12031_gp_0_0_0_0_Array_Sort_m1_12031_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1_12031_gp_0_0_0_0_Array_Sort_m1_12031_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_12032_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_12032_gp_0_0_0_0_Types[] = { &Array_Sort_m1_12032_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12032_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1_12032_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_12033_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_12033_gp_0_0_0_0_Types[] = { &Array_Sort_m1_12033_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_12033_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1_12033_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m1_12034_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m1_12034_gp_0_0_0_0_Types[] = { &Array_qsort_m1_12034_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m1_12034_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m1_12034_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m1_12034_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m1_12034_gp_0_0_0_0_Array_qsort_m1_12034_gp_1_0_0_0_Types[] = { &Array_qsort_m1_12034_gp_0_0_0_0, &Array_qsort_m1_12034_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m1_12034_gp_0_0_0_0_Array_qsort_m1_12034_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m1_12034_gp_0_0_0_0_Array_qsort_m1_12034_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m1_12035_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m1_12035_gp_0_0_0_0_Types[] = { &Array_compare_m1_12035_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m1_12035_gp_0_0_0_0 = { 1, GenInst_Array_compare_m1_12035_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m1_12036_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m1_12036_gp_0_0_0_0_Types[] = { &Array_qsort_m1_12036_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m1_12036_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m1_12036_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m1_12039_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m1_12039_gp_0_0_0_0_Types[] = { &Array_Resize_m1_12039_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m1_12039_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m1_12039_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m1_12041_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m1_12041_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m1_12041_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m1_12041_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m1_12041_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m1_12042_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m1_12042_gp_0_0_0_0_Types[] = { &Array_ForEach_m1_12042_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m1_12042_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m1_12042_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1_12043_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1_12043_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1_12043_gp_0_0_0_0_Array_ConvertAll_m1_12043_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1_12043_gp_0_0_0_0, &Array_ConvertAll_m1_12043_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1_12043_gp_0_0_0_0_Array_ConvertAll_m1_12043_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1_12043_gp_0_0_0_0_Array_ConvertAll_m1_12043_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m1_12044_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m1_12044_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m1_12044_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1_12044_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1_12044_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m1_12045_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m1_12045_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m1_12045_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1_12045_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1_12045_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m1_12046_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m1_12046_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m1_12046_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1_12046_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1_12046_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1_12047_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1_12047_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1_12047_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1_12047_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1_12047_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1_12048_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1_12048_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1_12048_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1_12048_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1_12048_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1_12049_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1_12049_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1_12049_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1_12049_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1_12049_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1_12050_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1_12050_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1_12050_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1_12050_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1_12050_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1_12051_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1_12051_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1_12051_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1_12051_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1_12051_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1_12052_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1_12052_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1_12052_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1_12052_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1_12052_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1_12053_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1_12053_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1_12053_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1_12053_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1_12053_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1_12054_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1_12054_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1_12054_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1_12054_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1_12054_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1_12055_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1_12055_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1_12055_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1_12055_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1_12055_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1_12056_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1_12056_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1_12056_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1_12056_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1_12056_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m1_12057_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m1_12057_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m1_12057_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1_12057_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1_12057_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m1_12058_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m1_12058_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m1_12058_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1_12058_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1_12058_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m1_12059_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m1_12059_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m1_12059_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1_12059_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1_12059_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m1_12060_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m1_12060_gp_0_0_0_0_Types[] = { &Array_FindAll_m1_12060_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m1_12060_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m1_12060_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m1_12061_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m1_12061_gp_0_0_0_0_Types[] = { &Array_Exists_m1_12061_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m1_12061_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1_12061_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m1_12062_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m1_12062_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m1_12062_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m1_12062_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m1_12062_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m1_12063_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m1_12063_gp_0_0_0_0_Types[] = { &Array_Find_m1_12063_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m1_12063_gp_0_0_0_0 = { 1, GenInst_Array_Find_m1_12063_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m1_12064_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m1_12064_gp_0_0_0_0_Types[] = { &Array_FindLast_m1_12064_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m1_12064_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m1_12064_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t1_1611_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t1_1611_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t1_1611_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t1_1611_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t1_1611_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t1_1612_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t1_1612_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t1_1612_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t1_1612_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t1_1612_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1_1613_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1_1613_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1_1613_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1_1613_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1_1613_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t1_1614_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t1_1614_gp_0_0_0_0_Types[] = { &IList_1_t1_1614_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1_1614_gp_0_0_0_0 = { 1, GenInst_IList_1_t1_1614_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1_1615_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1_1615_gp_0_0_0_0_Types[] = { &ICollection_1_t1_1615_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1_1615_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1_1615_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1_837_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1_837_gp_0_0_0_0_Types[] = { &Nullable_1_t1_837_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1_837_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1_837_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t1_1622_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1_1622_gp_0_0_0_0_Types[] = { &Comparer_1_t1_1622_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1_1622_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1_1622_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1_1623_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1_1623_gp_0_0_0_0_Types[] = { &DefaultComparer_t1_1623_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1_1623_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1_1623_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t1_1605_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t1_1605_gp_0_0_0_0_Types[] = { &GenericComparer_1_t1_1605_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1_1605_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1_1605_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t1_901_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Types[] = { &Dictionary_2_t1_901_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_901_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t1_901_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_Types[] = { &Dictionary_2_t1_901_gp_0_0_0_0, &Dictionary_2_t1_901_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2601_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2601_0_0_0_Types[] = { &KeyValuePair_2_t1_2601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2601_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2601_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m1_12210_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1_12210_gp_0_0_0_0_Types[] = { &Dictionary_2_t1_901_gp_0_0_0_0, &Dictionary_2_t1_901_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m1_12210_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1_12210_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1_12210_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m1_12215_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1_12215_gp_0_0_0_0_Types[] = { &Dictionary_2_t1_901_gp_0_0_0_0, &Dictionary_2_t1_901_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m1_12215_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1_12215_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1_12215_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m1_12215_gp_0_0_0_0_Object_t_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m1_12215_gp_0_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m1_12215_gp_0_0_0_0_Object_t_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m1_12215_gp_0_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_DictionaryEntry_t1_167_0_0_0_Types[] = { &Dictionary_2_t1_901_gp_0_0_0_0, &Dictionary_2_t1_901_gp_1_0_0_0, &DictionaryEntry_t1_167_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_DictionaryEntry_t1_167_0_0_0 = { 3, GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_DictionaryEntry_t1_167_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t1_1624_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t1_1624_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t1_1624_gp_0_0_0_0_ShimEnumerator_t1_1624_gp_1_0_0_0_Types[] = { &ShimEnumerator_t1_1624_gp_0_0_0_0, &ShimEnumerator_t1_1624_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t1_1624_gp_0_0_0_0_ShimEnumerator_t1_1624_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t1_1624_gp_0_0_0_0_ShimEnumerator_t1_1624_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1_1625_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1_1625_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1_1625_gp_0_0_0_0_Enumerator_t1_1625_gp_1_0_0_0_Types[] = { &Enumerator_t1_1625_gp_0_0_0_0, &Enumerator_t1_1625_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1_1625_gp_0_0_0_0_Enumerator_t1_1625_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1_1625_gp_0_0_0_0_Enumerator_t1_1625_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2615_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2615_0_0_0_Types[] = { &KeyValuePair_2_t1_2615_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2615_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2615_0_0_0_Types };
extern const Il2CppType KeyCollection_t1_1626_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1_1626_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t1_1626_gp_0_0_0_0_KeyCollection_t1_1626_gp_1_0_0_0_Types[] = { &KeyCollection_t1_1626_gp_0_0_0_0, &KeyCollection_t1_1626_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1_1626_gp_0_0_0_0_KeyCollection_t1_1626_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1_1626_gp_0_0_0_0_KeyCollection_t1_1626_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1_1626_gp_0_0_0_0_Types[] = { &KeyCollection_t1_1626_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1_1626_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1_1626_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1_1627_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1_1627_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1_1627_gp_0_0_0_0_Enumerator_t1_1627_gp_1_0_0_0_Types[] = { &Enumerator_t1_1627_gp_0_0_0_0, &Enumerator_t1_1627_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1_1627_gp_0_0_0_0_Enumerator_t1_1627_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1_1627_gp_0_0_0_0_Enumerator_t1_1627_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t1_1627_gp_0_0_0_0_Types[] = { &Enumerator_t1_1627_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1_1627_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1_1627_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1_1626_gp_0_0_0_0_KeyCollection_t1_1626_gp_1_0_0_0_KeyCollection_t1_1626_gp_0_0_0_0_Types[] = { &KeyCollection_t1_1626_gp_0_0_0_0, &KeyCollection_t1_1626_gp_1_0_0_0, &KeyCollection_t1_1626_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1_1626_gp_0_0_0_0_KeyCollection_t1_1626_gp_1_0_0_0_KeyCollection_t1_1626_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t1_1626_gp_0_0_0_0_KeyCollection_t1_1626_gp_1_0_0_0_KeyCollection_t1_1626_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1_1626_gp_0_0_0_0_KeyCollection_t1_1626_gp_0_0_0_0_Types[] = { &KeyCollection_t1_1626_gp_0_0_0_0, &KeyCollection_t1_1626_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1_1626_gp_0_0_0_0_KeyCollection_t1_1626_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t1_1626_gp_0_0_0_0_KeyCollection_t1_1626_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t1_1628_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t1_1628_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t1_1628_gp_0_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0_Types[] = { &ValueCollection_t1_1628_gp_0_0_0_0, &ValueCollection_t1_1628_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t1_1628_gp_0_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t1_1628_gp_0_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t1_1628_gp_1_0_0_0_Types[] = { &ValueCollection_t1_1628_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t1_1628_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t1_1628_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1_1629_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1_1629_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1_1629_gp_0_0_0_0_Enumerator_t1_1629_gp_1_0_0_0_Types[] = { &Enumerator_t1_1629_gp_0_0_0_0, &Enumerator_t1_1629_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1_1629_gp_0_0_0_0_Enumerator_t1_1629_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1_1629_gp_0_0_0_0_Enumerator_t1_1629_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t1_1629_gp_1_0_0_0_Types[] = { &Enumerator_t1_1629_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1_1629_gp_1_0_0_0 = { 1, GenInst_Enumerator_t1_1629_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t1_1628_gp_0_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0_Types[] = { &ValueCollection_t1_1628_gp_0_0_0_0, &ValueCollection_t1_1628_gp_1_0_0_0, &ValueCollection_t1_1628_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t1_1628_gp_0_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t1_1628_gp_0_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t1_1628_gp_1_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0_Types[] = { &ValueCollection_t1_1628_gp_1_0_0_0, &ValueCollection_t1_1628_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t1_1628_gp_1_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t1_1628_gp_1_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_KeyValuePair_2_t1_2601_0_0_0_Types[] = { &Dictionary_2_t1_901_gp_0_0_0_0, &Dictionary_2_t1_901_gp_1_0_0_0, &KeyValuePair_2_t1_2601_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_KeyValuePair_2_t1_2601_0_0_0 = { 3, GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_KeyValuePair_2_t1_2601_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2601_0_0_0_KeyValuePair_2_t1_2601_0_0_0_Types[] = { &KeyValuePair_2_t1_2601_0_0_0, &KeyValuePair_2_t1_2601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2601_0_0_0_KeyValuePair_2_t1_2601_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2601_0_0_0_KeyValuePair_2_t1_2601_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t1_901_gp_1_0_0_0_Types[] = { &Dictionary_2_t1_901_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_901_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t1_901_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t1_1631_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t1_1631_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t1_1631_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t1_1631_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t1_1631_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1_1632_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1_1632_gp_0_0_0_0_Types[] = { &DefaultComparer_t1_1632_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1_1632_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1_1632_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t1_1604_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t1_1604_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t1_1604_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t1_1604_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t1_1604_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2651_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2651_0_0_0_Types[] = { &KeyValuePair_2_t1_2651_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2651_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2651_0_0_0_Types };
extern const Il2CppType IDictionary_2_t1_1634_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t1_1634_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t1_1634_gp_0_0_0_0_IDictionary_2_t1_1634_gp_1_0_0_0_Types[] = { &IDictionary_2_t1_1634_gp_0_0_0_0, &IDictionary_2_t1_1634_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t1_1634_gp_0_0_0_0_IDictionary_2_t1_1634_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t1_1634_gp_0_0_0_0_IDictionary_2_t1_1634_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1636_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1_1636_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1636_gp_0_0_0_0_KeyValuePair_2_t1_1636_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t1_1636_gp_0_0_0_0, &KeyValuePair_2_t1_1636_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1636_gp_0_0_0_0_KeyValuePair_2_t1_1636_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1636_gp_0_0_0_0_KeyValuePair_2_t1_1636_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t1_1637_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1_1637_gp_0_0_0_0_Types[] = { &List_1_t1_1637_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1637_gp_0_0_0_0 = { 1, GenInst_List_1_t1_1637_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1_1638_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1_1638_gp_0_0_0_0_Types[] = { &Enumerator_t1_1638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1_1638_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1_1638_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t1_1639_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t1_1639_gp_0_0_0_0_Types[] = { &Collection_1_t1_1639_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t1_1639_gp_0_0_0_0 = { 1, GenInst_Collection_1_t1_1639_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t1_1640_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t1_1640_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t1_1640_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t1_1640_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t1_1640_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1_12475_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1_12475_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m1_12475_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1_12475_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m1_12475_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m1_12475_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m1_12475_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1_12475_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m1_12475_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1_12475_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1_12476_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1_12476_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1_12476_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1_12476_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1_12476_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t2_34_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t2_34_gp_0_0_0_0_Types[] = { &HashSet_1_t2_34_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2_34_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t2_34_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2_36_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2_36_gp_0_0_0_0_Types[] = { &Enumerator_t2_36_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2_36_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2_36_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t2_37_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t2_37_gp_0_0_0_0_Types[] = { &PrimeHelper_t2_37_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t2_37_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t2_37_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t3_205_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t3_205_gp_0_0_0_0_Types[] = { &LinkedList_1_t3_205_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t3_205_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t3_205_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3_206_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3_206_gp_0_0_0_0_Types[] = { &Enumerator_t3_206_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3_206_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3_206_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3_207_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t3_207_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t3_207_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3_207_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t3_207_gp_0_0_0_0_Types };
extern const Il2CppType Queue_1_t3_208_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t3_208_gp_0_0_0_0_Types[] = { &Queue_1_t3_208_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t3_208_gp_0_0_0_0 = { 1, GenInst_Queue_1_t3_208_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3_209_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3_209_gp_0_0_0_0_Types[] = { &Enumerator_t3_209_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3_209_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3_209_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t3_210_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t3_210_gp_0_0_0_0_Types[] = { &Stack_1_t3_210_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t3_210_gp_0_0_0_0 = { 1, GenInst_Stack_1_t3_210_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3_211_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3_211_gp_0_0_0_0_Types[] = { &Enumerator_t3_211_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3_211_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3_211_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m6_1722_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m6_1722_gp_0_0_0_0_Types[] = { &Component_GetComponents_m6_1722_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m6_1722_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m6_1722_gp_0_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[468] = 
{
	&GenInst_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1_332_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t1_331_0_0_0,
	&GenInst_StrongName_t1_623_0_0_0,
	&GenInst_DateTime_t1_127_0_0_0,
	&GenInst_DateTimeOffset_t1_707_0_0_0,
	&GenInst_TimeSpan_t1_213_0_0_0,
	&GenInst_Guid_t1_730_0_0_0,
	&GenInst_CustomAttributeData_t1_328_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_Int32_t1_3_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_NCommand_t5_13_0_0_0,
	&GenInst_NCommand_t5_13_0_0_0,
	&GenInst_CmdLogItem_t5_31_0_0_0,
	&GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0,
	&GenInst_MyAction_t5_6_0_0_0,
	&GenInst_SimulationItem_t5_28_0_0_0,
	&GenInst_Byte_t1_11_0_0_0_EnetChannel_t5_5_0_0_0,
	&GenInst_DictionaryEntry_t1_167_0_0_0,
	&GenInst_Type_t_0_0_0_CustomType_t5_50_0_0_0,
	&GenInst_Byte_t1_11_0_0_0_CustomType_t5_50_0_0_0,
	&GenInst_Boolean_t1_20_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst_ByteU5BU5D_t1_71_0_0_0,
	&GenInst_GcLeaderboard_t6_22_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t6_266_0_0_0,
	&GenInst_IAchievementU5BU5D_t6_268_0_0_0,
	&GenInst_IScoreU5BU5D_t6_218_0_0_0,
	&GenInst_IUserProfileU5BU5D_t6_214_0_0_0,
	&GenInst_Rigidbody2D_t6_113_0_0_0,
	&GenInst_Font_t6_148_0_0_0,
	&GenInst_UIVertex_t6_153_0_0_0,
	&GenInst_UICharInfo_t6_149_0_0_0,
	&GenInst_UILineInfo_t6_150_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_168_0_0_0,
	&GenInst_GUILayoutEntry_t6_171_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t6_166_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0,
	&GenInst_GUILayer_t6_31_0_0_0,
	&GenInst_Event_t6_154_0_0_0_TextEditOp_t6_237_0_0_0,
	&GenInst_PersistentCall_t6_242_0_0_0,
	&GenInst_BaseInvokableCall_t6_240_0_0_0,
	&GenInst_String_t_0_0_0_ChatChannel_t7_1_0_0_0,
	&GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_Animator_t6_138_0_0_0,
	&GenInst_PhotonView_t8_3_0_0_0,
	&GenInst_Rigidbody_t6_102_0_0_0,
	&GenInst_InputToEvent_t8_152_0_0_0,
	&GenInst_Renderer_t6_25_0_0_0,
	&GenInst_ChatGui_t8_14_0_0_0,
	&GenInst_FriendInfo_t8_74_0_0_0,
	&GenInst_Camera_t6_75_0_0_0,
	&GenInst_PhotonAnimatorView_t8_27_0_0_0,
	&GenInst_PickupController_t8_32_0_0_0,
	&GenInst_Collider_t6_100_0_0_0,
	&GenInst_Animation_t6_135_0_0_0,
	&GenInst_CharacterController_t6_99_0_0_0,
	&GenInst_PickupItem_t8_163_0_0_0,
	&GenInst_Team_t8_169_0_0_0_List_1_t1_955_0_0_0,
	&GenInst_PhotonPlayer_t8_102_0_0_0,
	&GenInst_PhotonTransformView_t8_39_0_0_0,
	&GenInst_ThirdPersonController_t8_47_0_0_0,
	&GenInst_ThirdPersonCamera_t8_46_0_0_0,
	&GenInst_InRoomChat_t8_150_0_0_0,
	&GenInst_AudioSource_t6_122_0_0_0,
	&GenInst_myThirdPersonController_t8_55_0_0_0,
	&GenInst_TypedLobbyInfo_t8_95_0_0_0,
	&GenInst_String_t_0_0_0_RoomInfo_t8_124_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_PhotonPlayer_t8_102_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_PhotonView_t8_3_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Hashtable_t5_1_0_0_0,
	&GenInst_Type_t_0_0_0_List_1_t1_893_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_ObjectU5BU5D_t1_157_0_0_0,
	&GenInst_String_t_0_0_0_GameObject_t6_85_0_0_0,
	&GenInst_GameObject_t6_85_0_0_0,
	&GenInst_Region_t8_110_0_0_0,
	&GenInst_Byte_t1_11_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0,
	&GenInst_PhotonHandler_t8_111_0_0_0,
	&GenInst_Component_t6_26_0_0_0_MethodInfo_t_0_0_0,
	&GenInst_MonoBehaviour_t6_80_0_0_0,
	&GenInst_SynchronizedParameter_t8_128_0_0_0,
	&GenInst_SynchronizedLayer_t8_129_0_0_0,
	&GenInst_Vector3_t6_49_0_0_0,
	&GenInst_OnClickDestroy_t8_157_0_0_0,
	&GenInst_SpriteRenderer_t6_69_0_0_0,
	&GenInst_Single_t1_17_0_0_0,
	&GenInst_MeshRenderer_t6_28_0_0_0,
	&GenInst_TextMesh_t6_145_0_0_0,
	&GenInst_SupportLogging_t8_178_0_0_0,
	&GenInst_Char_t1_15_0_0_0,
	&GenInst_IConvertible_t1_834_0_0_0,
	&GenInst_IComparable_t1_833_0_0_0,
	&GenInst_IComparable_1_t1_1747_0_0_0,
	&GenInst_IEquatable_1_t1_1752_0_0_0,
	&GenInst_ValueType_t1_1_0_0_0,
	&GenInst_Int64_t1_7_0_0_0,
	&GenInst_UInt32_t1_8_0_0_0,
	&GenInst_UInt64_t1_10_0_0_0,
	&GenInst_SByte_t1_12_0_0_0,
	&GenInst_Int16_t1_13_0_0_0,
	&GenInst_UInt16_t1_14_0_0_0,
	&GenInst_IEnumerable_t1_836_0_0_0,
	&GenInst_ICloneable_t1_846_0_0_0,
	&GenInst_IComparable_1_t1_1776_0_0_0,
	&GenInst_IEquatable_1_t1_1777_0_0_0,
	&GenInst_IReflect_t1_1618_0_0_0,
	&GenInst__Type_t1_1616_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t1_828_0_0_0,
	&GenInst__MemberInfo_t1_1617_0_0_0,
	&GenInst_IFormattable_t1_831_0_0_0,
	&GenInst_IComparable_1_t1_1768_0_0_0,
	&GenInst_IEquatable_1_t1_1769_0_0_0,
	&GenInst_IComparable_1_t1_1837_0_0_0,
	&GenInst_IEquatable_1_t1_1842_0_0_0,
	&GenInst_Double_t1_18_0_0_0,
	&GenInst_Decimal_t1_19_0_0_0,
	&GenInst_IComparable_1_t1_1733_0_0_0,
	&GenInst_IEquatable_1_t1_1734_0_0_0,
	&GenInst_Delegate_t1_22_0_0_0,
	&GenInst_ISerializable_t1_866_0_0_0,
	&GenInst_ParameterInfo_t1_351_0_0_0,
	&GenInst__ParameterInfo_t1_1658_0_0_0,
	&GenInst_ParameterModifier_t1_352_0_0_0,
	&GenInst_IComparable_1_t1_1774_0_0_0,
	&GenInst_IEquatable_1_t1_1775_0_0_0,
	&GenInst_IComparable_1_t1_1764_0_0_0,
	&GenInst_IEquatable_1_t1_1765_0_0_0,
	&GenInst_IComparable_1_t1_1766_0_0_0,
	&GenInst_IEquatable_1_t1_1767_0_0_0,
	&GenInst_IComparable_1_t1_1772_0_0_0,
	&GenInst_IEquatable_1_t1_1773_0_0_0,
	&GenInst_IComparable_1_t1_1770_0_0_0,
	&GenInst_IEquatable_1_t1_1771_0_0_0,
	&GenInst_IComparable_1_t1_1762_0_0_0,
	&GenInst_IEquatable_1_t1_1763_0_0_0,
	&GenInst_IComparable_1_t1_1846_0_0_0,
	&GenInst_IEquatable_1_t1_1847_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t1_1654_0_0_0,
	&GenInst__MethodInfo_t1_1656_0_0_0,
	&GenInst_MethodBase_t1_198_0_0_0,
	&GenInst__MethodBase_t1_1655_0_0_0,
	&GenInst_ConstructorInfo_t1_273_0_0_0,
	&GenInst__ConstructorInfo_t1_1652_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t1_66_0_0_0,
	&GenInst_TailoringInfo_t1_69_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_KeyValuePair_2_t1_1016_0_0_0,
	&GenInst_Link_t1_150_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_KeyValuePair_2_t1_1016_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1032_0_0_0,
	&GenInst_Contraction_t1_70_0_0_0,
	&GenInst_Level2Map_t1_73_0_0_0,
	&GenInst_BigInteger_t1_95_0_0_0,
	&GenInst_KeySizes_t1_575_0_0_0,
	&GenInst_KeyValuePair_2_t1_1044_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_1044_0_0_0,
	&GenInst_IComparable_1_t1_1859_0_0_0,
	&GenInst_IEquatable_1_t1_1860_0_0_0,
	&GenInst_Slot_t1_168_0_0_0,
	&GenInst_Slot_t1_183_0_0_0,
	&GenInst_StackFrame_t1_197_0_0_0,
	&GenInst_Calendar_t1_201_0_0_0,
	&GenInst_ModuleBuilder_t1_293_0_0_0,
	&GenInst__ModuleBuilder_t1_1647_0_0_0,
	&GenInst_Module_t1_289_0_0_0,
	&GenInst__Module_t1_1657_0_0_0,
	&GenInst_ParameterBuilder_t1_299_0_0_0,
	&GenInst__ParameterBuilder_t1_1648_0_0_0,
	&GenInst_TypeU5BU5D_t1_31_0_0_0,
	&GenInst_Array_t_0_0_0,
	&GenInst_ICollection_t1_811_0_0_0,
	&GenInst_IList_t1_413_0_0_0,
	&GenInst_ILTokenInfo_t1_283_0_0_0,
	&GenInst_LabelData_t1_285_0_0_0,
	&GenInst_LabelFixup_t1_284_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1_281_0_0_0,
	&GenInst_TypeBuilder_t1_275_0_0_0,
	&GenInst__TypeBuilder_t1_1649_0_0_0,
	&GenInst_MethodBuilder_t1_282_0_0_0,
	&GenInst__MethodBuilder_t1_1646_0_0_0,
	&GenInst_ConstructorBuilder_t1_272_0_0_0,
	&GenInst__ConstructorBuilder_t1_1642_0_0_0,
	&GenInst_FieldBuilder_t1_279_0_0_0,
	&GenInst__FieldBuilder_t1_1644_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1_1659_0_0_0,
	&GenInst_ResourceInfo_t1_363_0_0_0,
	&GenInst_ResourceCacheItem_t1_364_0_0_0,
	&GenInst_IContextProperty_t1_822_0_0_0,
	&GenInst_Header_t1_453_0_0_0,
	&GenInst_ITrackingHandler_t1_860_0_0_0,
	&GenInst_IContextAttribute_t1_847_0_0_0,
	&GenInst_IComparable_1_t1_2178_0_0_0,
	&GenInst_IEquatable_1_t1_2183_0_0_0,
	&GenInst_IComparable_1_t1_1848_0_0_0,
	&GenInst_IEquatable_1_t1_1849_0_0_0,
	&GenInst_IComparable_1_t1_2202_0_0_0,
	&GenInst_IEquatable_1_t1_2207_0_0_0,
	&GenInst_TypeTag_t1_510_0_0_0,
	&GenInst_Enum_t1_24_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_Version_t1_319_0_0_0,
	&GenInst_Link_t2_22_0_0_0,
	&GenInst_IPAddress_t3_54_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_KeyValuePair_2_t1_1126_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_KeyValuePair_2_t1_1126_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1140_0_0_0,
	&GenInst_X509Certificate_t1_545_0_0_0,
	&GenInst_IDeserializationCallback_t1_869_0_0_0,
	&GenInst_X509ChainStatus_t3_87_0_0_0,
	&GenInst_Capture_t3_107_0_0_0,
	&GenInst_Group_t3_110_0_0_0,
	&GenInst_Mark_t3_134_0_0_0,
	&GenInst_UriScheme_t3_169_0_0_0,
	&GenInst_BigInteger_t4_18_0_0_0,
	&GenInst_ClientCertificateType_t4_98_0_0_0,
	&GenInst_KeyValuePair_2_t1_1159_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_1159_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_NCommand_t5_13_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1170_0_0_0,
	&GenInst_KeyValuePair_2_t1_902_0_0_0,
	&GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_Byte_t1_11_0_0_0,
	&GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_Byte_t1_11_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_902_0_0_0,
	&GenInst_Byte_t1_11_0_0_0_EnetChannel_t5_5_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1188_0_0_0,
	&GenInst_EnetChannel_t5_5_0_0_0,
	&GenInst_Type_t_0_0_0_CustomType_t5_50_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1194_0_0_0,
	&GenInst_Byte_t1_11_0_0_0_CustomType_t5_50_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1199_0_0_0,
	&GenInst_Object_t6_5_0_0_0,
	&GenInst_IAchievementDescription_t6_297_0_0_0,
	&GenInst_IAchievement_t6_257_0_0_0,
	&GenInst_IScore_t6_220_0_0_0,
	&GenInst_IUserProfile_t6_296_0_0_0,
	&GenInst_AchievementDescription_t6_216_0_0_0,
	&GenInst_UserProfile_t6_213_0_0_0,
	&GenInst_GcAchievementData_t6_205_0_0_0,
	&GenInst_Achievement_t6_215_0_0_0,
	&GenInst_GcScoreData_t6_206_0_0_0,
	&GenInst_Score_t6_217_0_0_0,
	&GenInst_Behaviour_t6_30_0_0_0,
	&GenInst_Component_t6_26_0_0_0,
	&GenInst_Display_t6_78_0_0_0,
	&GenInst_ContactPoint_t6_104_0_0_0,
	&GenInst_ContactPoint2D_t6_114_0_0_0,
	&GenInst_Keyframe_t6_131_0_0_0,
	&GenInst_CharacterInfo_t6_146_0_0_0,
	&GenInst_GUIContent_t6_163_0_0_0,
	&GenInst_Rect_t6_52_0_0_0,
	&GenInst_GUILayoutOption_t6_176_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_168_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1246_0_0_0,
	&GenInst_GUIStyle_t6_166_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t6_166_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1253_0_0_0,
	&GenInst_DisallowMultipleComponent_t6_195_0_0_0,
	&GenInst_Attribute_t1_2_0_0_0,
	&GenInst__Attribute_t1_1606_0_0_0,
	&GenInst_ExecuteInEditMode_t6_198_0_0_0,
	&GenInst_RequireComponent_t6_196_0_0_0,
	&GenInst_HitInfo_t6_221_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0,
	&GenInst_KeyValuePair_2_t1_1266_0_0_0,
	&GenInst_TextEditOp_t6_237_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_TextEditOp_t6_237_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t6_237_0_0_0_KeyValuePair_2_t1_1266_0_0_0,
	&GenInst_Event_t6_154_0_0_0,
	&GenInst_Event_t6_154_0_0_0_TextEditOp_t6_237_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1280_0_0_0,
	&GenInst_String_t_0_0_0_ChatChannel_t7_1_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1289_0_0_0,
	&GenInst_KeyValuePair_2_t1_1294_0_0_0,
	&GenInst_ConnectionProtocol_t5_36_0_0_0,
	&GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_ConnectionProtocol_t5_36_0_0_0,
	&GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_ConnectionProtocol_t5_36_0_0_0_Int32_t1_3_0_0_0_KeyValuePair_2_t1_1294_0_0_0,
	&GenInst_Team_t8_169_0_0_0_Object_t_0_0_0,
	&GenInst_Team_t8_169_0_0_0,
	&GenInst_KeyValuePair_2_t1_1317_0_0_0,
	&GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_Team_t8_169_0_0_0,
	&GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_Team_t8_169_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_1317_0_0_0,
	&GenInst_Team_t8_169_0_0_0_List_1_t1_955_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1331_0_0_0,
	&GenInst_State_t8_41_0_0_0,
	&GenInst_Transform_t6_61_0_0_0,
	&GenInst_RoomInfo_t8_124_0_0_0,
	&GenInst_Color_t6_40_0_0_0,
	&GenInst_Material_t6_67_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_PhotonPlayer_t8_102_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1341_0_0_0,
	&GenInst_MonoBehaviour_t8_6_0_0_0,
	&GenInst_String_t_0_0_0_RoomInfo_t8_124_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1348_0_0_0,
	&GenInst_Link_t2_29_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_PhotonView_t8_3_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_964_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Hashtable_t5_1_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_967_0_0_0,
	&GenInst_String_t_0_0_0_GameObject_t6_85_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1358_0_0_0,
	&GenInst_Type_t_0_0_0_List_1_t1_893_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1364_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_ObjectU5BU5D_t1_157_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1370_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1382_0_0_0,
	&GenInst_Component_t6_26_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1390_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_DictionaryEntry_t1_167_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_KeyValuePair_2_t1_1016_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_1016_0_0_0_KeyValuePair_2_t1_1016_0_0_0,
	&GenInst_KeyValuePair_2_t1_1044_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_1044_0_0_0_KeyValuePair_2_t1_1044_0_0_0,
	&GenInst_Boolean_t1_20_0_0_0_Object_t_0_0_0,
	&GenInst_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_KeyValuePair_2_t1_1126_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_1126_0_0_0_KeyValuePair_2_t1_1126_0_0_0,
	&GenInst_KeyValuePair_2_t1_1159_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_1159_0_0_0_KeyValuePair_2_t1_1159_0_0_0,
	&GenInst_Byte_t1_11_0_0_0_Byte_t1_11_0_0_0,
	&GenInst_KeyValuePair_2_t1_902_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_902_0_0_0_KeyValuePair_2_t1_902_0_0_0,
	&GenInst_TextEditOp_t6_237_0_0_0_Object_t_0_0_0,
	&GenInst_TextEditOp_t6_237_0_0_0_TextEditOp_t6_237_0_0_0,
	&GenInst_KeyValuePair_2_t1_1266_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_1266_0_0_0_KeyValuePair_2_t1_1266_0_0_0,
	&GenInst_ConnectionProtocol_t5_36_0_0_0_Object_t_0_0_0,
	&GenInst_ConnectionProtocol_t5_36_0_0_0_ConnectionProtocol_t5_36_0_0_0,
	&GenInst_KeyValuePair_2_t1_1294_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_1294_0_0_0_KeyValuePair_2_t1_1294_0_0_0,
	&GenInst_Team_t8_169_0_0_0_Team_t8_169_0_0_0,
	&GenInst_KeyValuePair_2_t1_1317_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_1317_0_0_0_KeyValuePair_2_t1_1317_0_0_0,
	&GenInst_IEnumerable_1_t1_1610_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m1_12012_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_12024_gp_0_0_0_0_Array_Sort_m1_12024_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_12025_gp_0_0_0_0_Array_Sort_m1_12025_gp_1_0_0_0,
	&GenInst_Array_Sort_m1_12026_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_12026_gp_0_0_0_0_Array_Sort_m1_12026_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_12027_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_12027_gp_0_0_0_0_Array_Sort_m1_12027_gp_1_0_0_0,
	&GenInst_Array_Sort_m1_12028_gp_0_0_0_0_Array_Sort_m1_12028_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_12029_gp_0_0_0_0_Array_Sort_m1_12029_gp_1_0_0_0,
	&GenInst_Array_Sort_m1_12030_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_12030_gp_0_0_0_0_Array_Sort_m1_12030_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_12031_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_12031_gp_1_0_0_0,
	&GenInst_Array_Sort_m1_12031_gp_0_0_0_0_Array_Sort_m1_12031_gp_1_0_0_0,
	&GenInst_Array_Sort_m1_12032_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_12033_gp_0_0_0_0,
	&GenInst_Array_qsort_m1_12034_gp_0_0_0_0,
	&GenInst_Array_qsort_m1_12034_gp_0_0_0_0_Array_qsort_m1_12034_gp_1_0_0_0,
	&GenInst_Array_compare_m1_12035_gp_0_0_0_0,
	&GenInst_Array_qsort_m1_12036_gp_0_0_0_0,
	&GenInst_Array_Resize_m1_12039_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m1_12041_gp_0_0_0_0,
	&GenInst_Array_ForEach_m1_12042_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1_12043_gp_0_0_0_0_Array_ConvertAll_m1_12043_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m1_12044_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1_12045_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1_12046_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1_12047_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1_12048_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1_12049_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1_12050_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1_12051_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1_12052_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1_12053_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1_12054_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1_12055_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1_12056_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1_12057_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1_12058_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1_12059_gp_0_0_0_0,
	&GenInst_Array_FindAll_m1_12060_gp_0_0_0_0,
	&GenInst_Array_Exists_m1_12061_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m1_12062_gp_0_0_0_0,
	&GenInst_Array_Find_m1_12063_gp_0_0_0_0,
	&GenInst_Array_FindLast_m1_12064_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t1_1611_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t1_1612_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1_1613_gp_0_0_0_0,
	&GenInst_IList_1_t1_1614_gp_0_0_0_0,
	&GenInst_ICollection_1_t1_1615_gp_0_0_0_0,
	&GenInst_Nullable_1_t1_837_gp_0_0_0_0,
	&GenInst_Comparer_1_t1_1622_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1_1623_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1_1605_gp_0_0_0_0,
	&GenInst_Dictionary_2_t1_901_gp_0_0_0_0,
	&GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1_2601_0_0_0,
	&GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1_12210_gp_0_0_0_0,
	&GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1_12215_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m1_12215_gp_0_0_0_0_Object_t_0_0_0,
	&GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_DictionaryEntry_t1_167_0_0_0,
	&GenInst_ShimEnumerator_t1_1624_gp_0_0_0_0_ShimEnumerator_t1_1624_gp_1_0_0_0,
	&GenInst_Enumerator_t1_1625_gp_0_0_0_0_Enumerator_t1_1625_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1_2615_0_0_0,
	&GenInst_KeyCollection_t1_1626_gp_0_0_0_0_KeyCollection_t1_1626_gp_1_0_0_0,
	&GenInst_KeyCollection_t1_1626_gp_0_0_0_0,
	&GenInst_Enumerator_t1_1627_gp_0_0_0_0_Enumerator_t1_1627_gp_1_0_0_0,
	&GenInst_Enumerator_t1_1627_gp_0_0_0_0,
	&GenInst_KeyCollection_t1_1626_gp_0_0_0_0_KeyCollection_t1_1626_gp_1_0_0_0_KeyCollection_t1_1626_gp_0_0_0_0,
	&GenInst_KeyCollection_t1_1626_gp_0_0_0_0_KeyCollection_t1_1626_gp_0_0_0_0,
	&GenInst_ValueCollection_t1_1628_gp_0_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0,
	&GenInst_ValueCollection_t1_1628_gp_1_0_0_0,
	&GenInst_Enumerator_t1_1629_gp_0_0_0_0_Enumerator_t1_1629_gp_1_0_0_0,
	&GenInst_Enumerator_t1_1629_gp_1_0_0_0,
	&GenInst_ValueCollection_t1_1628_gp_0_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0,
	&GenInst_ValueCollection_t1_1628_gp_1_0_0_0_ValueCollection_t1_1628_gp_1_0_0_0,
	&GenInst_Dictionary_2_t1_901_gp_0_0_0_0_Dictionary_2_t1_901_gp_1_0_0_0_KeyValuePair_2_t1_2601_0_0_0,
	&GenInst_KeyValuePair_2_t1_2601_0_0_0_KeyValuePair_2_t1_2601_0_0_0,
	&GenInst_Dictionary_2_t1_901_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t1_1631_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1_1632_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t1_1604_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t1_2651_0_0_0,
	&GenInst_IDictionary_2_t1_1634_gp_0_0_0_0_IDictionary_2_t1_1634_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1_1636_gp_0_0_0_0_KeyValuePair_2_t1_1636_gp_1_0_0_0,
	&GenInst_List_1_t1_1637_gp_0_0_0_0,
	&GenInst_Enumerator_t1_1638_gp_0_0_0_0,
	&GenInst_Collection_1_t1_1639_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t1_1640_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m1_12475_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1_12475_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1_12476_gp_0_0_0_0,
	&GenInst_HashSet_1_t2_34_gp_0_0_0_0,
	&GenInst_Enumerator_t2_36_gp_0_0_0_0,
	&GenInst_PrimeHelper_t2_37_gp_0_0_0_0,
	&GenInst_LinkedList_1_t3_205_gp_0_0_0_0,
	&GenInst_Enumerator_t3_206_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t3_207_gp_0_0_0_0,
	&GenInst_Queue_1_t3_208_gp_0_0_0_0,
	&GenInst_Enumerator_t3_209_gp_0_0_0_0,
	&GenInst_Stack_1_t3_210_gp_0_0_0_0,
	&GenInst_Enumerator_t3_211_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m6_1722_gp_0_0_0_0,
};
