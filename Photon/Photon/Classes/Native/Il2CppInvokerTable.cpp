﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// System.Exception
struct Exception_t1_33;
// System.String
struct String_t;
// System.MulticastDelegate
struct MulticastDelegate_t1_21;
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t1_85;
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t1_86;
// Mono.Globalization.Unicode.CodePointIndexer
struct CodePointIndexer_t1_67;
// Mono.Globalization.Unicode.Contraction
struct Contraction_t1_70;
// System.Byte[]
struct ByteU5BU5D_t1_71;
// System.Reflection.MethodBase
struct MethodBase_t1_198;
// System.Reflection.Module
struct Module_t1_289;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1_464;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1_778;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_177;
// System.Text.StringBuilder
struct StringBuilder_t1_145;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1_652;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t1_642;
// System.Int64[]
struct Int64U5BU5D_t1_806;
// System.String[]
struct StringU5BU5D_t1_202;
// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t3_5;
// System.Net.SocketAddress
struct SocketAddress_t3_63;
// System.Net.EndPoint
struct EndPoint_t3_29;
// System.Net.IPAddress
struct IPAddress_t3_54;
// System.Net.IPv6Address
struct IPv6Address_t3_58;
// System.UriFormatException
struct UriFormatException_t3_171;
// System.Array
struct Array_t;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t6_20;
// UnityEngine.GUIStyle
struct GUIStyle_t6_166;
// ExitGames.Client.Photon.Chat.ChatChannel
struct ChatChannel_t7_1;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// PhotonPlayer
struct PhotonPlayer_t8_102;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1_838;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1_839;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t6_261;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t6_262;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t6_263;
// System.Int32[]
struct Int32U5BU5D_t1_160;
// System.Single[]
struct SingleU5BU5D_t1_863;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_SByte.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Int16.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_UInt16.h"
#include "mscorlib_System_UInt32.h"
#include "mscorlib_System_UInt64.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_Globalization_UnicodeCategory.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_UIntPtr.h"
#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_TypeCode.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_MemberTypes.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_IO_MonoIOError.h"
#include "mscorlib_System_IO_FileAttributes.h"
#include "mscorlib_System_IO_MonoFileType.h"
#include "mscorlib_System_IO_MonoIOStat.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
#include "mscorlib_System_Reflection_FieldAttributes.h"
#include "mscorlib_System_Reflection_Emit_OpCode.h"
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
#include "mscorlib_System_Reflection_Module.h"
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
#include "mscorlib_System_Reflection_EventAttributes.h"
#include "mscorlib_System_Reflection_MonoEventInfo.h"
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
#include "mscorlib_System_Reflection_PropertyAttributes.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceInfo.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
#include "mscorlib_System_DateTimeKind.h"
#include "mscorlib_System_DateTimeOffset.h"
#include "mscorlib_System_Nullable_1_gen.h"
#include "mscorlib_System_MonoEnumInfo.h"
#include "mscorlib_System_PlatformID.h"
#include "mscorlib_System_Guid.h"
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
#include "System_System_ComponentModel_EditorBrowsableState.h"
#include "System_System_Net_SocketAddress.h"
#include "System_System_Net_EndPoint.h"
#include "System_System_Net_Sockets_AddressFamily.h"
#include "System_System_Net_Sockets_SocketError.h"
#include "System_System_Net_IPAddress.h"
#include "System_System_Net_IPv6Address.h"
#include "System_System_Net_SecurityProtocolType.h"
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
#include "System_System_Text_RegularExpressions_RegexOptions.h"
#include "System_System_Text_RegularExpressions_Category.h"
#include "System_System_Text_RegularExpressions_OpFlags.h"
#include "System_System_Text_RegularExpressions_Interval.h"
#include "System_System_Text_RegularExpressions_Position.h"
#include "System_System_UriHostNameType.h"
#include "System_System_UriFormatException.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonSocketState.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonSocketError.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DebugLevel.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerStateValue.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_GpType.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_BoneWeight.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
#include "UnityEngine_UnityEngine_LayerMask.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_Bounds.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_TouchPhase.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "UnityEngine_UnityEngine_CollisionFlags.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
#include "UnityEngine_UnityEngine_CharacterInfo.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "UnityEngine_UnityEngine_EventModifiers.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "UnityEngine_UnityEngine_GUIStyle.h"
#include "UnityEngine_UnityEngine_ImagePosition.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelectionArgume.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterType.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_1.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_2.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExitGames_Client_Photon_Chat_C_7.h"
#include "AssemblyU2DCSharp_CustomAuthenticationType.h"
#include "AssemblyU2DCSharp_ServerConnection.h"
#include "AssemblyU2DCSharp_PeerState.h"
#include "AssemblyU2DCSharp_CloudRegionCode.h"
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "AssemblyU2DCSharp_PhotonPlayer.h"
#include "AssemblyU2DCSharp_ConnectionState.h"
#include "AssemblyU2DCSharp_CloudRegionFlag.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizeType.h"
#include "AssemblyU2DCSharp_PunTeams_Team.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_11.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__1.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_19.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_15.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_28.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_16.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"
#include "mscorlib_System_Collections_Generic_Link.h"
#include "mscorlib_System_Collections_Hashtable_Slot.h"
#include "mscorlib_System_Collections_SortedList_Slot.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCacheItem.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
#include "System_System_Text_RegularExpressions_Mark.h"
#include "System_System_Uri_UriScheme.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#include "UnityEngine_UnityEngine_ContactPoint.h"
#include "UnityEngine_UnityEngine_ContactPoint2D.h"
#include "UnityEngine_UnityEngine_Keyframe.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_17.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
#include "AssemblyU2DCSharp_CubeInter_State.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen_1.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_9.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_8.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_8.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_12.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_13.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_17.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen_3.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_24.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_24.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_27.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_30.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__2.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_22.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen_5.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25.h"

void* RuntimeInvoker_Void_t1_29 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

void* RuntimeInvoker_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_ObjectU5BU5DU26_t1_1676 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t1_157** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t1_157**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_ObjectU5BU5DU26_t1_1676 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t1_157** p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t1_157**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t1_11_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t1_15_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1_19_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1_19  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Decimal_t1_19  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1_13_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t1_7_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1_14_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t1_8_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_Int32_t1_3_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t1_33 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t1_33 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_Int32U26_t1_1678_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t1_33 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t1_33 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_SByte_t1_12_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t1_33 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t1_33 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32U26_t1_1678_Object_t_SByte_t1_12_SByte_t1_12_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t1_33 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t1_33 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32U26_t1_1678_Object_t_Object_t_BooleanU26_t1_1679_BooleanU26_t1_1679 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32U26_t1_1678_Object_t_Object_t_BooleanU26_t1_1679 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Int32U26_t1_1678_Object_t_Int32U26_t1_1678_SByte_t1_12_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t1_33 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t1_33 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32U26_t1_1678_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int16_t1_13_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_SByte_t1_12_Int32U26_t1_1678_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t1_33 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t1_33 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_Int64U26_t1_1680_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t1_33 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t1_33 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t1_7_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_SByte_t1_12_Int64U26_t1_1680_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t1_33 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t1_33 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t1_7_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int64U26_t1_1680 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_Int64U26_t1_1680 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_UInt32U26_t1_1681_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t1_33 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t1_33 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_SByte_t1_12_UInt32U26_t1_1681_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t1_33 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t1_33 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t1_8_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t1_8_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_UInt32U26_t1_1681 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_UInt32U26_t1_1681 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_SByte_t1_12_UInt64U26_t1_1682_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t1_33 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t1_33 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_UInt64U26_t1_1682 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t1_11_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t1_11_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_ByteU26_t1_1683 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_ByteU26_t1_1683 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_SByteU26_t1_1684_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t1_33 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t1_33 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1_12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1_12_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByteU26_t1_1684 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_Int16U26_t1_1685_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t1_33 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t1_33 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1_13_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1_13_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int16U26_t1_1685 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1_14_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1_14_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_UInt16U26_t1_1686 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_UInt16U26_t1_1686 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_ByteU2AU26_t1_1687_ByteU2AU26_t1_1687_DoubleU2AU26_t1_1688_UInt16U2AU26_t1_1689_UInt16U2AU26_t1_1689_UInt16U2AU26_t1_1689_UInt16U2AU26_t1_1689 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

void* RuntimeInvoker_UnicodeCategory_t1_218_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Char_t1_15_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t1_15_Int16_t1_13_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int16_t1_13_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Char_t1_15_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Object_t_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int16_t1_13_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int16_t1_13_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1_13_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32U26_t1_1678_Int32U26_t1_1678_Int32U26_t1_1678_BooleanU26_t1_1679_StringU26_t1_1690 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1_13_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_SByte_t1_12_DoubleU26_t1_1691_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t1_33 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t1_33 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_DoubleU26_t1_1691 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t1_19_Decimal_t1_19_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1_19  (*Func)(void* obj, Decimal_t1_19  p1, Decimal_t1_19  p2, const MethodInfo* method);
	Decimal_t1_19  ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), *((Decimal_t1_19 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t1_7_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Decimal_t1_19_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t1_19  p1, Decimal_t1_19  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), *((Decimal_t1_19 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1_19_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1_19  (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	Decimal_t1_19  ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Decimal_t1_19_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1_19  p1, Decimal_t1_19  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), *((Decimal_t1_19 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1_19_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1_19  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Decimal_t1_19  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t_Int32U26_t1_1678_BooleanU26_t1_1679_BooleanU26_t1_1679_Int32U26_t1_1678_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t1_19_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1_19  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Decimal_t1_19  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_DecimalU26_t1_1692_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t1_19 * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t1_19 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_DecimalU26_t1_1692_UInt64U26_t1_1682 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1_19 * p1, uint64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1_19 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_DecimalU26_t1_1692_Int64U26_t1_1680 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1_19 * p1, int64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1_19 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_DecimalU26_t1_1692_DecimalU26_t1_1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1_19 * p1, Decimal_t1_19 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1_19 *)args[0], (Decimal_t1_19 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_DecimalU26_t1_1692_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1_19 * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1_19 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_DecimalU26_t1_1692_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1_19 * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1_19 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18_DecimalU26_t1_1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t1_19 * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t1_19 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_DecimalU26_t1_1692_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t1_19 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t1_19 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_DecimalU26_t1_1692_DecimalU26_t1_1692_DecimalU26_t1_1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1_19 * p1, Decimal_t1_19 * p2, Decimal_t1_19 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1_19 *)args[0], (Decimal_t1_19 *)args[1], (Decimal_t1_19 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t1_11_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1_12_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1_13_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1_14_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t1_8_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1_19_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1_19  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Decimal_t1_19  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1_19_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1_19  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Decimal_t1_19  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1_19_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1_19  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Decimal_t1_19  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1_19_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1_19  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Decimal_t1_19  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1_19_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1_19  (*Func)(void* obj, float p1, const MethodInfo* method);
	Decimal_t1_19  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1_19_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1_19  (*Func)(void* obj, double p1, const MethodInfo* method);
	Decimal_t1_19  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_StreamingContext_t1_514 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t1_514  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1_514 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_UInt32_t1_8 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t1_8_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t1_1693 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t1_21 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t1_21 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t1_3_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_TypeCode_t1_769 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int64_t1_7_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t1_7_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t1_7_Int64_t1_7_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_Int64_t1_7_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_Object_t_Int64_t1_7_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_TypeAttributes_t1_359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MemberTypes_t1_337 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeTypeHandle_t1_30 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1_30  (*Func)(void* obj, const MethodInfo* method);
	RuntimeTypeHandle_t1_30  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_TypeCode_t1_769_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t1_30 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t1_30  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t1_30 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_RuntimeTypeHandle_t1_30_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1_30  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RuntimeTypeHandle_t1_30  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_SByte_t1_12_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_RuntimeFieldHandle_t1_36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t1_36  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t1_36 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_ContractionU5BU5DU26_t1_1694_Level2MapU5BU5DU26_t1_1695 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t1_85** p3, Level2MapU5BU5D_t1_86** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t1_85**)args[2], (Level2MapU5BU5D_t1_86**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_CodePointIndexerU26_t1_1696_ByteU2AU26_t1_1687_ByteU2AU26_t1_1687_CodePointIndexerU26_t1_1696_ByteU2AU26_t1_1687 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t1_67 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t1_67 ** p5, uint8_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t1_67 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t1_67 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t1_11_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t1_8_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t1_11_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExtenderType_t1_82_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_BooleanU26_t1_1679_BooleanU26_t1_1679_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_BooleanU26_t1_1679_BooleanU26_t1_1679_SByte_t1_12_SByte_t1_12_ContextU26_t1_1697 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t1_79 * p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t1_79 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12_ContextU26_t1_1697 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t1_79 * p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t1_79 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Int32_t1_3_Int32_t1_3_BooleanU26_t1_1679 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int16_t1_13_Int32_t1_3_SByte_t1_12_ContextU26_t1_1697 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t1_79 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1_79 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Object_t_ContextU26_t1_1697 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t1_79 * p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t1_79 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_SByte_t1_12_ContextU26_t1_1697 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t1_79 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1_79 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12_ContextU26_t1_1697 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t1_79 * p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t1_79 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_ContractionU26_t1_1698_ContextU26_t1_1697 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t1_70 ** p8, Context_t1_79 * p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t1_70 **)args[7], (Context_t1_79 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12_ContextU26_t1_1697 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t1_79 * p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t1_79 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_ContractionU26_t1_1698_ContextU26_t1_1697 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t1_70 ** p9, Context_t1_79 * p10, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t1_70 **)args[8], (Context_t1_79 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_ByteU5BU5DU26_t1_1699_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t1_71** p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t1_71**)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ConfidenceFactor_t1_91 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Sign_t1_93_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

void* RuntimeInvoker_DSAParameters_t1_563_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1_563  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DSAParameters_t1_563  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_DSAParameters_t1_563 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t1_563  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t1_563 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t1_563 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t1_563  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t1_563 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_RSAParameters_t1_592_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1_592  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	RSAParameters_t1_592  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_RSAParameters_t1_592 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t1_592  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t1_592 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_DSAParameters_t1_563_BooleanU26_t1_1679 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1_563  (*Func)(void* obj, bool* p1, const MethodInfo* method);
	DSAParameters_t1_563  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1_12_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_DateTime_t1_127 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t1_11 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32U26_t1_1678_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32U26_t1_1678_ByteU26_t1_1683_Int32U26_t1_1678_ByteU5BU5DU26_t1_1699 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t1_71** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t1_71**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int16_t1_13_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t1_13_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Single_t1_17_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Single_t1_17_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Single_t1_17_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_DictionaryEntry_t1_167 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1_167  (*Func)(void* obj, const MethodInfo* method);
	DictionaryEntry_t1_167  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_SByte_t1_12_MethodBaseU26_t1_1700_Int32U26_t1_1678_Int32U26_t1_1678_StringU26_t1_1690_Int32U26_t1_1678_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t1_198 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t1_198 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t1_3_DateTime_t1_127 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t1_127  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t1_709_DateTime_t1_127 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t1_127  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t1_709_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32U26_t1_1678_Int32U26_t1_1678_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32U26_t1_1678_Int32U26_t1_1678_Int32U26_t1_1678_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_DateTime_t1_127_DateTime_t1_127_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t1_127  p1, DateTime_t1_127  p2, TimeSpan_t1_213  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), *((DateTime_t1_127 *)args[1]), *((TimeSpan_t1_213 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1_213  (*Func)(void* obj, const MethodInfo* method);
	TimeSpan_t1_213  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t1_15 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1_19  (*Func)(void* obj, const MethodInfo* method);
	Decimal_t1_19  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1_14 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_SByte_t1_12_Int32_t1_3_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Int64_t1_7_Int64_t1_7_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_IntPtr_t_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_MonoIOErrorU26_t1_1701 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3_MonoIOErrorU26_t1_1701 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_MonoIOErrorU26_t1_1701 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_FileAttributes_t1_233_Object_t_MonoIOErrorU26_t1_1701 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MonoFileType_t1_245_IntPtr_t_MonoIOErrorU26_t1_1701 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_MonoIOStatU26_t1_1702_MonoIOErrorU26_t1_1701 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t1_243 * p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t1_243 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_MonoIOErrorU26_t1_1701 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_IntPtr_t_MonoIOErrorU26_t1_1701 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_IntPtr_t_Object_t_Int32_t1_3_Int32_t1_3_MonoIOErrorU26_t1_1701 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t1_7_IntPtr_t_Int64_t1_7_Int32_t1_3_MonoIOErrorU26_t1_1701 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t1_7_IntPtr_t_MonoIOErrorU26_t1_1701 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_IntPtr_t_Int64_t1_7_MonoIOErrorU26_t1_1701 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_SByte_t1_12_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_CallingConventions_t1_327 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeMethodHandle_t1_760 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t1_760  (*Func)(void* obj, const MethodInfo* method);
	RuntimeMethodHandle_t1_760  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t1_338 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodToken_t1_292 (const MethodInfo* method, void* obj, void** args)
{
	typedef MethodToken_t1_292  (*Func)(void* obj, const MethodInfo* method);
	MethodToken_t1_292  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FieldAttributes_t1_335 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeFieldHandle_t1_36 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t1_36  (*Func)(void* obj, const MethodInfo* method);
	RuntimeFieldHandle_t1_36  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_OpCode_t1_296 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1_296  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1_296 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_OpCode_t1_296_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1_296  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1_296 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_StackBehaviour_t1_300 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t1_1678_ModuleU26_t1_1703 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t1_289 ** p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t1_289 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_AssemblyNameFlags_t1_320 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Object_t_ObjectU5BU5DU26_t1_1676_Object_t_Object_t_Object_t_ObjectU26_t1_1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t1_157** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t1_157**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_ObjectU5BU5DU26_t1_1676_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t1_157** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t1_157**)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_ObjectU5BU5DU26_t1_1676_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t1_157** p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t1_157**)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Object_t_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_EventAttributes_t1_333 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t1_36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t1_36  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t1_36 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t1_514 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t1_514  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t1_514 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t1_760 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t1_760  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t1_760 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_MonoEventInfoU26_t1_1705 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t1_342 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t1_342 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoEventInfo_t1_342_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t1_342  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	MonoEventInfo_t1_342  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_MonoMethodInfoU26_t1_1706 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t1_346 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t1_346 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoMethodInfo_t1_346_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t1_346  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	MonoMethodInfo_t1_346  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t1_338_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CallingConventions_t1_327_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t1_33 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t1_33 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_MonoPropertyInfoU26_t1_1707_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t1_347 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t1_347 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_PropertyAttributes_t1_355 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_ParameterAttributes_t1_350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int64_t1_7_ResourceInfoU26_t1_1708 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, ResourceInfo_t1_363 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (ResourceInfo_t1_363 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_GCHandle_t1_393_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t1_393  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	GCHandle_t1_393  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Byte_t1_11_IntPtr_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_BooleanU26_t1_1679 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t1_1690 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t1_1690 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, String_t** p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (String_t**)args[3], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_Object_t_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t1_213_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1_213  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TimeSpan_t1_213  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t1_213  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t1_213 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Byte_t1_11 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1_514_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1_514  p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1_514 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1_514_ISurrogateSelectorU26_t1_1709 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t1_514  p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1_514 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StringU26_t1_1690 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (String_t**)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t1_1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_StringU26_t1_1690_StringU26_t1_1690 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WellKnownObjectMode_t1_506 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContext_t1_514 (const MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t1_514  (*Func)(void* obj, const MethodInfo* method);
	StreamingContext_t1_514  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeFilterLevel_t1_523 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_BooleanU26_t1_1679 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t1_11_Object_t_SByte_t1_12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t1_11_Object_t_SByte_t1_12_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_ObjectU26_t1_1704_HeaderU5BU5DU26_t1_1710 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t1_778** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t1_778**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Byte_t1_11_Object_t_SByte_t1_12_ObjectU26_t1_1704_HeaderU5BU5DU26_t1_1710 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t1_778** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t1_778**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Byte_t1_11_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Byte_t1_11_Object_t_Int64U26_t1_1680_ObjectU26_t1_1704_SerializationInfoU26_t1_1711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t1_177 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t1_177 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_SByte_t1_12_Int64U26_t1_1680_ObjectU26_t1_1704_SerializationInfoU26_t1_1711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t1_177 ** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t1_177 **)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int64U26_t1_1680_ObjectU26_t1_1704_SerializationInfoU26_t1_1711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t1_177 ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t1_177 **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int64_t1_7_ObjectU26_t1_1704_SerializationInfoU26_t1_1711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t1_177 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t1_177 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int64_t1_7_Object_t_Object_t_Int64_t1_7_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int64U26_t1_1680_ObjectU26_t1_1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int64U26_t1_1680_ObjectU26_t1_1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int64_t1_7_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int64_t1_7_Int64_t1_7_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int64_t1_7_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Byte_t1_11 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Int64_t1_7_Int32_t1_3_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int64_t1_7_Object_t_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_Object_t_Int64_t1_7_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_StreamingContext_t1_514 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1_514  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1_514 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_StreamingContext_t1_514 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1_514  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1_514 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_StreamingContext_t1_514 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t1_514  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t1_514 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t1_514_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t1_514  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t1_514 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_DateTime_t1_127 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t1_127  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t1_127 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_SerializationEntry_t1_540 (const MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t1_540  (*Func)(void* obj, const MethodInfo* method);
	SerializationEntry_t1_540  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContextStates_t1_544 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CspProviderFlags_t1_557 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t1_8_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int64_t1_7_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t1_8_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_UInt32U26_t1_1681_Int32_t1_3_UInt32U26_t1_1681_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t1_8_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int64_t1_7_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UInt64_t1_10_Int64_t1_7_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_Int64_t1_7_Int64_t1_7_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CipherMode_t1_553 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PaddingMode_t1_580 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_StringBuilderU26_t1_1712_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t1_145 ** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t1_145 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_EncoderFallbackBufferU26_t1_1713_CharU5BU5DU26_t1_1714 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t1_652 ** p6, CharU5BU5D_t1_16** p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t1_652 **)args[5], (CharU5BU5D_t1_16**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_DecoderFallbackBufferU26_t1_1715 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t1_642 ** p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t1_642 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int16_t1_13_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int16_t1_13_Int16_t1_13_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int16_t1_13_Int16_t1_13_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_SByte_t1_12_Int32U26_t1_1678_BooleanU26_t1_1679_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_CharU26_t1_1716_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_CharU26_t1_1716_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_CharU26_t1_1716_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Int32_t1_3_CharU26_t1_1716_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t_DecoderFallbackBufferU26_t1_1715_ByteU5BU5DU26_t1_1699_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t1_642 ** p7, ByteU5BU5D_t1_71** p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t1_642 **)args[6], (ByteU5BU5D_t1_71**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t_DecoderFallbackBufferU26_t1_1715_ByteU5BU5DU26_t1_1699_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t1_642 ** p6, ByteU5BU5D_t1_71** p7, int8_t p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t1_642 **)args[5], (ByteU5BU5D_t1_71**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_DecoderFallbackBufferU26_t1_1715_ByteU5BU5DU26_t1_1699_Object_t_Int64_t1_7_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1_642 ** p2, ByteU5BU5D_t1_71** p3, Object_t * p4, int64_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1_642 **)args[1], (ByteU5BU5D_t1_71**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_DecoderFallbackBufferU26_t1_1715_ByteU5BU5DU26_t1_1699_Object_t_Int64_t1_7_Int32_t1_3_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1_642 ** p2, ByteU5BU5D_t1_71** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1_642 **)args[1], (ByteU5BU5D_t1_71**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_UInt32U26_t1_1681_UInt32U26_t1_1681_Object_t_DecoderFallbackBufferU26_t1_1715_ByteU5BU5DU26_t1_1699_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t1_642 ** p9, ByteU5BU5D_t1_71** p10, int8_t p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t1_642 **)args[8], (ByteU5BU5D_t1_71**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Int32_t1_3_UInt32U26_t1_1681_UInt32U26_t1_1681_Object_t_DecoderFallbackBufferU26_t1_1715_ByteU5BU5DU26_t1_1699_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t1_642 ** p8, ByteU5BU5D_t1_71** p9, int8_t p10, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t1_642 **)args[7], (ByteU5BU5D_t1_71**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_SByte_t1_12_Object_t_BooleanU26_t1_1679 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_SByte_t1_12_SByte_t1_12_Object_t_BooleanU26_t1_1679 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_TimeSpan_t1_213_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t1_213  p1, TimeSpan_t1_213  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t1_213 *)args[0]), *((TimeSpan_t1_213 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int64_t1_7_Int64_t1_7_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_IntPtr_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t1_7_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t1_7_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1_14_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Byte_t1_11_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t1_11_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t1_11_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t1_11_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t1_11_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t1_15_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t1_15_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t1_15_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t1_15_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, float p1, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t1_18_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1_13_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1_13_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1_13_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1_13_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1_13_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1_13_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t1_7_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t1_7_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t1_7_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t1_7_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1_12_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1_12_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1_12_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1_12_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1_12_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1_14_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1_14_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1_14_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1_14_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1_14_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1_14_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t1_8_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t1_8_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t1_8_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t1_8_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t1_8_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1_10_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_ByteU5BU5DU26_t1_1699 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ByteU5BU5D_t1_71** p1, const MethodInfo* method);
	((Func)method->method)(obj, (ByteU5BU5D_t1_71**)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t1_213  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t1_213 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int64_t1_7_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DayOfWeek_t1_709 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTimeKind_t1_706 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, TimeSpan_t1_213  p1, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, *((TimeSpan_t1_213 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, double p1, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_DateTime_t1_127_DateTime_t1_127 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t1_127  p1, DateTime_t1_127  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), *((DateTime_t1_127 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_DateTime_t1_127 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t1_127  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_DateTime_t1_127_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, DateTime_t1_127  p1, int32_t p2, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_Object_t_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Int32_t1_3_DateTimeU26_t1_1717_DateTimeOffsetU26_t1_1718_SByte_t1_12_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t1_127 * p4, DateTimeOffset_t1_707 * p5, int8_t p6, Exception_t1_33 ** p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t1_127 *)args[3], (DateTimeOffset_t1_707 *)args[4], *((int8_t*)args[5]), (Exception_t1_33 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1_12_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t1_33 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t1_33 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Object_t_SByte_t1_12_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12_Int32U26_t1_1678_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_SByte_t1_12_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t_SByte_t1_12_DateTimeU26_t1_1717_DateTimeOffsetU26_t1_1718_Object_t_Int32_t1_3_SByte_t1_12_BooleanU26_t1_1679_BooleanU26_t1_1679 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t1_127 * p5, DateTimeOffset_t1_707 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t1_127 *)args[4], (DateTimeOffset_t1_707 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_Object_t_Object_t_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t_Int32_t1_3_DateTimeU26_t1_1717_SByte_t1_12_BooleanU26_t1_1679_SByte_t1_12_ExceptionU26_t1_1677 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t1_127 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t1_33 ** p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t1_127 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t1_33 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_DateTime_t1_127_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, DateTime_t1_127  p1, TimeSpan_t1_213  p2, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), *((TimeSpan_t1_213 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_DateTime_t1_127_DateTime_t1_127 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t1_127  p1, DateTime_t1_127  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), *((DateTime_t1_127 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_DateTime_t1_127 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t1_127  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_DateTime_t1_127_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t1_127  p1, TimeSpan_t1_213  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), *((TimeSpan_t1_213 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int64_t1_7_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t1_213  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t1_213 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_DateTimeOffset_t1_707 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t1_707  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t1_707 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_DateTimeOffset_t1_707 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t1_707  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t1_707 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t1_13_Object_t_BooleanU26_t1_1679_BooleanU26_t1_1679 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1_13_Object_t_BooleanU26_t1_1679_BooleanU26_t1_1679_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t1_127_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t1_127  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t1_127_Nullable_1_t1_826_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t1_127  p1, Nullable_1_t1_826  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), *((Nullable_1_t1_826 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_MonoEnumInfo_t1_720 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t1_720  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t1_720 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_MonoEnumInfoU26_t1_1719 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t1_720 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t1_720 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Int16_t1_13_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int64_t1_7_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PlatformID_t1_756 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int16_t1_13_Int16_t1_13_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Guid_t1_730 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t1_730  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t1_730 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Guid_t1_730 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t1_730  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t1_730 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Guid_t1_730 (const MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t1_730  (*Func)(void* obj, const MethodInfo* method);
	Guid_t1_730  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Double_t1_18_Double_t1_18_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeAttributes_t1_359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_UInt64U2AU26_t1_1720_Int32U2AU26_t1_1721_CharU2AU26_t1_1722_CharU2AU26_t1_1722_Int64U2AU26_t1_1723_Int32U2AU26_t1_1721 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Double_t1_18_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t1_19  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t1_19 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t1_13_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t1_7_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Single_t1_17_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Double_t1_18_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Decimal_t1_19_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t1_19  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t1_19 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Single_t1_17_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Double_t1_18_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_BooleanU26_t1_1679_SByte_t1_12_Int32U26_t1_1678_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t1_7_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t1_213_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1_213  (*Func)(void* obj, TimeSpan_t1_213  p1, const MethodInfo* method);
	TimeSpan_t1_213  ret = ((Func)method->method)(obj, *((TimeSpan_t1_213 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_TimeSpan_t1_213_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t1_213  p1, TimeSpan_t1_213  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t1_213 *)args[0]), *((TimeSpan_t1_213 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t1_213  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t1_213 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t1_213  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t1_213 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t1_213_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1_213  (*Func)(void* obj, double p1, const MethodInfo* method);
	TimeSpan_t1_213  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t1_213_Double_t1_18_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1_213  (*Func)(void* obj, double p1, int64_t p2, const MethodInfo* method);
	TimeSpan_t1_213  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t1_213_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1_213  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	TimeSpan_t1_213  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t1_213_TimeSpan_t1_213_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1_213  (*Func)(void* obj, TimeSpan_t1_213  p1, TimeSpan_t1_213  p2, const MethodInfo* method);
	TimeSpan_t1_213  ret = ((Func)method->method)(obj, *((TimeSpan_t1_213 *)args[0]), *((TimeSpan_t1_213 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t1_213_DateTime_t1_127 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1_213  (*Func)(void* obj, DateTime_t1_127  p1, const MethodInfo* method);
	TimeSpan_t1_213  ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_DateTime_t1_127_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t1_127  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t1_127_DateTime_t1_127 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t1_127  (*Func)(void* obj, DateTime_t1_127  p1, const MethodInfo* method);
	DateTime_t1_127  ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t1_213_DateTime_t1_127_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1_213  (*Func)(void* obj, DateTime_t1_127  p1, TimeSpan_t1_213  p2, const MethodInfo* method);
	TimeSpan_t1_213  ret = ((Func)method->method)(obj, *((DateTime_t1_127 *)args[0]), *((TimeSpan_t1_213 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int64U5BU5DU26_t1_1724_StringU5BU5DU26_t1_1725 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t1_806** p2, StringU5BU5D_t1_202** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t1_806**)args[1], (StringU5BU5D_t1_202**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t3_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t3_5 ** p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t3_5 **)args[1], method);
	return ret;
}

void* RuntimeInvoker_EditorBrowsableState_t3_15 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_IntPtr_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_IntPtr_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SocketAddressU26_t3_214_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, SocketAddress_t3_63 ** p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (SocketAddress_t3_63 **)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_EndPointU26_t3_215_SByte_t1_12_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, EndPoint_t3_29 ** p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (EndPoint_t3_29 **)args[4], *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AddressFamily_t3_23 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Object_t_Int32U26_t1_1678_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_IntPtr_t_Int32_t1_3_Int32_t1_3_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_IntPtr_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SocketErrorU26_t3_216 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Int32_t1_3_ObjectU26_t1_1704_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, Object_t ** p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t **)args[3], (int32_t*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Int32_t1_3_Object_t_Object_t_Int32_t1_3_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, int32_t p6, int32_t* p7, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], *((int32_t*)args[5]), (int32_t*)args[6], method);
	return NULL;
}

void* RuntimeInvoker_SocketError_t3_30 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_StringU26_t1_1690_StringU5BU5DU26_t1_1725_StringU5BU5DU26_t1_1725 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, StringU5BU5D_t1_202** p3, StringU5BU5D_t1_202** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (StringU5BU5D_t1_202**)args[2], (StringU5BU5D_t1_202**)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_IPAddressU26_t3_217 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t3_54 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t3_54 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_IPv6AddressU26_t3_218 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t3_58 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t3_58 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t3_59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_SByte_t1_12_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_AsnDecodeStatus_t3_102_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_X509ChainStatusFlags_t3_90_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t3_90_Object_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t3_90_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t3_90 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_X509RevocationFlag_t3_97 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509RevocationMode_t3_98 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509VerificationFlags_t3_101 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t3_95 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t3_95_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t1_11_Int16_t1_13_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_RegexOptions_t3_119 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Category_t3_126_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_UInt16_t1_14_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int16_t1_13_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_UInt16_t1_14_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int16_t1_13_Int16_t1_13_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int16_t1_13_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_UInt16_t1_14 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_UInt16_t1_14_UInt16_t1_14_UInt16_t1_14 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_OpFlags_t3_121_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_UInt16_t1_14_UInt16_t1_14 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int32U26_t1_1678_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int32U26_t1_1678_Int32U26_t1_1678_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32U26_t1_1678_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_UInt16_t1_14_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32U26_t1_1678_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t3_141 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t3_141  (*Func)(void* obj, const MethodInfo* method);
	Interval_t3_141  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Interval_t3_141 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t3_141  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t3_141 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Interval_t3_141 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t3_141  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t3_141 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t3_141_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t3_141  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Interval_t3_141  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Double_t1_18_Interval_t3_141 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t3_141  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t3_141 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Interval_t3_141_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t3_141  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t3_141 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32U26_t1_1678_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_RegexOptionsU26_t3_219 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_RegexOptionsU26_t3_219_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Int32U26_t1_1678_Int32U26_t1_1678_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Category_t3_126 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32U26_t1_1678_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_UInt16_t1_14_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int16_t1_13_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_UInt16_t1_14 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Position_t3_122 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriHostNameType_t3_172_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_StringU26_t1_1690 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Char_t1_15_Object_t_Int32U26_t1_1678_CharU26_t1_1716 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_UriFormatExceptionU26_t3_220 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t3_171 ** p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t3_171 **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Sign_t4_16_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ConfidenceFactor_t4_20 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t4_43 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Byte_t1_11 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Byte_t1_11_Byte_t1_11 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_AlertLevel_t4_55 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AlertDescription_t4_56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Byte_t1_11 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Int16_t1_13_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_Int16_t1_13_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_CipherAlgorithmType_t4_58 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HashAlgorithmType_t4_76 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExchangeAlgorithmType_t4_74 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int64_t1_7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_ByteU5BU5DU26_t1_1699_ByteU5BU5DU26_t1_1699 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t1_71** p2, ByteU5BU5D_t1_71** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t1_71**)args[1], (ByteU5BU5D_t1_71**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t1_11_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1_13_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_Int16_t1_13_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_SecurityProtocolType_t4_86 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityCompressionType_t4_85 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeType_t4_99 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeState_t4_75 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t4_86_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t1_11_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t1_11_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Byte_t1_11_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t1_11_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_SByte_t1_12_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Byte_t1_11_Byte_t1_11_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_RSAParameters_t1_592 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1_592  (*Func)(void* obj, const MethodInfo* method);
	RSAParameters_t1_592  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Byte_t1_11_Byte_t1_11 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Byte_t1_11_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_ContentType_t4_69 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_Byte_t1_11 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, uint8_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((uint8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1_12_Object_t_Byte_t1_11_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, uint8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((uint8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_ConnectionProtocol_t5_36 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PhotonSocketState_t5_24 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PhotonSocketError_t5_25_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Byte_t1_11 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_StringU26_t1_1690_UInt16U26_t1_1686 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, uint16_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_DebugLevel_t5_37 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PeerStateValue_t5_35 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t1_13_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_GpType_t5_45_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1_12_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int16_t1_13_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int16_t1_13_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Single_t1_17_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Single_t1_17_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, Object_t * p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Double_t1_18_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_BooleanU26_t1_1679_BooleanU26_t1_1679 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int16U26_t1_1685_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t* p1, Object_t * p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, (int16_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32U26_t1_1678_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SingleU26_t1_1726_Object_t_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, Object_t * p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], (Object_t *)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int16_t1_13_ArrayU26_t1_1727 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t p2, Array_t ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Array_t **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_ByteU26_t1_1683_ByteU26_t1_1683 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t* p2, uint8_t* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], (uint8_t*)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int64_t1_7_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_GcAchievementDescriptionData_t6_204_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t6_204  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t6_204 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_GcUserProfileData_t6_203_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t6_203  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t6_203 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Double_t1_18_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int64_t1_7_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_UserProfileU5BU5DU26_t6_303_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t6_20** p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t6_20**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_UserProfileU5BU5DU26_t6_303_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t6_20** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t6_20**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_GcScoreData_t6_206 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t6_206  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t6_206 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_BoneWeight_t6_24_BoneWeight_t6_24 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t6_24  p1, BoneWeight_t6_24  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t6_24 *)args[0]), *((BoneWeight_t6_24 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t6_49  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t6_49 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6_49 *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_IntPtr_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_CullingGroupEvent_t6_36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CullingGroupEvent_t6_36  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CullingGroupEvent_t6_36 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_CullingGroupEvent_t6_36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CullingGroupEvent_t6_36  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CullingGroupEvent_t6_36 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Color_t6_40_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6_40  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t6_40 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t6_305_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43 * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43 *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Vector3_t6_49_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Vector3U26_t6_304_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49 * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t6_49 *)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Vector3_t6_49_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49  p1, Vector3_t6_49  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Vector3_t6_49 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Vector3U26_t6_304_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49 * p1, Vector3_t6_49 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t6_49 *)args[0], (Vector3_t6_49 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Color_t6_40 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6_40  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t6_40 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_ColorU26_t6_306 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6_40 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t6_40 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_LayerMask_t6_47 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t6_47  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t6_47 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LayerMask_t6_47_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t6_47  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LayerMask_t6_47  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Vector2_t6_48 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t6_48  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t6_48 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6_48  (*Func)(void* obj, const MethodInfo* method);
	Vector2_t6_48  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t6_48_Vector2_t6_48_Vector2_t6_48 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6_48  (*Func)(void* obj, Vector2_t6_48  p1, Vector2_t6_48  p2, const MethodInfo* method);
	Vector2_t6_48  ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), *((Vector2_t6_48 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t6_48_Vector2_t6_48 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6_48  (*Func)(void* obj, Vector2_t6_48  p1, const MethodInfo* method);
	Vector2_t6_48  ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t6_48_Vector2_t6_48_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6_48  (*Func)(void* obj, Vector2_t6_48  p1, float p2, const MethodInfo* method);
	Vector2_t6_48  ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Vector2_t6_48_Vector2_t6_48 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t6_48  p1, Vector2_t6_48  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), *((Vector2_t6_48 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t6_48_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6_48  (*Func)(void* obj, Vector3_t6_49  p1, const MethodInfo* method);
	Vector2_t6_48  ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_Vector2_t6_48 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Vector2_t6_48  p1, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Single_t1_17_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t6_49_Vector3_t6_49_Vector3_t6_49_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Vector3_t6_49  p1, Vector3_t6_49  p2, float p3, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Vector3_t6_49 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_Vector3U26_t6_304_Vector3U26_t6_304_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Vector3_t6_49 * p1, Vector3_t6_49 * p2, float p3, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, (Vector3_t6_49 *)args[0], (Vector3_t6_49 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_Vector3_t6_49_Vector3_t6_49_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Vector3_t6_49  p1, Vector3_t6_49  p2, float p3, float p4, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Vector3_t6_49 *)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_Vector3U26_t6_304_Vector3U26_t6_304_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Vector3_t6_49 * p1, Vector3_t6_49 * p2, float p3, float p4, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, (Vector3_t6_49 *)args[0], (Vector3_t6_49 *)args[1], *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Vector3_t6_49  p1, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Vector3_t6_49_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t6_49  p1, Vector3_t6_49  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Vector3_t6_49 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t6_49  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_Vector3_t6_49_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Vector3_t6_49  p1, Vector3_t6_49  p2, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Vector3_t6_49 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_Vector3_t6_49_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Vector3_t6_49  p1, float p2, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_Single_t1_17_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, float p1, Vector3_t6_49  p2, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t6_49 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Vector3_t6_49_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t6_49  p1, Vector3_t6_49  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Vector3_t6_49 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t6_40 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t6_40  (*Func)(void* obj, const MethodInfo* method);
	Color_t6_40  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t6_40_Color_t6_40_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t6_40  (*Func)(void* obj, Color_t6_40  p1, float p2, const MethodInfo* method);
	Color_t6_40  ret = ((Func)method->method)(obj, *((Color_t6_40 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t6_55_Color_t6_40 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t6_55  (*Func)(void* obj, Color_t6_40  p1, const MethodInfo* method);
	Vector4_t6_55  ret = ((Func)method->method)(obj, *((Color_t6_40 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t6_51 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t6_51  (*Func)(void* obj, const MethodInfo* method);
	Quaternion_t6_51  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Quaternion_t6_51_Quaternion_t6_51 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Quaternion_t6_51  p1, Quaternion_t6_51  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Quaternion_t6_51 *)args[0]), *((Quaternion_t6_51 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t6_51_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t6_51  (*Func)(void* obj, Vector3_t6_49  p1, const MethodInfo* method);
	Quaternion_t6_51  ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t6_51_Vector3U26_t6_304_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t6_51  (*Func)(void* obj, Vector3_t6_49 * p1, Vector3_t6_49 * p2, const MethodInfo* method);
	Quaternion_t6_51  ret = ((Func)method->method)(obj, (Vector3_t6_49 *)args[0], (Vector3_t6_49 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t6_51_Quaternion_t6_51_Quaternion_t6_51_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t6_51  (*Func)(void* obj, Quaternion_t6_51  p1, Quaternion_t6_51  p2, float p3, const MethodInfo* method);
	Quaternion_t6_51  ret = ((Func)method->method)(obj, *((Quaternion_t6_51 *)args[0]), *((Quaternion_t6_51 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t6_51_QuaternionU26_t6_307_QuaternionU26_t6_307_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t6_51  (*Func)(void* obj, Quaternion_t6_51 * p1, Quaternion_t6_51 * p2, float p3, const MethodInfo* method);
	Quaternion_t6_51  ret = ((Func)method->method)(obj, (Quaternion_t6_51 *)args[0], (Quaternion_t6_51 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t6_51_Quaternion_t6_51 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t6_51  (*Func)(void* obj, Quaternion_t6_51  p1, const MethodInfo* method);
	Quaternion_t6_51  ret = ((Func)method->method)(obj, *((Quaternion_t6_51 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t6_51_QuaternionU26_t6_307 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t6_51  (*Func)(void* obj, Quaternion_t6_51 * p1, const MethodInfo* method);
	Quaternion_t6_51  ret = ((Func)method->method)(obj, (Quaternion_t6_51 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t6_51_Single_t1_17_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t6_51  (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Quaternion_t6_51  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_Quaternion_t6_51 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Quaternion_t6_51  p1, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, *((Quaternion_t6_51 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_QuaternionU26_t6_307 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Quaternion_t6_51 * p1, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, (Quaternion_t6_51 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t6_51_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t6_51  (*Func)(void* obj, Vector3_t6_49 * p1, const MethodInfo* method);
	Quaternion_t6_51  ret = ((Func)method->method)(obj, (Vector3_t6_49 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t6_51_Quaternion_t6_51_Quaternion_t6_51 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t6_51  (*Func)(void* obj, Quaternion_t6_51  p1, Quaternion_t6_51  p2, const MethodInfo* method);
	Quaternion_t6_51  ret = ((Func)method->method)(obj, *((Quaternion_t6_51 *)args[0]), *((Quaternion_t6_51 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_Quaternion_t6_51_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Quaternion_t6_51  p1, Vector3_t6_49  p2, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, *((Quaternion_t6_51 *)args[0]), *((Vector3_t6_49 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Quaternion_t6_51_Quaternion_t6_51 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t6_51  p1, Quaternion_t6_51  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t6_51 *)args[0]), *((Quaternion_t6_51 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t6_52  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Rect_t6_52  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Vector2_t6_48 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t6_48  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t6_49  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Rect_t6_52_Rect_t6_52 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t6_52  p1, Rect_t6_52  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((Rect_t6_52 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t6_53_Matrix4x4_t6_53 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t6_53  (*Func)(void* obj, Matrix4x4_t6_53  p1, const MethodInfo* method);
	Matrix4x4_t6_53  ret = ((Func)method->method)(obj, *((Matrix4x4_t6_53 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t6_53_Matrix4x4U26_t6_308 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t6_53  (*Func)(void* obj, Matrix4x4_t6_53 * p1, const MethodInfo* method);
	Matrix4x4_t6_53  ret = ((Func)method->method)(obj, (Matrix4x4_t6_53 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Matrix4x4_t6_53_Matrix4x4U26_t6_308 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t6_53  p1, Matrix4x4_t6_53 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t6_53 *)args[0]), (Matrix4x4_t6_53 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Matrix4x4U26_t6_308_Matrix4x4U26_t6_308 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t6_53 * p1, Matrix4x4_t6_53 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t6_53 *)args[0], (Matrix4x4_t6_53 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t6_53 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t6_53  (*Func)(void* obj, const MethodInfo* method);
	Matrix4x4_t6_53  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t6_55_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t6_55  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector4_t6_55  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Vector4_t6_55 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t6_55  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t6_55 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t6_53_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t6_53  (*Func)(void* obj, Vector3_t6_49  p1, const MethodInfo* method);
	Matrix4x4_t6_53  ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Vector3_t6_49_Quaternion_t6_51_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49  p1, Quaternion_t6_51  p2, Vector3_t6_49  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Quaternion_t6_51 *)args[1]), *((Vector3_t6_49 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t6_53_Vector3_t6_49_Quaternion_t6_51_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t6_53  (*Func)(void* obj, Vector3_t6_49  p1, Quaternion_t6_51  p2, Vector3_t6_49  p3, const MethodInfo* method);
	Matrix4x4_t6_53  ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Quaternion_t6_51 *)args[1]), *((Vector3_t6_49 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t6_53_Vector3U26_t6_304_QuaternionU26_t6_307_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t6_53  (*Func)(void* obj, Vector3_t6_49 * p1, Quaternion_t6_51 * p2, Vector3_t6_49 * p3, const MethodInfo* method);
	Matrix4x4_t6_53  ret = ((Func)method->method)(obj, (Vector3_t6_49 *)args[0], (Quaternion_t6_51 *)args[1], (Vector3_t6_49 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t6_53_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t6_53  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, const MethodInfo* method);
	Matrix4x4_t6_53  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t6_53_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t6_53  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Matrix4x4_t6_53  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t6_53_Matrix4x4_t6_53_Matrix4x4_t6_53 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t6_53  (*Func)(void* obj, Matrix4x4_t6_53  p1, Matrix4x4_t6_53  p2, const MethodInfo* method);
	Matrix4x4_t6_53  ret = ((Func)method->method)(obj, *((Matrix4x4_t6_53 *)args[0]), *((Matrix4x4_t6_53 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t6_55_Matrix4x4_t6_53_Vector4_t6_55 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t6_55  (*Func)(void* obj, Matrix4x4_t6_53  p1, Vector4_t6_55  p2, const MethodInfo* method);
	Vector4_t6_55  ret = ((Func)method->method)(obj, *((Matrix4x4_t6_53 *)args[0]), *((Vector4_t6_55 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Matrix4x4_t6_53_Matrix4x4_t6_53 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t6_53  p1, Matrix4x4_t6_53  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t6_53 *)args[0]), *((Matrix4x4_t6_53 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Bounds_t6_54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t6_54  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t6_54 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Bounds_t6_54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t6_54  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t6_54 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Bounds_t6_54_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t6_54  p1, Vector3_t6_49  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t6_54 *)args[0]), *((Vector3_t6_49 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_BoundsU26_t6_309_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t6_54 * p1, Vector3_t6_49 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t6_54 *)args[0], (Vector3_t6_49 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Bounds_t6_54_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t6_54  p1, Vector3_t6_49  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t6_54 *)args[0]), *((Vector3_t6_49 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_BoundsU26_t6_309_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t6_54 * p1, Vector3_t6_49 * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t6_54 *)args[0], (Vector3_t6_49 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_RayU26_t6_310_BoundsU26_t6_309_SingleU26_t1_1726 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t6_56 * p1, Bounds_t6_54 * p2, float* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t6_56 *)args[0], (Bounds_t6_54 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Ray_t6_56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t6_56  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t6_56 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Ray_t6_56_SingleU26_t1_1726 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t6_56  p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t6_56 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_BoundsU26_t6_309_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Bounds_t6_54 * p1, Vector3_t6_49 * p2, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, (Bounds_t6_54 *)args[0], (Vector3_t6_49 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Bounds_t6_54_Bounds_t6_54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t6_54  p1, Bounds_t6_54  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t6_54 *)args[0]), *((Bounds_t6_54 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Vector4_t6_55_Vector4_t6_55 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t6_55  p1, Vector4_t6_55  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t6_55 *)args[0]), *((Vector4_t6_55 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Vector4_t6_55 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t6_55  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t6_55 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t6_55_Vector4_t6_55_Vector4_t6_55 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t6_55  (*Func)(void* obj, Vector4_t6_55  p1, Vector4_t6_55  p2, const MethodInfo* method);
	Vector4_t6_55  ret = ((Func)method->method)(obj, *((Vector4_t6_55 *)args[0]), *((Vector4_t6_55 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Vector4_t6_55_Vector4_t6_55 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t6_55  p1, Vector4_t6_55  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t6_55 *)args[0]), *((Vector4_t6_55 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, float p1, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17_SingleU26_t1_1726_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17_SingleU26_t1_1726_Single_t1_17_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, float p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17_SingleU26_t1_1726_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Color_t6_40 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t6_40  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t6_40 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Color_t6_40 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color_t6_40  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Color_t6_40 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_ColorU26_t6_306 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Color_t6_40 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Color_t6_40 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SphericalHarmonicsL2U26_t6_311 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SphericalHarmonicsL2_t6_68 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (SphericalHarmonicsL2_t6_68 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Color_t6_40_SphericalHarmonicsL2U26_t6_311 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6_40  p1, SphericalHarmonicsL2_t6_68 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t6_40 *)args[0]), (SphericalHarmonicsL2_t6_68 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_ColorU26_t6_306_SphericalHarmonicsL2U26_t6_311 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6_40 * p1, SphericalHarmonicsL2_t6_68 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t6_40 *)args[0], (SphericalHarmonicsL2_t6_68 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Vector3_t6_49_Color_t6_40_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49  p1, Color_t6_40  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Color_t6_40 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Vector3_t6_49_Color_t6_40_SphericalHarmonicsL2U26_t6_311 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49  p1, Color_t6_40  p2, SphericalHarmonicsL2_t6_68 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Color_t6_40 *)args[1]), (SphericalHarmonicsL2_t6_68 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Vector3U26_t6_304_ColorU26_t6_306_SphericalHarmonicsL2U26_t6_311 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49 * p1, Color_t6_40 * p2, SphericalHarmonicsL2_t6_68 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t6_49 *)args[0], (Color_t6_40 *)args[1], (SphericalHarmonicsL2_t6_68 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_SphericalHarmonicsL2_t6_68_SphericalHarmonicsL2_t6_68_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t6_68  (*Func)(void* obj, SphericalHarmonicsL2_t6_68  p1, float p2, const MethodInfo* method);
	SphericalHarmonicsL2_t6_68  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t6_68 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SphericalHarmonicsL2_t6_68_Single_t1_17_SphericalHarmonicsL2_t6_68 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t6_68  (*Func)(void* obj, float p1, SphericalHarmonicsL2_t6_68  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t6_68  ret = ((Func)method->method)(obj, *((float*)args[0]), *((SphericalHarmonicsL2_t6_68 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SphericalHarmonicsL2_t6_68_SphericalHarmonicsL2_t6_68_SphericalHarmonicsL2_t6_68 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t6_68  (*Func)(void* obj, SphericalHarmonicsL2_t6_68  p1, SphericalHarmonicsL2_t6_68  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t6_68  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t6_68 *)args[0]), *((SphericalHarmonicsL2_t6_68 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_SphericalHarmonicsL2_t6_68_SphericalHarmonicsL2_t6_68 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SphericalHarmonicsL2_t6_68  p1, SphericalHarmonicsL2_t6_68  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t6_68 *)args[0]), *((SphericalHarmonicsL2_t6_68 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_RuntimePlatform_t6_8 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Rect_t6_52 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t6_52  (*Func)(void* obj, const MethodInfo* method);
	Rect_t6_52  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_RectU26_t6_312 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t6_52 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_CameraClearFlags_t6_209 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t6_56_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t6_56  (*Func)(void* obj, Vector3_t6_49  p1, const MethodInfo* method);
	Ray_t6_56  ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t6_56_Object_t_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t6_56  (*Func)(void* obj, Object_t * p1, Vector3_t6_49 * p2, const MethodInfo* method);
	Ray_t6_56  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6_49 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Ray_t6_56_Single_t1_17_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t6_56  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t6_56 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_RayU26_t6_310_Single_t1_17_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t6_56 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t6_56 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_RayU26_t6_310_Single_t1_17_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t6_56 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t6_56 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Vector3_t6_49_Vector3_t6_49_Color_t6_40_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49  p1, Vector3_t6_49  p2, Color_t6_40  p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Vector3_t6_49 *)args[1]), *((Color_t6_40 *)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Vector3U26_t6_304_Vector3U26_t6_304_ColorU26_t6_306_Single_t1_17_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49 * p1, Vector3_t6_49 * p2, Color_t6_40 * p3, float p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t6_49 *)args[0], (Vector3_t6_49 *)args[1], (Color_t6_40 *)args[2], *((float*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_RenderBuffer_t6_208 (const MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t6_208  (*Func)(void* obj, const MethodInfo* method);
	RenderBuffer_t6_208  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Int32U26_t1_1678_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_RenderBufferU26_t6_313_RenderBufferU26_t6_313 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t6_208 * p2, RenderBuffer_t6_208 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t6_208 *)args[1], (RenderBuffer_t6_208 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32U26_t1_1678_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_TouchPhase_t6_81 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t6_49 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Touch_t6_82_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t6_82  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Touch_t6_82  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Vector2_t6_48 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t6_48  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Vector2U26_t6_314 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t6_48 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t6_48 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Vector3_t6_49_Quaternion_t6_51 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t6_49  p2, Quaternion_t6_51  p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t6_49 *)args[1]), *((Quaternion_t6_51 *)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t6_304_QuaternionU26_t6_307 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t6_49 * p2, Quaternion_t6_51 * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6_49 *)args[1], (Quaternion_t6_51 *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Quaternion_t6_51 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t6_51  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t6_51 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_QuaternionU26_t6_307 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t6_51 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t6_51 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Vector3_t6_49_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Single_t1_17_Single_t1_17_Single_t1_17_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t6_49  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t6_49 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Vector3U26_t6_304_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t6_49 * p2, Vector3_t6_49 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6_49 *)args[1], (Vector3_t6_49 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t6_49_Object_t_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, Object_t * p1, Vector3_t6_49 * p2, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6_49 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Vector3_t6_49_Vector3_t6_49_RaycastHitU26_t6_315_Single_t1_17_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t6_49  p1, Vector3_t6_49  p2, RaycastHit_t6_108 * p3, float p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Vector3_t6_49 *)args[1]), (RaycastHit_t6_108 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Ray_t6_56_RaycastHitU26_t6_315_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t6_56  p1, RaycastHit_t6_108 * p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t6_56 *)args[0]), (RaycastHit_t6_108 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Ray_t6_56_RaycastHitU26_t6_315_Single_t1_17_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t6_56  p1, RaycastHit_t6_108 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t6_56 *)args[0]), (RaycastHit_t6_108 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Vector3U26_t6_304_Vector3U26_t6_304_RaycastHitU26_t6_315_Single_t1_17_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t6_49 * p1, Vector3_t6_49 * p2, RaycastHit_t6_108 * p3, float p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector3_t6_49 *)args[0], (Vector3_t6_49 *)args[1], (RaycastHit_t6_108 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Vector3U26_t6_304_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t6_49 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6_49 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Bounds_t6_54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Bounds_t6_54  (*Func)(void* obj, const MethodInfo* method);
	Bounds_t6_54  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_BoundsU26_t6_309 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t6_54 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Bounds_t6_54 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_CollisionFlags_t6_105_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t6_49  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CollisionFlags_t6_105_Object_t_Vector3U26_t6_304 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector3_t6_49 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t6_49 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Vector2_t6_48_Vector2_t6_48_Single_t1_17_Int32_t1_3_Single_t1_17_Single_t1_17_RaycastHit2DU26_t6_316 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t6_48  p1, Vector2_t6_48  p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t6_110 * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), *((Vector2_t6_48 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t6_110 *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Vector2U26_t6_314_Vector2U26_t6_314_Single_t1_17_Int32_t1_3_Single_t1_17_Single_t1_17_RaycastHit2DU26_t6_316 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t6_48 * p1, Vector2_t6_48 * p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t6_110 * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t6_48 *)args[0], (Vector2_t6_48 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t6_110 *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit2D_t6_110_Vector2_t6_48_Vector2_t6_48_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t6_110  (*Func)(void* obj, Vector2_t6_48  p1, Vector2_t6_48  p2, float p3, const MethodInfo* method);
	RaycastHit2D_t6_110  ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), *((Vector2_t6_48 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit2D_t6_110_Vector2_t6_48_Vector2_t6_48_Single_t1_17_Int32_t1_3_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t6_110  (*Func)(void* obj, Vector2_t6_48  p1, Vector2_t6_48  p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	RaycastHit2D_t6_110  ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), *((Vector2_t6_48 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Vector2U26_t6_314_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t6_48 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t6_48 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_SByte_t1_12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_SendMessageOptions_t6_6 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AnimatorStateInfo_t6_128 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t6_128  (*Func)(void* obj, const MethodInfo* method);
	AnimatorStateInfo_t6_128  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AnimatorClipInfo_t6_129 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorClipInfo_t6_129  (*Func)(void* obj, const MethodInfo* method);
	AnimatorClipInfo_t6_129  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Single_t1_17_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Single_t1_17_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_AnimatorStateInfo_t6_128_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t6_128  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	AnimatorStateInfo_t6_128  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int16_t1_13_CharacterInfoU26_t6_317_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t6_146 * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t6_146 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int16_t1_13_CharacterInfoU26_t6_317_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t6_146 * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t6_146 *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int16_t1_13_CharacterInfoU26_t6_317 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t6_146 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t6_146 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Color_t6_40_Int32_t1_3_Single_t1_17_Single_t1_17_Int32_t1_3_SByte_t1_12_SByte_t1_12_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3_Vector2_t6_48_Vector2_t6_48_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t6_40  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, Vector2_t6_48  p16, Vector2_t6_48  p17, int8_t p18, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t6_40 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((Vector2_t6_48 *)args[15]), *((Vector2_t6_48 *)args[16]), *((int8_t*)args[17]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Color_t6_40_Int32_t1_3_Single_t1_17_Single_t1_17_Int32_t1_3_SByte_t1_12_SByte_t1_12_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t6_40  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t6_40 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t_ColorU26_t6_306_Int32_t1_3_Single_t1_17_Single_t1_17_Int32_t1_3_SByte_t1_12_SByte_t1_12_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Color_t6_40 * p4, int32_t p5, float p6, float p7, int32_t p8, int8_t p9, int8_t p10, int32_t p11, int32_t p12, int32_t p13, int32_t p14, int8_t p15, int32_t p16, float p17, float p18, float p19, float p20, int8_t p21, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Color_t6_40 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((float*)args[6]), *((int32_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int32_t*)args[13]), *((int8_t*)args[14]), *((int32_t*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((float*)args[19]), *((int8_t*)args[20]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextGenerationSettings_t6_152_TextGenerationSettings_t6_152 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t6_152  (*Func)(void* obj, TextGenerationSettings_t6_152  p1, const MethodInfo* method);
	TextGenerationSettings_t6_152  ret = ((Func)method->method)(obj, *((TextGenerationSettings_t6_152 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Object_t_TextGenerationSettings_t6_152 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t6_152  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t6_152 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_TextGenerationSettings_t6_152 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t6_152  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t6_152 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t6_56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t6_56  (*Func)(void* obj, const MethodInfo* method);
	Ray_t6_56  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Ray_t6_56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Ray_t6_56  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Ray_t6_56 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_EventType_t6_156 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EventType_t6_156_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Vector2U26_t6_314 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t6_48 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t6_48 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_EventModifiers_t6_157 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyCode_t6_155 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Rect_t6_52_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Rect_t6_52_Object_t_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, int32_t p2, Object_t * p3, int8_t p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, int32_t p2, Object_t * p3, int8_t p4, int32_t p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_Object_t_Object_t_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, int32_t p2, Object_t * p3, int8_t p4, int32_t p5, Object_t * p6, Object_t * p7, int16_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], *((int16_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_Object_t_Object_t_Int16_t1_13_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, int32_t p2, Object_t * p3, int8_t p4, int32_t p5, Object_t * p6, Object_t * p7, int16_t p8, Object_t * p9, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], *((int16_t*)args[7]), (Object_t *)args[8], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Rect_t6_52_SByte_t1_12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t6_52  p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Rect_t6_52_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Rect_t6_52  p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_GUIStyleU26_t6_318_GUIStyleU26_t6_318_GUIStyleU26_t6_318_GUIStyleU26_t6_318_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GUIStyle_t6_166 ** p1, GUIStyle_t6_166 ** p2, GUIStyle_t6_166 ** p3, GUIStyle_t6_166 ** p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (GUIStyle_t6_166 **)args[0], (GUIStyle_t6_166 **)args[1], (GUIStyle_t6_166 **)args[2], (GUIStyle_t6_166 **)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Rect_t6_52_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Rect_t6_52  p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Rect_t6_52_Int32_t1_3_Int32_t1_3_Single_t1_17_Single_t1_17_Object_t_Object_t_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Rect_t6_52  p1, int32_t p2, int32_t p3, float p4, float p5, Object_t * p6, Object_t * p7, Object_t * p8, Object_t * p9, int8_t p10, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((float*)args[3]), *((float*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], (Object_t *)args[8], *((int8_t*)args[9]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Vector2_t6_48_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector2_t6_48  p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t6_48 *)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Rect_t6_52  p1, float p2, float p3, float p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t_SByte_t1_12_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Rect_t6_52  p1, float p2, float p3, float p4, float p5, Object_t * p6, Object_t * p7, int8_t p8, int32_t p9, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), (Object_t *)args[5], (Object_t *)args[6], *((int8_t*)args[7]), *((int32_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Rect_t6_52  p1, float p2, float p3, float p4, float p5, Object_t * p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), (Object_t *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Rect_t6_52_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Rect_t6_52  p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t6_52 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Rect_t6_52  p1, float p2, float p3, float p4, float p5, Object_t * p6, Object_t * p7, Object_t * p8, Object_t * p9, int8_t p10, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], (Object_t *)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t6_48_Rect_t6_52_Vector2_t6_48_Rect_t6_52_SByte_t1_12_SByte_t1_12_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6_48  (*Func)(void* obj, Rect_t6_52  p1, Vector2_t6_48  p2, Rect_t6_52  p3, int8_t p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Vector2_t6_48  ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((Vector2_t6_48 *)args[1]), *((Rect_t6_52 *)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t6_52_Int32_t1_3_Rect_t6_52_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t6_52  (*Func)(void* obj, int32_t p1, Rect_t6_52  p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Rect_t6_52  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t6_52 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Single_t1_17_Single_t1_17_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, float p5, float p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_RectU26_t6_312_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t6_52 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Rect_t6_52_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_RectU26_t6_312_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t6_52 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t6_52 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Rect_t6_52_Int32_t1_3_SByte_t1_12_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t6_52  p1, int32_t p2, int8_t p3, Object_t * p4, IntPtr_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], *((IntPtr_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_RectU26_t6_312_Int32_t1_3_SByte_t1_12_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t6_52 * p1, int32_t p2, int8_t p3, Object_t * p4, IntPtr_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t6_52 *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], *((IntPtr_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t6_52_Int32_t1_3_Rect_t6_52_Object_t_Object_t_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t6_52  (*Func)(void* obj, int32_t p1, Rect_t6_52  p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	Rect_t6_52  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t6_52 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t6_52_Int32_t1_3_RectU26_t6_312_Object_t_Object_t_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t6_52  (*Func)(void* obj, int32_t p1, Rect_t6_52 * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	Rect_t6_52  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t6_52 *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t1_3_SByte_t1_12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, Object_t * p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t6_48_Vector2_t6_48_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6_48  (*Func)(void* obj, Vector2_t6_48  p1, Object_t * p2, const MethodInfo* method);
	Vector2_t6_48  ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t6_48_Vector2_t6_48_SByte_t1_12_SByte_t1_12_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6_48  (*Func)(void* obj, Vector2_t6_48  p1, int8_t p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	Vector2_t6_48  ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t6_52_Int32_t1_3_Rect_t6_52_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t6_52  (*Func)(void* obj, int32_t p1, Rect_t6_52  p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Rect_t6_52  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t6_52 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Rect_t6_52_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Rect_t6_52  p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Rect_t6_52 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Rect_t6_52_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t6_52  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Rect_t6_52  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t6_52_Single_t1_17_Single_t1_17_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t6_52  (*Func)(void* obj, float p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Rect_t6_52  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t6_52  (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Rect_t6_52  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t6_52_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t6_52  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Rect_t6_52  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Rect_t6_52 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t6_52  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t6_52 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_RectU26_t6_312 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t6_52 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t6_52 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Rect_t6_52_Rect_t6_52 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t6_52  (*Func)(void* obj, Rect_t6_52  p1, const MethodInfo* method);
	Rect_t6_52  ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t6_52_Object_t_RectU26_t6_312 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t6_52  (*Func)(void* obj, Object_t * p1, Rect_t6_52 * p2, const MethodInfo* method);
	Rect_t6_52  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Rect_t6_52 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Rect_t6_52_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t6_52  p2, Object_t * p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t6_52 *)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t6_48_Rect_t6_52_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6_48  (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Vector2_t6_48  ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Rect_t6_52_Object_t_Vector2_t6_48 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Rect_t6_52  p1, Object_t * p2, Vector2_t6_48  p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), (Object_t *)args[1], *((Vector2_t6_48 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t6_48_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6_48  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector2_t6_48  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_Object_t_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_SingleU26_t1_1726_SingleU26_t1_1726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float* p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (float*)args[1], (float*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ImagePosition_t6_182 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t1_17_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Internal_DrawArgumentsU26_t6_319 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Internal_DrawArguments_t6_187 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Internal_DrawArguments_t6_187 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Rect_t6_52_Object_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t6_52  p2, Object_t * p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t6_52 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_RectU26_t6_312_Object_t_Int32_t1_3_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t6_52 * p2, Object_t * p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t6_52 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Rect_t6_52_Object_t_Int32_t1_3_Color_t6_40 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t6_52  p2, Object_t * p3, int32_t p4, Color_t6_40  p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t6_52 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((Color_t6_40 *)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_RectU26_t6_312_Object_t_Int32_t1_3_ColorU26_t6_306 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t6_52 * p2, Object_t * p3, int32_t p4, Color_t6_40 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t6_52 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Color_t6_40 *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Internal_DrawWithTextSelectionArgumentsU26_t6_320 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Internal_DrawWithTextSelectionArguments_t6_188 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Internal_DrawWithTextSelectionArguments_t6_188 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Rect_t6_52_Object_t_Int32_t1_3_Vector2U26_t6_314 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t6_52  p2, Object_t * p3, int32_t p4, Vector2_t6_48 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t6_52 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t6_48 *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_RectU26_t6_312_Object_t_Int32_t1_3_Vector2U26_t6_314 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t6_52 * p2, Object_t * p3, int32_t p4, Vector2_t6_48 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t6_52 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t6_48 *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_IntPtr_t_Rect_t6_52_Object_t_Vector2_t6_48 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Rect_t6_52  p2, Object_t * p3, Vector2_t6_48  p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t6_52 *)args[1]), (Object_t *)args[2], *((Vector2_t6_48 *)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_IntPtr_t_RectU26_t6_312_Object_t_Vector2U26_t6_314 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Rect_t6_52 * p2, Object_t * p3, Vector2_t6_48 * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t6_52 *)args[1], (Object_t *)args[2], (Vector2_t6_48 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Object_t_Vector2U26_t6_314 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Vector2_t6_48 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Vector2_t6_48 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Single_t1_17_IntPtr_t_Object_t_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_IntPtr_t_Object_t_SingleU26_t1_1726_SingleU26_t1_1726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float* p3, float* p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (float*)args[2], (float*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_Rect_t6_52 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Rect_t6_52  p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((Rect_t6_52 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_RectU26_t6_312 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Rect_t6_52 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Rect_t6_52 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Vector2_t6_48_Vector2_t6_48_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, Vector2_t6_48  p2, Vector2_t6_48  p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((Vector2_t6_48 *)args[1]), *((Vector2_t6_48 *)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_RectU26_t6_312_Vector2U26_t6_314_Vector2U26_t6_314_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52 * p1, Vector2_t6_48 * p2, Vector2_t6_48 * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t6_52 *)args[0], (Vector2_t6_48 *)args[1], (Vector2_t6_48 *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_UserState_t6_225 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Double_t1_18_SByte_t1_12_SByte_t1_12_DateTime_t1_127 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t1_127  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t1_127 *)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1_12_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_Object_t_DateTime_t1_127_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t1_127  p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t1_127 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_UserScope_t6_226 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Range_t6_219 (const MethodInfo* method, void* obj, void** args)
{
	typedef Range_t6_219  (*Func)(void* obj, const MethodInfo* method);
	Range_t6_219  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Range_t6_219 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t6_219  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Range_t6_219 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_TimeScope_t6_227 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_HitInfo_t6_221 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t6_221  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t6_221 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_HitInfo_t6_221_HitInfo_t6_221 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t6_221  p1, HitInfo_t6_221  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t6_221 *)args[0]), *((HitInfo_t6_221 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_HitInfo_t6_221 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t6_221  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t6_221 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t_SByte_t1_12_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t6_52  p1, float p2, float p3, float p4, float p5, Object_t * p6, Object_t * p7, int8_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), (Object_t *)args[5], (Object_t *)args[6], *((int8_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_StringU26_t1_1690_StringU26_t1_1690 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_CharacterType_t6_236_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Color_t6_40_Color_t6_40 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t6_40  p1, Color_t6_40  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t6_40 *)args[0]), *((Color_t6_40 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_TextGenerationSettings_t6_152 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TextGenerationSettings_t6_152  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TextGenerationSettings_t6_152 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ChatState_t7_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ChatDisconnectCause_t7_6 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_ChatChannelU26_t7_16 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, ChatChannel_t7_1 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (ChatChannel_t7_1 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_ChatChannelU26_t7_16 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, ChatChannel_t7_1 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (ChatChannel_t7_1 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAuthenticationType_t7_10 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Vector2_t6_48 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t6_48  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Color_t6_40_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t6_40  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Color_t6_40  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Vector3_t6_49_Vector3_t6_49_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t6_49  p1, Vector3_t6_49  p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((Vector3_t6_49 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Vector2_t6_48_Vector2_t6_48_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t6_48  p1, Vector2_t6_48  p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t6_48 *)args[0]), *((Vector2_t6_48 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Quaternion_t6_51_Quaternion_t6_51_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t6_51  p1, Quaternion_t6_51  p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t6_51 *)args[0]), *((Quaternion_t6_51 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Single_t1_17_Single_t1_17_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Vector3_t6_49_Int32_t1_3_Color_t6_40_Single_t1_17 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49  p1, int32_t p2, Color_t6_40  p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), *((int32_t*)args[1]), *((Color_t6_40 *)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAuthenticationType_t8_96 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ServerConnection_t8_67 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PeerState_t8_69 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CloudRegionCode_t8_65 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Vector3_t6_49_Quaternion_t6_51_Int32_t1_3_Object_t_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t6_49  p2, Quaternion_t6_51  p3, int32_t p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t6_49 *)args[1]), *((Quaternion_t6_51 *)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_Int16_t1_13 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int16_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_MethodInfoU26_t1_1728 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo_t ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MethodInfo_t **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_CharU26_t1_1716 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t* p1, const MethodInfo* method);
	((Func)method->method)(obj, (uint16_t*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int16U26_t1_1685 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t* p1, const MethodInfo* method);
	((Func)method->method)(obj, (int16_t*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SingleU26_t1_1726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_PhotonPlayerU26_t8_191 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PhotonPlayer_t8_102 ** p1, const MethodInfo* method);
	((Func)method->method)(obj, (PhotonPlayer_t8_102 **)args[0], method);
	return NULL;
}

void* RuntimeInvoker_ConnectionState_t8_68 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_Byte_t1_11_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((uint8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Vector3_t6_49_Quaternion_t6_51_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t6_49  p2, Quaternion_t6_51  p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t6_49 *)args[1]), *((Quaternion_t6_51 *)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Vector3_t6_49_Quaternion_t6_51_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t6_49  p2, Quaternion_t6_51  p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t6_49 *)args[1]), *((Quaternion_t6_51 *)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Void_t1_29_SByte_t1_12_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_SByte_t1_12_Object_t_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_CloudRegionCode_t8_65_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CloudRegionFlag_t8_66_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SynchronizeType_t8_127_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SynchronizeType_t8_127_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Vector3_t6_49_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t6_49  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Quaternion_t6_51_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t6_51  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t6_51 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Double_t1_18_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Team_t8_169_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_ObjectU26_t1_1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_ObjectU5BU5DU26_t1_1676_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t1_157** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t1_157**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_ObjectU5BU5DU26_t1_1676_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t1_157** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t1_157**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1_1044  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1_1044 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1_1044  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1044 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1044_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1044  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t1_1044  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_ObjectU26_t1_1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1046 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1046  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1046  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1_167_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1_167  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1_167  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1044  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1_1044  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_894 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_894  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_894  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1049 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1049  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1049  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_975 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_975  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_975  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2_23 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2_23  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2_23  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3_193 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3_193  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3_193  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3_195 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3_195  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3_195  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3_197 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3_197  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3_197  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1180 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1180  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1180  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1162 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1162  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1162  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_903 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_903  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_903  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_902 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_902  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1_902  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1314 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1314  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1314  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Team_t8_169 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1166 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1166  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1166  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1163 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1163  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1163  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1159 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1159  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1_1159  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ParameterModifier_t1_352_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1_352  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParameterModifier_t1_352  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_ParameterModifier_t1_352 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t1_352  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t1_352 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_ParameterModifier_t1_352 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t1_352  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t1_352 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_ParameterModifier_t1_352 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t1_352  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t1_352 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_ParameterModifier_t1_352 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t1_352  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t1_352 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Double_t1_18 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TableRange_t1_66_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1_66  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TableRange_t1_66  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_TableRange_t1_66 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t1_66  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t1_66 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_TableRange_t1_66 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t1_66  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t1_66 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_TableRange_t1_66 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t1_66  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t1_66 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_TableRange_t1_66 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t1_66  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t1_66 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t1_1016_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1016  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1_1016  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1016 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1_1016  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1_1016 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1016 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1_1016  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1016 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1016 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1_1016  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1016 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1016 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1_1016  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1_1016 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Link_t1_150_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1_150  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t1_150  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Link_t1_150 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t1_150  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t1_150 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Link_t1_150 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t1_150  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t1_150 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Link_t1_150 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t1_150  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t1_150 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Link_t1_150 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t1_150  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t1_150 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DictionaryEntry_t1_167_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1_167  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DictionaryEntry_t1_167  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_DictionaryEntry_t1_167 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t1_167  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t1_167 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_DictionaryEntry_t1_167 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t1_167  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t1_167 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_DictionaryEntry_t1_167 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t1_167  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t1_167 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_DictionaryEntry_t1_167 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t1_167  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t1_167 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t1_1044_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1044  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1_1044  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1_1044  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1044 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1_1044  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1_1044 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t1_168_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1_168  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1_168  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Slot_t1_168 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1_168  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1_168 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Slot_t1_168 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1_168  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1_168 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Slot_t1_168 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1_168  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1_168 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Slot_t1_168 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1_168  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1_168 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t1_183_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1_183  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1_183  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Slot_t1_183 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1_183  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1_183 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Slot_t1_183 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1_183  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1_183 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Slot_t1_183 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1_183  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1_183 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Slot_t1_183 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1_183  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1_183 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ILTokenInfo_t1_283_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1_283  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ILTokenInfo_t1_283  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_ILTokenInfo_t1_283 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ILTokenInfo_t1_283  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ILTokenInfo_t1_283 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_ILTokenInfo_t1_283 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ILTokenInfo_t1_283  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ILTokenInfo_t1_283 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_ILTokenInfo_t1_283 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ILTokenInfo_t1_283  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ILTokenInfo_t1_283 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_ILTokenInfo_t1_283 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ILTokenInfo_t1_283  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ILTokenInfo_t1_283 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelData_t1_285_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1_285  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelData_t1_285  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_LabelData_t1_285 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelData_t1_285  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelData_t1_285 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_LabelData_t1_285 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelData_t1_285  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelData_t1_285 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_LabelData_t1_285 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelData_t1_285  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelData_t1_285 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_LabelData_t1_285 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelData_t1_285  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelData_t1_285 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelFixup_t1_284_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1_284  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelFixup_t1_284  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_LabelFixup_t1_284 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelFixup_t1_284  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelFixup_t1_284 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_LabelFixup_t1_284 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelFixup_t1_284  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelFixup_t1_284 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_LabelFixup_t1_284 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelFixup_t1_284  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelFixup_t1_284 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_LabelFixup_t1_284 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelFixup_t1_284  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelFixup_t1_284 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t1_332_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t1_332  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CustomAttributeTypedArgument_t1_332  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_CustomAttributeTypedArgument_t1_332 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgument_t1_332  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1_332 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_CustomAttributeTypedArgument_t1_332 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t1_332  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1_332 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_CustomAttributeTypedArgument_t1_332 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t1_332  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1_332 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_CustomAttributeTypedArgument_t1_332 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeTypedArgument_t1_332  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CustomAttributeTypedArgument_t1_332 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t1_331_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t1_331  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CustomAttributeNamedArgument_t1_331  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_CustomAttributeNamedArgument_t1_331 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgument_t1_331  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1_331 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_CustomAttributeNamedArgument_t1_331 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t1_331  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1_331 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_CustomAttributeNamedArgument_t1_331 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t1_331  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1_331 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_CustomAttributeNamedArgument_t1_331 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeNamedArgument_t1_331  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CustomAttributeNamedArgument_t1_331 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_CustomAttributeTypedArgumentU5BU5DU26_t1_1729_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t1_838** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeTypedArgumentU5BU5D_t1_838**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_CustomAttributeTypedArgumentU5BU5DU26_t1_1729_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t1_838** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeTypedArgumentU5BU5D_t1_838**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_CustomAttributeTypedArgument_t1_332_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeTypedArgument_t1_332  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeTypedArgument_t1_332 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_CustomAttributeTypedArgument_t1_332 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeTypedArgument_t1_332  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeTypedArgument_t1_332 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_CustomAttributeNamedArgumentU5BU5DU26_t1_1730_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t1_839** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeNamedArgumentU5BU5D_t1_839**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_CustomAttributeNamedArgumentU5BU5DU26_t1_1730_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t1_839** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeNamedArgumentU5BU5D_t1_839**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_CustomAttributeNamedArgument_t1_331_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeNamedArgument_t1_331  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeNamedArgument_t1_331 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Object_t_CustomAttributeNamedArgument_t1_331 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeNamedArgument_t1_331  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeNamedArgument_t1_331 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceInfo_t1_363_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceInfo_t1_363  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ResourceInfo_t1_363  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_ResourceInfo_t1_363 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceInfo_t1_363  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ResourceInfo_t1_363 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_ResourceInfo_t1_363 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceInfo_t1_363  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ResourceInfo_t1_363 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_ResourceInfo_t1_363 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceInfo_t1_363  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ResourceInfo_t1_363 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_ResourceInfo_t1_363 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceInfo_t1_363  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ResourceInfo_t1_363 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ResourceCacheItem_t1_364_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceCacheItem_t1_364  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ResourceCacheItem_t1_364  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_ResourceCacheItem_t1_364 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceCacheItem_t1_364  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ResourceCacheItem_t1_364 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_ResourceCacheItem_t1_364 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceCacheItem_t1_364  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ResourceCacheItem_t1_364 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_ResourceCacheItem_t1_364 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceCacheItem_t1_364  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ResourceCacheItem_t1_364 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_ResourceCacheItem_t1_364 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceCacheItem_t1_364  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ResourceCacheItem_t1_364 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_DateTime_t1_127 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t1_127  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t1_127 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t1_19  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t1_19 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Decimal_t1_19 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t1_19  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t1_19 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t1_213_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1_213  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TimeSpan_t1_213  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_TimeSpan_t1_213 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t1_213  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t1_213 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TypeTag_t1_510_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Byte_t1_11 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Byte_t1_11 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Link_t2_22_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t2_22  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t2_22  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Link_t2_22 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t2_22  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t2_22 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Link_t2_22 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t2_22  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t2_22 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Link_t2_22 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t2_22  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t2_22 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Link_t2_22 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t2_22  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t2_22 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t1_1126_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1126  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1_1126  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1126 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1_1126  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1_1126 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1126 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1_1126  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1126 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1126 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1_1126  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1126 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1126 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1_1126  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1_1126 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_X509ChainStatus_t3_87_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t3_87  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	X509ChainStatus_t3_87  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_X509ChainStatus_t3_87 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t3_87  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t3_87 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_X509ChainStatus_t3_87 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t3_87  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t3_87 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_X509ChainStatus_t3_87 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t3_87  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t3_87 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_X509ChainStatus_t3_87 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t3_87  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t3_87 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t3_134_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t3_134  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Mark_t3_134  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Mark_t3_134 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t3_134  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t3_134 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Mark_t3_134 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t3_134  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t3_134 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Mark_t3_134 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t3_134  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t3_134 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Mark_t3_134 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t3_134  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t3_134 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UriScheme_t3_169_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t3_169  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UriScheme_t3_169  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_UriScheme_t3_169 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t3_169  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t3_169 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_UriScheme_t3_169 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t3_169  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t3_169 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_UriScheme_t3_169 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t3_169  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t3_169 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_UriScheme_t3_169 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t3_169  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t3_169 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ClientCertificateType_t4_98_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1159_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1159  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1_1159  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1159 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1_1159  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1_1159 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1159 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1_1159  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1159 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1159 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1_1159  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1159 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1159 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1_1159  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1_1159 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t1_902_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_902  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1_902  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_902 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1_902  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1_902 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_902 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1_902  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_902 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_902 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1_902  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_902 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_902 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1_902  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1_902 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_GcAchievementData_t6_205_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t6_205  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcAchievementData_t6_205  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_GcAchievementData_t6_205 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t6_205  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t6_205 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_GcAchievementData_t6_205 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t6_205  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t6_205 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_GcAchievementData_t6_205 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t6_205  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t6_205 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_GcAchievementData_t6_205 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t6_205  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t6_205 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_GcScoreData_t6_206_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t6_206  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcScoreData_t6_206  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_GcScoreData_t6_206 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t6_206  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t6_206 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_GcScoreData_t6_206 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t6_206  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t6_206 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_GcScoreData_t6_206 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t6_206  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t6_206 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ContactPoint_t6_104_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t6_104  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ContactPoint_t6_104  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_ContactPoint_t6_104 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ContactPoint_t6_104  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ContactPoint_t6_104 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_ContactPoint_t6_104 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ContactPoint_t6_104  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ContactPoint_t6_104 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_ContactPoint_t6_104 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ContactPoint_t6_104  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ContactPoint_t6_104 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_ContactPoint_t6_104 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ContactPoint_t6_104  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ContactPoint_t6_104 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ContactPoint2D_t6_114_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint2D_t6_114  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ContactPoint2D_t6_114  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_ContactPoint2D_t6_114 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ContactPoint2D_t6_114  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ContactPoint2D_t6_114 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_ContactPoint2D_t6_114 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ContactPoint2D_t6_114  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ContactPoint2D_t6_114 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_ContactPoint2D_t6_114 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ContactPoint2D_t6_114  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ContactPoint2D_t6_114 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_ContactPoint2D_t6_114 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ContactPoint2D_t6_114  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ContactPoint2D_t6_114 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Keyframe_t6_131_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t6_131  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Keyframe_t6_131  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Keyframe_t6_131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t6_131  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t6_131 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Keyframe_t6_131 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t6_131  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t6_131 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Keyframe_t6_131 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t6_131  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t6_131 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Keyframe_t6_131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t6_131  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t6_131 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CharacterInfo_t6_146_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef CharacterInfo_t6_146  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CharacterInfo_t6_146  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_CharacterInfo_t6_146 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CharacterInfo_t6_146  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CharacterInfo_t6_146 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_CharacterInfo_t6_146 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CharacterInfo_t6_146  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CharacterInfo_t6_146 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_CharacterInfo_t6_146 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CharacterInfo_t6_146  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CharacterInfo_t6_146 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_CharacterInfo_t6_146 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CharacterInfo_t6_146  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CharacterInfo_t6_146 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UIVertex_t6_153_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t6_153  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIVertex_t6_153  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_UIVertex_t6_153 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t6_153  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t6_153 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_UIVertex_t6_153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t6_153  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t6_153 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_UIVertex_t6_153 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t6_153  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t6_153 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_UIVertex_t6_153 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UIVertex_t6_153  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UIVertex_t6_153 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_UIVertexU5BU5DU26_t6_321_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t6_261** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t6_261**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_UIVertexU5BU5DU26_t6_321_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t6_261** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t6_261**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_UIVertex_t6_153_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UIVertex_t6_153  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t6_153 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UICharInfo_t6_149_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t6_149  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UICharInfo_t6_149  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_UICharInfo_t6_149 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfo_t6_149  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UICharInfo_t6_149 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_UICharInfo_t6_149 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t6_149  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t6_149 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_UICharInfo_t6_149 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t6_149  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t6_149 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_UICharInfo_t6_149 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UICharInfo_t6_149  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UICharInfo_t6_149 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_UICharInfoU5BU5DU26_t6_322_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t6_262** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t6_262**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_UICharInfoU5BU5DU26_t6_322_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t6_262** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t6_262**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_UICharInfo_t6_149_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UICharInfo_t6_149  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UICharInfo_t6_149 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UILineInfo_t6_150_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t6_150  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UILineInfo_t6_150  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_UILineInfo_t6_150 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfo_t6_150  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UILineInfo_t6_150 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_UILineInfo_t6_150 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t6_150  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t6_150 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_UILineInfo_t6_150 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t6_150  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t6_150 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_UILineInfo_t6_150 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UILineInfo_t6_150  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UILineInfo_t6_150 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_UILineInfoU5BU5DU26_t6_323_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t6_263** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t6_263**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_UILineInfoU5BU5DU26_t6_323_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t6_263** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t6_263**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_UILineInfo_t6_150_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UILineInfo_t6_150  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UILineInfo_t6_150 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Rect_t6_52 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t6_52  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Rect_t6_52 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Rect_t6_52  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Rect_t6_52 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HitInfo_t6_221_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t6_221  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	HitInfo_t6_221  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_HitInfo_t6_221 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t6_221  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t6_221 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_HitInfo_t6_221 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t6_221  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t6_221 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1266_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1266  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1_1266  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1266 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1_1266  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1_1266 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1266 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1_1266  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1266 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1266 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1_1266  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1266 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1266 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1_1266  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1_1266 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TextEditOp_t6_237_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1294_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1294  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1_1294  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1294 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1_1294  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1_1294 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1294 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1_1294  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1294 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1294 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1_1294  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1294 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1294 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1_1294  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1_1294 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ConnectionProtocol_t5_36_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1317_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1317  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1_1317  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1317 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1_1317  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1_1317 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1317 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1_1317  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1317 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1317 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1_1317  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1_1317 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1317 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1_1317  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1_1317 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Team_t8_169_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_State_t8_41_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef State_t8_41  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	State_t8_41  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_State_t8_41 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, State_t8_41  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((State_t8_41 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_State_t8_41 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, State_t8_41  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((State_t8_41 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_State_t8_41 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, State_t8_41  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((State_t8_41 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_State_t8_41 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, State_t8_41  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((State_t8_41 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Color_t6_40 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t6_40  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t6_40 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Color_t6_40 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color_t6_40  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color_t6_40 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Link_t2_29_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t2_29  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t2_29  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Link_t2_29 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t2_29  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t2_29 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t1_20_Link_t2_29 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t2_29  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t2_29 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Link_t2_29 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t2_29  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t2_29 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Link_t2_29 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t2_29  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t2_29 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32U5BU5DU26_t1_1731_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t1_160** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t1_160**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_Int32U5BU5DU26_t1_1731_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t1_160** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t1_160**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_ByteU5BU5DU26_t1_1699_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ByteU5BU5D_t1_71** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ByteU5BU5D_t1_71**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_ByteU5BU5DU26_t1_1699_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ByteU5BU5D_t1_71** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ByteU5BU5D_t1_71**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t6_49_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t6_49  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector3_t6_49  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t6_49  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t6_49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Int32_t1_3_Vector3_t6_49 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector3_t6_49  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector3_t6_49 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SingleU5BU5DU26_t1_1732_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SingleU5BU5D_t1_863** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (SingleU5BU5D_t1_863**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1_29_SingleU5BU5DU26_t1_1732_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SingleU5BU5D_t1_863** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (SingleU5BU5D_t1_863**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t1_3_Object_t_Single_t1_17_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, float p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ParameterModifier_t1_352 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1_352  (*Func)(void* obj, const MethodInfo* method);
	ParameterModifier_t1_352  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TableRange_t1_66 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1_66  (*Func)(void* obj, const MethodInfo* method);
	TableRange_t1_66  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1016_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1016  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t1_1016  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1021 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1021  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1021  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1_167_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1_167  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t1_167  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1016 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1016  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1_1016  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Link_t1_150 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1_150  (*Func)(void* obj, const MethodInfo* method);
	Link_t1_150  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1020 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1020  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1020  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1024 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1024  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1024  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1_167_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1_167  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DictionaryEntry_t1_167  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1016_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1016  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1_1016  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1044_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1044  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1_1044  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t1_168 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1_168  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1_168  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t1_183 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1_183  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1_183  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ILTokenInfo_t1_283 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1_283  (*Func)(void* obj, const MethodInfo* method);
	ILTokenInfo_t1_283  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelData_t1_285 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1_285  (*Func)(void* obj, const MethodInfo* method);
	LabelData_t1_285  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelFixup_t1_284 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1_284  (*Func)(void* obj, const MethodInfo* method);
	LabelFixup_t1_284  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t1_332 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t1_332  (*Func)(void* obj, const MethodInfo* method);
	CustomAttributeTypedArgument_t1_332  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t1_331 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t1_331  (*Func)(void* obj, const MethodInfo* method);
	CustomAttributeNamedArgument_t1_331  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t1_332_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t1_332  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	CustomAttributeTypedArgument_t1_332  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1074 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1074  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1074  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_CustomAttributeTypedArgument_t1_332_CustomAttributeTypedArgument_t1_332 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t1_332  p1, CustomAttributeTypedArgument_t1_332  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1_332 *)args[0]), *((CustomAttributeTypedArgument_t1_332 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1_332_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeTypedArgument_t1_332  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1_332 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t1_331_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t1_331  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	CustomAttributeNamedArgument_t1_331  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1082 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1082  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1082  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_CustomAttributeNamedArgument_t1_331_CustomAttributeNamedArgument_t1_331 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t1_331  p1, CustomAttributeNamedArgument_t1_331  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1_331 *)args[0]), *((CustomAttributeNamedArgument_t1_331 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1_331_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeNamedArgument_t1_331  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1_331 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_ResourceInfo_t1_363 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceInfo_t1_363  (*Func)(void* obj, const MethodInfo* method);
	ResourceInfo_t1_363  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceCacheItem_t1_364 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceCacheItem_t1_364  (*Func)(void* obj, const MethodInfo* method);
	ResourceCacheItem_t1_364  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeTag_t1_510 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_DateTimeOffset_t1_707_DateTimeOffset_t1_707 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t1_707  p1, DateTimeOffset_t1_707  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t1_707 *)args[0]), *((DateTimeOffset_t1_707 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_DateTimeOffset_t1_707_DateTimeOffset_t1_707 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t1_707  p1, DateTimeOffset_t1_707  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t1_707 *)args[0]), *((DateTimeOffset_t1_707 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Nullable_1_t1_826 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t1_826  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t1_826 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Guid_t1_730_Guid_t1_730 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t1_730  p1, Guid_t1_730  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t1_730 *)args[0]), *((Guid_t1_730 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Guid_t1_730_Guid_t1_730 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t1_730  p1, Guid_t1_730  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t1_730 *)args[0]), *((Guid_t1_730 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Link_t2_22 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t2_22  (*Func)(void* obj, const MethodInfo* method);
	Link_t2_22  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1126_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1126  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t1_1126  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_BooleanU26_t1_1679 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1130 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1130  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1130  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1_167_Object_t_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1_167  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t1_167  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1126 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1126  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1_1126  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1129 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1129  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1129  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1_12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t1_1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1133  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1133  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1126_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1126  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1_1126  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_SByte_t1_12_SByte_t1_12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatus_t3_87 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t3_87  (*Func)(void* obj, const MethodInfo* method);
	X509ChainStatus_t3_87  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t3_134 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t3_134  (*Func)(void* obj, const MethodInfo* method);
	Mark_t3_134  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriScheme_t3_169 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t3_169  (*Func)(void* obj, const MethodInfo* method);
	UriScheme_t3_169  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ClientCertificateType_t4_98 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1159_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1159  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t1_1159  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_ObjectU26_t1_1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1_167_Int32_t1_3_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1_167  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1_167  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1159_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1159  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1_1159  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_902_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_902  (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t1_902  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t1_11_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t1_20_SByte_t1_12_ObjectU26_t1_1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1_167_SByte_t1_12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1_167  (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1_167  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1177 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1177  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1177  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1_12_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t1_902_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_902  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1_902  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3_201 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3_201  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3_201  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcAchievementData_t6_205 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t6_205  (*Func)(void* obj, const MethodInfo* method);
	GcAchievementData_t6_205  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcScoreData_t6_206 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t6_206  (*Func)(void* obj, const MethodInfo* method);
	GcScoreData_t6_206  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContactPoint_t6_104 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t6_104  (*Func)(void* obj, const MethodInfo* method);
	ContactPoint_t6_104  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContactPoint2D_t6_114 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint2D_t6_114  (*Func)(void* obj, const MethodInfo* method);
	ContactPoint2D_t6_114  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Keyframe_t6_131 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t6_131  (*Func)(void* obj, const MethodInfo* method);
	Keyframe_t6_131  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CharacterInfo_t6_146 (const MethodInfo* method, void* obj, void** args)
{
	typedef CharacterInfo_t6_146  (*Func)(void* obj, const MethodInfo* method);
	CharacterInfo_t6_146  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIVertex_t6_153 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t6_153  (*Func)(void* obj, const MethodInfo* method);
	UIVertex_t6_153  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1228 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1228  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1228  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_UIVertex_t6_153_UIVertex_t6_153 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t6_153  p1, UIVertex_t6_153  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t6_153 *)args[0]), *((UIVertex_t6_153 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UIVertex_t6_153_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t6_153  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t6_153 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_UICharInfo_t6_149 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t6_149  (*Func)(void* obj, const MethodInfo* method);
	UICharInfo_t6_149  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1233 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1233  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1233  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_UICharInfo_t6_149_UICharInfo_t6_149 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t6_149  p1, UICharInfo_t6_149  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t6_149 *)args[0]), *((UICharInfo_t6_149 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UICharInfo_t6_149_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t6_149  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t6_149 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_UILineInfo_t6_150 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t6_150  (*Func)(void* obj, const MethodInfo* method);
	UILineInfo_t6_150  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1238 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1238  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1238  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_UILineInfo_t6_150_UILineInfo_t6_150 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t6_150  p1, UILineInfo_t6_150  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t6_150 *)args[0]), *((UILineInfo_t6_150 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UILineInfo_t6_150_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t6_150  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t6_150 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_HitInfo_t6_221 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t6_221  (*Func)(void* obj, const MethodInfo* method);
	HitInfo_t6_221  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t6_237_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1266_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1266  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t1_1266  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t6_237_Object_t_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Object_t_TextEditOpU26_t6_324 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1271 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1271  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1271  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1266 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1266  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1_1266  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t6_237 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1270 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1270  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1270  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1274  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1274  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1266_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1266  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1_1266  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1_29_Byte_t1_11_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t1_1294_Byte_t1_11_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1294  (*Func)(void* obj, uint8_t p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t1_1294  ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ConnectionProtocol_t5_36_Byte_t1_11_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, uint8_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t1_3_Byte_t1_11_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint8_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Byte_t1_11_Int32U26_t1_1678 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ConnectionProtocol_t5_36_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1299 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1299  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1299  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1_167_Byte_t1_11_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1_167  (*Func)(void* obj, uint8_t p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t1_167  ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1294 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1294  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1_1294  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1298 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1298  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1298  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Byte_t1_11_Int32_t1_3_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t1_1302 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1302  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1302  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1294_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1294  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1_1294  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Byte_t1_11_Byte_t1_11 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1317 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1317  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1_1317  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1317_Byte_t1_11_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1317  (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t1_1317  ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Team_t8_169_Byte_t1_11_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Byte_t1_11_ObjectU26_t1_1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1315 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1315  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1315  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1_167_Byte_t1_11_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1_167  (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1_167  ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1325 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1325  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1325  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1_1317_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1_1317  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1_1317  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_State_t8_41 (const MethodInfo* method, void* obj, void** args)
{
	typedef State_t8_41  (*Func)(void* obj, const MethodInfo* method);
	State_t8_41  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int32_t1_3_Int32_t1_3 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2_30 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2_30  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2_30  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Link_t2_29 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t2_29  (*Func)(void* obj, const MethodInfo* method);
	Link_t2_29  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1374  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1374  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1379 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1379  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1379  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t3_204 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3_204  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3_204  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1_1396 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1_1396  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1_1396  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Single_t1_17_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

extern const InvokerMethod g_Il2CppInvokerPointers[1548] = 
{
	RuntimeInvoker_Void_t1_29,
	RuntimeInvoker_Boolean_t1_20_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t,
	RuntimeInvoker_Int32_t1_3,
	RuntimeInvoker_Object_t,
	RuntimeInvoker_Int32_t1_3_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_ObjectU5BU5DU26_t1_1676,
	RuntimeInvoker_Int32_t1_3_Object_t_ObjectU5BU5DU26_t1_1676,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Byte_t1_11_Object_t,
	RuntimeInvoker_Char_t1_15_Object_t,
	RuntimeInvoker_DateTime_t1_127_Object_t,
	RuntimeInvoker_Decimal_t1_19_Object_t,
	RuntimeInvoker_Double_t1_18_Object_t,
	RuntimeInvoker_Int16_t1_13_Object_t,
	RuntimeInvoker_Int64_t1_7_Object_t,
	RuntimeInvoker_SByte_t1_12_Object_t,
	RuntimeInvoker_Single_t1_17_Object_t,
	RuntimeInvoker_UInt16_t1_14_Object_t,
	RuntimeInvoker_UInt32_t1_8_Object_t,
	RuntimeInvoker_UInt64_t1_10_Object_t,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_Int32_t1_3_ExceptionU26_t1_1677,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_Int32U26_t1_1678_ExceptionU26_t1_1677,
	RuntimeInvoker_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_SByte_t1_12_ExceptionU26_t1_1677,
	RuntimeInvoker_Boolean_t1_20_Int32U26_t1_1678_Object_t_SByte_t1_12_SByte_t1_12_ExceptionU26_t1_1677,
	RuntimeInvoker_Void_t1_29_Int32U26_t1_1678_Object_t_Object_t_BooleanU26_t1_1679_BooleanU26_t1_1679,
	RuntimeInvoker_Void_t1_29_Int32U26_t1_1678_Object_t_Object_t_BooleanU26_t1_1679,
	RuntimeInvoker_Boolean_t1_20_Int32U26_t1_1678_Object_t_Int32U26_t1_1678_SByte_t1_12_ExceptionU26_t1_1677,
	RuntimeInvoker_Boolean_t1_20_Int32U26_t1_1678_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_Int16_t1_13_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_SByte_t1_12_Int32U26_t1_1678_ExceptionU26_t1_1677,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20,
	RuntimeInvoker_Void_t1_29_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Int64_t1_7,
	RuntimeInvoker_Boolean_t1_20_Int64_t1_7,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_Int64U26_t1_1680_ExceptionU26_t1_1677,
	RuntimeInvoker_Int64_t1_7_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_SByte_t1_12_Int64U26_t1_1680_ExceptionU26_t1_1677,
	RuntimeInvoker_Int64_t1_7_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int64U26_t1_1680,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_Int64U26_t1_1680,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_UInt32U26_t1_1681_ExceptionU26_t1_1677,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_SByte_t1_12_UInt32U26_t1_1681_ExceptionU26_t1_1677,
	RuntimeInvoker_UInt32_t1_8_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_UInt32_t1_8_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_UInt32U26_t1_1681,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_UInt32U26_t1_1681,
	RuntimeInvoker_UInt64_t1_10_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_SByte_t1_12_UInt64U26_t1_1682_ExceptionU26_t1_1677,
	RuntimeInvoker_UInt64_t1_10_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_UInt64U26_t1_1682,
	RuntimeInvoker_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_SByte_t1_12,
	RuntimeInvoker_Byte_t1_11_Object_t_Object_t,
	RuntimeInvoker_Byte_t1_11_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_ByteU26_t1_1683,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_ByteU26_t1_1683,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_SByteU26_t1_1684_ExceptionU26_t1_1677,
	RuntimeInvoker_SByte_t1_12_Object_t_Object_t,
	RuntimeInvoker_SByte_t1_12_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByteU26_t1_1684,
	RuntimeInvoker_Int32_t1_3_Int16_t1_13,
	RuntimeInvoker_Boolean_t1_20_Int16_t1_13,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_Int16U26_t1_1685_ExceptionU26_t1_1677,
	RuntimeInvoker_Int16_t1_13_Object_t_Object_t,
	RuntimeInvoker_Int16_t1_13_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int16U26_t1_1685,
	RuntimeInvoker_UInt16_t1_14_Object_t_Object_t,
	RuntimeInvoker_UInt16_t1_14_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_UInt16U26_t1_1686,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_UInt16U26_t1_1686,
	RuntimeInvoker_Void_t1_29_ByteU2AU26_t1_1687_ByteU2AU26_t1_1687_DoubleU2AU26_t1_1688_UInt16U2AU26_t1_1689_UInt16U2AU26_t1_1689_UInt16U2AU26_t1_1689_UInt16U2AU26_t1_1689,
	RuntimeInvoker_UnicodeCategory_t1_218_Int16_t1_13,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3,
	RuntimeInvoker_Char_t1_15_Int16_t1_13,
	RuntimeInvoker_Char_t1_15_Int16_t1_13_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t,
	RuntimeInvoker_Void_t1_29_Int16_t1_13_Int32_t1_3,
	RuntimeInvoker_Char_t1_15_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t,
	RuntimeInvoker_Int32_t1_3_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Object_t_SByte_t1_12_Object_t,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12_Object_t,
	RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Int16_t1_13_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Int16_t1_13_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Int32_t1_3_Int16_t1_13,
	RuntimeInvoker_Object_t_Int16_t1_13_Int16_t1_13,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Int32U26_t1_1678_Int32U26_t1_1678_Int32U26_t1_1678_BooleanU26_t1_1679_StringU26_t1_1690,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int16_t1_13,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Object_t,
	RuntimeInvoker_Object_t_Int16_t1_13_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Single_t1_17,
	RuntimeInvoker_Boolean_t1_20_Single_t1_17,
	RuntimeInvoker_Single_t1_17_Object_t_Object_t,
	RuntimeInvoker_Int32_t1_3_Double_t1_18,
	RuntimeInvoker_Boolean_t1_20_Double_t1_18,
	RuntimeInvoker_Double_t1_18_Object_t_Object_t,
	RuntimeInvoker_Double_t1_18_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_SByte_t1_12_DoubleU26_t1_1691_ExceptionU26_t1_1677,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Object_t_DoubleU26_t1_1691,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Int64_t1_7,
	RuntimeInvoker_Void_t1_29_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Double_t1_18,
	RuntimeInvoker_Object_t_Decimal_t1_19,
	RuntimeInvoker_Decimal_t1_19_Decimal_t1_19_Decimal_t1_19,
	RuntimeInvoker_UInt64_t1_10_Decimal_t1_19,
	RuntimeInvoker_Int64_t1_7_Decimal_t1_19,
	RuntimeInvoker_Boolean_t1_20_Decimal_t1_19_Decimal_t1_19,
	RuntimeInvoker_Decimal_t1_19_Decimal_t1_19,
	RuntimeInvoker_Int32_t1_3_Decimal_t1_19_Decimal_t1_19,
	RuntimeInvoker_Int32_t1_3_Decimal_t1_19,
	RuntimeInvoker_Boolean_t1_20_Decimal_t1_19,
	RuntimeInvoker_Decimal_t1_19_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t_Int32U26_t1_1678_BooleanU26_t1_1679_BooleanU26_t1_1679_Int32U26_t1_1678_SByte_t1_12,
	RuntimeInvoker_Decimal_t1_19_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_DecimalU26_t1_1692_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_DecimalU26_t1_1692_UInt64U26_t1_1682,
	RuntimeInvoker_Int32_t1_3_DecimalU26_t1_1692_Int64U26_t1_1680,
	RuntimeInvoker_Int32_t1_3_DecimalU26_t1_1692_DecimalU26_t1_1692,
	RuntimeInvoker_Int32_t1_3_DecimalU26_t1_1692_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_DecimalU26_t1_1692_Int32_t1_3,
	RuntimeInvoker_Double_t1_18_DecimalU26_t1_1692,
	RuntimeInvoker_Void_t1_29_DecimalU26_t1_1692_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_DecimalU26_t1_1692_DecimalU26_t1_1692_DecimalU26_t1_1692,
	RuntimeInvoker_Byte_t1_11_Decimal_t1_19,
	RuntimeInvoker_SByte_t1_12_Decimal_t1_19,
	RuntimeInvoker_Int16_t1_13_Decimal_t1_19,
	RuntimeInvoker_UInt16_t1_14_Decimal_t1_19,
	RuntimeInvoker_UInt32_t1_8_Decimal_t1_19,
	RuntimeInvoker_Decimal_t1_19_SByte_t1_12,
	RuntimeInvoker_Decimal_t1_19_Int16_t1_13,
	RuntimeInvoker_Decimal_t1_19_Int32_t1_3,
	RuntimeInvoker_Decimal_t1_19_Int64_t1_7,
	RuntimeInvoker_Decimal_t1_19_Single_t1_17,
	RuntimeInvoker_Decimal_t1_19_Double_t1_18,
	RuntimeInvoker_Single_t1_17_Decimal_t1_19,
	RuntimeInvoker_Double_t1_18_Decimal_t1_19,
	RuntimeInvoker_Void_t1_29_Object_t_StreamingContext_t1_514,
	RuntimeInvoker_Int64_t1_7,
	RuntimeInvoker_Boolean_t1_20_IntPtr_t_IntPtr_t,
	RuntimeInvoker_IntPtr_t_Int32_t1_3,
	RuntimeInvoker_IntPtr_t_Int64_t1_7,
	RuntimeInvoker_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t1_3_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t,
	RuntimeInvoker_UInt32_t1_8,
	RuntimeInvoker_UInt64_t1_10,
	RuntimeInvoker_UInt64_t1_10_IntPtr_t,
	RuntimeInvoker_UInt32_t1_8_IntPtr_t,
	RuntimeInvoker_UIntPtr_t_Int64_t1_7,
	RuntimeInvoker_UIntPtr_t_Object_t,
	RuntimeInvoker_UIntPtr_t_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t1_1693,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t1_3_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_TypeCode_t1_769,
	RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_UInt64_t1_10_Object_t_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Object_t_Object_t_Int16_t1_13,
	RuntimeInvoker_Object_t_Object_t_Int64_t1_7,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t,
	RuntimeInvoker_Int64_t1_7_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Int64_t1_7,
	RuntimeInvoker_Object_t_Int64_t1_7_Int64_t1_7,
	RuntimeInvoker_Object_t_Int64_t1_7_Int64_t1_7_Int64_t1_7,
	RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7,
	RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_Int64_t1_7,
	RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_Int64_t1_7_Int64_t1_7,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_Object_t_Int64_t1_7_Int64_t1_7,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int64_t1_7,
	RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Object_t,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_IntPtr_t,
	RuntimeInvoker_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_TypeAttributes_t1_359,
	RuntimeInvoker_MemberTypes_t1_337,
	RuntimeInvoker_RuntimeTypeHandle_t1_30,
	RuntimeInvoker_Object_t_Object_t_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_TypeCode_t1_769_Object_t,
	RuntimeInvoker_Object_t_RuntimeTypeHandle_t1_30,
	RuntimeInvoker_RuntimeTypeHandle_t1_30_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t1_3_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12,
	RuntimeInvoker_Object_t_SByte_t1_12,
	RuntimeInvoker_IntPtr_t,
	RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_SByte_t1_12_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_RuntimeFieldHandle_t1_36,
	RuntimeInvoker_Void_t1_29_IntPtr_t_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_ContractionU5BU5DU26_t1_1694_Level2MapU5BU5DU26_t1_1695,
	RuntimeInvoker_Void_t1_29_Object_t_CodePointIndexerU26_t1_1696_ByteU2AU26_t1_1687_ByteU2AU26_t1_1687_CodePointIndexerU26_t1_1696_ByteU2AU26_t1_1687,
	RuntimeInvoker_Byte_t1_11_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_UInt32_t1_8_Object_t_Int32_t1_3,
	RuntimeInvoker_Byte_t1_11_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_ExtenderType_t1_82_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_BooleanU26_t1_1679_BooleanU26_t1_1679_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_BooleanU26_t1_1679_BooleanU26_t1_1679_SByte_t1_12_SByte_t1_12_ContextU26_t1_1697,
	RuntimeInvoker_Int32_t1_3_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12_ContextU26_t1_1697,
	RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Int32_t1_3_Int32_t1_3_BooleanU26_t1_1679,
	RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int16_t1_13_Int32_t1_3_SByte_t1_12_ContextU26_t1_1697,
	RuntimeInvoker_Int32_t1_3_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Object_t_ContextU26_t1_1697,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_SByte_t1_12_ContextU26_t1_1697,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12_ContextU26_t1_1697,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_ContractionU26_t1_1698_ContextU26_t1_1697,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12_ContextU26_t1_1697,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_ContractionU26_t1_1698_ContextU26_t1_1697,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_ByteU5BU5DU26_t1_1699_Int32U26_t1_1678,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_ConfidenceFactor_t1_91,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Sign_t1_93_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_DSAParameters_t1_563_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_DSAParameters_t1_563,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Object_t_Object_t_DSAParameters_t1_563,
	RuntimeInvoker_RSAParameters_t1_592_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_RSAParameters_t1_592,
	RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_Object_t,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_DSAParameters_t1_563_BooleanU26_t1_1679,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t1_12_Object_t_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_DateTime_t1_127,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_Object_t,
	RuntimeInvoker_Byte_t1_11,
	RuntimeInvoker_Void_t1_29_Object_t_Int32U26_t1_1678_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Int32U26_t1_1678_ByteU26_t1_1683_Int32U26_t1_1678_ByteU5BU5DU26_t1_1699,
	RuntimeInvoker_Object_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Int16_t1_13_Object_t_Int32_t1_3,
	RuntimeInvoker_Single_t1_17_Object_t_Int32_t1_3,
	RuntimeInvoker_Double_t1_18_Object_t_Int32_t1_3,
	RuntimeInvoker_Object_t_Int16_t1_13_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Single_t1_17_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Object_t_Single_t1_17_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Single_t1_17_Object_t,
	RuntimeInvoker_DictionaryEntry_t1_167,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_SByte_t1_12_MethodBaseU26_t1_1700_Int32U26_t1_1678_Int32U26_t1_1678_StringU26_t1_1690_Int32U26_t1_1678_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_DateTime_t1_127,
	RuntimeInvoker_DayOfWeek_t1_709_DateTime_t1_127,
	RuntimeInvoker_Int32_t1_3_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_DayOfWeek_t1_709_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32U26_t1_1678_Int32U26_t1_1678_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32U26_t1_1678_Int32U26_t1_1678_Int32U26_t1_1678_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Object_t_Int16_t1_13,
	RuntimeInvoker_Void_t1_29_DateTime_t1_127_DateTime_t1_127_TimeSpan_t1_213,
	RuntimeInvoker_TimeSpan_t1_213,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Object_t_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32U26_t1_1678,
	RuntimeInvoker_Char_t1_15,
	RuntimeInvoker_Decimal_t1_19,
	RuntimeInvoker_Double_t1_18,
	RuntimeInvoker_Int16_t1_13,
	RuntimeInvoker_SByte_t1_12,
	RuntimeInvoker_Single_t1_17,
	RuntimeInvoker_UInt16_t1_14,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_SByte_t1_12_Int32_t1_3_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Int64_t1_7_Int64_t1_7_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_IntPtr_t_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Object_t_MonoIOErrorU26_t1_1701,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3_MonoIOErrorU26_t1_1701,
	RuntimeInvoker_Object_t_MonoIOErrorU26_t1_1701,
	RuntimeInvoker_FileAttributes_t1_233_Object_t_MonoIOErrorU26_t1_1701,
	RuntimeInvoker_MonoFileType_t1_245_IntPtr_t_MonoIOErrorU26_t1_1701,
	RuntimeInvoker_Boolean_t1_20_Object_t_MonoIOStatU26_t1_1702_MonoIOErrorU26_t1_1701,
	RuntimeInvoker_IntPtr_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_MonoIOErrorU26_t1_1701,
	RuntimeInvoker_Boolean_t1_20_IntPtr_t_MonoIOErrorU26_t1_1701,
	RuntimeInvoker_Int32_t1_3_IntPtr_t_Object_t_Int32_t1_3_Int32_t1_3_MonoIOErrorU26_t1_1701,
	RuntimeInvoker_Int64_t1_7_IntPtr_t_Int64_t1_7_Int32_t1_3_MonoIOErrorU26_t1_1701,
	RuntimeInvoker_Int64_t1_7_IntPtr_t_MonoIOErrorU26_t1_1701,
	RuntimeInvoker_Boolean_t1_20_IntPtr_t_Int64_t1_7_MonoIOErrorU26_t1_1701,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_SByte_t1_12_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int16_t1_13,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Object_t_Object_t,
	RuntimeInvoker_CallingConventions_t1_327,
	RuntimeInvoker_RuntimeMethodHandle_t1_760,
	RuntimeInvoker_MethodAttributes_t1_338,
	RuntimeInvoker_MethodToken_t1_292,
	RuntimeInvoker_FieldAttributes_t1_335,
	RuntimeInvoker_RuntimeFieldHandle_t1_36,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_OpCode_t1_296,
	RuntimeInvoker_Void_t1_29_OpCode_t1_296_Object_t,
	RuntimeInvoker_StackBehaviour_t1_300,
	RuntimeInvoker_Object_t_Int32_t1_3_Int32_t1_3_Object_t,
	RuntimeInvoker_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_SByte_t1_12_Object_t,
	RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t1_1678_ModuleU26_t1_1703,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_AssemblyNameFlags_t1_320,
	RuntimeInvoker_Object_t_Int32_t1_3_Object_t_ObjectU5BU5DU26_t1_1676_Object_t_Object_t_Object_t_ObjectU26_t1_1704,
	RuntimeInvoker_Void_t1_29_ObjectU5BU5DU26_t1_1676_Object_t,
	RuntimeInvoker_Object_t_Int32_t1_3_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_ObjectU5BU5DU26_t1_1676_Object_t,
	RuntimeInvoker_Object_t_Int32_t1_3_Object_t_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_EventAttributes_t1_333,
	RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Object_t_RuntimeFieldHandle_t1_36,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Object_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Object_t_StreamingContext_t1_514,
	RuntimeInvoker_Object_t_RuntimeMethodHandle_t1_760,
	RuntimeInvoker_Void_t1_29_Object_t_MonoEventInfoU26_t1_1705,
	RuntimeInvoker_MonoEventInfo_t1_342_Object_t,
	RuntimeInvoker_Void_t1_29_IntPtr_t_MonoMethodInfoU26_t1_1706,
	RuntimeInvoker_MonoMethodInfo_t1_346_IntPtr_t,
	RuntimeInvoker_MethodAttributes_t1_338_IntPtr_t,
	RuntimeInvoker_CallingConventions_t1_327_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t1_1677,
	RuntimeInvoker_Void_t1_29_Object_t_MonoPropertyInfoU26_t1_1707_Int32_t1_3,
	RuntimeInvoker_PropertyAttributes_t1_355,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Int32_t1_3,
	RuntimeInvoker_ParameterAttributes_t1_350,
	RuntimeInvoker_Void_t1_29_Int64_t1_7_ResourceInfoU26_t1_1708,
	RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_Int32_t1_3,
	RuntimeInvoker_GCHandle_t1_393_Object_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Object_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Byte_t1_11_IntPtr_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_BooleanU26_t1_1679,
	RuntimeInvoker_Void_t1_29_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t1_1690,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t1_1690,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_Object_t_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_TimeSpan_t1_213_Object_t,
	RuntimeInvoker_Void_t1_29_TimeSpan_t1_213,
	RuntimeInvoker_Void_t1_29_Object_t_Byte_t1_11,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_SByte_t1_12_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1_514_Object_t,
	RuntimeInvoker_Object_t_Object_t_StreamingContext_t1_514_ISurrogateSelectorU26_t1_1709,
	RuntimeInvoker_Void_t1_29_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_StringU26_t1_1690,
	RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t1_1704,
	RuntimeInvoker_Boolean_t1_20_Object_t_StringU26_t1_1690_StringU26_t1_1690,
	RuntimeInvoker_WellKnownObjectMode_t1_506,
	RuntimeInvoker_StreamingContext_t1_514,
	RuntimeInvoker_TypeFilterLevel_t1_523,
	RuntimeInvoker_Void_t1_29_Object_t_BooleanU26_t1_1679,
	RuntimeInvoker_Object_t_Byte_t1_11_Object_t_SByte_t1_12_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t1_11_Object_t_SByte_t1_12_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_ObjectU26_t1_1704_HeaderU5BU5DU26_t1_1710,
	RuntimeInvoker_Void_t1_29_Byte_t1_11_Object_t_SByte_t1_12_ObjectU26_t1_1704_HeaderU5BU5DU26_t1_1710,
	RuntimeInvoker_Boolean_t1_20_Byte_t1_11_Object_t,
	RuntimeInvoker_Void_t1_29_Byte_t1_11_Object_t_Int64U26_t1_1680_ObjectU26_t1_1704_SerializationInfoU26_t1_1711,
	RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_SByte_t1_12_Int64U26_t1_1680_ObjectU26_t1_1704_SerializationInfoU26_t1_1711,
	RuntimeInvoker_Void_t1_29_Object_t_Int64U26_t1_1680_ObjectU26_t1_1704_SerializationInfoU26_t1_1711,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int64_t1_7_ObjectU26_t1_1704_SerializationInfoU26_t1_1711,
	RuntimeInvoker_Void_t1_29_Int64_t1_7_Object_t_Object_t_Int64_t1_7_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Int64U26_t1_1680_ObjectU26_t1_1704,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int64U26_t1_1680_ObjectU26_t1_1704,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int64_t1_7_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Int64_t1_7_Int64_t1_7_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int64_t1_7_Object_t,
	RuntimeInvoker_Object_t_Object_t_Byte_t1_11,
	RuntimeInvoker_Void_t1_29_Int64_t1_7_Int32_t1_3_Int64_t1_7,
	RuntimeInvoker_Void_t1_29_Int64_t1_7_Object_t_Int64_t1_7,
	RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_Object_t_Int64_t1_7_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_StreamingContext_t1_514,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_StreamingContext_t1_514,
	RuntimeInvoker_Void_t1_29_StreamingContext_t1_514,
	RuntimeInvoker_Object_t_StreamingContext_t1_514_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Int16_t1_13,
	RuntimeInvoker_Void_t1_29_Object_t_DateTime_t1_127,
	RuntimeInvoker_Void_t1_29_Object_t_Single_t1_17,
	RuntimeInvoker_SerializationEntry_t1_540,
	RuntimeInvoker_StreamingContextStates_t1_544,
	RuntimeInvoker_CspProviderFlags_t1_557,
	RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_Object_t_Object_t,
	RuntimeInvoker_UInt32_t1_8_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Int64_t1_7_Object_t_Int32_t1_3,
	RuntimeInvoker_UInt32_t1_8_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_UInt32U26_t1_1681_Int32_t1_3_UInt32U26_t1_1681_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t,
	RuntimeInvoker_UInt32_t1_8_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int64_t1_7_Int64_t1_7,
	RuntimeInvoker_UInt64_t1_10_Int64_t1_7_Int32_t1_3,
	RuntimeInvoker_UInt64_t1_10_Int64_t1_7_Int64_t1_7_Int64_t1_7,
	RuntimeInvoker_UInt64_t1_10_Int64_t1_7,
	RuntimeInvoker_CipherMode_t1_553,
	RuntimeInvoker_PaddingMode_t1_580,
	RuntimeInvoker_Void_t1_29_StringBuilderU26_t1_1712_Int32_t1_3,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_EncoderFallbackBufferU26_t1_1713_CharU5BU5DU26_t1_1714,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_DecoderFallbackBufferU26_t1_1715,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Int16_t1_13_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Int16_t1_13_Int16_t1_13_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int16_t1_13_Int16_t1_13_Int32_t1_3,
	RuntimeInvoker_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Int32_t1_3_Object_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_SByte_t1_12_Int32U26_t1_1678_BooleanU26_t1_1679_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_Int32U26_t1_1678,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_CharU26_t1_1716_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_CharU26_t1_1716_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_CharU26_t1_1716_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Int32_t1_3_CharU26_t1_1716_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t_DecoderFallbackBufferU26_t1_1715_ByteU5BU5DU26_t1_1699_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t_DecoderFallbackBufferU26_t1_1715_ByteU5BU5DU26_t1_1699_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_DecoderFallbackBufferU26_t1_1715_ByteU5BU5DU26_t1_1699_Object_t_Int64_t1_7_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_DecoderFallbackBufferU26_t1_1715_ByteU5BU5DU26_t1_1699_Object_t_Int64_t1_7_Int32_t1_3_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32_t1_3_UInt32U26_t1_1681_UInt32U26_t1_1681_Object_t_DecoderFallbackBufferU26_t1_1715_ByteU5BU5DU26_t1_1699_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Int32_t1_3_UInt32U26_t1_1681_UInt32U26_t1_1681_Object_t_DecoderFallbackBufferU26_t1_1715_ByteU5BU5DU26_t1_1699_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_Int32_t1_3,
	RuntimeInvoker_IntPtr_t_SByte_t1_12_Object_t_BooleanU26_t1_1679,
	RuntimeInvoker_Boolean_t1_20_IntPtr_t,
	RuntimeInvoker_IntPtr_t_SByte_t1_12_SByte_t1_12_Object_t_BooleanU26_t1_1679,
	RuntimeInvoker_Boolean_t1_20_TimeSpan_t1_213_TimeSpan_t1_213,
	RuntimeInvoker_Boolean_t1_20_Int64_t1_7_Int64_t1_7_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_IntPtr_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Int64_t1_7_Double_t1_18,
	RuntimeInvoker_Object_t_Double_t1_18,
	RuntimeInvoker_Int64_t1_7_Object_t_Int32_t1_3,
	RuntimeInvoker_UInt16_t1_14_Object_t_Int32_t1_3,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Byte_t1_11_SByte_t1_12,
	RuntimeInvoker_Byte_t1_11_Int16_t1_13,
	RuntimeInvoker_Byte_t1_11_Double_t1_18,
	RuntimeInvoker_Byte_t1_11_Single_t1_17,
	RuntimeInvoker_Byte_t1_11_Int64_t1_7,
	RuntimeInvoker_Char_t1_15_SByte_t1_12,
	RuntimeInvoker_Char_t1_15_Int64_t1_7,
	RuntimeInvoker_Char_t1_15_Single_t1_17,
	RuntimeInvoker_Char_t1_15_Object_t_Object_t,
	RuntimeInvoker_DateTime_t1_127_Object_t_Object_t,
	RuntimeInvoker_DateTime_t1_127_Int16_t1_13,
	RuntimeInvoker_DateTime_t1_127_Int32_t1_3,
	RuntimeInvoker_DateTime_t1_127_Int64_t1_7,
	RuntimeInvoker_DateTime_t1_127_Single_t1_17,
	RuntimeInvoker_DateTime_t1_127_SByte_t1_12,
	RuntimeInvoker_Double_t1_18_SByte_t1_12,
	RuntimeInvoker_Double_t1_18_Double_t1_18,
	RuntimeInvoker_Double_t1_18_Single_t1_17,
	RuntimeInvoker_Double_t1_18_Int32_t1_3,
	RuntimeInvoker_Double_t1_18_Int64_t1_7,
	RuntimeInvoker_Double_t1_18_Int16_t1_13,
	RuntimeInvoker_Int16_t1_13_SByte_t1_12,
	RuntimeInvoker_Int16_t1_13_Int16_t1_13,
	RuntimeInvoker_Int16_t1_13_Double_t1_18,
	RuntimeInvoker_Int16_t1_13_Single_t1_17,
	RuntimeInvoker_Int16_t1_13_Int32_t1_3,
	RuntimeInvoker_Int16_t1_13_Int64_t1_7,
	RuntimeInvoker_Int64_t1_7_SByte_t1_12,
	RuntimeInvoker_Int64_t1_7_Int16_t1_13,
	RuntimeInvoker_Int64_t1_7_Single_t1_17,
	RuntimeInvoker_Int64_t1_7_Int64_t1_7,
	RuntimeInvoker_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_SByte_t1_12_Int16_t1_13,
	RuntimeInvoker_SByte_t1_12_Double_t1_18,
	RuntimeInvoker_SByte_t1_12_Single_t1_17,
	RuntimeInvoker_SByte_t1_12_Int32_t1_3,
	RuntimeInvoker_SByte_t1_12_Int64_t1_7,
	RuntimeInvoker_Single_t1_17_SByte_t1_12,
	RuntimeInvoker_Single_t1_17_Double_t1_18,
	RuntimeInvoker_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Single_t1_17_Int32_t1_3,
	RuntimeInvoker_Single_t1_17_Int64_t1_7,
	RuntimeInvoker_Single_t1_17_Int16_t1_13,
	RuntimeInvoker_UInt16_t1_14_SByte_t1_12,
	RuntimeInvoker_UInt16_t1_14_Int16_t1_13,
	RuntimeInvoker_UInt16_t1_14_Double_t1_18,
	RuntimeInvoker_UInt16_t1_14_Single_t1_17,
	RuntimeInvoker_UInt16_t1_14_Int32_t1_3,
	RuntimeInvoker_UInt16_t1_14_Int64_t1_7,
	RuntimeInvoker_UInt32_t1_8_SByte_t1_12,
	RuntimeInvoker_UInt32_t1_8_Int16_t1_13,
	RuntimeInvoker_UInt32_t1_8_Double_t1_18,
	RuntimeInvoker_UInt32_t1_8_Single_t1_17,
	RuntimeInvoker_UInt32_t1_8_Int64_t1_7,
	RuntimeInvoker_UInt64_t1_10_SByte_t1_12,
	RuntimeInvoker_UInt64_t1_10_Int16_t1_13,
	RuntimeInvoker_UInt64_t1_10_Double_t1_18,
	RuntimeInvoker_UInt64_t1_10_Single_t1_17,
	RuntimeInvoker_UInt64_t1_10_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_ByteU5BU5DU26_t1_1699,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_TimeSpan_t1_213,
	RuntimeInvoker_Void_t1_29_Int64_t1_7_Int32_t1_3,
	RuntimeInvoker_DayOfWeek_t1_709,
	RuntimeInvoker_DateTimeKind_t1_706,
	RuntimeInvoker_DateTime_t1_127_TimeSpan_t1_213,
	RuntimeInvoker_DateTime_t1_127_Double_t1_18,
	RuntimeInvoker_Int32_t1_3_DateTime_t1_127_DateTime_t1_127,
	RuntimeInvoker_Boolean_t1_20_DateTime_t1_127,
	RuntimeInvoker_DateTime_t1_127_DateTime_t1_127_Int32_t1_3,
	RuntimeInvoker_DateTime_t1_127_Object_t_Object_t_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Int32_t1_3_DateTimeU26_t1_1717_DateTimeOffsetU26_t1_1718_SByte_t1_12_ExceptionU26_t1_1677,
	RuntimeInvoker_Object_t_Object_t_SByte_t1_12_ExceptionU26_t1_1677,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12_Int32U26_t1_1678,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Object_t_SByte_t1_12_Int32U26_t1_1678,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Int32_t1_3_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12_Int32U26_t1_1678_Int32U26_t1_1678,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_SByte_t1_12_Int32U26_t1_1678,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t_SByte_t1_12_DateTimeU26_t1_1717_DateTimeOffsetU26_t1_1718_Object_t_Int32_t1_3_SByte_t1_12_BooleanU26_t1_1679_BooleanU26_t1_1679,
	RuntimeInvoker_DateTime_t1_127_Object_t_Object_t_Object_t_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t_Int32_t1_3_DateTimeU26_t1_1717_SByte_t1_12_BooleanU26_t1_1679_SByte_t1_12_ExceptionU26_t1_1677,
	RuntimeInvoker_DateTime_t1_127_DateTime_t1_127_TimeSpan_t1_213,
	RuntimeInvoker_Boolean_t1_20_DateTime_t1_127_DateTime_t1_127,
	RuntimeInvoker_Void_t1_29_DateTime_t1_127,
	RuntimeInvoker_Void_t1_29_DateTime_t1_127_TimeSpan_t1_213,
	RuntimeInvoker_Void_t1_29_Int64_t1_7_TimeSpan_t1_213,
	RuntimeInvoker_Int32_t1_3_DateTimeOffset_t1_707,
	RuntimeInvoker_Boolean_t1_20_DateTimeOffset_t1_707,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int16_t1_13,
	RuntimeInvoker_Object_t_Int16_t1_13_Object_t_BooleanU26_t1_1679_BooleanU26_t1_1679,
	RuntimeInvoker_Object_t_Int16_t1_13_Object_t_BooleanU26_t1_1679_BooleanU26_t1_1679_SByte_t1_12,
	RuntimeInvoker_Object_t_DateTime_t1_127_Object_t_Object_t,
	RuntimeInvoker_Object_t_DateTime_t1_127_Nullable_1_t1_826_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_MonoEnumInfo_t1_720,
	RuntimeInvoker_Void_t1_29_Object_t_MonoEnumInfoU26_t1_1719,
	RuntimeInvoker_Int32_t1_3_Int16_t1_13_Int16_t1_13,
	RuntimeInvoker_Int32_t1_3_Int64_t1_7_Int64_t1_7,
	RuntimeInvoker_PlatformID_t1_756,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int16_t1_13_Int16_t1_13_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Guid_t1_730,
	RuntimeInvoker_Boolean_t1_20_Guid_t1_730,
	RuntimeInvoker_Guid_t1_730,
	RuntimeInvoker_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Double_t1_18_Double_t1_18_Double_t1_18,
	RuntimeInvoker_TypeAttributes_t1_359_Object_t,
	RuntimeInvoker_Object_t_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_UInt64U2AU26_t1_1720_Int32U2AU26_t1_1721_CharU2AU26_t1_1722_CharU2AU26_t1_1722_Int64U2AU26_t1_1723_Int32U2AU26_t1_1721,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int64_t1_7,
	RuntimeInvoker_Void_t1_29_Object_t_Double_t1_18_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Decimal_t1_19,
	RuntimeInvoker_Object_t_Object_t_SByte_t1_12_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int16_t1_13_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int64_t1_7_Object_t,
	RuntimeInvoker_Object_t_Object_t_Single_t1_17_Object_t,
	RuntimeInvoker_Object_t_Object_t_Double_t1_18_Object_t,
	RuntimeInvoker_Object_t_Object_t_Decimal_t1_19_Object_t,
	RuntimeInvoker_Object_t_Single_t1_17_Object_t,
	RuntimeInvoker_Object_t_Double_t1_18_Object_t,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_BooleanU26_t1_1679_SByte_t1_12_Int32U26_t1_1678_Int32U26_t1_1678,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int64_t1_7_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_TimeSpan_t1_213_TimeSpan_t1_213,
	RuntimeInvoker_Int32_t1_3_TimeSpan_t1_213_TimeSpan_t1_213,
	RuntimeInvoker_Int32_t1_3_TimeSpan_t1_213,
	RuntimeInvoker_Boolean_t1_20_TimeSpan_t1_213,
	RuntimeInvoker_TimeSpan_t1_213_Double_t1_18,
	RuntimeInvoker_TimeSpan_t1_213_Double_t1_18_Int64_t1_7,
	RuntimeInvoker_TimeSpan_t1_213_Int64_t1_7,
	RuntimeInvoker_TimeSpan_t1_213_TimeSpan_t1_213_TimeSpan_t1_213,
	RuntimeInvoker_TimeSpan_t1_213_DateTime_t1_127,
	RuntimeInvoker_Boolean_t1_20_DateTime_t1_127_Object_t,
	RuntimeInvoker_DateTime_t1_127_DateTime_t1_127,
	RuntimeInvoker_TimeSpan_t1_213_DateTime_t1_127_TimeSpan_t1_213,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int64U5BU5DU26_t1_1724_StringU5BU5DU26_t1_1725,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t3_213,
	RuntimeInvoker_EditorBrowsableState_t3_15,
	RuntimeInvoker_Int32_t1_3_IntPtr_t_Int32U26_t1_1678,
	RuntimeInvoker_Int32_t1_3_IntPtr_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SocketAddressU26_t3_214_Int32U26_t1_1678,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_EndPointU26_t3_215_SByte_t1_12_Int32U26_t1_1678,
	RuntimeInvoker_IntPtr_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32U26_t1_1678,
	RuntimeInvoker_AddressFamily_t3_23,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Object_t_Int32U26_t1_1678_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_IntPtr_t_Int32_t1_3_Int32_t1_3_Int32U26_t1_1678,
	RuntimeInvoker_Int32_t1_3_IntPtr_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32U26_t1_1678,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SocketErrorU26_t3_216,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Int32_t1_3_ObjectU26_t1_1704_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Int32_t1_3_Object_t_Object_t_Int32_t1_3_Int32U26_t1_1678,
	RuntimeInvoker_SocketError_t3_30,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Object_t_StringU26_t1_1690_StringU5BU5DU26_t1_1725_StringU5BU5DU26_t1_1725,
	RuntimeInvoker_Boolean_t1_20_Object_t_IPAddressU26_t3_217,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Object_t_IPv6AddressU26_t3_218,
	RuntimeInvoker_SecurityProtocolType_t3_59,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_SByte_t1_12_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_AsnDecodeStatus_t3_102_Object_t,
	RuntimeInvoker_Object_t_Int32_t1_3_Object_t_SByte_t1_12,
	RuntimeInvoker_X509ChainStatusFlags_t3_90_Object_t,
	RuntimeInvoker_X509ChainStatusFlags_t3_90_Object_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_X509ChainStatusFlags_t3_90_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_X509ChainStatusFlags_t3_90,
	RuntimeInvoker_Void_t1_29_Object_t_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_X509RevocationFlag_t3_97,
	RuntimeInvoker_X509RevocationMode_t3_98,
	RuntimeInvoker_X509VerificationFlags_t3_101,
	RuntimeInvoker_X509KeyUsageFlags_t3_95,
	RuntimeInvoker_X509KeyUsageFlags_t3_95_Int32_t1_3,
	RuntimeInvoker_Byte_t1_11_Int16_t1_13_Int16_t1_13,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t1_3,
	RuntimeInvoker_RegexOptions_t3_119,
	RuntimeInvoker_Category_t3_126_Object_t,
	RuntimeInvoker_Boolean_t1_20_UInt16_t1_14_Int16_t1_13,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int16_t1_13,
	RuntimeInvoker_Void_t1_29_Int16_t1_13_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_UInt16_t1_14_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Int16_t1_13_Int16_t1_13_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Int16_t1_13_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_UInt16_t1_14,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_SByte_t1_12_Object_t,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_Int32_t1_3_Object_t,
	RuntimeInvoker_UInt16_t1_14_UInt16_t1_14_UInt16_t1_14,
	RuntimeInvoker_OpFlags_t3_121_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_UInt16_t1_14_UInt16_t1_14,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int32U26_t1_1678_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int32U26_t1_1678_Int32U26_t1_1678_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Int32U26_t1_1678_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_UInt16_t1_14_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32U26_t1_1678_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3,
	RuntimeInvoker_Interval_t3_141,
	RuntimeInvoker_Boolean_t1_20_Interval_t3_141,
	RuntimeInvoker_Void_t1_29_Interval_t3_141,
	RuntimeInvoker_Interval_t3_141_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Double_t1_18_Interval_t3_141,
	RuntimeInvoker_Object_t_Interval_t3_141_Object_t_Object_t,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32U26_t1_1678_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32U26_t1_1678_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Object_t_RegexOptionsU26_t3_219,
	RuntimeInvoker_Void_t1_29_RegexOptionsU26_t3_219_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Int32U26_t1_1678_Int32U26_t1_1678_Int32_t1_3,
	RuntimeInvoker_Category_t3_126,
	RuntimeInvoker_Int32_t1_3_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_Int32U26_t1_1678_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_UInt16_t1_14_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Int16_t1_13_Int16_t1_13,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Object_t_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_UInt16_t1_14,
	RuntimeInvoker_Position_t3_122,
	RuntimeInvoker_UriHostNameType_t3_172_Object_t,
	RuntimeInvoker_Void_t1_29_StringU26_t1_1690,
	RuntimeInvoker_Object_t_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Char_t1_15_Object_t_Int32U26_t1_1678_CharU26_t1_1716,
	RuntimeInvoker_Void_t1_29_Object_t_UriFormatExceptionU26_t3_220,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Sign_t4_16_Object_t_Object_t,
	RuntimeInvoker_ConfidenceFactor_t4_20,
	RuntimeInvoker_X509ChainStatusFlags_t4_43,
	RuntimeInvoker_Void_t1_29_Byte_t1_11,
	RuntimeInvoker_Void_t1_29_Byte_t1_11_Byte_t1_11,
	RuntimeInvoker_AlertLevel_t4_55,
	RuntimeInvoker_AlertDescription_t4_56,
	RuntimeInvoker_Object_t_Byte_t1_11,
	RuntimeInvoker_Void_t1_29_Int16_t1_13_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_Int16_t1_13_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_CipherAlgorithmType_t4_58,
	RuntimeInvoker_HashAlgorithmType_t4_76,
	RuntimeInvoker_ExchangeAlgorithmType_t4_74,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int16_t1_13,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int64_t1_7,
	RuntimeInvoker_Void_t1_29_Object_t_ByteU5BU5DU26_t1_1699_ByteU5BU5DU26_t1_1699,
	RuntimeInvoker_Object_t_Byte_t1_11_Object_t,
	RuntimeInvoker_Object_t_Int16_t1_13_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_Int16_t1_13_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_SecurityProtocolType_t4_86,
	RuntimeInvoker_SecurityCompressionType_t4_85,
	RuntimeInvoker_HandshakeType_t4_99,
	RuntimeInvoker_HandshakeState_t4_75,
	RuntimeInvoker_SecurityProtocolType_t4_86_Int16_t1_13,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t1_11_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t1_11_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Byte_t1_11_Object_t,
	RuntimeInvoker_Object_t_Byte_t1_11_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_SByte_t1_12_Int32_t1_3_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Byte_t1_11_Byte_t1_11_Object_t,
	RuntimeInvoker_RSAParameters_t1_592,
	RuntimeInvoker_Void_t1_29_Object_t_Byte_t1_11_Byte_t1_11,
	RuntimeInvoker_Void_t1_29_Object_t_Byte_t1_11_Object_t,
	RuntimeInvoker_ContentType_t4_69,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_Byte_t1_11,
	RuntimeInvoker_Object_t_SByte_t1_12_Object_t_Byte_t1_11_SByte_t1_12,
	RuntimeInvoker_ConnectionProtocol_t5_36,
	RuntimeInvoker_PhotonSocketState_t5_24,
	RuntimeInvoker_PhotonSocketError_t5_25_Object_t_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Byte_t1_11,
	RuntimeInvoker_Boolean_t1_20_Object_t_StringU26_t1_1690_UInt16U26_t1_1686,
	RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_Object_t_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_DebugLevel_t5_37,
	RuntimeInvoker_PeerStateValue_t5_35,
	RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int16_t1_13_Object_t_Object_t,
	RuntimeInvoker_GpType_t5_45_Object_t,
	RuntimeInvoker_Object_t_SByte_t1_12_Int16_t1_13,
	RuntimeInvoker_Void_t1_29_Object_t_Int16_t1_13_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Int16_t1_13_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Single_t1_17_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Single_t1_17_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_Object_t_Double_t1_18_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_BooleanU26_t1_1679_BooleanU26_t1_1679,
	RuntimeInvoker_Void_t1_29_Int16U26_t1_1685_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_Int32U26_t1_1678_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_SingleU26_t1_1726_Object_t_Int32U26_t1_1678,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int16_t1_13_ArrayU26_t1_1727,
	RuntimeInvoker_Object_t_Object_t_ByteU26_t1_1683_ByteU26_t1_1683,
	RuntimeInvoker_Void_t1_29_Object_t_Double_t1_18,
	RuntimeInvoker_Void_t1_29_Int64_t1_7_Object_t,
	RuntimeInvoker_Void_t1_29_GcAchievementDescriptionData_t6_204_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_GcUserProfileData_t6_203_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Double_t1_18_Object_t,
	RuntimeInvoker_Void_t1_29_Int64_t1_7_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_UserProfileU5BU5DU26_t6_303_Object_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_UserProfileU5BU5DU26_t6_303_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_GcScoreData_t6_206,
	RuntimeInvoker_Boolean_t1_20_BoneWeight_t6_24_BoneWeight_t6_24,
	RuntimeInvoker_Object_t_Vector3_t6_49,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t6_304,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_SByte_t1_12_IntPtr_t,
	RuntimeInvoker_Void_t1_29_Object_t_IntPtr_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_CullingGroupEvent_t6_36,
	RuntimeInvoker_Object_t_CullingGroupEvent_t6_36_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Color_t6_40_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_Object_t,
	RuntimeInvoker_Void_t1_29_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t6_305_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_Object_t,
	RuntimeInvoker_Void_t1_29_Vector3_t6_49_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Vector3U26_t6_304_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Vector3_t6_49_Vector3_t6_49,
	RuntimeInvoker_Void_t1_29_Vector3U26_t6_304_Vector3U26_t6_304,
	RuntimeInvoker_Void_t1_29_Color_t6_40,
	RuntimeInvoker_Void_t1_29_ColorU26_t6_306,
	RuntimeInvoker_Int32_t1_3_LayerMask_t6_47,
	RuntimeInvoker_LayerMask_t6_47_Int32_t1_3,
	RuntimeInvoker_Single_t1_17_Vector2_t6_48,
	RuntimeInvoker_Vector2_t6_48,
	RuntimeInvoker_Vector2_t6_48_Vector2_t6_48_Vector2_t6_48,
	RuntimeInvoker_Vector2_t6_48_Vector2_t6_48,
	RuntimeInvoker_Vector2_t6_48_Vector2_t6_48_Single_t1_17,
	RuntimeInvoker_Boolean_t1_20_Vector2_t6_48_Vector2_t6_48,
	RuntimeInvoker_Vector2_t6_48_Vector3_t6_49,
	RuntimeInvoker_Vector3_t6_49_Vector2_t6_48,
	RuntimeInvoker_Void_t1_29_Single_t1_17_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Vector3_t6_49_Vector3_t6_49_Vector3_t6_49_Single_t1_17,
	RuntimeInvoker_Vector3_t6_49_Vector3U26_t6_304_Vector3U26_t6_304_Single_t1_17,
	RuntimeInvoker_Vector3_t6_49_Vector3_t6_49_Vector3_t6_49_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Vector3_t6_49_Vector3U26_t6_304_Vector3U26_t6_304_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Vector3_t6_49_Vector3_t6_49,
	RuntimeInvoker_Vector3_t6_49,
	RuntimeInvoker_Single_t1_17_Vector3_t6_49_Vector3_t6_49,
	RuntimeInvoker_Single_t1_17_Vector3_t6_49,
	RuntimeInvoker_Vector3_t6_49_Vector3_t6_49_Vector3_t6_49,
	RuntimeInvoker_Vector3_t6_49_Vector3_t6_49_Single_t1_17,
	RuntimeInvoker_Vector3_t6_49_Single_t1_17_Vector3_t6_49,
	RuntimeInvoker_Boolean_t1_20_Vector3_t6_49_Vector3_t6_49,
	RuntimeInvoker_Void_t1_29_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Color_t6_40,
	RuntimeInvoker_Color_t6_40_Color_t6_40_Single_t1_17,
	RuntimeInvoker_Vector4_t6_55_Color_t6_40,
	RuntimeInvoker_Quaternion_t6_51,
	RuntimeInvoker_Single_t1_17_Quaternion_t6_51_Quaternion_t6_51,
	RuntimeInvoker_Quaternion_t6_51_Vector3_t6_49,
	RuntimeInvoker_Quaternion_t6_51_Vector3U26_t6_304_Vector3U26_t6_304,
	RuntimeInvoker_Quaternion_t6_51_Quaternion_t6_51_Quaternion_t6_51_Single_t1_17,
	RuntimeInvoker_Quaternion_t6_51_QuaternionU26_t6_307_QuaternionU26_t6_307_Single_t1_17,
	RuntimeInvoker_Quaternion_t6_51_Quaternion_t6_51,
	RuntimeInvoker_Quaternion_t6_51_QuaternionU26_t6_307,
	RuntimeInvoker_Quaternion_t6_51_Single_t1_17_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Vector3_t6_49_Quaternion_t6_51,
	RuntimeInvoker_Vector3_t6_49_QuaternionU26_t6_307,
	RuntimeInvoker_Quaternion_t6_51_Vector3U26_t6_304,
	RuntimeInvoker_Quaternion_t6_51_Quaternion_t6_51_Quaternion_t6_51,
	RuntimeInvoker_Vector3_t6_49_Quaternion_t6_51_Vector3_t6_49,
	RuntimeInvoker_Boolean_t1_20_Quaternion_t6_51_Quaternion_t6_51,
	RuntimeInvoker_Void_t1_29_Rect_t6_52,
	RuntimeInvoker_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Boolean_t1_20_Vector2_t6_48,
	RuntimeInvoker_Boolean_t1_20_Vector3_t6_49,
	RuntimeInvoker_Boolean_t1_20_Rect_t6_52_Rect_t6_52,
	RuntimeInvoker_Single_t1_17_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Int32_t1_3_Single_t1_17,
	RuntimeInvoker_Matrix4x4_t6_53_Matrix4x4_t6_53,
	RuntimeInvoker_Matrix4x4_t6_53_Matrix4x4U26_t6_308,
	RuntimeInvoker_Boolean_t1_20_Matrix4x4_t6_53_Matrix4x4U26_t6_308,
	RuntimeInvoker_Boolean_t1_20_Matrix4x4U26_t6_308_Matrix4x4U26_t6_308,
	RuntimeInvoker_Matrix4x4_t6_53,
	RuntimeInvoker_Vector4_t6_55_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Vector4_t6_55,
	RuntimeInvoker_Matrix4x4_t6_53_Vector3_t6_49,
	RuntimeInvoker_Void_t1_29_Vector3_t6_49_Quaternion_t6_51_Vector3_t6_49,
	RuntimeInvoker_Matrix4x4_t6_53_Vector3_t6_49_Quaternion_t6_51_Vector3_t6_49,
	RuntimeInvoker_Matrix4x4_t6_53_Vector3U26_t6_304_QuaternionU26_t6_307_Vector3U26_t6_304,
	RuntimeInvoker_Matrix4x4_t6_53_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Matrix4x4_t6_53_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Matrix4x4_t6_53_Matrix4x4_t6_53_Matrix4x4_t6_53,
	RuntimeInvoker_Vector4_t6_55_Matrix4x4_t6_53_Vector4_t6_55,
	RuntimeInvoker_Boolean_t1_20_Matrix4x4_t6_53_Matrix4x4_t6_53,
	RuntimeInvoker_Void_t1_29_Vector3_t6_49,
	RuntimeInvoker_Void_t1_29_Bounds_t6_54,
	RuntimeInvoker_Boolean_t1_20_Bounds_t6_54,
	RuntimeInvoker_Boolean_t1_20_Bounds_t6_54_Vector3_t6_49,
	RuntimeInvoker_Boolean_t1_20_BoundsU26_t6_309_Vector3U26_t6_304,
	RuntimeInvoker_Single_t1_17_Bounds_t6_54_Vector3_t6_49,
	RuntimeInvoker_Single_t1_17_BoundsU26_t6_309_Vector3U26_t6_304,
	RuntimeInvoker_Boolean_t1_20_RayU26_t6_310_BoundsU26_t6_309_SingleU26_t1_1726,
	RuntimeInvoker_Boolean_t1_20_Ray_t6_56,
	RuntimeInvoker_Boolean_t1_20_Ray_t6_56_SingleU26_t1_1726,
	RuntimeInvoker_Vector3_t6_49_BoundsU26_t6_309_Vector3U26_t6_304,
	RuntimeInvoker_Boolean_t1_20_Bounds_t6_54_Bounds_t6_54,
	RuntimeInvoker_Single_t1_17_Vector4_t6_55_Vector4_t6_55,
	RuntimeInvoker_Single_t1_17_Vector4_t6_55,
	RuntimeInvoker_Vector4_t6_55_Vector4_t6_55_Vector4_t6_55,
	RuntimeInvoker_Boolean_t1_20_Vector4_t6_55_Vector4_t6_55,
	RuntimeInvoker_Vector3_t6_49_Single_t1_17,
	RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Boolean_t1_20_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17_SingleU26_t1_1726_Single_t1_17,
	RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17_SingleU26_t1_1726_Single_t1_17_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17_SingleU26_t1_1726_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Object_t_Color_t6_40,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Color_t6_40,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_ColorU26_t6_306,
	RuntimeInvoker_Void_t1_29_SphericalHarmonicsL2U26_t6_311,
	RuntimeInvoker_Void_t1_29_Color_t6_40_SphericalHarmonicsL2U26_t6_311,
	RuntimeInvoker_Void_t1_29_ColorU26_t6_306_SphericalHarmonicsL2U26_t6_311,
	RuntimeInvoker_Void_t1_29_Vector3_t6_49_Color_t6_40_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Vector3_t6_49_Color_t6_40_SphericalHarmonicsL2U26_t6_311,
	RuntimeInvoker_Void_t1_29_Vector3U26_t6_304_ColorU26_t6_306_SphericalHarmonicsL2U26_t6_311,
	RuntimeInvoker_SphericalHarmonicsL2_t6_68_SphericalHarmonicsL2_t6_68_Single_t1_17,
	RuntimeInvoker_SphericalHarmonicsL2_t6_68_Single_t1_17_SphericalHarmonicsL2_t6_68,
	RuntimeInvoker_SphericalHarmonicsL2_t6_68_SphericalHarmonicsL2_t6_68_SphericalHarmonicsL2_t6_68,
	RuntimeInvoker_Boolean_t1_20_SphericalHarmonicsL2_t6_68_SphericalHarmonicsL2_t6_68,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_RuntimePlatform_t6_8,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Rect_t6_52,
	RuntimeInvoker_Void_t1_29_RectU26_t6_312,
	RuntimeInvoker_CameraClearFlags_t6_209,
	RuntimeInvoker_Ray_t6_56_Vector3_t6_49,
	RuntimeInvoker_Ray_t6_56_Object_t_Vector3U26_t6_304,
	RuntimeInvoker_Object_t_Ray_t6_56_Single_t1_17_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_RayU26_t6_310_Single_t1_17_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_RayU26_t6_310_Single_t1_17_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Vector3_t6_49_Vector3_t6_49_Color_t6_40_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Vector3U26_t6_304_Vector3U26_t6_304_ColorU26_t6_306_Single_t1_17_SByte_t1_12,
	RuntimeInvoker_RenderBuffer_t6_208,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Int32U26_t1_1678_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_IntPtr_t_RenderBufferU26_t6_313_RenderBufferU26_t6_313,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32U26_t1_1678_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_Object_t_Single_t1_17_Single_t1_17,
	RuntimeInvoker_TouchPhase_t6_81,
	RuntimeInvoker_Void_t1_29_Vector3U26_t6_304,
	RuntimeInvoker_Touch_t6_82_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Vector2_t6_48,
	RuntimeInvoker_Void_t1_29_Vector2U26_t6_314,
	RuntimeInvoker_Object_t_Object_t_Vector3_t6_49_Quaternion_t6_51,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t6_304_QuaternionU26_t6_307,
	RuntimeInvoker_Object_t_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12_Object_t,
	RuntimeInvoker_Void_t1_29_Quaternion_t6_51,
	RuntimeInvoker_Void_t1_29_QuaternionU26_t6_307,
	RuntimeInvoker_Void_t1_29_Vector3_t6_49_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Single_t1_17_Single_t1_17_Single_t1_17_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Vector3_t6_49,
	RuntimeInvoker_Void_t1_29_Object_t_Vector3U26_t6_304_Vector3U26_t6_304,
	RuntimeInvoker_Vector3_t6_49_Object_t_Vector3U26_t6_304,
	RuntimeInvoker_Void_t1_29_Object_t_SByte_t1_12_SByte_t1_12_Object_t,
	RuntimeInvoker_Boolean_t1_20_Vector3_t6_49_Vector3_t6_49_RaycastHitU26_t6_315_Single_t1_17_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Ray_t6_56_RaycastHitU26_t6_315_Single_t1_17,
	RuntimeInvoker_Boolean_t1_20_Ray_t6_56_RaycastHitU26_t6_315_Single_t1_17_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Vector3U26_t6_304_Vector3U26_t6_304_RaycastHitU26_t6_315_Single_t1_17_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Vector3U26_t6_304_Int32_t1_3,
	RuntimeInvoker_Bounds_t6_54,
	RuntimeInvoker_Void_t1_29_BoundsU26_t6_309,
	RuntimeInvoker_CollisionFlags_t6_105_Vector3_t6_49,
	RuntimeInvoker_CollisionFlags_t6_105_Object_t_Vector3U26_t6_304,
	RuntimeInvoker_Void_t1_29_Vector2_t6_48_Vector2_t6_48_Single_t1_17_Int32_t1_3_Single_t1_17_Single_t1_17_RaycastHit2DU26_t6_316,
	RuntimeInvoker_Void_t1_29_Vector2U26_t6_314_Vector2U26_t6_314_Single_t1_17_Int32_t1_3_Single_t1_17_Single_t1_17_RaycastHit2DU26_t6_316,
	RuntimeInvoker_RaycastHit2D_t6_110_Vector2_t6_48_Vector2_t6_48_Single_t1_17,
	RuntimeInvoker_RaycastHit2D_t6_110_Vector2_t6_48_Vector2_t6_48_Single_t1_17_Int32_t1_3_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Object_t_Vector2U26_t6_314_Int32_t1_3,
	RuntimeInvoker_Object_t_SByte_t1_12_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_SendMessageOptions_t6_6,
	RuntimeInvoker_AnimatorStateInfo_t6_128,
	RuntimeInvoker_AnimatorClipInfo_t6_129,
	RuntimeInvoker_Void_t1_29_Object_t_Single_t1_17_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Single_t1_17_Single_t1_17_Single_t1_17,
	RuntimeInvoker_AnimatorStateInfo_t6_128_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Int16_t1_13_CharacterInfoU26_t6_317_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Int16_t1_13_CharacterInfoU26_t6_317_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Int16_t1_13_CharacterInfoU26_t6_317,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Color_t6_40_Int32_t1_3_Single_t1_17_Single_t1_17_Int32_t1_3_SByte_t1_12_SByte_t1_12_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3_Vector2_t6_48_Vector2_t6_48_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Color_t6_40_Int32_t1_3_Single_t1_17_Single_t1_17_Int32_t1_3_SByte_t1_12_SByte_t1_12_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_Object_t_ColorU26_t6_306_Int32_t1_3_Single_t1_17_Single_t1_17_Int32_t1_3_SByte_t1_12_SByte_t1_12_Int32_t1_3_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12_Int32_t1_3_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_SByte_t1_12,
	RuntimeInvoker_TextGenerationSettings_t6_152_TextGenerationSettings_t6_152,
	RuntimeInvoker_Single_t1_17_Object_t_TextGenerationSettings_t6_152,
	RuntimeInvoker_Boolean_t1_20_Object_t_TextGenerationSettings_t6_152,
	RuntimeInvoker_Ray_t6_56,
	RuntimeInvoker_Void_t1_29_Ray_t6_56,
	RuntimeInvoker_EventType_t6_156,
	RuntimeInvoker_EventType_t6_156_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Vector2U26_t6_314,
	RuntimeInvoker_EventModifiers_t6_157,
	RuntimeInvoker_KeyCode_t6_155,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_Rect_t6_52_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_Rect_t6_52_Object_t_Object_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_Object_t,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_Object_t_Object_t_Int16_t1_13,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_Object_t_Object_t_Int16_t1_13_Object_t,
	RuntimeInvoker_Boolean_t1_20_Rect_t6_52_SByte_t1_12_Object_t_Object_t,
	RuntimeInvoker_Int32_t1_3_Rect_t6_52_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_GUIStyleU26_t6_318_GUIStyleU26_t6_318_GUIStyleU26_t6_318_GUIStyleU26_t6_318_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t1_3_Rect_t6_52_Int32_t1_3_Object_t_Int32_t1_3_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Rect_t6_52_Int32_t1_3_Int32_t1_3_Single_t1_17_Single_t1_17_Object_t_Object_t_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Int32_t1_3_Object_t_Vector2_t6_48_SByte_t1_12,
	RuntimeInvoker_Single_t1_17_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t,
	RuntimeInvoker_Single_t1_17_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t_SByte_t1_12_Int32_t1_3,
	RuntimeInvoker_Single_t1_17_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Rect_t6_52_Object_t,
	RuntimeInvoker_Single_t1_17_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Vector2_t6_48_Rect_t6_52_Vector2_t6_48_Rect_t6_52_SByte_t1_12_SByte_t1_12_Object_t_Object_t_Object_t,
	RuntimeInvoker_Rect_t6_52_Int32_t1_3_Rect_t6_52_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_Object_t_Int32_t1_3_Single_t1_17_Single_t1_17_Object_t,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_IntPtr_t,
	RuntimeInvoker_Void_t1_29_RectU26_t6_312_Object_t_IntPtr_t,
	RuntimeInvoker_Boolean_t1_20_Rect_t6_52_Object_t_IntPtr_t,
	RuntimeInvoker_Boolean_t1_20_RectU26_t6_312_Object_t_IntPtr_t,
	RuntimeInvoker_Boolean_t1_20_Rect_t6_52_Int32_t1_3_SByte_t1_12_Object_t_IntPtr_t,
	RuntimeInvoker_Boolean_t1_20_RectU26_t6_312_Int32_t1_3_SByte_t1_12_Object_t_IntPtr_t,
	RuntimeInvoker_Rect_t6_52_Int32_t1_3_Rect_t6_52_Object_t_Object_t_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Rect_t6_52_Int32_t1_3_RectU26_t6_312_Object_t_Object_t_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Object_t_Object_t_Int32_t1_3_SByte_t1_12_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3_Object_t_Object_t_Object_t,
	RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t,
	RuntimeInvoker_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t_Object_t,
	RuntimeInvoker_Vector2_t6_48_Vector2_t6_48_Object_t,
	RuntimeInvoker_Vector2_t6_48_Vector2_t6_48_SByte_t1_12_SByte_t1_12_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Rect_t6_52_Int32_t1_3_Rect_t6_52_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Object_t_Rect_t6_52_Object_t_Object_t_Object_t,
	RuntimeInvoker_Rect_t6_52_Object_t_Object_t_Object_t,
	RuntimeInvoker_Rect_t6_52_Single_t1_17_Single_t1_17_Object_t_Object_t,
	RuntimeInvoker_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t,
	RuntimeInvoker_Rect_t6_52_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Rect_t6_52,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_RectU26_t6_312,
	RuntimeInvoker_Void_t1_29_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t,
	RuntimeInvoker_Void_t1_29_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t,
	RuntimeInvoker_Rect_t6_52_Rect_t6_52,
	RuntimeInvoker_Rect_t6_52_Object_t_RectU26_t6_312,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Rect_t6_52_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_SByte_t1_12_SByte_t1_12_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Vector2_t6_48_Rect_t6_52_Object_t_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Rect_t6_52_Object_t_Vector2_t6_48,
	RuntimeInvoker_Vector2_t6_48_Object_t,
	RuntimeInvoker_Single_t1_17_Object_t_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Object_t_SingleU26_t1_1726_SingleU26_t1_1726,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_IntPtr_t,
	RuntimeInvoker_ImagePosition_t6_182,
	RuntimeInvoker_Single_t1_17_IntPtr_t,
	RuntimeInvoker_Void_t1_29_Object_t_Internal_DrawArgumentsU26_t6_319,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Rect_t6_52_Object_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_IntPtr_t_RectU26_t6_312_Object_t_Int32_t1_3_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Rect_t6_52_Object_t_Int32_t1_3_Color_t6_40,
	RuntimeInvoker_Void_t1_29_IntPtr_t_RectU26_t6_312_Object_t_Int32_t1_3_ColorU26_t6_306,
	RuntimeInvoker_Void_t1_29_Object_t_Internal_DrawWithTextSelectionArgumentsU26_t6_320,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Rect_t6_52_Object_t_Int32_t1_3_Vector2U26_t6_314,
	RuntimeInvoker_Void_t1_29_IntPtr_t_RectU26_t6_312_Object_t_Int32_t1_3_Vector2U26_t6_314,
	RuntimeInvoker_Int32_t1_3_IntPtr_t_Rect_t6_52_Object_t_Vector2_t6_48,
	RuntimeInvoker_Int32_t1_3_IntPtr_t_RectU26_t6_312_Object_t_Vector2U26_t6_314,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Object_t_Vector2U26_t6_314,
	RuntimeInvoker_Single_t1_17_IntPtr_t_Object_t_Single_t1_17,
	RuntimeInvoker_Void_t1_29_IntPtr_t_Object_t_SingleU26_t1_1726_SingleU26_t1_1726,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_Rect_t6_52,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_RectU26_t6_312,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Vector2_t6_48_Vector2_t6_48_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_RectU26_t6_312_Vector2U26_t6_314_Vector2U26_t6_314_SByte_t1_12,
	RuntimeInvoker_UserState_t6_225,
	RuntimeInvoker_Void_t1_29_Object_t_Double_t1_18_SByte_t1_12_SByte_t1_12_DateTime_t1_127,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1_12_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_Int64_t1_7_Object_t_DateTime_t1_127_Object_t_Int32_t1_3,
	RuntimeInvoker_UserScope_t6_226,
	RuntimeInvoker_Range_t6_219,
	RuntimeInvoker_Void_t1_29_Range_t6_219,
	RuntimeInvoker_TimeScope_t6_227,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_HitInfo_t6_221,
	RuntimeInvoker_Boolean_t1_20_HitInfo_t6_221_HitInfo_t6_221,
	RuntimeInvoker_Boolean_t1_20_HitInfo_t6_221,
	RuntimeInvoker_Void_t1_29_Rect_t6_52_Single_t1_17_Single_t1_17_Single_t1_17_Single_t1_17_Object_t_Object_t_SByte_t1_12_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Object_t_StringU26_t1_1690_StringU26_t1_1690,
	RuntimeInvoker_CharacterType_t6_236_Int16_t1_13,
	RuntimeInvoker_Boolean_t1_20_Color_t6_40_Color_t6_40,
	RuntimeInvoker_Boolean_t1_20_TextGenerationSettings_t6_152,
	RuntimeInvoker_ChatState_t7_13,
	RuntimeInvoker_ChatDisconnectCause_t7_6,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Object_t_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_ChatChannelU26_t7_16,
	RuntimeInvoker_Boolean_t1_20_Object_t_ChatChannelU26_t7_16,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_Int32_t1_3,
	RuntimeInvoker_CustomAuthenticationType_t7_10,
	RuntimeInvoker_Void_t1_29_Object_t_Int32_t1_3_SByte_t1_12_Object_t,
	RuntimeInvoker_Object_t_Vector2_t6_48,
	RuntimeInvoker_Color_t6_40_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Vector3_t6_49_Vector3_t6_49_Single_t1_17,
	RuntimeInvoker_Boolean_t1_20_Vector2_t6_48_Vector2_t6_48_Single_t1_17,
	RuntimeInvoker_Boolean_t1_20_Quaternion_t6_51_Quaternion_t6_51_Single_t1_17,
	RuntimeInvoker_Boolean_t1_20_Single_t1_17_Single_t1_17_Single_t1_17,
	RuntimeInvoker_Void_t1_29_Vector3_t6_49_Int32_t1_3_Color_t6_40_Single_t1_17,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_SByte_t1_12_Object_t_SByte_t1_12_Object_t,
	RuntimeInvoker_CustomAuthenticationType_t8_96,
	RuntimeInvoker_ServerConnection_t8_67,
	RuntimeInvoker_PeerState_t8_69,
	RuntimeInvoker_CloudRegionCode_t8_65,
	RuntimeInvoker_Object_t_Object_t_Vector3_t6_49_Quaternion_t6_51_Int32_t1_3_Object_t_Object_t_SByte_t1_12,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Object_t_SByte_t1_12_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_SByte_t1_12_Object_t,
	RuntimeInvoker_Void_t1_29_Object_t_Object_t_Int32_t1_3_Int16_t1_13,
	RuntimeInvoker_Boolean_t1_20_Object_t_Object_t_MethodInfoU26_t1_1728,
	RuntimeInvoker_Void_t1_29_Int32U26_t1_1678,
	RuntimeInvoker_Void_t1_29_CharU26_t1_1716,
	RuntimeInvoker_Void_t1_29_Int16U26_t1_1685,
	RuntimeInvoker_Void_t1_29_SingleU26_t1_1726,
	RuntimeInvoker_Void_t1_29_PhotonPlayerU26_t8_191,
	RuntimeInvoker_ConnectionState_t8_68,
	RuntimeInvoker_Boolean_t1_20_Object_t_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_SByte_t1_12_Byte_t1_11_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Vector3_t6_49_Quaternion_t6_51_Int32_t1_3,
	RuntimeInvoker_Object_t_Object_t_Vector3_t6_49_Quaternion_t6_51_Int32_t1_3_Object_t,
	RuntimeInvoker_Void_t1_29_SByte_t1_12_Object_t_Int32_t1_3,
	RuntimeInvoker_Object_t_SByte_t1_12_Object_t_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_CloudRegionCode_t8_65_Object_t,
	RuntimeInvoker_CloudRegionFlag_t8_66_Object_t,
	RuntimeInvoker_SynchronizeType_t8_127_Int32_t1_3,
	RuntimeInvoker_SynchronizeType_t8_127_Object_t,
	RuntimeInvoker_Void_t1_29_Vector3_t6_49_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Quaternion_t6_51_Object_t_Object_t,
	RuntimeInvoker_Void_t1_29_Double_t1_18_Object_t,
	RuntimeInvoker_Team_t8_169_Object_t,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_ObjectU26_t1_1704,
	RuntimeInvoker_Void_t1_29_ObjectU5BU5DU26_t1_1676_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_ObjectU5BU5DU26_t1_1676_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1044,
	RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1044,
	RuntimeInvoker_KeyValuePair_2_t1_1044_Object_t_Object_t,
	RuntimeInvoker_Boolean_t1_20_Object_t_ObjectU26_t1_1704,
	RuntimeInvoker_Enumerator_t1_1046,
	RuntimeInvoker_DictionaryEntry_t1_167_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1_1044,
	RuntimeInvoker_Enumerator_t1_894,
	RuntimeInvoker_Enumerator_t1_1049,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t,
	RuntimeInvoker_Enumerator_t1_975,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int32_t1_3_Object_t,
	RuntimeInvoker_Enumerator_t2_23,
	RuntimeInvoker_Enumerator_t3_193,
	RuntimeInvoker_Enumerator_t3_195,
	RuntimeInvoker_Enumerator_t3_197,
	RuntimeInvoker_Enumerator_t1_1180,
	RuntimeInvoker_Enumerator_t1_1162,
	RuntimeInvoker_Enumerator_t1_903,
	RuntimeInvoker_KeyValuePair_2_t1_902,
	RuntimeInvoker_Enumerator_t1_1314,
	RuntimeInvoker_Team_t8_169,
	RuntimeInvoker_Enumerator_t1_1166,
	RuntimeInvoker_Enumerator_t1_1163,
	RuntimeInvoker_KeyValuePair_2_t1_1159,
	RuntimeInvoker_ParameterModifier_t1_352_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_ParameterModifier_t1_352,
	RuntimeInvoker_Boolean_t1_20_ParameterModifier_t1_352,
	RuntimeInvoker_Int32_t1_3_ParameterModifier_t1_352,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_ParameterModifier_t1_352,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Double_t1_18,
	RuntimeInvoker_TableRange_t1_66_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_TableRange_t1_66,
	RuntimeInvoker_Boolean_t1_20_TableRange_t1_66,
	RuntimeInvoker_Int32_t1_3_TableRange_t1_66,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_TableRange_t1_66,
	RuntimeInvoker_KeyValuePair_2_t1_1016_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1016,
	RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1016,
	RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1016,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1016,
	RuntimeInvoker_Link_t1_150_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Link_t1_150,
	RuntimeInvoker_Boolean_t1_20_Link_t1_150,
	RuntimeInvoker_Int32_t1_3_Link_t1_150,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Link_t1_150,
	RuntimeInvoker_DictionaryEntry_t1_167_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_DictionaryEntry_t1_167,
	RuntimeInvoker_Boolean_t1_20_DictionaryEntry_t1_167,
	RuntimeInvoker_Int32_t1_3_DictionaryEntry_t1_167,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_DictionaryEntry_t1_167,
	RuntimeInvoker_KeyValuePair_2_t1_1044_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1044,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1044,
	RuntimeInvoker_Slot_t1_168_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Slot_t1_168,
	RuntimeInvoker_Boolean_t1_20_Slot_t1_168,
	RuntimeInvoker_Int32_t1_3_Slot_t1_168,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Slot_t1_168,
	RuntimeInvoker_Slot_t1_183_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Slot_t1_183,
	RuntimeInvoker_Boolean_t1_20_Slot_t1_183,
	RuntimeInvoker_Int32_t1_3_Slot_t1_183,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Slot_t1_183,
	RuntimeInvoker_ILTokenInfo_t1_283_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_ILTokenInfo_t1_283,
	RuntimeInvoker_Boolean_t1_20_ILTokenInfo_t1_283,
	RuntimeInvoker_Int32_t1_3_ILTokenInfo_t1_283,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_ILTokenInfo_t1_283,
	RuntimeInvoker_LabelData_t1_285_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_LabelData_t1_285,
	RuntimeInvoker_Boolean_t1_20_LabelData_t1_285,
	RuntimeInvoker_Int32_t1_3_LabelData_t1_285,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_LabelData_t1_285,
	RuntimeInvoker_LabelFixup_t1_284_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_LabelFixup_t1_284,
	RuntimeInvoker_Boolean_t1_20_LabelFixup_t1_284,
	RuntimeInvoker_Int32_t1_3_LabelFixup_t1_284,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_LabelFixup_t1_284,
	RuntimeInvoker_CustomAttributeTypedArgument_t1_332_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_CustomAttributeTypedArgument_t1_332,
	RuntimeInvoker_Boolean_t1_20_CustomAttributeTypedArgument_t1_332,
	RuntimeInvoker_Int32_t1_3_CustomAttributeTypedArgument_t1_332,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_CustomAttributeTypedArgument_t1_332,
	RuntimeInvoker_CustomAttributeNamedArgument_t1_331_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_CustomAttributeNamedArgument_t1_331,
	RuntimeInvoker_Boolean_t1_20_CustomAttributeNamedArgument_t1_331,
	RuntimeInvoker_Int32_t1_3_CustomAttributeNamedArgument_t1_331,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_CustomAttributeNamedArgument_t1_331,
	RuntimeInvoker_Void_t1_29_CustomAttributeTypedArgumentU5BU5DU26_t1_1729_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_CustomAttributeTypedArgumentU5BU5DU26_t1_1729_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_CustomAttributeTypedArgument_t1_332_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_CustomAttributeTypedArgument_t1_332,
	RuntimeInvoker_Void_t1_29_CustomAttributeNamedArgumentU5BU5DU26_t1_1730_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_CustomAttributeNamedArgumentU5BU5DU26_t1_1730_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_CustomAttributeNamedArgument_t1_331_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_CustomAttributeNamedArgument_t1_331,
	RuntimeInvoker_ResourceInfo_t1_363_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_ResourceInfo_t1_363,
	RuntimeInvoker_Boolean_t1_20_ResourceInfo_t1_363,
	RuntimeInvoker_Int32_t1_3_ResourceInfo_t1_363,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_ResourceInfo_t1_363,
	RuntimeInvoker_ResourceCacheItem_t1_364_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_ResourceCacheItem_t1_364,
	RuntimeInvoker_Boolean_t1_20_ResourceCacheItem_t1_364,
	RuntimeInvoker_Int32_t1_3_ResourceCacheItem_t1_364,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_ResourceCacheItem_t1_364,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_DateTime_t1_127,
	RuntimeInvoker_Void_t1_29_Decimal_t1_19,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Decimal_t1_19,
	RuntimeInvoker_TimeSpan_t1_213_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_TimeSpan_t1_213,
	RuntimeInvoker_TypeTag_t1_510_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Byte_t1_11,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Byte_t1_11,
	RuntimeInvoker_Link_t2_22_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Link_t2_22,
	RuntimeInvoker_Boolean_t1_20_Link_t2_22,
	RuntimeInvoker_Int32_t1_3_Link_t2_22,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Link_t2_22,
	RuntimeInvoker_KeyValuePair_2_t1_1126_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1126,
	RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1126,
	RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1126,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1126,
	RuntimeInvoker_X509ChainStatus_t3_87_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_X509ChainStatus_t3_87,
	RuntimeInvoker_Boolean_t1_20_X509ChainStatus_t3_87,
	RuntimeInvoker_Int32_t1_3_X509ChainStatus_t3_87,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_X509ChainStatus_t3_87,
	RuntimeInvoker_Int32_t1_3_Object_t_Int32_t1_3_Int32_t1_3_Int32_t1_3_Object_t,
	RuntimeInvoker_Mark_t3_134_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Mark_t3_134,
	RuntimeInvoker_Boolean_t1_20_Mark_t3_134,
	RuntimeInvoker_Int32_t1_3_Mark_t3_134,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Mark_t3_134,
	RuntimeInvoker_UriScheme_t3_169_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_UriScheme_t3_169,
	RuntimeInvoker_Boolean_t1_20_UriScheme_t3_169,
	RuntimeInvoker_Int32_t1_3_UriScheme_t3_169,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_UriScheme_t3_169,
	RuntimeInvoker_ClientCertificateType_t4_98_Int32_t1_3,
	RuntimeInvoker_KeyValuePair_2_t1_1159_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1159,
	RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1159,
	RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1159,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1159,
	RuntimeInvoker_KeyValuePair_2_t1_902_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_902,
	RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_902,
	RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_902,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_902,
	RuntimeInvoker_GcAchievementData_t6_205_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_GcAchievementData_t6_205,
	RuntimeInvoker_Boolean_t1_20_GcAchievementData_t6_205,
	RuntimeInvoker_Int32_t1_3_GcAchievementData_t6_205,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_GcAchievementData_t6_205,
	RuntimeInvoker_GcScoreData_t6_206_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_GcScoreData_t6_206,
	RuntimeInvoker_Int32_t1_3_GcScoreData_t6_206,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_GcScoreData_t6_206,
	RuntimeInvoker_ContactPoint_t6_104_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_ContactPoint_t6_104,
	RuntimeInvoker_Boolean_t1_20_ContactPoint_t6_104,
	RuntimeInvoker_Int32_t1_3_ContactPoint_t6_104,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_ContactPoint_t6_104,
	RuntimeInvoker_ContactPoint2D_t6_114_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_ContactPoint2D_t6_114,
	RuntimeInvoker_Boolean_t1_20_ContactPoint2D_t6_114,
	RuntimeInvoker_Int32_t1_3_ContactPoint2D_t6_114,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_ContactPoint2D_t6_114,
	RuntimeInvoker_Keyframe_t6_131_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Keyframe_t6_131,
	RuntimeInvoker_Boolean_t1_20_Keyframe_t6_131,
	RuntimeInvoker_Int32_t1_3_Keyframe_t6_131,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Keyframe_t6_131,
	RuntimeInvoker_CharacterInfo_t6_146_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_CharacterInfo_t6_146,
	RuntimeInvoker_Boolean_t1_20_CharacterInfo_t6_146,
	RuntimeInvoker_Int32_t1_3_CharacterInfo_t6_146,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_CharacterInfo_t6_146,
	RuntimeInvoker_UIVertex_t6_153_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_UIVertex_t6_153,
	RuntimeInvoker_Boolean_t1_20_UIVertex_t6_153,
	RuntimeInvoker_Int32_t1_3_UIVertex_t6_153,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_UIVertex_t6_153,
	RuntimeInvoker_Void_t1_29_UIVertexU5BU5DU26_t6_321_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_UIVertexU5BU5DU26_t6_321_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_UIVertex_t6_153_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_UICharInfo_t6_149_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_UICharInfo_t6_149,
	RuntimeInvoker_Boolean_t1_20_UICharInfo_t6_149,
	RuntimeInvoker_Int32_t1_3_UICharInfo_t6_149,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_UICharInfo_t6_149,
	RuntimeInvoker_Void_t1_29_UICharInfoU5BU5DU26_t6_322_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_UICharInfoU5BU5DU26_t6_322_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_UICharInfo_t6_149_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_UILineInfo_t6_150_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_UILineInfo_t6_150,
	RuntimeInvoker_Boolean_t1_20_UILineInfo_t6_150,
	RuntimeInvoker_Int32_t1_3_UILineInfo_t6_150,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_UILineInfo_t6_150,
	RuntimeInvoker_Void_t1_29_UILineInfoU5BU5DU26_t6_323_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_UILineInfoU5BU5DU26_t6_323_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_UILineInfo_t6_150_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Rect_t6_52,
	RuntimeInvoker_Int32_t1_3_Rect_t6_52,
	RuntimeInvoker_HitInfo_t6_221_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_HitInfo_t6_221,
	RuntimeInvoker_Int32_t1_3_HitInfo_t6_221,
	RuntimeInvoker_KeyValuePair_2_t1_1266_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1266,
	RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1266,
	RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1266,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1266,
	RuntimeInvoker_TextEditOp_t6_237_Int32_t1_3,
	RuntimeInvoker_KeyValuePair_2_t1_1294_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1294,
	RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1294,
	RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1294,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1294,
	RuntimeInvoker_ConnectionProtocol_t5_36_Int32_t1_3,
	RuntimeInvoker_KeyValuePair_2_t1_1317_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_KeyValuePair_2_t1_1317,
	RuntimeInvoker_Boolean_t1_20_KeyValuePair_2_t1_1317,
	RuntimeInvoker_Int32_t1_3_KeyValuePair_2_t1_1317,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_KeyValuePair_2_t1_1317,
	RuntimeInvoker_Team_t8_169_Int32_t1_3,
	RuntimeInvoker_State_t8_41_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_State_t8_41,
	RuntimeInvoker_Boolean_t1_20_State_t8_41,
	RuntimeInvoker_Int32_t1_3_State_t8_41,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_State_t8_41,
	RuntimeInvoker_Boolean_t1_20_Color_t6_40,
	RuntimeInvoker_Int32_t1_3_Color_t6_40,
	RuntimeInvoker_Link_t2_29_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Link_t2_29,
	RuntimeInvoker_Boolean_t1_20_Link_t2_29,
	RuntimeInvoker_Int32_t1_3_Link_t2_29,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Link_t2_29,
	RuntimeInvoker_Void_t1_29_Int32U5BU5DU26_t1_1731_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_Int32U5BU5DU26_t1_1731_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_ByteU5BU5DU26_t1_1699_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_ByteU5BU5DU26_t1_1699_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_SByte_t1_12_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Vector3_t6_49_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Vector3_t6_49,
	RuntimeInvoker_Void_t1_29_Int32_t1_3_Vector3_t6_49,
	RuntimeInvoker_Void_t1_29_SingleU5BU5DU26_t1_1732_Int32_t1_3,
	RuntimeInvoker_Void_t1_29_SingleU5BU5DU26_t1_1732_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Object_t_Single_t1_17_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_ParameterModifier_t1_352,
	RuntimeInvoker_TableRange_t1_66,
	RuntimeInvoker_KeyValuePair_2_t1_1016_Object_t_Int32_t1_3,
	RuntimeInvoker_Enumerator_t1_1021,
	RuntimeInvoker_DictionaryEntry_t1_167_Object_t_Int32_t1_3,
	RuntimeInvoker_KeyValuePair_2_t1_1016,
	RuntimeInvoker_Link_t1_150,
	RuntimeInvoker_Enumerator_t1_1020,
	RuntimeInvoker_Enumerator_t1_1024,
	RuntimeInvoker_DictionaryEntry_t1_167_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1_1016_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1_1044_Object_t,
	RuntimeInvoker_Slot_t1_168,
	RuntimeInvoker_Slot_t1_183,
	RuntimeInvoker_ILTokenInfo_t1_283,
	RuntimeInvoker_LabelData_t1_285,
	RuntimeInvoker_LabelFixup_t1_284,
	RuntimeInvoker_CustomAttributeTypedArgument_t1_332,
	RuntimeInvoker_CustomAttributeNamedArgument_t1_331,
	RuntimeInvoker_CustomAttributeTypedArgument_t1_332_Object_t,
	RuntimeInvoker_Enumerator_t1_1074,
	RuntimeInvoker_Boolean_t1_20_CustomAttributeTypedArgument_t1_332_CustomAttributeTypedArgument_t1_332,
	RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1_332_Object_t_Object_t,
	RuntimeInvoker_CustomAttributeNamedArgument_t1_331_Object_t,
	RuntimeInvoker_Enumerator_t1_1082,
	RuntimeInvoker_Boolean_t1_20_CustomAttributeNamedArgument_t1_331_CustomAttributeNamedArgument_t1_331,
	RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1_331_Object_t_Object_t,
	RuntimeInvoker_ResourceInfo_t1_363,
	RuntimeInvoker_ResourceCacheItem_t1_364,
	RuntimeInvoker_TypeTag_t1_510,
	RuntimeInvoker_Int32_t1_3_DateTimeOffset_t1_707_DateTimeOffset_t1_707,
	RuntimeInvoker_Boolean_t1_20_DateTimeOffset_t1_707_DateTimeOffset_t1_707,
	RuntimeInvoker_Boolean_t1_20_Nullable_1_t1_826,
	RuntimeInvoker_Int32_t1_3_Guid_t1_730_Guid_t1_730,
	RuntimeInvoker_Boolean_t1_20_Guid_t1_730_Guid_t1_730,
	RuntimeInvoker_Link_t2_22,
	RuntimeInvoker_KeyValuePair_2_t1_1126_Object_t_SByte_t1_12,
	RuntimeInvoker_Boolean_t1_20_Object_t_BooleanU26_t1_1679,
	RuntimeInvoker_Enumerator_t1_1130,
	RuntimeInvoker_DictionaryEntry_t1_167_Object_t_SByte_t1_12,
	RuntimeInvoker_KeyValuePair_2_t1_1126,
	RuntimeInvoker_Enumerator_t1_1129,
	RuntimeInvoker_Object_t_Object_t_SByte_t1_12_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1_1133,
	RuntimeInvoker_KeyValuePair_2_t1_1126_Object_t,
	RuntimeInvoker_Boolean_t1_20_SByte_t1_12_SByte_t1_12,
	RuntimeInvoker_X509ChainStatus_t3_87,
	RuntimeInvoker_Mark_t3_134,
	RuntimeInvoker_UriScheme_t3_169,
	RuntimeInvoker_ClientCertificateType_t4_98,
	RuntimeInvoker_KeyValuePair_2_t1_1159_Int32_t1_3_Object_t,
	RuntimeInvoker_Int32_t1_3_Int32_t1_3_Object_t,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_ObjectU26_t1_1704,
	RuntimeInvoker_DictionaryEntry_t1_167_Int32_t1_3_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1_1159_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1_902_SByte_t1_12_Object_t,
	RuntimeInvoker_Byte_t1_11_SByte_t1_12_Object_t,
	RuntimeInvoker_Object_t_SByte_t1_12_Object_t,
	RuntimeInvoker_Boolean_t1_20_SByte_t1_12_ObjectU26_t1_1704,
	RuntimeInvoker_DictionaryEntry_t1_167_SByte_t1_12_Object_t,
	RuntimeInvoker_Enumerator_t1_1177,
	RuntimeInvoker_Object_t_SByte_t1_12_Object_t_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1_902_Object_t,
	RuntimeInvoker_Enumerator_t3_201,
	RuntimeInvoker_GcAchievementData_t6_205,
	RuntimeInvoker_GcScoreData_t6_206,
	RuntimeInvoker_ContactPoint_t6_104,
	RuntimeInvoker_ContactPoint2D_t6_114,
	RuntimeInvoker_Keyframe_t6_131,
	RuntimeInvoker_CharacterInfo_t6_146,
	RuntimeInvoker_UIVertex_t6_153,
	RuntimeInvoker_Enumerator_t1_1228,
	RuntimeInvoker_Boolean_t1_20_UIVertex_t6_153_UIVertex_t6_153,
	RuntimeInvoker_Object_t_UIVertex_t6_153_Object_t_Object_t,
	RuntimeInvoker_UICharInfo_t6_149,
	RuntimeInvoker_Enumerator_t1_1233,
	RuntimeInvoker_Boolean_t1_20_UICharInfo_t6_149_UICharInfo_t6_149,
	RuntimeInvoker_Object_t_UICharInfo_t6_149_Object_t_Object_t,
	RuntimeInvoker_UILineInfo_t6_150,
	RuntimeInvoker_Enumerator_t1_1238,
	RuntimeInvoker_Boolean_t1_20_UILineInfo_t6_150_UILineInfo_t6_150,
	RuntimeInvoker_Object_t_UILineInfo_t6_150_Object_t_Object_t,
	RuntimeInvoker_HitInfo_t6_221,
	RuntimeInvoker_TextEditOp_t6_237_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1_1266_Object_t_Int32_t1_3,
	RuntimeInvoker_TextEditOp_t6_237_Object_t_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Object_t_TextEditOpU26_t6_324,
	RuntimeInvoker_Enumerator_t1_1271,
	RuntimeInvoker_KeyValuePair_2_t1_1266,
	RuntimeInvoker_TextEditOp_t6_237,
	RuntimeInvoker_Enumerator_t1_1270,
	RuntimeInvoker_Enumerator_t1_1274,
	RuntimeInvoker_KeyValuePair_2_t1_1266_Object_t,
	RuntimeInvoker_Void_t1_29_Byte_t1_11_Int32_t1_3,
	RuntimeInvoker_KeyValuePair_2_t1_1294_Byte_t1_11_Int32_t1_3,
	RuntimeInvoker_ConnectionProtocol_t5_36_Byte_t1_11_Int32_t1_3,
	RuntimeInvoker_Int32_t1_3_Byte_t1_11_Int32_t1_3,
	RuntimeInvoker_Boolean_t1_20_Byte_t1_11_Int32U26_t1_1678,
	RuntimeInvoker_ConnectionProtocol_t5_36_Object_t,
	RuntimeInvoker_Enumerator_t1_1299,
	RuntimeInvoker_DictionaryEntry_t1_167_Byte_t1_11_Int32_t1_3,
	RuntimeInvoker_KeyValuePair_2_t1_1294,
	RuntimeInvoker_Enumerator_t1_1298,
	RuntimeInvoker_Object_t_Byte_t1_11_Int32_t1_3_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1_1302,
	RuntimeInvoker_KeyValuePair_2_t1_1294_Object_t,
	RuntimeInvoker_Boolean_t1_20_Byte_t1_11_Byte_t1_11,
	RuntimeInvoker_KeyValuePair_2_t1_1317,
	RuntimeInvoker_KeyValuePair_2_t1_1317_Byte_t1_11_Object_t,
	RuntimeInvoker_Team_t8_169_Byte_t1_11_Object_t,
	RuntimeInvoker_Boolean_t1_20_Byte_t1_11_ObjectU26_t1_1704,
	RuntimeInvoker_Enumerator_t1_1315,
	RuntimeInvoker_DictionaryEntry_t1_167_Byte_t1_11_Object_t,
	RuntimeInvoker_Enumerator_t1_1325,
	RuntimeInvoker_KeyValuePair_2_t1_1317_Object_t,
	RuntimeInvoker_State_t8_41,
	RuntimeInvoker_Boolean_t1_20_Int32_t1_3_Int32_t1_3_Int32_t1_3,
	RuntimeInvoker_Enumerator_t2_30,
	RuntimeInvoker_Link_t2_29,
	RuntimeInvoker_Enumerator_t1_1374,
	RuntimeInvoker_Enumerator_t1_1379,
	RuntimeInvoker_Enumerator_t3_204,
	RuntimeInvoker_Enumerator_t1_1396,
	RuntimeInvoker_Object_t_Single_t1_17_Object_t_Object_t,
};
