﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void Object__ctor_m1_0 ();
extern "C" void Object_Equals_m1_1 ();
extern "C" void Object_Equals_m1_2 ();
extern "C" void Object_Finalize_m1_3 ();
extern "C" void Object_GetHashCode_m1_4 ();
extern "C" void Object_GetType_m1_5 ();
extern "C" void Object_MemberwiseClone_m1_6 ();
extern "C" void Object_ToString_m1_7 ();
extern "C" void Object_ReferenceEquals_m1_8 ();
extern "C" void Object_InternalGetHashCode_m1_9 ();
extern "C" void ValueType__ctor_m1_10 ();
extern "C" void ValueType_InternalEquals_m1_11 ();
extern "C" void ValueType_DefaultEquals_m1_12 ();
extern "C" void ValueType_Equals_m1_13 ();
extern "C" void ValueType_InternalGetHashCode_m1_14 ();
extern "C" void ValueType_GetHashCode_m1_15 ();
extern "C" void ValueType_ToString_m1_16 ();
extern "C" void Attribute__ctor_m1_17 ();
extern "C" void Attribute_CheckParameters_m1_18 ();
extern "C" void Attribute_GetCustomAttribute_m1_19 ();
extern "C" void Attribute_GetCustomAttribute_m1_20 ();
extern "C" void Attribute_GetHashCode_m1_21 ();
extern "C" void Attribute_IsDefined_m1_22 ();
extern "C" void Attribute_IsDefined_m1_23 ();
extern "C" void Attribute_IsDefined_m1_24 ();
extern "C" void Attribute_IsDefined_m1_25 ();
extern "C" void Attribute_Equals_m1_26 ();
extern "C" void Int32_System_IConvertible_ToBoolean_m1_27 ();
extern "C" void Int32_System_IConvertible_ToByte_m1_28 ();
extern "C" void Int32_System_IConvertible_ToChar_m1_29 ();
extern "C" void Int32_System_IConvertible_ToDateTime_m1_30 ();
extern "C" void Int32_System_IConvertible_ToDecimal_m1_31 ();
extern "C" void Int32_System_IConvertible_ToDouble_m1_32 ();
extern "C" void Int32_System_IConvertible_ToInt16_m1_33 ();
extern "C" void Int32_System_IConvertible_ToInt32_m1_34 ();
extern "C" void Int32_System_IConvertible_ToInt64_m1_35 ();
extern "C" void Int32_System_IConvertible_ToSByte_m1_36 ();
extern "C" void Int32_System_IConvertible_ToSingle_m1_37 ();
extern "C" void Int32_System_IConvertible_ToType_m1_38 ();
extern "C" void Int32_System_IConvertible_ToUInt16_m1_39 ();
extern "C" void Int32_System_IConvertible_ToUInt32_m1_40 ();
extern "C" void Int32_System_IConvertible_ToUInt64_m1_41 ();
extern "C" void Int32_CompareTo_m1_42 ();
extern "C" void Int32_Equals_m1_43 ();
extern "C" void Int32_GetHashCode_m1_44 ();
extern "C" void Int32_CompareTo_m1_45 ();
extern "C" void Int32_Equals_m1_46 ();
extern "C" void Int32_ProcessTrailingWhitespace_m1_47 ();
extern "C" void Int32_Parse_m1_48 ();
extern "C" void Int32_Parse_m1_49 ();
extern "C" void Int32_CheckStyle_m1_50 ();
extern "C" void Int32_JumpOverWhite_m1_51 ();
extern "C" void Int32_FindSign_m1_52 ();
extern "C" void Int32_FindCurrency_m1_53 ();
extern "C" void Int32_FindExponent_m1_54 ();
extern "C" void Int32_FindOther_m1_55 ();
extern "C" void Int32_ValidDigit_m1_56 ();
extern "C" void Int32_GetFormatException_m1_57 ();
extern "C" void Int32_Parse_m1_58 ();
extern "C" void Int32_Parse_m1_59 ();
extern "C" void Int32_Parse_m1_60 ();
extern "C" void Int32_TryParse_m1_61 ();
extern "C" void Int32_TryParse_m1_62 ();
extern "C" void Int32_ToString_m1_63 ();
extern "C" void Int32_ToString_m1_64 ();
extern "C" void Int32_ToString_m1_65 ();
extern "C" void Int32_ToString_m1_66 ();
extern "C" void SerializableAttribute__ctor_m1_67 ();
extern "C" void AttributeUsageAttribute__ctor_m1_68 ();
extern "C" void AttributeUsageAttribute_get_AllowMultiple_m1_69 ();
extern "C" void AttributeUsageAttribute_set_AllowMultiple_m1_70 ();
extern "C" void AttributeUsageAttribute_get_Inherited_m1_71 ();
extern "C" void AttributeUsageAttribute_set_Inherited_m1_72 ();
extern "C" void ComVisibleAttribute__ctor_m1_73 ();
extern "C" void Int64_System_IConvertible_ToBoolean_m1_74 ();
extern "C" void Int64_System_IConvertible_ToByte_m1_75 ();
extern "C" void Int64_System_IConvertible_ToChar_m1_76 ();
extern "C" void Int64_System_IConvertible_ToDateTime_m1_77 ();
extern "C" void Int64_System_IConvertible_ToDecimal_m1_78 ();
extern "C" void Int64_System_IConvertible_ToDouble_m1_79 ();
extern "C" void Int64_System_IConvertible_ToInt16_m1_80 ();
extern "C" void Int64_System_IConvertible_ToInt32_m1_81 ();
extern "C" void Int64_System_IConvertible_ToInt64_m1_82 ();
extern "C" void Int64_System_IConvertible_ToSByte_m1_83 ();
extern "C" void Int64_System_IConvertible_ToSingle_m1_84 ();
extern "C" void Int64_System_IConvertible_ToType_m1_85 ();
extern "C" void Int64_System_IConvertible_ToUInt16_m1_86 ();
extern "C" void Int64_System_IConvertible_ToUInt32_m1_87 ();
extern "C" void Int64_System_IConvertible_ToUInt64_m1_88 ();
extern "C" void Int64_CompareTo_m1_89 ();
extern "C" void Int64_Equals_m1_90 ();
extern "C" void Int64_GetHashCode_m1_91 ();
extern "C" void Int64_CompareTo_m1_92 ();
extern "C" void Int64_Equals_m1_93 ();
extern "C" void Int64_Parse_m1_94 ();
extern "C" void Int64_Parse_m1_95 ();
extern "C" void Int64_Parse_m1_96 ();
extern "C" void Int64_Parse_m1_97 ();
extern "C" void Int64_Parse_m1_98 ();
extern "C" void Int64_TryParse_m1_99 ();
extern "C" void Int64_TryParse_m1_100 ();
extern "C" void Int64_ToString_m1_101 ();
extern "C" void Int64_ToString_m1_102 ();
extern "C" void Int64_ToString_m1_103 ();
extern "C" void Int64_ToString_m1_104 ();
extern "C" void UInt32_System_IConvertible_ToBoolean_m1_105 ();
extern "C" void UInt32_System_IConvertible_ToByte_m1_106 ();
extern "C" void UInt32_System_IConvertible_ToChar_m1_107 ();
extern "C" void UInt32_System_IConvertible_ToDateTime_m1_108 ();
extern "C" void UInt32_System_IConvertible_ToDecimal_m1_109 ();
extern "C" void UInt32_System_IConvertible_ToDouble_m1_110 ();
extern "C" void UInt32_System_IConvertible_ToInt16_m1_111 ();
extern "C" void UInt32_System_IConvertible_ToInt32_m1_112 ();
extern "C" void UInt32_System_IConvertible_ToInt64_m1_113 ();
extern "C" void UInt32_System_IConvertible_ToSByte_m1_114 ();
extern "C" void UInt32_System_IConvertible_ToSingle_m1_115 ();
extern "C" void UInt32_System_IConvertible_ToType_m1_116 ();
extern "C" void UInt32_System_IConvertible_ToUInt16_m1_117 ();
extern "C" void UInt32_System_IConvertible_ToUInt32_m1_118 ();
extern "C" void UInt32_System_IConvertible_ToUInt64_m1_119 ();
extern "C" void UInt32_CompareTo_m1_120 ();
extern "C" void UInt32_Equals_m1_121 ();
extern "C" void UInt32_GetHashCode_m1_122 ();
extern "C" void UInt32_CompareTo_m1_123 ();
extern "C" void UInt32_Equals_m1_124 ();
extern "C" void UInt32_Parse_m1_125 ();
extern "C" void UInt32_Parse_m1_126 ();
extern "C" void UInt32_Parse_m1_127 ();
extern "C" void UInt32_Parse_m1_128 ();
extern "C" void UInt32_TryParse_m1_129 ();
extern "C" void UInt32_TryParse_m1_130 ();
extern "C" void UInt32_ToString_m1_131 ();
extern "C" void UInt32_ToString_m1_132 ();
extern "C" void UInt32_ToString_m1_133 ();
extern "C" void UInt32_ToString_m1_134 ();
extern "C" void CLSCompliantAttribute__ctor_m1_135 ();
extern "C" void UInt64_System_IConvertible_ToBoolean_m1_136 ();
extern "C" void UInt64_System_IConvertible_ToByte_m1_137 ();
extern "C" void UInt64_System_IConvertible_ToChar_m1_138 ();
extern "C" void UInt64_System_IConvertible_ToDateTime_m1_139 ();
extern "C" void UInt64_System_IConvertible_ToDecimal_m1_140 ();
extern "C" void UInt64_System_IConvertible_ToDouble_m1_141 ();
extern "C" void UInt64_System_IConvertible_ToInt16_m1_142 ();
extern "C" void UInt64_System_IConvertible_ToInt32_m1_143 ();
extern "C" void UInt64_System_IConvertible_ToInt64_m1_144 ();
extern "C" void UInt64_System_IConvertible_ToSByte_m1_145 ();
extern "C" void UInt64_System_IConvertible_ToSingle_m1_146 ();
extern "C" void UInt64_System_IConvertible_ToType_m1_147 ();
extern "C" void UInt64_System_IConvertible_ToUInt16_m1_148 ();
extern "C" void UInt64_System_IConvertible_ToUInt32_m1_149 ();
extern "C" void UInt64_System_IConvertible_ToUInt64_m1_150 ();
extern "C" void UInt64_CompareTo_m1_151 ();
extern "C" void UInt64_Equals_m1_152 ();
extern "C" void UInt64_GetHashCode_m1_153 ();
extern "C" void UInt64_CompareTo_m1_154 ();
extern "C" void UInt64_Equals_m1_155 ();
extern "C" void UInt64_Parse_m1_156 ();
extern "C" void UInt64_Parse_m1_157 ();
extern "C" void UInt64_Parse_m1_158 ();
extern "C" void UInt64_TryParse_m1_159 ();
extern "C" void UInt64_ToString_m1_160 ();
extern "C" void UInt64_ToString_m1_161 ();
extern "C" void UInt64_ToString_m1_162 ();
extern "C" void UInt64_ToString_m1_163 ();
extern "C" void Byte_System_IConvertible_ToType_m1_164 ();
extern "C" void Byte_System_IConvertible_ToBoolean_m1_165 ();
extern "C" void Byte_System_IConvertible_ToByte_m1_166 ();
extern "C" void Byte_System_IConvertible_ToChar_m1_167 ();
extern "C" void Byte_System_IConvertible_ToDateTime_m1_168 ();
extern "C" void Byte_System_IConvertible_ToDecimal_m1_169 ();
extern "C" void Byte_System_IConvertible_ToDouble_m1_170 ();
extern "C" void Byte_System_IConvertible_ToInt16_m1_171 ();
extern "C" void Byte_System_IConvertible_ToInt32_m1_172 ();
extern "C" void Byte_System_IConvertible_ToInt64_m1_173 ();
extern "C" void Byte_System_IConvertible_ToSByte_m1_174 ();
extern "C" void Byte_System_IConvertible_ToSingle_m1_175 ();
extern "C" void Byte_System_IConvertible_ToUInt16_m1_176 ();
extern "C" void Byte_System_IConvertible_ToUInt32_m1_177 ();
extern "C" void Byte_System_IConvertible_ToUInt64_m1_178 ();
extern "C" void Byte_CompareTo_m1_179 ();
extern "C" void Byte_Equals_m1_180 ();
extern "C" void Byte_GetHashCode_m1_181 ();
extern "C" void Byte_CompareTo_m1_182 ();
extern "C" void Byte_Equals_m1_183 ();
extern "C" void Byte_Parse_m1_184 ();
extern "C" void Byte_Parse_m1_185 ();
extern "C" void Byte_Parse_m1_186 ();
extern "C" void Byte_TryParse_m1_187 ();
extern "C" void Byte_TryParse_m1_188 ();
extern "C" void Byte_ToString_m1_189 ();
extern "C" void Byte_ToString_m1_190 ();
extern "C" void Byte_ToString_m1_191 ();
extern "C" void Byte_ToString_m1_192 ();
extern "C" void SByte_System_IConvertible_ToBoolean_m1_193 ();
extern "C" void SByte_System_IConvertible_ToByte_m1_194 ();
extern "C" void SByte_System_IConvertible_ToChar_m1_195 ();
extern "C" void SByte_System_IConvertible_ToDateTime_m1_196 ();
extern "C" void SByte_System_IConvertible_ToDecimal_m1_197 ();
extern "C" void SByte_System_IConvertible_ToDouble_m1_198 ();
extern "C" void SByte_System_IConvertible_ToInt16_m1_199 ();
extern "C" void SByte_System_IConvertible_ToInt32_m1_200 ();
extern "C" void SByte_System_IConvertible_ToInt64_m1_201 ();
extern "C" void SByte_System_IConvertible_ToSByte_m1_202 ();
extern "C" void SByte_System_IConvertible_ToSingle_m1_203 ();
extern "C" void SByte_System_IConvertible_ToType_m1_204 ();
extern "C" void SByte_System_IConvertible_ToUInt16_m1_205 ();
extern "C" void SByte_System_IConvertible_ToUInt32_m1_206 ();
extern "C" void SByte_System_IConvertible_ToUInt64_m1_207 ();
extern "C" void SByte_CompareTo_m1_208 ();
extern "C" void SByte_Equals_m1_209 ();
extern "C" void SByte_GetHashCode_m1_210 ();
extern "C" void SByte_CompareTo_m1_211 ();
extern "C" void SByte_Equals_m1_212 ();
extern "C" void SByte_Parse_m1_213 ();
extern "C" void SByte_Parse_m1_214 ();
extern "C" void SByte_Parse_m1_215 ();
extern "C" void SByte_TryParse_m1_216 ();
extern "C" void SByte_ToString_m1_217 ();
extern "C" void SByte_ToString_m1_218 ();
extern "C" void SByte_ToString_m1_219 ();
extern "C" void SByte_ToString_m1_220 ();
extern "C" void Int16_System_IConvertible_ToBoolean_m1_221 ();
extern "C" void Int16_System_IConvertible_ToByte_m1_222 ();
extern "C" void Int16_System_IConvertible_ToChar_m1_223 ();
extern "C" void Int16_System_IConvertible_ToDateTime_m1_224 ();
extern "C" void Int16_System_IConvertible_ToDecimal_m1_225 ();
extern "C" void Int16_System_IConvertible_ToDouble_m1_226 ();
extern "C" void Int16_System_IConvertible_ToInt16_m1_227 ();
extern "C" void Int16_System_IConvertible_ToInt32_m1_228 ();
extern "C" void Int16_System_IConvertible_ToInt64_m1_229 ();
extern "C" void Int16_System_IConvertible_ToSByte_m1_230 ();
extern "C" void Int16_System_IConvertible_ToSingle_m1_231 ();
extern "C" void Int16_System_IConvertible_ToType_m1_232 ();
extern "C" void Int16_System_IConvertible_ToUInt16_m1_233 ();
extern "C" void Int16_System_IConvertible_ToUInt32_m1_234 ();
extern "C" void Int16_System_IConvertible_ToUInt64_m1_235 ();
extern "C" void Int16_CompareTo_m1_236 ();
extern "C" void Int16_Equals_m1_237 ();
extern "C" void Int16_GetHashCode_m1_238 ();
extern "C" void Int16_CompareTo_m1_239 ();
extern "C" void Int16_Equals_m1_240 ();
extern "C" void Int16_Parse_m1_241 ();
extern "C" void Int16_Parse_m1_242 ();
extern "C" void Int16_Parse_m1_243 ();
extern "C" void Int16_TryParse_m1_244 ();
extern "C" void Int16_ToString_m1_245 ();
extern "C" void Int16_ToString_m1_246 ();
extern "C" void Int16_ToString_m1_247 ();
extern "C" void Int16_ToString_m1_248 ();
extern "C" void UInt16_System_IConvertible_ToBoolean_m1_249 ();
extern "C" void UInt16_System_IConvertible_ToByte_m1_250 ();
extern "C" void UInt16_System_IConvertible_ToChar_m1_251 ();
extern "C" void UInt16_System_IConvertible_ToDateTime_m1_252 ();
extern "C" void UInt16_System_IConvertible_ToDecimal_m1_253 ();
extern "C" void UInt16_System_IConvertible_ToDouble_m1_254 ();
extern "C" void UInt16_System_IConvertible_ToInt16_m1_255 ();
extern "C" void UInt16_System_IConvertible_ToInt32_m1_256 ();
extern "C" void UInt16_System_IConvertible_ToInt64_m1_257 ();
extern "C" void UInt16_System_IConvertible_ToSByte_m1_258 ();
extern "C" void UInt16_System_IConvertible_ToSingle_m1_259 ();
extern "C" void UInt16_System_IConvertible_ToType_m1_260 ();
extern "C" void UInt16_System_IConvertible_ToUInt16_m1_261 ();
extern "C" void UInt16_System_IConvertible_ToUInt32_m1_262 ();
extern "C" void UInt16_System_IConvertible_ToUInt64_m1_263 ();
extern "C" void UInt16_CompareTo_m1_264 ();
extern "C" void UInt16_Equals_m1_265 ();
extern "C" void UInt16_GetHashCode_m1_266 ();
extern "C" void UInt16_CompareTo_m1_267 ();
extern "C" void UInt16_Equals_m1_268 ();
extern "C" void UInt16_Parse_m1_269 ();
extern "C" void UInt16_Parse_m1_270 ();
extern "C" void UInt16_TryParse_m1_271 ();
extern "C" void UInt16_TryParse_m1_272 ();
extern "C" void UInt16_ToString_m1_273 ();
extern "C" void UInt16_ToString_m1_274 ();
extern "C" void UInt16_ToString_m1_275 ();
extern "C" void UInt16_ToString_m1_276 ();
extern "C" void Char__cctor_m1_277 ();
extern "C" void Char_System_IConvertible_ToType_m1_278 ();
extern "C" void Char_System_IConvertible_ToBoolean_m1_279 ();
extern "C" void Char_System_IConvertible_ToByte_m1_280 ();
extern "C" void Char_System_IConvertible_ToChar_m1_281 ();
extern "C" void Char_System_IConvertible_ToDateTime_m1_282 ();
extern "C" void Char_System_IConvertible_ToDecimal_m1_283 ();
extern "C" void Char_System_IConvertible_ToDouble_m1_284 ();
extern "C" void Char_System_IConvertible_ToInt16_m1_285 ();
extern "C" void Char_System_IConvertible_ToInt32_m1_286 ();
extern "C" void Char_System_IConvertible_ToInt64_m1_287 ();
extern "C" void Char_System_IConvertible_ToSByte_m1_288 ();
extern "C" void Char_System_IConvertible_ToSingle_m1_289 ();
extern "C" void Char_System_IConvertible_ToUInt16_m1_290 ();
extern "C" void Char_System_IConvertible_ToUInt32_m1_291 ();
extern "C" void Char_System_IConvertible_ToUInt64_m1_292 ();
extern "C" void Char_GetDataTablePointers_m1_293 ();
extern "C" void Char_CompareTo_m1_294 ();
extern "C" void Char_Equals_m1_295 ();
extern "C" void Char_CompareTo_m1_296 ();
extern "C" void Char_Equals_m1_297 ();
extern "C" void Char_GetHashCode_m1_298 ();
extern "C" void Char_GetUnicodeCategory_m1_299 ();
extern "C" void Char_IsDigit_m1_300 ();
extern "C" void Char_IsLetter_m1_301 ();
extern "C" void Char_IsLetterOrDigit_m1_302 ();
extern "C" void Char_IsLower_m1_303 ();
extern "C" void Char_IsSurrogate_m1_304 ();
extern "C" void Char_IsWhiteSpace_m1_305 ();
extern "C" void Char_IsWhiteSpace_m1_306 ();
extern "C" void Char_CheckParameter_m1_307 ();
extern "C" void Char_Parse_m1_308 ();
extern "C" void Char_ToLower_m1_309 ();
extern "C" void Char_ToLowerInvariant_m1_310 ();
extern "C" void Char_ToLower_m1_311 ();
extern "C" void Char_ToUpper_m1_312 ();
extern "C" void Char_ToUpperInvariant_m1_313 ();
extern "C" void Char_ToString_m1_314 ();
extern "C" void Char_ToString_m1_315 ();
extern "C" void String__ctor_m1_316 ();
extern "C" void String__ctor_m1_317 ();
extern "C" void String__ctor_m1_318 ();
extern "C" void String__ctor_m1_319 ();
extern "C" void String__cctor_m1_320 ();
extern "C" void String_System_IConvertible_ToBoolean_m1_321 ();
extern "C" void String_System_IConvertible_ToByte_m1_322 ();
extern "C" void String_System_IConvertible_ToChar_m1_323 ();
extern "C" void String_System_IConvertible_ToDateTime_m1_324 ();
extern "C" void String_System_IConvertible_ToDecimal_m1_325 ();
extern "C" void String_System_IConvertible_ToDouble_m1_326 ();
extern "C" void String_System_IConvertible_ToInt16_m1_327 ();
extern "C" void String_System_IConvertible_ToInt32_m1_328 ();
extern "C" void String_System_IConvertible_ToInt64_m1_329 ();
extern "C" void String_System_IConvertible_ToSByte_m1_330 ();
extern "C" void String_System_IConvertible_ToSingle_m1_331 ();
extern "C" void String_System_IConvertible_ToType_m1_332 ();
extern "C" void String_System_IConvertible_ToUInt16_m1_333 ();
extern "C" void String_System_IConvertible_ToUInt32_m1_334 ();
extern "C" void String_System_IConvertible_ToUInt64_m1_335 ();
extern "C" void String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m1_336 ();
extern "C" void String_System_Collections_IEnumerable_GetEnumerator_m1_337 ();
extern "C" void String_Equals_m1_338 ();
extern "C" void String_Equals_m1_339 ();
extern "C" void String_Equals_m1_340 ();
extern "C" void String_get_Chars_m1_341 ();
extern "C" void String_Clone_m1_342 ();
extern "C" void String_CopyTo_m1_343 ();
extern "C" void String_ToCharArray_m1_344 ();
extern "C" void String_ToCharArray_m1_345 ();
extern "C" void String_Split_m1_346 ();
extern "C" void String_Split_m1_347 ();
extern "C" void String_Split_m1_348 ();
extern "C" void String_Split_m1_349 ();
extern "C" void String_Split_m1_350 ();
extern "C" void String_Substring_m1_351 ();
extern "C" void String_Substring_m1_352 ();
extern "C" void String_SubstringUnchecked_m1_353 ();
extern "C" void String_Trim_m1_354 ();
extern "C" void String_Trim_m1_355 ();
extern "C" void String_TrimStart_m1_356 ();
extern "C" void String_TrimEnd_m1_357 ();
extern "C" void String_FindNotWhiteSpace_m1_358 ();
extern "C" void String_FindNotInTable_m1_359 ();
extern "C" void String_Compare_m1_360 ();
extern "C" void String_Compare_m1_361 ();
extern "C" void String_Compare_m1_362 ();
extern "C" void String_Compare_m1_363 ();
extern "C" void String_Compare_m1_364 ();
extern "C" void String_Equals_m1_365 ();
extern "C" void String_CompareTo_m1_366 ();
extern "C" void String_CompareTo_m1_367 ();
extern "C" void String_CompareOrdinal_m1_368 ();
extern "C" void String_CompareOrdinalUnchecked_m1_369 ();
extern "C" void String_CompareOrdinalCaseInsensitiveUnchecked_m1_370 ();
extern "C" void String_EndsWith_m1_371 ();
extern "C" void String_IndexOfAny_m1_372 ();
extern "C" void String_IndexOfAny_m1_373 ();
extern "C" void String_IndexOfAny_m1_374 ();
extern "C" void String_IndexOfAnyUnchecked_m1_375 ();
extern "C" void String_IndexOf_m1_376 ();
extern "C" void String_IndexOf_m1_377 ();
extern "C" void String_IndexOfOrdinal_m1_378 ();
extern "C" void String_IndexOfOrdinalUnchecked_m1_379 ();
extern "C" void String_IndexOfOrdinalIgnoreCaseUnchecked_m1_380 ();
extern "C" void String_IndexOf_m1_381 ();
extern "C" void String_IndexOf_m1_382 ();
extern "C" void String_IndexOf_m1_383 ();
extern "C" void String_IndexOfUnchecked_m1_384 ();
extern "C" void String_IndexOf_m1_385 ();
extern "C" void String_IndexOf_m1_386 ();
extern "C" void String_IndexOf_m1_387 ();
extern "C" void String_LastIndexOfAny_m1_388 ();
extern "C" void String_LastIndexOfAnyUnchecked_m1_389 ();
extern "C" void String_LastIndexOf_m1_390 ();
extern "C" void String_LastIndexOf_m1_391 ();
extern "C" void String_LastIndexOf_m1_392 ();
extern "C" void String_LastIndexOfUnchecked_m1_393 ();
extern "C" void String_LastIndexOf_m1_394 ();
extern "C" void String_LastIndexOf_m1_395 ();
extern "C" void String_Contains_m1_396 ();
extern "C" void String_IsNullOrEmpty_m1_397 ();
extern "C" void String_PadRight_m1_398 ();
extern "C" void String_StartsWith_m1_399 ();
extern "C" void String_Replace_m1_400 ();
extern "C" void String_Replace_m1_401 ();
extern "C" void String_ReplaceUnchecked_m1_402 ();
extern "C" void String_ReplaceFallback_m1_403 ();
extern "C" void String_Remove_m1_404 ();
extern "C" void String_ToLower_m1_405 ();
extern "C" void String_ToLower_m1_406 ();
extern "C" void String_ToLowerInvariant_m1_407 ();
extern "C" void String_ToString_m1_408 ();
extern "C" void String_ToString_m1_409 ();
extern "C" void String_Format_m1_410 ();
extern "C" void String_Format_m1_411 ();
extern "C" void String_Format_m1_412 ();
extern "C" void String_Format_m1_413 ();
extern "C" void String_Format_m1_414 ();
extern "C" void String_FormatHelper_m1_415 ();
extern "C" void String_Concat_m1_416 ();
extern "C" void String_Concat_m1_417 ();
extern "C" void String_Concat_m1_418 ();
extern "C" void String_Concat_m1_419 ();
extern "C" void String_Concat_m1_420 ();
extern "C" void String_Concat_m1_421 ();
extern "C" void String_Concat_m1_422 ();
extern "C" void String_ConcatInternal_m1_423 ();
extern "C" void String_Insert_m1_424 ();
extern "C" void String_Join_m1_425 ();
extern "C" void String_Join_m1_426 ();
extern "C" void String_JoinUnchecked_m1_427 ();
extern "C" void String_get_Length_m1_428 ();
extern "C" void String_ParseFormatSpecifier_m1_429 ();
extern "C" void String_ParseDecimal_m1_430 ();
extern "C" void String_InternalSetChar_m1_431 ();
extern "C" void String_InternalSetLength_m1_432 ();
extern "C" void String_GetHashCode_m1_433 ();
extern "C" void String_GetCaseInsensitiveHashCode_m1_434 ();
extern "C" void String_CreateString_m1_435 ();
extern "C" void String_CreateString_m1_436 ();
extern "C" void String_CreateString_m1_437 ();
extern "C" void String_CreateString_m1_438 ();
extern "C" void String_CreateString_m1_439 ();
extern "C" void String_CreateString_m1_440 ();
extern "C" void String_CreateString_m1_441 ();
extern "C" void String_CreateString_m1_442 ();
extern "C" void String_memcpy4_m1_443 ();
extern "C" void String_memcpy2_m1_444 ();
extern "C" void String_memcpy1_m1_445 ();
extern "C" void String_memcpy_m1_446 ();
extern "C" void String_CharCopy_m1_447 ();
extern "C" void String_CharCopyReverse_m1_448 ();
extern "C" void String_CharCopy_m1_449 ();
extern "C" void String_CharCopy_m1_450 ();
extern "C" void String_CharCopyReverse_m1_451 ();
extern "C" void String_InternalSplit_m1_452 ();
extern "C" void String_InternalAllocateStr_m1_453 ();
extern "C" void String_op_Equality_m1_454 ();
extern "C" void String_op_Inequality_m1_455 ();
extern "C" void Single_System_IConvertible_ToBoolean_m1_456 ();
extern "C" void Single_System_IConvertible_ToByte_m1_457 ();
extern "C" void Single_System_IConvertible_ToChar_m1_458 ();
extern "C" void Single_System_IConvertible_ToDateTime_m1_459 ();
extern "C" void Single_System_IConvertible_ToDecimal_m1_460 ();
extern "C" void Single_System_IConvertible_ToDouble_m1_461 ();
extern "C" void Single_System_IConvertible_ToInt16_m1_462 ();
extern "C" void Single_System_IConvertible_ToInt32_m1_463 ();
extern "C" void Single_System_IConvertible_ToInt64_m1_464 ();
extern "C" void Single_System_IConvertible_ToSByte_m1_465 ();
extern "C" void Single_System_IConvertible_ToSingle_m1_466 ();
extern "C" void Single_System_IConvertible_ToType_m1_467 ();
extern "C" void Single_System_IConvertible_ToUInt16_m1_468 ();
extern "C" void Single_System_IConvertible_ToUInt32_m1_469 ();
extern "C" void Single_System_IConvertible_ToUInt64_m1_470 ();
extern "C" void Single_CompareTo_m1_471 ();
extern "C" void Single_Equals_m1_472 ();
extern "C" void Single_CompareTo_m1_473 ();
extern "C" void Single_Equals_m1_474 ();
extern "C" void Single_GetHashCode_m1_475 ();
extern "C" void Single_IsInfinity_m1_476 ();
extern "C" void Single_IsNaN_m1_477 ();
extern "C" void Single_IsNegativeInfinity_m1_478 ();
extern "C" void Single_IsPositiveInfinity_m1_479 ();
extern "C" void Single_Parse_m1_480 ();
extern "C" void Single_ToString_m1_481 ();
extern "C" void Single_ToString_m1_482 ();
extern "C" void Single_ToString_m1_483 ();
extern "C" void Single_ToString_m1_484 ();
extern "C" void Double_System_IConvertible_ToType_m1_485 ();
extern "C" void Double_System_IConvertible_ToBoolean_m1_486 ();
extern "C" void Double_System_IConvertible_ToByte_m1_487 ();
extern "C" void Double_System_IConvertible_ToChar_m1_488 ();
extern "C" void Double_System_IConvertible_ToDateTime_m1_489 ();
extern "C" void Double_System_IConvertible_ToDecimal_m1_490 ();
extern "C" void Double_System_IConvertible_ToDouble_m1_491 ();
extern "C" void Double_System_IConvertible_ToInt16_m1_492 ();
extern "C" void Double_System_IConvertible_ToInt32_m1_493 ();
extern "C" void Double_System_IConvertible_ToInt64_m1_494 ();
extern "C" void Double_System_IConvertible_ToSByte_m1_495 ();
extern "C" void Double_System_IConvertible_ToSingle_m1_496 ();
extern "C" void Double_System_IConvertible_ToUInt16_m1_497 ();
extern "C" void Double_System_IConvertible_ToUInt32_m1_498 ();
extern "C" void Double_System_IConvertible_ToUInt64_m1_499 ();
extern "C" void Double_CompareTo_m1_500 ();
extern "C" void Double_Equals_m1_501 ();
extern "C" void Double_CompareTo_m1_502 ();
extern "C" void Double_Equals_m1_503 ();
extern "C" void Double_GetHashCode_m1_504 ();
extern "C" void Double_IsInfinity_m1_505 ();
extern "C" void Double_IsNaN_m1_506 ();
extern "C" void Double_IsNegativeInfinity_m1_507 ();
extern "C" void Double_IsPositiveInfinity_m1_508 ();
extern "C" void Double_Parse_m1_509 ();
extern "C" void Double_Parse_m1_510 ();
extern "C" void Double_Parse_m1_511 ();
extern "C" void Double_Parse_m1_512 ();
extern "C" void Double_TryParseStringConstant_m1_513 ();
extern "C" void Double_ParseImpl_m1_514 ();
extern "C" void Double_ToString_m1_515 ();
extern "C" void Double_ToString_m1_516 ();
extern "C" void Double_ToString_m1_517 ();
extern "C" void Decimal__ctor_m1_518 ();
extern "C" void Decimal__ctor_m1_519 ();
extern "C" void Decimal__ctor_m1_520 ();
extern "C" void Decimal__ctor_m1_521 ();
extern "C" void Decimal__ctor_m1_522 ();
extern "C" void Decimal__ctor_m1_523 ();
extern "C" void Decimal__ctor_m1_524 ();
extern "C" void Decimal__cctor_m1_525 ();
extern "C" void Decimal_System_IConvertible_ToType_m1_526 ();
extern "C" void Decimal_System_IConvertible_ToBoolean_m1_527 ();
extern "C" void Decimal_System_IConvertible_ToByte_m1_528 ();
extern "C" void Decimal_System_IConvertible_ToChar_m1_529 ();
extern "C" void Decimal_System_IConvertible_ToDateTime_m1_530 ();
extern "C" void Decimal_System_IConvertible_ToDecimal_m1_531 ();
extern "C" void Decimal_System_IConvertible_ToDouble_m1_532 ();
extern "C" void Decimal_System_IConvertible_ToInt16_m1_533 ();
extern "C" void Decimal_System_IConvertible_ToInt32_m1_534 ();
extern "C" void Decimal_System_IConvertible_ToInt64_m1_535 ();
extern "C" void Decimal_System_IConvertible_ToSByte_m1_536 ();
extern "C" void Decimal_System_IConvertible_ToSingle_m1_537 ();
extern "C" void Decimal_System_IConvertible_ToUInt16_m1_538 ();
extern "C" void Decimal_System_IConvertible_ToUInt32_m1_539 ();
extern "C" void Decimal_System_IConvertible_ToUInt64_m1_540 ();
extern "C" void Decimal_GetBits_m1_541 ();
extern "C" void Decimal_Add_m1_542 ();
extern "C" void Decimal_Subtract_m1_543 ();
extern "C" void Decimal_GetHashCode_m1_544 ();
extern "C" void Decimal_u64_m1_545 ();
extern "C" void Decimal_s64_m1_546 ();
extern "C" void Decimal_Equals_m1_547 ();
extern "C" void Decimal_Equals_m1_548 ();
extern "C" void Decimal_IsZero_m1_549 ();
extern "C" void Decimal_Floor_m1_550 ();
extern "C" void Decimal_Multiply_m1_551 ();
extern "C" void Decimal_Divide_m1_552 ();
extern "C" void Decimal_Compare_m1_553 ();
extern "C" void Decimal_CompareTo_m1_554 ();
extern "C" void Decimal_CompareTo_m1_555 ();
extern "C" void Decimal_Equals_m1_556 ();
extern "C" void Decimal_Parse_m1_557 ();
extern "C" void Decimal_ThrowAtPos_m1_558 ();
extern "C" void Decimal_ThrowInvalidExp_m1_559 ();
extern "C" void Decimal_stripStyles_m1_560 ();
extern "C" void Decimal_Parse_m1_561 ();
extern "C" void Decimal_PerformParse_m1_562 ();
extern "C" void Decimal_ToString_m1_563 ();
extern "C" void Decimal_ToString_m1_564 ();
extern "C" void Decimal_ToString_m1_565 ();
extern "C" void Decimal_decimal2UInt64_m1_566 ();
extern "C" void Decimal_decimal2Int64_m1_567 ();
extern "C" void Decimal_decimalIncr_m1_568 ();
extern "C" void Decimal_string2decimal_m1_569 ();
extern "C" void Decimal_decimalSetExponent_m1_570 ();
extern "C" void Decimal_decimal2double_m1_571 ();
extern "C" void Decimal_decimalFloorAndTrunc_m1_572 ();
extern "C" void Decimal_decimalMult_m1_573 ();
extern "C" void Decimal_decimalDiv_m1_574 ();
extern "C" void Decimal_decimalCompare_m1_575 ();
extern "C" void Decimal_op_Increment_m1_576 ();
extern "C" void Decimal_op_Subtraction_m1_577 ();
extern "C" void Decimal_op_Multiply_m1_578 ();
extern "C" void Decimal_op_Division_m1_579 ();
extern "C" void Decimal_op_Explicit_m1_580 ();
extern "C" void Decimal_op_Explicit_m1_581 ();
extern "C" void Decimal_op_Explicit_m1_582 ();
extern "C" void Decimal_op_Explicit_m1_583 ();
extern "C" void Decimal_op_Explicit_m1_584 ();
extern "C" void Decimal_op_Explicit_m1_585 ();
extern "C" void Decimal_op_Explicit_m1_586 ();
extern "C" void Decimal_op_Explicit_m1_587 ();
extern "C" void Decimal_op_Implicit_m1_588 ();
extern "C" void Decimal_op_Implicit_m1_589 ();
extern "C" void Decimal_op_Implicit_m1_590 ();
extern "C" void Decimal_op_Implicit_m1_591 ();
extern "C" void Decimal_op_Implicit_m1_592 ();
extern "C" void Decimal_op_Implicit_m1_593 ();
extern "C" void Decimal_op_Implicit_m1_594 ();
extern "C" void Decimal_op_Implicit_m1_595 ();
extern "C" void Decimal_op_Explicit_m1_596 ();
extern "C" void Decimal_op_Explicit_m1_597 ();
extern "C" void Decimal_op_Explicit_m1_598 ();
extern "C" void Decimal_op_Explicit_m1_599 ();
extern "C" void Decimal_op_Inequality_m1_600 ();
extern "C" void Decimal_op_Equality_m1_601 ();
extern "C" void Decimal_op_GreaterThan_m1_602 ();
extern "C" void Decimal_op_LessThan_m1_603 ();
extern "C" void Boolean__cctor_m1_604 ();
extern "C" void Boolean_System_IConvertible_ToType_m1_605 ();
extern "C" void Boolean_System_IConvertible_ToBoolean_m1_606 ();
extern "C" void Boolean_System_IConvertible_ToByte_m1_607 ();
extern "C" void Boolean_System_IConvertible_ToChar_m1_608 ();
extern "C" void Boolean_System_IConvertible_ToDateTime_m1_609 ();
extern "C" void Boolean_System_IConvertible_ToDecimal_m1_610 ();
extern "C" void Boolean_System_IConvertible_ToDouble_m1_611 ();
extern "C" void Boolean_System_IConvertible_ToInt16_m1_612 ();
extern "C" void Boolean_System_IConvertible_ToInt32_m1_613 ();
extern "C" void Boolean_System_IConvertible_ToInt64_m1_614 ();
extern "C" void Boolean_System_IConvertible_ToSByte_m1_615 ();
extern "C" void Boolean_System_IConvertible_ToSingle_m1_616 ();
extern "C" void Boolean_System_IConvertible_ToUInt16_m1_617 ();
extern "C" void Boolean_System_IConvertible_ToUInt32_m1_618 ();
extern "C" void Boolean_System_IConvertible_ToUInt64_m1_619 ();
extern "C" void Boolean_CompareTo_m1_620 ();
extern "C" void Boolean_Equals_m1_621 ();
extern "C" void Boolean_CompareTo_m1_622 ();
extern "C" void Boolean_Equals_m1_623 ();
extern "C" void Boolean_GetHashCode_m1_624 ();
extern "C" void Boolean_Parse_m1_625 ();
extern "C" void Boolean_ToString_m1_626 ();
extern "C" void Boolean_ToString_m1_627 ();
extern "C" void IntPtr__ctor_m1_628 ();
extern "C" void IntPtr__ctor_m1_629 ();
extern "C" void IntPtr__ctor_m1_630 ();
extern "C" void IntPtr__ctor_m1_631 ();
extern "C" void IntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m1_632 ();
extern "C" void IntPtr_get_Size_m1_633 ();
extern "C" void IntPtr_Equals_m1_634 ();
extern "C" void IntPtr_GetHashCode_m1_635 ();
extern "C" void IntPtr_ToInt64_m1_636 ();
extern "C" void IntPtr_ToPointer_m1_637 ();
extern "C" void IntPtr_ToString_m1_638 ();
extern "C" void IntPtr_ToString_m1_639 ();
extern "C" void IntPtr_op_Equality_m1_640 ();
extern "C" void IntPtr_op_Inequality_m1_641 ();
extern "C" void IntPtr_op_Explicit_m1_642 ();
extern "C" void IntPtr_op_Explicit_m1_643 ();
extern "C" void IntPtr_op_Explicit_m1_644 ();
extern "C" void IntPtr_op_Explicit_m1_645 ();
extern "C" void IntPtr_op_Explicit_m1_646 ();
extern "C" void UIntPtr__ctor_m1_647 ();
extern "C" void UIntPtr__ctor_m1_648 ();
extern "C" void UIntPtr__ctor_m1_649 ();
extern "C" void UIntPtr__cctor_m1_650 ();
extern "C" void UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m1_651 ();
extern "C" void UIntPtr_Equals_m1_652 ();
extern "C" void UIntPtr_GetHashCode_m1_653 ();
extern "C" void UIntPtr_ToUInt32_m1_654 ();
extern "C" void UIntPtr_ToUInt64_m1_655 ();
extern "C" void UIntPtr_ToPointer_m1_656 ();
extern "C" void UIntPtr_ToString_m1_657 ();
extern "C" void UIntPtr_get_Size_m1_658 ();
extern "C" void UIntPtr_op_Equality_m1_659 ();
extern "C" void UIntPtr_op_Inequality_m1_660 ();
extern "C" void UIntPtr_op_Explicit_m1_661 ();
extern "C" void UIntPtr_op_Explicit_m1_662 ();
extern "C" void UIntPtr_op_Explicit_m1_663 ();
extern "C" void UIntPtr_op_Explicit_m1_664 ();
extern "C" void UIntPtr_op_Explicit_m1_665 ();
extern "C" void UIntPtr_op_Explicit_m1_666 ();
extern "C" void MulticastDelegate_GetObjectData_m1_667 ();
extern "C" void MulticastDelegate_Equals_m1_668 ();
extern "C" void MulticastDelegate_GetHashCode_m1_669 ();
extern "C" void MulticastDelegate_GetInvocationList_m1_670 ();
extern "C" void MulticastDelegate_CombineImpl_m1_671 ();
extern "C" void MulticastDelegate_BaseEquals_m1_672 ();
extern "C" void MulticastDelegate_KPM_m1_673 ();
extern "C" void MulticastDelegate_RemoveImpl_m1_674 ();
extern "C" void Delegate_get_Method_m1_675 ();
extern "C" void Delegate_get_Target_m1_676 ();
extern "C" void Delegate_CreateDelegate_internal_m1_677 ();
extern "C" void Delegate_SetMulticastInvoke_m1_678 ();
extern "C" void Delegate_arg_type_match_m1_679 ();
extern "C" void Delegate_return_type_match_m1_680 ();
extern "C" void Delegate_CreateDelegate_m1_681 ();
extern "C" void Delegate_CreateDelegate_m1_682 ();
extern "C" void Delegate_CreateDelegate_m1_683 ();
extern "C" void Delegate_CreateDelegate_m1_684 ();
extern "C" void Delegate_GetCandidateMethod_m1_685 ();
extern "C" void Delegate_CreateDelegate_m1_686 ();
extern "C" void Delegate_CreateDelegate_m1_687 ();
extern "C" void Delegate_CreateDelegate_m1_688 ();
extern "C" void Delegate_CreateDelegate_m1_689 ();
extern "C" void Delegate_Clone_m1_690 ();
extern "C" void Delegate_Equals_m1_691 ();
extern "C" void Delegate_GetHashCode_m1_692 ();
extern "C" void Delegate_GetObjectData_m1_693 ();
extern "C" void Delegate_GetInvocationList_m1_694 ();
extern "C" void Delegate_Combine_m1_695 ();
extern "C" void Delegate_Combine_m1_696 ();
extern "C" void Delegate_CombineImpl_m1_697 ();
extern "C" void Delegate_Remove_m1_698 ();
extern "C" void Delegate_RemoveImpl_m1_699 ();
extern "C" void Enum__ctor_m1_700 ();
extern "C" void Enum__cctor_m1_701 ();
extern "C" void Enum_System_IConvertible_ToBoolean_m1_702 ();
extern "C" void Enum_System_IConvertible_ToByte_m1_703 ();
extern "C" void Enum_System_IConvertible_ToChar_m1_704 ();
extern "C" void Enum_System_IConvertible_ToDateTime_m1_705 ();
extern "C" void Enum_System_IConvertible_ToDecimal_m1_706 ();
extern "C" void Enum_System_IConvertible_ToDouble_m1_707 ();
extern "C" void Enum_System_IConvertible_ToInt16_m1_708 ();
extern "C" void Enum_System_IConvertible_ToInt32_m1_709 ();
extern "C" void Enum_System_IConvertible_ToInt64_m1_710 ();
extern "C" void Enum_System_IConvertible_ToSByte_m1_711 ();
extern "C" void Enum_System_IConvertible_ToSingle_m1_712 ();
extern "C" void Enum_System_IConvertible_ToType_m1_713 ();
extern "C" void Enum_System_IConvertible_ToUInt16_m1_714 ();
extern "C" void Enum_System_IConvertible_ToUInt32_m1_715 ();
extern "C" void Enum_System_IConvertible_ToUInt64_m1_716 ();
extern "C" void Enum_GetTypeCode_m1_717 ();
extern "C" void Enum_get_value_m1_718 ();
extern "C" void Enum_get_Value_m1_719 ();
extern "C" void Enum_GetValues_m1_720 ();
extern "C" void Enum_FindPosition_m1_721 ();
extern "C" void Enum_GetName_m1_722 ();
extern "C" void Enum_IsDefined_m1_723 ();
extern "C" void Enum_get_underlying_type_m1_724 ();
extern "C" void Enum_GetUnderlyingType_m1_725 ();
extern "C" void Enum_Parse_m1_726 ();
extern "C" void Enum_FindName_m1_727 ();
extern "C" void Enum_GetValue_m1_728 ();
extern "C" void Enum_Parse_m1_729 ();
extern "C" void Enum_compare_value_to_m1_730 ();
extern "C" void Enum_CompareTo_m1_731 ();
extern "C" void Enum_ToString_m1_732 ();
extern "C" void Enum_ToString_m1_733 ();
extern "C" void Enum_ToString_m1_734 ();
extern "C" void Enum_ToString_m1_735 ();
extern "C" void Enum_ToObject_m1_736 ();
extern "C" void Enum_ToObject_m1_737 ();
extern "C" void Enum_ToObject_m1_738 ();
extern "C" void Enum_ToObject_m1_739 ();
extern "C" void Enum_ToObject_m1_740 ();
extern "C" void Enum_ToObject_m1_741 ();
extern "C" void Enum_ToObject_m1_742 ();
extern "C" void Enum_ToObject_m1_743 ();
extern "C" void Enum_ToObject_m1_744 ();
extern "C" void Enum_Equals_m1_745 ();
extern "C" void Enum_get_hashcode_m1_746 ();
extern "C" void Enum_GetHashCode_m1_747 ();
extern "C" void Enum_FormatSpecifier_X_m1_748 ();
extern "C" void Enum_FormatFlags_m1_749 ();
extern "C" void Enum_Format_m1_750 ();
extern "C" void SimpleEnumerator__ctor_m1_751 ();
extern "C" void SimpleEnumerator_get_Current_m1_752 ();
extern "C" void SimpleEnumerator_MoveNext_m1_753 ();
extern "C" void SimpleEnumerator_Reset_m1_754 ();
extern "C" void SimpleEnumerator_Clone_m1_755 ();
extern "C" void Swapper__ctor_m1_756 ();
extern "C" void Swapper_Invoke_m1_757 ();
extern "C" void Swapper_BeginInvoke_m1_758 ();
extern "C" void Swapper_EndInvoke_m1_759 ();
extern "C" void Array__ctor_m1_760 ();
extern "C" void Array_System_Collections_IList_get_Item_m1_761 ();
extern "C" void Array_System_Collections_IList_set_Item_m1_762 ();
extern "C" void Array_System_Collections_IList_Add_m1_763 ();
extern "C" void Array_System_Collections_IList_Clear_m1_764 ();
extern "C" void Array_System_Collections_IList_Contains_m1_765 ();
extern "C" void Array_System_Collections_IList_IndexOf_m1_766 ();
extern "C" void Array_System_Collections_IList_Insert_m1_767 ();
extern "C" void Array_System_Collections_IList_Remove_m1_768 ();
extern "C" void Array_System_Collections_IList_RemoveAt_m1_769 ();
extern "C" void Array_System_Collections_ICollection_get_Count_m1_770 ();
extern "C" void Array_InternalArray__ICollection_get_Count_m1_771 ();
extern "C" void Array_InternalArray__ICollection_get_IsReadOnly_m1_772 ();
extern "C" void Array_InternalArray__ICollection_Clear_m1_773 ();
extern "C" void Array_InternalArray__RemoveAt_m1_774 ();
extern "C" void Array_get_Length_m1_775 ();
extern "C" void Array_get_LongLength_m1_776 ();
extern "C" void Array_get_Rank_m1_777 ();
extern "C" void Array_GetRank_m1_778 ();
extern "C" void Array_GetLength_m1_779 ();
extern "C" void Array_GetLongLength_m1_780 ();
extern "C" void Array_GetLowerBound_m1_781 ();
extern "C" void Array_GetValue_m1_782 ();
extern "C" void Array_SetValue_m1_783 ();
extern "C" void Array_GetValueImpl_m1_784 ();
extern "C" void Array_SetValueImpl_m1_785 ();
extern "C" void Array_FastCopy_m1_786 ();
extern "C" void Array_CreateInstanceImpl_m1_787 ();
extern "C" void Array_get_IsSynchronized_m1_788 ();
extern "C" void Array_get_SyncRoot_m1_789 ();
extern "C" void Array_get_IsFixedSize_m1_790 ();
extern "C" void Array_get_IsReadOnly_m1_791 ();
extern "C" void Array_GetEnumerator_m1_792 ();
extern "C" void Array_GetUpperBound_m1_793 ();
extern "C" void Array_GetValue_m1_794 ();
extern "C" void Array_GetValue_m1_795 ();
extern "C" void Array_GetValue_m1_796 ();
extern "C" void Array_GetValue_m1_797 ();
extern "C" void Array_GetValue_m1_798 ();
extern "C" void Array_GetValue_m1_799 ();
extern "C" void Array_SetValue_m1_800 ();
extern "C" void Array_SetValue_m1_801 ();
extern "C" void Array_SetValue_m1_802 ();
extern "C" void Array_SetValue_m1_803 ();
extern "C" void Array_SetValue_m1_804 ();
extern "C" void Array_SetValue_m1_805 ();
extern "C" void Array_CreateInstance_m1_806 ();
extern "C" void Array_CreateInstance_m1_807 ();
extern "C" void Array_CreateInstance_m1_808 ();
extern "C" void Array_CreateInstance_m1_809 ();
extern "C" void Array_CreateInstance_m1_810 ();
extern "C" void Array_GetIntArray_m1_811 ();
extern "C" void Array_CreateInstance_m1_812 ();
extern "C" void Array_GetValue_m1_813 ();
extern "C" void Array_SetValue_m1_814 ();
extern "C" void Array_BinarySearch_m1_815 ();
extern "C" void Array_BinarySearch_m1_816 ();
extern "C" void Array_BinarySearch_m1_817 ();
extern "C" void Array_BinarySearch_m1_818 ();
extern "C" void Array_DoBinarySearch_m1_819 ();
extern "C" void Array_Clear_m1_820 ();
extern "C" void Array_ClearInternal_m1_821 ();
extern "C" void Array_Clone_m1_822 ();
extern "C" void Array_Copy_m1_823 ();
extern "C" void Array_Copy_m1_824 ();
extern "C" void Array_Copy_m1_825 ();
extern "C" void Array_Copy_m1_826 ();
extern "C" void Array_IndexOf_m1_827 ();
extern "C" void Array_IndexOf_m1_828 ();
extern "C" void Array_IndexOf_m1_829 ();
extern "C" void Array_Initialize_m1_830 ();
extern "C" void Array_LastIndexOf_m1_831 ();
extern "C" void Array_LastIndexOf_m1_832 ();
extern "C" void Array_LastIndexOf_m1_833 ();
extern "C" void Array_get_swapper_m1_834 ();
extern "C" void Array_Reverse_m1_835 ();
extern "C" void Array_Reverse_m1_836 ();
extern "C" void Array_Sort_m1_837 ();
extern "C" void Array_Sort_m1_838 ();
extern "C" void Array_Sort_m1_839 ();
extern "C" void Array_Sort_m1_840 ();
extern "C" void Array_Sort_m1_841 ();
extern "C" void Array_Sort_m1_842 ();
extern "C" void Array_Sort_m1_843 ();
extern "C" void Array_Sort_m1_844 ();
extern "C" void Array_int_swapper_m1_845 ();
extern "C" void Array_obj_swapper_m1_846 ();
extern "C" void Array_slow_swapper_m1_847 ();
extern "C" void Array_double_swapper_m1_848 ();
extern "C" void Array_new_gap_m1_849 ();
extern "C" void Array_combsort_m1_850 ();
extern "C" void Array_combsort_m1_851 ();
extern "C" void Array_combsort_m1_852 ();
extern "C" void Array_qsort_m1_853 ();
extern "C" void Array_swap_m1_854 ();
extern "C" void Array_compare_m1_855 ();
extern "C" void Array_CopyTo_m1_856 ();
extern "C" void Array_CopyTo_m1_857 ();
extern "C" void Array_ConstrainedCopy_m1_858 ();
extern "C" void Type__ctor_m1_859 ();
extern "C" void Type__cctor_m1_860 ();
extern "C" void Type_FilterName_impl_m1_861 ();
extern "C" void Type_FilterNameIgnoreCase_impl_m1_862 ();
extern "C" void Type_FilterAttribute_impl_m1_863 ();
extern "C" void Type_get_Attributes_m1_864 ();
extern "C" void Type_get_DeclaringType_m1_865 ();
extern "C" void Type_get_HasElementType_m1_866 ();
extern "C" void Type_get_IsAbstract_m1_867 ();
extern "C" void Type_get_IsArray_m1_868 ();
extern "C" void Type_get_IsByRef_m1_869 ();
extern "C" void Type_get_IsClass_m1_870 ();
extern "C" void Type_get_IsContextful_m1_871 ();
extern "C" void Type_get_IsEnum_m1_872 ();
extern "C" void Type_get_IsExplicitLayout_m1_873 ();
extern "C" void Type_get_IsInterface_m1_874 ();
extern "C" void Type_get_IsMarshalByRef_m1_875 ();
extern "C" void Type_get_IsPointer_m1_876 ();
extern "C" void Type_get_IsPrimitive_m1_877 ();
extern "C" void Type_get_IsSealed_m1_878 ();
extern "C" void Type_get_IsSerializable_m1_879 ();
extern "C" void Type_get_IsValueType_m1_880 ();
extern "C" void Type_get_MemberType_m1_881 ();
extern "C" void Type_get_ReflectedType_m1_882 ();
extern "C" void Type_get_TypeHandle_m1_883 ();
extern "C" void Type_Equals_m1_884 ();
extern "C" void Type_Equals_m1_885 ();
extern "C" void Type_EqualsInternal_m1_886 ();
extern "C" void Type_internal_from_handle_m1_887 ();
extern "C" void Type_internal_from_name_m1_888 ();
extern "C" void Type_GetType_m1_889 ();
extern "C" void Type_GetType_m1_890 ();
extern "C" void Type_GetTypeCodeInternal_m1_891 ();
extern "C" void Type_GetTypeCode_m1_892 ();
extern "C" void Type_GetTypeFromHandle_m1_893 ();
extern "C" void Type_GetTypeHandle_m1_894 ();
extern "C" void Type_type_is_subtype_of_m1_895 ();
extern "C" void Type_type_is_assignable_from_m1_896 ();
extern "C" void Type_IsSubclassOf_m1_897 ();
extern "C" void Type_IsAssignableFrom_m1_898 ();
extern "C" void Type_IsInstanceOfType_m1_899 ();
extern "C" void Type_GetHashCode_m1_900 ();
extern "C" void Type_GetMethod_m1_901 ();
extern "C" void Type_GetMethod_m1_902 ();
extern "C" void Type_GetMethod_m1_903 ();
extern "C" void Type_GetMethod_m1_904 ();
extern "C" void Type_GetProperty_m1_905 ();
extern "C" void Type_GetProperty_m1_906 ();
extern "C" void Type_GetProperty_m1_907 ();
extern "C" void Type_GetProperty_m1_908 ();
extern "C" void Type_IsArrayImpl_m1_909 ();
extern "C" void Type_IsValueTypeImpl_m1_910 ();
extern "C" void Type_IsContextfulImpl_m1_911 ();
extern "C" void Type_IsMarshalByRefImpl_m1_912 ();
extern "C" void Type_GetConstructor_m1_913 ();
extern "C" void Type_GetConstructor_m1_914 ();
extern "C" void Type_GetConstructor_m1_915 ();
extern "C" void Type_ToString_m1_916 ();
extern "C" void Type_get_IsSystemType_m1_917 ();
extern "C" void Type_GetGenericArguments_m1_918 ();
extern "C" void Type_get_ContainsGenericParameters_m1_919 ();
extern "C" void Type_get_IsGenericTypeDefinition_m1_920 ();
extern "C" void Type_GetGenericTypeDefinition_impl_m1_921 ();
extern "C" void Type_GetGenericTypeDefinition_m1_922 ();
extern "C" void Type_get_IsGenericType_m1_923 ();
extern "C" void Type_MakeGenericType_m1_924 ();
extern "C" void Type_MakeGenericType_m1_925 ();
extern "C" void Type_get_IsGenericParameter_m1_926 ();
extern "C" void Type_get_IsNested_m1_927 ();
extern "C" void Type_GetPseudoCustomAttributes_m1_928 ();
extern "C" void MemberInfo__ctor_m1_929 ();
extern "C" void MemberInfo_get_Module_m1_930 ();
extern "C" void Exception__ctor_m1_931 ();
extern "C" void Exception__ctor_m1_932 ();
extern "C" void Exception__ctor_m1_933 ();
extern "C" void Exception__ctor_m1_934 ();
extern "C" void Exception_get_InnerException_m1_935 ();
extern "C" void Exception_set_HResult_m1_936 ();
extern "C" void Exception_get_ClassName_m1_937 ();
extern "C" void Exception_get_Message_m1_938 ();
extern "C" void Exception_get_Source_m1_939 ();
extern "C" void Exception_get_StackTrace_m1_940 ();
extern "C" void Exception_GetObjectData_m1_941 ();
extern "C" void Exception_ToString_m1_942 ();
extern "C" void Exception_GetFullNameForStackTrace_m1_943 ();
extern "C" void Exception_GetType_m1_944 ();
extern "C" void RuntimeFieldHandle__ctor_m1_945 ();
extern "C" void RuntimeFieldHandle_get_Value_m1_946 ();
extern "C" void RuntimeFieldHandle_GetObjectData_m1_947 ();
extern "C" void RuntimeFieldHandle_Equals_m1_948 ();
extern "C" void RuntimeFieldHandle_GetHashCode_m1_949 ();
extern "C" void RuntimeTypeHandle__ctor_m1_950 ();
extern "C" void RuntimeTypeHandle_get_Value_m1_951 ();
extern "C" void RuntimeTypeHandle_GetObjectData_m1_952 ();
extern "C" void RuntimeTypeHandle_Equals_m1_953 ();
extern "C" void RuntimeTypeHandle_GetHashCode_m1_954 ();
extern "C" void ParamArrayAttribute__ctor_m1_955 ();
extern "C" void OutAttribute__ctor_m1_956 ();
extern "C" void ObsoleteAttribute__ctor_m1_957 ();
extern "C" void ObsoleteAttribute__ctor_m1_958 ();
extern "C" void ObsoleteAttribute__ctor_m1_959 ();
extern "C" void DllImportAttribute__ctor_m1_960 ();
extern "C" void DllImportAttribute_get_Value_m1_961 ();
extern "C" void MarshalAsAttribute__ctor_m1_962 ();
extern "C" void InAttribute__ctor_m1_963 ();
extern "C" void ConditionalAttribute__ctor_m1_964 ();
extern "C" void GuidAttribute__ctor_m1_965 ();
extern "C" void ComImportAttribute__ctor_m1_966 ();
extern "C" void OptionalAttribute__ctor_m1_967 ();
extern "C" void CompilerGeneratedAttribute__ctor_m1_968 ();
extern "C" void InternalsVisibleToAttribute__ctor_m1_969 ();
extern "C" void RuntimeCompatibilityAttribute__ctor_m1_970 ();
extern "C" void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1_971 ();
extern "C" void DebuggerHiddenAttribute__ctor_m1_972 ();
extern "C" void DefaultMemberAttribute__ctor_m1_973 ();
extern "C" void DefaultMemberAttribute_get_MemberName_m1_974 ();
extern "C" void DecimalConstantAttribute__ctor_m1_975 ();
extern "C" void FieldOffsetAttribute__ctor_m1_976 ();
extern "C" void AsyncCallback__ctor_m1_977 ();
extern "C" void AsyncCallback_Invoke_m1_978 ();
extern "C" void AsyncCallback_BeginInvoke_m1_979 ();
extern "C" void AsyncCallback_EndInvoke_m1_980 ();
extern "C" void TypedReference_Equals_m1_981 ();
extern "C" void TypedReference_GetHashCode_m1_982 ();
extern "C" void ArgIterator_Equals_m1_983 ();
extern "C" void ArgIterator_GetHashCode_m1_984 ();
extern "C" void MarshalByRefObject__ctor_m1_985 ();
extern "C" void MarshalByRefObject_get_ObjectIdentity_m1_986 ();
extern "C" void RuntimeHelpers_InitializeArray_m1_987 ();
extern "C" void RuntimeHelpers_InitializeArray_m1_988 ();
extern "C" void RuntimeHelpers_get_OffsetToStringData_m1_989 ();
extern "C" void Locale_GetText_m1_990 ();
extern "C" void Locale_GetText_m1_991 ();
extern "C" void MonoTODOAttribute__ctor_m1_992 ();
extern "C" void MonoTODOAttribute__ctor_m1_993 ();
extern "C" void MonoDocumentationNoteAttribute__ctor_m1_994 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid__ctor_m1_995 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m1_996 ();
extern "C" void SafeWaitHandle__ctor_m1_997 ();
extern "C" void SafeWaitHandle_ReleaseHandle_m1_998 ();
extern "C" void TableRange__ctor_m1_999 ();
extern "C" void CodePointIndexer__ctor_m1_1000 ();
extern "C" void CodePointIndexer_ToIndex_m1_1001 ();
extern "C" void TailoringInfo__ctor_m1_1002 ();
extern "C" void Contraction__ctor_m1_1003 ();
extern "C" void ContractionComparer__ctor_m1_1004 ();
extern "C" void ContractionComparer__cctor_m1_1005 ();
extern "C" void ContractionComparer_Compare_m1_1006 ();
extern "C" void Level2Map__ctor_m1_1007 ();
extern "C" void Level2MapComparer__ctor_m1_1008 ();
extern "C" void Level2MapComparer__cctor_m1_1009 ();
extern "C" void Level2MapComparer_Compare_m1_1010 ();
extern "C" void MSCompatUnicodeTable__cctor_m1_1011 ();
extern "C" void MSCompatUnicodeTable_GetTailoringInfo_m1_1012 ();
extern "C" void MSCompatUnicodeTable_BuildTailoringTables_m1_1013 ();
extern "C" void MSCompatUnicodeTable_SetCJKReferences_m1_1014 ();
extern "C" void MSCompatUnicodeTable_Category_m1_1015 ();
extern "C" void MSCompatUnicodeTable_Level1_m1_1016 ();
extern "C" void MSCompatUnicodeTable_Level2_m1_1017 ();
extern "C" void MSCompatUnicodeTable_Level3_m1_1018 ();
extern "C" void MSCompatUnicodeTable_IsIgnorable_m1_1019 ();
extern "C" void MSCompatUnicodeTable_IsIgnorableNonSpacing_m1_1020 ();
extern "C" void MSCompatUnicodeTable_ToKanaTypeInsensitive_m1_1021 ();
extern "C" void MSCompatUnicodeTable_ToWidthCompat_m1_1022 ();
extern "C" void MSCompatUnicodeTable_HasSpecialWeight_m1_1023 ();
extern "C" void MSCompatUnicodeTable_IsHalfWidthKana_m1_1024 ();
extern "C" void MSCompatUnicodeTable_IsHiragana_m1_1025 ();
extern "C" void MSCompatUnicodeTable_IsJapaneseSmallLetter_m1_1026 ();
extern "C" void MSCompatUnicodeTable_get_IsReady_m1_1027 ();
extern "C" void MSCompatUnicodeTable_GetResource_m1_1028 ();
extern "C" void MSCompatUnicodeTable_UInt32FromBytePtr_m1_1029 ();
extern "C" void MSCompatUnicodeTable_FillCJK_m1_1030 ();
extern "C" void MSCompatUnicodeTable_FillCJKCore_m1_1031 ();
extern "C" void MSCompatUnicodeTableUtil__cctor_m1_1032 ();
extern "C" void Context__ctor_m1_1033 ();
extern "C" void PreviousInfo__ctor_m1_1034 ();
extern "C" void SimpleCollator__ctor_m1_1035 ();
extern "C" void SimpleCollator__cctor_m1_1036 ();
extern "C" void SimpleCollator_SetCJKTable_m1_1037 ();
extern "C" void SimpleCollator_GetNeutralCulture_m1_1038 ();
extern "C" void SimpleCollator_Category_m1_1039 ();
extern "C" void SimpleCollator_Level1_m1_1040 ();
extern "C" void SimpleCollator_Level2_m1_1041 ();
extern "C" void SimpleCollator_IsHalfKana_m1_1042 ();
extern "C" void SimpleCollator_GetContraction_m1_1043 ();
extern "C" void SimpleCollator_GetContraction_m1_1044 ();
extern "C" void SimpleCollator_GetTailContraction_m1_1045 ();
extern "C" void SimpleCollator_GetTailContraction_m1_1046 ();
extern "C" void SimpleCollator_FilterOptions_m1_1047 ();
extern "C" void SimpleCollator_GetExtenderType_m1_1048 ();
extern "C" void SimpleCollator_ToDashTypeValue_m1_1049 ();
extern "C" void SimpleCollator_FilterExtender_m1_1050 ();
extern "C" void SimpleCollator_IsIgnorable_m1_1051 ();
extern "C" void SimpleCollator_IsSafe_m1_1052 ();
extern "C" void SimpleCollator_GetSortKey_m1_1053 ();
extern "C" void SimpleCollator_GetSortKey_m1_1054 ();
extern "C" void SimpleCollator_GetSortKey_m1_1055 ();
extern "C" void SimpleCollator_FillSortKeyRaw_m1_1056 ();
extern "C" void SimpleCollator_FillSurrogateSortKeyRaw_m1_1057 ();
extern "C" void SimpleCollator_CompareOrdinal_m1_1058 ();
extern "C" void SimpleCollator_CompareQuick_m1_1059 ();
extern "C" void SimpleCollator_CompareOrdinalIgnoreCase_m1_1060 ();
extern "C" void SimpleCollator_Compare_m1_1061 ();
extern "C" void SimpleCollator_ClearBuffer_m1_1062 ();
extern "C" void SimpleCollator_QuickCheckPossible_m1_1063 ();
extern "C" void SimpleCollator_CompareInternal_m1_1064 ();
extern "C" void SimpleCollator_CompareFlagPair_m1_1065 ();
extern "C" void SimpleCollator_IsPrefix_m1_1066 ();
extern "C" void SimpleCollator_IsPrefix_m1_1067 ();
extern "C" void SimpleCollator_IsPrefix_m1_1068 ();
extern "C" void SimpleCollator_IsSuffix_m1_1069 ();
extern "C" void SimpleCollator_IsSuffix_m1_1070 ();
extern "C" void SimpleCollator_QuickIndexOf_m1_1071 ();
extern "C" void SimpleCollator_IndexOf_m1_1072 ();
extern "C" void SimpleCollator_IndexOfOrdinal_m1_1073 ();
extern "C" void SimpleCollator_IndexOfOrdinalIgnoreCase_m1_1074 ();
extern "C" void SimpleCollator_IndexOfSortKey_m1_1075 ();
extern "C" void SimpleCollator_IndexOf_m1_1076 ();
extern "C" void SimpleCollator_LastIndexOf_m1_1077 ();
extern "C" void SimpleCollator_LastIndexOfOrdinal_m1_1078 ();
extern "C" void SimpleCollator_LastIndexOfOrdinalIgnoreCase_m1_1079 ();
extern "C" void SimpleCollator_LastIndexOfSortKey_m1_1080 ();
extern "C" void SimpleCollator_LastIndexOf_m1_1081 ();
extern "C" void SimpleCollator_MatchesForward_m1_1082 ();
extern "C" void SimpleCollator_MatchesForwardCore_m1_1083 ();
extern "C" void SimpleCollator_MatchesPrimitive_m1_1084 ();
extern "C" void SimpleCollator_MatchesBackward_m1_1085 ();
extern "C" void SimpleCollator_MatchesBackwardCore_m1_1086 ();
extern "C" void SortKey__ctor_m1_1087 ();
extern "C" void SortKey__ctor_m1_1088 ();
extern "C" void SortKey_Compare_m1_1089 ();
extern "C" void SortKey_get_OriginalString_m1_1090 ();
extern "C" void SortKey_get_KeyData_m1_1091 ();
extern "C" void SortKey_Equals_m1_1092 ();
extern "C" void SortKey_GetHashCode_m1_1093 ();
extern "C" void SortKey_ToString_m1_1094 ();
extern "C" void SortKeyBuffer__ctor_m1_1095 ();
extern "C" void SortKeyBuffer_Reset_m1_1096 ();
extern "C" void SortKeyBuffer_Initialize_m1_1097 ();
extern "C" void SortKeyBuffer_AppendCJKExtension_m1_1098 ();
extern "C" void SortKeyBuffer_AppendKana_m1_1099 ();
extern "C" void SortKeyBuffer_AppendNormal_m1_1100 ();
extern "C" void SortKeyBuffer_AppendLevel5_m1_1101 ();
extern "C" void SortKeyBuffer_AppendBufferPrimitive_m1_1102 ();
extern "C" void SortKeyBuffer_GetResultAndReset_m1_1103 ();
extern "C" void SortKeyBuffer_GetOptimizedLength_m1_1104 ();
extern "C" void SortKeyBuffer_GetResult_m1_1105 ();
extern "C" void PrimeGeneratorBase__ctor_m1_1106 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m1_1107 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m1_1108 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m1_1109 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m1_1110 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m1_1111 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m1_1112 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m1_1113 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m1_1114 ();
extern "C" void PrimalityTests_GetSPPRounds_m1_1115 ();
extern "C" void PrimalityTests_Test_m1_1116 ();
extern "C" void PrimalityTests_RabinMillerTest_m1_1117 ();
extern "C" void PrimalityTests_SmallPrimeSppTest_m1_1118 ();
extern "C" void ModulusRing__ctor_m1_1119 ();
extern "C" void ModulusRing_BarrettReduction_m1_1120 ();
extern "C" void ModulusRing_Multiply_m1_1121 ();
extern "C" void ModulusRing_Difference_m1_1122 ();
extern "C" void ModulusRing_Pow_m1_1123 ();
extern "C" void ModulusRing_Pow_m1_1124 ();
extern "C" void Kernel_AddSameSign_m1_1125 ();
extern "C" void Kernel_Subtract_m1_1126 ();
extern "C" void Kernel_MinusEq_m1_1127 ();
extern "C" void Kernel_PlusEq_m1_1128 ();
extern "C" void Kernel_Compare_m1_1129 ();
extern "C" void Kernel_SingleByteDivideInPlace_m1_1130 ();
extern "C" void Kernel_DwordMod_m1_1131 ();
extern "C" void Kernel_DwordDivMod_m1_1132 ();
extern "C" void Kernel_multiByteDivide_m1_1133 ();
extern "C" void Kernel_LeftShift_m1_1134 ();
extern "C" void Kernel_RightShift_m1_1135 ();
extern "C" void Kernel_MultiplyByDword_m1_1136 ();
extern "C" void Kernel_Multiply_m1_1137 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m1_1138 ();
extern "C" void Kernel_modInverse_m1_1139 ();
extern "C" void Kernel_modInverse_m1_1140 ();
extern "C" void BigInteger__ctor_m1_1141 ();
extern "C" void BigInteger__ctor_m1_1142 ();
extern "C" void BigInteger__ctor_m1_1143 ();
extern "C" void BigInteger__ctor_m1_1144 ();
extern "C" void BigInteger__ctor_m1_1145 ();
extern "C" void BigInteger__cctor_m1_1146 ();
extern "C" void BigInteger_get_Rng_m1_1147 ();
extern "C" void BigInteger_GenerateRandom_m1_1148 ();
extern "C" void BigInteger_GenerateRandom_m1_1149 ();
extern "C" void BigInteger_Randomize_m1_1150 ();
extern "C" void BigInteger_Randomize_m1_1151 ();
extern "C" void BigInteger_BitCount_m1_1152 ();
extern "C" void BigInteger_TestBit_m1_1153 ();
extern "C" void BigInteger_TestBit_m1_1154 ();
extern "C" void BigInteger_SetBit_m1_1155 ();
extern "C" void BigInteger_SetBit_m1_1156 ();
extern "C" void BigInteger_LowestSetBit_m1_1157 ();
extern "C" void BigInteger_GetBytes_m1_1158 ();
extern "C" void BigInteger_ToString_m1_1159 ();
extern "C" void BigInteger_ToString_m1_1160 ();
extern "C" void BigInteger_Normalize_m1_1161 ();
extern "C" void BigInteger_Clear_m1_1162 ();
extern "C" void BigInteger_GetHashCode_m1_1163 ();
extern "C" void BigInteger_ToString_m1_1164 ();
extern "C" void BigInteger_Equals_m1_1165 ();
extern "C" void BigInteger_ModInverse_m1_1166 ();
extern "C" void BigInteger_ModPow_m1_1167 ();
extern "C" void BigInteger_IsProbablePrime_m1_1168 ();
extern "C" void BigInteger_GeneratePseudoPrime_m1_1169 ();
extern "C" void BigInteger_Incr2_m1_1170 ();
extern "C" void BigInteger_op_Implicit_m1_1171 ();
extern "C" void BigInteger_op_Implicit_m1_1172 ();
extern "C" void BigInteger_op_Addition_m1_1173 ();
extern "C" void BigInteger_op_Subtraction_m1_1174 ();
extern "C" void BigInteger_op_Modulus_m1_1175 ();
extern "C" void BigInteger_op_Modulus_m1_1176 ();
extern "C" void BigInteger_op_Division_m1_1177 ();
extern "C" void BigInteger_op_Multiply_m1_1178 ();
extern "C" void BigInteger_op_Multiply_m1_1179 ();
extern "C" void BigInteger_op_LeftShift_m1_1180 ();
extern "C" void BigInteger_op_RightShift_m1_1181 ();
extern "C" void BigInteger_op_Equality_m1_1182 ();
extern "C" void BigInteger_op_Inequality_m1_1183 ();
extern "C" void BigInteger_op_Equality_m1_1184 ();
extern "C" void BigInteger_op_Inequality_m1_1185 ();
extern "C" void BigInteger_op_GreaterThan_m1_1186 ();
extern "C" void BigInteger_op_LessThan_m1_1187 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m1_1188 ();
extern "C" void BigInteger_op_LessThanOrEqual_m1_1189 ();
extern "C" void CryptoConvert_ToInt32LE_m1_1190 ();
extern "C" void CryptoConvert_ToUInt32LE_m1_1191 ();
extern "C" void CryptoConvert_GetBytesLE_m1_1192 ();
extern "C" void CryptoConvert_ToCapiPrivateKeyBlob_m1_1193 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m1_1194 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m1_1195 ();
extern "C" void CryptoConvert_ToCapiPublicKeyBlob_m1_1196 ();
extern "C" void CryptoConvert_ToCapiKeyBlob_m1_1197 ();
extern "C" void KeyBuilder_get_Rng_m1_1198 ();
extern "C" void KeyBuilder_Key_m1_1199 ();
extern "C" void KeyBuilder_IV_m1_1200 ();
extern "C" void BlockProcessor__ctor_m1_1201 ();
extern "C" void BlockProcessor_Finalize_m1_1202 ();
extern "C" void BlockProcessor_Initialize_m1_1203 ();
extern "C" void BlockProcessor_Core_m1_1204 ();
extern "C" void BlockProcessor_Core_m1_1205 ();
extern "C" void BlockProcessor_Final_m1_1206 ();
extern "C" void KeyGeneratedEventHandler__ctor_m1_1207 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m1_1208 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m1_1209 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m1_1210 ();
extern "C" void DSAManaged__ctor_m1_1211 ();
extern "C" void DSAManaged_add_KeyGenerated_m1_1212 ();
extern "C" void DSAManaged_remove_KeyGenerated_m1_1213 ();
extern "C" void DSAManaged_Finalize_m1_1214 ();
extern "C" void DSAManaged_Generate_m1_1215 ();
extern "C" void DSAManaged_GenerateKeyPair_m1_1216 ();
extern "C" void DSAManaged_add_m1_1217 ();
extern "C" void DSAManaged_GenerateParams_m1_1218 ();
extern "C" void DSAManaged_get_Random_m1_1219 ();
extern "C" void DSAManaged_get_KeySize_m1_1220 ();
extern "C" void DSAManaged_get_PublicOnly_m1_1221 ();
extern "C" void DSAManaged_NormalizeArray_m1_1222 ();
extern "C" void DSAManaged_ExportParameters_m1_1223 ();
extern "C" void DSAManaged_ImportParameters_m1_1224 ();
extern "C" void DSAManaged_CreateSignature_m1_1225 ();
extern "C" void DSAManaged_VerifySignature_m1_1226 ();
extern "C" void DSAManaged_Dispose_m1_1227 ();
extern "C" void KeyPairPersistence__ctor_m1_1228 ();
extern "C" void KeyPairPersistence__ctor_m1_1229 ();
extern "C" void KeyPairPersistence__cctor_m1_1230 ();
extern "C" void KeyPairPersistence_get_Filename_m1_1231 ();
extern "C" void KeyPairPersistence_get_KeyValue_m1_1232 ();
extern "C" void KeyPairPersistence_set_KeyValue_m1_1233 ();
extern "C" void KeyPairPersistence_Load_m1_1234 ();
extern "C" void KeyPairPersistence_Save_m1_1235 ();
extern "C" void KeyPairPersistence_Remove_m1_1236 ();
extern "C" void KeyPairPersistence_get_UserPath_m1_1237 ();
extern "C" void KeyPairPersistence_get_MachinePath_m1_1238 ();
extern "C" void KeyPairPersistence__CanSecure_m1_1239 ();
extern "C" void KeyPairPersistence__ProtectUser_m1_1240 ();
extern "C" void KeyPairPersistence__ProtectMachine_m1_1241 ();
extern "C" void KeyPairPersistence__IsUserProtected_m1_1242 ();
extern "C" void KeyPairPersistence__IsMachineProtected_m1_1243 ();
extern "C" void KeyPairPersistence_CanSecure_m1_1244 ();
extern "C" void KeyPairPersistence_ProtectUser_m1_1245 ();
extern "C" void KeyPairPersistence_ProtectMachine_m1_1246 ();
extern "C" void KeyPairPersistence_IsUserProtected_m1_1247 ();
extern "C" void KeyPairPersistence_IsMachineProtected_m1_1248 ();
extern "C" void KeyPairPersistence_get_CanChange_m1_1249 ();
extern "C" void KeyPairPersistence_get_UseDefaultKeyContainer_m1_1250 ();
extern "C" void KeyPairPersistence_get_UseMachineKeyStore_m1_1251 ();
extern "C" void KeyPairPersistence_get_ContainerName_m1_1252 ();
extern "C" void KeyPairPersistence_Copy_m1_1253 ();
extern "C" void KeyPairPersistence_FromXml_m1_1254 ();
extern "C" void KeyPairPersistence_ToXml_m1_1255 ();
extern "C" void MACAlgorithm__ctor_m1_1256 ();
extern "C" void MACAlgorithm_Initialize_m1_1257 ();
extern "C" void MACAlgorithm_Core_m1_1258 ();
extern "C" void MACAlgorithm_Final_m1_1259 ();
extern "C" void PKCS1__cctor_m1_1260 ();
extern "C" void PKCS1_Compare_m1_1261 ();
extern "C" void PKCS1_I2OSP_m1_1262 ();
extern "C" void PKCS1_OS2IP_m1_1263 ();
extern "C" void PKCS1_RSAEP_m1_1264 ();
extern "C" void PKCS1_RSASP1_m1_1265 ();
extern "C" void PKCS1_RSAVP1_m1_1266 ();
extern "C" void PKCS1_Encrypt_v15_m1_1267 ();
extern "C" void PKCS1_Sign_v15_m1_1268 ();
extern "C" void PKCS1_Verify_v15_m1_1269 ();
extern "C" void PKCS1_Verify_v15_m1_1270 ();
extern "C" void PKCS1_Encode_v15_m1_1271 ();
extern "C" void PrivateKeyInfo__ctor_m1_1272 ();
extern "C" void PrivateKeyInfo__ctor_m1_1273 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m1_1274 ();
extern "C" void PrivateKeyInfo_Decode_m1_1275 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m1_1276 ();
extern "C" void PrivateKeyInfo_Normalize_m1_1277 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m1_1278 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m1_1279 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m1_1280 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m1_1281 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m1_1282 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m1_1283 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m1_1284 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m1_1285 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m1_1286 ();
extern "C" void KeyGeneratedEventHandler__ctor_m1_1287 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m1_1288 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m1_1289 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m1_1290 ();
extern "C" void RSAManaged__ctor_m1_1291 ();
extern "C" void RSAManaged_add_KeyGenerated_m1_1292 ();
extern "C" void RSAManaged_remove_KeyGenerated_m1_1293 ();
extern "C" void RSAManaged_Finalize_m1_1294 ();
extern "C" void RSAManaged_GenerateKeyPair_m1_1295 ();
extern "C" void RSAManaged_get_KeySize_m1_1296 ();
extern "C" void RSAManaged_get_PublicOnly_m1_1297 ();
extern "C" void RSAManaged_DecryptValue_m1_1298 ();
extern "C" void RSAManaged_EncryptValue_m1_1299 ();
extern "C" void RSAManaged_ExportParameters_m1_1300 ();
extern "C" void RSAManaged_ImportParameters_m1_1301 ();
extern "C" void RSAManaged_Dispose_m1_1302 ();
extern "C" void RSAManaged_ToXmlString_m1_1303 ();
extern "C" void RSAManaged_get_IsCrtPossible_m1_1304 ();
extern "C" void RSAManaged_GetPaddedValue_m1_1305 ();
extern "C" void SymmetricTransform__ctor_m1_1306 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m1_1307 ();
extern "C" void SymmetricTransform_Finalize_m1_1308 ();
extern "C" void SymmetricTransform_Dispose_m1_1309 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m1_1310 ();
extern "C" void SymmetricTransform_Transform_m1_1311 ();
extern "C" void SymmetricTransform_CBC_m1_1312 ();
extern "C" void SymmetricTransform_CFB_m1_1313 ();
extern "C" void SymmetricTransform_OFB_m1_1314 ();
extern "C" void SymmetricTransform_CTS_m1_1315 ();
extern "C" void SymmetricTransform_CheckInput_m1_1316 ();
extern "C" void SymmetricTransform_TransformBlock_m1_1317 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m1_1318 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m1_1319 ();
extern "C" void SymmetricTransform_Random_m1_1320 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m1_1321 ();
extern "C" void SymmetricTransform_FinalEncrypt_m1_1322 ();
extern "C" void SymmetricTransform_FinalDecrypt_m1_1323 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m1_1324 ();
extern "C" void SafeBag__ctor_m1_1325 ();
extern "C" void SafeBag_get_BagOID_m1_1326 ();
extern "C" void SafeBag_get_ASN1_m1_1327 ();
extern "C" void DeriveBytes__ctor_m1_1328 ();
extern "C" void DeriveBytes__cctor_m1_1329 ();
extern "C" void DeriveBytes_set_HashName_m1_1330 ();
extern "C" void DeriveBytes_set_IterationCount_m1_1331 ();
extern "C" void DeriveBytes_set_Password_m1_1332 ();
extern "C" void DeriveBytes_set_Salt_m1_1333 ();
extern "C" void DeriveBytes_Adjust_m1_1334 ();
extern "C" void DeriveBytes_Derive_m1_1335 ();
extern "C" void DeriveBytes_DeriveKey_m1_1336 ();
extern "C" void DeriveBytes_DeriveIV_m1_1337 ();
extern "C" void DeriveBytes_DeriveMAC_m1_1338 ();
extern "C" void PKCS12__ctor_m1_1339 ();
extern "C" void PKCS12__ctor_m1_1340 ();
extern "C" void PKCS12__ctor_m1_1341 ();
extern "C" void PKCS12__cctor_m1_1342 ();
extern "C" void PKCS12_Decode_m1_1343 ();
extern "C" void PKCS12_Finalize_m1_1344 ();
extern "C" void PKCS12_set_Password_m1_1345 ();
extern "C" void PKCS12_get_IterationCount_m1_1346 ();
extern "C" void PKCS12_set_IterationCount_m1_1347 ();
extern "C" void PKCS12_get_Certificates_m1_1348 ();
extern "C" void PKCS12_get_RNG_m1_1349 ();
extern "C" void PKCS12_Compare_m1_1350 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m1_1351 ();
extern "C" void PKCS12_Decrypt_m1_1352 ();
extern "C" void PKCS12_Decrypt_m1_1353 ();
extern "C" void PKCS12_Encrypt_m1_1354 ();
extern "C" void PKCS12_GetExistingParameters_m1_1355 ();
extern "C" void PKCS12_AddPrivateKey_m1_1356 ();
extern "C" void PKCS12_ReadSafeBag_m1_1357 ();
extern "C" void PKCS12_CertificateSafeBag_m1_1358 ();
extern "C" void PKCS12_MAC_m1_1359 ();
extern "C" void PKCS12_GetBytes_m1_1360 ();
extern "C" void PKCS12_EncryptedContentInfo_m1_1361 ();
extern "C" void PKCS12_AddCertificate_m1_1362 ();
extern "C" void PKCS12_AddCertificate_m1_1363 ();
extern "C" void PKCS12_RemoveCertificate_m1_1364 ();
extern "C" void PKCS12_RemoveCertificate_m1_1365 ();
extern "C" void PKCS12_Clone_m1_1366 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m1_1367 ();
extern "C" void X501__cctor_m1_1368 ();
extern "C" void X501_ToString_m1_1369 ();
extern "C" void X501_ToString_m1_1370 ();
extern "C" void X501_AppendEntry_m1_1371 ();
extern "C" void X509Certificate__ctor_m1_1372 ();
extern "C" void X509Certificate__cctor_m1_1373 ();
extern "C" void X509Certificate_Parse_m1_1374 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m1_1375 ();
extern "C" void X509Certificate_get_DSA_m1_1376 ();
extern "C" void X509Certificate_get_IssuerName_m1_1377 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m1_1378 ();
extern "C" void X509Certificate_get_PublicKey_m1_1379 ();
extern "C" void X509Certificate_get_RawData_m1_1380 ();
extern "C" void X509Certificate_get_SubjectName_m1_1381 ();
extern "C" void X509Certificate_get_ValidFrom_m1_1382 ();
extern "C" void X509Certificate_get_ValidUntil_m1_1383 ();
extern "C" void X509Certificate_GetIssuerName_m1_1384 ();
extern "C" void X509Certificate_GetSubjectName_m1_1385 ();
extern "C" void X509Certificate_GetObjectData_m1_1386 ();
extern "C" void X509Certificate_PEM_m1_1387 ();
extern "C" void X509CertificateEnumerator__ctor_m1_1388 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m1_1389 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m1_1390 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m1_1391 ();
extern "C" void X509CertificateEnumerator_get_Current_m1_1392 ();
extern "C" void X509CertificateEnumerator_MoveNext_m1_1393 ();
extern "C" void X509CertificateEnumerator_Reset_m1_1394 ();
extern "C" void X509CertificateCollection__ctor_m1_1395 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m1_1396 ();
extern "C" void X509CertificateCollection_get_Item_m1_1397 ();
extern "C" void X509CertificateCollection_Add_m1_1398 ();
extern "C" void X509CertificateCollection_GetEnumerator_m1_1399 ();
extern "C" void X509CertificateCollection_GetHashCode_m1_1400 ();
extern "C" void X509Extension__ctor_m1_1401 ();
extern "C" void X509Extension_Decode_m1_1402 ();
extern "C" void X509Extension_Equals_m1_1403 ();
extern "C" void X509Extension_GetHashCode_m1_1404 ();
extern "C" void X509Extension_WriteLine_m1_1405 ();
extern "C" void X509Extension_ToString_m1_1406 ();
extern "C" void X509ExtensionCollection__ctor_m1_1407 ();
extern "C" void X509ExtensionCollection__ctor_m1_1408 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m1_1409 ();
extern "C" void ASN1__ctor_m1_1410 ();
extern "C" void ASN1__ctor_m1_1411 ();
extern "C" void ASN1__ctor_m1_1412 ();
extern "C" void ASN1_get_Count_m1_1413 ();
extern "C" void ASN1_get_Tag_m1_1414 ();
extern "C" void ASN1_get_Length_m1_1415 ();
extern "C" void ASN1_get_Value_m1_1416 ();
extern "C" void ASN1_set_Value_m1_1417 ();
extern "C" void ASN1_CompareArray_m1_1418 ();
extern "C" void ASN1_CompareValue_m1_1419 ();
extern "C" void ASN1_Add_m1_1420 ();
extern "C" void ASN1_GetBytes_m1_1421 ();
extern "C" void ASN1_Decode_m1_1422 ();
extern "C" void ASN1_DecodeTLV_m1_1423 ();
extern "C" void ASN1_get_Item_m1_1424 ();
extern "C" void ASN1_Element_m1_1425 ();
extern "C" void ASN1_ToString_m1_1426 ();
extern "C" void ASN1Convert_FromInt32_m1_1427 ();
extern "C" void ASN1Convert_FromOid_m1_1428 ();
extern "C" void ASN1Convert_ToInt32_m1_1429 ();
extern "C" void ASN1Convert_ToOid_m1_1430 ();
extern "C" void ASN1Convert_ToDateTime_m1_1431 ();
extern "C" void BitConverterLE_GetUIntBytes_m1_1432 ();
extern "C" void BitConverterLE_GetBytes_m1_1433 ();
extern "C" void BitConverterLE_UShortFromBytes_m1_1434 ();
extern "C" void BitConverterLE_UIntFromBytes_m1_1435 ();
extern "C" void BitConverterLE_ULongFromBytes_m1_1436 ();
extern "C" void BitConverterLE_ToInt16_m1_1437 ();
extern "C" void BitConverterLE_ToInt32_m1_1438 ();
extern "C" void BitConverterLE_ToSingle_m1_1439 ();
extern "C" void BitConverterLE_ToDouble_m1_1440 ();
extern "C" void ContentInfo__ctor_m1_1441 ();
extern "C" void ContentInfo__ctor_m1_1442 ();
extern "C" void ContentInfo__ctor_m1_1443 ();
extern "C" void ContentInfo__ctor_m1_1444 ();
extern "C" void ContentInfo_get_ASN1_m1_1445 ();
extern "C" void ContentInfo_get_Content_m1_1446 ();
extern "C" void ContentInfo_set_Content_m1_1447 ();
extern "C" void ContentInfo_get_ContentType_m1_1448 ();
extern "C" void ContentInfo_set_ContentType_m1_1449 ();
extern "C" void ContentInfo_GetASN1_m1_1450 ();
extern "C" void EncryptedData__ctor_m1_1451 ();
extern "C" void EncryptedData__ctor_m1_1452 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m1_1453 ();
extern "C" void EncryptedData_get_EncryptedContent_m1_1454 ();
extern "C" void StrongName__cctor_m1_1455 ();
extern "C" void StrongName_get_PublicKey_m1_1456 ();
extern "C" void StrongName_get_PublicKeyToken_m1_1457 ();
extern "C" void StrongName_get_TokenAlgorithm_m1_1458 ();
extern "C" void SecurityParser__ctor_m1_1459 ();
extern "C" void SecurityParser_LoadXml_m1_1460 ();
extern "C" void SecurityParser_ToXml_m1_1461 ();
extern "C" void SecurityParser_OnStartParsing_m1_1462 ();
extern "C" void SecurityParser_OnProcessingInstruction_m1_1463 ();
extern "C" void SecurityParser_OnIgnorableWhitespace_m1_1464 ();
extern "C" void SecurityParser_OnStartElement_m1_1465 ();
extern "C" void SecurityParser_OnEndElement_m1_1466 ();
extern "C" void SecurityParser_OnChars_m1_1467 ();
extern "C" void SecurityParser_OnEndParsing_m1_1468 ();
extern "C" void AttrListImpl__ctor_m1_1469 ();
extern "C" void AttrListImpl_get_Length_m1_1470 ();
extern "C" void AttrListImpl_GetName_m1_1471 ();
extern "C" void AttrListImpl_GetValue_m1_1472 ();
extern "C" void AttrListImpl_GetValue_m1_1473 ();
extern "C" void AttrListImpl_get_Names_m1_1474 ();
extern "C" void AttrListImpl_get_Values_m1_1475 ();
extern "C" void AttrListImpl_Clear_m1_1476 ();
extern "C" void AttrListImpl_Add_m1_1477 ();
extern "C" void SmallXmlParser__ctor_m1_1478 ();
extern "C" void SmallXmlParser_Error_m1_1479 ();
extern "C" void SmallXmlParser_UnexpectedEndError_m1_1480 ();
extern "C" void SmallXmlParser_IsNameChar_m1_1481 ();
extern "C" void SmallXmlParser_IsWhitespace_m1_1482 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m1_1483 ();
extern "C" void SmallXmlParser_HandleWhitespaces_m1_1484 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m1_1485 ();
extern "C" void SmallXmlParser_Peek_m1_1486 ();
extern "C" void SmallXmlParser_Read_m1_1487 ();
extern "C" void SmallXmlParser_Expect_m1_1488 ();
extern "C" void SmallXmlParser_ReadUntil_m1_1489 ();
extern "C" void SmallXmlParser_ReadName_m1_1490 ();
extern "C" void SmallXmlParser_Parse_m1_1491 ();
extern "C" void SmallXmlParser_Cleanup_m1_1492 ();
extern "C" void SmallXmlParser_ReadContent_m1_1493 ();
extern "C" void SmallXmlParser_HandleBufferedContent_m1_1494 ();
extern "C" void SmallXmlParser_ReadCharacters_m1_1495 ();
extern "C" void SmallXmlParser_ReadReference_m1_1496 ();
extern "C" void SmallXmlParser_ReadCharacterReference_m1_1497 ();
extern "C" void SmallXmlParser_ReadAttribute_m1_1498 ();
extern "C" void SmallXmlParser_ReadCDATASection_m1_1499 ();
extern "C" void SmallXmlParser_ReadComment_m1_1500 ();
extern "C" void SmallXmlParserException__ctor_m1_1501 ();
extern "C" void Runtime_GetDisplayName_m1_1502 ();
extern "C" void KeyNotFoundException__ctor_m1_1503 ();
extern "C" void KeyNotFoundException__ctor_m1_1504 ();
extern "C" void SimpleEnumerator__ctor_m1_1505 ();
extern "C" void SimpleEnumerator__cctor_m1_1506 ();
extern "C" void SimpleEnumerator_Clone_m1_1507 ();
extern "C" void SimpleEnumerator_MoveNext_m1_1508 ();
extern "C" void SimpleEnumerator_get_Current_m1_1509 ();
extern "C" void SimpleEnumerator_Reset_m1_1510 ();
extern "C" void ArrayListWrapper__ctor_m1_1511 ();
extern "C" void ArrayListWrapper_get_Item_m1_1512 ();
extern "C" void ArrayListWrapper_set_Item_m1_1513 ();
extern "C" void ArrayListWrapper_get_Count_m1_1514 ();
extern "C" void ArrayListWrapper_get_Capacity_m1_1515 ();
extern "C" void ArrayListWrapper_set_Capacity_m1_1516 ();
extern "C" void ArrayListWrapper_get_IsReadOnly_m1_1517 ();
extern "C" void ArrayListWrapper_get_IsSynchronized_m1_1518 ();
extern "C" void ArrayListWrapper_get_SyncRoot_m1_1519 ();
extern "C" void ArrayListWrapper_Add_m1_1520 ();
extern "C" void ArrayListWrapper_Clear_m1_1521 ();
extern "C" void ArrayListWrapper_Contains_m1_1522 ();
extern "C" void ArrayListWrapper_IndexOf_m1_1523 ();
extern "C" void ArrayListWrapper_IndexOf_m1_1524 ();
extern "C" void ArrayListWrapper_IndexOf_m1_1525 ();
extern "C" void ArrayListWrapper_Insert_m1_1526 ();
extern "C" void ArrayListWrapper_InsertRange_m1_1527 ();
extern "C" void ArrayListWrapper_Remove_m1_1528 ();
extern "C" void ArrayListWrapper_RemoveAt_m1_1529 ();
extern "C" void ArrayListWrapper_CopyTo_m1_1530 ();
extern "C" void ArrayListWrapper_CopyTo_m1_1531 ();
extern "C" void ArrayListWrapper_CopyTo_m1_1532 ();
extern "C" void ArrayListWrapper_GetEnumerator_m1_1533 ();
extern "C" void ArrayListWrapper_AddRange_m1_1534 ();
extern "C" void ArrayListWrapper_Clone_m1_1535 ();
extern "C" void ArrayListWrapper_Sort_m1_1536 ();
extern "C" void ArrayListWrapper_Sort_m1_1537 ();
extern "C" void ArrayListWrapper_ToArray_m1_1538 ();
extern "C" void ArrayListWrapper_ToArray_m1_1539 ();
extern "C" void SynchronizedArrayListWrapper__ctor_m1_1540 ();
extern "C" void SynchronizedArrayListWrapper_get_Item_m1_1541 ();
extern "C" void SynchronizedArrayListWrapper_set_Item_m1_1542 ();
extern "C" void SynchronizedArrayListWrapper_get_Count_m1_1543 ();
extern "C" void SynchronizedArrayListWrapper_get_Capacity_m1_1544 ();
extern "C" void SynchronizedArrayListWrapper_set_Capacity_m1_1545 ();
extern "C" void SynchronizedArrayListWrapper_get_IsReadOnly_m1_1546 ();
extern "C" void SynchronizedArrayListWrapper_get_IsSynchronized_m1_1547 ();
extern "C" void SynchronizedArrayListWrapper_get_SyncRoot_m1_1548 ();
extern "C" void SynchronizedArrayListWrapper_Add_m1_1549 ();
extern "C" void SynchronizedArrayListWrapper_Clear_m1_1550 ();
extern "C" void SynchronizedArrayListWrapper_Contains_m1_1551 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m1_1552 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m1_1553 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m1_1554 ();
extern "C" void SynchronizedArrayListWrapper_Insert_m1_1555 ();
extern "C" void SynchronizedArrayListWrapper_InsertRange_m1_1556 ();
extern "C" void SynchronizedArrayListWrapper_Remove_m1_1557 ();
extern "C" void SynchronizedArrayListWrapper_RemoveAt_m1_1558 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m1_1559 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m1_1560 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m1_1561 ();
extern "C" void SynchronizedArrayListWrapper_GetEnumerator_m1_1562 ();
extern "C" void SynchronizedArrayListWrapper_AddRange_m1_1563 ();
extern "C" void SynchronizedArrayListWrapper_Clone_m1_1564 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m1_1565 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m1_1566 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m1_1567 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m1_1568 ();
extern "C" void FixedSizeArrayListWrapper__ctor_m1_1569 ();
extern "C" void FixedSizeArrayListWrapper_get_ErrorMessage_m1_1570 ();
extern "C" void FixedSizeArrayListWrapper_get_Capacity_m1_1571 ();
extern "C" void FixedSizeArrayListWrapper_set_Capacity_m1_1572 ();
extern "C" void FixedSizeArrayListWrapper_Add_m1_1573 ();
extern "C" void FixedSizeArrayListWrapper_AddRange_m1_1574 ();
extern "C" void FixedSizeArrayListWrapper_Clear_m1_1575 ();
extern "C" void FixedSizeArrayListWrapper_Insert_m1_1576 ();
extern "C" void FixedSizeArrayListWrapper_InsertRange_m1_1577 ();
extern "C" void FixedSizeArrayListWrapper_Remove_m1_1578 ();
extern "C" void FixedSizeArrayListWrapper_RemoveAt_m1_1579 ();
extern "C" void ReadOnlyArrayListWrapper__ctor_m1_1580 ();
extern "C" void ReadOnlyArrayListWrapper_get_ErrorMessage_m1_1581 ();
extern "C" void ReadOnlyArrayListWrapper_get_IsReadOnly_m1_1582 ();
extern "C" void ReadOnlyArrayListWrapper_get_Item_m1_1583 ();
extern "C" void ReadOnlyArrayListWrapper_set_Item_m1_1584 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m1_1585 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m1_1586 ();
extern "C" void ArrayList__ctor_m1_1587 ();
extern "C" void ArrayList__ctor_m1_1588 ();
extern "C" void ArrayList__ctor_m1_1589 ();
extern "C" void ArrayList__ctor_m1_1590 ();
extern "C" void ArrayList__cctor_m1_1591 ();
extern "C" void ArrayList_get_Item_m1_1592 ();
extern "C" void ArrayList_set_Item_m1_1593 ();
extern "C" void ArrayList_get_Count_m1_1594 ();
extern "C" void ArrayList_get_Capacity_m1_1595 ();
extern "C" void ArrayList_set_Capacity_m1_1596 ();
extern "C" void ArrayList_get_IsReadOnly_m1_1597 ();
extern "C" void ArrayList_get_IsSynchronized_m1_1598 ();
extern "C" void ArrayList_get_SyncRoot_m1_1599 ();
extern "C" void ArrayList_EnsureCapacity_m1_1600 ();
extern "C" void ArrayList_Shift_m1_1601 ();
extern "C" void ArrayList_Add_m1_1602 ();
extern "C" void ArrayList_Clear_m1_1603 ();
extern "C" void ArrayList_Contains_m1_1604 ();
extern "C" void ArrayList_IndexOf_m1_1605 ();
extern "C" void ArrayList_IndexOf_m1_1606 ();
extern "C" void ArrayList_IndexOf_m1_1607 ();
extern "C" void ArrayList_Insert_m1_1608 ();
extern "C" void ArrayList_InsertRange_m1_1609 ();
extern "C" void ArrayList_Remove_m1_1610 ();
extern "C" void ArrayList_RemoveAt_m1_1611 ();
extern "C" void ArrayList_CopyTo_m1_1612 ();
extern "C" void ArrayList_CopyTo_m1_1613 ();
extern "C" void ArrayList_CopyTo_m1_1614 ();
extern "C" void ArrayList_GetEnumerator_m1_1615 ();
extern "C" void ArrayList_AddRange_m1_1616 ();
extern "C" void ArrayList_Sort_m1_1617 ();
extern "C" void ArrayList_Sort_m1_1618 ();
extern "C" void ArrayList_ToArray_m1_1619 ();
extern "C" void ArrayList_ToArray_m1_1620 ();
extern "C" void ArrayList_Clone_m1_1621 ();
extern "C" void ArrayList_ThrowNewArgumentOutOfRangeException_m1_1622 ();
extern "C" void ArrayList_Synchronized_m1_1623 ();
extern "C" void ArrayList_ReadOnly_m1_1624 ();
extern "C" void BitArrayEnumerator__ctor_m1_1625 ();
extern "C" void BitArrayEnumerator_Clone_m1_1626 ();
extern "C" void BitArrayEnumerator_get_Current_m1_1627 ();
extern "C" void BitArrayEnumerator_MoveNext_m1_1628 ();
extern "C" void BitArrayEnumerator_Reset_m1_1629 ();
extern "C" void BitArrayEnumerator_checkVersion_m1_1630 ();
extern "C" void BitArray__ctor_m1_1631 ();
extern "C" void BitArray__ctor_m1_1632 ();
extern "C" void BitArray_getByte_m1_1633 ();
extern "C" void BitArray_get_Count_m1_1634 ();
extern "C" void BitArray_get_Item_m1_1635 ();
extern "C" void BitArray_set_Item_m1_1636 ();
extern "C" void BitArray_get_Length_m1_1637 ();
extern "C" void BitArray_get_SyncRoot_m1_1638 ();
extern "C" void BitArray_Clone_m1_1639 ();
extern "C" void BitArray_CopyTo_m1_1640 ();
extern "C" void BitArray_Get_m1_1641 ();
extern "C" void BitArray_Set_m1_1642 ();
extern "C" void BitArray_GetEnumerator_m1_1643 ();
extern "C" void CaseInsensitiveComparer__ctor_m1_1644 ();
extern "C" void CaseInsensitiveComparer__ctor_m1_1645 ();
extern "C" void CaseInsensitiveComparer__cctor_m1_1646 ();
extern "C" void CaseInsensitiveComparer_get_DefaultInvariant_m1_1647 ();
extern "C" void CaseInsensitiveComparer_Compare_m1_1648 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m1_1649 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m1_1650 ();
extern "C" void CaseInsensitiveHashCodeProvider__cctor_m1_1651 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m1_1652 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m1_1653 ();
extern "C" void CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m1_1654 ();
extern "C" void CaseInsensitiveHashCodeProvider_GetHashCode_m1_1655 ();
extern "C" void CollectionBase__ctor_m1_1656 ();
extern "C" void CollectionBase_System_Collections_ICollection_CopyTo_m1_1657 ();
extern "C" void CollectionBase_System_Collections_ICollection_get_SyncRoot_m1_1658 ();
extern "C" void CollectionBase_System_Collections_IList_Add_m1_1659 ();
extern "C" void CollectionBase_System_Collections_IList_Contains_m1_1660 ();
extern "C" void CollectionBase_System_Collections_IList_IndexOf_m1_1661 ();
extern "C" void CollectionBase_System_Collections_IList_Insert_m1_1662 ();
extern "C" void CollectionBase_System_Collections_IList_Remove_m1_1663 ();
extern "C" void CollectionBase_System_Collections_IList_get_Item_m1_1664 ();
extern "C" void CollectionBase_System_Collections_IList_set_Item_m1_1665 ();
extern "C" void CollectionBase_get_Count_m1_1666 ();
extern "C" void CollectionBase_GetEnumerator_m1_1667 ();
extern "C" void CollectionBase_Clear_m1_1668 ();
extern "C" void CollectionBase_RemoveAt_m1_1669 ();
extern "C" void CollectionBase_get_InnerList_m1_1670 ();
extern "C" void CollectionBase_get_List_m1_1671 ();
extern "C" void CollectionBase_OnClear_m1_1672 ();
extern "C" void CollectionBase_OnClearComplete_m1_1673 ();
extern "C" void CollectionBase_OnInsert_m1_1674 ();
extern "C" void CollectionBase_OnInsertComplete_m1_1675 ();
extern "C" void CollectionBase_OnRemove_m1_1676 ();
extern "C" void CollectionBase_OnRemoveComplete_m1_1677 ();
extern "C" void CollectionBase_OnSet_m1_1678 ();
extern "C" void CollectionBase_OnSetComplete_m1_1679 ();
extern "C" void CollectionBase_OnValidate_m1_1680 ();
extern "C" void Comparer__ctor_m1_1681 ();
extern "C" void Comparer__ctor_m1_1682 ();
extern "C" void Comparer__cctor_m1_1683 ();
extern "C" void Comparer_Compare_m1_1684 ();
extern "C" void Comparer_GetObjectData_m1_1685 ();
extern "C" void DictionaryEntry__ctor_m1_1686 ();
extern "C" void DictionaryEntry_get_Key_m1_1687 ();
extern "C" void DictionaryEntry_get_Value_m1_1688 ();
extern "C" void KeyMarker__ctor_m1_1689 ();
extern "C" void KeyMarker__cctor_m1_1690 ();
extern "C" void Enumerator__ctor_m1_1691 ();
extern "C" void Enumerator__cctor_m1_1692 ();
extern "C" void Enumerator_FailFast_m1_1693 ();
extern "C" void Enumerator_Reset_m1_1694 ();
extern "C" void Enumerator_MoveNext_m1_1695 ();
extern "C" void Enumerator_get_Entry_m1_1696 ();
extern "C" void Enumerator_get_Key_m1_1697 ();
extern "C" void Enumerator_get_Value_m1_1698 ();
extern "C" void Enumerator_get_Current_m1_1699 ();
extern "C" void HashKeys__ctor_m1_1700 ();
extern "C" void HashKeys_get_Count_m1_1701 ();
extern "C" void HashKeys_get_SyncRoot_m1_1702 ();
extern "C" void HashKeys_CopyTo_m1_1703 ();
extern "C" void HashKeys_GetEnumerator_m1_1704 ();
extern "C" void HashValues__ctor_m1_1705 ();
extern "C" void HashValues_get_Count_m1_1706 ();
extern "C" void HashValues_get_SyncRoot_m1_1707 ();
extern "C" void HashValues_CopyTo_m1_1708 ();
extern "C" void HashValues_GetEnumerator_m1_1709 ();
extern "C" void SyncHashtable__ctor_m1_1710 ();
extern "C" void SyncHashtable__ctor_m1_1711 ();
extern "C" void SyncHashtable_System_Collections_IEnumerable_GetEnumerator_m1_1712 ();
extern "C" void SyncHashtable_GetObjectData_m1_1713 ();
extern "C" void SyncHashtable_get_Count_m1_1714 ();
extern "C" void SyncHashtable_get_SyncRoot_m1_1715 ();
extern "C" void SyncHashtable_get_Keys_m1_1716 ();
extern "C" void SyncHashtable_get_Values_m1_1717 ();
extern "C" void SyncHashtable_get_Item_m1_1718 ();
extern "C" void SyncHashtable_set_Item_m1_1719 ();
extern "C" void SyncHashtable_CopyTo_m1_1720 ();
extern "C" void SyncHashtable_Add_m1_1721 ();
extern "C" void SyncHashtable_Clear_m1_1722 ();
extern "C" void SyncHashtable_Contains_m1_1723 ();
extern "C" void SyncHashtable_GetEnumerator_m1_1724 ();
extern "C" void SyncHashtable_Remove_m1_1725 ();
extern "C" void SyncHashtable_ContainsKey_m1_1726 ();
extern "C" void SyncHashtable_Clone_m1_1727 ();
extern "C" void Hashtable__ctor_m1_1728 ();
extern "C" void Hashtable__ctor_m1_1729 ();
extern "C" void Hashtable__ctor_m1_1730 ();
extern "C" void Hashtable__ctor_m1_1731 ();
extern "C" void Hashtable__ctor_m1_1732 ();
extern "C" void Hashtable__ctor_m1_1733 ();
extern "C" void Hashtable__ctor_m1_1734 ();
extern "C" void Hashtable__ctor_m1_1735 ();
extern "C" void Hashtable__ctor_m1_1736 ();
extern "C" void Hashtable__ctor_m1_1737 ();
extern "C" void Hashtable__ctor_m1_1738 ();
extern "C" void Hashtable__ctor_m1_1739 ();
extern "C" void Hashtable__cctor_m1_1740 ();
extern "C" void Hashtable_System_Collections_IEnumerable_GetEnumerator_m1_1741 ();
extern "C" void Hashtable_set_comparer_m1_1742 ();
extern "C" void Hashtable_set_hcp_m1_1743 ();
extern "C" void Hashtable_get_Count_m1_1744 ();
extern "C" void Hashtable_get_SyncRoot_m1_1745 ();
extern "C" void Hashtable_get_Keys_m1_1746 ();
extern "C" void Hashtable_get_Values_m1_1747 ();
extern "C" void Hashtable_get_Item_m1_1748 ();
extern "C" void Hashtable_set_Item_m1_1749 ();
extern "C" void Hashtable_CopyTo_m1_1750 ();
extern "C" void Hashtable_Add_m1_1751 ();
extern "C" void Hashtable_Clear_m1_1752 ();
extern "C" void Hashtable_Contains_m1_1753 ();
extern "C" void Hashtable_GetEnumerator_m1_1754 ();
extern "C" void Hashtable_Remove_m1_1755 ();
extern "C" void Hashtable_ContainsKey_m1_1756 ();
extern "C" void Hashtable_Clone_m1_1757 ();
extern "C" void Hashtable_GetObjectData_m1_1758 ();
extern "C" void Hashtable_OnDeserialization_m1_1759 ();
extern "C" void Hashtable_Synchronized_m1_1760 ();
extern "C" void Hashtable_GetHash_m1_1761 ();
extern "C" void Hashtable_KeyEquals_m1_1762 ();
extern "C" void Hashtable_AdjustThreshold_m1_1763 ();
extern "C" void Hashtable_SetTable_m1_1764 ();
extern "C" void Hashtable_Find_m1_1765 ();
extern "C" void Hashtable_Rehash_m1_1766 ();
extern "C" void Hashtable_PutImpl_m1_1767 ();
extern "C" void Hashtable_CopyToArray_m1_1768 ();
extern "C" void Hashtable_TestPrime_m1_1769 ();
extern "C" void Hashtable_CalcPrime_m1_1770 ();
extern "C" void Hashtable_ToPrime_m1_1771 ();
extern "C" void QueueEnumerator__ctor_m1_1772 ();
extern "C" void QueueEnumerator_Clone_m1_1773 ();
extern "C" void QueueEnumerator_get_Current_m1_1774 ();
extern "C" void QueueEnumerator_MoveNext_m1_1775 ();
extern "C" void QueueEnumerator_Reset_m1_1776 ();
extern "C" void Queue__ctor_m1_1777 ();
extern "C" void Queue__ctor_m1_1778 ();
extern "C" void Queue__ctor_m1_1779 ();
extern "C" void Queue_get_Count_m1_1780 ();
extern "C" void Queue_get_SyncRoot_m1_1781 ();
extern "C" void Queue_CopyTo_m1_1782 ();
extern "C" void Queue_GetEnumerator_m1_1783 ();
extern "C" void Queue_Clone_m1_1784 ();
extern "C" void Enumerator__ctor_m1_1785 ();
extern "C" void Enumerator__cctor_m1_1786 ();
extern "C" void Enumerator_Reset_m1_1787 ();
extern "C" void Enumerator_MoveNext_m1_1788 ();
extern "C" void Enumerator_get_Entry_m1_1789 ();
extern "C" void Enumerator_get_Key_m1_1790 ();
extern "C" void Enumerator_get_Value_m1_1791 ();
extern "C" void Enumerator_get_Current_m1_1792 ();
extern "C" void Enumerator_Clone_m1_1793 ();
extern "C" void ListKeys__ctor_m1_1794 ();
extern "C" void ListKeys_get_Count_m1_1795 ();
extern "C" void ListKeys_get_SyncRoot_m1_1796 ();
extern "C" void ListKeys_CopyTo_m1_1797 ();
extern "C" void ListKeys_get_Item_m1_1798 ();
extern "C" void ListKeys_set_Item_m1_1799 ();
extern "C" void ListKeys_Add_m1_1800 ();
extern "C" void ListKeys_Clear_m1_1801 ();
extern "C" void ListKeys_Contains_m1_1802 ();
extern "C" void ListKeys_IndexOf_m1_1803 ();
extern "C" void ListKeys_Insert_m1_1804 ();
extern "C" void ListKeys_Remove_m1_1805 ();
extern "C" void ListKeys_RemoveAt_m1_1806 ();
extern "C" void ListKeys_GetEnumerator_m1_1807 ();
extern "C" void SortedList__ctor_m1_1808 ();
extern "C" void SortedList__ctor_m1_1809 ();
extern "C" void SortedList__ctor_m1_1810 ();
extern "C" void SortedList__ctor_m1_1811 ();
extern "C" void SortedList__cctor_m1_1812 ();
extern "C" void SortedList_System_Collections_IEnumerable_GetEnumerator_m1_1813 ();
extern "C" void SortedList_get_Count_m1_1814 ();
extern "C" void SortedList_get_SyncRoot_m1_1815 ();
extern "C" void SortedList_get_IsFixedSize_m1_1816 ();
extern "C" void SortedList_get_IsReadOnly_m1_1817 ();
extern "C" void SortedList_get_Keys_m1_1818 ();
extern "C" void SortedList_get_Item_m1_1819 ();
extern "C" void SortedList_set_Item_m1_1820 ();
extern "C" void SortedList_get_Capacity_m1_1821 ();
extern "C" void SortedList_set_Capacity_m1_1822 ();
extern "C" void SortedList_Add_m1_1823 ();
extern "C" void SortedList_Contains_m1_1824 ();
extern "C" void SortedList_GetEnumerator_m1_1825 ();
extern "C" void SortedList_Remove_m1_1826 ();
extern "C" void SortedList_CopyTo_m1_1827 ();
extern "C" void SortedList_Clone_m1_1828 ();
extern "C" void SortedList_RemoveAt_m1_1829 ();
extern "C" void SortedList_IndexOfKey_m1_1830 ();
extern "C" void SortedList_ContainsKey_m1_1831 ();
extern "C" void SortedList_GetByIndex_m1_1832 ();
extern "C" void SortedList_GetKey_m1_1833 ();
extern "C" void SortedList_EnsureCapacity_m1_1834 ();
extern "C" void SortedList_PutImpl_m1_1835 ();
extern "C" void SortedList_GetImpl_m1_1836 ();
extern "C" void SortedList_InitTable_m1_1837 ();
extern "C" void SortedList_CopyToArray_m1_1838 ();
extern "C" void SortedList_Find_m1_1839 ();
extern "C" void Enumerator__ctor_m1_1840 ();
extern "C" void Enumerator_Clone_m1_1841 ();
extern "C" void Enumerator_get_Current_m1_1842 ();
extern "C" void Enumerator_MoveNext_m1_1843 ();
extern "C" void Enumerator_Reset_m1_1844 ();
extern "C" void Stack__ctor_m1_1845 ();
extern "C" void Stack__ctor_m1_1846 ();
extern "C" void Stack__ctor_m1_1847 ();
extern "C" void Stack_Resize_m1_1848 ();
extern "C" void Stack_get_Count_m1_1849 ();
extern "C" void Stack_get_SyncRoot_m1_1850 ();
extern "C" void Stack_Clear_m1_1851 ();
extern "C" void Stack_Clone_m1_1852 ();
extern "C" void Stack_CopyTo_m1_1853 ();
extern "C" void Stack_GetEnumerator_m1_1854 ();
extern "C" void Stack_Peek_m1_1855 ();
extern "C" void Stack_Pop_m1_1856 ();
extern "C" void Stack_Push_m1_1857 ();
extern "C" void DebuggableAttribute__ctor_m1_1858 ();
extern "C" void DebuggerDisplayAttribute__ctor_m1_1859 ();
extern "C" void DebuggerDisplayAttribute_set_Name_m1_1860 ();
extern "C" void DebuggerStepThroughAttribute__ctor_m1_1861 ();
extern "C" void DebuggerTypeProxyAttribute__ctor_m1_1862 ();
extern "C" void StackFrame__ctor_m1_1863 ();
extern "C" void StackFrame__ctor_m1_1864 ();
extern "C" void StackFrame_get_frame_info_m1_1865 ();
extern "C" void StackFrame_GetFileLineNumber_m1_1866 ();
extern "C" void StackFrame_GetFileName_m1_1867 ();
extern "C" void StackFrame_GetSecureFileName_m1_1868 ();
extern "C" void StackFrame_GetILOffset_m1_1869 ();
extern "C" void StackFrame_GetMethod_m1_1870 ();
extern "C" void StackFrame_GetNativeOffset_m1_1871 ();
extern "C" void StackFrame_GetInternalMethodName_m1_1872 ();
extern "C" void StackFrame_ToString_m1_1873 ();
extern "C" void StackTrace__ctor_m1_1874 ();
extern "C" void StackTrace__ctor_m1_1875 ();
extern "C" void StackTrace__ctor_m1_1876 ();
extern "C" void StackTrace__ctor_m1_1877 ();
extern "C" void StackTrace__ctor_m1_1878 ();
extern "C" void StackTrace_init_frames_m1_1879 ();
extern "C" void StackTrace_get_trace_m1_1880 ();
extern "C" void StackTrace_get_FrameCount_m1_1881 ();
extern "C" void StackTrace_GetFrame_m1_1882 ();
extern "C" void StackTrace_ToString_m1_1883 ();
extern "C" void Calendar__ctor_m1_1884 ();
extern "C" void Calendar_Clone_m1_1885 ();
extern "C" void Calendar_CheckReadOnly_m1_1886 ();
extern "C" void Calendar_get_EraNames_m1_1887 ();
extern "C" void CCMath_div_m1_1888 ();
extern "C" void CCMath_mod_m1_1889 ();
extern "C" void CCMath_div_mod_m1_1890 ();
extern "C" void CCFixed_FromDateTime_m1_1891 ();
extern "C" void CCFixed_day_of_week_m1_1892 ();
extern "C" void CCGregorianCalendar_is_leap_year_m1_1893 ();
extern "C" void CCGregorianCalendar_fixed_from_dmy_m1_1894 ();
extern "C" void CCGregorianCalendar_year_from_fixed_m1_1895 ();
extern "C" void CCGregorianCalendar_my_from_fixed_m1_1896 ();
extern "C" void CCGregorianCalendar_dmy_from_fixed_m1_1897 ();
extern "C" void CCGregorianCalendar_month_from_fixed_m1_1898 ();
extern "C" void CCGregorianCalendar_day_from_fixed_m1_1899 ();
extern "C" void CCGregorianCalendar_GetDayOfMonth_m1_1900 ();
extern "C" void CCGregorianCalendar_GetMonth_m1_1901 ();
extern "C" void CCGregorianCalendar_GetYear_m1_1902 ();
extern "C" void CompareInfo__ctor_m1_1903 ();
extern "C" void CompareInfo__ctor_m1_1904 ();
extern "C" void CompareInfo__cctor_m1_1905 ();
extern "C" void CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_1906 ();
extern "C" void CompareInfo_get_UseManagedCollation_m1_1907 ();
extern "C" void CompareInfo_construct_compareinfo_m1_1908 ();
extern "C" void CompareInfo_free_internal_collator_m1_1909 ();
extern "C" void CompareInfo_internal_compare_m1_1910 ();
extern "C" void CompareInfo_assign_sortkey_m1_1911 ();
extern "C" void CompareInfo_internal_index_m1_1912 ();
extern "C" void CompareInfo_Finalize_m1_1913 ();
extern "C" void CompareInfo_internal_compare_managed_m1_1914 ();
extern "C" void CompareInfo_internal_compare_switch_m1_1915 ();
extern "C" void CompareInfo_Compare_m1_1916 ();
extern "C" void CompareInfo_Compare_m1_1917 ();
extern "C" void CompareInfo_Compare_m1_1918 ();
extern "C" void CompareInfo_Equals_m1_1919 ();
extern "C" void CompareInfo_GetHashCode_m1_1920 ();
extern "C" void CompareInfo_GetSortKey_m1_1921 ();
extern "C" void CompareInfo_IndexOf_m1_1922 ();
extern "C" void CompareInfo_internal_index_managed_m1_1923 ();
extern "C" void CompareInfo_internal_index_switch_m1_1924 ();
extern "C" void CompareInfo_IndexOf_m1_1925 ();
extern "C" void CompareInfo_IsPrefix_m1_1926 ();
extern "C" void CompareInfo_IsSuffix_m1_1927 ();
extern "C" void CompareInfo_LastIndexOf_m1_1928 ();
extern "C" void CompareInfo_LastIndexOf_m1_1929 ();
extern "C" void CompareInfo_ToString_m1_1930 ();
extern "C" void CompareInfo_get_LCID_m1_1931 ();
extern "C" void CultureInfo__ctor_m1_1932 ();
extern "C" void CultureInfo__ctor_m1_1933 ();
extern "C" void CultureInfo__ctor_m1_1934 ();
extern "C" void CultureInfo__ctor_m1_1935 ();
extern "C" void CultureInfo__ctor_m1_1936 ();
extern "C" void CultureInfo__cctor_m1_1937 ();
extern "C" void CultureInfo_get_InvariantCulture_m1_1938 ();
extern "C" void CultureInfo_get_CurrentCulture_m1_1939 ();
extern "C" void CultureInfo_get_CurrentUICulture_m1_1940 ();
extern "C" void CultureInfo_ConstructCurrentCulture_m1_1941 ();
extern "C" void CultureInfo_ConstructCurrentUICulture_m1_1942 ();
extern "C" void CultureInfo_get_LCID_m1_1943 ();
extern "C" void CultureInfo_get_Name_m1_1944 ();
extern "C" void CultureInfo_get_Parent_m1_1945 ();
extern "C" void CultureInfo_get_TextInfo_m1_1946 ();
extern "C" void CultureInfo_get_IcuName_m1_1947 ();
extern "C" void CultureInfo_Clone_m1_1948 ();
extern "C" void CultureInfo_Equals_m1_1949 ();
extern "C" void CultureInfo_GetHashCode_m1_1950 ();
extern "C" void CultureInfo_ToString_m1_1951 ();
extern "C" void CultureInfo_get_CompareInfo_m1_1952 ();
extern "C" void CultureInfo_get_IsNeutralCulture_m1_1953 ();
extern "C" void CultureInfo_CheckNeutral_m1_1954 ();
extern "C" void CultureInfo_get_NumberFormat_m1_1955 ();
extern "C" void CultureInfo_set_NumberFormat_m1_1956 ();
extern "C" void CultureInfo_get_DateTimeFormat_m1_1957 ();
extern "C" void CultureInfo_set_DateTimeFormat_m1_1958 ();
extern "C" void CultureInfo_get_IsReadOnly_m1_1959 ();
extern "C" void CultureInfo_GetFormat_m1_1960 ();
extern "C" void CultureInfo_Construct_m1_1961 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromName_m1_1962 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromLcid_m1_1963 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromCurrentLocale_m1_1964 ();
extern "C" void CultureInfo_construct_internal_locale_from_lcid_m1_1965 ();
extern "C" void CultureInfo_construct_internal_locale_from_name_m1_1966 ();
extern "C" void CultureInfo_construct_internal_locale_from_current_locale_m1_1967 ();
extern "C" void CultureInfo_construct_datetime_format_m1_1968 ();
extern "C" void CultureInfo_construct_number_format_m1_1969 ();
extern "C" void CultureInfo_ConstructInvariant_m1_1970 ();
extern "C" void CultureInfo_CreateTextInfo_m1_1971 ();
extern "C" void CultureInfo_CreateCulture_m1_1972 ();
extern "C" void DateTimeFormatInfo__ctor_m1_1973 ();
extern "C" void DateTimeFormatInfo__ctor_m1_1974 ();
extern "C" void DateTimeFormatInfo__cctor_m1_1975 ();
extern "C" void DateTimeFormatInfo_GetInstance_m1_1976 ();
extern "C" void DateTimeFormatInfo_get_IsReadOnly_m1_1977 ();
extern "C" void DateTimeFormatInfo_ReadOnly_m1_1978 ();
extern "C" void DateTimeFormatInfo_Clone_m1_1979 ();
extern "C" void DateTimeFormatInfo_GetFormat_m1_1980 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedMonthName_m1_1981 ();
extern "C" void DateTimeFormatInfo_GetEraName_m1_1982 ();
extern "C" void DateTimeFormatInfo_GetMonthName_m1_1983 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedDayNames_m1_1984 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m1_1985 ();
extern "C" void DateTimeFormatInfo_get_RawDayNames_m1_1986 ();
extern "C" void DateTimeFormatInfo_get_RawMonthNames_m1_1987 ();
extern "C" void DateTimeFormatInfo_get_AMDesignator_m1_1988 ();
extern "C" void DateTimeFormatInfo_get_PMDesignator_m1_1989 ();
extern "C" void DateTimeFormatInfo_get_DateSeparator_m1_1990 ();
extern "C" void DateTimeFormatInfo_get_TimeSeparator_m1_1991 ();
extern "C" void DateTimeFormatInfo_get_LongDatePattern_m1_1992 ();
extern "C" void DateTimeFormatInfo_get_ShortDatePattern_m1_1993 ();
extern "C" void DateTimeFormatInfo_get_ShortTimePattern_m1_1994 ();
extern "C" void DateTimeFormatInfo_get_LongTimePattern_m1_1995 ();
extern "C" void DateTimeFormatInfo_get_MonthDayPattern_m1_1996 ();
extern "C" void DateTimeFormatInfo_get_YearMonthPattern_m1_1997 ();
extern "C" void DateTimeFormatInfo_get_FullDateTimePattern_m1_1998 ();
extern "C" void DateTimeFormatInfo_get_CurrentInfo_m1_1999 ();
extern "C" void DateTimeFormatInfo_get_InvariantInfo_m1_2000 ();
extern "C" void DateTimeFormatInfo_get_Calendar_m1_2001 ();
extern "C" void DateTimeFormatInfo_set_Calendar_m1_2002 ();
extern "C" void DateTimeFormatInfo_get_RFC1123Pattern_m1_2003 ();
extern "C" void DateTimeFormatInfo_get_RoundtripPattern_m1_2004 ();
extern "C" void DateTimeFormatInfo_get_SortableDateTimePattern_m1_2005 ();
extern "C" void DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m1_2006 ();
extern "C" void DateTimeFormatInfo_GetAllDateTimePatternsInternal_m1_2007 ();
extern "C" void DateTimeFormatInfo_FillAllDateTimePatterns_m1_2008 ();
extern "C" void DateTimeFormatInfo_GetAllRawDateTimePatterns_m1_2009 ();
extern "C" void DateTimeFormatInfo_GetDayName_m1_2010 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedDayName_m1_2011 ();
extern "C" void DateTimeFormatInfo_FillInvariantPatterns_m1_2012 ();
extern "C" void DateTimeFormatInfo_PopulateCombinedList_m1_2013 ();
extern "C" void DaylightTime__ctor_m1_2014 ();
extern "C" void DaylightTime_get_Start_m1_2015 ();
extern "C" void DaylightTime_get_End_m1_2016 ();
extern "C" void DaylightTime_get_Delta_m1_2017 ();
extern "C" void GregorianCalendar__ctor_m1_2018 ();
extern "C" void GregorianCalendar__ctor_m1_2019 ();
extern "C" void GregorianCalendar_get_Eras_m1_2020 ();
extern "C" void GregorianCalendar_set_CalendarType_m1_2021 ();
extern "C" void GregorianCalendar_GetDayOfMonth_m1_2022 ();
extern "C" void GregorianCalendar_GetDayOfWeek_m1_2023 ();
extern "C" void GregorianCalendar_GetEra_m1_2024 ();
extern "C" void GregorianCalendar_GetMonth_m1_2025 ();
extern "C" void GregorianCalendar_GetYear_m1_2026 ();
extern "C" void NumberFormatInfo__ctor_m1_2027 ();
extern "C" void NumberFormatInfo__ctor_m1_2028 ();
extern "C" void NumberFormatInfo__ctor_m1_2029 ();
extern "C" void NumberFormatInfo__cctor_m1_2030 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalDigits_m1_2031 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalSeparator_m1_2032 ();
extern "C" void NumberFormatInfo_get_CurrencyGroupSeparator_m1_2033 ();
extern "C" void NumberFormatInfo_get_RawCurrencyGroupSizes_m1_2034 ();
extern "C" void NumberFormatInfo_get_CurrencyNegativePattern_m1_2035 ();
extern "C" void NumberFormatInfo_get_CurrencyPositivePattern_m1_2036 ();
extern "C" void NumberFormatInfo_get_CurrencySymbol_m1_2037 ();
extern "C" void NumberFormatInfo_get_CurrentInfo_m1_2038 ();
extern "C" void NumberFormatInfo_get_InvariantInfo_m1_2039 ();
extern "C" void NumberFormatInfo_get_NaNSymbol_m1_2040 ();
extern "C" void NumberFormatInfo_get_NegativeInfinitySymbol_m1_2041 ();
extern "C" void NumberFormatInfo_get_NegativeSign_m1_2042 ();
extern "C" void NumberFormatInfo_get_NumberDecimalDigits_m1_2043 ();
extern "C" void NumberFormatInfo_get_NumberDecimalSeparator_m1_2044 ();
extern "C" void NumberFormatInfo_get_NumberGroupSeparator_m1_2045 ();
extern "C" void NumberFormatInfo_get_RawNumberGroupSizes_m1_2046 ();
extern "C" void NumberFormatInfo_get_NumberNegativePattern_m1_2047 ();
extern "C" void NumberFormatInfo_set_NumberNegativePattern_m1_2048 ();
extern "C" void NumberFormatInfo_get_PercentDecimalDigits_m1_2049 ();
extern "C" void NumberFormatInfo_get_PercentDecimalSeparator_m1_2050 ();
extern "C" void NumberFormatInfo_get_PercentGroupSeparator_m1_2051 ();
extern "C" void NumberFormatInfo_get_RawPercentGroupSizes_m1_2052 ();
extern "C" void NumberFormatInfo_get_PercentNegativePattern_m1_2053 ();
extern "C" void NumberFormatInfo_get_PercentPositivePattern_m1_2054 ();
extern "C" void NumberFormatInfo_get_PercentSymbol_m1_2055 ();
extern "C" void NumberFormatInfo_get_PerMilleSymbol_m1_2056 ();
extern "C" void NumberFormatInfo_get_PositiveInfinitySymbol_m1_2057 ();
extern "C" void NumberFormatInfo_get_PositiveSign_m1_2058 ();
extern "C" void NumberFormatInfo_GetFormat_m1_2059 ();
extern "C" void NumberFormatInfo_Clone_m1_2060 ();
extern "C" void NumberFormatInfo_GetInstance_m1_2061 ();
extern "C" void TextInfo__ctor_m1_2062 ();
extern "C" void TextInfo__ctor_m1_2063 ();
extern "C" void TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_2064 ();
extern "C" void TextInfo_get_ListSeparator_m1_2065 ();
extern "C" void TextInfo_get_CultureName_m1_2066 ();
extern "C" void TextInfo_Equals_m1_2067 ();
extern "C" void TextInfo_GetHashCode_m1_2068 ();
extern "C" void TextInfo_ToString_m1_2069 ();
extern "C" void TextInfo_ToLower_m1_2070 ();
extern "C" void TextInfo_ToUpper_m1_2071 ();
extern "C" void TextInfo_ToLower_m1_2072 ();
extern "C" void TextInfo_Clone_m1_2073 ();
extern "C" void IsolatedStorageException__ctor_m1_2074 ();
extern "C" void IsolatedStorageException__ctor_m1_2075 ();
extern "C" void IsolatedStorageException__ctor_m1_2076 ();
extern "C" void BinaryReader__ctor_m1_2077 ();
extern "C" void BinaryReader__ctor_m1_2078 ();
extern "C" void BinaryReader_System_IDisposable_Dispose_m1_2079 ();
extern "C" void BinaryReader_get_BaseStream_m1_2080 ();
extern "C" void BinaryReader_Close_m1_2081 ();
extern "C" void BinaryReader_Dispose_m1_2082 ();
extern "C" void BinaryReader_FillBuffer_m1_2083 ();
extern "C" void BinaryReader_Read_m1_2084 ();
extern "C" void BinaryReader_Read_m1_2085 ();
extern "C" void BinaryReader_Read_m1_2086 ();
extern "C" void BinaryReader_ReadCharBytes_m1_2087 ();
extern "C" void BinaryReader_Read7BitEncodedInt_m1_2088 ();
extern "C" void BinaryReader_ReadBoolean_m1_2089 ();
extern "C" void BinaryReader_ReadByte_m1_2090 ();
extern "C" void BinaryReader_ReadBytes_m1_2091 ();
extern "C" void BinaryReader_ReadChar_m1_2092 ();
extern "C" void BinaryReader_ReadDecimal_m1_2093 ();
extern "C" void BinaryReader_ReadDouble_m1_2094 ();
extern "C" void BinaryReader_ReadInt16_m1_2095 ();
extern "C" void BinaryReader_ReadInt32_m1_2096 ();
extern "C" void BinaryReader_ReadInt64_m1_2097 ();
extern "C" void BinaryReader_ReadSByte_m1_2098 ();
extern "C" void BinaryReader_ReadString_m1_2099 ();
extern "C" void BinaryReader_ReadSingle_m1_2100 ();
extern "C" void BinaryReader_ReadUInt16_m1_2101 ();
extern "C" void BinaryReader_ReadUInt32_m1_2102 ();
extern "C" void BinaryReader_ReadUInt64_m1_2103 ();
extern "C" void BinaryReader_CheckBuffer_m1_2104 ();
extern "C" void BinaryWriter__ctor_m1_2105 ();
extern "C" void BinaryWriter__ctor_m1_2106 ();
extern "C" void BinaryWriter__ctor_m1_2107 ();
extern "C" void BinaryWriter__cctor_m1_2108 ();
extern "C" void BinaryWriter_System_IDisposable_Dispose_m1_2109 ();
extern "C" void BinaryWriter_Dispose_m1_2110 ();
extern "C" void BinaryWriter_Write_m1_2111 ();
extern "C" void Directory_CreateDirectory_m1_2112 ();
extern "C" void Directory_CreateDirectoriesInternal_m1_2113 ();
extern "C" void Directory_Exists_m1_2114 ();
extern "C" void Directory_GetCurrentDirectory_m1_2115 ();
extern "C" void Directory_GetFiles_m1_2116 ();
extern "C" void Directory_GetFileSystemEntries_m1_2117 ();
extern "C" void DirectoryInfo__ctor_m1_2118 ();
extern "C" void DirectoryInfo__ctor_m1_2119 ();
extern "C" void DirectoryInfo__ctor_m1_2120 ();
extern "C" void DirectoryInfo_Initialize_m1_2121 ();
extern "C" void DirectoryInfo_get_Exists_m1_2122 ();
extern "C" void DirectoryInfo_get_Parent_m1_2123 ();
extern "C" void DirectoryInfo_Create_m1_2124 ();
extern "C" void DirectoryInfo_ToString_m1_2125 ();
extern "C" void DirectoryNotFoundException__ctor_m1_2126 ();
extern "C" void DirectoryNotFoundException__ctor_m1_2127 ();
extern "C" void DirectoryNotFoundException__ctor_m1_2128 ();
extern "C" void EndOfStreamException__ctor_m1_2129 ();
extern "C" void EndOfStreamException__ctor_m1_2130 ();
extern "C" void File_Delete_m1_2131 ();
extern "C" void File_Exists_m1_2132 ();
extern "C" void File_Open_m1_2133 ();
extern "C" void File_OpenRead_m1_2134 ();
extern "C" void File_OpenText_m1_2135 ();
extern "C" void FileNotFoundException__ctor_m1_2136 ();
extern "C" void FileNotFoundException__ctor_m1_2137 ();
extern "C" void FileNotFoundException__ctor_m1_2138 ();
extern "C" void FileNotFoundException_get_Message_m1_2139 ();
extern "C" void FileNotFoundException_GetObjectData_m1_2140 ();
extern "C" void FileNotFoundException_ToString_m1_2141 ();
extern "C" void ReadDelegate__ctor_m1_2142 ();
extern "C" void ReadDelegate_Invoke_m1_2143 ();
extern "C" void ReadDelegate_BeginInvoke_m1_2144 ();
extern "C" void ReadDelegate_EndInvoke_m1_2145 ();
extern "C" void WriteDelegate__ctor_m1_2146 ();
extern "C" void WriteDelegate_Invoke_m1_2147 ();
extern "C" void WriteDelegate_BeginInvoke_m1_2148 ();
extern "C" void WriteDelegate_EndInvoke_m1_2149 ();
extern "C" void FileStream__ctor_m1_2150 ();
extern "C" void FileStream__ctor_m1_2151 ();
extern "C" void FileStream__ctor_m1_2152 ();
extern "C" void FileStream__ctor_m1_2153 ();
extern "C" void FileStream__ctor_m1_2154 ();
extern "C" void FileStream_get_CanRead_m1_2155 ();
extern "C" void FileStream_get_CanWrite_m1_2156 ();
extern "C" void FileStream_get_CanSeek_m1_2157 ();
extern "C" void FileStream_get_Length_m1_2158 ();
extern "C" void FileStream_get_Position_m1_2159 ();
extern "C" void FileStream_set_Position_m1_2160 ();
extern "C" void FileStream_ReadByte_m1_2161 ();
extern "C" void FileStream_WriteByte_m1_2162 ();
extern "C" void FileStream_Read_m1_2163 ();
extern "C" void FileStream_ReadInternal_m1_2164 ();
extern "C" void FileStream_BeginRead_m1_2165 ();
extern "C" void FileStream_EndRead_m1_2166 ();
extern "C" void FileStream_Write_m1_2167 ();
extern "C" void FileStream_WriteInternal_m1_2168 ();
extern "C" void FileStream_BeginWrite_m1_2169 ();
extern "C" void FileStream_EndWrite_m1_2170 ();
extern "C" void FileStream_Seek_m1_2171 ();
extern "C" void FileStream_SetLength_m1_2172 ();
extern "C" void FileStream_Flush_m1_2173 ();
extern "C" void FileStream_Finalize_m1_2174 ();
extern "C" void FileStream_Dispose_m1_2175 ();
extern "C" void FileStream_ReadSegment_m1_2176 ();
extern "C" void FileStream_WriteSegment_m1_2177 ();
extern "C" void FileStream_FlushBuffer_m1_2178 ();
extern "C" void FileStream_FlushBuffer_m1_2179 ();
extern "C" void FileStream_FlushBufferIfDirty_m1_2180 ();
extern "C" void FileStream_RefillBuffer_m1_2181 ();
extern "C" void FileStream_ReadData_m1_2182 ();
extern "C" void FileStream_InitBuffer_m1_2183 ();
extern "C" void FileStream_GetSecureFileName_m1_2184 ();
extern "C" void FileStream_GetSecureFileName_m1_2185 ();
extern "C" void FileStreamAsyncResult__ctor_m1_2186 ();
extern "C" void FileStreamAsyncResult_CBWrapper_m1_2187 ();
extern "C" void FileStreamAsyncResult_get_AsyncState_m1_2188 ();
extern "C" void FileStreamAsyncResult_get_AsyncWaitHandle_m1_2189 ();
extern "C" void FileStreamAsyncResult_get_IsCompleted_m1_2190 ();
extern "C" void FileSystemInfo__ctor_m1_2191 ();
extern "C" void FileSystemInfo__ctor_m1_2192 ();
extern "C" void FileSystemInfo_GetObjectData_m1_2193 ();
extern "C" void FileSystemInfo_get_FullName_m1_2194 ();
extern "C" void FileSystemInfo_Refresh_m1_2195 ();
extern "C" void FileSystemInfo_InternalRefresh_m1_2196 ();
extern "C" void FileSystemInfo_CheckPath_m1_2197 ();
extern "C" void IOException__ctor_m1_2198 ();
extern "C" void IOException__ctor_m1_2199 ();
extern "C" void IOException__ctor_m1_2200 ();
extern "C" void IOException__ctor_m1_2201 ();
extern "C" void IOException__ctor_m1_2202 ();
extern "C" void MemoryStream__ctor_m1_2203 ();
extern "C" void MemoryStream__ctor_m1_2204 ();
extern "C" void MemoryStream__ctor_m1_2205 ();
extern "C" void MemoryStream_InternalConstructor_m1_2206 ();
extern "C" void MemoryStream_CheckIfClosedThrowDisposed_m1_2207 ();
extern "C" void MemoryStream_get_CanRead_m1_2208 ();
extern "C" void MemoryStream_get_CanSeek_m1_2209 ();
extern "C" void MemoryStream_get_CanWrite_m1_2210 ();
extern "C" void MemoryStream_set_Capacity_m1_2211 ();
extern "C" void MemoryStream_get_Length_m1_2212 ();
extern "C" void MemoryStream_get_Position_m1_2213 ();
extern "C" void MemoryStream_set_Position_m1_2214 ();
extern "C" void MemoryStream_Dispose_m1_2215 ();
extern "C" void MemoryStream_Flush_m1_2216 ();
extern "C" void MemoryStream_Read_m1_2217 ();
extern "C" void MemoryStream_ReadByte_m1_2218 ();
extern "C" void MemoryStream_Seek_m1_2219 ();
extern "C" void MemoryStream_CalculateNewCapacity_m1_2220 ();
extern "C" void MemoryStream_Expand_m1_2221 ();
extern "C" void MemoryStream_SetLength_m1_2222 ();
extern "C" void MemoryStream_ToArray_m1_2223 ();
extern "C" void MemoryStream_Write_m1_2224 ();
extern "C" void MemoryStream_WriteByte_m1_2225 ();
extern "C" void MonoIO__cctor_m1_2226 ();
extern "C" void MonoIO_GetException_m1_2227 ();
extern "C" void MonoIO_GetException_m1_2228 ();
extern "C" void MonoIO_CreateDirectory_m1_2229 ();
extern "C" void MonoIO_GetFileSystemEntries_m1_2230 ();
extern "C" void MonoIO_GetCurrentDirectory_m1_2231 ();
extern "C" void MonoIO_DeleteFile_m1_2232 ();
extern "C" void MonoIO_GetFileAttributes_m1_2233 ();
extern "C" void MonoIO_GetFileType_m1_2234 ();
extern "C" void MonoIO_ExistsFile_m1_2235 ();
extern "C" void MonoIO_ExistsDirectory_m1_2236 ();
extern "C" void MonoIO_GetFileStat_m1_2237 ();
extern "C" void MonoIO_Open_m1_2238 ();
extern "C" void MonoIO_Close_m1_2239 ();
extern "C" void MonoIO_Read_m1_2240 ();
extern "C" void MonoIO_Write_m1_2241 ();
extern "C" void MonoIO_Seek_m1_2242 ();
extern "C" void MonoIO_GetLength_m1_2243 ();
extern "C" void MonoIO_SetLength_m1_2244 ();
extern "C" void MonoIO_get_ConsoleOutput_m1_2245 ();
extern "C" void MonoIO_get_ConsoleInput_m1_2246 ();
extern "C" void MonoIO_get_ConsoleError_m1_2247 ();
extern "C" void MonoIO_get_VolumeSeparatorChar_m1_2248 ();
extern "C" void MonoIO_get_DirectorySeparatorChar_m1_2249 ();
extern "C" void MonoIO_get_AltDirectorySeparatorChar_m1_2250 ();
extern "C" void MonoIO_get_PathSeparator_m1_2251 ();
extern "C" void Path__cctor_m1_2252 ();
extern "C" void Path_Combine_m1_2253 ();
extern "C" void Path_CleanPath_m1_2254 ();
extern "C" void Path_GetDirectoryName_m1_2255 ();
extern "C" void Path_GetFileName_m1_2256 ();
extern "C" void Path_GetFullPath_m1_2257 ();
extern "C" void Path_WindowsDriveAdjustment_m1_2258 ();
extern "C" void Path_InsecureGetFullPath_m1_2259 ();
extern "C" void Path_IsDsc_m1_2260 ();
extern "C" void Path_GetPathRoot_m1_2261 ();
extern "C" void Path_IsPathRooted_m1_2262 ();
extern "C" void Path_GetInvalidPathChars_m1_2263 ();
extern "C" void Path_GetServerAndShare_m1_2264 ();
extern "C" void Path_SameRoot_m1_2265 ();
extern "C" void Path_CanonicalizePath_m1_2266 ();
extern "C" void PathTooLongException__ctor_m1_2267 ();
extern "C" void PathTooLongException__ctor_m1_2268 ();
extern "C" void PathTooLongException__ctor_m1_2269 ();
extern "C" void SearchPattern__cctor_m1_2270 ();
extern "C" void Stream__ctor_m1_2271 ();
extern "C" void Stream__cctor_m1_2272 ();
extern "C" void Stream_Dispose_m1_2273 ();
extern "C" void Stream_Dispose_m1_2274 ();
extern "C" void Stream_Close_m1_2275 ();
extern "C" void Stream_ReadByte_m1_2276 ();
extern "C" void Stream_WriteByte_m1_2277 ();
extern "C" void Stream_BeginRead_m1_2278 ();
extern "C" void Stream_BeginWrite_m1_2279 ();
extern "C" void Stream_EndRead_m1_2280 ();
extern "C" void Stream_EndWrite_m1_2281 ();
extern "C" void NullStream__ctor_m1_2282 ();
extern "C" void NullStream_get_CanRead_m1_2283 ();
extern "C" void NullStream_get_CanSeek_m1_2284 ();
extern "C" void NullStream_get_CanWrite_m1_2285 ();
extern "C" void NullStream_get_Length_m1_2286 ();
extern "C" void NullStream_get_Position_m1_2287 ();
extern "C" void NullStream_set_Position_m1_2288 ();
extern "C" void NullStream_Flush_m1_2289 ();
extern "C" void NullStream_Read_m1_2290 ();
extern "C" void NullStream_ReadByte_m1_2291 ();
extern "C" void NullStream_Seek_m1_2292 ();
extern "C" void NullStream_SetLength_m1_2293 ();
extern "C" void NullStream_Write_m1_2294 ();
extern "C" void NullStream_WriteByte_m1_2295 ();
extern "C" void StreamAsyncResult__ctor_m1_2296 ();
extern "C" void StreamAsyncResult_SetComplete_m1_2297 ();
extern "C" void StreamAsyncResult_SetComplete_m1_2298 ();
extern "C" void StreamAsyncResult_get_AsyncState_m1_2299 ();
extern "C" void StreamAsyncResult_get_AsyncWaitHandle_m1_2300 ();
extern "C" void StreamAsyncResult_get_IsCompleted_m1_2301 ();
extern "C" void StreamAsyncResult_get_Exception_m1_2302 ();
extern "C" void StreamAsyncResult_get_NBytes_m1_2303 ();
extern "C" void StreamAsyncResult_get_Done_m1_2304 ();
extern "C" void StreamAsyncResult_set_Done_m1_2305 ();
extern "C" void NullStreamReader__ctor_m1_2306 ();
extern "C" void NullStreamReader_Peek_m1_2307 ();
extern "C" void NullStreamReader_Read_m1_2308 ();
extern "C" void NullStreamReader_Read_m1_2309 ();
extern "C" void NullStreamReader_ReadLine_m1_2310 ();
extern "C" void NullStreamReader_ReadToEnd_m1_2311 ();
extern "C" void StreamReader__ctor_m1_2312 ();
extern "C" void StreamReader__ctor_m1_2313 ();
extern "C" void StreamReader__ctor_m1_2314 ();
extern "C" void StreamReader__ctor_m1_2315 ();
extern "C" void StreamReader__ctor_m1_2316 ();
extern "C" void StreamReader__cctor_m1_2317 ();
extern "C" void StreamReader_Initialize_m1_2318 ();
extern "C" void StreamReader_Dispose_m1_2319 ();
extern "C" void StreamReader_DoChecks_m1_2320 ();
extern "C" void StreamReader_ReadBuffer_m1_2321 ();
extern "C" void StreamReader_Peek_m1_2322 ();
extern "C" void StreamReader_Read_m1_2323 ();
extern "C" void StreamReader_Read_m1_2324 ();
extern "C" void StreamReader_FindNextEOL_m1_2325 ();
extern "C" void StreamReader_ReadLine_m1_2326 ();
extern "C" void StreamReader_ReadToEnd_m1_2327 ();
extern "C" void StreamWriter__ctor_m1_2328 ();
extern "C" void StreamWriter__ctor_m1_2329 ();
extern "C" void StreamWriter__cctor_m1_2330 ();
extern "C" void StreamWriter_Initialize_m1_2331 ();
extern "C" void StreamWriter_set_AutoFlush_m1_2332 ();
extern "C" void StreamWriter_Dispose_m1_2333 ();
extern "C" void StreamWriter_Flush_m1_2334 ();
extern "C" void StreamWriter_FlushBytes_m1_2335 ();
extern "C" void StreamWriter_Decode_m1_2336 ();
extern "C" void StreamWriter_Write_m1_2337 ();
extern "C" void StreamWriter_LowLevelWrite_m1_2338 ();
extern "C" void StreamWriter_LowLevelWrite_m1_2339 ();
extern "C" void StreamWriter_Write_m1_2340 ();
extern "C" void StreamWriter_Write_m1_2341 ();
extern "C" void StreamWriter_Write_m1_2342 ();
extern "C" void StreamWriter_Close_m1_2343 ();
extern "C" void StreamWriter_Finalize_m1_2344 ();
extern "C" void StringReader__ctor_m1_2345 ();
extern "C" void StringReader_Dispose_m1_2346 ();
extern "C" void StringReader_Peek_m1_2347 ();
extern "C" void StringReader_Read_m1_2348 ();
extern "C" void StringReader_Read_m1_2349 ();
extern "C" void StringReader_ReadLine_m1_2350 ();
extern "C" void StringReader_ReadToEnd_m1_2351 ();
extern "C" void StringReader_CheckObjectDisposedException_m1_2352 ();
extern "C" void NullTextReader__ctor_m1_2353 ();
extern "C" void NullTextReader_ReadLine_m1_2354 ();
extern "C" void TextReader__ctor_m1_2355 ();
extern "C" void TextReader__cctor_m1_2356 ();
extern "C" void TextReader_Dispose_m1_2357 ();
extern "C" void TextReader_Dispose_m1_2358 ();
extern "C" void TextReader_Peek_m1_2359 ();
extern "C" void TextReader_Read_m1_2360 ();
extern "C" void TextReader_Read_m1_2361 ();
extern "C" void TextReader_ReadLine_m1_2362 ();
extern "C" void TextReader_ReadToEnd_m1_2363 ();
extern "C" void TextReader_Synchronized_m1_2364 ();
extern "C" void SynchronizedReader__ctor_m1_2365 ();
extern "C" void SynchronizedReader_Peek_m1_2366 ();
extern "C" void SynchronizedReader_ReadLine_m1_2367 ();
extern "C" void SynchronizedReader_ReadToEnd_m1_2368 ();
extern "C" void SynchronizedReader_Read_m1_2369 ();
extern "C" void SynchronizedReader_Read_m1_2370 ();
extern "C" void NullTextWriter__ctor_m1_2371 ();
extern "C" void NullTextWriter_Write_m1_2372 ();
extern "C" void NullTextWriter_Write_m1_2373 ();
extern "C" void NullTextWriter_Write_m1_2374 ();
extern "C" void TextWriter__ctor_m1_2375 ();
extern "C" void TextWriter__cctor_m1_2376 ();
extern "C" void TextWriter_Close_m1_2377 ();
extern "C" void TextWriter_Dispose_m1_2378 ();
extern "C" void TextWriter_Dispose_m1_2379 ();
extern "C" void TextWriter_Flush_m1_2380 ();
extern "C" void TextWriter_Synchronized_m1_2381 ();
extern "C" void TextWriter_Write_m1_2382 ();
extern "C" void TextWriter_Write_m1_2383 ();
extern "C" void TextWriter_Write_m1_2384 ();
extern "C" void TextWriter_Write_m1_2385 ();
extern "C" void TextWriter_Write_m1_2386 ();
extern "C" void TextWriter_WriteLine_m1_2387 ();
extern "C" void TextWriter_WriteLine_m1_2388 ();
extern "C" void TextWriter_WriteLine_m1_2389 ();
extern "C" void SynchronizedWriter__ctor_m1_2390 ();
extern "C" void SynchronizedWriter_Close_m1_2391 ();
extern "C" void SynchronizedWriter_Flush_m1_2392 ();
extern "C" void SynchronizedWriter_Write_m1_2393 ();
extern "C" void SynchronizedWriter_Write_m1_2394 ();
extern "C" void SynchronizedWriter_Write_m1_2395 ();
extern "C" void SynchronizedWriter_Write_m1_2396 ();
extern "C" void SynchronizedWriter_Write_m1_2397 ();
extern "C" void SynchronizedWriter_WriteLine_m1_2398 ();
extern "C" void SynchronizedWriter_WriteLine_m1_2399 ();
extern "C" void SynchronizedWriter_WriteLine_m1_2400 ();
extern "C" void UnexceptionalStreamReader__ctor_m1_2401 ();
extern "C" void UnexceptionalStreamReader__cctor_m1_2402 ();
extern "C" void UnexceptionalStreamReader_Peek_m1_2403 ();
extern "C" void UnexceptionalStreamReader_Read_m1_2404 ();
extern "C" void UnexceptionalStreamReader_Read_m1_2405 ();
extern "C" void UnexceptionalStreamReader_CheckEOL_m1_2406 ();
extern "C" void UnexceptionalStreamReader_ReadLine_m1_2407 ();
extern "C" void UnexceptionalStreamReader_ReadToEnd_m1_2408 ();
extern "C" void UnexceptionalStreamWriter__ctor_m1_2409 ();
extern "C" void UnexceptionalStreamWriter_Flush_m1_2410 ();
extern "C" void UnexceptionalStreamWriter_Write_m1_2411 ();
extern "C" void UnexceptionalStreamWriter_Write_m1_2412 ();
extern "C" void UnexceptionalStreamWriter_Write_m1_2413 ();
extern "C" void UnexceptionalStreamWriter_Write_m1_2414 ();
extern "C" void UnmanagedMemoryStream_get_CanRead_m1_2415 ();
extern "C" void UnmanagedMemoryStream_get_CanSeek_m1_2416 ();
extern "C" void UnmanagedMemoryStream_get_CanWrite_m1_2417 ();
extern "C" void UnmanagedMemoryStream_get_Length_m1_2418 ();
extern "C" void UnmanagedMemoryStream_get_Position_m1_2419 ();
extern "C" void UnmanagedMemoryStream_set_Position_m1_2420 ();
extern "C" void UnmanagedMemoryStream_Read_m1_2421 ();
extern "C" void UnmanagedMemoryStream_ReadByte_m1_2422 ();
extern "C" void UnmanagedMemoryStream_Seek_m1_2423 ();
extern "C" void UnmanagedMemoryStream_SetLength_m1_2424 ();
extern "C" void UnmanagedMemoryStream_Flush_m1_2425 ();
extern "C" void UnmanagedMemoryStream_Dispose_m1_2426 ();
extern "C" void UnmanagedMemoryStream_Write_m1_2427 ();
extern "C" void UnmanagedMemoryStream_WriteByte_m1_2428 ();
extern "C" void AssemblyBuilder_get_Location_m1_2429 ();
extern "C" void AssemblyBuilder_GetModulesInternal_m1_2430 ();
extern "C" void AssemblyBuilder_GetTypes_m1_2431 ();
extern "C" void AssemblyBuilder_get_IsCompilerContext_m1_2432 ();
extern "C" void AssemblyBuilder_not_supported_m1_2433 ();
extern "C" void AssemblyBuilder_UnprotectedGetName_m1_2434 ();
extern "C" void ConstructorBuilder__ctor_m1_2435 ();
extern "C" void ConstructorBuilder_get_CallingConvention_m1_2436 ();
extern "C" void ConstructorBuilder_get_TypeBuilder_m1_2437 ();
extern "C" void ConstructorBuilder_GetParameters_m1_2438 ();
extern "C" void ConstructorBuilder_GetParametersInternal_m1_2439 ();
extern "C" void ConstructorBuilder_GetParameterCount_m1_2440 ();
extern "C" void ConstructorBuilder_Invoke_m1_2441 ();
extern "C" void ConstructorBuilder_Invoke_m1_2442 ();
extern "C" void ConstructorBuilder_get_MethodHandle_m1_2443 ();
extern "C" void ConstructorBuilder_get_Attributes_m1_2444 ();
extern "C" void ConstructorBuilder_get_ReflectedType_m1_2445 ();
extern "C" void ConstructorBuilder_get_DeclaringType_m1_2446 ();
extern "C" void ConstructorBuilder_get_Name_m1_2447 ();
extern "C" void ConstructorBuilder_IsDefined_m1_2448 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m1_2449 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m1_2450 ();
extern "C" void ConstructorBuilder_GetILGenerator_m1_2451 ();
extern "C" void ConstructorBuilder_GetILGenerator_m1_2452 ();
extern "C" void ConstructorBuilder_GetToken_m1_2453 ();
extern "C" void ConstructorBuilder_get_Module_m1_2454 ();
extern "C" void ConstructorBuilder_ToString_m1_2455 ();
extern "C" void ConstructorBuilder_fixup_m1_2456 ();
extern "C" void ConstructorBuilder_get_next_table_index_m1_2457 ();
extern "C" void ConstructorBuilder_get_IsCompilerContext_m1_2458 ();
extern "C" void ConstructorBuilder_not_supported_m1_2459 ();
extern "C" void ConstructorBuilder_not_created_m1_2460 ();
extern "C" void EnumBuilder_get_Assembly_m1_2461 ();
extern "C" void EnumBuilder_get_AssemblyQualifiedName_m1_2462 ();
extern "C" void EnumBuilder_get_BaseType_m1_2463 ();
extern "C" void EnumBuilder_get_DeclaringType_m1_2464 ();
extern "C" void EnumBuilder_get_FullName_m1_2465 ();
extern "C" void EnumBuilder_get_Module_m1_2466 ();
extern "C" void EnumBuilder_get_Name_m1_2467 ();
extern "C" void EnumBuilder_get_Namespace_m1_2468 ();
extern "C" void EnumBuilder_get_ReflectedType_m1_2469 ();
extern "C" void EnumBuilder_get_TypeHandle_m1_2470 ();
extern "C" void EnumBuilder_get_UnderlyingSystemType_m1_2471 ();
extern "C" void EnumBuilder_GetAttributeFlagsImpl_m1_2472 ();
extern "C" void EnumBuilder_GetConstructorImpl_m1_2473 ();
extern "C" void EnumBuilder_GetConstructors_m1_2474 ();
extern "C" void EnumBuilder_GetCustomAttributes_m1_2475 ();
extern "C" void EnumBuilder_GetCustomAttributes_m1_2476 ();
extern "C" void EnumBuilder_GetElementType_m1_2477 ();
extern "C" void EnumBuilder_GetEvent_m1_2478 ();
extern "C" void EnumBuilder_GetField_m1_2479 ();
extern "C" void EnumBuilder_GetFields_m1_2480 ();
extern "C" void EnumBuilder_GetInterfaces_m1_2481 ();
extern "C" void EnumBuilder_GetMethodImpl_m1_2482 ();
extern "C" void EnumBuilder_GetMethods_m1_2483 ();
extern "C" void EnumBuilder_GetPropertyImpl_m1_2484 ();
extern "C" void EnumBuilder_HasElementTypeImpl_m1_2485 ();
extern "C" void EnumBuilder_InvokeMember_m1_2486 ();
extern "C" void EnumBuilder_IsArrayImpl_m1_2487 ();
extern "C" void EnumBuilder_IsByRefImpl_m1_2488 ();
extern "C" void EnumBuilder_IsPointerImpl_m1_2489 ();
extern "C" void EnumBuilder_IsPrimitiveImpl_m1_2490 ();
extern "C" void EnumBuilder_IsValueTypeImpl_m1_2491 ();
extern "C" void EnumBuilder_IsDefined_m1_2492 ();
extern "C" void EnumBuilder_CreateNotSupportedException_m1_2493 ();
extern "C" void FieldBuilder_get_Attributes_m1_2494 ();
extern "C" void FieldBuilder_get_DeclaringType_m1_2495 ();
extern "C" void FieldBuilder_get_FieldHandle_m1_2496 ();
extern "C" void FieldBuilder_get_FieldType_m1_2497 ();
extern "C" void FieldBuilder_get_Name_m1_2498 ();
extern "C" void FieldBuilder_get_ReflectedType_m1_2499 ();
extern "C" void FieldBuilder_GetCustomAttributes_m1_2500 ();
extern "C" void FieldBuilder_GetCustomAttributes_m1_2501 ();
extern "C" void FieldBuilder_GetValue_m1_2502 ();
extern "C" void FieldBuilder_IsDefined_m1_2503 ();
extern "C" void FieldBuilder_GetFieldOffset_m1_2504 ();
extern "C" void FieldBuilder_SetValue_m1_2505 ();
extern "C" void FieldBuilder_get_UMarshal_m1_2506 ();
extern "C" void FieldBuilder_CreateNotSupportedException_m1_2507 ();
extern "C" void FieldBuilder_get_Module_m1_2508 ();
extern "C" void GenericTypeParameterBuilder_IsSubclassOf_m1_2509 ();
extern "C" void GenericTypeParameterBuilder_GetAttributeFlagsImpl_m1_2510 ();
extern "C" void GenericTypeParameterBuilder_GetConstructorImpl_m1_2511 ();
extern "C" void GenericTypeParameterBuilder_GetConstructors_m1_2512 ();
extern "C" void GenericTypeParameterBuilder_GetEvent_m1_2513 ();
extern "C" void GenericTypeParameterBuilder_GetField_m1_2514 ();
extern "C" void GenericTypeParameterBuilder_GetFields_m1_2515 ();
extern "C" void GenericTypeParameterBuilder_GetInterfaces_m1_2516 ();
extern "C" void GenericTypeParameterBuilder_GetMethods_m1_2517 ();
extern "C" void GenericTypeParameterBuilder_GetMethodImpl_m1_2518 ();
extern "C" void GenericTypeParameterBuilder_GetPropertyImpl_m1_2519 ();
extern "C" void GenericTypeParameterBuilder_HasElementTypeImpl_m1_2520 ();
extern "C" void GenericTypeParameterBuilder_IsAssignableFrom_m1_2521 ();
extern "C" void GenericTypeParameterBuilder_IsInstanceOfType_m1_2522 ();
extern "C" void GenericTypeParameterBuilder_IsArrayImpl_m1_2523 ();
extern "C" void GenericTypeParameterBuilder_IsByRefImpl_m1_2524 ();
extern "C" void GenericTypeParameterBuilder_IsPointerImpl_m1_2525 ();
extern "C" void GenericTypeParameterBuilder_IsPrimitiveImpl_m1_2526 ();
extern "C" void GenericTypeParameterBuilder_IsValueTypeImpl_m1_2527 ();
extern "C" void GenericTypeParameterBuilder_InvokeMember_m1_2528 ();
extern "C" void GenericTypeParameterBuilder_GetElementType_m1_2529 ();
extern "C" void GenericTypeParameterBuilder_get_UnderlyingSystemType_m1_2530 ();
extern "C" void GenericTypeParameterBuilder_get_Assembly_m1_2531 ();
extern "C" void GenericTypeParameterBuilder_get_AssemblyQualifiedName_m1_2532 ();
extern "C" void GenericTypeParameterBuilder_get_BaseType_m1_2533 ();
extern "C" void GenericTypeParameterBuilder_get_FullName_m1_2534 ();
extern "C" void GenericTypeParameterBuilder_IsDefined_m1_2535 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m1_2536 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m1_2537 ();
extern "C" void GenericTypeParameterBuilder_get_Name_m1_2538 ();
extern "C" void GenericTypeParameterBuilder_get_Namespace_m1_2539 ();
extern "C" void GenericTypeParameterBuilder_get_Module_m1_2540 ();
extern "C" void GenericTypeParameterBuilder_get_DeclaringType_m1_2541 ();
extern "C" void GenericTypeParameterBuilder_get_ReflectedType_m1_2542 ();
extern "C" void GenericTypeParameterBuilder_get_TypeHandle_m1_2543 ();
extern "C" void GenericTypeParameterBuilder_GetGenericArguments_m1_2544 ();
extern "C" void GenericTypeParameterBuilder_GetGenericTypeDefinition_m1_2545 ();
extern "C" void GenericTypeParameterBuilder_get_ContainsGenericParameters_m1_2546 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericParameter_m1_2547 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericType_m1_2548 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m1_2549 ();
extern "C" void GenericTypeParameterBuilder_not_supported_m1_2550 ();
extern "C" void GenericTypeParameterBuilder_ToString_m1_2551 ();
extern "C" void GenericTypeParameterBuilder_Equals_m1_2552 ();
extern "C" void GenericTypeParameterBuilder_GetHashCode_m1_2553 ();
extern "C" void GenericTypeParameterBuilder_MakeGenericType_m1_2554 ();
extern "C" void ILGenerator__ctor_m1_2555 ();
extern "C" void ILGenerator__cctor_m1_2556 ();
extern "C" void ILGenerator_add_token_fixup_m1_2557 ();
extern "C" void ILGenerator_make_room_m1_2558 ();
extern "C" void ILGenerator_emit_int_m1_2559 ();
extern "C" void ILGenerator_ll_emit_m1_2560 ();
extern "C" void ILGenerator_Emit_m1_2561 ();
extern "C" void ILGenerator_Emit_m1_2562 ();
extern "C" void ILGenerator_label_fixup_m1_2563 ();
extern "C" void ILGenerator_Mono_GetCurrentOffset_m1_2564 ();
extern "C" void MethodBuilder_get_ContainsGenericParameters_m1_2565 ();
extern "C" void MethodBuilder_get_MethodHandle_m1_2566 ();
extern "C" void MethodBuilder_get_ReturnType_m1_2567 ();
extern "C" void MethodBuilder_get_ReflectedType_m1_2568 ();
extern "C" void MethodBuilder_get_DeclaringType_m1_2569 ();
extern "C" void MethodBuilder_get_Name_m1_2570 ();
extern "C" void MethodBuilder_get_Attributes_m1_2571 ();
extern "C" void MethodBuilder_get_CallingConvention_m1_2572 ();
extern "C" void MethodBuilder_GetBaseDefinition_m1_2573 ();
extern "C" void MethodBuilder_GetParameters_m1_2574 ();
extern "C" void MethodBuilder_GetParameterCount_m1_2575 ();
extern "C" void MethodBuilder_Invoke_m1_2576 ();
extern "C" void MethodBuilder_IsDefined_m1_2577 ();
extern "C" void MethodBuilder_GetCustomAttributes_m1_2578 ();
extern "C" void MethodBuilder_GetCustomAttributes_m1_2579 ();
extern "C" void MethodBuilder_check_override_m1_2580 ();
extern "C" void MethodBuilder_fixup_m1_2581 ();
extern "C" void MethodBuilder_ToString_m1_2582 ();
extern "C" void MethodBuilder_Equals_m1_2583 ();
extern "C" void MethodBuilder_GetHashCode_m1_2584 ();
extern "C" void MethodBuilder_get_next_table_index_m1_2585 ();
extern "C" void MethodBuilder_NotSupported_m1_2586 ();
extern "C" void MethodBuilder_MakeGenericMethod_m1_2587 ();
extern "C" void MethodBuilder_get_IsGenericMethodDefinition_m1_2588 ();
extern "C" void MethodBuilder_get_IsGenericMethod_m1_2589 ();
extern "C" void MethodBuilder_GetGenericArguments_m1_2590 ();
extern "C" void MethodBuilder_get_Module_m1_2591 ();
extern "C" void MethodToken__ctor_m1_2592 ();
extern "C" void MethodToken__cctor_m1_2593 ();
extern "C" void MethodToken_Equals_m1_2594 ();
extern "C" void MethodToken_GetHashCode_m1_2595 ();
extern "C" void MethodToken_get_Token_m1_2596 ();
extern "C" void ModuleBuilder__cctor_m1_2597 ();
extern "C" void ModuleBuilder_get_next_table_index_m1_2598 ();
extern "C" void ModuleBuilder_GetTypes_m1_2599 ();
extern "C" void ModuleBuilder_getToken_m1_2600 ();
extern "C" void ModuleBuilder_GetToken_m1_2601 ();
extern "C" void ModuleBuilder_RegisterToken_m1_2602 ();
extern "C" void ModuleBuilder_GetTokenGenerator_m1_2603 ();
extern "C" void ModuleBuilderTokenGenerator__ctor_m1_2604 ();
extern "C" void ModuleBuilderTokenGenerator_GetToken_m1_2605 ();
extern "C" void OpCode__ctor_m1_2606 ();
extern "C" void OpCode_GetHashCode_m1_2607 ();
extern "C" void OpCode_Equals_m1_2608 ();
extern "C" void OpCode_ToString_m1_2609 ();
extern "C" void OpCode_get_Name_m1_2610 ();
extern "C" void OpCode_get_Size_m1_2611 ();
extern "C" void OpCode_get_StackBehaviourPop_m1_2612 ();
extern "C" void OpCode_get_StackBehaviourPush_m1_2613 ();
extern "C" void OpCodeNames__cctor_m1_2614 ();
extern "C" void OpCodes__cctor_m1_2615 ();
extern "C" void ParameterBuilder_get_Attributes_m1_2616 ();
extern "C" void ParameterBuilder_get_Name_m1_2617 ();
extern "C" void ParameterBuilder_get_Position_m1_2618 ();
extern "C" void TypeBuilder_GetAttributeFlagsImpl_m1_2619 ();
extern "C" void TypeBuilder_setup_internal_class_m1_2620 ();
extern "C" void TypeBuilder_create_generic_class_m1_2621 ();
extern "C" void TypeBuilder_get_Assembly_m1_2622 ();
extern "C" void TypeBuilder_get_AssemblyQualifiedName_m1_2623 ();
extern "C" void TypeBuilder_get_BaseType_m1_2624 ();
extern "C" void TypeBuilder_get_DeclaringType_m1_2625 ();
extern "C" void TypeBuilder_get_UnderlyingSystemType_m1_2626 ();
extern "C" void TypeBuilder_get_FullName_m1_2627 ();
extern "C" void TypeBuilder_get_Module_m1_2628 ();
extern "C" void TypeBuilder_get_Name_m1_2629 ();
extern "C" void TypeBuilder_get_Namespace_m1_2630 ();
extern "C" void TypeBuilder_get_ReflectedType_m1_2631 ();
extern "C" void TypeBuilder_GetConstructorImpl_m1_2632 ();
extern "C" void TypeBuilder_IsDefined_m1_2633 ();
extern "C" void TypeBuilder_GetCustomAttributes_m1_2634 ();
extern "C" void TypeBuilder_GetCustomAttributes_m1_2635 ();
extern "C" void TypeBuilder_DefineConstructor_m1_2636 ();
extern "C" void TypeBuilder_DefineConstructor_m1_2637 ();
extern "C" void TypeBuilder_DefineDefaultConstructor_m1_2638 ();
extern "C" void TypeBuilder_create_runtime_class_m1_2639 ();
extern "C" void TypeBuilder_is_nested_in_m1_2640 ();
extern "C" void TypeBuilder_has_ctor_method_m1_2641 ();
extern "C" void TypeBuilder_CreateType_m1_2642 ();
extern "C" void TypeBuilder_GetConstructors_m1_2643 ();
extern "C" void TypeBuilder_GetConstructorsInternal_m1_2644 ();
extern "C" void TypeBuilder_GetElementType_m1_2645 ();
extern "C" void TypeBuilder_GetEvent_m1_2646 ();
extern "C" void TypeBuilder_GetField_m1_2647 ();
extern "C" void TypeBuilder_GetFields_m1_2648 ();
extern "C" void TypeBuilder_GetInterfaces_m1_2649 ();
extern "C" void TypeBuilder_GetMethodsByName_m1_2650 ();
extern "C" void TypeBuilder_GetMethods_m1_2651 ();
extern "C" void TypeBuilder_GetMethodImpl_m1_2652 ();
extern "C" void TypeBuilder_GetPropertyImpl_m1_2653 ();
extern "C" void TypeBuilder_HasElementTypeImpl_m1_2654 ();
extern "C" void TypeBuilder_InvokeMember_m1_2655 ();
extern "C" void TypeBuilder_IsArrayImpl_m1_2656 ();
extern "C" void TypeBuilder_IsByRefImpl_m1_2657 ();
extern "C" void TypeBuilder_IsPointerImpl_m1_2658 ();
extern "C" void TypeBuilder_IsPrimitiveImpl_m1_2659 ();
extern "C" void TypeBuilder_IsValueTypeImpl_m1_2660 ();
extern "C" void TypeBuilder_MakeGenericType_m1_2661 ();
extern "C" void TypeBuilder_get_TypeHandle_m1_2662 ();
extern "C" void TypeBuilder_SetParent_m1_2663 ();
extern "C" void TypeBuilder_get_next_table_index_m1_2664 ();
extern "C" void TypeBuilder_get_IsCompilerContext_m1_2665 ();
extern "C" void TypeBuilder_get_is_created_m1_2666 ();
extern "C" void TypeBuilder_not_supported_m1_2667 ();
extern "C" void TypeBuilder_check_not_created_m1_2668 ();
extern "C" void TypeBuilder_check_created_m1_2669 ();
extern "C" void TypeBuilder_ToString_m1_2670 ();
extern "C" void TypeBuilder_IsAssignableFrom_m1_2671 ();
extern "C" void TypeBuilder_IsSubclassOf_m1_2672 ();
extern "C" void TypeBuilder_IsAssignableTo_m1_2673 ();
extern "C" void TypeBuilder_GetGenericArguments_m1_2674 ();
extern "C" void TypeBuilder_GetGenericTypeDefinition_m1_2675 ();
extern "C" void TypeBuilder_get_ContainsGenericParameters_m1_2676 ();
extern "C" void TypeBuilder_get_IsGenericParameter_m1_2677 ();
extern "C" void TypeBuilder_get_IsGenericTypeDefinition_m1_2678 ();
extern "C" void TypeBuilder_get_IsGenericType_m1_2679 ();
extern "C" void UnmanagedMarshal_ToMarshalAsAttribute_m1_2680 ();
extern "C" void AmbiguousMatchException__ctor_m1_2681 ();
extern "C" void AmbiguousMatchException__ctor_m1_2682 ();
extern "C" void AmbiguousMatchException__ctor_m1_2683 ();
extern "C" void ResolveEventHolder__ctor_m1_2684 ();
extern "C" void Assembly__ctor_m1_2685 ();
extern "C" void Assembly_get_code_base_m1_2686 ();
extern "C" void Assembly_get_fullname_m1_2687 ();
extern "C" void Assembly_get_location_m1_2688 ();
extern "C" void Assembly_GetCodeBase_m1_2689 ();
extern "C" void Assembly_get_FullName_m1_2690 ();
extern "C" void Assembly_get_Location_m1_2691 ();
extern "C" void Assembly_IsDefined_m1_2692 ();
extern "C" void Assembly_GetCustomAttributes_m1_2693 ();
extern "C" void Assembly_GetManifestResourceInternal_m1_2694 ();
extern "C" void Assembly_GetTypes_m1_2695 ();
extern "C" void Assembly_GetTypes_m1_2696 ();
extern "C" void Assembly_GetType_m1_2697 ();
extern "C" void Assembly_GetType_m1_2698 ();
extern "C" void Assembly_InternalGetType_m1_2699 ();
extern "C" void Assembly_GetType_m1_2700 ();
extern "C" void Assembly_FillName_m1_2701 ();
extern "C" void Assembly_GetName_m1_2702 ();
extern "C" void Assembly_GetName_m1_2703 ();
extern "C" void Assembly_UnprotectedGetName_m1_2704 ();
extern "C" void Assembly_ToString_m1_2705 ();
extern "C" void Assembly_Load_m1_2706 ();
extern "C" void Assembly_GetModule_m1_2707 ();
extern "C" void Assembly_GetModulesInternal_m1_2708 ();
extern "C" void Assembly_GetModules_m1_2709 ();
extern "C" void Assembly_GetExecutingAssembly_m1_2710 ();
extern "C" void AssemblyCompanyAttribute__ctor_m1_2711 ();
extern "C" void AssemblyConfigurationAttribute__ctor_m1_2712 ();
extern "C" void AssemblyCopyrightAttribute__ctor_m1_2713 ();
extern "C" void AssemblyDefaultAliasAttribute__ctor_m1_2714 ();
extern "C" void AssemblyDelaySignAttribute__ctor_m1_2715 ();
extern "C" void AssemblyDescriptionAttribute__ctor_m1_2716 ();
extern "C" void AssemblyFileVersionAttribute__ctor_m1_2717 ();
extern "C" void AssemblyInformationalVersionAttribute__ctor_m1_2718 ();
extern "C" void AssemblyKeyFileAttribute__ctor_m1_2719 ();
extern "C" void AssemblyName__ctor_m1_2720 ();
extern "C" void AssemblyName__ctor_m1_2721 ();
extern "C" void AssemblyName_get_Name_m1_2722 ();
extern "C" void AssemblyName_get_Flags_m1_2723 ();
extern "C" void AssemblyName_get_FullName_m1_2724 ();
extern "C" void AssemblyName_get_Version_m1_2725 ();
extern "C" void AssemblyName_set_Version_m1_2726 ();
extern "C" void AssemblyName_ToString_m1_2727 ();
extern "C" void AssemblyName_get_IsPublicKeyValid_m1_2728 ();
extern "C" void AssemblyName_InternalGetPublicKeyToken_m1_2729 ();
extern "C" void AssemblyName_ComputePublicKeyToken_m1_2730 ();
extern "C" void AssemblyName_SetPublicKey_m1_2731 ();
extern "C" void AssemblyName_SetPublicKeyToken_m1_2732 ();
extern "C" void AssemblyName_GetObjectData_m1_2733 ();
extern "C" void AssemblyName_Clone_m1_2734 ();
extern "C" void AssemblyName_OnDeserialization_m1_2735 ();
extern "C" void AssemblyProductAttribute__ctor_m1_2736 ();
extern "C" void AssemblyTitleAttribute__ctor_m1_2737 ();
extern "C" void AssemblyTrademarkAttribute__ctor_m1_2738 ();
extern "C" void Default__ctor_m1_2739 ();
extern "C" void Default_BindToMethod_m1_2740 ();
extern "C" void Default_ReorderParameters_m1_2741 ();
extern "C" void Default_IsArrayAssignable_m1_2742 ();
extern "C" void Default_ChangeType_m1_2743 ();
extern "C" void Default_ReorderArgumentArray_m1_2744 ();
extern "C" void Default_check_type_m1_2745 ();
extern "C" void Default_check_arguments_m1_2746 ();
extern "C" void Default_SelectMethod_m1_2747 ();
extern "C" void Default_SelectMethod_m1_2748 ();
extern "C" void Default_GetBetterMethod_m1_2749 ();
extern "C" void Default_CompareCloserType_m1_2750 ();
extern "C" void Default_SelectProperty_m1_2751 ();
extern "C" void Default_check_arguments_with_score_m1_2752 ();
extern "C" void Default_check_type_with_score_m1_2753 ();
extern "C" void Binder__ctor_m1_2754 ();
extern "C" void Binder__cctor_m1_2755 ();
extern "C" void Binder_get_DefaultBinder_m1_2756 ();
extern "C" void Binder_ConvertArgs_m1_2757 ();
extern "C" void Binder_GetDerivedLevel_m1_2758 ();
extern "C" void Binder_FindMostDerivedMatch_m1_2759 ();
extern "C" void ConstructorInfo__ctor_m1_2760 ();
extern "C" void ConstructorInfo__cctor_m1_2761 ();
extern "C" void ConstructorInfo_get_MemberType_m1_2762 ();
extern "C" void ConstructorInfo_Invoke_m1_2763 ();
extern "C" void CustomAttributeData__ctor_m1_2764 ();
extern "C" void CustomAttributeData_get_Constructor_m1_2765 ();
extern "C" void CustomAttributeData_get_ConstructorArguments_m1_2766 ();
extern "C" void CustomAttributeData_get_NamedArguments_m1_2767 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m1_2768 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m1_2769 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m1_2770 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m1_2771 ();
extern "C" void CustomAttributeData_ToString_m1_2772 ();
extern "C" void CustomAttributeData_Equals_m1_2773 ();
extern "C" void CustomAttributeData_GetHashCode_m1_2774 ();
extern "C" void CustomAttributeNamedArgument_ToString_m1_2775 ();
extern "C" void CustomAttributeNamedArgument_Equals_m1_2776 ();
extern "C" void CustomAttributeNamedArgument_GetHashCode_m1_2777 ();
extern "C" void CustomAttributeTypedArgument_ToString_m1_2778 ();
extern "C" void CustomAttributeTypedArgument_Equals_m1_2779 ();
extern "C" void CustomAttributeTypedArgument_GetHashCode_m1_2780 ();
extern "C" void AddEventAdapter__ctor_m1_2781 ();
extern "C" void AddEventAdapter_Invoke_m1_2782 ();
extern "C" void AddEventAdapter_BeginInvoke_m1_2783 ();
extern "C" void AddEventAdapter_EndInvoke_m1_2784 ();
extern "C" void EventInfo__ctor_m1_2785 ();
extern "C" void EventInfo_get_EventHandlerType_m1_2786 ();
extern "C" void EventInfo_get_MemberType_m1_2787 ();
extern "C" void FieldInfo__ctor_m1_2788 ();
extern "C" void FieldInfo_get_MemberType_m1_2789 ();
extern "C" void FieldInfo_get_IsLiteral_m1_2790 ();
extern "C" void FieldInfo_get_IsStatic_m1_2791 ();
extern "C" void FieldInfo_get_IsNotSerialized_m1_2792 ();
extern "C" void FieldInfo_SetValue_m1_2793 ();
extern "C" void FieldInfo_internal_from_handle_type_m1_2794 ();
extern "C" void FieldInfo_GetFieldFromHandle_m1_2795 ();
extern "C" void FieldInfo_GetFieldOffset_m1_2796 ();
extern "C" void FieldInfo_GetUnmanagedMarshal_m1_2797 ();
extern "C" void FieldInfo_get_UMarshal_m1_2798 ();
extern "C" void FieldInfo_GetPseudoCustomAttributes_m1_2799 ();
extern "C" void MemberInfoSerializationHolder__ctor_m1_2800 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m1_2801 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m1_2802 ();
extern "C" void MemberInfoSerializationHolder_GetObjectData_m1_2803 ();
extern "C" void MemberInfoSerializationHolder_GetRealObject_m1_2804 ();
extern "C" void MethodBase__ctor_m1_2805 ();
extern "C" void MethodBase_GetMethodFromHandleNoGenericCheck_m1_2806 ();
extern "C" void MethodBase_GetMethodFromIntPtr_m1_2807 ();
extern "C" void MethodBase_GetMethodFromHandle_m1_2808 ();
extern "C" void MethodBase_GetMethodFromHandleInternalType_m1_2809 ();
extern "C" void MethodBase_GetParameterCount_m1_2810 ();
extern "C" void MethodBase_Invoke_m1_2811 ();
extern "C" void MethodBase_get_CallingConvention_m1_2812 ();
extern "C" void MethodBase_get_IsPublic_m1_2813 ();
extern "C" void MethodBase_get_IsStatic_m1_2814 ();
extern "C" void MethodBase_get_IsVirtual_m1_2815 ();
extern "C" void MethodBase_get_IsAbstract_m1_2816 ();
extern "C" void MethodBase_get_next_table_index_m1_2817 ();
extern "C" void MethodBase_GetGenericArguments_m1_2818 ();
extern "C" void MethodBase_get_ContainsGenericParameters_m1_2819 ();
extern "C" void MethodBase_get_IsGenericMethodDefinition_m1_2820 ();
extern "C" void MethodBase_get_IsGenericMethod_m1_2821 ();
extern "C" void MethodInfo__ctor_m1_2822 ();
extern "C" void MethodInfo_get_MemberType_m1_2823 ();
extern "C" void MethodInfo_get_ReturnType_m1_2824 ();
extern "C" void MethodInfo_MakeGenericMethod_m1_2825 ();
extern "C" void MethodInfo_GetGenericArguments_m1_2826 ();
extern "C" void MethodInfo_get_IsGenericMethod_m1_2827 ();
extern "C" void MethodInfo_get_IsGenericMethodDefinition_m1_2828 ();
extern "C" void MethodInfo_get_ContainsGenericParameters_m1_2829 ();
extern "C" void Missing__ctor_m1_2830 ();
extern "C" void Missing__cctor_m1_2831 ();
extern "C" void Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m1_2832 ();
extern "C" void Module__ctor_m1_2833 ();
extern "C" void Module__cctor_m1_2834 ();
extern "C" void Module_get_Assembly_m1_2835 ();
extern "C" void Module_get_ScopeName_m1_2836 ();
extern "C" void Module_GetCustomAttributes_m1_2837 ();
extern "C" void Module_GetObjectData_m1_2838 ();
extern "C" void Module_InternalGetTypes_m1_2839 ();
extern "C" void Module_GetTypes_m1_2840 ();
extern "C" void Module_IsDefined_m1_2841 ();
extern "C" void Module_IsResource_m1_2842 ();
extern "C" void Module_ToString_m1_2843 ();
extern "C" void Module_filter_by_type_name_m1_2844 ();
extern "C" void Module_filter_by_type_name_ignore_case_m1_2845 ();
extern "C" void MonoEventInfo_get_event_info_m1_2846 ();
extern "C" void MonoEventInfo_GetEventInfo_m1_2847 ();
extern "C" void MonoEvent__ctor_m1_2848 ();
extern "C" void MonoEvent_get_Attributes_m1_2849 ();
extern "C" void MonoEvent_GetAddMethod_m1_2850 ();
extern "C" void MonoEvent_get_DeclaringType_m1_2851 ();
extern "C" void MonoEvent_get_ReflectedType_m1_2852 ();
extern "C" void MonoEvent_get_Name_m1_2853 ();
extern "C" void MonoEvent_ToString_m1_2854 ();
extern "C" void MonoEvent_IsDefined_m1_2855 ();
extern "C" void MonoEvent_GetCustomAttributes_m1_2856 ();
extern "C" void MonoEvent_GetCustomAttributes_m1_2857 ();
extern "C" void MonoEvent_GetObjectData_m1_2858 ();
extern "C" void MonoField__ctor_m1_2859 ();
extern "C" void MonoField_get_Attributes_m1_2860 ();
extern "C" void MonoField_get_FieldHandle_m1_2861 ();
extern "C" void MonoField_get_FieldType_m1_2862 ();
extern "C" void MonoField_GetParentType_m1_2863 ();
extern "C" void MonoField_get_ReflectedType_m1_2864 ();
extern "C" void MonoField_get_DeclaringType_m1_2865 ();
extern "C" void MonoField_get_Name_m1_2866 ();
extern "C" void MonoField_IsDefined_m1_2867 ();
extern "C" void MonoField_GetCustomAttributes_m1_2868 ();
extern "C" void MonoField_GetCustomAttributes_m1_2869 ();
extern "C" void MonoField_GetFieldOffset_m1_2870 ();
extern "C" void MonoField_GetValueInternal_m1_2871 ();
extern "C" void MonoField_GetValue_m1_2872 ();
extern "C" void MonoField_ToString_m1_2873 ();
extern "C" void MonoField_SetValueInternal_m1_2874 ();
extern "C" void MonoField_SetValue_m1_2875 ();
extern "C" void MonoField_GetObjectData_m1_2876 ();
extern "C" void MonoField_CheckGeneric_m1_2877 ();
extern "C" void MonoGenericMethod__ctor_m1_2878 ();
extern "C" void MonoGenericMethod_get_ReflectedType_m1_2879 ();
extern "C" void MonoGenericCMethod__ctor_m1_2880 ();
extern "C" void MonoGenericCMethod_get_ReflectedType_m1_2881 ();
extern "C" void MonoMethodInfo_get_method_info_m1_2882 ();
extern "C" void MonoMethodInfo_GetMethodInfo_m1_2883 ();
extern "C" void MonoMethodInfo_GetDeclaringType_m1_2884 ();
extern "C" void MonoMethodInfo_GetReturnType_m1_2885 ();
extern "C" void MonoMethodInfo_GetAttributes_m1_2886 ();
extern "C" void MonoMethodInfo_GetCallingConvention_m1_2887 ();
extern "C" void MonoMethodInfo_get_parameter_info_m1_2888 ();
extern "C" void MonoMethodInfo_GetParametersInfo_m1_2889 ();
extern "C" void MonoMethod__ctor_m1_2890 ();
extern "C" void MonoMethod_get_name_m1_2891 ();
extern "C" void MonoMethod_get_base_definition_m1_2892 ();
extern "C" void MonoMethod_GetBaseDefinition_m1_2893 ();
extern "C" void MonoMethod_get_ReturnType_m1_2894 ();
extern "C" void MonoMethod_GetParameters_m1_2895 ();
extern "C" void MonoMethod_InternalInvoke_m1_2896 ();
extern "C" void MonoMethod_Invoke_m1_2897 ();
extern "C" void MonoMethod_get_MethodHandle_m1_2898 ();
extern "C" void MonoMethod_get_Attributes_m1_2899 ();
extern "C" void MonoMethod_get_CallingConvention_m1_2900 ();
extern "C" void MonoMethod_get_ReflectedType_m1_2901 ();
extern "C" void MonoMethod_get_DeclaringType_m1_2902 ();
extern "C" void MonoMethod_get_Name_m1_2903 ();
extern "C" void MonoMethod_IsDefined_m1_2904 ();
extern "C" void MonoMethod_GetCustomAttributes_m1_2905 ();
extern "C" void MonoMethod_GetCustomAttributes_m1_2906 ();
extern "C" void MonoMethod_GetDllImportAttribute_m1_2907 ();
extern "C" void MonoMethod_GetPseudoCustomAttributes_m1_2908 ();
extern "C" void MonoMethod_ShouldPrintFullName_m1_2909 ();
extern "C" void MonoMethod_ToString_m1_2910 ();
extern "C" void MonoMethod_GetObjectData_m1_2911 ();
extern "C" void MonoMethod_MakeGenericMethod_m1_2912 ();
extern "C" void MonoMethod_MakeGenericMethod_impl_m1_2913 ();
extern "C" void MonoMethod_GetGenericArguments_m1_2914 ();
extern "C" void MonoMethod_get_IsGenericMethodDefinition_m1_2915 ();
extern "C" void MonoMethod_get_IsGenericMethod_m1_2916 ();
extern "C" void MonoMethod_get_ContainsGenericParameters_m1_2917 ();
extern "C" void MonoCMethod__ctor_m1_2918 ();
extern "C" void MonoCMethod_GetParameters_m1_2919 ();
extern "C" void MonoCMethod_InternalInvoke_m1_2920 ();
extern "C" void MonoCMethod_Invoke_m1_2921 ();
extern "C" void MonoCMethod_Invoke_m1_2922 ();
extern "C" void MonoCMethod_get_MethodHandle_m1_2923 ();
extern "C" void MonoCMethod_get_Attributes_m1_2924 ();
extern "C" void MonoCMethod_get_CallingConvention_m1_2925 ();
extern "C" void MonoCMethod_get_ReflectedType_m1_2926 ();
extern "C" void MonoCMethod_get_DeclaringType_m1_2927 ();
extern "C" void MonoCMethod_get_Name_m1_2928 ();
extern "C" void MonoCMethod_IsDefined_m1_2929 ();
extern "C" void MonoCMethod_GetCustomAttributes_m1_2930 ();
extern "C" void MonoCMethod_GetCustomAttributes_m1_2931 ();
extern "C" void MonoCMethod_ToString_m1_2932 ();
extern "C" void MonoCMethod_GetObjectData_m1_2933 ();
extern "C" void MonoPropertyInfo_get_property_info_m1_2934 ();
extern "C" void MonoPropertyInfo_GetTypeModifiers_m1_2935 ();
extern "C" void GetterAdapter__ctor_m1_2936 ();
extern "C" void GetterAdapter_Invoke_m1_2937 ();
extern "C" void GetterAdapter_BeginInvoke_m1_2938 ();
extern "C" void GetterAdapter_EndInvoke_m1_2939 ();
extern "C" void MonoProperty__ctor_m1_2940 ();
extern "C" void MonoProperty_CachePropertyInfo_m1_2941 ();
extern "C" void MonoProperty_get_Attributes_m1_2942 ();
extern "C" void MonoProperty_get_CanRead_m1_2943 ();
extern "C" void MonoProperty_get_CanWrite_m1_2944 ();
extern "C" void MonoProperty_get_PropertyType_m1_2945 ();
extern "C" void MonoProperty_get_ReflectedType_m1_2946 ();
extern "C" void MonoProperty_get_DeclaringType_m1_2947 ();
extern "C" void MonoProperty_get_Name_m1_2948 ();
extern "C" void MonoProperty_GetAccessors_m1_2949 ();
extern "C" void MonoProperty_GetGetMethod_m1_2950 ();
extern "C" void MonoProperty_GetIndexParameters_m1_2951 ();
extern "C" void MonoProperty_GetSetMethod_m1_2952 ();
extern "C" void MonoProperty_IsDefined_m1_2953 ();
extern "C" void MonoProperty_GetCustomAttributes_m1_2954 ();
extern "C" void MonoProperty_GetCustomAttributes_m1_2955 ();
extern "C" void MonoProperty_CreateGetterDelegate_m1_2956 ();
extern "C" void MonoProperty_GetValue_m1_2957 ();
extern "C" void MonoProperty_GetValue_m1_2958 ();
extern "C" void MonoProperty_SetValue_m1_2959 ();
extern "C" void MonoProperty_ToString_m1_2960 ();
extern "C" void MonoProperty_GetOptionalCustomModifiers_m1_2961 ();
extern "C" void MonoProperty_GetRequiredCustomModifiers_m1_2962 ();
extern "C" void MonoProperty_GetObjectData_m1_2963 ();
extern "C" void ParameterInfo__ctor_m1_2964 ();
extern "C" void ParameterInfo__ctor_m1_2965 ();
extern "C" void ParameterInfo__ctor_m1_2966 ();
extern "C" void ParameterInfo_ToString_m1_2967 ();
extern "C" void ParameterInfo_get_ParameterType_m1_2968 ();
extern "C" void ParameterInfo_get_Attributes_m1_2969 ();
extern "C" void ParameterInfo_get_IsIn_m1_2970 ();
extern "C" void ParameterInfo_get_IsOptional_m1_2971 ();
extern "C" void ParameterInfo_get_IsOut_m1_2972 ();
extern "C" void ParameterInfo_get_IsRetval_m1_2973 ();
extern "C" void ParameterInfo_get_Member_m1_2974 ();
extern "C" void ParameterInfo_get_Name_m1_2975 ();
extern "C" void ParameterInfo_get_Position_m1_2976 ();
extern "C" void ParameterInfo_GetCustomAttributes_m1_2977 ();
extern "C" void ParameterInfo_IsDefined_m1_2978 ();
extern "C" void ParameterInfo_GetPseudoCustomAttributes_m1_2979 ();
extern "C" void Pointer__ctor_m1_2980 ();
extern "C" void Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m1_2981 ();
extern "C" void PropertyInfo__ctor_m1_2982 ();
extern "C" void PropertyInfo_get_MemberType_m1_2983 ();
extern "C" void PropertyInfo_GetValue_m1_2984 ();
extern "C" void PropertyInfo_SetValue_m1_2985 ();
extern "C" void PropertyInfo_GetOptionalCustomModifiers_m1_2986 ();
extern "C" void PropertyInfo_GetRequiredCustomModifiers_m1_2987 ();
extern "C" void StrongNameKeyPair__ctor_m1_2988 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m1_2989 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_2990 ();
extern "C" void TargetException__ctor_m1_2991 ();
extern "C" void TargetException__ctor_m1_2992 ();
extern "C" void TargetException__ctor_m1_2993 ();
extern "C" void TargetInvocationException__ctor_m1_2994 ();
extern "C" void TargetInvocationException__ctor_m1_2995 ();
extern "C" void TargetParameterCountException__ctor_m1_2996 ();
extern "C" void TargetParameterCountException__ctor_m1_2997 ();
extern "C" void TargetParameterCountException__ctor_m1_2998 ();
extern "C" void NeutralResourcesLanguageAttribute__ctor_m1_2999 ();
extern "C" void ResourceManager__ctor_m1_3000 ();
extern "C" void ResourceManager__cctor_m1_3001 ();
extern "C" void ResourceInfo__ctor_m1_3002 ();
extern "C" void ResourceCacheItem__ctor_m1_3003 ();
extern "C" void ResourceEnumerator__ctor_m1_3004 ();
extern "C" void ResourceEnumerator_get_Entry_m1_3005 ();
extern "C" void ResourceEnumerator_get_Key_m1_3006 ();
extern "C" void ResourceEnumerator_get_Value_m1_3007 ();
extern "C" void ResourceEnumerator_get_Current_m1_3008 ();
extern "C" void ResourceEnumerator_MoveNext_m1_3009 ();
extern "C" void ResourceEnumerator_Reset_m1_3010 ();
extern "C" void ResourceEnumerator_FillCache_m1_3011 ();
extern "C" void ResourceReader__ctor_m1_3012 ();
extern "C" void ResourceReader__ctor_m1_3013 ();
extern "C" void ResourceReader_System_Collections_IEnumerable_GetEnumerator_m1_3014 ();
extern "C" void ResourceReader_System_IDisposable_Dispose_m1_3015 ();
extern "C" void ResourceReader_ReadHeaders_m1_3016 ();
extern "C" void ResourceReader_CreateResourceInfo_m1_3017 ();
extern "C" void ResourceReader_Read7BitEncodedInt_m1_3018 ();
extern "C" void ResourceReader_ReadValueVer2_m1_3019 ();
extern "C" void ResourceReader_ReadValueVer1_m1_3020 ();
extern "C" void ResourceReader_ReadNonPredefinedValue_m1_3021 ();
extern "C" void ResourceReader_LoadResourceValues_m1_3022 ();
extern "C" void ResourceReader_Close_m1_3023 ();
extern "C" void ResourceReader_GetEnumerator_m1_3024 ();
extern "C" void ResourceReader_Dispose_m1_3025 ();
extern "C" void ResourceSet__ctor_m1_3026 ();
extern "C" void ResourceSet__ctor_m1_3027 ();
extern "C" void ResourceSet__ctor_m1_3028 ();
extern "C" void ResourceSet__ctor_m1_3029 ();
extern "C" void ResourceSet_System_Collections_IEnumerable_GetEnumerator_m1_3030 ();
extern "C" void ResourceSet_Dispose_m1_3031 ();
extern "C" void ResourceSet_Dispose_m1_3032 ();
extern "C" void ResourceSet_GetEnumerator_m1_3033 ();
extern "C" void ResourceSet_GetObjectInternal_m1_3034 ();
extern "C" void ResourceSet_GetObject_m1_3035 ();
extern "C" void ResourceSet_GetObject_m1_3036 ();
extern "C" void ResourceSet_ReadResources_m1_3037 ();
extern "C" void RuntimeResourceSet__ctor_m1_3038 ();
extern "C" void RuntimeResourceSet__ctor_m1_3039 ();
extern "C" void RuntimeResourceSet__ctor_m1_3040 ();
extern "C" void RuntimeResourceSet_GetObject_m1_3041 ();
extern "C" void RuntimeResourceSet_GetObject_m1_3042 ();
extern "C" void RuntimeResourceSet_CloneDisposableObjectIfPossible_m1_3043 ();
extern "C" void SatelliteContractVersionAttribute__ctor_m1_3044 ();
extern "C" void CompilationRelaxationsAttribute__ctor_m1_3045 ();
extern "C" void CompilationRelaxationsAttribute__ctor_m1_3046 ();
extern "C" void DefaultDependencyAttribute__ctor_m1_3047 ();
extern "C" void StringFreezingAttribute__ctor_m1_3048 ();
extern "C" void CriticalFinalizerObject__ctor_m1_3049 ();
extern "C" void CriticalFinalizerObject_Finalize_m1_3050 ();
extern "C" void ReliabilityContractAttribute__ctor_m1_3051 ();
extern "C" void ClassInterfaceAttribute__ctor_m1_3052 ();
extern "C" void ComDefaultInterfaceAttribute__ctor_m1_3053 ();
extern "C" void DispIdAttribute__ctor_m1_3054 ();
extern "C" void ExternalException__ctor_m1_3055 ();
extern "C" void ExternalException__ctor_m1_3056 ();
extern "C" void ExternalException__ctor_m1_3057 ();
extern "C" void GCHandle__ctor_m1_3058 ();
extern "C" void GCHandle_get_IsAllocated_m1_3059 ();
extern "C" void GCHandle_get_Target_m1_3060 ();
extern "C" void GCHandle_Alloc_m1_3061 ();
extern "C" void GCHandle_Free_m1_3062 ();
extern "C" void GCHandle_GetTarget_m1_3063 ();
extern "C" void GCHandle_GetTargetHandle_m1_3064 ();
extern "C" void GCHandle_FreeHandle_m1_3065 ();
extern "C" void GCHandle_Equals_m1_3066 ();
extern "C" void GCHandle_GetHashCode_m1_3067 ();
extern "C" void InterfaceTypeAttribute__ctor_m1_3068 ();
extern "C" void Marshal__cctor_m1_3069 ();
extern "C" void Marshal_copy_from_unmanaged_m1_3070 ();
extern "C" void Marshal_Copy_m1_3071 ();
extern "C" void Marshal_Copy_m1_3072 ();
extern "C" void Marshal_GetLastWin32Error_m1_3073 ();
extern "C" void Marshal_ReadByte_m1_3074 ();
extern "C" void Marshal_WriteByte_m1_3075 ();
extern "C" void MarshalDirectiveException__ctor_m1_3076 ();
extern "C" void MarshalDirectiveException__ctor_m1_3077 ();
extern "C" void PreserveSigAttribute__ctor_m1_3078 ();
extern "C" void SafeHandle__ctor_m1_3079 ();
extern "C" void SafeHandle_Close_m1_3080 ();
extern "C" void SafeHandle_DangerousAddRef_m1_3081 ();
extern "C" void SafeHandle_DangerousGetHandle_m1_3082 ();
extern "C" void SafeHandle_DangerousRelease_m1_3083 ();
extern "C" void SafeHandle_Dispose_m1_3084 ();
extern "C" void SafeHandle_Dispose_m1_3085 ();
extern "C" void SafeHandle_SetHandle_m1_3086 ();
extern "C" void SafeHandle_Finalize_m1_3087 ();
extern "C" void TypeLibImportClassAttribute__ctor_m1_3088 ();
extern "C" void TypeLibVersionAttribute__ctor_m1_3089 ();
extern "C" void ActivationServices_get_ConstructionActivator_m1_3090 ();
extern "C" void ActivationServices_CreateProxyFromAttributes_m1_3091 ();
extern "C" void ActivationServices_CreateConstructionCall_m1_3092 ();
extern "C" void ActivationServices_AllocateUninitializedClassInstance_m1_3093 ();
extern "C" void ActivationServices_EnableProxyActivation_m1_3094 ();
extern "C" void AppDomainLevelActivator__ctor_m1_3095 ();
extern "C" void ConstructionLevelActivator__ctor_m1_3096 ();
extern "C" void ContextLevelActivator__ctor_m1_3097 ();
extern "C" void UrlAttribute_get_UrlValue_m1_3098 ();
extern "C" void UrlAttribute_Equals_m1_3099 ();
extern "C" void UrlAttribute_GetHashCode_m1_3100 ();
extern "C" void UrlAttribute_GetPropertiesForNewContext_m1_3101 ();
extern "C" void UrlAttribute_IsContextOK_m1_3102 ();
extern "C" void ChannelInfo__ctor_m1_3103 ();
extern "C" void ChannelInfo_get_ChannelData_m1_3104 ();
extern "C" void ChannelServices__cctor_m1_3105 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m1_3106 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m1_3107 ();
extern "C" void ChannelServices_RegisterChannel_m1_3108 ();
extern "C" void ChannelServices_RegisterChannel_m1_3109 ();
extern "C" void ChannelServices_RegisterChannelConfig_m1_3110 ();
extern "C" void ChannelServices_CreateProvider_m1_3111 ();
extern "C" void ChannelServices_GetCurrentChannelInfo_m1_3112 ();
extern "C" void CrossAppDomainData__ctor_m1_3113 ();
extern "C" void CrossAppDomainData_get_DomainID_m1_3114 ();
extern "C" void CrossAppDomainData_get_ProcessID_m1_3115 ();
extern "C" void CrossAppDomainChannel__ctor_m1_3116 ();
extern "C" void CrossAppDomainChannel__cctor_m1_3117 ();
extern "C" void CrossAppDomainChannel_RegisterCrossAppDomainChannel_m1_3118 ();
extern "C" void CrossAppDomainChannel_get_ChannelName_m1_3119 ();
extern "C" void CrossAppDomainChannel_get_ChannelPriority_m1_3120 ();
extern "C" void CrossAppDomainChannel_get_ChannelData_m1_3121 ();
extern "C" void CrossAppDomainChannel_StartListening_m1_3122 ();
extern "C" void CrossAppDomainChannel_CreateMessageSink_m1_3123 ();
extern "C" void CrossAppDomainSink__ctor_m1_3124 ();
extern "C" void CrossAppDomainSink__cctor_m1_3125 ();
extern "C" void CrossAppDomainSink_GetSink_m1_3126 ();
extern "C" void CrossAppDomainSink_get_TargetDomainId_m1_3127 ();
extern "C" void SinkProviderData__ctor_m1_3128 ();
extern "C" void SinkProviderData_get_Children_m1_3129 ();
extern "C" void SinkProviderData_get_Properties_m1_3130 ();
extern "C" void Context__ctor_m1_3131 ();
extern "C" void Context__cctor_m1_3132 ();
extern "C" void Context_Finalize_m1_3133 ();
extern "C" void Context_get_DefaultContext_m1_3134 ();
extern "C" void Context_get_ContextID_m1_3135 ();
extern "C" void Context_get_ContextProperties_m1_3136 ();
extern "C" void Context_get_IsDefaultContext_m1_3137 ();
extern "C" void Context_get_NeedsContextSink_m1_3138 ();
extern "C" void Context_RegisterDynamicProperty_m1_3139 ();
extern "C" void Context_UnregisterDynamicProperty_m1_3140 ();
extern "C" void Context_GetDynamicPropertyCollection_m1_3141 ();
extern "C" void Context_NotifyGlobalDynamicSinks_m1_3142 ();
extern "C" void Context_get_HasGlobalDynamicSinks_m1_3143 ();
extern "C" void Context_NotifyDynamicSinks_m1_3144 ();
extern "C" void Context_get_HasDynamicSinks_m1_3145 ();
extern "C" void Context_get_HasExitSinks_m1_3146 ();
extern "C" void Context_GetProperty_m1_3147 ();
extern "C" void Context_SetProperty_m1_3148 ();
extern "C" void Context_Freeze_m1_3149 ();
extern "C" void Context_ToString_m1_3150 ();
extern "C" void Context_GetServerContextSinkChain_m1_3151 ();
extern "C" void Context_GetClientContextSinkChain_m1_3152 ();
extern "C" void Context_CreateServerObjectSinkChain_m1_3153 ();
extern "C" void Context_CreateEnvoySink_m1_3154 ();
extern "C" void Context_SwitchToContext_m1_3155 ();
extern "C" void Context_CreateNewContext_m1_3156 ();
extern "C" void Context_DoCallBack_m1_3157 ();
extern "C" void Context_AllocateDataSlot_m1_3158 ();
extern "C" void Context_AllocateNamedDataSlot_m1_3159 ();
extern "C" void Context_FreeNamedDataSlot_m1_3160 ();
extern "C" void Context_GetData_m1_3161 ();
extern "C" void Context_GetNamedDataSlot_m1_3162 ();
extern "C" void Context_SetData_m1_3163 ();
extern "C" void DynamicPropertyReg__ctor_m1_3164 ();
extern "C" void DynamicPropertyCollection__ctor_m1_3165 ();
extern "C" void DynamicPropertyCollection_get_HasProperties_m1_3166 ();
extern "C" void DynamicPropertyCollection_RegisterDynamicProperty_m1_3167 ();
extern "C" void DynamicPropertyCollection_UnregisterDynamicProperty_m1_3168 ();
extern "C" void DynamicPropertyCollection_NotifyMessage_m1_3169 ();
extern "C" void DynamicPropertyCollection_FindProperty_m1_3170 ();
extern "C" void ContextCallbackObject__ctor_m1_3171 ();
extern "C" void ContextCallbackObject_DoCallBack_m1_3172 ();
extern "C" void ContextAttribute__ctor_m1_3173 ();
extern "C" void ContextAttribute_get_Name_m1_3174 ();
extern "C" void ContextAttribute_Equals_m1_3175 ();
extern "C" void ContextAttribute_Freeze_m1_3176 ();
extern "C" void ContextAttribute_GetHashCode_m1_3177 ();
extern "C" void ContextAttribute_GetPropertiesForNewContext_m1_3178 ();
extern "C" void ContextAttribute_IsContextOK_m1_3179 ();
extern "C" void ContextAttribute_IsNewContextOK_m1_3180 ();
extern "C" void CrossContextChannel__ctor_m1_3181 ();
extern "C" void SynchronizationAttribute__ctor_m1_3182 ();
extern "C" void SynchronizationAttribute__ctor_m1_3183 ();
extern "C" void SynchronizationAttribute_set_Locked_m1_3184 ();
extern "C" void SynchronizationAttribute_ReleaseLock_m1_3185 ();
extern "C" void SynchronizationAttribute_GetPropertiesForNewContext_m1_3186 ();
extern "C" void SynchronizationAttribute_GetClientContextSink_m1_3187 ();
extern "C" void SynchronizationAttribute_GetServerContextSink_m1_3188 ();
extern "C" void SynchronizationAttribute_IsContextOK_m1_3189 ();
extern "C" void SynchronizationAttribute_ExitContext_m1_3190 ();
extern "C" void SynchronizationAttribute_EnterContext_m1_3191 ();
extern "C" void SynchronizedClientContextSink__ctor_m1_3192 ();
extern "C" void SynchronizedServerContextSink__ctor_m1_3193 ();
extern "C" void RenewalDelegate__ctor_m1_3194 ();
extern "C" void RenewalDelegate_Invoke_m1_3195 ();
extern "C" void RenewalDelegate_BeginInvoke_m1_3196 ();
extern "C" void RenewalDelegate_EndInvoke_m1_3197 ();
extern "C" void LeaseManager__ctor_m1_3198 ();
extern "C" void LeaseManager_SetPollTime_m1_3199 ();
extern "C" void LeaseSink__ctor_m1_3200 ();
extern "C" void LifetimeServices__cctor_m1_3201 ();
extern "C" void LifetimeServices_set_LeaseManagerPollTime_m1_3202 ();
extern "C" void LifetimeServices_set_LeaseTime_m1_3203 ();
extern "C" void LifetimeServices_set_RenewOnCallTime_m1_3204 ();
extern "C" void LifetimeServices_set_SponsorshipTimeout_m1_3205 ();
extern "C" void ArgInfo__ctor_m1_3206 ();
extern "C" void ArgInfo_GetInOutArgs_m1_3207 ();
extern "C" void AsyncResult__ctor_m1_3208 ();
extern "C" void AsyncResult_get_AsyncState_m1_3209 ();
extern "C" void AsyncResult_get_AsyncWaitHandle_m1_3210 ();
extern "C" void AsyncResult_get_CompletedSynchronously_m1_3211 ();
extern "C" void AsyncResult_get_IsCompleted_m1_3212 ();
extern "C" void AsyncResult_get_EndInvokeCalled_m1_3213 ();
extern "C" void AsyncResult_set_EndInvokeCalled_m1_3214 ();
extern "C" void AsyncResult_get_AsyncDelegate_m1_3215 ();
extern "C" void AsyncResult_get_NextSink_m1_3216 ();
extern "C" void AsyncResult_AsyncProcessMessage_m1_3217 ();
extern "C" void AsyncResult_GetReplyMessage_m1_3218 ();
extern "C" void AsyncResult_SetMessageCtrl_m1_3219 ();
extern "C" void AsyncResult_SetCompletedSynchronously_m1_3220 ();
extern "C" void AsyncResult_EndInvoke_m1_3221 ();
extern "C" void AsyncResult_SyncProcessMessage_m1_3222 ();
extern "C" void AsyncResult_get_CallMessage_m1_3223 ();
extern "C" void AsyncResult_set_CallMessage_m1_3224 ();
extern "C" void ClientContextTerminatorSink__ctor_m1_3225 ();
extern "C" void ConstructionCall__ctor_m1_3226 ();
extern "C" void ConstructionCall__ctor_m1_3227 ();
extern "C" void ConstructionCall_InitDictionary_m1_3228 ();
extern "C" void ConstructionCall_set_IsContextOk_m1_3229 ();
extern "C" void ConstructionCall_get_ActivationType_m1_3230 ();
extern "C" void ConstructionCall_get_ActivationTypeName_m1_3231 ();
extern "C" void ConstructionCall_get_Activator_m1_3232 ();
extern "C" void ConstructionCall_set_Activator_m1_3233 ();
extern "C" void ConstructionCall_get_CallSiteActivationAttributes_m1_3234 ();
extern "C" void ConstructionCall_SetActivationAttributes_m1_3235 ();
extern "C" void ConstructionCall_get_ContextProperties_m1_3236 ();
extern "C" void ConstructionCall_InitMethodProperty_m1_3237 ();
extern "C" void ConstructionCall_GetObjectData_m1_3238 ();
extern "C" void ConstructionCall_get_Properties_m1_3239 ();
extern "C" void ConstructionCallDictionary__ctor_m1_3240 ();
extern "C" void ConstructionCallDictionary__cctor_m1_3241 ();
extern "C" void ConstructionCallDictionary_GetMethodProperty_m1_3242 ();
extern "C" void ConstructionCallDictionary_SetMethodProperty_m1_3243 ();
extern "C" void EnvoyTerminatorSink__ctor_m1_3244 ();
extern "C" void EnvoyTerminatorSink__cctor_m1_3245 ();
extern "C" void Header__ctor_m1_3246 ();
extern "C" void Header__ctor_m1_3247 ();
extern "C" void Header__ctor_m1_3248 ();
extern "C" void LogicalCallContext__ctor_m1_3249 ();
extern "C" void LogicalCallContext__ctor_m1_3250 ();
extern "C" void LogicalCallContext_GetObjectData_m1_3251 ();
extern "C" void LogicalCallContext_SetData_m1_3252 ();
extern "C" void LogicalCallContext_Clone_m1_3253 ();
extern "C" void CallContextRemotingData__ctor_m1_3254 ();
extern "C" void CallContextRemotingData_Clone_m1_3255 ();
extern "C" void MethodCall__ctor_m1_3256 ();
extern "C" void MethodCall__ctor_m1_3257 ();
extern "C" void MethodCall__ctor_m1_3258 ();
extern "C" void MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m1_3259 ();
extern "C" void MethodCall_InitMethodProperty_m1_3260 ();
extern "C" void MethodCall_GetObjectData_m1_3261 ();
extern "C" void MethodCall_get_Args_m1_3262 ();
extern "C" void MethodCall_get_LogicalCallContext_m1_3263 ();
extern "C" void MethodCall_get_MethodBase_m1_3264 ();
extern "C" void MethodCall_get_MethodName_m1_3265 ();
extern "C" void MethodCall_get_MethodSignature_m1_3266 ();
extern "C" void MethodCall_get_Properties_m1_3267 ();
extern "C" void MethodCall_InitDictionary_m1_3268 ();
extern "C" void MethodCall_get_TypeName_m1_3269 ();
extern "C" void MethodCall_get_Uri_m1_3270 ();
extern "C" void MethodCall_set_Uri_m1_3271 ();
extern "C" void MethodCall_Init_m1_3272 ();
extern "C" void MethodCall_ResolveMethod_m1_3273 ();
extern "C" void MethodCall_CastTo_m1_3274 ();
extern "C" void MethodCall_GetTypeNameFromAssemblyQualifiedName_m1_3275 ();
extern "C" void MethodCall_get_GenericArguments_m1_3276 ();
extern "C" void MethodCallDictionary__ctor_m1_3277 ();
extern "C" void MethodCallDictionary__cctor_m1_3278 ();
extern "C" void DictionaryEnumerator__ctor_m1_3279 ();
extern "C" void DictionaryEnumerator_get_Current_m1_3280 ();
extern "C" void DictionaryEnumerator_MoveNext_m1_3281 ();
extern "C" void DictionaryEnumerator_Reset_m1_3282 ();
extern "C" void DictionaryEnumerator_get_Entry_m1_3283 ();
extern "C" void DictionaryEnumerator_get_Key_m1_3284 ();
extern "C" void DictionaryEnumerator_get_Value_m1_3285 ();
extern "C" void MethodDictionary__ctor_m1_3286 ();
extern "C" void MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m1_3287 ();
extern "C" void MethodDictionary_set_MethodKeys_m1_3288 ();
extern "C" void MethodDictionary_AllocInternalProperties_m1_3289 ();
extern "C" void MethodDictionary_GetInternalProperties_m1_3290 ();
extern "C" void MethodDictionary_IsOverridenKey_m1_3291 ();
extern "C" void MethodDictionary_get_Item_m1_3292 ();
extern "C" void MethodDictionary_set_Item_m1_3293 ();
extern "C" void MethodDictionary_GetMethodProperty_m1_3294 ();
extern "C" void MethodDictionary_SetMethodProperty_m1_3295 ();
extern "C" void MethodDictionary_get_Keys_m1_3296 ();
extern "C" void MethodDictionary_get_Values_m1_3297 ();
extern "C" void MethodDictionary_Add_m1_3298 ();
extern "C" void MethodDictionary_Contains_m1_3299 ();
extern "C" void MethodDictionary_Remove_m1_3300 ();
extern "C" void MethodDictionary_get_Count_m1_3301 ();
extern "C" void MethodDictionary_get_SyncRoot_m1_3302 ();
extern "C" void MethodDictionary_CopyTo_m1_3303 ();
extern "C" void MethodDictionary_GetEnumerator_m1_3304 ();
extern "C" void MethodReturnDictionary__ctor_m1_3305 ();
extern "C" void MethodReturnDictionary__cctor_m1_3306 ();
extern "C" void MonoMethodMessage_get_Args_m1_3307 ();
extern "C" void MonoMethodMessage_get_LogicalCallContext_m1_3308 ();
extern "C" void MonoMethodMessage_get_MethodBase_m1_3309 ();
extern "C" void MonoMethodMessage_get_MethodName_m1_3310 ();
extern "C" void MonoMethodMessage_get_MethodSignature_m1_3311 ();
extern "C" void MonoMethodMessage_get_TypeName_m1_3312 ();
extern "C" void MonoMethodMessage_get_Uri_m1_3313 ();
extern "C" void MonoMethodMessage_set_Uri_m1_3314 ();
extern "C" void MonoMethodMessage_get_Exception_m1_3315 ();
extern "C" void MonoMethodMessage_get_OutArgCount_m1_3316 ();
extern "C" void MonoMethodMessage_get_OutArgs_m1_3317 ();
extern "C" void MonoMethodMessage_get_ReturnValue_m1_3318 ();
extern "C" void RemotingSurrogate__ctor_m1_3319 ();
extern "C" void RemotingSurrogate_SetObjectData_m1_3320 ();
extern "C" void ObjRefSurrogate__ctor_m1_3321 ();
extern "C" void ObjRefSurrogate_SetObjectData_m1_3322 ();
extern "C" void RemotingSurrogateSelector__ctor_m1_3323 ();
extern "C" void RemotingSurrogateSelector__cctor_m1_3324 ();
extern "C" void RemotingSurrogateSelector_GetSurrogate_m1_3325 ();
extern "C" void ReturnMessage__ctor_m1_3326 ();
extern "C" void ReturnMessage__ctor_m1_3327 ();
extern "C" void ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m1_3328 ();
extern "C" void ReturnMessage_get_Args_m1_3329 ();
extern "C" void ReturnMessage_get_LogicalCallContext_m1_3330 ();
extern "C" void ReturnMessage_get_MethodBase_m1_3331 ();
extern "C" void ReturnMessage_get_MethodName_m1_3332 ();
extern "C" void ReturnMessage_get_MethodSignature_m1_3333 ();
extern "C" void ReturnMessage_get_Properties_m1_3334 ();
extern "C" void ReturnMessage_get_TypeName_m1_3335 ();
extern "C" void ReturnMessage_get_Uri_m1_3336 ();
extern "C" void ReturnMessage_set_Uri_m1_3337 ();
extern "C" void ReturnMessage_get_Exception_m1_3338 ();
extern "C" void ReturnMessage_get_OutArgs_m1_3339 ();
extern "C" void ReturnMessage_get_ReturnValue_m1_3340 ();
extern "C" void ServerContextTerminatorSink__ctor_m1_3341 ();
extern "C" void ServerObjectTerminatorSink__ctor_m1_3342 ();
extern "C" void StackBuilderSink__ctor_m1_3343 ();
extern "C" void SoapAttribute__ctor_m1_3344 ();
extern "C" void SoapAttribute_get_UseAttribute_m1_3345 ();
extern "C" void SoapAttribute_get_XmlNamespace_m1_3346 ();
extern "C" void SoapAttribute_SetReflectionObject_m1_3347 ();
extern "C" void SoapFieldAttribute__ctor_m1_3348 ();
extern "C" void SoapFieldAttribute_get_XmlElementName_m1_3349 ();
extern "C" void SoapFieldAttribute_IsInteropXmlElement_m1_3350 ();
extern "C" void SoapFieldAttribute_SetReflectionObject_m1_3351 ();
extern "C" void SoapMethodAttribute__ctor_m1_3352 ();
extern "C" void SoapMethodAttribute_get_UseAttribute_m1_3353 ();
extern "C" void SoapMethodAttribute_get_XmlNamespace_m1_3354 ();
extern "C" void SoapMethodAttribute_SetReflectionObject_m1_3355 ();
extern "C" void SoapParameterAttribute__ctor_m1_3356 ();
extern "C" void SoapTypeAttribute__ctor_m1_3357 ();
extern "C" void SoapTypeAttribute_get_UseAttribute_m1_3358 ();
extern "C" void SoapTypeAttribute_get_XmlElementName_m1_3359 ();
extern "C" void SoapTypeAttribute_get_XmlNamespace_m1_3360 ();
extern "C" void SoapTypeAttribute_get_XmlTypeName_m1_3361 ();
extern "C" void SoapTypeAttribute_get_XmlTypeNamespace_m1_3362 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlElement_m1_3363 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlType_m1_3364 ();
extern "C" void SoapTypeAttribute_SetReflectionObject_m1_3365 ();
extern "C" void ProxyAttribute_CreateInstance_m1_3366 ();
extern "C" void ProxyAttribute_CreateProxy_m1_3367 ();
extern "C" void ProxyAttribute_GetPropertiesForNewContext_m1_3368 ();
extern "C" void ProxyAttribute_IsContextOK_m1_3369 ();
extern "C" void RealProxy__ctor_m1_3370 ();
extern "C" void RealProxy__ctor_m1_3371 ();
extern "C" void RealProxy__ctor_m1_3372 ();
extern "C" void RealProxy_InternalGetProxyType_m1_3373 ();
extern "C" void RealProxy_GetProxiedType_m1_3374 ();
extern "C" void RealProxy_get_ObjectIdentity_m1_3375 ();
extern "C" void RealProxy_InternalGetTransparentProxy_m1_3376 ();
extern "C" void RealProxy_GetTransparentProxy_m1_3377 ();
extern "C" void RealProxy_SetTargetDomain_m1_3378 ();
extern "C" void RemotingProxy__ctor_m1_3379 ();
extern "C" void RemotingProxy__ctor_m1_3380 ();
extern "C" void RemotingProxy__cctor_m1_3381 ();
extern "C" void RemotingProxy_get_TypeName_m1_3382 ();
extern "C" void RemotingProxy_Finalize_m1_3383 ();
extern "C" void TrackingServices__cctor_m1_3384 ();
extern "C" void TrackingServices_NotifyUnmarshaledObject_m1_3385 ();
extern "C" void ActivatedClientTypeEntry__ctor_m1_3386 ();
extern "C" void ActivatedClientTypeEntry_get_ApplicationUrl_m1_3387 ();
extern "C" void ActivatedClientTypeEntry_get_ContextAttributes_m1_3388 ();
extern "C" void ActivatedClientTypeEntry_get_ObjectType_m1_3389 ();
extern "C" void ActivatedClientTypeEntry_ToString_m1_3390 ();
extern "C" void ActivatedServiceTypeEntry__ctor_m1_3391 ();
extern "C" void ActivatedServiceTypeEntry_get_ObjectType_m1_3392 ();
extern "C" void ActivatedServiceTypeEntry_ToString_m1_3393 ();
extern "C" void EnvoyInfo__ctor_m1_3394 ();
extern "C" void EnvoyInfo_get_EnvoySinks_m1_3395 ();
extern "C" void Identity__ctor_m1_3396 ();
extern "C" void Identity_get_ChannelSink_m1_3397 ();
extern "C" void Identity_set_ChannelSink_m1_3398 ();
extern "C" void Identity_get_ObjectUri_m1_3399 ();
extern "C" void Identity_get_Disposed_m1_3400 ();
extern "C" void Identity_set_Disposed_m1_3401 ();
extern "C" void Identity_get_ClientDynamicProperties_m1_3402 ();
extern "C" void Identity_get_ServerDynamicProperties_m1_3403 ();
extern "C" void ClientIdentity__ctor_m1_3404 ();
extern "C" void ClientIdentity_get_ClientProxy_m1_3405 ();
extern "C" void ClientIdentity_set_ClientProxy_m1_3406 ();
extern "C" void ClientIdentity_CreateObjRef_m1_3407 ();
extern "C" void ClientIdentity_get_TargetUri_m1_3408 ();
extern "C" void InternalRemotingServices__cctor_m1_3409 ();
extern "C" void InternalRemotingServices_GetCachedSoapAttribute_m1_3410 ();
extern "C" void ObjRef__ctor_m1_3411 ();
extern "C" void ObjRef__ctor_m1_3412 ();
extern "C" void ObjRef__cctor_m1_3413 ();
extern "C" void ObjRef_get_IsReferenceToWellKnow_m1_3414 ();
extern "C" void ObjRef_get_ChannelInfo_m1_3415 ();
extern "C" void ObjRef_get_EnvoyInfo_m1_3416 ();
extern "C" void ObjRef_set_EnvoyInfo_m1_3417 ();
extern "C" void ObjRef_get_TypeInfo_m1_3418 ();
extern "C" void ObjRef_set_TypeInfo_m1_3419 ();
extern "C" void ObjRef_get_URI_m1_3420 ();
extern "C" void ObjRef_set_URI_m1_3421 ();
extern "C" void ObjRef_GetObjectData_m1_3422 ();
extern "C" void ObjRef_GetRealObject_m1_3423 ();
extern "C" void ObjRef_UpdateChannelInfo_m1_3424 ();
extern "C" void ObjRef_get_ServerType_m1_3425 ();
extern "C" void RemotingConfiguration__cctor_m1_3426 ();
extern "C" void RemotingConfiguration_get_ApplicationName_m1_3427 ();
extern "C" void RemotingConfiguration_set_ApplicationName_m1_3428 ();
extern "C" void RemotingConfiguration_get_ProcessId_m1_3429 ();
extern "C" void RemotingConfiguration_LoadDefaultDelayedChannels_m1_3430 ();
extern "C" void RemotingConfiguration_IsRemotelyActivatedClientType_m1_3431 ();
extern "C" void RemotingConfiguration_RegisterActivatedClientType_m1_3432 ();
extern "C" void RemotingConfiguration_RegisterActivatedServiceType_m1_3433 ();
extern "C" void RemotingConfiguration_RegisterWellKnownClientType_m1_3434 ();
extern "C" void RemotingConfiguration_RegisterWellKnownServiceType_m1_3435 ();
extern "C" void RemotingConfiguration_RegisterChannelTemplate_m1_3436 ();
extern "C" void RemotingConfiguration_RegisterClientProviderTemplate_m1_3437 ();
extern "C" void RemotingConfiguration_RegisterServerProviderTemplate_m1_3438 ();
extern "C" void RemotingConfiguration_RegisterChannels_m1_3439 ();
extern "C" void RemotingConfiguration_RegisterTypes_m1_3440 ();
extern "C" void RemotingConfiguration_SetCustomErrorsMode_m1_3441 ();
extern "C" void ConfigHandler__ctor_m1_3442 ();
extern "C" void ConfigHandler_ValidatePath_m1_3443 ();
extern "C" void ConfigHandler_CheckPath_m1_3444 ();
extern "C" void ConfigHandler_OnStartParsing_m1_3445 ();
extern "C" void ConfigHandler_OnProcessingInstruction_m1_3446 ();
extern "C" void ConfigHandler_OnIgnorableWhitespace_m1_3447 ();
extern "C" void ConfigHandler_OnStartElement_m1_3448 ();
extern "C" void ConfigHandler_ParseElement_m1_3449 ();
extern "C" void ConfigHandler_OnEndElement_m1_3450 ();
extern "C" void ConfigHandler_ReadCustomProviderData_m1_3451 ();
extern "C" void ConfigHandler_ReadLifetine_m1_3452 ();
extern "C" void ConfigHandler_ParseTime_m1_3453 ();
extern "C" void ConfigHandler_ReadChannel_m1_3454 ();
extern "C" void ConfigHandler_ReadProvider_m1_3455 ();
extern "C" void ConfigHandler_ReadClientActivated_m1_3456 ();
extern "C" void ConfigHandler_ReadServiceActivated_m1_3457 ();
extern "C" void ConfigHandler_ReadClientWellKnown_m1_3458 ();
extern "C" void ConfigHandler_ReadServiceWellKnown_m1_3459 ();
extern "C" void ConfigHandler_ReadInteropXml_m1_3460 ();
extern "C" void ConfigHandler_ReadPreload_m1_3461 ();
extern "C" void ConfigHandler_GetNotNull_m1_3462 ();
extern "C" void ConfigHandler_ExtractAssembly_m1_3463 ();
extern "C" void ConfigHandler_OnChars_m1_3464 ();
extern "C" void ConfigHandler_OnEndParsing_m1_3465 ();
extern "C" void ChannelData__ctor_m1_3466 ();
extern "C" void ChannelData_get_ServerProviders_m1_3467 ();
extern "C" void ChannelData_get_ClientProviders_m1_3468 ();
extern "C" void ChannelData_get_CustomProperties_m1_3469 ();
extern "C" void ChannelData_CopyFrom_m1_3470 ();
extern "C" void ProviderData__ctor_m1_3471 ();
extern "C" void ProviderData_CopyFrom_m1_3472 ();
extern "C" void FormatterData__ctor_m1_3473 ();
extern "C" void RemotingException__ctor_m1_3474 ();
extern "C" void RemotingException__ctor_m1_3475 ();
extern "C" void RemotingException__ctor_m1_3476 ();
extern "C" void RemotingException__ctor_m1_3477 ();
extern "C" void RemotingServices__cctor_m1_3478 ();
extern "C" void RemotingServices_GetVirtualMethod_m1_3479 ();
extern "C" void RemotingServices_IsTransparentProxy_m1_3480 ();
extern "C" void RemotingServices_GetServerTypeForUri_m1_3481 ();
extern "C" void RemotingServices_Unmarshal_m1_3482 ();
extern "C" void RemotingServices_Unmarshal_m1_3483 ();
extern "C" void RemotingServices_GetRealProxy_m1_3484 ();
extern "C" void RemotingServices_GetMethodBaseFromMethodMessage_m1_3485 ();
extern "C" void RemotingServices_GetMethodBaseFromName_m1_3486 ();
extern "C" void RemotingServices_FindInterfaceMethod_m1_3487 ();
extern "C" void RemotingServices_CreateClientProxy_m1_3488 ();
extern "C" void RemotingServices_CreateClientProxy_m1_3489 ();
extern "C" void RemotingServices_CreateClientProxyForContextBound_m1_3490 ();
extern "C" void RemotingServices_GetIdentityForUri_m1_3491 ();
extern "C" void RemotingServices_RemoveAppNameFromUri_m1_3492 ();
extern "C" void RemotingServices_GetOrCreateClientIdentity_m1_3493 ();
extern "C" void RemotingServices_GetClientChannelSinkChain_m1_3494 ();
extern "C" void RemotingServices_CreateWellKnownServerIdentity_m1_3495 ();
extern "C" void RemotingServices_RegisterServerIdentity_m1_3496 ();
extern "C" void RemotingServices_GetProxyForRemoteObject_m1_3497 ();
extern "C" void RemotingServices_GetRemoteObject_m1_3498 ();
extern "C" void RemotingServices_RegisterInternalChannels_m1_3499 ();
extern "C" void RemotingServices_DisposeIdentity_m1_3500 ();
extern "C" void RemotingServices_GetNormalizedUri_m1_3501 ();
extern "C" void ServerIdentity__ctor_m1_3502 ();
extern "C" void ServerIdentity_get_ObjectType_m1_3503 ();
extern "C" void ServerIdentity_CreateObjRef_m1_3504 ();
extern "C" void ClientActivatedIdentity_GetServerObject_m1_3505 ();
extern "C" void SingletonIdentity__ctor_m1_3506 ();
extern "C" void SingleCallIdentity__ctor_m1_3507 ();
extern "C" void TypeInfo__ctor_m1_3508 ();
extern "C" void SoapServices__cctor_m1_3509 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithAssembly_m1_3510 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNs_m1_3511 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m1_3512 ();
extern "C" void SoapServices_CodeXmlNamespaceForClrTypeNamespace_m1_3513 ();
extern "C" void SoapServices_GetNameKey_m1_3514 ();
extern "C" void SoapServices_GetAssemblyName_m1_3515 ();
extern "C" void SoapServices_GetXmlElementForInteropType_m1_3516 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodCall_m1_3517 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodResponse_m1_3518 ();
extern "C" void SoapServices_GetXmlTypeForInteropType_m1_3519 ();
extern "C" void SoapServices_PreLoad_m1_3520 ();
extern "C" void SoapServices_PreLoad_m1_3521 ();
extern "C" void SoapServices_RegisterInteropXmlElement_m1_3522 ();
extern "C" void SoapServices_RegisterInteropXmlType_m1_3523 ();
extern "C" void SoapServices_EncodeNs_m1_3524 ();
extern "C" void TypeEntry__ctor_m1_3525 ();
extern "C" void TypeEntry_get_AssemblyName_m1_3526 ();
extern "C" void TypeEntry_set_AssemblyName_m1_3527 ();
extern "C" void TypeEntry_get_TypeName_m1_3528 ();
extern "C" void TypeEntry_set_TypeName_m1_3529 ();
extern "C" void TypeInfo__ctor_m1_3530 ();
extern "C" void TypeInfo_get_TypeName_m1_3531 ();
extern "C" void WellKnownClientTypeEntry__ctor_m1_3532 ();
extern "C" void WellKnownClientTypeEntry_get_ApplicationUrl_m1_3533 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectType_m1_3534 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectUrl_m1_3535 ();
extern "C" void WellKnownClientTypeEntry_ToString_m1_3536 ();
extern "C" void WellKnownServiceTypeEntry__ctor_m1_3537 ();
extern "C" void WellKnownServiceTypeEntry_get_Mode_m1_3538 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectType_m1_3539 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectUri_m1_3540 ();
extern "C" void WellKnownServiceTypeEntry_ToString_m1_3541 ();
extern "C" void BinaryCommon__cctor_m1_3542 ();
extern "C" void BinaryCommon_IsPrimitive_m1_3543 ();
extern "C" void BinaryCommon_GetTypeFromCode_m1_3544 ();
extern "C" void BinaryCommon_SwapBytes_m1_3545 ();
extern "C" void BinaryFormatter__ctor_m1_3546 ();
extern "C" void BinaryFormatter__ctor_m1_3547 ();
extern "C" void BinaryFormatter_get_DefaultSurrogateSelector_m1_3548 ();
extern "C" void BinaryFormatter_set_AssemblyFormat_m1_3549 ();
extern "C" void BinaryFormatter_get_Binder_m1_3550 ();
extern "C" void BinaryFormatter_get_Context_m1_3551 ();
extern "C" void BinaryFormatter_get_SurrogateSelector_m1_3552 ();
extern "C" void BinaryFormatter_get_FilterLevel_m1_3553 ();
extern "C" void BinaryFormatter_Deserialize_m1_3554 ();
extern "C" void BinaryFormatter_NoCheckDeserialize_m1_3555 ();
extern "C" void BinaryFormatter_ReadBinaryHeader_m1_3556 ();
extern "C" void MessageFormatter_ReadMethodCall_m1_3557 ();
extern "C" void MessageFormatter_ReadMethodResponse_m1_3558 ();
extern "C" void TypeMetadata__ctor_m1_3559 ();
extern "C" void ArrayNullFiller__ctor_m1_3560 ();
extern "C" void ObjectReader__ctor_m1_3561 ();
extern "C" void ObjectReader_ReadObjectGraph_m1_3562 ();
extern "C" void ObjectReader_ReadObjectGraph_m1_3563 ();
extern "C" void ObjectReader_ReadNextObject_m1_3564 ();
extern "C" void ObjectReader_ReadNextObject_m1_3565 ();
extern "C" void ObjectReader_get_CurrentObject_m1_3566 ();
extern "C" void ObjectReader_ReadObject_m1_3567 ();
extern "C" void ObjectReader_ReadAssembly_m1_3568 ();
extern "C" void ObjectReader_ReadObjectInstance_m1_3569 ();
extern "C" void ObjectReader_ReadRefTypeObjectInstance_m1_3570 ();
extern "C" void ObjectReader_ReadObjectContent_m1_3571 ();
extern "C" void ObjectReader_RegisterObject_m1_3572 ();
extern "C" void ObjectReader_ReadStringIntance_m1_3573 ();
extern "C" void ObjectReader_ReadGenericArray_m1_3574 ();
extern "C" void ObjectReader_ReadBoxedPrimitiveTypeValue_m1_3575 ();
extern "C" void ObjectReader_ReadArrayOfPrimitiveType_m1_3576 ();
extern "C" void ObjectReader_BlockRead_m1_3577 ();
extern "C" void ObjectReader_ReadArrayOfObject_m1_3578 ();
extern "C" void ObjectReader_ReadArrayOfString_m1_3579 ();
extern "C" void ObjectReader_ReadSimpleArray_m1_3580 ();
extern "C" void ObjectReader_ReadTypeMetadata_m1_3581 ();
extern "C" void ObjectReader_ReadValue_m1_3582 ();
extern "C" void ObjectReader_SetObjectValue_m1_3583 ();
extern "C" void ObjectReader_RecordFixup_m1_3584 ();
extern "C" void ObjectReader_GetDeserializationType_m1_3585 ();
extern "C" void ObjectReader_ReadType_m1_3586 ();
extern "C" void ObjectReader_ReadPrimitiveTypeValue_m1_3587 ();
extern "C" void FormatterConverter__ctor_m1_3588 ();
extern "C" void FormatterConverter_Convert_m1_3589 ();
extern "C" void FormatterConverter_ToBoolean_m1_3590 ();
extern "C" void FormatterConverter_ToInt16_m1_3591 ();
extern "C" void FormatterConverter_ToInt32_m1_3592 ();
extern "C" void FormatterConverter_ToInt64_m1_3593 ();
extern "C" void FormatterConverter_ToString_m1_3594 ();
extern "C" void FormatterConverter_ToUInt32_m1_3595 ();
extern "C" void FormatterServices_GetUninitializedObject_m1_3596 ();
extern "C" void FormatterServices_GetSafeUninitializedObject_m1_3597 ();
extern "C" void ObjectManager__ctor_m1_3598 ();
extern "C" void ObjectManager_DoFixups_m1_3599 ();
extern "C" void ObjectManager_GetObjectRecord_m1_3600 ();
extern "C" void ObjectManager_GetObject_m1_3601 ();
extern "C" void ObjectManager_RaiseDeserializationEvent_m1_3602 ();
extern "C" void ObjectManager_RaiseOnDeserializingEvent_m1_3603 ();
extern "C" void ObjectManager_RaiseOnDeserializedEvent_m1_3604 ();
extern "C" void ObjectManager_AddFixup_m1_3605 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m1_3606 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m1_3607 ();
extern "C" void ObjectManager_RecordDelayedFixup_m1_3608 ();
extern "C" void ObjectManager_RecordFixup_m1_3609 ();
extern "C" void ObjectManager_RegisterObjectInternal_m1_3610 ();
extern "C" void ObjectManager_RegisterObject_m1_3611 ();
extern "C" void BaseFixupRecord__ctor_m1_3612 ();
extern "C" void BaseFixupRecord_DoFixup_m1_3613 ();
extern "C" void ArrayFixupRecord__ctor_m1_3614 ();
extern "C" void ArrayFixupRecord_FixupImpl_m1_3615 ();
extern "C" void MultiArrayFixupRecord__ctor_m1_3616 ();
extern "C" void MultiArrayFixupRecord_FixupImpl_m1_3617 ();
extern "C" void FixupRecord__ctor_m1_3618 ();
extern "C" void FixupRecord_FixupImpl_m1_3619 ();
extern "C" void DelayedFixupRecord__ctor_m1_3620 ();
extern "C" void DelayedFixupRecord_FixupImpl_m1_3621 ();
extern "C" void ObjectRecord__ctor_m1_3622 ();
extern "C" void ObjectRecord_SetMemberValue_m1_3623 ();
extern "C" void ObjectRecord_SetArrayValue_m1_3624 ();
extern "C" void ObjectRecord_SetMemberValue_m1_3625 ();
extern "C" void ObjectRecord_get_IsInstanceReady_m1_3626 ();
extern "C" void ObjectRecord_get_IsUnsolvedObjectReference_m1_3627 ();
extern "C" void ObjectRecord_get_IsRegistered_m1_3628 ();
extern "C" void ObjectRecord_DoFixups_m1_3629 ();
extern "C" void ObjectRecord_RemoveFixup_m1_3630 ();
extern "C" void ObjectRecord_UnchainFixup_m1_3631 ();
extern "C" void ObjectRecord_ChainFixup_m1_3632 ();
extern "C" void ObjectRecord_LoadData_m1_3633 ();
extern "C" void ObjectRecord_get_HasPendingFixups_m1_3634 ();
extern "C" void SerializationBinder__ctor_m1_3635 ();
extern "C" void CallbackHandler__ctor_m1_3636 ();
extern "C" void CallbackHandler_Invoke_m1_3637 ();
extern "C" void CallbackHandler_BeginInvoke_m1_3638 ();
extern "C" void CallbackHandler_EndInvoke_m1_3639 ();
extern "C" void SerializationCallbacks__ctor_m1_3640 ();
extern "C" void SerializationCallbacks__cctor_m1_3641 ();
extern "C" void SerializationCallbacks_get_HasDeserializedCallbacks_m1_3642 ();
extern "C" void SerializationCallbacks_GetMethodsByAttribute_m1_3643 ();
extern "C" void SerializationCallbacks_Invoke_m1_3644 ();
extern "C" void SerializationCallbacks_RaiseOnDeserializing_m1_3645 ();
extern "C" void SerializationCallbacks_RaiseOnDeserialized_m1_3646 ();
extern "C" void SerializationCallbacks_GetSerializationCallbacks_m1_3647 ();
extern "C" void SerializationEntry__ctor_m1_3648 ();
extern "C" void SerializationEntry_get_Name_m1_3649 ();
extern "C" void SerializationEntry_get_Value_m1_3650 ();
extern "C" void SerializationException__ctor_m1_3651 ();
extern "C" void SerializationException__ctor_m1_3652 ();
extern "C" void SerializationException__ctor_m1_3653 ();
extern "C" void SerializationInfo__ctor_m1_3654 ();
extern "C" void SerializationInfo_AddValue_m1_3655 ();
extern "C" void SerializationInfo_GetValue_m1_3656 ();
extern "C" void SerializationInfo_SetType_m1_3657 ();
extern "C" void SerializationInfo_GetEnumerator_m1_3658 ();
extern "C" void SerializationInfo_AddValue_m1_3659 ();
extern "C" void SerializationInfo_AddValue_m1_3660 ();
extern "C" void SerializationInfo_AddValue_m1_3661 ();
extern "C" void SerializationInfo_AddValue_m1_3662 ();
extern "C" void SerializationInfo_AddValue_m1_3663 ();
extern "C" void SerializationInfo_AddValue_m1_3664 ();
extern "C" void SerializationInfo_AddValue_m1_3665 ();
extern "C" void SerializationInfo_AddValue_m1_3666 ();
extern "C" void SerializationInfo_AddValue_m1_3667 ();
extern "C" void SerializationInfo_GetBoolean_m1_3668 ();
extern "C" void SerializationInfo_GetInt16_m1_3669 ();
extern "C" void SerializationInfo_GetInt32_m1_3670 ();
extern "C" void SerializationInfo_GetInt64_m1_3671 ();
extern "C" void SerializationInfo_GetString_m1_3672 ();
extern "C" void SerializationInfo_GetUInt32_m1_3673 ();
extern "C" void SerializationInfoEnumerator__ctor_m1_3674 ();
extern "C" void SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m1_3675 ();
extern "C" void SerializationInfoEnumerator_get_Current_m1_3676 ();
extern "C" void SerializationInfoEnumerator_get_Name_m1_3677 ();
extern "C" void SerializationInfoEnumerator_get_Value_m1_3678 ();
extern "C" void SerializationInfoEnumerator_MoveNext_m1_3679 ();
extern "C" void SerializationInfoEnumerator_Reset_m1_3680 ();
extern "C" void StreamingContext__ctor_m1_3681 ();
extern "C" void StreamingContext__ctor_m1_3682 ();
extern "C" void StreamingContext_get_State_m1_3683 ();
extern "C" void StreamingContext_Equals_m1_3684 ();
extern "C" void StreamingContext_GetHashCode_m1_3685 ();
extern "C" void X509Certificate__ctor_m1_3686 ();
extern "C" void X509Certificate__ctor_m1_3687 ();
extern "C" void X509Certificate__ctor_m1_3688 ();
extern "C" void X509Certificate__ctor_m1_3689 ();
extern "C" void X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_3690 ();
extern "C" void X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m1_3691 ();
extern "C" void X509Certificate_tostr_m1_3692 ();
extern "C" void X509Certificate_Equals_m1_3693 ();
extern "C" void X509Certificate_GetCertHash_m1_3694 ();
extern "C" void X509Certificate_GetCertHashString_m1_3695 ();
extern "C" void X509Certificate_GetEffectiveDateString_m1_3696 ();
extern "C" void X509Certificate_GetExpirationDateString_m1_3697 ();
extern "C" void X509Certificate_GetHashCode_m1_3698 ();
extern "C" void X509Certificate_GetIssuerName_m1_3699 ();
extern "C" void X509Certificate_GetName_m1_3700 ();
extern "C" void X509Certificate_GetPublicKey_m1_3701 ();
extern "C" void X509Certificate_GetRawCertData_m1_3702 ();
extern "C" void X509Certificate_ToString_m1_3703 ();
extern "C" void X509Certificate_ToString_m1_3704 ();
extern "C" void X509Certificate_get_Issuer_m1_3705 ();
extern "C" void X509Certificate_get_Subject_m1_3706 ();
extern "C" void X509Certificate_Equals_m1_3707 ();
extern "C" void X509Certificate_Import_m1_3708 ();
extern "C" void X509Certificate_Reset_m1_3709 ();
extern "C" void AsymmetricAlgorithm__ctor_m1_3710 ();
extern "C" void AsymmetricAlgorithm_System_IDisposable_Dispose_m1_3711 ();
extern "C" void AsymmetricAlgorithm_get_KeySize_m1_3712 ();
extern "C" void AsymmetricAlgorithm_set_KeySize_m1_3713 ();
extern "C" void AsymmetricAlgorithm_Clear_m1_3714 ();
extern "C" void AsymmetricAlgorithm_GetNamedParam_m1_3715 ();
extern "C" void AsymmetricKeyExchangeFormatter__ctor_m1_3716 ();
extern "C" void AsymmetricSignatureDeformatter__ctor_m1_3717 ();
extern "C" void AsymmetricSignatureFormatter__ctor_m1_3718 ();
extern "C" void Base64Constants__cctor_m1_3719 ();
extern "C" void CryptoConfig__cctor_m1_3720 ();
extern "C" void CryptoConfig_Initialize_m1_3721 ();
extern "C" void CryptoConfig_CreateFromName_m1_3722 ();
extern "C" void CryptoConfig_CreateFromName_m1_3723 ();
extern "C" void CryptoConfig_MapNameToOID_m1_3724 ();
extern "C" void CryptoConfig_EncodeOID_m1_3725 ();
extern "C" void CryptoConfig_EncodeLongNumber_m1_3726 ();
extern "C" void CryptographicException__ctor_m1_3727 ();
extern "C" void CryptographicException__ctor_m1_3728 ();
extern "C" void CryptographicException__ctor_m1_3729 ();
extern "C" void CryptographicException__ctor_m1_3730 ();
extern "C" void CryptographicException__ctor_m1_3731 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m1_3732 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m1_3733 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m1_3734 ();
extern "C" void CspParameters__ctor_m1_3735 ();
extern "C" void CspParameters__ctor_m1_3736 ();
extern "C" void CspParameters__ctor_m1_3737 ();
extern "C" void CspParameters__ctor_m1_3738 ();
extern "C" void CspParameters_get_Flags_m1_3739 ();
extern "C" void CspParameters_set_Flags_m1_3740 ();
extern "C" void DES__ctor_m1_3741 ();
extern "C" void DES__cctor_m1_3742 ();
extern "C" void DES_Create_m1_3743 ();
extern "C" void DES_Create_m1_3744 ();
extern "C" void DES_IsWeakKey_m1_3745 ();
extern "C" void DES_IsSemiWeakKey_m1_3746 ();
extern "C" void DES_get_Key_m1_3747 ();
extern "C" void DES_set_Key_m1_3748 ();
extern "C" void DESTransform__ctor_m1_3749 ();
extern "C" void DESTransform__cctor_m1_3750 ();
extern "C" void DESTransform_CipherFunct_m1_3751 ();
extern "C" void DESTransform_Permutation_m1_3752 ();
extern "C" void DESTransform_BSwap_m1_3753 ();
extern "C" void DESTransform_SetKey_m1_3754 ();
extern "C" void DESTransform_ProcessBlock_m1_3755 ();
extern "C" void DESTransform_ECB_m1_3756 ();
extern "C" void DESTransform_GetStrongKey_m1_3757 ();
extern "C" void DESCryptoServiceProvider__ctor_m1_3758 ();
extern "C" void DESCryptoServiceProvider_CreateDecryptor_m1_3759 ();
extern "C" void DESCryptoServiceProvider_CreateEncryptor_m1_3760 ();
extern "C" void DESCryptoServiceProvider_GenerateIV_m1_3761 ();
extern "C" void DESCryptoServiceProvider_GenerateKey_m1_3762 ();
extern "C" void DSA__ctor_m1_3763 ();
extern "C" void DSA_Create_m1_3764 ();
extern "C" void DSA_Create_m1_3765 ();
extern "C" void DSA_ZeroizePrivateKey_m1_3766 ();
extern "C" void DSA_FromXmlString_m1_3767 ();
extern "C" void DSA_ToXmlString_m1_3768 ();
extern "C" void DSACryptoServiceProvider__ctor_m1_3769 ();
extern "C" void DSACryptoServiceProvider__ctor_m1_3770 ();
extern "C" void DSACryptoServiceProvider__ctor_m1_3771 ();
extern "C" void DSACryptoServiceProvider__cctor_m1_3772 ();
extern "C" void DSACryptoServiceProvider_Finalize_m1_3773 ();
extern "C" void DSACryptoServiceProvider_get_KeySize_m1_3774 ();
extern "C" void DSACryptoServiceProvider_get_PublicOnly_m1_3775 ();
extern "C" void DSACryptoServiceProvider_ExportParameters_m1_3776 ();
extern "C" void DSACryptoServiceProvider_ImportParameters_m1_3777 ();
extern "C" void DSACryptoServiceProvider_CreateSignature_m1_3778 ();
extern "C" void DSACryptoServiceProvider_VerifySignature_m1_3779 ();
extern "C" void DSACryptoServiceProvider_Dispose_m1_3780 ();
extern "C" void DSACryptoServiceProvider_OnKeyGenerated_m1_3781 ();
extern "C" void DSASignatureDeformatter__ctor_m1_3782 ();
extern "C" void DSASignatureDeformatter__ctor_m1_3783 ();
extern "C" void DSASignatureDeformatter_SetHashAlgorithm_m1_3784 ();
extern "C" void DSASignatureDeformatter_SetKey_m1_3785 ();
extern "C" void DSASignatureDeformatter_VerifySignature_m1_3786 ();
extern "C" void DSASignatureFormatter__ctor_m1_3787 ();
extern "C" void DSASignatureFormatter_CreateSignature_m1_3788 ();
extern "C" void DSASignatureFormatter_SetHashAlgorithm_m1_3789 ();
extern "C" void DSASignatureFormatter_SetKey_m1_3790 ();
extern "C" void HMAC__ctor_m1_3791 ();
extern "C" void HMAC_get_BlockSizeValue_m1_3792 ();
extern "C" void HMAC_set_BlockSizeValue_m1_3793 ();
extern "C" void HMAC_set_HashName_m1_3794 ();
extern "C" void HMAC_get_Key_m1_3795 ();
extern "C" void HMAC_set_Key_m1_3796 ();
extern "C" void HMAC_get_Block_m1_3797 ();
extern "C" void HMAC_KeySetup_m1_3798 ();
extern "C" void HMAC_Dispose_m1_3799 ();
extern "C" void HMAC_HashCore_m1_3800 ();
extern "C" void HMAC_HashFinal_m1_3801 ();
extern "C" void HMAC_Initialize_m1_3802 ();
extern "C" void HMAC_Create_m1_3803 ();
extern "C" void HMAC_Create_m1_3804 ();
extern "C" void HMACMD5__ctor_m1_3805 ();
extern "C" void HMACMD5__ctor_m1_3806 ();
extern "C" void HMACRIPEMD160__ctor_m1_3807 ();
extern "C" void HMACRIPEMD160__ctor_m1_3808 ();
extern "C" void HMACSHA1__ctor_m1_3809 ();
extern "C" void HMACSHA1__ctor_m1_3810 ();
extern "C" void HMACSHA256__ctor_m1_3811 ();
extern "C" void HMACSHA256__ctor_m1_3812 ();
extern "C" void HMACSHA384__ctor_m1_3813 ();
extern "C" void HMACSHA384__ctor_m1_3814 ();
extern "C" void HMACSHA384__cctor_m1_3815 ();
extern "C" void HMACSHA384_set_ProduceLegacyHmacValues_m1_3816 ();
extern "C" void HMACSHA512__ctor_m1_3817 ();
extern "C" void HMACSHA512__ctor_m1_3818 ();
extern "C" void HMACSHA512__cctor_m1_3819 ();
extern "C" void HMACSHA512_set_ProduceLegacyHmacValues_m1_3820 ();
extern "C" void HashAlgorithm__ctor_m1_3821 ();
extern "C" void HashAlgorithm_System_IDisposable_Dispose_m1_3822 ();
extern "C" void HashAlgorithm_get_CanReuseTransform_m1_3823 ();
extern "C" void HashAlgorithm_ComputeHash_m1_3824 ();
extern "C" void HashAlgorithm_ComputeHash_m1_3825 ();
extern "C" void HashAlgorithm_Create_m1_3826 ();
extern "C" void HashAlgorithm_get_Hash_m1_3827 ();
extern "C" void HashAlgorithm_get_HashSize_m1_3828 ();
extern "C" void HashAlgorithm_Dispose_m1_3829 ();
extern "C" void HashAlgorithm_TransformBlock_m1_3830 ();
extern "C" void HashAlgorithm_TransformFinalBlock_m1_3831 ();
extern "C" void KeySizes__ctor_m1_3832 ();
extern "C" void KeySizes_get_MaxSize_m1_3833 ();
extern "C" void KeySizes_get_MinSize_m1_3834 ();
extern "C" void KeySizes_get_SkipSize_m1_3835 ();
extern "C" void KeySizes_IsLegal_m1_3836 ();
extern "C" void KeySizes_IsLegalKeySize_m1_3837 ();
extern "C" void KeyedHashAlgorithm__ctor_m1_3838 ();
extern "C" void KeyedHashAlgorithm_Finalize_m1_3839 ();
extern "C" void KeyedHashAlgorithm_get_Key_m1_3840 ();
extern "C" void KeyedHashAlgorithm_set_Key_m1_3841 ();
extern "C" void KeyedHashAlgorithm_Dispose_m1_3842 ();
extern "C" void KeyedHashAlgorithm_ZeroizeKey_m1_3843 ();
extern "C" void MACTripleDES__ctor_m1_3844 ();
extern "C" void MACTripleDES_Setup_m1_3845 ();
extern "C" void MACTripleDES_Finalize_m1_3846 ();
extern "C" void MACTripleDES_Dispose_m1_3847 ();
extern "C" void MACTripleDES_Initialize_m1_3848 ();
extern "C" void MACTripleDES_HashCore_m1_3849 ();
extern "C" void MACTripleDES_HashFinal_m1_3850 ();
extern "C" void MD5__ctor_m1_3851 ();
extern "C" void MD5_Create_m1_3852 ();
extern "C" void MD5_Create_m1_3853 ();
extern "C" void MD5CryptoServiceProvider__ctor_m1_3854 ();
extern "C" void MD5CryptoServiceProvider__cctor_m1_3855 ();
extern "C" void MD5CryptoServiceProvider_Finalize_m1_3856 ();
extern "C" void MD5CryptoServiceProvider_Dispose_m1_3857 ();
extern "C" void MD5CryptoServiceProvider_HashCore_m1_3858 ();
extern "C" void MD5CryptoServiceProvider_HashFinal_m1_3859 ();
extern "C" void MD5CryptoServiceProvider_Initialize_m1_3860 ();
extern "C" void MD5CryptoServiceProvider_ProcessBlock_m1_3861 ();
extern "C" void MD5CryptoServiceProvider_ProcessFinalBlock_m1_3862 ();
extern "C" void MD5CryptoServiceProvider_AddLength_m1_3863 ();
extern "C" void RC2__ctor_m1_3864 ();
extern "C" void RC2_Create_m1_3865 ();
extern "C" void RC2_Create_m1_3866 ();
extern "C" void RC2_get_EffectiveKeySize_m1_3867 ();
extern "C" void RC2_get_KeySize_m1_3868 ();
extern "C" void RC2_set_KeySize_m1_3869 ();
extern "C" void RC2CryptoServiceProvider__ctor_m1_3870 ();
extern "C" void RC2CryptoServiceProvider_get_EffectiveKeySize_m1_3871 ();
extern "C" void RC2CryptoServiceProvider_CreateDecryptor_m1_3872 ();
extern "C" void RC2CryptoServiceProvider_CreateEncryptor_m1_3873 ();
extern "C" void RC2CryptoServiceProvider_GenerateIV_m1_3874 ();
extern "C" void RC2CryptoServiceProvider_GenerateKey_m1_3875 ();
extern "C" void RC2Transform__ctor_m1_3876 ();
extern "C" void RC2Transform__cctor_m1_3877 ();
extern "C" void RC2Transform_ECB_m1_3878 ();
extern "C" void RIPEMD160__ctor_m1_3879 ();
extern "C" void RIPEMD160Managed__ctor_m1_3880 ();
extern "C" void RIPEMD160Managed_Initialize_m1_3881 ();
extern "C" void RIPEMD160Managed_HashCore_m1_3882 ();
extern "C" void RIPEMD160Managed_HashFinal_m1_3883 ();
extern "C" void RIPEMD160Managed_Finalize_m1_3884 ();
extern "C" void RIPEMD160Managed_ProcessBlock_m1_3885 ();
extern "C" void RIPEMD160Managed_Compress_m1_3886 ();
extern "C" void RIPEMD160Managed_CompressFinal_m1_3887 ();
extern "C" void RIPEMD160Managed_ROL_m1_3888 ();
extern "C" void RIPEMD160Managed_F_m1_3889 ();
extern "C" void RIPEMD160Managed_G_m1_3890 ();
extern "C" void RIPEMD160Managed_H_m1_3891 ();
extern "C" void RIPEMD160Managed_I_m1_3892 ();
extern "C" void RIPEMD160Managed_J_m1_3893 ();
extern "C" void RIPEMD160Managed_FF_m1_3894 ();
extern "C" void RIPEMD160Managed_GG_m1_3895 ();
extern "C" void RIPEMD160Managed_HH_m1_3896 ();
extern "C" void RIPEMD160Managed_II_m1_3897 ();
extern "C" void RIPEMD160Managed_JJ_m1_3898 ();
extern "C" void RIPEMD160Managed_FFF_m1_3899 ();
extern "C" void RIPEMD160Managed_GGG_m1_3900 ();
extern "C" void RIPEMD160Managed_HHH_m1_3901 ();
extern "C" void RIPEMD160Managed_III_m1_3902 ();
extern "C" void RIPEMD160Managed_JJJ_m1_3903 ();
extern "C" void RNGCryptoServiceProvider__ctor_m1_3904 ();
extern "C" void RNGCryptoServiceProvider__cctor_m1_3905 ();
extern "C" void RNGCryptoServiceProvider_Check_m1_3906 ();
extern "C" void RNGCryptoServiceProvider_RngOpen_m1_3907 ();
extern "C" void RNGCryptoServiceProvider_RngInitialize_m1_3908 ();
extern "C" void RNGCryptoServiceProvider_RngGetBytes_m1_3909 ();
extern "C" void RNGCryptoServiceProvider_RngClose_m1_3910 ();
extern "C" void RNGCryptoServiceProvider_GetBytes_m1_3911 ();
extern "C" void RNGCryptoServiceProvider_GetNonZeroBytes_m1_3912 ();
extern "C" void RNGCryptoServiceProvider_Finalize_m1_3913 ();
extern "C" void RSA__ctor_m1_3914 ();
extern "C" void RSA_Create_m1_3915 ();
extern "C" void RSA_Create_m1_3916 ();
extern "C" void RSA_ZeroizePrivateKey_m1_3917 ();
extern "C" void RSA_FromXmlString_m1_3918 ();
extern "C" void RSA_ToXmlString_m1_3919 ();
extern "C" void RSACryptoServiceProvider__ctor_m1_3920 ();
extern "C" void RSACryptoServiceProvider__ctor_m1_3921 ();
extern "C" void RSACryptoServiceProvider__ctor_m1_3922 ();
extern "C" void RSACryptoServiceProvider__cctor_m1_3923 ();
extern "C" void RSACryptoServiceProvider_Common_m1_3924 ();
extern "C" void RSACryptoServiceProvider_Finalize_m1_3925 ();
extern "C" void RSACryptoServiceProvider_get_KeySize_m1_3926 ();
extern "C" void RSACryptoServiceProvider_get_PublicOnly_m1_3927 ();
extern "C" void RSACryptoServiceProvider_DecryptValue_m1_3928 ();
extern "C" void RSACryptoServiceProvider_EncryptValue_m1_3929 ();
extern "C" void RSACryptoServiceProvider_ExportParameters_m1_3930 ();
extern "C" void RSACryptoServiceProvider_ImportParameters_m1_3931 ();
extern "C" void RSACryptoServiceProvider_Dispose_m1_3932 ();
extern "C" void RSACryptoServiceProvider_OnKeyGenerated_m1_3933 ();
extern "C" void RSAPKCS1KeyExchangeFormatter__ctor_m1_3934 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m1_3935 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_SetRSAKey_m1_3936 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m1_3937 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m1_3938 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m1_3939 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetKey_m1_3940 ();
extern "C" void RSAPKCS1SignatureDeformatter_VerifySignature_m1_3941 ();
extern "C" void RSAPKCS1SignatureFormatter__ctor_m1_3942 ();
extern "C" void RSAPKCS1SignatureFormatter_CreateSignature_m1_3943 ();
extern "C" void RSAPKCS1SignatureFormatter_SetHashAlgorithm_m1_3944 ();
extern "C" void RSAPKCS1SignatureFormatter_SetKey_m1_3945 ();
extern "C" void RandomNumberGenerator__ctor_m1_3946 ();
extern "C" void RandomNumberGenerator_Create_m1_3947 ();
extern "C" void RandomNumberGenerator_Create_m1_3948 ();
extern "C" void Rijndael__ctor_m1_3949 ();
extern "C" void Rijndael_Create_m1_3950 ();
extern "C" void Rijndael_Create_m1_3951 ();
extern "C" void RijndaelManaged__ctor_m1_3952 ();
extern "C" void RijndaelManaged_GenerateIV_m1_3953 ();
extern "C" void RijndaelManaged_GenerateKey_m1_3954 ();
extern "C" void RijndaelManaged_CreateDecryptor_m1_3955 ();
extern "C" void RijndaelManaged_CreateEncryptor_m1_3956 ();
extern "C" void RijndaelTransform__ctor_m1_3957 ();
extern "C" void RijndaelTransform__cctor_m1_3958 ();
extern "C" void RijndaelTransform_Clear_m1_3959 ();
extern "C" void RijndaelTransform_ECB_m1_3960 ();
extern "C" void RijndaelTransform_SubByte_m1_3961 ();
extern "C" void RijndaelTransform_Encrypt128_m1_3962 ();
extern "C" void RijndaelTransform_Encrypt192_m1_3963 ();
extern "C" void RijndaelTransform_Encrypt256_m1_3964 ();
extern "C" void RijndaelTransform_Decrypt128_m1_3965 ();
extern "C" void RijndaelTransform_Decrypt192_m1_3966 ();
extern "C" void RijndaelTransform_Decrypt256_m1_3967 ();
extern "C" void RijndaelManagedTransform__ctor_m1_3968 ();
extern "C" void RijndaelManagedTransform_System_IDisposable_Dispose_m1_3969 ();
extern "C" void RijndaelManagedTransform_get_CanReuseTransform_m1_3970 ();
extern "C" void RijndaelManagedTransform_TransformBlock_m1_3971 ();
extern "C" void RijndaelManagedTransform_TransformFinalBlock_m1_3972 ();
extern "C" void SHA1__ctor_m1_3973 ();
extern "C" void SHA1_Create_m1_3974 ();
extern "C" void SHA1_Create_m1_3975 ();
extern "C" void SHA1Internal__ctor_m1_3976 ();
extern "C" void SHA1Internal_HashCore_m1_3977 ();
extern "C" void SHA1Internal_HashFinal_m1_3978 ();
extern "C" void SHA1Internal_Initialize_m1_3979 ();
extern "C" void SHA1Internal_ProcessBlock_m1_3980 ();
extern "C" void SHA1Internal_InitialiseBuff_m1_3981 ();
extern "C" void SHA1Internal_FillBuff_m1_3982 ();
extern "C" void SHA1Internal_ProcessFinalBlock_m1_3983 ();
extern "C" void SHA1Internal_AddLength_m1_3984 ();
extern "C" void SHA1CryptoServiceProvider__ctor_m1_3985 ();
extern "C" void SHA1CryptoServiceProvider_Finalize_m1_3986 ();
extern "C" void SHA1CryptoServiceProvider_Dispose_m1_3987 ();
extern "C" void SHA1CryptoServiceProvider_HashCore_m1_3988 ();
extern "C" void SHA1CryptoServiceProvider_HashFinal_m1_3989 ();
extern "C" void SHA1CryptoServiceProvider_Initialize_m1_3990 ();
extern "C" void SHA1Managed__ctor_m1_3991 ();
extern "C" void SHA1Managed_HashCore_m1_3992 ();
extern "C" void SHA1Managed_HashFinal_m1_3993 ();
extern "C" void SHA1Managed_Initialize_m1_3994 ();
extern "C" void SHA256__ctor_m1_3995 ();
extern "C" void SHA256_Create_m1_3996 ();
extern "C" void SHA256_Create_m1_3997 ();
extern "C" void SHA256Managed__ctor_m1_3998 ();
extern "C" void SHA256Managed_HashCore_m1_3999 ();
extern "C" void SHA256Managed_HashFinal_m1_4000 ();
extern "C" void SHA256Managed_Initialize_m1_4001 ();
extern "C" void SHA256Managed_ProcessBlock_m1_4002 ();
extern "C" void SHA256Managed_ProcessFinalBlock_m1_4003 ();
extern "C" void SHA256Managed_AddLength_m1_4004 ();
extern "C" void SHA384__ctor_m1_4005 ();
extern "C" void SHA384Managed__ctor_m1_4006 ();
extern "C" void SHA384Managed_Initialize_m1_4007 ();
extern "C" void SHA384Managed_Initialize_m1_4008 ();
extern "C" void SHA384Managed_HashCore_m1_4009 ();
extern "C" void SHA384Managed_HashFinal_m1_4010 ();
extern "C" void SHA384Managed_update_m1_4011 ();
extern "C" void SHA384Managed_processWord_m1_4012 ();
extern "C" void SHA384Managed_unpackWord_m1_4013 ();
extern "C" void SHA384Managed_adjustByteCounts_m1_4014 ();
extern "C" void SHA384Managed_processLength_m1_4015 ();
extern "C" void SHA384Managed_processBlock_m1_4016 ();
extern "C" void SHA512__ctor_m1_4017 ();
extern "C" void SHA512Managed__ctor_m1_4018 ();
extern "C" void SHA512Managed_Initialize_m1_4019 ();
extern "C" void SHA512Managed_Initialize_m1_4020 ();
extern "C" void SHA512Managed_HashCore_m1_4021 ();
extern "C" void SHA512Managed_HashFinal_m1_4022 ();
extern "C" void SHA512Managed_update_m1_4023 ();
extern "C" void SHA512Managed_processWord_m1_4024 ();
extern "C" void SHA512Managed_unpackWord_m1_4025 ();
extern "C" void SHA512Managed_adjustByteCounts_m1_4026 ();
extern "C" void SHA512Managed_processLength_m1_4027 ();
extern "C" void SHA512Managed_processBlock_m1_4028 ();
extern "C" void SHA512Managed_rotateRight_m1_4029 ();
extern "C" void SHA512Managed_Ch_m1_4030 ();
extern "C" void SHA512Managed_Maj_m1_4031 ();
extern "C" void SHA512Managed_Sum0_m1_4032 ();
extern "C" void SHA512Managed_Sum1_m1_4033 ();
extern "C" void SHA512Managed_Sigma0_m1_4034 ();
extern "C" void SHA512Managed_Sigma1_m1_4035 ();
extern "C" void SHAConstants__cctor_m1_4036 ();
extern "C" void SignatureDescription__ctor_m1_4037 ();
extern "C" void SignatureDescription_set_DeformatterAlgorithm_m1_4038 ();
extern "C" void SignatureDescription_set_DigestAlgorithm_m1_4039 ();
extern "C" void SignatureDescription_set_FormatterAlgorithm_m1_4040 ();
extern "C" void SignatureDescription_set_KeyAlgorithm_m1_4041 ();
extern "C" void DSASignatureDescription__ctor_m1_4042 ();
extern "C" void RSAPKCS1SHA1SignatureDescription__ctor_m1_4043 ();
extern "C" void SymmetricAlgorithm__ctor_m1_4044 ();
extern "C" void SymmetricAlgorithm_System_IDisposable_Dispose_m1_4045 ();
extern "C" void SymmetricAlgorithm_Finalize_m1_4046 ();
extern "C" void SymmetricAlgorithm_Clear_m1_4047 ();
extern "C" void SymmetricAlgorithm_Dispose_m1_4048 ();
extern "C" void SymmetricAlgorithm_get_BlockSize_m1_4049 ();
extern "C" void SymmetricAlgorithm_set_BlockSize_m1_4050 ();
extern "C" void SymmetricAlgorithm_get_FeedbackSize_m1_4051 ();
extern "C" void SymmetricAlgorithm_get_IV_m1_4052 ();
extern "C" void SymmetricAlgorithm_set_IV_m1_4053 ();
extern "C" void SymmetricAlgorithm_get_Key_m1_4054 ();
extern "C" void SymmetricAlgorithm_set_Key_m1_4055 ();
extern "C" void SymmetricAlgorithm_get_KeySize_m1_4056 ();
extern "C" void SymmetricAlgorithm_set_KeySize_m1_4057 ();
extern "C" void SymmetricAlgorithm_get_LegalKeySizes_m1_4058 ();
extern "C" void SymmetricAlgorithm_get_Mode_m1_4059 ();
extern "C" void SymmetricAlgorithm_set_Mode_m1_4060 ();
extern "C" void SymmetricAlgorithm_get_Padding_m1_4061 ();
extern "C" void SymmetricAlgorithm_set_Padding_m1_4062 ();
extern "C" void SymmetricAlgorithm_CreateDecryptor_m1_4063 ();
extern "C" void SymmetricAlgorithm_CreateEncryptor_m1_4064 ();
extern "C" void SymmetricAlgorithm_Create_m1_4065 ();
extern "C" void ToBase64Transform_System_IDisposable_Dispose_m1_4066 ();
extern "C" void ToBase64Transform_Finalize_m1_4067 ();
extern "C" void ToBase64Transform_get_CanReuseTransform_m1_4068 ();
extern "C" void ToBase64Transform_get_InputBlockSize_m1_4069 ();
extern "C" void ToBase64Transform_get_OutputBlockSize_m1_4070 ();
extern "C" void ToBase64Transform_Dispose_m1_4071 ();
extern "C" void ToBase64Transform_TransformBlock_m1_4072 ();
extern "C" void ToBase64Transform_InternalTransformBlock_m1_4073 ();
extern "C" void ToBase64Transform_TransformFinalBlock_m1_4074 ();
extern "C" void ToBase64Transform_InternalTransformFinalBlock_m1_4075 ();
extern "C" void TripleDES__ctor_m1_4076 ();
extern "C" void TripleDES_get_Key_m1_4077 ();
extern "C" void TripleDES_set_Key_m1_4078 ();
extern "C" void TripleDES_IsWeakKey_m1_4079 ();
extern "C" void TripleDES_Create_m1_4080 ();
extern "C" void TripleDES_Create_m1_4081 ();
extern "C" void TripleDESCryptoServiceProvider__ctor_m1_4082 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateIV_m1_4083 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateKey_m1_4084 ();
extern "C" void TripleDESCryptoServiceProvider_CreateDecryptor_m1_4085 ();
extern "C" void TripleDESCryptoServiceProvider_CreateEncryptor_m1_4086 ();
extern "C" void TripleDESTransform__ctor_m1_4087 ();
extern "C" void TripleDESTransform_ECB_m1_4088 ();
extern "C" void TripleDESTransform_GetStrongKey_m1_4089 ();
extern "C" void SecurityPermission__ctor_m1_4090 ();
extern "C" void SecurityPermission_set_Flags_m1_4091 ();
extern "C" void SecurityPermission_IsUnrestricted_m1_4092 ();
extern "C" void SecurityPermission_IsSubsetOf_m1_4093 ();
extern "C" void SecurityPermission_ToXml_m1_4094 ();
extern "C" void SecurityPermission_IsEmpty_m1_4095 ();
extern "C" void SecurityPermission_Cast_m1_4096 ();
extern "C" void StrongNamePublicKeyBlob_Equals_m1_4097 ();
extern "C" void StrongNamePublicKeyBlob_GetHashCode_m1_4098 ();
extern "C" void StrongNamePublicKeyBlob_ToString_m1_4099 ();
extern "C" void ApplicationTrust__ctor_m1_4100 ();
extern "C" void EvidenceEnumerator__ctor_m1_4101 ();
extern "C" void EvidenceEnumerator_MoveNext_m1_4102 ();
extern "C" void EvidenceEnumerator_Reset_m1_4103 ();
extern "C" void EvidenceEnumerator_get_Current_m1_4104 ();
extern "C" void Evidence__ctor_m1_4105 ();
extern "C" void Evidence_get_Count_m1_4106 ();
extern "C" void Evidence_get_SyncRoot_m1_4107 ();
extern "C" void Evidence_get_HostEvidenceList_m1_4108 ();
extern "C" void Evidence_get_AssemblyEvidenceList_m1_4109 ();
extern "C" void Evidence_CopyTo_m1_4110 ();
extern "C" void Evidence_Equals_m1_4111 ();
extern "C" void Evidence_GetEnumerator_m1_4112 ();
extern "C" void Evidence_GetHashCode_m1_4113 ();
extern "C" void Hash__ctor_m1_4114 ();
extern "C" void Hash__ctor_m1_4115 ();
extern "C" void Hash_GetObjectData_m1_4116 ();
extern "C" void Hash_ToString_m1_4117 ();
extern "C" void Hash_GetData_m1_4118 ();
extern "C" void StrongName_get_Name_m1_4119 ();
extern "C" void StrongName_get_PublicKey_m1_4120 ();
extern "C" void StrongName_get_Version_m1_4121 ();
extern "C" void StrongName_Equals_m1_4122 ();
extern "C" void StrongName_GetHashCode_m1_4123 ();
extern "C" void StrongName_ToString_m1_4124 ();
extern "C" void WindowsIdentity__ctor_m1_4125 ();
extern "C" void WindowsIdentity__cctor_m1_4126 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_4127 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m1_4128 ();
extern "C" void WindowsIdentity_Dispose_m1_4129 ();
extern "C" void WindowsIdentity_GetCurrentToken_m1_4130 ();
extern "C" void WindowsIdentity_GetTokenName_m1_4131 ();
extern "C" void CodeAccessPermission__ctor_m1_4132 ();
extern "C" void CodeAccessPermission_Equals_m1_4133 ();
extern "C" void CodeAccessPermission_GetHashCode_m1_4134 ();
extern "C" void CodeAccessPermission_ToString_m1_4135 ();
extern "C" void CodeAccessPermission_Element_m1_4136 ();
extern "C" void CodeAccessPermission_ThrowInvalidPermission_m1_4137 ();
extern "C" void PermissionSet__ctor_m1_4138 ();
extern "C" void PermissionSet__ctor_m1_4139 ();
extern "C" void PermissionSet_set_DeclarativeSecurity_m1_4140 ();
extern "C" void PermissionSet_CreateFromBinaryFormat_m1_4141 ();
extern "C" void SecurityContext__ctor_m1_4142 ();
extern "C" void SecurityContext__ctor_m1_4143 ();
extern "C" void SecurityContext_Capture_m1_4144 ();
extern "C" void SecurityContext_get_FlowSuppressed_m1_4145 ();
extern "C" void SecurityContext_get_CompressedStack_m1_4146 ();
extern "C" void SecurityAttribute__ctor_m1_4147 ();
extern "C" void SecurityAttribute_get_Name_m1_4148 ();
extern "C" void SecurityAttribute_get_Value_m1_4149 ();
extern "C" void SecurityElement__ctor_m1_4150 ();
extern "C" void SecurityElement__ctor_m1_4151 ();
extern "C" void SecurityElement__cctor_m1_4152 ();
extern "C" void SecurityElement_get_Children_m1_4153 ();
extern "C" void SecurityElement_get_Tag_m1_4154 ();
extern "C" void SecurityElement_set_Text_m1_4155 ();
extern "C" void SecurityElement_AddAttribute_m1_4156 ();
extern "C" void SecurityElement_AddChild_m1_4157 ();
extern "C" void SecurityElement_Escape_m1_4158 ();
extern "C" void SecurityElement_Unescape_m1_4159 ();
extern "C" void SecurityElement_IsValidAttributeName_m1_4160 ();
extern "C" void SecurityElement_IsValidAttributeValue_m1_4161 ();
extern "C" void SecurityElement_IsValidTag_m1_4162 ();
extern "C" void SecurityElement_IsValidText_m1_4163 ();
extern "C" void SecurityElement_SearchForChildByTag_m1_4164 ();
extern "C" void SecurityElement_ToString_m1_4165 ();
extern "C" void SecurityElement_ToXml_m1_4166 ();
extern "C" void SecurityElement_GetAttribute_m1_4167 ();
extern "C" void SecurityException__ctor_m1_4168 ();
extern "C" void SecurityException__ctor_m1_4169 ();
extern "C" void SecurityException__ctor_m1_4170 ();
extern "C" void SecurityException_get_Demanded_m1_4171 ();
extern "C" void SecurityException_get_FirstPermissionThatFailed_m1_4172 ();
extern "C" void SecurityException_get_PermissionState_m1_4173 ();
extern "C" void SecurityException_get_PermissionType_m1_4174 ();
extern "C" void SecurityException_get_GrantedSet_m1_4175 ();
extern "C" void SecurityException_get_RefusedSet_m1_4176 ();
extern "C" void SecurityException_GetObjectData_m1_4177 ();
extern "C" void SecurityException_ToString_m1_4178 ();
extern "C" void SecurityFrame__ctor_m1_4179 ();
extern "C" void SecurityFrame__GetSecurityStack_m1_4180 ();
extern "C" void SecurityFrame_InitFromRuntimeFrame_m1_4181 ();
extern "C" void SecurityFrame_get_Assembly_m1_4182 ();
extern "C" void SecurityFrame_get_Domain_m1_4183 ();
extern "C" void SecurityFrame_ToString_m1_4184 ();
extern "C" void SecurityFrame_GetStack_m1_4185 ();
extern "C" void SecurityManager__cctor_m1_4186 ();
extern "C" void SecurityManager_get_SecurityEnabled_m1_4187 ();
extern "C" void SecurityManager_Decode_m1_4188 ();
extern "C" void SecurityManager_Decode_m1_4189 ();
extern "C" void SecuritySafeCriticalAttribute__ctor_m1_4190 ();
extern "C" void SuppressUnmanagedCodeSecurityAttribute__ctor_m1_4191 ();
extern "C" void UnverifiableCodeAttribute__ctor_m1_4192 ();
extern "C" void ASCIIEncoding__ctor_m1_4193 ();
extern "C" void ASCIIEncoding_GetByteCount_m1_4194 ();
extern "C" void ASCIIEncoding_GetByteCount_m1_4195 ();
extern "C" void ASCIIEncoding_GetBytes_m1_4196 ();
extern "C" void ASCIIEncoding_GetBytes_m1_4197 ();
extern "C" void ASCIIEncoding_GetBytes_m1_4198 ();
extern "C" void ASCIIEncoding_GetBytes_m1_4199 ();
extern "C" void ASCIIEncoding_GetCharCount_m1_4200 ();
extern "C" void ASCIIEncoding_GetChars_m1_4201 ();
extern "C" void ASCIIEncoding_GetChars_m1_4202 ();
extern "C" void ASCIIEncoding_GetMaxByteCount_m1_4203 ();
extern "C" void ASCIIEncoding_GetMaxCharCount_m1_4204 ();
extern "C" void ASCIIEncoding_GetString_m1_4205 ();
extern "C" void ASCIIEncoding_GetBytes_m1_4206 ();
extern "C" void ASCIIEncoding_GetByteCount_m1_4207 ();
extern "C" void ASCIIEncoding_GetDecoder_m1_4208 ();
extern "C" void Decoder__ctor_m1_4209 ();
extern "C" void Decoder_set_Fallback_m1_4210 ();
extern "C" void Decoder_get_FallbackBuffer_m1_4211 ();
extern "C" void DecoderExceptionFallback__ctor_m1_4212 ();
extern "C" void DecoderExceptionFallback_CreateFallbackBuffer_m1_4213 ();
extern "C" void DecoderExceptionFallback_Equals_m1_4214 ();
extern "C" void DecoderExceptionFallback_GetHashCode_m1_4215 ();
extern "C" void DecoderExceptionFallbackBuffer__ctor_m1_4216 ();
extern "C" void DecoderExceptionFallbackBuffer_get_Remaining_m1_4217 ();
extern "C" void DecoderExceptionFallbackBuffer_Fallback_m1_4218 ();
extern "C" void DecoderExceptionFallbackBuffer_GetNextChar_m1_4219 ();
extern "C" void DecoderFallback__ctor_m1_4220 ();
extern "C" void DecoderFallback__cctor_m1_4221 ();
extern "C" void DecoderFallback_get_ExceptionFallback_m1_4222 ();
extern "C" void DecoderFallback_get_ReplacementFallback_m1_4223 ();
extern "C" void DecoderFallback_get_StandardSafeFallback_m1_4224 ();
extern "C" void DecoderFallbackBuffer__ctor_m1_4225 ();
extern "C" void DecoderFallbackBuffer_Reset_m1_4226 ();
extern "C" void DecoderFallbackException__ctor_m1_4227 ();
extern "C" void DecoderFallbackException__ctor_m1_4228 ();
extern "C" void DecoderFallbackException__ctor_m1_4229 ();
extern "C" void DecoderReplacementFallback__ctor_m1_4230 ();
extern "C" void DecoderReplacementFallback__ctor_m1_4231 ();
extern "C" void DecoderReplacementFallback_get_DefaultString_m1_4232 ();
extern "C" void DecoderReplacementFallback_CreateFallbackBuffer_m1_4233 ();
extern "C" void DecoderReplacementFallback_Equals_m1_4234 ();
extern "C" void DecoderReplacementFallback_GetHashCode_m1_4235 ();
extern "C" void DecoderReplacementFallbackBuffer__ctor_m1_4236 ();
extern "C" void DecoderReplacementFallbackBuffer_get_Remaining_m1_4237 ();
extern "C" void DecoderReplacementFallbackBuffer_Fallback_m1_4238 ();
extern "C" void DecoderReplacementFallbackBuffer_GetNextChar_m1_4239 ();
extern "C" void DecoderReplacementFallbackBuffer_Reset_m1_4240 ();
extern "C" void EncoderExceptionFallback__ctor_m1_4241 ();
extern "C" void EncoderExceptionFallback_CreateFallbackBuffer_m1_4242 ();
extern "C" void EncoderExceptionFallback_Equals_m1_4243 ();
extern "C" void EncoderExceptionFallback_GetHashCode_m1_4244 ();
extern "C" void EncoderExceptionFallbackBuffer__ctor_m1_4245 ();
extern "C" void EncoderExceptionFallbackBuffer_get_Remaining_m1_4246 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m1_4247 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m1_4248 ();
extern "C" void EncoderExceptionFallbackBuffer_GetNextChar_m1_4249 ();
extern "C" void EncoderFallback__ctor_m1_4250 ();
extern "C" void EncoderFallback__cctor_m1_4251 ();
extern "C" void EncoderFallback_get_ExceptionFallback_m1_4252 ();
extern "C" void EncoderFallback_get_ReplacementFallback_m1_4253 ();
extern "C" void EncoderFallback_get_StandardSafeFallback_m1_4254 ();
extern "C" void EncoderFallbackBuffer__ctor_m1_4255 ();
extern "C" void EncoderFallbackException__ctor_m1_4256 ();
extern "C" void EncoderFallbackException__ctor_m1_4257 ();
extern "C" void EncoderFallbackException__ctor_m1_4258 ();
extern "C" void EncoderFallbackException__ctor_m1_4259 ();
extern "C" void EncoderReplacementFallback__ctor_m1_4260 ();
extern "C" void EncoderReplacementFallback__ctor_m1_4261 ();
extern "C" void EncoderReplacementFallback_get_DefaultString_m1_4262 ();
extern "C" void EncoderReplacementFallback_CreateFallbackBuffer_m1_4263 ();
extern "C" void EncoderReplacementFallback_Equals_m1_4264 ();
extern "C" void EncoderReplacementFallback_GetHashCode_m1_4265 ();
extern "C" void EncoderReplacementFallbackBuffer__ctor_m1_4266 ();
extern "C" void EncoderReplacementFallbackBuffer_get_Remaining_m1_4267 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m1_4268 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m1_4269 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m1_4270 ();
extern "C" void EncoderReplacementFallbackBuffer_GetNextChar_m1_4271 ();
extern "C" void ForwardingDecoder__ctor_m1_4272 ();
extern "C" void ForwardingDecoder_GetChars_m1_4273 ();
extern "C" void Encoding__ctor_m1_4274 ();
extern "C" void Encoding__ctor_m1_4275 ();
extern "C" void Encoding__cctor_m1_4276 ();
extern "C" void Encoding___m1_4277 ();
extern "C" void Encoding_get_IsReadOnly_m1_4278 ();
extern "C" void Encoding_get_DecoderFallback_m1_4279 ();
extern "C" void Encoding_set_DecoderFallback_m1_4280 ();
extern "C" void Encoding_get_EncoderFallback_m1_4281 ();
extern "C" void Encoding_SetFallbackInternal_m1_4282 ();
extern "C" void Encoding_Equals_m1_4283 ();
extern "C" void Encoding_GetByteCount_m1_4284 ();
extern "C" void Encoding_GetByteCount_m1_4285 ();
extern "C" void Encoding_GetBytes_m1_4286 ();
extern "C" void Encoding_GetBytes_m1_4287 ();
extern "C" void Encoding_GetBytes_m1_4288 ();
extern "C" void Encoding_GetBytes_m1_4289 ();
extern "C" void Encoding_GetChars_m1_4290 ();
extern "C" void Encoding_GetDecoder_m1_4291 ();
extern "C" void Encoding_InvokeI18N_m1_4292 ();
extern "C" void Encoding_GetEncoding_m1_4293 ();
extern "C" void Encoding_Clone_m1_4294 ();
extern "C" void Encoding_GetEncoding_m1_4295 ();
extern "C" void Encoding_GetHashCode_m1_4296 ();
extern "C" void Encoding_GetPreamble_m1_4297 ();
extern "C" void Encoding_GetString_m1_4298 ();
extern "C" void Encoding_GetString_m1_4299 ();
extern "C" void Encoding_get_ASCII_m1_4300 ();
extern "C" void Encoding_get_BigEndianUnicode_m1_4301 ();
extern "C" void Encoding_InternalCodePage_m1_4302 ();
extern "C" void Encoding_get_Default_m1_4303 ();
extern "C" void Encoding_get_ISOLatin1_m1_4304 ();
extern "C" void Encoding_get_UTF7_m1_4305 ();
extern "C" void Encoding_get_UTF8_m1_4306 ();
extern "C" void Encoding_get_UTF8Unmarked_m1_4307 ();
extern "C" void Encoding_get_UTF8UnmarkedUnsafe_m1_4308 ();
extern "C" void Encoding_get_Unicode_m1_4309 ();
extern "C" void Encoding_get_UTF32_m1_4310 ();
extern "C" void Encoding_get_BigEndianUTF32_m1_4311 ();
extern "C" void Encoding_GetByteCount_m1_4312 ();
extern "C" void Encoding_GetBytes_m1_4313 ();
extern "C" void Latin1Encoding__ctor_m1_4314 ();
extern "C" void Latin1Encoding_GetByteCount_m1_4315 ();
extern "C" void Latin1Encoding_GetByteCount_m1_4316 ();
extern "C" void Latin1Encoding_GetBytes_m1_4317 ();
extern "C" void Latin1Encoding_GetBytes_m1_4318 ();
extern "C" void Latin1Encoding_GetBytes_m1_4319 ();
extern "C" void Latin1Encoding_GetBytes_m1_4320 ();
extern "C" void Latin1Encoding_GetCharCount_m1_4321 ();
extern "C" void Latin1Encoding_GetChars_m1_4322 ();
extern "C" void Latin1Encoding_GetMaxByteCount_m1_4323 ();
extern "C" void Latin1Encoding_GetMaxCharCount_m1_4324 ();
extern "C" void Latin1Encoding_GetString_m1_4325 ();
extern "C" void Latin1Encoding_GetString_m1_4326 ();
extern "C" void StringBuilder__ctor_m1_4327 ();
extern "C" void StringBuilder__ctor_m1_4328 ();
extern "C" void StringBuilder__ctor_m1_4329 ();
extern "C" void StringBuilder__ctor_m1_4330 ();
extern "C" void StringBuilder__ctor_m1_4331 ();
extern "C" void StringBuilder__ctor_m1_4332 ();
extern "C" void StringBuilder__ctor_m1_4333 ();
extern "C" void StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m1_4334 ();
extern "C" void StringBuilder_get_Capacity_m1_4335 ();
extern "C" void StringBuilder_set_Capacity_m1_4336 ();
extern "C" void StringBuilder_get_Length_m1_4337 ();
extern "C" void StringBuilder_set_Length_m1_4338 ();
extern "C" void StringBuilder_get_Chars_m1_4339 ();
extern "C" void StringBuilder_set_Chars_m1_4340 ();
extern "C" void StringBuilder_ToString_m1_4341 ();
extern "C" void StringBuilder_ToString_m1_4342 ();
extern "C" void StringBuilder_Remove_m1_4343 ();
extern "C" void StringBuilder_Replace_m1_4344 ();
extern "C" void StringBuilder_Replace_m1_4345 ();
extern "C" void StringBuilder_Append_m1_4346 ();
extern "C" void StringBuilder_Append_m1_4347 ();
extern "C" void StringBuilder_Append_m1_4348 ();
extern "C" void StringBuilder_Append_m1_4349 ();
extern "C" void StringBuilder_Append_m1_4350 ();
extern "C" void StringBuilder_Append_m1_4351 ();
extern "C" void StringBuilder_Append_m1_4352 ();
extern "C" void StringBuilder_Append_m1_4353 ();
extern "C" void StringBuilder_AppendLine_m1_4354 ();
extern "C" void StringBuilder_AppendFormat_m1_4355 ();
extern "C" void StringBuilder_AppendFormat_m1_4356 ();
extern "C" void StringBuilder_AppendFormat_m1_4357 ();
extern "C" void StringBuilder_AppendFormat_m1_4358 ();
extern "C" void StringBuilder_AppendFormat_m1_4359 ();
extern "C" void StringBuilder_Insert_m1_4360 ();
extern "C" void StringBuilder_Insert_m1_4361 ();
extern "C" void StringBuilder_Insert_m1_4362 ();
extern "C" void StringBuilder_InternalEnsureCapacity_m1_4363 ();
extern "C" void UTF32Decoder__ctor_m1_4364 ();
extern "C" void UTF32Decoder_GetChars_m1_4365 ();
extern "C" void UTF32Encoding__ctor_m1_4366 ();
extern "C" void UTF32Encoding__ctor_m1_4367 ();
extern "C" void UTF32Encoding__ctor_m1_4368 ();
extern "C" void UTF32Encoding_GetByteCount_m1_4369 ();
extern "C" void UTF32Encoding_GetBytes_m1_4370 ();
extern "C" void UTF32Encoding_GetCharCount_m1_4371 ();
extern "C" void UTF32Encoding_GetChars_m1_4372 ();
extern "C" void UTF32Encoding_GetMaxByteCount_m1_4373 ();
extern "C" void UTF32Encoding_GetMaxCharCount_m1_4374 ();
extern "C" void UTF32Encoding_GetDecoder_m1_4375 ();
extern "C" void UTF32Encoding_GetPreamble_m1_4376 ();
extern "C" void UTF32Encoding_Equals_m1_4377 ();
extern "C" void UTF32Encoding_GetHashCode_m1_4378 ();
extern "C" void UTF32Encoding_GetByteCount_m1_4379 ();
extern "C" void UTF32Encoding_GetByteCount_m1_4380 ();
extern "C" void UTF32Encoding_GetBytes_m1_4381 ();
extern "C" void UTF32Encoding_GetBytes_m1_4382 ();
extern "C" void UTF32Encoding_GetString_m1_4383 ();
extern "C" void UTF7Decoder__ctor_m1_4384 ();
extern "C" void UTF7Decoder_GetChars_m1_4385 ();
extern "C" void UTF7Encoding__ctor_m1_4386 ();
extern "C" void UTF7Encoding__ctor_m1_4387 ();
extern "C" void UTF7Encoding__cctor_m1_4388 ();
extern "C" void UTF7Encoding_GetHashCode_m1_4389 ();
extern "C" void UTF7Encoding_Equals_m1_4390 ();
extern "C" void UTF7Encoding_InternalGetByteCount_m1_4391 ();
extern "C" void UTF7Encoding_GetByteCount_m1_4392 ();
extern "C" void UTF7Encoding_InternalGetBytes_m1_4393 ();
extern "C" void UTF7Encoding_GetBytes_m1_4394 ();
extern "C" void UTF7Encoding_InternalGetCharCount_m1_4395 ();
extern "C" void UTF7Encoding_GetCharCount_m1_4396 ();
extern "C" void UTF7Encoding_InternalGetChars_m1_4397 ();
extern "C" void UTF7Encoding_GetChars_m1_4398 ();
extern "C" void UTF7Encoding_GetMaxByteCount_m1_4399 ();
extern "C" void UTF7Encoding_GetMaxCharCount_m1_4400 ();
extern "C" void UTF7Encoding_GetDecoder_m1_4401 ();
extern "C" void UTF7Encoding_GetByteCount_m1_4402 ();
extern "C" void UTF7Encoding_GetByteCount_m1_4403 ();
extern "C" void UTF7Encoding_GetBytes_m1_4404 ();
extern "C" void UTF7Encoding_GetBytes_m1_4405 ();
extern "C" void UTF7Encoding_GetString_m1_4406 ();
extern "C" void UTF8Decoder__ctor_m1_4407 ();
extern "C" void UTF8Decoder_GetChars_m1_4408 ();
extern "C" void UTF8Encoding__ctor_m1_4409 ();
extern "C" void UTF8Encoding__ctor_m1_4410 ();
extern "C" void UTF8Encoding__ctor_m1_4411 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m1_4412 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m1_4413 ();
extern "C" void UTF8Encoding_GetByteCount_m1_4414 ();
extern "C" void UTF8Encoding_GetByteCount_m1_4415 ();
extern "C" void UTF8Encoding_InternalGetBytes_m1_4416 ();
extern "C" void UTF8Encoding_InternalGetBytes_m1_4417 ();
extern "C" void UTF8Encoding_GetBytes_m1_4418 ();
extern "C" void UTF8Encoding_GetBytes_m1_4419 ();
extern "C" void UTF8Encoding_GetBytes_m1_4420 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m1_4421 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m1_4422 ();
extern "C" void UTF8Encoding_Fallback_m1_4423 ();
extern "C" void UTF8Encoding_Fallback_m1_4424 ();
extern "C" void UTF8Encoding_GetCharCount_m1_4425 ();
extern "C" void UTF8Encoding_InternalGetChars_m1_4426 ();
extern "C" void UTF8Encoding_InternalGetChars_m1_4427 ();
extern "C" void UTF8Encoding_GetChars_m1_4428 ();
extern "C" void UTF8Encoding_GetMaxByteCount_m1_4429 ();
extern "C" void UTF8Encoding_GetMaxCharCount_m1_4430 ();
extern "C" void UTF8Encoding_GetDecoder_m1_4431 ();
extern "C" void UTF8Encoding_GetPreamble_m1_4432 ();
extern "C" void UTF8Encoding_Equals_m1_4433 ();
extern "C" void UTF8Encoding_GetHashCode_m1_4434 ();
extern "C" void UTF8Encoding_GetByteCount_m1_4435 ();
extern "C" void UTF8Encoding_GetString_m1_4436 ();
extern "C" void UnicodeDecoder__ctor_m1_4437 ();
extern "C" void UnicodeDecoder_GetChars_m1_4438 ();
extern "C" void UnicodeEncoding__ctor_m1_4439 ();
extern "C" void UnicodeEncoding__ctor_m1_4440 ();
extern "C" void UnicodeEncoding__ctor_m1_4441 ();
extern "C" void UnicodeEncoding_GetByteCount_m1_4442 ();
extern "C" void UnicodeEncoding_GetByteCount_m1_4443 ();
extern "C" void UnicodeEncoding_GetByteCount_m1_4444 ();
extern "C" void UnicodeEncoding_GetBytes_m1_4445 ();
extern "C" void UnicodeEncoding_GetBytes_m1_4446 ();
extern "C" void UnicodeEncoding_GetBytes_m1_4447 ();
extern "C" void UnicodeEncoding_GetBytesInternal_m1_4448 ();
extern "C" void UnicodeEncoding_GetCharCount_m1_4449 ();
extern "C" void UnicodeEncoding_GetChars_m1_4450 ();
extern "C" void UnicodeEncoding_GetString_m1_4451 ();
extern "C" void UnicodeEncoding_GetCharsInternal_m1_4452 ();
extern "C" void UnicodeEncoding_GetMaxByteCount_m1_4453 ();
extern "C" void UnicodeEncoding_GetMaxCharCount_m1_4454 ();
extern "C" void UnicodeEncoding_GetDecoder_m1_4455 ();
extern "C" void UnicodeEncoding_GetPreamble_m1_4456 ();
extern "C" void UnicodeEncoding_Equals_m1_4457 ();
extern "C" void UnicodeEncoding_GetHashCode_m1_4458 ();
extern "C" void UnicodeEncoding_CopyChars_m1_4459 ();
extern "C" void CompressedStack__ctor_m1_4460 ();
extern "C" void CompressedStack__ctor_m1_4461 ();
extern "C" void CompressedStack_CreateCopy_m1_4462 ();
extern "C" void CompressedStack_Capture_m1_4463 ();
extern "C" void CompressedStack_GetObjectData_m1_4464 ();
extern "C" void CompressedStack_IsEmpty_m1_4465 ();
extern "C" void EventWaitHandle__ctor_m1_4466 ();
extern "C" void EventWaitHandle_IsManualReset_m1_4467 ();
extern "C" void EventWaitHandle_Reset_m1_4468 ();
extern "C" void EventWaitHandle_Set_m1_4469 ();
extern "C" void ExecutionContext__ctor_m1_4470 ();
extern "C" void ExecutionContext__ctor_m1_4471 ();
extern "C" void ExecutionContext__ctor_m1_4472 ();
extern "C" void ExecutionContext_Capture_m1_4473 ();
extern "C" void ExecutionContext_GetObjectData_m1_4474 ();
extern "C" void ExecutionContext_get_SecurityContext_m1_4475 ();
extern "C" void ExecutionContext_set_SecurityContext_m1_4476 ();
extern "C" void ExecutionContext_get_FlowSuppressed_m1_4477 ();
extern "C" void ExecutionContext_IsFlowSuppressed_m1_4478 ();
extern "C" void Interlocked_CompareExchange_m1_4479 ();
extern "C" void ManualResetEvent__ctor_m1_4480 ();
extern "C" void Monitor_Enter_m1_4481 ();
extern "C" void Monitor_Exit_m1_4482 ();
extern "C" void Monitor_Monitor_pulse_m1_4483 ();
extern "C" void Monitor_Monitor_test_synchronised_m1_4484 ();
extern "C" void Monitor_Pulse_m1_4485 ();
extern "C" void Monitor_Monitor_wait_m1_4486 ();
extern "C" void Monitor_Wait_m1_4487 ();
extern "C" void Mutex__ctor_m1_4488 ();
extern "C" void Mutex_CreateMutex_internal_m1_4489 ();
extern "C" void Mutex_ReleaseMutex_internal_m1_4490 ();
extern "C" void Mutex_ReleaseMutex_m1_4491 ();
extern "C" void NativeEventCalls_CreateEvent_internal_m1_4492 ();
extern "C" void NativeEventCalls_SetEvent_internal_m1_4493 ();
extern "C" void NativeEventCalls_ResetEvent_internal_m1_4494 ();
extern "C" void NativeEventCalls_CloseEvent_internal_m1_4495 ();
extern "C" void SynchronizationLockException__ctor_m1_4496 ();
extern "C" void SynchronizationLockException__ctor_m1_4497 ();
extern "C" void SynchronizationLockException__ctor_m1_4498 ();
extern "C" void Thread__ctor_m1_4499 ();
extern "C" void Thread__cctor_m1_4500 ();
extern "C" void Thread_get_CurrentContext_m1_4501 ();
extern "C" void Thread_CurrentThread_internal_m1_4502 ();
extern "C" void Thread_get_CurrentThread_m1_4503 ();
extern "C" void Thread_FreeLocalSlotValues_m1_4504 ();
extern "C" void Thread_GetDomainID_m1_4505 ();
extern "C" void Thread_ResetAbort_internal_m1_4506 ();
extern "C" void Thread_ResetAbort_m1_4507 ();
extern "C" void Thread_Sleep_internal_m1_4508 ();
extern "C" void Thread_Sleep_m1_4509 ();
extern "C" void Thread_Thread_internal_m1_4510 ();
extern "C" void Thread_Thread_init_m1_4511 ();
extern "C" void Thread_GetCachedCurrentCulture_m1_4512 ();
extern "C" void Thread_GetSerializedCurrentCulture_m1_4513 ();
extern "C" void Thread_SetCachedCurrentCulture_m1_4514 ();
extern "C" void Thread_GetCachedCurrentUICulture_m1_4515 ();
extern "C" void Thread_GetSerializedCurrentUICulture_m1_4516 ();
extern "C" void Thread_SetCachedCurrentUICulture_m1_4517 ();
extern "C" void Thread_get_CurrentCulture_m1_4518 ();
extern "C" void Thread_get_CurrentUICulture_m1_4519 ();
extern "C" void Thread_set_IsBackground_m1_4520 ();
extern "C" void Thread_SetName_internal_m1_4521 ();
extern "C" void Thread_set_Name_m1_4522 ();
extern "C" void Thread_Abort_internal_m1_4523 ();
extern "C" void Thread_Abort_m1_4524 ();
extern "C" void Thread_Start_m1_4525 ();
extern "C" void Thread_Thread_free_internal_m1_4526 ();
extern "C" void Thread_Finalize_m1_4527 ();
extern "C" void Thread_SetState_m1_4528 ();
extern "C" void Thread_ClrState_m1_4529 ();
extern "C" void Thread_GetNewManagedId_m1_4530 ();
extern "C" void Thread_GetNewManagedId_internal_m1_4531 ();
extern "C" void Thread_get_ExecutionContext_m1_4532 ();
extern "C" void Thread_get_ManagedThreadId_m1_4533 ();
extern "C" void Thread_GetHashCode_m1_4534 ();
extern "C" void Thread_GetCompressedStack_m1_4535 ();
extern "C" void ThreadAbortException__ctor_m1_4536 ();
extern "C" void ThreadAbortException__ctor_m1_4537 ();
extern "C" void ThreadInterruptedException__ctor_m1_4538 ();
extern "C" void ThreadInterruptedException__ctor_m1_4539 ();
extern "C" void ThreadPool_QueueUserWorkItem_m1_4540 ();
extern "C" void ThreadStateException__ctor_m1_4541 ();
extern "C" void ThreadStateException__ctor_m1_4542 ();
extern "C" void TimerComparer__ctor_m1_4543 ();
extern "C" void TimerComparer_Compare_m1_4544 ();
extern "C" void Scheduler__ctor_m1_4545 ();
extern "C" void Scheduler__cctor_m1_4546 ();
extern "C" void Scheduler_get_Instance_m1_4547 ();
extern "C" void Scheduler_Remove_m1_4548 ();
extern "C" void Scheduler_Change_m1_4549 ();
extern "C" void Scheduler_Add_m1_4550 ();
extern "C" void Scheduler_InternalRemove_m1_4551 ();
extern "C" void Scheduler_SchedulerThread_m1_4552 ();
extern "C" void Scheduler_ShrinkIfNeeded_m1_4553 ();
extern "C" void Timer__cctor_m1_4554 ();
extern "C" void Timer_Change_m1_4555 ();
extern "C" void Timer_Dispose_m1_4556 ();
extern "C" void Timer_Change_m1_4557 ();
extern "C" void WaitHandle__ctor_m1_4558 ();
extern "C" void WaitHandle__cctor_m1_4559 ();
extern "C" void WaitHandle_System_IDisposable_Dispose_m1_4560 ();
extern "C" void WaitHandle_get_Handle_m1_4561 ();
extern "C" void WaitHandle_set_Handle_m1_4562 ();
extern "C" void WaitHandle_WaitOne_internal_m1_4563 ();
extern "C" void WaitHandle_Dispose_m1_4564 ();
extern "C" void WaitHandle_WaitOne_m1_4565 ();
extern "C" void WaitHandle_WaitOne_m1_4566 ();
extern "C" void WaitHandle_CheckDisposed_m1_4567 ();
extern "C" void WaitHandle_Finalize_m1_4568 ();
extern "C" void AccessViolationException__ctor_m1_4569 ();
extern "C" void AccessViolationException__ctor_m1_4570 ();
extern "C" void ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m1_4571 ();
extern "C" void ActivationContext_Finalize_m1_4572 ();
extern "C" void ActivationContext_Dispose_m1_4573 ();
extern "C" void ActivationContext_Dispose_m1_4574 ();
extern "C" void Activator_CreateInstance_m1_4575 ();
extern "C" void Activator_CreateInstance_m1_4576 ();
extern "C" void Activator_CreateInstance_m1_4577 ();
extern "C" void Activator_CreateInstance_m1_4578 ();
extern "C" void Activator_CreateInstance_m1_4579 ();
extern "C" void Activator_CheckType_m1_4580 ();
extern "C" void Activator_CheckAbstractType_m1_4581 ();
extern "C" void Activator_CreateInstanceInternal_m1_4582 ();
extern "C" void AppDomain_add_UnhandledException_m1_4583 ();
extern "C" void AppDomain_remove_UnhandledException_m1_4584 ();
extern "C" void AppDomain_getFriendlyName_m1_4585 ();
extern "C" void AppDomain_getCurDomain_m1_4586 ();
extern "C" void AppDomain_get_CurrentDomain_m1_4587 ();
extern "C" void AppDomain_LoadAssembly_m1_4588 ();
extern "C" void AppDomain_Load_m1_4589 ();
extern "C" void AppDomain_Load_m1_4590 ();
extern "C" void AppDomain_InternalSetContext_m1_4591 ();
extern "C" void AppDomain_InternalGetContext_m1_4592 ();
extern "C" void AppDomain_InternalGetDefaultContext_m1_4593 ();
extern "C" void AppDomain_InternalGetProcessGuid_m1_4594 ();
extern "C" void AppDomain_GetProcessGuid_m1_4595 ();
extern "C" void AppDomain_ToString_m1_4596 ();
extern "C" void AppDomain_DoTypeResolve_m1_4597 ();
extern "C" void AppDomainSetup__ctor_m1_4598 ();
extern "C" void ApplicationException__ctor_m1_4599 ();
extern "C" void ApplicationException__ctor_m1_4600 ();
extern "C" void ApplicationException__ctor_m1_4601 ();
extern "C" void ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m1_4602 ();
extern "C" void ApplicationIdentity_ToString_m1_4603 ();
extern "C" void ArgumentException__ctor_m1_4604 ();
extern "C" void ArgumentException__ctor_m1_4605 ();
extern "C" void ArgumentException__ctor_m1_4606 ();
extern "C" void ArgumentException__ctor_m1_4607 ();
extern "C" void ArgumentException__ctor_m1_4608 ();
extern "C" void ArgumentException__ctor_m1_4609 ();
extern "C" void ArgumentException_get_ParamName_m1_4610 ();
extern "C" void ArgumentException_get_Message_m1_4611 ();
extern "C" void ArgumentException_GetObjectData_m1_4612 ();
extern "C" void ArgumentNullException__ctor_m1_4613 ();
extern "C" void ArgumentNullException__ctor_m1_4614 ();
extern "C" void ArgumentNullException__ctor_m1_4615 ();
extern "C" void ArgumentNullException__ctor_m1_4616 ();
extern "C" void ArgumentOutOfRangeException__ctor_m1_4617 ();
extern "C" void ArgumentOutOfRangeException__ctor_m1_4618 ();
extern "C" void ArgumentOutOfRangeException__ctor_m1_4619 ();
extern "C" void ArgumentOutOfRangeException__ctor_m1_4620 ();
extern "C" void ArgumentOutOfRangeException__ctor_m1_4621 ();
extern "C" void ArgumentOutOfRangeException_get_Message_m1_4622 ();
extern "C" void ArgumentOutOfRangeException_GetObjectData_m1_4623 ();
extern "C" void ArithmeticException__ctor_m1_4624 ();
extern "C" void ArithmeticException__ctor_m1_4625 ();
extern "C" void ArithmeticException__ctor_m1_4626 ();
extern "C" void ArrayTypeMismatchException__ctor_m1_4627 ();
extern "C" void ArrayTypeMismatchException__ctor_m1_4628 ();
extern "C" void ArrayTypeMismatchException__ctor_m1_4629 ();
extern "C" void BitConverter__cctor_m1_4630 ();
extern "C" void BitConverter_AmILittleEndian_m1_4631 ();
extern "C" void BitConverter_DoubleWordsAreSwapped_m1_4632 ();
extern "C" void BitConverter_DoubleToInt64Bits_m1_4633 ();
extern "C" void BitConverter_GetBytes_m1_4634 ();
extern "C" void BitConverter_GetBytes_m1_4635 ();
extern "C" void BitConverter_GetBytes_m1_4636 ();
extern "C" void BitConverter_PutBytes_m1_4637 ();
extern "C" void BitConverter_ToInt64_m1_4638 ();
extern "C" void BitConverter_ToUInt16_m1_4639 ();
extern "C" void BitConverter_ToUInt32_m1_4640 ();
extern "C" void BitConverter_ToUInt64_m1_4641 ();
extern "C" void BitConverter_ToSingle_m1_4642 ();
extern "C" void BitConverter_ToDouble_m1_4643 ();
extern "C" void BitConverter_ToString_m1_4644 ();
extern "C" void BitConverter_ToString_m1_4645 ();
extern "C" void Buffer_ByteLength_m1_4646 ();
extern "C" void Buffer_BlockCopy_m1_4647 ();
extern "C" void Buffer_ByteLengthInternal_m1_4648 ();
extern "C" void Buffer_BlockCopyInternal_m1_4649 ();
extern "C" void CharEnumerator__ctor_m1_4650 ();
extern "C" void CharEnumerator_System_Collections_IEnumerator_get_Current_m1_4651 ();
extern "C" void CharEnumerator_System_IDisposable_Dispose_m1_4652 ();
extern "C" void CharEnumerator_get_Current_m1_4653 ();
extern "C" void CharEnumerator_Clone_m1_4654 ();
extern "C" void CharEnumerator_MoveNext_m1_4655 ();
extern "C" void CharEnumerator_Reset_m1_4656 ();
extern "C" void Console__cctor_m1_4657 ();
extern "C" void Console_SetEncodings_m1_4658 ();
extern "C" void Console_get_Error_m1_4659 ();
extern "C" void Console_Open_m1_4660 ();
extern "C" void Console_OpenStandardError_m1_4661 ();
extern "C" void Console_OpenStandardInput_m1_4662 ();
extern "C" void Console_OpenStandardOutput_m1_4663 ();
extern "C" void Console_WriteLine_m1_4664 ();
extern "C" void Console_WriteLine_m1_4665 ();
extern "C" void ContextBoundObject__ctor_m1_4666 ();
extern "C" void Convert__cctor_m1_4667 ();
extern "C" void Convert_InternalFromBase64String_m1_4668 ();
extern "C" void Convert_FromBase64String_m1_4669 ();
extern "C" void Convert_ToBase64String_m1_4670 ();
extern "C" void Convert_ToBase64String_m1_4671 ();
extern "C" void Convert_ToBoolean_m1_4672 ();
extern "C" void Convert_ToBoolean_m1_4673 ();
extern "C" void Convert_ToBoolean_m1_4674 ();
extern "C" void Convert_ToBoolean_m1_4675 ();
extern "C" void Convert_ToBoolean_m1_4676 ();
extern "C" void Convert_ToBoolean_m1_4677 ();
extern "C" void Convert_ToBoolean_m1_4678 ();
extern "C" void Convert_ToBoolean_m1_4679 ();
extern "C" void Convert_ToBoolean_m1_4680 ();
extern "C" void Convert_ToBoolean_m1_4681 ();
extern "C" void Convert_ToBoolean_m1_4682 ();
extern "C" void Convert_ToBoolean_m1_4683 ();
extern "C" void Convert_ToBoolean_m1_4684 ();
extern "C" void Convert_ToBoolean_m1_4685 ();
extern "C" void Convert_ToByte_m1_4686 ();
extern "C" void Convert_ToByte_m1_4687 ();
extern "C" void Convert_ToByte_m1_4688 ();
extern "C" void Convert_ToByte_m1_4689 ();
extern "C" void Convert_ToByte_m1_4690 ();
extern "C" void Convert_ToByte_m1_4691 ();
extern "C" void Convert_ToByte_m1_4692 ();
extern "C" void Convert_ToByte_m1_4693 ();
extern "C" void Convert_ToByte_m1_4694 ();
extern "C" void Convert_ToByte_m1_4695 ();
extern "C" void Convert_ToByte_m1_4696 ();
extern "C" void Convert_ToByte_m1_4697 ();
extern "C" void Convert_ToByte_m1_4698 ();
extern "C" void Convert_ToByte_m1_4699 ();
extern "C" void Convert_ToByte_m1_4700 ();
extern "C" void Convert_ToChar_m1_4701 ();
extern "C" void Convert_ToChar_m1_4702 ();
extern "C" void Convert_ToChar_m1_4703 ();
extern "C" void Convert_ToChar_m1_4704 ();
extern "C" void Convert_ToChar_m1_4705 ();
extern "C" void Convert_ToChar_m1_4706 ();
extern "C" void Convert_ToChar_m1_4707 ();
extern "C" void Convert_ToChar_m1_4708 ();
extern "C" void Convert_ToChar_m1_4709 ();
extern "C" void Convert_ToChar_m1_4710 ();
extern "C" void Convert_ToChar_m1_4711 ();
extern "C" void Convert_ToDateTime_m1_4712 ();
extern "C" void Convert_ToDateTime_m1_4713 ();
extern "C" void Convert_ToDateTime_m1_4714 ();
extern "C" void Convert_ToDateTime_m1_4715 ();
extern "C" void Convert_ToDateTime_m1_4716 ();
extern "C" void Convert_ToDateTime_m1_4717 ();
extern "C" void Convert_ToDateTime_m1_4718 ();
extern "C" void Convert_ToDateTime_m1_4719 ();
extern "C" void Convert_ToDateTime_m1_4720 ();
extern "C" void Convert_ToDateTime_m1_4721 ();
extern "C" void Convert_ToDecimal_m1_4722 ();
extern "C" void Convert_ToDecimal_m1_4723 ();
extern "C" void Convert_ToDecimal_m1_4724 ();
extern "C" void Convert_ToDecimal_m1_4725 ();
extern "C" void Convert_ToDecimal_m1_4726 ();
extern "C" void Convert_ToDecimal_m1_4727 ();
extern "C" void Convert_ToDecimal_m1_4728 ();
extern "C" void Convert_ToDecimal_m1_4729 ();
extern "C" void Convert_ToDecimal_m1_4730 ();
extern "C" void Convert_ToDecimal_m1_4731 ();
extern "C" void Convert_ToDecimal_m1_4732 ();
extern "C" void Convert_ToDecimal_m1_4733 ();
extern "C" void Convert_ToDecimal_m1_4734 ();
extern "C" void Convert_ToDouble_m1_4735 ();
extern "C" void Convert_ToDouble_m1_4736 ();
extern "C" void Convert_ToDouble_m1_4737 ();
extern "C" void Convert_ToDouble_m1_4738 ();
extern "C" void Convert_ToDouble_m1_4739 ();
extern "C" void Convert_ToDouble_m1_4740 ();
extern "C" void Convert_ToDouble_m1_4741 ();
extern "C" void Convert_ToDouble_m1_4742 ();
extern "C" void Convert_ToDouble_m1_4743 ();
extern "C" void Convert_ToDouble_m1_4744 ();
extern "C" void Convert_ToDouble_m1_4745 ();
extern "C" void Convert_ToDouble_m1_4746 ();
extern "C" void Convert_ToDouble_m1_4747 ();
extern "C" void Convert_ToDouble_m1_4748 ();
extern "C" void Convert_ToInt16_m1_4749 ();
extern "C" void Convert_ToInt16_m1_4750 ();
extern "C" void Convert_ToInt16_m1_4751 ();
extern "C" void Convert_ToInt16_m1_4752 ();
extern "C" void Convert_ToInt16_m1_4753 ();
extern "C" void Convert_ToInt16_m1_4754 ();
extern "C" void Convert_ToInt16_m1_4755 ();
extern "C" void Convert_ToInt16_m1_4756 ();
extern "C" void Convert_ToInt16_m1_4757 ();
extern "C" void Convert_ToInt16_m1_4758 ();
extern "C" void Convert_ToInt16_m1_4759 ();
extern "C" void Convert_ToInt16_m1_4760 ();
extern "C" void Convert_ToInt16_m1_4761 ();
extern "C" void Convert_ToInt16_m1_4762 ();
extern "C" void Convert_ToInt16_m1_4763 ();
extern "C" void Convert_ToInt16_m1_4764 ();
extern "C" void Convert_ToInt32_m1_4765 ();
extern "C" void Convert_ToInt32_m1_4766 ();
extern "C" void Convert_ToInt32_m1_4767 ();
extern "C" void Convert_ToInt32_m1_4768 ();
extern "C" void Convert_ToInt32_m1_4769 ();
extern "C" void Convert_ToInt32_m1_4770 ();
extern "C" void Convert_ToInt32_m1_4771 ();
extern "C" void Convert_ToInt32_m1_4772 ();
extern "C" void Convert_ToInt32_m1_4773 ();
extern "C" void Convert_ToInt32_m1_4774 ();
extern "C" void Convert_ToInt32_m1_4775 ();
extern "C" void Convert_ToInt32_m1_4776 ();
extern "C" void Convert_ToInt32_m1_4777 ();
extern "C" void Convert_ToInt32_m1_4778 ();
extern "C" void Convert_ToInt32_m1_4779 ();
extern "C" void Convert_ToInt64_m1_4780 ();
extern "C" void Convert_ToInt64_m1_4781 ();
extern "C" void Convert_ToInt64_m1_4782 ();
extern "C" void Convert_ToInt64_m1_4783 ();
extern "C" void Convert_ToInt64_m1_4784 ();
extern "C" void Convert_ToInt64_m1_4785 ();
extern "C" void Convert_ToInt64_m1_4786 ();
extern "C" void Convert_ToInt64_m1_4787 ();
extern "C" void Convert_ToInt64_m1_4788 ();
extern "C" void Convert_ToInt64_m1_4789 ();
extern "C" void Convert_ToInt64_m1_4790 ();
extern "C" void Convert_ToInt64_m1_4791 ();
extern "C" void Convert_ToInt64_m1_4792 ();
extern "C" void Convert_ToInt64_m1_4793 ();
extern "C" void Convert_ToInt64_m1_4794 ();
extern "C" void Convert_ToInt64_m1_4795 ();
extern "C" void Convert_ToInt64_m1_4796 ();
extern "C" void Convert_ToSByte_m1_4797 ();
extern "C" void Convert_ToSByte_m1_4798 ();
extern "C" void Convert_ToSByte_m1_4799 ();
extern "C" void Convert_ToSByte_m1_4800 ();
extern "C" void Convert_ToSByte_m1_4801 ();
extern "C" void Convert_ToSByte_m1_4802 ();
extern "C" void Convert_ToSByte_m1_4803 ();
extern "C" void Convert_ToSByte_m1_4804 ();
extern "C" void Convert_ToSByte_m1_4805 ();
extern "C" void Convert_ToSByte_m1_4806 ();
extern "C" void Convert_ToSByte_m1_4807 ();
extern "C" void Convert_ToSByte_m1_4808 ();
extern "C" void Convert_ToSByte_m1_4809 ();
extern "C" void Convert_ToSByte_m1_4810 ();
extern "C" void Convert_ToSingle_m1_4811 ();
extern "C" void Convert_ToSingle_m1_4812 ();
extern "C" void Convert_ToSingle_m1_4813 ();
extern "C" void Convert_ToSingle_m1_4814 ();
extern "C" void Convert_ToSingle_m1_4815 ();
extern "C" void Convert_ToSingle_m1_4816 ();
extern "C" void Convert_ToSingle_m1_4817 ();
extern "C" void Convert_ToSingle_m1_4818 ();
extern "C" void Convert_ToSingle_m1_4819 ();
extern "C" void Convert_ToSingle_m1_4820 ();
extern "C" void Convert_ToSingle_m1_4821 ();
extern "C" void Convert_ToSingle_m1_4822 ();
extern "C" void Convert_ToSingle_m1_4823 ();
extern "C" void Convert_ToSingle_m1_4824 ();
extern "C" void Convert_ToString_m1_4825 ();
extern "C" void Convert_ToString_m1_4826 ();
extern "C" void Convert_ToString_m1_4827 ();
extern "C" void Convert_ToUInt16_m1_4828 ();
extern "C" void Convert_ToUInt16_m1_4829 ();
extern "C" void Convert_ToUInt16_m1_4830 ();
extern "C" void Convert_ToUInt16_m1_4831 ();
extern "C" void Convert_ToUInt16_m1_4832 ();
extern "C" void Convert_ToUInt16_m1_4833 ();
extern "C" void Convert_ToUInt16_m1_4834 ();
extern "C" void Convert_ToUInt16_m1_4835 ();
extern "C" void Convert_ToUInt16_m1_4836 ();
extern "C" void Convert_ToUInt16_m1_4837 ();
extern "C" void Convert_ToUInt16_m1_4838 ();
extern "C" void Convert_ToUInt16_m1_4839 ();
extern "C" void Convert_ToUInt16_m1_4840 ();
extern "C" void Convert_ToUInt16_m1_4841 ();
extern "C" void Convert_ToUInt32_m1_4842 ();
extern "C" void Convert_ToUInt32_m1_4843 ();
extern "C" void Convert_ToUInt32_m1_4844 ();
extern "C" void Convert_ToUInt32_m1_4845 ();
extern "C" void Convert_ToUInt32_m1_4846 ();
extern "C" void Convert_ToUInt32_m1_4847 ();
extern "C" void Convert_ToUInt32_m1_4848 ();
extern "C" void Convert_ToUInt32_m1_4849 ();
extern "C" void Convert_ToUInt32_m1_4850 ();
extern "C" void Convert_ToUInt32_m1_4851 ();
extern "C" void Convert_ToUInt32_m1_4852 ();
extern "C" void Convert_ToUInt32_m1_4853 ();
extern "C" void Convert_ToUInt32_m1_4854 ();
extern "C" void Convert_ToUInt32_m1_4855 ();
extern "C" void Convert_ToUInt32_m1_4856 ();
extern "C" void Convert_ToUInt64_m1_4857 ();
extern "C" void Convert_ToUInt64_m1_4858 ();
extern "C" void Convert_ToUInt64_m1_4859 ();
extern "C" void Convert_ToUInt64_m1_4860 ();
extern "C" void Convert_ToUInt64_m1_4861 ();
extern "C" void Convert_ToUInt64_m1_4862 ();
extern "C" void Convert_ToUInt64_m1_4863 ();
extern "C" void Convert_ToUInt64_m1_4864 ();
extern "C" void Convert_ToUInt64_m1_4865 ();
extern "C" void Convert_ToUInt64_m1_4866 ();
extern "C" void Convert_ToUInt64_m1_4867 ();
extern "C" void Convert_ToUInt64_m1_4868 ();
extern "C" void Convert_ToUInt64_m1_4869 ();
extern "C" void Convert_ToUInt64_m1_4870 ();
extern "C" void Convert_ToUInt64_m1_4871 ();
extern "C" void Convert_ChangeType_m1_4872 ();
extern "C" void Convert_EndianSwap_m1_4873 ();
extern "C" void Convert_ConvertToBase2_m1_4874 ();
extern "C" void Convert_ConvertToBase8_m1_4875 ();
extern "C" void Convert_ConvertToBase16_m1_4876 ();
extern "C" void Convert_ToType_m1_4877 ();
extern "C" void DBNull__ctor_m1_4878 ();
extern "C" void DBNull__ctor_m1_4879 ();
extern "C" void DBNull__cctor_m1_4880 ();
extern "C" void DBNull_System_IConvertible_ToBoolean_m1_4881 ();
extern "C" void DBNull_System_IConvertible_ToByte_m1_4882 ();
extern "C" void DBNull_System_IConvertible_ToChar_m1_4883 ();
extern "C" void DBNull_System_IConvertible_ToDateTime_m1_4884 ();
extern "C" void DBNull_System_IConvertible_ToDecimal_m1_4885 ();
extern "C" void DBNull_System_IConvertible_ToDouble_m1_4886 ();
extern "C" void DBNull_System_IConvertible_ToInt16_m1_4887 ();
extern "C" void DBNull_System_IConvertible_ToInt32_m1_4888 ();
extern "C" void DBNull_System_IConvertible_ToInt64_m1_4889 ();
extern "C" void DBNull_System_IConvertible_ToSByte_m1_4890 ();
extern "C" void DBNull_System_IConvertible_ToSingle_m1_4891 ();
extern "C" void DBNull_System_IConvertible_ToType_m1_4892 ();
extern "C" void DBNull_System_IConvertible_ToUInt16_m1_4893 ();
extern "C" void DBNull_System_IConvertible_ToUInt32_m1_4894 ();
extern "C" void DBNull_System_IConvertible_ToUInt64_m1_4895 ();
extern "C" void DBNull_GetObjectData_m1_4896 ();
extern "C" void DBNull_ToString_m1_4897 ();
extern "C" void DBNull_ToString_m1_4898 ();
extern "C" void DateTime__ctor_m1_4899 ();
extern "C" void DateTime__ctor_m1_4900 ();
extern "C" void DateTime__ctor_m1_4901 ();
extern "C" void DateTime__ctor_m1_4902 ();
extern "C" void DateTime__ctor_m1_4903 ();
extern "C" void DateTime__ctor_m1_4904 ();
extern "C" void DateTime__ctor_m1_4905 ();
extern "C" void DateTime__cctor_m1_4906 ();
extern "C" void DateTime_System_IConvertible_ToBoolean_m1_4907 ();
extern "C" void DateTime_System_IConvertible_ToByte_m1_4908 ();
extern "C" void DateTime_System_IConvertible_ToChar_m1_4909 ();
extern "C" void DateTime_System_IConvertible_ToDateTime_m1_4910 ();
extern "C" void DateTime_System_IConvertible_ToDecimal_m1_4911 ();
extern "C" void DateTime_System_IConvertible_ToDouble_m1_4912 ();
extern "C" void DateTime_System_IConvertible_ToInt16_m1_4913 ();
extern "C" void DateTime_System_IConvertible_ToInt32_m1_4914 ();
extern "C" void DateTime_System_IConvertible_ToInt64_m1_4915 ();
extern "C" void DateTime_System_IConvertible_ToSByte_m1_4916 ();
extern "C" void DateTime_System_IConvertible_ToSingle_m1_4917 ();
extern "C" void DateTime_System_IConvertible_ToType_m1_4918 ();
extern "C" void DateTime_System_IConvertible_ToUInt16_m1_4919 ();
extern "C" void DateTime_System_IConvertible_ToUInt32_m1_4920 ();
extern "C" void DateTime_System_IConvertible_ToUInt64_m1_4921 ();
extern "C" void DateTime_AbsoluteDays_m1_4922 ();
extern "C" void DateTime_FromTicks_m1_4923 ();
extern "C" void DateTime_get_Month_m1_4924 ();
extern "C" void DateTime_get_Day_m1_4925 ();
extern "C" void DateTime_get_DayOfWeek_m1_4926 ();
extern "C" void DateTime_get_Hour_m1_4927 ();
extern "C" void DateTime_get_Minute_m1_4928 ();
extern "C" void DateTime_get_Second_m1_4929 ();
extern "C" void DateTime_GetTimeMonotonic_m1_4930 ();
extern "C" void DateTime_GetNow_m1_4931 ();
extern "C" void DateTime_get_Now_m1_4932 ();
extern "C" void DateTime_get_Ticks_m1_4933 ();
extern "C" void DateTime_get_Today_m1_4934 ();
extern "C" void DateTime_get_UtcNow_m1_4935 ();
extern "C" void DateTime_get_Year_m1_4936 ();
extern "C" void DateTime_get_Kind_m1_4937 ();
extern "C" void DateTime_Add_m1_4938 ();
extern "C" void DateTime_AddTicks_m1_4939 ();
extern "C" void DateTime_AddMilliseconds_m1_4940 ();
extern "C" void DateTime_AddSeconds_m1_4941 ();
extern "C" void DateTime_Compare_m1_4942 ();
extern "C" void DateTime_CompareTo_m1_4943 ();
extern "C" void DateTime_CompareTo_m1_4944 ();
extern "C" void DateTime_Equals_m1_4945 ();
extern "C" void DateTime_FromBinary_m1_4946 ();
extern "C" void DateTime_SpecifyKind_m1_4947 ();
extern "C" void DateTime_DaysInMonth_m1_4948 ();
extern "C" void DateTime_Equals_m1_4949 ();
extern "C" void DateTime_CheckDateTimeKind_m1_4950 ();
extern "C" void DateTime_GetHashCode_m1_4951 ();
extern "C" void DateTime_IsLeapYear_m1_4952 ();
extern "C" void DateTime_Parse_m1_4953 ();
extern "C" void DateTime_Parse_m1_4954 ();
extern "C" void DateTime_CoreParse_m1_4955 ();
extern "C" void DateTime_YearMonthDayFormats_m1_4956 ();
extern "C" void DateTime__ParseNumber_m1_4957 ();
extern "C" void DateTime__ParseEnum_m1_4958 ();
extern "C" void DateTime__ParseString_m1_4959 ();
extern "C" void DateTime__ParseAmPm_m1_4960 ();
extern "C" void DateTime__ParseTimeSeparator_m1_4961 ();
extern "C" void DateTime__ParseDateSeparator_m1_4962 ();
extern "C" void DateTime_IsLetter_m1_4963 ();
extern "C" void DateTime__DoParse_m1_4964 ();
extern "C" void DateTime_ParseExact_m1_4965 ();
extern "C" void DateTime_ParseExact_m1_4966 ();
extern "C" void DateTime_CheckStyle_m1_4967 ();
extern "C" void DateTime_ParseExact_m1_4968 ();
extern "C" void DateTime_Subtract_m1_4969 ();
extern "C" void DateTime_ToString_m1_4970 ();
extern "C" void DateTime_ToString_m1_4971 ();
extern "C" void DateTime_ToString_m1_4972 ();
extern "C" void DateTime_ToLocalTime_m1_4973 ();
extern "C" void DateTime_ToUniversalTime_m1_4974 ();
extern "C" void DateTime_op_Addition_m1_4975 ();
extern "C" void DateTime_op_Equality_m1_4976 ();
extern "C" void DateTime_op_GreaterThan_m1_4977 ();
extern "C" void DateTime_op_GreaterThanOrEqual_m1_4978 ();
extern "C" void DateTime_op_Inequality_m1_4979 ();
extern "C" void DateTime_op_LessThan_m1_4980 ();
extern "C" void DateTime_op_LessThanOrEqual_m1_4981 ();
extern "C" void DateTime_op_Subtraction_m1_4982 ();
extern "C" void DateTimeOffset__ctor_m1_4983 ();
extern "C" void DateTimeOffset__ctor_m1_4984 ();
extern "C" void DateTimeOffset__ctor_m1_4985 ();
extern "C" void DateTimeOffset__ctor_m1_4986 ();
extern "C" void DateTimeOffset__cctor_m1_4987 ();
extern "C" void DateTimeOffset_System_IComparable_CompareTo_m1_4988 ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m1_4989 ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_4990 ();
extern "C" void DateTimeOffset_CompareTo_m1_4991 ();
extern "C" void DateTimeOffset_Equals_m1_4992 ();
extern "C" void DateTimeOffset_Equals_m1_4993 ();
extern "C" void DateTimeOffset_GetHashCode_m1_4994 ();
extern "C" void DateTimeOffset_ToString_m1_4995 ();
extern "C" void DateTimeOffset_ToString_m1_4996 ();
extern "C" void DateTimeOffset_get_DateTime_m1_4997 ();
extern "C" void DateTimeOffset_get_Offset_m1_4998 ();
extern "C" void DateTimeOffset_get_UtcDateTime_m1_4999 ();
extern "C" void DateTimeUtils_CountRepeat_m1_5000 ();
extern "C" void DateTimeUtils_ZeroPad_m1_5001 ();
extern "C" void DateTimeUtils_ParseQuotedString_m1_5002 ();
extern "C" void DateTimeUtils_GetStandardPattern_m1_5003 ();
extern "C" void DateTimeUtils_GetStandardPattern_m1_5004 ();
extern "C" void DateTimeUtils_ToString_m1_5005 ();
extern "C" void DateTimeUtils_ToString_m1_5006 ();
extern "C" void DelegateEntry__ctor_m1_5007 ();
extern "C" void DelegateEntry_DeserializeDelegate_m1_5008 ();
extern "C" void DelegateSerializationHolder__ctor_m1_5009 ();
extern "C" void DelegateSerializationHolder_GetDelegateData_m1_5010 ();
extern "C" void DelegateSerializationHolder_GetObjectData_m1_5011 ();
extern "C" void DelegateSerializationHolder_GetRealObject_m1_5012 ();
extern "C" void DivideByZeroException__ctor_m1_5013 ();
extern "C" void DivideByZeroException__ctor_m1_5014 ();
extern "C" void DllNotFoundException__ctor_m1_5015 ();
extern "C" void DllNotFoundException__ctor_m1_5016 ();
extern "C" void EntryPointNotFoundException__ctor_m1_5017 ();
extern "C" void EntryPointNotFoundException__ctor_m1_5018 ();
extern "C" void SByteComparer__ctor_m1_5019 ();
extern "C" void SByteComparer_Compare_m1_5020 ();
extern "C" void SByteComparer_Compare_m1_5021 ();
extern "C" void ShortComparer__ctor_m1_5022 ();
extern "C" void ShortComparer_Compare_m1_5023 ();
extern "C" void ShortComparer_Compare_m1_5024 ();
extern "C" void IntComparer__ctor_m1_5025 ();
extern "C" void IntComparer_Compare_m1_5026 ();
extern "C" void IntComparer_Compare_m1_5027 ();
extern "C" void LongComparer__ctor_m1_5028 ();
extern "C" void LongComparer_Compare_m1_5029 ();
extern "C" void LongComparer_Compare_m1_5030 ();
extern "C" void MonoEnumInfo__ctor_m1_5031 ();
extern "C" void MonoEnumInfo__cctor_m1_5032 ();
extern "C" void MonoEnumInfo_get_enum_info_m1_5033 ();
extern "C" void MonoEnumInfo_get_Cache_m1_5034 ();
extern "C" void MonoEnumInfo_GetInfo_m1_5035 ();
extern "C" void Environment_get_SocketSecurityEnabled_m1_5036 ();
extern "C" void Environment_get_NewLine_m1_5037 ();
extern "C" void Environment_get_Platform_m1_5038 ();
extern "C" void Environment_GetOSVersionString_m1_5039 ();
extern "C" void Environment_get_OSVersion_m1_5040 ();
extern "C" void Environment_get_TickCount_m1_5041 ();
extern "C" void Environment_internalGetEnvironmentVariable_m1_5042 ();
extern "C" void Environment_GetEnvironmentVariable_m1_5043 ();
extern "C" void Environment_GetWindowsFolderPath_m1_5044 ();
extern "C" void Environment_GetFolderPath_m1_5045 ();
extern "C" void Environment_ReadXdgUserDir_m1_5046 ();
extern "C" void Environment_InternalGetFolderPath_m1_5047 ();
extern "C" void Environment_get_IsRunningOnWindows_m1_5048 ();
extern "C" void Environment_GetMachineConfigPath_m1_5049 ();
extern "C" void Environment_internalGetHome_m1_5050 ();
extern "C" void EventArgs__ctor_m1_5051 ();
extern "C" void EventArgs__cctor_m1_5052 ();
extern "C" void ExecutionEngineException__ctor_m1_5053 ();
extern "C" void ExecutionEngineException__ctor_m1_5054 ();
extern "C" void FieldAccessException__ctor_m1_5055 ();
extern "C" void FieldAccessException__ctor_m1_5056 ();
extern "C" void FieldAccessException__ctor_m1_5057 ();
extern "C" void FlagsAttribute__ctor_m1_5058 ();
extern "C" void FormatException__ctor_m1_5059 ();
extern "C" void FormatException__ctor_m1_5060 ();
extern "C" void FormatException__ctor_m1_5061 ();
extern "C" void GC_SuppressFinalize_m1_5062 ();
extern "C" void Guid__ctor_m1_5063 ();
extern "C" void Guid__ctor_m1_5064 ();
extern "C" void Guid__cctor_m1_5065 ();
extern "C" void Guid_CheckNull_m1_5066 ();
extern "C" void Guid_CheckLength_m1_5067 ();
extern "C" void Guid_CheckArray_m1_5068 ();
extern "C" void Guid_Compare_m1_5069 ();
extern "C" void Guid_CompareTo_m1_5070 ();
extern "C" void Guid_Equals_m1_5071 ();
extern "C" void Guid_CompareTo_m1_5072 ();
extern "C" void Guid_Equals_m1_5073 ();
extern "C" void Guid_GetHashCode_m1_5074 ();
extern "C" void Guid_ToHex_m1_5075 ();
extern "C" void Guid_NewGuid_m1_5076 ();
extern "C" void Guid_AppendInt_m1_5077 ();
extern "C" void Guid_AppendShort_m1_5078 ();
extern "C" void Guid_AppendByte_m1_5079 ();
extern "C" void Guid_BaseToString_m1_5080 ();
extern "C" void Guid_ToString_m1_5081 ();
extern "C" void Guid_ToString_m1_5082 ();
extern "C" void Guid_ToString_m1_5083 ();
extern "C" void IndexOutOfRangeException__ctor_m1_5084 ();
extern "C" void IndexOutOfRangeException__ctor_m1_5085 ();
extern "C" void IndexOutOfRangeException__ctor_m1_5086 ();
extern "C" void InvalidCastException__ctor_m1_5087 ();
extern "C" void InvalidCastException__ctor_m1_5088 ();
extern "C" void InvalidCastException__ctor_m1_5089 ();
extern "C" void InvalidOperationException__ctor_m1_5090 ();
extern "C" void InvalidOperationException__ctor_m1_5091 ();
extern "C" void InvalidOperationException__ctor_m1_5092 ();
extern "C" void InvalidOperationException__ctor_m1_5093 ();
extern "C" void LocalDataStoreSlot__ctor_m1_5094 ();
extern "C" void LocalDataStoreSlot__cctor_m1_5095 ();
extern "C" void LocalDataStoreSlot_Finalize_m1_5096 ();
extern "C" void Math_Abs_m1_5097 ();
extern "C" void Math_Abs_m1_5098 ();
extern "C" void Math_Abs_m1_5099 ();
extern "C" void Math_Floor_m1_5100 ();
extern "C" void Math_Max_m1_5101 ();
extern "C" void Math_Min_m1_5102 ();
extern "C" void Math_Round_m1_5103 ();
extern "C" void Math_Round_m1_5104 ();
extern "C" void Math_Acos_m1_5105 ();
extern "C" void Math_Pow_m1_5106 ();
extern "C" void Math_Sqrt_m1_5107 ();
extern "C" void MemberAccessException__ctor_m1_5108 ();
extern "C" void MemberAccessException__ctor_m1_5109 ();
extern "C" void MemberAccessException__ctor_m1_5110 ();
extern "C" void MethodAccessException__ctor_m1_5111 ();
extern "C" void MethodAccessException__ctor_m1_5112 ();
extern "C" void MissingFieldException__ctor_m1_5113 ();
extern "C" void MissingFieldException__ctor_m1_5114 ();
extern "C" void MissingFieldException__ctor_m1_5115 ();
extern "C" void MissingFieldException_get_Message_m1_5116 ();
extern "C" void MissingMemberException__ctor_m1_5117 ();
extern "C" void MissingMemberException__ctor_m1_5118 ();
extern "C" void MissingMemberException__ctor_m1_5119 ();
extern "C" void MissingMemberException__ctor_m1_5120 ();
extern "C" void MissingMemberException_GetObjectData_m1_5121 ();
extern "C" void MissingMemberException_get_Message_m1_5122 ();
extern "C" void MissingMethodException__ctor_m1_5123 ();
extern "C" void MissingMethodException__ctor_m1_5124 ();
extern "C" void MissingMethodException__ctor_m1_5125 ();
extern "C" void MissingMethodException__ctor_m1_5126 ();
extern "C" void MissingMethodException_get_Message_m1_5127 ();
extern "C" void MonoAsyncCall__ctor_m1_5128 ();
extern "C" void AttributeInfo__ctor_m1_5129 ();
extern "C" void AttributeInfo_get_Usage_m1_5130 ();
extern "C" void AttributeInfo_get_InheritanceLevel_m1_5131 ();
extern "C" void MonoCustomAttrs__cctor_m1_5132 ();
extern "C" void MonoCustomAttrs_IsUserCattrProvider_m1_5133 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesInternal_m1_5134 ();
extern "C" void MonoCustomAttrs_GetPseudoCustomAttributes_m1_5135 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesBase_m1_5136 ();
extern "C" void MonoCustomAttrs_GetCustomAttribute_m1_5137 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m1_5138 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m1_5139 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesDataInternal_m1_5140 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesData_m1_5141 ();
extern "C" void MonoCustomAttrs_IsDefined_m1_5142 ();
extern "C" void MonoCustomAttrs_IsDefinedInternal_m1_5143 ();
extern "C" void MonoCustomAttrs_GetBasePropertyDefinition_m1_5144 ();
extern "C" void MonoCustomAttrs_GetBase_m1_5145 ();
extern "C" void MonoCustomAttrs_RetrieveAttributeUsage_m1_5146 ();
extern "C" void MonoTouchAOTHelper__cctor_m1_5147 ();
extern "C" void MonoTypeInfo__ctor_m1_5148 ();
extern "C" void MonoType_get_attributes_m1_5149 ();
extern "C" void MonoType_GetDefaultConstructor_m1_5150 ();
extern "C" void MonoType_GetAttributeFlagsImpl_m1_5151 ();
extern "C" void MonoType_GetConstructorImpl_m1_5152 ();
extern "C" void MonoType_GetConstructors_internal_m1_5153 ();
extern "C" void MonoType_GetConstructors_m1_5154 ();
extern "C" void MonoType_InternalGetEvent_m1_5155 ();
extern "C" void MonoType_GetEvent_m1_5156 ();
extern "C" void MonoType_GetField_m1_5157 ();
extern "C" void MonoType_GetFields_internal_m1_5158 ();
extern "C" void MonoType_GetFields_m1_5159 ();
extern "C" void MonoType_GetInterfaces_m1_5160 ();
extern "C" void MonoType_GetMethodsByName_m1_5161 ();
extern "C" void MonoType_GetMethods_m1_5162 ();
extern "C" void MonoType_GetMethodImpl_m1_5163 ();
extern "C" void MonoType_GetPropertiesByName_m1_5164 ();
extern "C" void MonoType_GetPropertyImpl_m1_5165 ();
extern "C" void MonoType_HasElementTypeImpl_m1_5166 ();
extern "C" void MonoType_IsArrayImpl_m1_5167 ();
extern "C" void MonoType_IsByRefImpl_m1_5168 ();
extern "C" void MonoType_IsPointerImpl_m1_5169 ();
extern "C" void MonoType_IsPrimitiveImpl_m1_5170 ();
extern "C" void MonoType_IsSubclassOf_m1_5171 ();
extern "C" void MonoType_InvokeMember_m1_5172 ();
extern "C" void MonoType_GetElementType_m1_5173 ();
extern "C" void MonoType_get_UnderlyingSystemType_m1_5174 ();
extern "C" void MonoType_get_Assembly_m1_5175 ();
extern "C" void MonoType_get_AssemblyQualifiedName_m1_5176 ();
extern "C" void MonoType_getFullName_m1_5177 ();
extern "C" void MonoType_get_BaseType_m1_5178 ();
extern "C" void MonoType_get_FullName_m1_5179 ();
extern "C" void MonoType_IsDefined_m1_5180 ();
extern "C" void MonoType_GetCustomAttributes_m1_5181 ();
extern "C" void MonoType_GetCustomAttributes_m1_5182 ();
extern "C" void MonoType_get_MemberType_m1_5183 ();
extern "C" void MonoType_get_Name_m1_5184 ();
extern "C" void MonoType_get_Namespace_m1_5185 ();
extern "C" void MonoType_get_Module_m1_5186 ();
extern "C" void MonoType_get_DeclaringType_m1_5187 ();
extern "C" void MonoType_get_ReflectedType_m1_5188 ();
extern "C" void MonoType_get_TypeHandle_m1_5189 ();
extern "C" void MonoType_GetObjectData_m1_5190 ();
extern "C" void MonoType_ToString_m1_5191 ();
extern "C" void MonoType_GetGenericArguments_m1_5192 ();
extern "C" void MonoType_get_ContainsGenericParameters_m1_5193 ();
extern "C" void MonoType_get_IsGenericParameter_m1_5194 ();
extern "C" void MonoType_GetGenericTypeDefinition_m1_5195 ();
extern "C" void MonoType_CheckMethodSecurity_m1_5196 ();
extern "C" void MonoType_ReorderParamArrayArguments_m1_5197 ();
extern "C" void MulticastNotSupportedException__ctor_m1_5198 ();
extern "C" void MulticastNotSupportedException__ctor_m1_5199 ();
extern "C" void MulticastNotSupportedException__ctor_m1_5200 ();
extern "C" void NonSerializedAttribute__ctor_m1_5201 ();
extern "C" void NotImplementedException__ctor_m1_5202 ();
extern "C" void NotImplementedException__ctor_m1_5203 ();
extern "C" void NotImplementedException__ctor_m1_5204 ();
extern "C" void NotSupportedException__ctor_m1_5205 ();
extern "C" void NotSupportedException__ctor_m1_5206 ();
extern "C" void NotSupportedException__ctor_m1_5207 ();
extern "C" void NullReferenceException__ctor_m1_5208 ();
extern "C" void NullReferenceException__ctor_m1_5209 ();
extern "C" void NullReferenceException__ctor_m1_5210 ();
extern "C" void CustomInfo__ctor_m1_5211 ();
extern "C" void CustomInfo_GetActiveSection_m1_5212 ();
extern "C" void CustomInfo_Parse_m1_5213 ();
extern "C" void CustomInfo_Format_m1_5214 ();
extern "C" void NumberFormatter__ctor_m1_5215 ();
extern "C" void NumberFormatter__cctor_m1_5216 ();
extern "C" void NumberFormatter_GetFormatterTables_m1_5217 ();
extern "C" void NumberFormatter_GetTenPowerOf_m1_5218 ();
extern "C" void NumberFormatter_InitDecHexDigits_m1_5219 ();
extern "C" void NumberFormatter_InitDecHexDigits_m1_5220 ();
extern "C" void NumberFormatter_InitDecHexDigits_m1_5221 ();
extern "C" void NumberFormatter_FastToDecHex_m1_5222 ();
extern "C" void NumberFormatter_ToDecHex_m1_5223 ();
extern "C" void NumberFormatter_FastDecHexLen_m1_5224 ();
extern "C" void NumberFormatter_DecHexLen_m1_5225 ();
extern "C" void NumberFormatter_DecHexLen_m1_5226 ();
extern "C" void NumberFormatter_ScaleOrder_m1_5227 ();
extern "C" void NumberFormatter_InitialFloatingPrecision_m1_5228 ();
extern "C" void NumberFormatter_ParsePrecision_m1_5229 ();
extern "C" void NumberFormatter_Init_m1_5230 ();
extern "C" void NumberFormatter_InitHex_m1_5231 ();
extern "C" void NumberFormatter_Init_m1_5232 ();
extern "C" void NumberFormatter_Init_m1_5233 ();
extern "C" void NumberFormatter_Init_m1_5234 ();
extern "C" void NumberFormatter_Init_m1_5235 ();
extern "C" void NumberFormatter_Init_m1_5236 ();
extern "C" void NumberFormatter_Init_m1_5237 ();
extern "C" void NumberFormatter_ResetCharBuf_m1_5238 ();
extern "C" void NumberFormatter_Resize_m1_5239 ();
extern "C" void NumberFormatter_Append_m1_5240 ();
extern "C" void NumberFormatter_Append_m1_5241 ();
extern "C" void NumberFormatter_Append_m1_5242 ();
extern "C" void NumberFormatter_GetNumberFormatInstance_m1_5243 ();
extern "C" void NumberFormatter_set_CurrentCulture_m1_5244 ();
extern "C" void NumberFormatter_get_IntegerDigits_m1_5245 ();
extern "C" void NumberFormatter_get_DecimalDigits_m1_5246 ();
extern "C" void NumberFormatter_get_IsFloatingSource_m1_5247 ();
extern "C" void NumberFormatter_get_IsZero_m1_5248 ();
extern "C" void NumberFormatter_get_IsZeroInteger_m1_5249 ();
extern "C" void NumberFormatter_RoundPos_m1_5250 ();
extern "C" void NumberFormatter_RoundDecimal_m1_5251 ();
extern "C" void NumberFormatter_RoundBits_m1_5252 ();
extern "C" void NumberFormatter_RemoveTrailingZeros_m1_5253 ();
extern "C" void NumberFormatter_AddOneToDecHex_m1_5254 ();
extern "C" void NumberFormatter_AddOneToDecHex_m1_5255 ();
extern "C" void NumberFormatter_CountTrailingZeros_m1_5256 ();
extern "C" void NumberFormatter_CountTrailingZeros_m1_5257 ();
extern "C" void NumberFormatter_GetInstance_m1_5258 ();
extern "C" void NumberFormatter_Release_m1_5259 ();
extern "C" void NumberFormatter_SetThreadCurrentCulture_m1_5260 ();
extern "C" void NumberFormatter_NumberToString_m1_5261 ();
extern "C" void NumberFormatter_NumberToString_m1_5262 ();
extern "C" void NumberFormatter_NumberToString_m1_5263 ();
extern "C" void NumberFormatter_NumberToString_m1_5264 ();
extern "C" void NumberFormatter_NumberToString_m1_5265 ();
extern "C" void NumberFormatter_NumberToString_m1_5266 ();
extern "C" void NumberFormatter_NumberToString_m1_5267 ();
extern "C" void NumberFormatter_NumberToString_m1_5268 ();
extern "C" void NumberFormatter_NumberToString_m1_5269 ();
extern "C" void NumberFormatter_NumberToString_m1_5270 ();
extern "C" void NumberFormatter_NumberToString_m1_5271 ();
extern "C" void NumberFormatter_NumberToString_m1_5272 ();
extern "C" void NumberFormatter_NumberToString_m1_5273 ();
extern "C" void NumberFormatter_NumberToString_m1_5274 ();
extern "C" void NumberFormatter_NumberToString_m1_5275 ();
extern "C" void NumberFormatter_NumberToString_m1_5276 ();
extern "C" void NumberFormatter_NumberToString_m1_5277 ();
extern "C" void NumberFormatter_FastIntegerToString_m1_5278 ();
extern "C" void NumberFormatter_IntegerToString_m1_5279 ();
extern "C" void NumberFormatter_NumberToString_m1_5280 ();
extern "C" void NumberFormatter_FormatCurrency_m1_5281 ();
extern "C" void NumberFormatter_FormatDecimal_m1_5282 ();
extern "C" void NumberFormatter_FormatHexadecimal_m1_5283 ();
extern "C" void NumberFormatter_FormatFixedPoint_m1_5284 ();
extern "C" void NumberFormatter_FormatRoundtrip_m1_5285 ();
extern "C" void NumberFormatter_FormatRoundtrip_m1_5286 ();
extern "C" void NumberFormatter_FormatGeneral_m1_5287 ();
extern "C" void NumberFormatter_FormatNumber_m1_5288 ();
extern "C" void NumberFormatter_FormatPercent_m1_5289 ();
extern "C" void NumberFormatter_FormatExponential_m1_5290 ();
extern "C" void NumberFormatter_FormatExponential_m1_5291 ();
extern "C" void NumberFormatter_FormatCustom_m1_5292 ();
extern "C" void NumberFormatter_ZeroTrimEnd_m1_5293 ();
extern "C" void NumberFormatter_IsZeroOnly_m1_5294 ();
extern "C" void NumberFormatter_AppendNonNegativeNumber_m1_5295 ();
extern "C" void NumberFormatter_AppendIntegerString_m1_5296 ();
extern "C" void NumberFormatter_AppendIntegerString_m1_5297 ();
extern "C" void NumberFormatter_AppendDecimalString_m1_5298 ();
extern "C" void NumberFormatter_AppendDecimalString_m1_5299 ();
extern "C" void NumberFormatter_AppendIntegerStringWithGroupSeparator_m1_5300 ();
extern "C" void NumberFormatter_AppendExponent_m1_5301 ();
extern "C" void NumberFormatter_AppendOneDigit_m1_5302 ();
extern "C" void NumberFormatter_FastAppendDigits_m1_5303 ();
extern "C" void NumberFormatter_AppendDigits_m1_5304 ();
extern "C" void NumberFormatter_AppendDigits_m1_5305 ();
extern "C" void NumberFormatter_Multiply10_m1_5306 ();
extern "C" void NumberFormatter_Divide10_m1_5307 ();
extern "C" void NumberFormatter_GetClone_m1_5308 ();
extern "C" void ObjectDisposedException__ctor_m1_5309 ();
extern "C" void ObjectDisposedException__ctor_m1_5310 ();
extern "C" void ObjectDisposedException__ctor_m1_5311 ();
extern "C" void ObjectDisposedException_get_Message_m1_5312 ();
extern "C" void ObjectDisposedException_GetObjectData_m1_5313 ();
extern "C" void OperatingSystem__ctor_m1_5314 ();
extern "C" void OperatingSystem_get_Platform_m1_5315 ();
extern "C" void OperatingSystem_Clone_m1_5316 ();
extern "C" void OperatingSystem_GetObjectData_m1_5317 ();
extern "C" void OperatingSystem_ToString_m1_5318 ();
extern "C" void OutOfMemoryException__ctor_m1_5319 ();
extern "C" void OutOfMemoryException__ctor_m1_5320 ();
extern "C" void OverflowException__ctor_m1_5321 ();
extern "C" void OverflowException__ctor_m1_5322 ();
extern "C" void OverflowException__ctor_m1_5323 ();
extern "C" void Random__ctor_m1_5324 ();
extern "C" void Random__ctor_m1_5325 ();
extern "C" void Random_Sample_m1_5326 ();
extern "C" void Random_Next_m1_5327 ();
extern "C" void Random_Next_m1_5328 ();
extern "C" void Random_NextDouble_m1_5329 ();
extern "C" void RankException__ctor_m1_5330 ();
extern "C" void RankException__ctor_m1_5331 ();
extern "C" void RankException__ctor_m1_5332 ();
extern "C" void ResolveEventArgs__ctor_m1_5333 ();
extern "C" void RuntimeMethodHandle__ctor_m1_5334 ();
extern "C" void RuntimeMethodHandle__ctor_m1_5335 ();
extern "C" void RuntimeMethodHandle_get_Value_m1_5336 ();
extern "C" void RuntimeMethodHandle_GetObjectData_m1_5337 ();
extern "C" void RuntimeMethodHandle_Equals_m1_5338 ();
extern "C" void RuntimeMethodHandle_GetHashCode_m1_5339 ();
extern "C" void StringComparer__ctor_m1_5340 ();
extern "C" void StringComparer__cctor_m1_5341 ();
extern "C" void StringComparer_get_InvariantCultureIgnoreCase_m1_5342 ();
extern "C" void StringComparer_get_OrdinalIgnoreCase_m1_5343 ();
extern "C" void StringComparer_Compare_m1_5344 ();
extern "C" void StringComparer_Equals_m1_5345 ();
extern "C" void StringComparer_GetHashCode_m1_5346 ();
extern "C" void CultureAwareComparer__ctor_m1_5347 ();
extern "C" void CultureAwareComparer_Compare_m1_5348 ();
extern "C" void CultureAwareComparer_Equals_m1_5349 ();
extern "C" void CultureAwareComparer_GetHashCode_m1_5350 ();
extern "C" void OrdinalComparer__ctor_m1_5351 ();
extern "C" void OrdinalComparer_Compare_m1_5352 ();
extern "C" void OrdinalComparer_Equals_m1_5353 ();
extern "C" void OrdinalComparer_GetHashCode_m1_5354 ();
extern "C" void SystemException__ctor_m1_5355 ();
extern "C" void SystemException__ctor_m1_5356 ();
extern "C" void SystemException__ctor_m1_5357 ();
extern "C" void SystemException__ctor_m1_5358 ();
extern "C" void ThreadStaticAttribute__ctor_m1_5359 ();
extern "C" void TimeSpan__ctor_m1_5360 ();
extern "C" void TimeSpan__ctor_m1_5361 ();
extern "C" void TimeSpan__ctor_m1_5362 ();
extern "C" void TimeSpan__cctor_m1_5363 ();
extern "C" void TimeSpan_CalculateTicks_m1_5364 ();
extern "C" void TimeSpan_get_Days_m1_5365 ();
extern "C" void TimeSpan_get_Hours_m1_5366 ();
extern "C" void TimeSpan_get_Milliseconds_m1_5367 ();
extern "C" void TimeSpan_get_Minutes_m1_5368 ();
extern "C" void TimeSpan_get_Seconds_m1_5369 ();
extern "C" void TimeSpan_get_Ticks_m1_5370 ();
extern "C" void TimeSpan_get_TotalDays_m1_5371 ();
extern "C" void TimeSpan_get_TotalHours_m1_5372 ();
extern "C" void TimeSpan_get_TotalMilliseconds_m1_5373 ();
extern "C" void TimeSpan_get_TotalMinutes_m1_5374 ();
extern "C" void TimeSpan_get_TotalSeconds_m1_5375 ();
extern "C" void TimeSpan_Add_m1_5376 ();
extern "C" void TimeSpan_Compare_m1_5377 ();
extern "C" void TimeSpan_CompareTo_m1_5378 ();
extern "C" void TimeSpan_CompareTo_m1_5379 ();
extern "C" void TimeSpan_Equals_m1_5380 ();
extern "C" void TimeSpan_Duration_m1_5381 ();
extern "C" void TimeSpan_Equals_m1_5382 ();
extern "C" void TimeSpan_FromDays_m1_5383 ();
extern "C" void TimeSpan_FromHours_m1_5384 ();
extern "C" void TimeSpan_FromMinutes_m1_5385 ();
extern "C" void TimeSpan_FromSeconds_m1_5386 ();
extern "C" void TimeSpan_FromMilliseconds_m1_5387 ();
extern "C" void TimeSpan_From_m1_5388 ();
extern "C" void TimeSpan_FromTicks_m1_5389 ();
extern "C" void TimeSpan_GetHashCode_m1_5390 ();
extern "C" void TimeSpan_Negate_m1_5391 ();
extern "C" void TimeSpan_Subtract_m1_5392 ();
extern "C" void TimeSpan_ToString_m1_5393 ();
extern "C" void TimeSpan_op_Addition_m1_5394 ();
extern "C" void TimeSpan_op_Equality_m1_5395 ();
extern "C" void TimeSpan_op_GreaterThan_m1_5396 ();
extern "C" void TimeSpan_op_GreaterThanOrEqual_m1_5397 ();
extern "C" void TimeSpan_op_Inequality_m1_5398 ();
extern "C" void TimeSpan_op_LessThan_m1_5399 ();
extern "C" void TimeSpan_op_LessThanOrEqual_m1_5400 ();
extern "C" void TimeSpan_op_Subtraction_m1_5401 ();
extern "C" void TimeZone__ctor_m1_5402 ();
extern "C" void TimeZone__cctor_m1_5403 ();
extern "C" void TimeZone_get_CurrentTimeZone_m1_5404 ();
extern "C" void TimeZone_IsDaylightSavingTime_m1_5405 ();
extern "C" void TimeZone_IsDaylightSavingTime_m1_5406 ();
extern "C" void TimeZone_ToLocalTime_m1_5407 ();
extern "C" void TimeZone_ToUniversalTime_m1_5408 ();
extern "C" void TimeZone_GetLocalTimeDiff_m1_5409 ();
extern "C" void TimeZone_GetLocalTimeDiff_m1_5410 ();
extern "C" void CurrentSystemTimeZone__ctor_m1_5411 ();
extern "C" void CurrentSystemTimeZone__ctor_m1_5412 ();
extern "C" void CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_5413 ();
extern "C" void CurrentSystemTimeZone_GetTimeZoneData_m1_5414 ();
extern "C" void CurrentSystemTimeZone_GetDaylightChanges_m1_5415 ();
extern "C" void CurrentSystemTimeZone_GetUtcOffset_m1_5416 ();
extern "C" void CurrentSystemTimeZone_OnDeserialization_m1_5417 ();
extern "C" void CurrentSystemTimeZone_GetDaylightTimeFromData_m1_5418 ();
extern "C" void TypeInitializationException__ctor_m1_5419 ();
extern "C" void TypeInitializationException_GetObjectData_m1_5420 ();
extern "C" void TypeLoadException__ctor_m1_5421 ();
extern "C" void TypeLoadException__ctor_m1_5422 ();
extern "C" void TypeLoadException__ctor_m1_5423 ();
extern "C" void TypeLoadException_get_Message_m1_5424 ();
extern "C" void TypeLoadException_GetObjectData_m1_5425 ();
extern "C" void UnauthorizedAccessException__ctor_m1_5426 ();
extern "C" void UnauthorizedAccessException__ctor_m1_5427 ();
extern "C" void UnauthorizedAccessException__ctor_m1_5428 ();
extern "C" void UnhandledExceptionEventArgs__ctor_m1_5429 ();
extern "C" void UnhandledExceptionEventArgs_get_ExceptionObject_m1_5430 ();
extern "C" void UnhandledExceptionEventArgs_get_IsTerminating_m1_5431 ();
extern "C" void UnitySerializationHolder__ctor_m1_5432 ();
extern "C" void UnitySerializationHolder_GetTypeData_m1_5433 ();
extern "C" void UnitySerializationHolder_GetDBNullData_m1_5434 ();
extern "C" void UnitySerializationHolder_GetModuleData_m1_5435 ();
extern "C" void UnitySerializationHolder_GetObjectData_m1_5436 ();
extern "C" void UnitySerializationHolder_GetRealObject_m1_5437 ();
extern "C" void Version__ctor_m1_5438 ();
extern "C" void Version__ctor_m1_5439 ();
extern "C" void Version__ctor_m1_5440 ();
extern "C" void Version__ctor_m1_5441 ();
extern "C" void Version__ctor_m1_5442 ();
extern "C" void Version_CheckedSet_m1_5443 ();
extern "C" void Version_get_Build_m1_5444 ();
extern "C" void Version_get_Major_m1_5445 ();
extern "C" void Version_get_Minor_m1_5446 ();
extern "C" void Version_get_Revision_m1_5447 ();
extern "C" void Version_Clone_m1_5448 ();
extern "C" void Version_CompareTo_m1_5449 ();
extern "C" void Version_Equals_m1_5450 ();
extern "C" void Version_CompareTo_m1_5451 ();
extern "C" void Version_Equals_m1_5452 ();
extern "C" void Version_GetHashCode_m1_5453 ();
extern "C" void Version_ToString_m1_5454 ();
extern "C" void Version_CreateFromString_m1_5455 ();
extern "C" void Version_op_Equality_m1_5456 ();
extern "C" void Version_op_Inequality_m1_5457 ();
extern "C" void WeakReference__ctor_m1_5458 ();
extern "C" void WeakReference__ctor_m1_5459 ();
extern "C" void WeakReference__ctor_m1_5460 ();
extern "C" void WeakReference__ctor_m1_5461 ();
extern "C" void WeakReference_AllocateHandle_m1_5462 ();
extern "C" void WeakReference_get_Target_m1_5463 ();
extern "C" void WeakReference_get_TrackResurrection_m1_5464 ();
extern "C" void WeakReference_Finalize_m1_5465 ();
extern "C" void WeakReference_GetObjectData_m1_5466 ();
extern "C" void PrimalityTest__ctor_m1_5467 ();
extern "C" void PrimalityTest_Invoke_m1_5468 ();
extern "C" void PrimalityTest_BeginInvoke_m1_5469 ();
extern "C" void PrimalityTest_EndInvoke_m1_5470 ();
extern "C" void MemberFilter__ctor_m1_5471 ();
extern "C" void MemberFilter_Invoke_m1_5472 ();
extern "C" void MemberFilter_BeginInvoke_m1_5473 ();
extern "C" void MemberFilter_EndInvoke_m1_5474 ();
extern "C" void TypeFilter__ctor_m1_5475 ();
extern "C" void TypeFilter_Invoke_m1_5476 ();
extern "C" void TypeFilter_BeginInvoke_m1_5477 ();
extern "C" void TypeFilter_EndInvoke_m1_5478 ();
extern "C" void CrossContextDelegate__ctor_m1_5479 ();
extern "C" void CrossContextDelegate_Invoke_m1_5480 ();
extern "C" void CrossContextDelegate_BeginInvoke_m1_5481 ();
extern "C" void CrossContextDelegate_EndInvoke_m1_5482 ();
extern "C" void HeaderHandler__ctor_m1_5483 ();
extern "C" void HeaderHandler_Invoke_m1_5484 ();
extern "C" void HeaderHandler_BeginInvoke_m1_5485 ();
extern "C" void HeaderHandler_EndInvoke_m1_5486 ();
extern "C" void ThreadStart__ctor_m1_5487 ();
extern "C" void ThreadStart_Invoke_m1_5488 ();
extern "C" void ThreadStart_BeginInvoke_m1_5489 ();
extern "C" void ThreadStart_EndInvoke_m1_5490 ();
extern "C" void TimerCallback__ctor_m1_5491 ();
extern "C" void TimerCallback_Invoke_m1_5492 ();
extern "C" void TimerCallback_BeginInvoke_m1_5493 ();
extern "C" void TimerCallback_EndInvoke_m1_5494 ();
extern "C" void WaitCallback__ctor_m1_5495 ();
extern "C" void WaitCallback_Invoke_m1_5496 ();
extern "C" void WaitCallback_BeginInvoke_m1_5497 ();
extern "C" void WaitCallback_EndInvoke_m1_5498 ();
extern "C" void AppDomainInitializer__ctor_m1_5499 ();
extern "C" void AppDomainInitializer_Invoke_m1_5500 ();
extern "C" void AppDomainInitializer_BeginInvoke_m1_5501 ();
extern "C" void AppDomainInitializer_EndInvoke_m1_5502 ();
extern "C" void AssemblyLoadEventHandler__ctor_m1_5503 ();
extern "C" void AssemblyLoadEventHandler_Invoke_m1_5504 ();
extern "C" void AssemblyLoadEventHandler_BeginInvoke_m1_5505 ();
extern "C" void AssemblyLoadEventHandler_EndInvoke_m1_5506 ();
extern "C" void EventHandler__ctor_m1_5507 ();
extern "C" void EventHandler_Invoke_m1_5508 ();
extern "C" void EventHandler_BeginInvoke_m1_5509 ();
extern "C" void EventHandler_EndInvoke_m1_5510 ();
extern "C" void ResolveEventHandler__ctor_m1_5511 ();
extern "C" void ResolveEventHandler_Invoke_m1_5512 ();
extern "C" void ResolveEventHandler_BeginInvoke_m1_5513 ();
extern "C" void ResolveEventHandler_EndInvoke_m1_5514 ();
extern "C" void UnhandledExceptionEventHandler__ctor_m1_5515 ();
extern "C" void UnhandledExceptionEventHandler_Invoke_m1_5516 ();
extern "C" void UnhandledExceptionEventHandler_BeginInvoke_m1_5517 ();
extern "C" void UnhandledExceptionEventHandler_EndInvoke_m1_5518 ();
extern "C" void ExtensionAttribute__ctor_m2_0 ();
extern "C" void Locale_GetText_m2_1 ();
extern "C" void Locale_GetText_m2_2 ();
extern "C" void MonoTODOAttribute__ctor_m2_3 ();
extern "C" void KeyBuilder_get_Rng_m2_4 ();
extern "C" void KeyBuilder_Key_m2_5 ();
extern "C" void KeyBuilder_IV_m2_6 ();
extern "C" void SymmetricTransform__ctor_m2_7 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m2_8 ();
extern "C" void SymmetricTransform_Finalize_m2_9 ();
extern "C" void SymmetricTransform_Dispose_m2_10 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m2_11 ();
extern "C" void SymmetricTransform_Transform_m2_12 ();
extern "C" void SymmetricTransform_CBC_m2_13 ();
extern "C" void SymmetricTransform_CFB_m2_14 ();
extern "C" void SymmetricTransform_OFB_m2_15 ();
extern "C" void SymmetricTransform_CTS_m2_16 ();
extern "C" void SymmetricTransform_CheckInput_m2_17 ();
extern "C" void SymmetricTransform_TransformBlock_m2_18 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m2_19 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m2_20 ();
extern "C" void SymmetricTransform_Random_m2_21 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m2_22 ();
extern "C" void SymmetricTransform_FinalEncrypt_m2_23 ();
extern "C" void SymmetricTransform_FinalDecrypt_m2_24 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m2_25 ();
extern "C" void Aes__ctor_m2_26 ();
extern "C" void AesManaged__ctor_m2_27 ();
extern "C" void AesManaged_GenerateIV_m2_28 ();
extern "C" void AesManaged_GenerateKey_m2_29 ();
extern "C" void AesManaged_CreateDecryptor_m2_30 ();
extern "C" void AesManaged_CreateEncryptor_m2_31 ();
extern "C" void AesManaged_get_IV_m2_32 ();
extern "C" void AesManaged_set_IV_m2_33 ();
extern "C" void AesManaged_get_Key_m2_34 ();
extern "C" void AesManaged_set_Key_m2_35 ();
extern "C" void AesManaged_get_KeySize_m2_36 ();
extern "C" void AesManaged_set_KeySize_m2_37 ();
extern "C" void AesManaged_CreateDecryptor_m2_38 ();
extern "C" void AesManaged_CreateEncryptor_m2_39 ();
extern "C" void AesManaged_Dispose_m2_40 ();
extern "C" void AesTransform__ctor_m2_41 ();
extern "C" void AesTransform__cctor_m2_42 ();
extern "C" void AesTransform_ECB_m2_43 ();
extern "C" void AesTransform_SubByte_m2_44 ();
extern "C" void AesTransform_Encrypt128_m2_45 ();
extern "C" void AesTransform_Decrypt128_m2_46 ();
extern "C" void Locale_GetText_m3_0 ();
extern "C" void Locale_GetText_m3_1 ();
extern "C" void MonoTODOAttribute__ctor_m3_2 ();
extern "C" void MonoTODOAttribute__ctor_m3_3 ();
extern "C" void HybridDictionary__ctor_m3_4 ();
extern "C" void HybridDictionary__ctor_m3_5 ();
extern "C" void HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m3_6 ();
extern "C" void HybridDictionary_get_inner_m3_7 ();
extern "C" void HybridDictionary_get_Count_m3_8 ();
extern "C" void HybridDictionary_get_Item_m3_9 ();
extern "C" void HybridDictionary_set_Item_m3_10 ();
extern "C" void HybridDictionary_get_Keys_m3_11 ();
extern "C" void HybridDictionary_get_SyncRoot_m3_12 ();
extern "C" void HybridDictionary_Add_m3_13 ();
extern "C" void HybridDictionary_Contains_m3_14 ();
extern "C" void HybridDictionary_CopyTo_m3_15 ();
extern "C" void HybridDictionary_GetEnumerator_m3_16 ();
extern "C" void HybridDictionary_Remove_m3_17 ();
extern "C" void HybridDictionary_Switch_m3_18 ();
extern "C" void DictionaryNode__ctor_m3_19 ();
extern "C" void DictionaryNodeEnumerator__ctor_m3_20 ();
extern "C" void DictionaryNodeEnumerator_FailFast_m3_21 ();
extern "C" void DictionaryNodeEnumerator_MoveNext_m3_22 ();
extern "C" void DictionaryNodeEnumerator_Reset_m3_23 ();
extern "C" void DictionaryNodeEnumerator_get_Current_m3_24 ();
extern "C" void DictionaryNodeEnumerator_get_DictionaryNode_m3_25 ();
extern "C" void DictionaryNodeEnumerator_get_Entry_m3_26 ();
extern "C" void DictionaryNodeEnumerator_get_Key_m3_27 ();
extern "C" void DictionaryNodeEnumerator_get_Value_m3_28 ();
extern "C" void DictionaryNodeCollectionEnumerator__ctor_m3_29 ();
extern "C" void DictionaryNodeCollectionEnumerator_get_Current_m3_30 ();
extern "C" void DictionaryNodeCollectionEnumerator_MoveNext_m3_31 ();
extern "C" void DictionaryNodeCollectionEnumerator_Reset_m3_32 ();
extern "C" void DictionaryNodeCollection__ctor_m3_33 ();
extern "C" void DictionaryNodeCollection_get_Count_m3_34 ();
extern "C" void DictionaryNodeCollection_get_SyncRoot_m3_35 ();
extern "C" void DictionaryNodeCollection_CopyTo_m3_36 ();
extern "C" void DictionaryNodeCollection_GetEnumerator_m3_37 ();
extern "C" void ListDictionary__ctor_m3_38 ();
extern "C" void ListDictionary__ctor_m3_39 ();
extern "C" void ListDictionary_System_Collections_IEnumerable_GetEnumerator_m3_40 ();
extern "C" void ListDictionary_FindEntry_m3_41 ();
extern "C" void ListDictionary_FindEntry_m3_42 ();
extern "C" void ListDictionary_AddImpl_m3_43 ();
extern "C" void ListDictionary_get_Count_m3_44 ();
extern "C" void ListDictionary_get_SyncRoot_m3_45 ();
extern "C" void ListDictionary_CopyTo_m3_46 ();
extern "C" void ListDictionary_get_Item_m3_47 ();
extern "C" void ListDictionary_set_Item_m3_48 ();
extern "C" void ListDictionary_get_Keys_m3_49 ();
extern "C" void ListDictionary_Add_m3_50 ();
extern "C" void ListDictionary_Clear_m3_51 ();
extern "C" void ListDictionary_Contains_m3_52 ();
extern "C" void ListDictionary_GetEnumerator_m3_53 ();
extern "C" void ListDictionary_Remove_m3_54 ();
extern "C" void _Item__ctor_m3_55 ();
extern "C" void _KeysEnumerator__ctor_m3_56 ();
extern "C" void _KeysEnumerator_get_Current_m3_57 ();
extern "C" void _KeysEnumerator_MoveNext_m3_58 ();
extern "C" void _KeysEnumerator_Reset_m3_59 ();
extern "C" void KeysCollection__ctor_m3_60 ();
extern "C" void KeysCollection_System_Collections_ICollection_CopyTo_m3_61 ();
extern "C" void KeysCollection_System_Collections_ICollection_get_SyncRoot_m3_62 ();
extern "C" void KeysCollection_get_Count_m3_63 ();
extern "C" void KeysCollection_GetEnumerator_m3_64 ();
extern "C" void NameObjectCollectionBase__ctor_m3_65 ();
extern "C" void NameObjectCollectionBase__ctor_m3_66 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m3_67 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m3_68 ();
extern "C" void NameObjectCollectionBase_Init_m3_69 ();
extern "C" void NameObjectCollectionBase_get_Keys_m3_70 ();
extern "C" void NameObjectCollectionBase_GetEnumerator_m3_71 ();
extern "C" void NameObjectCollectionBase_GetObjectData_m3_72 ();
extern "C" void NameObjectCollectionBase_get_Count_m3_73 ();
extern "C" void NameObjectCollectionBase_OnDeserialization_m3_74 ();
extern "C" void NameObjectCollectionBase_get_IsReadOnly_m3_75 ();
extern "C" void NameObjectCollectionBase_BaseAdd_m3_76 ();
extern "C" void NameObjectCollectionBase_BaseGet_m3_77 ();
extern "C" void NameObjectCollectionBase_BaseGet_m3_78 ();
extern "C" void NameObjectCollectionBase_BaseGetKey_m3_79 ();
extern "C" void NameObjectCollectionBase_FindFirstMatchedItem_m3_80 ();
extern "C" void NameValueCollection__ctor_m3_81 ();
extern "C" void NameValueCollection__ctor_m3_82 ();
extern "C" void NameValueCollection_Add_m3_83 ();
extern "C" void NameValueCollection_Get_m3_84 ();
extern "C" void NameValueCollection_AsSingleString_m3_85 ();
extern "C" void NameValueCollection_GetKey_m3_86 ();
extern "C" void NameValueCollection_InvalidateCachedArrays_m3_87 ();
extern "C" void EditorBrowsableAttribute__ctor_m3_88 ();
extern "C" void EditorBrowsableAttribute_get_State_m3_89 ();
extern "C" void EditorBrowsableAttribute_Equals_m3_90 ();
extern "C" void EditorBrowsableAttribute_GetHashCode_m3_91 ();
extern "C" void TypeConverterAttribute__ctor_m3_92 ();
extern "C" void TypeConverterAttribute__ctor_m3_93 ();
extern "C" void TypeConverterAttribute__cctor_m3_94 ();
extern "C" void TypeConverterAttribute_Equals_m3_95 ();
extern "C" void TypeConverterAttribute_GetHashCode_m3_96 ();
extern "C" void TypeConverterAttribute_get_ConverterTypeName_m3_97 ();
extern "C" void Win32Exception__ctor_m3_98 ();
extern "C" void Win32Exception__ctor_m3_99 ();
extern "C" void Win32Exception__ctor_m3_100 ();
extern "C" void Win32Exception__ctor_m3_101 ();
extern "C" void Win32Exception_get_NativeErrorCode_m3_102 ();
extern "C" void Win32Exception_GetObjectData_m3_103 ();
extern "C" void Win32Exception_W32ErrorMessage_m3_104 ();
extern "C" void Debug_WriteLine_m3_105 ();
extern "C" void Stopwatch__ctor_m3_106 ();
extern "C" void Stopwatch__cctor_m3_107 ();
extern "C" void Stopwatch_GetTimestamp_m3_108 ();
extern "C" void Stopwatch_get_Elapsed_m3_109 ();
extern "C" void Stopwatch_get_ElapsedMilliseconds_m3_110 ();
extern "C" void Stopwatch_get_ElapsedTicks_m3_111 ();
extern "C" void Stopwatch_Reset_m3_112 ();
extern "C" void Stopwatch_Start_m3_113 ();
extern "C" void Stopwatch_Stop_m3_114 ();
extern "C" void LingerOption__ctor_m3_115 ();
extern "C" void Socket__ctor_m3_116 ();
extern "C" void Socket__cctor_m3_117 ();
extern "C" void Socket_Available_internal_m3_118 ();
extern "C" void Socket_get_Available_m3_119 ();
extern "C" void Socket_set_ReceiveTimeout_m3_120 ();
extern "C" void Socket_Connect_m3_121 ();
extern "C" void Socket_Connect_m3_122 ();
extern "C" void Socket_Connect_m3_123 ();
extern "C" void Socket_Poll_m3_124 ();
extern "C" void Socket_Receive_m3_125 ();
extern "C" void Socket_Receive_m3_126 ();
extern "C" void Socket_Receive_m3_127 ();
extern "C" void Socket_RecvFrom_internal_m3_128 ();
extern "C" void Socket_ReceiveFrom_nochecks_exc_m3_129 ();
extern "C" void Socket_Send_m3_130 ();
extern "C" void Socket_Send_m3_131 ();
extern "C" void Socket_CheckProtocolSupport_m3_132 ();
extern "C" void Socket_get_SupportsIPv4_m3_133 ();
extern "C" void Socket_get_SupportsIPv6_m3_134 ();
extern "C" void Socket_Socket_internal_m3_135 ();
extern "C" void Socket_Finalize_m3_136 ();
extern "C" void Socket_get_AddressFamily_m3_137 ();
extern "C" void Socket_get_Connected_m3_138 ();
extern "C" void Socket_set_NoDelay_m3_139 ();
extern "C" void Socket_Linger_m3_140 ();
extern "C" void Socket_Dispose_m3_141 ();
extern "C" void Socket_Dispose_m3_142 ();
extern "C" void Socket_Close_internal_m3_143 ();
extern "C" void Socket_Close_m3_144 ();
extern "C" void Socket_Connect_internal_real_m3_145 ();
extern "C" void Socket_Connect_internal_m3_146 ();
extern "C" void Socket_Connect_internal_m3_147 ();
extern "C" void Socket_CheckEndPoint_m3_148 ();
extern "C" void Socket_GetUnityCrossDomainHelperMethod_m3_149 ();
extern "C" void Socket_Connect_m3_150 ();
extern "C" void Socket_Connect_m3_151 ();
extern "C" void Socket_Poll_internal_m3_152 ();
extern "C" void Socket_Receive_internal_m3_153 ();
extern "C" void Socket_Receive_nochecks_m3_154 ();
extern "C" void Socket_GetSocketOption_obj_internal_m3_155 ();
extern "C" void Socket_Send_internal_m3_156 ();
extern "C" void Socket_Send_nochecks_m3_157 ();
extern "C" void Socket_GetSocketOption_m3_158 ();
extern "C" void Socket_Shutdown_internal_m3_159 ();
extern "C" void Socket_SetSocketOption_internal_m3_160 ();
extern "C" void Socket_SetSocketOption_m3_161 ();
extern "C" void Socket_ThrowIfUpd_m3_162 ();
extern "C" void SocketException__ctor_m3_163 ();
extern "C" void SocketException__ctor_m3_164 ();
extern "C" void SocketException__ctor_m3_165 ();
extern "C" void SocketException__ctor_m3_166 ();
extern "C" void SocketException_WSAGetLastError_internal_m3_167 ();
extern "C" void SocketException_get_SocketErrorCode_m3_168 ();
extern "C" void SocketException_get_Message_m3_169 ();
extern "C" void DefaultCertificatePolicy__ctor_m3_170 ();
extern "C" void DefaultCertificatePolicy_CheckValidationResult_m3_171 ();
extern "C" void Dns__cctor_m3_172 ();
extern "C" void Dns_GetHostByName_internal_m3_173 ();
extern "C" void Dns_GetHostByAddr_internal_m3_174 ();
extern "C" void Dns_hostent_to_IPHostEntry_m3_175 ();
extern "C" void Dns_GetHostByAddressFromString_m3_176 ();
extern "C" void Dns_GetHostEntry_m3_177 ();
extern "C" void Dns_GetHostEntry_m3_178 ();
extern "C" void Dns_GetHostAddresses_m3_179 ();
extern "C" void Dns_GetHostByName_m3_180 ();
extern "C" void EndPoint__ctor_m3_181 ();
extern "C" void EndPoint_get_AddressFamily_m3_182 ();
extern "C" void EndPoint_Create_m3_183 ();
extern "C" void EndPoint_Serialize_m3_184 ();
extern "C" void EndPoint_NotImplemented_m3_185 ();
extern "C" void FileWebRequest__ctor_m3_186 ();
extern "C" void FileWebRequest__ctor_m3_187 ();
extern "C" void FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m3_188 ();
extern "C" void FileWebRequest_GetObjectData_m3_189 ();
extern "C" void FileWebRequestCreator__ctor_m3_190 ();
extern "C" void FileWebRequestCreator_Create_m3_191 ();
extern "C" void FtpRequestCreator__ctor_m3_192 ();
extern "C" void FtpRequestCreator_Create_m3_193 ();
extern "C" void FtpWebRequest__ctor_m3_194 ();
extern "C" void FtpWebRequest__cctor_m3_195 ();
extern "C" void FtpWebRequest_U3CcallbackU3Em__B_m3_196 ();
extern "C" void GlobalProxySelection_get_Select_m3_197 ();
extern "C" void HttpRequestCreator__ctor_m3_198 ();
extern "C" void HttpRequestCreator_Create_m3_199 ();
extern "C" void HttpVersion__cctor_m3_200 ();
extern "C" void HttpWebRequest__ctor_m3_201 ();
extern "C" void HttpWebRequest__ctor_m3_202 ();
extern "C" void HttpWebRequest__cctor_m3_203 ();
extern "C" void HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m3_204 ();
extern "C" void HttpWebRequest_get_Address_m3_205 ();
extern "C" void HttpWebRequest_get_ServicePoint_m3_206 ();
extern "C" void HttpWebRequest_GetServicePoint_m3_207 ();
extern "C" void HttpWebRequest_GetObjectData_m3_208 ();
extern "C" void IPAddress__ctor_m3_209 ();
extern "C" void IPAddress__ctor_m3_210 ();
extern "C" void IPAddress__cctor_m3_211 ();
extern "C" void IPAddress_SwapShort_m3_212 ();
extern "C" void IPAddress_HostToNetworkOrder_m3_213 ();
extern "C" void IPAddress_NetworkToHostOrder_m3_214 ();
extern "C" void IPAddress_Parse_m3_215 ();
extern "C" void IPAddress_TryParse_m3_216 ();
extern "C" void IPAddress_ParseIPV4_m3_217 ();
extern "C" void IPAddress_ParseIPV6_m3_218 ();
extern "C" void IPAddress_get_InternalIPv4Address_m3_219 ();
extern "C" void IPAddress_get_ScopeId_m3_220 ();
extern "C" void IPAddress_GetAddressBytes_m3_221 ();
extern "C" void IPAddress_get_AddressFamily_m3_222 ();
extern "C" void IPAddress_IsLoopback_m3_223 ();
extern "C" void IPAddress_ToString_m3_224 ();
extern "C" void IPAddress_ToString_m3_225 ();
extern "C" void IPAddress_Equals_m3_226 ();
extern "C" void IPAddress_GetHashCode_m3_227 ();
extern "C" void IPAddress_Hash_m3_228 ();
extern "C" void IPEndPoint__ctor_m3_229 ();
extern "C" void IPEndPoint__ctor_m3_230 ();
extern "C" void IPEndPoint_get_Address_m3_231 ();
extern "C" void IPEndPoint_set_Address_m3_232 ();
extern "C" void IPEndPoint_get_AddressFamily_m3_233 ();
extern "C" void IPEndPoint_get_Port_m3_234 ();
extern "C" void IPEndPoint_set_Port_m3_235 ();
extern "C" void IPEndPoint_Create_m3_236 ();
extern "C" void IPEndPoint_Serialize_m3_237 ();
extern "C" void IPEndPoint_ToString_m3_238 ();
extern "C" void IPEndPoint_Equals_m3_239 ();
extern "C" void IPEndPoint_GetHashCode_m3_240 ();
extern "C" void IPHostEntry__ctor_m3_241 ();
extern "C" void IPHostEntry_get_AddressList_m3_242 ();
extern "C" void IPHostEntry_set_AddressList_m3_243 ();
extern "C" void IPHostEntry_set_Aliases_m3_244 ();
extern "C" void IPHostEntry_set_HostName_m3_245 ();
extern "C" void IPv6Address__ctor_m3_246 ();
extern "C" void IPv6Address__ctor_m3_247 ();
extern "C" void IPv6Address__ctor_m3_248 ();
extern "C" void IPv6Address__cctor_m3_249 ();
extern "C" void IPv6Address_Parse_m3_250 ();
extern "C" void IPv6Address_Fill_m3_251 ();
extern "C" void IPv6Address_TryParse_m3_252 ();
extern "C" void IPv6Address_TryParse_m3_253 ();
extern "C" void IPv6Address_get_Address_m3_254 ();
extern "C" void IPv6Address_get_ScopeId_m3_255 ();
extern "C" void IPv6Address_set_ScopeId_m3_256 ();
extern "C" void IPv6Address_IsLoopback_m3_257 ();
extern "C" void IPv6Address_SwapUShort_m3_258 ();
extern "C" void IPv6Address_AsIPv4Int_m3_259 ();
extern "C" void IPv6Address_IsIPv4Compatible_m3_260 ();
extern "C" void IPv6Address_IsIPv4Mapped_m3_261 ();
extern "C" void IPv6Address_ToString_m3_262 ();
extern "C" void IPv6Address_ToString_m3_263 ();
extern "C" void IPv6Address_Equals_m3_264 ();
extern "C" void IPv6Address_GetHashCode_m3_265 ();
extern "C" void IPv6Address_Hash_m3_266 ();
extern "C" void ServicePoint__ctor_m3_267 ();
extern "C" void ServicePoint_get_Address_m3_268 ();
extern "C" void ServicePoint_get_CurrentConnections_m3_269 ();
extern "C" void ServicePoint_get_IdleSince_m3_270 ();
extern "C" void ServicePoint_set_IdleSince_m3_271 ();
extern "C" void ServicePoint_set_Expect100Continue_m3_272 ();
extern "C" void ServicePoint_set_UseNagleAlgorithm_m3_273 ();
extern "C" void ServicePoint_set_SendContinue_m3_274 ();
extern "C" void ServicePoint_set_UsesProxy_m3_275 ();
extern "C" void ServicePoint_set_UseConnect_m3_276 ();
extern "C" void ServicePoint_get_AvailableForRecycling_m3_277 ();
extern "C" void SPKey__ctor_m3_278 ();
extern "C" void SPKey_GetHashCode_m3_279 ();
extern "C" void SPKey_Equals_m3_280 ();
extern "C" void ServicePointManager__cctor_m3_281 ();
extern "C" void ServicePointManager_get_CertificatePolicy_m3_282 ();
extern "C" void ServicePointManager_get_CheckCertificateRevocationList_m3_283 ();
extern "C" void ServicePointManager_get_SecurityProtocol_m3_284 ();
extern "C" void ServicePointManager_get_ServerCertificateValidationCallback_m3_285 ();
extern "C" void ServicePointManager_FindServicePoint_m3_286 ();
extern "C" void ServicePointManager_RecycleServicePoints_m3_287 ();
extern "C" void SocketAddress__ctor_m3_288 ();
extern "C" void SocketAddress_get_Family_m3_289 ();
extern "C" void SocketAddress_get_Size_m3_290 ();
extern "C" void SocketAddress_get_Item_m3_291 ();
extern "C" void SocketAddress_set_Item_m3_292 ();
extern "C" void SocketAddress_ToString_m3_293 ();
extern "C" void SocketAddress_Equals_m3_294 ();
extern "C" void SocketAddress_GetHashCode_m3_295 ();
extern "C" void WebHeaderCollection__ctor_m3_296 ();
extern "C" void WebHeaderCollection__ctor_m3_297 ();
extern "C" void WebHeaderCollection__ctor_m3_298 ();
extern "C" void WebHeaderCollection__cctor_m3_299 ();
extern "C" void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m3_300 ();
extern "C" void WebHeaderCollection_Add_m3_301 ();
extern "C" void WebHeaderCollection_AddWithoutValidate_m3_302 ();
extern "C" void WebHeaderCollection_IsRestricted_m3_303 ();
extern "C" void WebHeaderCollection_OnDeserialization_m3_304 ();
extern "C" void WebHeaderCollection_ToString_m3_305 ();
extern "C" void WebHeaderCollection_GetObjectData_m3_306 ();
extern "C" void WebHeaderCollection_get_Count_m3_307 ();
extern "C" void WebHeaderCollection_get_Keys_m3_308 ();
extern "C" void WebHeaderCollection_Get_m3_309 ();
extern "C" void WebHeaderCollection_GetKey_m3_310 ();
extern "C" void WebHeaderCollection_GetEnumerator_m3_311 ();
extern "C" void WebHeaderCollection_IsHeaderValue_m3_312 ();
extern "C" void WebHeaderCollection_IsHeaderName_m3_313 ();
extern "C" void WebProxy__ctor_m3_314 ();
extern "C" void WebProxy__ctor_m3_315 ();
extern "C" void WebProxy__ctor_m3_316 ();
extern "C" void WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m3_317 ();
extern "C" void WebProxy_get_UseDefaultCredentials_m3_318 ();
extern "C" void WebProxy_GetProxy_m3_319 ();
extern "C" void WebProxy_IsBypassed_m3_320 ();
extern "C" void WebProxy_GetObjectData_m3_321 ();
extern "C" void WebProxy_CheckBypassList_m3_322 ();
extern "C" void WebRequest__ctor_m3_323 ();
extern "C" void WebRequest__ctor_m3_324 ();
extern "C" void WebRequest__cctor_m3_325 ();
extern "C" void WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m3_326 ();
extern "C" void WebRequest_AddDynamicPrefix_m3_327 ();
extern "C" void WebRequest_GetMustImplement_m3_328 ();
extern "C" void WebRequest_get_DefaultWebProxy_m3_329 ();
extern "C" void WebRequest_GetDefaultWebProxy_m3_330 ();
extern "C" void WebRequest_GetObjectData_m3_331 ();
extern "C" void WebRequest_AddPrefix_m3_332 ();
extern "C" void PublicKey__ctor_m3_333 ();
extern "C" void PublicKey_get_EncodedKeyValue_m3_334 ();
extern "C" void PublicKey_get_EncodedParameters_m3_335 ();
extern "C" void PublicKey_get_Key_m3_336 ();
extern "C" void PublicKey_get_Oid_m3_337 ();
extern "C" void PublicKey_GetUnsignedBigInteger_m3_338 ();
extern "C" void PublicKey_DecodeDSA_m3_339 ();
extern "C" void PublicKey_DecodeRSA_m3_340 ();
extern "C" void X500DistinguishedName__ctor_m3_341 ();
extern "C" void X500DistinguishedName_Decode_m3_342 ();
extern "C" void X500DistinguishedName_GetSeparator_m3_343 ();
extern "C" void X500DistinguishedName_DecodeRawData_m3_344 ();
extern "C" void X500DistinguishedName_Canonize_m3_345 ();
extern "C" void X500DistinguishedName_AreEqual_m3_346 ();
extern "C" void X509BasicConstraintsExtension__ctor_m3_347 ();
extern "C" void X509BasicConstraintsExtension__ctor_m3_348 ();
extern "C" void X509BasicConstraintsExtension__ctor_m3_349 ();
extern "C" void X509BasicConstraintsExtension_get_CertificateAuthority_m3_350 ();
extern "C" void X509BasicConstraintsExtension_get_HasPathLengthConstraint_m3_351 ();
extern "C" void X509BasicConstraintsExtension_get_PathLengthConstraint_m3_352 ();
extern "C" void X509BasicConstraintsExtension_CopyFrom_m3_353 ();
extern "C" void X509BasicConstraintsExtension_Decode_m3_354 ();
extern "C" void X509BasicConstraintsExtension_Encode_m3_355 ();
extern "C" void X509BasicConstraintsExtension_ToString_m3_356 ();
extern "C" void X509Certificate2__ctor_m3_357 ();
extern "C" void X509Certificate2__cctor_m3_358 ();
extern "C" void X509Certificate2_get_Extensions_m3_359 ();
extern "C" void X509Certificate2_get_IssuerName_m3_360 ();
extern "C" void X509Certificate2_get_NotAfter_m3_361 ();
extern "C" void X509Certificate2_get_NotBefore_m3_362 ();
extern "C" void X509Certificate2_get_PrivateKey_m3_363 ();
extern "C" void X509Certificate2_get_PublicKey_m3_364 ();
extern "C" void X509Certificate2_get_SerialNumber_m3_365 ();
extern "C" void X509Certificate2_get_SignatureAlgorithm_m3_366 ();
extern "C" void X509Certificate2_get_SubjectName_m3_367 ();
extern "C" void X509Certificate2_get_Thumbprint_m3_368 ();
extern "C" void X509Certificate2_get_Version_m3_369 ();
extern "C" void X509Certificate2_GetNameInfo_m3_370 ();
extern "C" void X509Certificate2_Find_m3_371 ();
extern "C" void X509Certificate2_GetValueAsString_m3_372 ();
extern "C" void X509Certificate2_ImportPkcs12_m3_373 ();
extern "C" void X509Certificate2_Import_m3_374 ();
extern "C" void X509Certificate2_Reset_m3_375 ();
extern "C" void X509Certificate2_ToString_m3_376 ();
extern "C" void X509Certificate2_ToString_m3_377 ();
extern "C" void X509Certificate2_AppendBuffer_m3_378 ();
extern "C" void X509Certificate2_Verify_m3_379 ();
extern "C" void X509Certificate2_get_MonoCertificate_m3_380 ();
extern "C" void X509Certificate2Collection__ctor_m3_381 ();
extern "C" void X509Certificate2Collection__ctor_m3_382 ();
extern "C" void X509Certificate2Collection_get_Item_m3_383 ();
extern "C" void X509Certificate2Collection_Add_m3_384 ();
extern "C" void X509Certificate2Collection_AddRange_m3_385 ();
extern "C" void X509Certificate2Collection_Contains_m3_386 ();
extern "C" void X509Certificate2Collection_Find_m3_387 ();
extern "C" void X509Certificate2Collection_GetEnumerator_m3_388 ();
extern "C" void X509Certificate2Enumerator__ctor_m3_389 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m3_390 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m3_391 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m3_392 ();
extern "C" void X509Certificate2Enumerator_get_Current_m3_393 ();
extern "C" void X509Certificate2Enumerator_MoveNext_m3_394 ();
extern "C" void X509Certificate2Enumerator_Reset_m3_395 ();
extern "C" void X509CertificateEnumerator__ctor_m3_396 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m3_397 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m3_398 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m3_399 ();
extern "C" void X509CertificateEnumerator_get_Current_m3_400 ();
extern "C" void X509CertificateEnumerator_MoveNext_m3_401 ();
extern "C" void X509CertificateEnumerator_Reset_m3_402 ();
extern "C" void X509CertificateCollection__ctor_m3_403 ();
extern "C" void X509CertificateCollection__ctor_m3_404 ();
extern "C" void X509CertificateCollection_get_Item_m3_405 ();
extern "C" void X509CertificateCollection_AddRange_m3_406 ();
extern "C" void X509CertificateCollection_GetEnumerator_m3_407 ();
extern "C" void X509CertificateCollection_GetHashCode_m3_408 ();
extern "C" void X509Chain__ctor_m3_409 ();
extern "C" void X509Chain__ctor_m3_410 ();
extern "C" void X509Chain__cctor_m3_411 ();
extern "C" void X509Chain_get_ChainPolicy_m3_412 ();
extern "C" void X509Chain_Build_m3_413 ();
extern "C" void X509Chain_Reset_m3_414 ();
extern "C" void X509Chain_get_Roots_m3_415 ();
extern "C" void X509Chain_get_CertificateAuthorities_m3_416 ();
extern "C" void X509Chain_get_CertificateCollection_m3_417 ();
extern "C" void X509Chain_BuildChainFrom_m3_418 ();
extern "C" void X509Chain_SelectBestFromCollection_m3_419 ();
extern "C" void X509Chain_FindParent_m3_420 ();
extern "C" void X509Chain_IsChainComplete_m3_421 ();
extern "C" void X509Chain_IsSelfIssued_m3_422 ();
extern "C" void X509Chain_ValidateChain_m3_423 ();
extern "C" void X509Chain_Process_m3_424 ();
extern "C" void X509Chain_PrepareForNextCertificate_m3_425 ();
extern "C" void X509Chain_WrapUp_m3_426 ();
extern "C" void X509Chain_ProcessCertificateExtensions_m3_427 ();
extern "C" void X509Chain_IsSignedWith_m3_428 ();
extern "C" void X509Chain_GetSubjectKeyIdentifier_m3_429 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m3_430 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m3_431 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m3_432 ();
extern "C" void X509Chain_CheckRevocationOnChain_m3_433 ();
extern "C" void X509Chain_CheckRevocation_m3_434 ();
extern "C" void X509Chain_CheckRevocation_m3_435 ();
extern "C" void X509Chain_FindCrl_m3_436 ();
extern "C" void X509Chain_ProcessCrlExtensions_m3_437 ();
extern "C" void X509Chain_ProcessCrlEntryExtensions_m3_438 ();
extern "C" void X509ChainElement__ctor_m3_439 ();
extern "C" void X509ChainElement_get_Certificate_m3_440 ();
extern "C" void X509ChainElement_get_ChainElementStatus_m3_441 ();
extern "C" void X509ChainElement_get_StatusFlags_m3_442 ();
extern "C" void X509ChainElement_set_StatusFlags_m3_443 ();
extern "C" void X509ChainElement_Count_m3_444 ();
extern "C" void X509ChainElement_Set_m3_445 ();
extern "C" void X509ChainElement_UncompressFlags_m3_446 ();
extern "C" void X509ChainElementCollection__ctor_m3_447 ();
extern "C" void X509ChainElementCollection_System_Collections_ICollection_CopyTo_m3_448 ();
extern "C" void X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m3_449 ();
extern "C" void X509ChainElementCollection_get_Count_m3_450 ();
extern "C" void X509ChainElementCollection_get_Item_m3_451 ();
extern "C" void X509ChainElementCollection_get_SyncRoot_m3_452 ();
extern "C" void X509ChainElementCollection_GetEnumerator_m3_453 ();
extern "C" void X509ChainElementCollection_Add_m3_454 ();
extern "C" void X509ChainElementCollection_Clear_m3_455 ();
extern "C" void X509ChainElementCollection_Contains_m3_456 ();
extern "C" void X509ChainElementEnumerator__ctor_m3_457 ();
extern "C" void X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m3_458 ();
extern "C" void X509ChainElementEnumerator_get_Current_m3_459 ();
extern "C" void X509ChainElementEnumerator_MoveNext_m3_460 ();
extern "C" void X509ChainElementEnumerator_Reset_m3_461 ();
extern "C" void X509ChainPolicy__ctor_m3_462 ();
extern "C" void X509ChainPolicy_get_ExtraStore_m3_463 ();
extern "C" void X509ChainPolicy_get_RevocationFlag_m3_464 ();
extern "C" void X509ChainPolicy_get_RevocationMode_m3_465 ();
extern "C" void X509ChainPolicy_get_VerificationFlags_m3_466 ();
extern "C" void X509ChainPolicy_get_VerificationTime_m3_467 ();
extern "C" void X509ChainPolicy_Reset_m3_468 ();
extern "C" void X509ChainStatus__ctor_m3_469 ();
extern "C" void X509ChainStatus_get_Status_m3_470 ();
extern "C" void X509ChainStatus_set_Status_m3_471 ();
extern "C" void X509ChainStatus_set_StatusInformation_m3_472 ();
extern "C" void X509ChainStatus_GetInformation_m3_473 ();
extern "C" void X509EnhancedKeyUsageExtension__ctor_m3_474 ();
extern "C" void X509EnhancedKeyUsageExtension_CopyFrom_m3_475 ();
extern "C" void X509EnhancedKeyUsageExtension_Decode_m3_476 ();
extern "C" void X509EnhancedKeyUsageExtension_ToString_m3_477 ();
extern "C" void X509Extension__ctor_m3_478 ();
extern "C" void X509Extension__ctor_m3_479 ();
extern "C" void X509Extension_get_Critical_m3_480 ();
extern "C" void X509Extension_set_Critical_m3_481 ();
extern "C" void X509Extension_CopyFrom_m3_482 ();
extern "C" void X509Extension_FormatUnkownData_m3_483 ();
extern "C" void X509ExtensionCollection__ctor_m3_484 ();
extern "C" void X509ExtensionCollection_System_Collections_ICollection_CopyTo_m3_485 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m3_486 ();
extern "C" void X509ExtensionCollection_get_Count_m3_487 ();
extern "C" void X509ExtensionCollection_get_SyncRoot_m3_488 ();
extern "C" void X509ExtensionCollection_get_Item_m3_489 ();
extern "C" void X509ExtensionCollection_GetEnumerator_m3_490 ();
extern "C" void X509ExtensionEnumerator__ctor_m3_491 ();
extern "C" void X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m3_492 ();
extern "C" void X509ExtensionEnumerator_get_Current_m3_493 ();
extern "C" void X509ExtensionEnumerator_MoveNext_m3_494 ();
extern "C" void X509ExtensionEnumerator_Reset_m3_495 ();
extern "C" void X509KeyUsageExtension__ctor_m3_496 ();
extern "C" void X509KeyUsageExtension__ctor_m3_497 ();
extern "C" void X509KeyUsageExtension__ctor_m3_498 ();
extern "C" void X509KeyUsageExtension_get_KeyUsages_m3_499 ();
extern "C" void X509KeyUsageExtension_CopyFrom_m3_500 ();
extern "C" void X509KeyUsageExtension_GetValidFlags_m3_501 ();
extern "C" void X509KeyUsageExtension_Decode_m3_502 ();
extern "C" void X509KeyUsageExtension_Encode_m3_503 ();
extern "C" void X509KeyUsageExtension_ToString_m3_504 ();
extern "C" void X509Store__ctor_m3_505 ();
extern "C" void X509Store_get_Certificates_m3_506 ();
extern "C" void X509Store_get_Factory_m3_507 ();
extern "C" void X509Store_get_Store_m3_508 ();
extern "C" void X509Store_Close_m3_509 ();
extern "C" void X509Store_Open_m3_510 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m3_511 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m3_512 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m3_513 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m3_514 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m3_515 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m3_516 ();
extern "C" void X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m3_517 ();
extern "C" void X509SubjectKeyIdentifierExtension_CopyFrom_m3_518 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChar_m3_519 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChars_m3_520 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHex_m3_521 ();
extern "C" void X509SubjectKeyIdentifierExtension_Decode_m3_522 ();
extern "C" void X509SubjectKeyIdentifierExtension_Encode_m3_523 ();
extern "C" void X509SubjectKeyIdentifierExtension_ToString_m3_524 ();
extern "C" void AsnEncodedData__ctor_m3_525 ();
extern "C" void AsnEncodedData__ctor_m3_526 ();
extern "C" void AsnEncodedData__ctor_m3_527 ();
extern "C" void AsnEncodedData_get_Oid_m3_528 ();
extern "C" void AsnEncodedData_set_Oid_m3_529 ();
extern "C" void AsnEncodedData_get_RawData_m3_530 ();
extern "C" void AsnEncodedData_set_RawData_m3_531 ();
extern "C" void AsnEncodedData_CopyFrom_m3_532 ();
extern "C" void AsnEncodedData_ToString_m3_533 ();
extern "C" void AsnEncodedData_Default_m3_534 ();
extern "C" void AsnEncodedData_BasicConstraintsExtension_m3_535 ();
extern "C" void AsnEncodedData_EnhancedKeyUsageExtension_m3_536 ();
extern "C" void AsnEncodedData_KeyUsageExtension_m3_537 ();
extern "C" void AsnEncodedData_SubjectKeyIdentifierExtension_m3_538 ();
extern "C" void AsnEncodedData_SubjectAltName_m3_539 ();
extern "C" void AsnEncodedData_NetscapeCertType_m3_540 ();
extern "C" void Oid__ctor_m3_541 ();
extern "C" void Oid__ctor_m3_542 ();
extern "C" void Oid__ctor_m3_543 ();
extern "C" void Oid__ctor_m3_544 ();
extern "C" void Oid_get_FriendlyName_m3_545 ();
extern "C" void Oid_get_Value_m3_546 ();
extern "C" void Oid_GetName_m3_547 ();
extern "C" void OidCollection__ctor_m3_548 ();
extern "C" void OidCollection_System_Collections_ICollection_CopyTo_m3_549 ();
extern "C" void OidCollection_System_Collections_IEnumerable_GetEnumerator_m3_550 ();
extern "C" void OidCollection_get_Count_m3_551 ();
extern "C" void OidCollection_get_Item_m3_552 ();
extern "C" void OidCollection_get_SyncRoot_m3_553 ();
extern "C" void OidCollection_Add_m3_554 ();
extern "C" void OidEnumerator__ctor_m3_555 ();
extern "C" void OidEnumerator_System_Collections_IEnumerator_get_Current_m3_556 ();
extern "C" void OidEnumerator_MoveNext_m3_557 ();
extern "C" void OidEnumerator_Reset_m3_558 ();
extern "C" void MatchAppendEvaluator__ctor_m3_559 ();
extern "C" void MatchAppendEvaluator_Invoke_m3_560 ();
extern "C" void MatchAppendEvaluator_BeginInvoke_m3_561 ();
extern "C" void MatchAppendEvaluator_EndInvoke_m3_562 ();
extern "C" void BaseMachine__ctor_m3_563 ();
extern "C" void BaseMachine_Replace_m3_564 ();
extern "C" void BaseMachine_Scan_m3_565 ();
extern "C" void BaseMachine_LTRReplace_m3_566 ();
extern "C" void BaseMachine_RTLReplace_m3_567 ();
extern "C" void Capture__ctor_m3_568 ();
extern "C" void Capture__ctor_m3_569 ();
extern "C" void Capture_get_Index_m3_570 ();
extern "C" void Capture_get_Length_m3_571 ();
extern "C" void Capture_get_Value_m3_572 ();
extern "C" void Capture_ToString_m3_573 ();
extern "C" void Capture_get_Text_m3_574 ();
extern "C" void CaptureCollection__ctor_m3_575 ();
extern "C" void CaptureCollection_get_Count_m3_576 ();
extern "C" void CaptureCollection_SetValue_m3_577 ();
extern "C" void CaptureCollection_get_SyncRoot_m3_578 ();
extern "C" void CaptureCollection_CopyTo_m3_579 ();
extern "C" void CaptureCollection_GetEnumerator_m3_580 ();
extern "C" void Group__ctor_m3_581 ();
extern "C" void Group__ctor_m3_582 ();
extern "C" void Group__ctor_m3_583 ();
extern "C" void Group__cctor_m3_584 ();
extern "C" void Group_get_Captures_m3_585 ();
extern "C" void Group_get_Success_m3_586 ();
extern "C" void GroupCollection__ctor_m3_587 ();
extern "C" void GroupCollection_get_Count_m3_588 ();
extern "C" void GroupCollection_get_Item_m3_589 ();
extern "C" void GroupCollection_SetValue_m3_590 ();
extern "C" void GroupCollection_get_SyncRoot_m3_591 ();
extern "C" void GroupCollection_CopyTo_m3_592 ();
extern "C" void GroupCollection_GetEnumerator_m3_593 ();
extern "C" void Match__ctor_m3_594 ();
extern "C" void Match__ctor_m3_595 ();
extern "C" void Match__ctor_m3_596 ();
extern "C" void Match__cctor_m3_597 ();
extern "C" void Match_get_Empty_m3_598 ();
extern "C" void Match_get_Groups_m3_599 ();
extern "C" void Match_NextMatch_m3_600 ();
extern "C" void Match_get_Regex_m3_601 ();
extern "C" void Enumerator__ctor_m3_602 ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m3_603 ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m3_604 ();
extern "C" void Enumerator_System_Collections_IEnumerator_MoveNext_m3_605 ();
extern "C" void MatchCollection__ctor_m3_606 ();
extern "C" void MatchCollection_get_Count_m3_607 ();
extern "C" void MatchCollection_get_Item_m3_608 ();
extern "C" void MatchCollection_get_SyncRoot_m3_609 ();
extern "C" void MatchCollection_CopyTo_m3_610 ();
extern "C" void MatchCollection_GetEnumerator_m3_611 ();
extern "C" void MatchCollection_TryToGet_m3_612 ();
extern "C" void MatchCollection_get_FullList_m3_613 ();
extern "C" void Regex__ctor_m3_614 ();
extern "C" void Regex__ctor_m3_615 ();
extern "C" void Regex__ctor_m3_616 ();
extern "C" void Regex__ctor_m3_617 ();
extern "C" void Regex__cctor_m3_618 ();
extern "C" void Regex_System_Runtime_Serialization_ISerializable_GetObjectData_m3_619 ();
extern "C" void Regex_Replace_m3_620 ();
extern "C" void Regex_Replace_m3_621 ();
extern "C" void Regex_validate_options_m3_622 ();
extern "C" void Regex_Init_m3_623 ();
extern "C" void Regex_InitNewRegex_m3_624 ();
extern "C" void Regex_CreateMachineFactory_m3_625 ();
extern "C" void Regex_get_Options_m3_626 ();
extern "C" void Regex_get_RightToLeft_m3_627 ();
extern "C" void Regex_GroupNumberFromName_m3_628 ();
extern "C" void Regex_GetGroupIndex_m3_629 ();
extern "C" void Regex_default_startat_m3_630 ();
extern "C" void Regex_IsMatch_m3_631 ();
extern "C" void Regex_IsMatch_m3_632 ();
extern "C" void Regex_Match_m3_633 ();
extern "C" void Regex_Matches_m3_634 ();
extern "C" void Regex_Matches_m3_635 ();
extern "C" void Regex_Replace_m3_636 ();
extern "C" void Regex_Replace_m3_637 ();
extern "C" void Regex_ToString_m3_638 ();
extern "C" void Regex_get_GroupCount_m3_639 ();
extern "C" void Regex_get_Gap_m3_640 ();
extern "C" void Regex_CreateMachine_m3_641 ();
extern "C" void Regex_GetGroupNamesArray_m3_642 ();
extern "C" void Regex_get_GroupNumbers_m3_643 ();
extern "C" void Key__ctor_m3_644 ();
extern "C" void Key_GetHashCode_m3_645 ();
extern "C" void Key_Equals_m3_646 ();
extern "C" void Key_ToString_m3_647 ();
extern "C" void FactoryCache__ctor_m3_648 ();
extern "C" void FactoryCache_Add_m3_649 ();
extern "C" void FactoryCache_Cleanup_m3_650 ();
extern "C" void FactoryCache_Lookup_m3_651 ();
extern "C" void Node__ctor_m3_652 ();
extern "C" void MRUList__ctor_m3_653 ();
extern "C" void MRUList_Use_m3_654 ();
extern "C" void MRUList_Evict_m3_655 ();
extern "C" void CategoryUtils_CategoryFromName_m3_656 ();
extern "C" void CategoryUtils_IsCategory_m3_657 ();
extern "C" void CategoryUtils_IsCategory_m3_658 ();
extern "C" void LinkRef__ctor_m3_659 ();
extern "C" void InterpreterFactory__ctor_m3_660 ();
extern "C" void InterpreterFactory_NewInstance_m3_661 ();
extern "C" void InterpreterFactory_get_GroupCount_m3_662 ();
extern "C" void InterpreterFactory_get_Gap_m3_663 ();
extern "C" void InterpreterFactory_set_Gap_m3_664 ();
extern "C" void InterpreterFactory_get_Mapping_m3_665 ();
extern "C" void InterpreterFactory_set_Mapping_m3_666 ();
extern "C" void InterpreterFactory_get_NamesMapping_m3_667 ();
extern "C" void InterpreterFactory_set_NamesMapping_m3_668 ();
extern "C" void PatternLinkStack__ctor_m3_669 ();
extern "C" void PatternLinkStack_set_BaseAddress_m3_670 ();
extern "C" void PatternLinkStack_get_OffsetAddress_m3_671 ();
extern "C" void PatternLinkStack_set_OffsetAddress_m3_672 ();
extern "C" void PatternLinkStack_GetOffset_m3_673 ();
extern "C" void PatternLinkStack_GetCurrent_m3_674 ();
extern "C" void PatternLinkStack_SetCurrent_m3_675 ();
extern "C" void PatternCompiler__ctor_m3_676 ();
extern "C" void PatternCompiler_EncodeOp_m3_677 ();
extern "C" void PatternCompiler_GetMachineFactory_m3_678 ();
extern "C" void PatternCompiler_EmitFalse_m3_679 ();
extern "C" void PatternCompiler_EmitTrue_m3_680 ();
extern "C" void PatternCompiler_EmitCount_m3_681 ();
extern "C" void PatternCompiler_EmitCharacter_m3_682 ();
extern "C" void PatternCompiler_EmitCategory_m3_683 ();
extern "C" void PatternCompiler_EmitNotCategory_m3_684 ();
extern "C" void PatternCompiler_EmitRange_m3_685 ();
extern "C" void PatternCompiler_EmitSet_m3_686 ();
extern "C" void PatternCompiler_EmitString_m3_687 ();
extern "C" void PatternCompiler_EmitPosition_m3_688 ();
extern "C" void PatternCompiler_EmitOpen_m3_689 ();
extern "C" void PatternCompiler_EmitClose_m3_690 ();
extern "C" void PatternCompiler_EmitBalanceStart_m3_691 ();
extern "C" void PatternCompiler_EmitBalance_m3_692 ();
extern "C" void PatternCompiler_EmitReference_m3_693 ();
extern "C" void PatternCompiler_EmitIfDefined_m3_694 ();
extern "C" void PatternCompiler_EmitSub_m3_695 ();
extern "C" void PatternCompiler_EmitTest_m3_696 ();
extern "C" void PatternCompiler_EmitBranch_m3_697 ();
extern "C" void PatternCompiler_EmitJump_m3_698 ();
extern "C" void PatternCompiler_EmitRepeat_m3_699 ();
extern "C" void PatternCompiler_EmitUntil_m3_700 ();
extern "C" void PatternCompiler_EmitFastRepeat_m3_701 ();
extern "C" void PatternCompiler_EmitIn_m3_702 ();
extern "C" void PatternCompiler_EmitAnchor_m3_703 ();
extern "C" void PatternCompiler_EmitInfo_m3_704 ();
extern "C" void PatternCompiler_NewLink_m3_705 ();
extern "C" void PatternCompiler_ResolveLink_m3_706 ();
extern "C" void PatternCompiler_EmitBranchEnd_m3_707 ();
extern "C" void PatternCompiler_EmitAlternationEnd_m3_708 ();
extern "C" void PatternCompiler_MakeFlags_m3_709 ();
extern "C" void PatternCompiler_Emit_m3_710 ();
extern "C" void PatternCompiler_Emit_m3_711 ();
extern "C" void PatternCompiler_Emit_m3_712 ();
extern "C" void PatternCompiler_get_CurrentAddress_m3_713 ();
extern "C" void PatternCompiler_BeginLink_m3_714 ();
extern "C" void PatternCompiler_EmitLink_m3_715 ();
extern "C" void LinkStack__ctor_m3_716 ();
extern "C" void LinkStack_Push_m3_717 ();
extern "C" void LinkStack_Pop_m3_718 ();
extern "C" void Mark_get_IsDefined_m3_719 ();
extern "C" void Mark_get_Index_m3_720 ();
extern "C" void Mark_get_Length_m3_721 ();
extern "C" void IntStack_Pop_m3_722 ();
extern "C" void IntStack_Push_m3_723 ();
extern "C" void IntStack_get_Count_m3_724 ();
extern "C" void IntStack_set_Count_m3_725 ();
extern "C" void RepeatContext__ctor_m3_726 ();
extern "C" void RepeatContext_get_Count_m3_727 ();
extern "C" void RepeatContext_set_Count_m3_728 ();
extern "C" void RepeatContext_get_Start_m3_729 ();
extern "C" void RepeatContext_set_Start_m3_730 ();
extern "C" void RepeatContext_get_IsMinimum_m3_731 ();
extern "C" void RepeatContext_get_IsMaximum_m3_732 ();
extern "C" void RepeatContext_get_IsLazy_m3_733 ();
extern "C" void RepeatContext_get_Expression_m3_734 ();
extern "C" void RepeatContext_get_Previous_m3_735 ();
extern "C" void Interpreter__ctor_m3_736 ();
extern "C" void Interpreter_ReadProgramCount_m3_737 ();
extern "C" void Interpreter_Scan_m3_738 ();
extern "C" void Interpreter_Reset_m3_739 ();
extern "C" void Interpreter_Eval_m3_740 ();
extern "C" void Interpreter_EvalChar_m3_741 ();
extern "C" void Interpreter_TryMatch_m3_742 ();
extern "C" void Interpreter_IsPosition_m3_743 ();
extern "C" void Interpreter_IsWordChar_m3_744 ();
extern "C" void Interpreter_GetString_m3_745 ();
extern "C" void Interpreter_Open_m3_746 ();
extern "C" void Interpreter_Close_m3_747 ();
extern "C" void Interpreter_Balance_m3_748 ();
extern "C" void Interpreter_Checkpoint_m3_749 ();
extern "C" void Interpreter_Backtrack_m3_750 ();
extern "C" void Interpreter_ResetGroups_m3_751 ();
extern "C" void Interpreter_GetLastDefined_m3_752 ();
extern "C" void Interpreter_CreateMark_m3_753 ();
extern "C" void Interpreter_GetGroupInfo_m3_754 ();
extern "C" void Interpreter_PopulateGroup_m3_755 ();
extern "C" void Interpreter_GenerateMatch_m3_756 ();
extern "C" void Interval__ctor_m3_757 ();
extern "C" void Interval_get_Empty_m3_758 ();
extern "C" void Interval_get_IsDiscontiguous_m3_759 ();
extern "C" void Interval_get_IsSingleton_m3_760 ();
extern "C" void Interval_get_IsEmpty_m3_761 ();
extern "C" void Interval_get_Size_m3_762 ();
extern "C" void Interval_IsDisjoint_m3_763 ();
extern "C" void Interval_IsAdjacent_m3_764 ();
extern "C" void Interval_Contains_m3_765 ();
extern "C" void Interval_Contains_m3_766 ();
extern "C" void Interval_Intersects_m3_767 ();
extern "C" void Interval_Merge_m3_768 ();
extern "C" void Interval_CompareTo_m3_769 ();
extern "C" void Enumerator__ctor_m3_770 ();
extern "C" void Enumerator_get_Current_m3_771 ();
extern "C" void Enumerator_MoveNext_m3_772 ();
extern "C" void Enumerator_Reset_m3_773 ();
extern "C" void CostDelegate__ctor_m3_774 ();
extern "C" void CostDelegate_Invoke_m3_775 ();
extern "C" void CostDelegate_BeginInvoke_m3_776 ();
extern "C" void CostDelegate_EndInvoke_m3_777 ();
extern "C" void IntervalCollection__ctor_m3_778 ();
extern "C" void IntervalCollection_get_Item_m3_779 ();
extern "C" void IntervalCollection_Add_m3_780 ();
extern "C" void IntervalCollection_Normalize_m3_781 ();
extern "C" void IntervalCollection_GetMetaCollection_m3_782 ();
extern "C" void IntervalCollection_Optimize_m3_783 ();
extern "C" void IntervalCollection_get_Count_m3_784 ();
extern "C" void IntervalCollection_get_SyncRoot_m3_785 ();
extern "C" void IntervalCollection_CopyTo_m3_786 ();
extern "C" void IntervalCollection_GetEnumerator_m3_787 ();
extern "C" void Parser__ctor_m3_788 ();
extern "C" void Parser_ParseDecimal_m3_789 ();
extern "C" void Parser_ParseOctal_m3_790 ();
extern "C" void Parser_ParseHex_m3_791 ();
extern "C" void Parser_ParseNumber_m3_792 ();
extern "C" void Parser_ParseName_m3_793 ();
extern "C" void Parser_ParseRegularExpression_m3_794 ();
extern "C" void Parser_GetMapping_m3_795 ();
extern "C" void Parser_ParseGroup_m3_796 ();
extern "C" void Parser_ParseGroupingConstruct_m3_797 ();
extern "C" void Parser_ParseAssertionType_m3_798 ();
extern "C" void Parser_ParseOptions_m3_799 ();
extern "C" void Parser_ParseCharacterClass_m3_800 ();
extern "C" void Parser_ParseRepetitionBounds_m3_801 ();
extern "C" void Parser_ParseUnicodeCategory_m3_802 ();
extern "C" void Parser_ParseSpecial_m3_803 ();
extern "C" void Parser_ParseEscape_m3_804 ();
extern "C" void Parser_ParseName_m3_805 ();
extern "C" void Parser_IsNameChar_m3_806 ();
extern "C" void Parser_ParseNumber_m3_807 ();
extern "C" void Parser_ParseDigit_m3_808 ();
extern "C" void Parser_ConsumeWhitespace_m3_809 ();
extern "C" void Parser_ResolveReferences_m3_810 ();
extern "C" void Parser_HandleExplicitNumericGroups_m3_811 ();
extern "C" void Parser_IsIgnoreCase_m3_812 ();
extern "C" void Parser_IsMultiline_m3_813 ();
extern "C" void Parser_IsExplicitCapture_m3_814 ();
extern "C" void Parser_IsSingleline_m3_815 ();
extern "C" void Parser_IsIgnorePatternWhitespace_m3_816 ();
extern "C" void Parser_IsECMAScript_m3_817 ();
extern "C" void Parser_NewParseException_m3_818 ();
extern "C" void QuickSearch__ctor_m3_819 ();
extern "C" void QuickSearch__cctor_m3_820 ();
extern "C" void QuickSearch_get_Length_m3_821 ();
extern "C" void QuickSearch_Search_m3_822 ();
extern "C" void QuickSearch_SetupShiftTable_m3_823 ();
extern "C" void QuickSearch_GetShiftDistance_m3_824 ();
extern "C" void QuickSearch_GetChar_m3_825 ();
extern "C" void ReplacementEvaluator__ctor_m3_826 ();
extern "C" void ReplacementEvaluator_Evaluate_m3_827 ();
extern "C" void ReplacementEvaluator_EvaluateAppend_m3_828 ();
extern "C" void ReplacementEvaluator_get_NeedsGroupsOrCaptures_m3_829 ();
extern "C" void ReplacementEvaluator_Ensure_m3_830 ();
extern "C" void ReplacementEvaluator_AddFromReplacement_m3_831 ();
extern "C" void ReplacementEvaluator_AddInt_m3_832 ();
extern "C" void ReplacementEvaluator_Compile_m3_833 ();
extern "C" void ReplacementEvaluator_CompileTerm_m3_834 ();
extern "C" void ExpressionCollection__ctor_m3_835 ();
extern "C" void ExpressionCollection_Add_m3_836 ();
extern "C" void ExpressionCollection_get_Item_m3_837 ();
extern "C" void ExpressionCollection_set_Item_m3_838 ();
extern "C" void ExpressionCollection_OnValidate_m3_839 ();
extern "C" void Expression__ctor_m3_840 ();
extern "C" void Expression_GetFixedWidth_m3_841 ();
extern "C" void Expression_GetAnchorInfo_m3_842 ();
extern "C" void CompositeExpression__ctor_m3_843 ();
extern "C" void CompositeExpression_get_Expressions_m3_844 ();
extern "C" void CompositeExpression_GetWidth_m3_845 ();
extern "C" void CompositeExpression_IsComplex_m3_846 ();
extern "C" void Group__ctor_m3_847 ();
extern "C" void Group_AppendExpression_m3_848 ();
extern "C" void Group_Compile_m3_849 ();
extern "C" void Group_GetWidth_m3_850 ();
extern "C" void Group_GetAnchorInfo_m3_851 ();
extern "C" void RegularExpression__ctor_m3_852 ();
extern "C" void RegularExpression_set_GroupCount_m3_853 ();
extern "C" void RegularExpression_Compile_m3_854 ();
extern "C" void CapturingGroup__ctor_m3_855 ();
extern "C" void CapturingGroup_get_Index_m3_856 ();
extern "C" void CapturingGroup_set_Index_m3_857 ();
extern "C" void CapturingGroup_get_Name_m3_858 ();
extern "C" void CapturingGroup_set_Name_m3_859 ();
extern "C" void CapturingGroup_get_IsNamed_m3_860 ();
extern "C" void CapturingGroup_Compile_m3_861 ();
extern "C" void CapturingGroup_IsComplex_m3_862 ();
extern "C" void CapturingGroup_CompareTo_m3_863 ();
extern "C" void BalancingGroup__ctor_m3_864 ();
extern "C" void BalancingGroup_set_Balance_m3_865 ();
extern "C" void BalancingGroup_Compile_m3_866 ();
extern "C" void NonBacktrackingGroup__ctor_m3_867 ();
extern "C" void NonBacktrackingGroup_Compile_m3_868 ();
extern "C" void NonBacktrackingGroup_IsComplex_m3_869 ();
extern "C" void Repetition__ctor_m3_870 ();
extern "C" void Repetition_get_Expression_m3_871 ();
extern "C" void Repetition_set_Expression_m3_872 ();
extern "C" void Repetition_get_Minimum_m3_873 ();
extern "C" void Repetition_Compile_m3_874 ();
extern "C" void Repetition_GetWidth_m3_875 ();
extern "C" void Repetition_GetAnchorInfo_m3_876 ();
extern "C" void Assertion__ctor_m3_877 ();
extern "C" void Assertion_get_TrueExpression_m3_878 ();
extern "C" void Assertion_set_TrueExpression_m3_879 ();
extern "C" void Assertion_get_FalseExpression_m3_880 ();
extern "C" void Assertion_set_FalseExpression_m3_881 ();
extern "C" void Assertion_GetWidth_m3_882 ();
extern "C" void CaptureAssertion__ctor_m3_883 ();
extern "C" void CaptureAssertion_set_CapturingGroup_m3_884 ();
extern "C" void CaptureAssertion_Compile_m3_885 ();
extern "C" void CaptureAssertion_IsComplex_m3_886 ();
extern "C" void CaptureAssertion_get_Alternate_m3_887 ();
extern "C" void ExpressionAssertion__ctor_m3_888 ();
extern "C" void ExpressionAssertion_set_Reverse_m3_889 ();
extern "C" void ExpressionAssertion_set_Negate_m3_890 ();
extern "C" void ExpressionAssertion_get_TestExpression_m3_891 ();
extern "C" void ExpressionAssertion_set_TestExpression_m3_892 ();
extern "C" void ExpressionAssertion_Compile_m3_893 ();
extern "C" void ExpressionAssertion_IsComplex_m3_894 ();
extern "C" void Alternation__ctor_m3_895 ();
extern "C" void Alternation_get_Alternatives_m3_896 ();
extern "C" void Alternation_AddAlternative_m3_897 ();
extern "C" void Alternation_Compile_m3_898 ();
extern "C" void Alternation_GetWidth_m3_899 ();
extern "C" void Literal__ctor_m3_900 ();
extern "C" void Literal_CompileLiteral_m3_901 ();
extern "C" void Literal_Compile_m3_902 ();
extern "C" void Literal_GetWidth_m3_903 ();
extern "C" void Literal_GetAnchorInfo_m3_904 ();
extern "C" void Literal_IsComplex_m3_905 ();
extern "C" void PositionAssertion__ctor_m3_906 ();
extern "C" void PositionAssertion_Compile_m3_907 ();
extern "C" void PositionAssertion_GetWidth_m3_908 ();
extern "C" void PositionAssertion_IsComplex_m3_909 ();
extern "C" void PositionAssertion_GetAnchorInfo_m3_910 ();
extern "C" void Reference__ctor_m3_911 ();
extern "C" void Reference_get_CapturingGroup_m3_912 ();
extern "C" void Reference_set_CapturingGroup_m3_913 ();
extern "C" void Reference_get_IgnoreCase_m3_914 ();
extern "C" void Reference_Compile_m3_915 ();
extern "C" void Reference_GetWidth_m3_916 ();
extern "C" void Reference_IsComplex_m3_917 ();
extern "C" void BackslashNumber__ctor_m3_918 ();
extern "C" void BackslashNumber_ResolveReference_m3_919 ();
extern "C" void BackslashNumber_Compile_m3_920 ();
extern "C" void CharacterClass__ctor_m3_921 ();
extern "C" void CharacterClass__ctor_m3_922 ();
extern "C" void CharacterClass__cctor_m3_923 ();
extern "C" void CharacterClass_AddCategory_m3_924 ();
extern "C" void CharacterClass_AddCharacter_m3_925 ();
extern "C" void CharacterClass_AddRange_m3_926 ();
extern "C" void CharacterClass_Compile_m3_927 ();
extern "C" void CharacterClass_GetWidth_m3_928 ();
extern "C" void CharacterClass_IsComplex_m3_929 ();
extern "C" void CharacterClass_GetIntervalCost_m3_930 ();
extern "C" void AnchorInfo__ctor_m3_931 ();
extern "C" void AnchorInfo__ctor_m3_932 ();
extern "C" void AnchorInfo__ctor_m3_933 ();
extern "C" void AnchorInfo_get_Offset_m3_934 ();
extern "C" void AnchorInfo_get_Width_m3_935 ();
extern "C" void AnchorInfo_get_Length_m3_936 ();
extern "C" void AnchorInfo_get_IsUnknownWidth_m3_937 ();
extern "C" void AnchorInfo_get_IsComplete_m3_938 ();
extern "C" void AnchorInfo_get_Substring_m3_939 ();
extern "C" void AnchorInfo_get_IgnoreCase_m3_940 ();
extern "C" void AnchorInfo_get_Position_m3_941 ();
extern "C" void AnchorInfo_get_IsSubstring_m3_942 ();
extern "C" void AnchorInfo_get_IsPosition_m3_943 ();
extern "C" void AnchorInfo_GetInterval_m3_944 ();
extern "C" void DefaultUriParser__ctor_m3_945 ();
extern "C" void DefaultUriParser__ctor_m3_946 ();
extern "C" void UriScheme__ctor_m3_947 ();
extern "C" void Uri__ctor_m3_948 ();
extern "C" void Uri__ctor_m3_949 ();
extern "C" void Uri__ctor_m3_950 ();
extern "C" void Uri__cctor_m3_951 ();
extern "C" void Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m3_952 ();
extern "C" void Uri_get_AbsoluteUri_m3_953 ();
extern "C" void Uri_get_Authority_m3_954 ();
extern "C" void Uri_get_Host_m3_955 ();
extern "C" void Uri_get_IsFile_m3_956 ();
extern "C" void Uri_get_IsLoopback_m3_957 ();
extern "C" void Uri_get_IsUnc_m3_958 ();
extern "C" void Uri_get_Scheme_m3_959 ();
extern "C" void Uri_get_IsAbsoluteUri_m3_960 ();
extern "C" void Uri_CheckHostName_m3_961 ();
extern "C" void Uri_IsIPv4Address_m3_962 ();
extern "C" void Uri_IsDomainAddress_m3_963 ();
extern "C" void Uri_CheckSchemeName_m3_964 ();
extern "C" void Uri_IsAlpha_m3_965 ();
extern "C" void Uri_Equals_m3_966 ();
extern "C" void Uri_InternalEquals_m3_967 ();
extern "C" void Uri_GetHashCode_m3_968 ();
extern "C" void Uri_GetLeftPart_m3_969 ();
extern "C" void Uri_FromHex_m3_970 ();
extern "C" void Uri_HexEscape_m3_971 ();
extern "C" void Uri_IsHexDigit_m3_972 ();
extern "C" void Uri_IsHexEncoding_m3_973 ();
extern "C" void Uri_AppendQueryAndFragment_m3_974 ();
extern "C" void Uri_ToString_m3_975 ();
extern "C" void Uri_EscapeString_m3_976 ();
extern "C" void Uri_EscapeString_m3_977 ();
extern "C" void Uri_ParseUri_m3_978 ();
extern "C" void Uri_Unescape_m3_979 ();
extern "C" void Uri_Unescape_m3_980 ();
extern "C" void Uri_ParseAsWindowsUNC_m3_981 ();
extern "C" void Uri_ParseAsWindowsAbsoluteFilePath_m3_982 ();
extern "C" void Uri_ParseAsUnixAbsoluteFilePath_m3_983 ();
extern "C" void Uri_Parse_m3_984 ();
extern "C" void Uri_ParseNoExceptions_m3_985 ();
extern "C" void Uri_CompactEscaped_m3_986 ();
extern "C" void Uri_Reduce_m3_987 ();
extern "C" void Uri_HexUnescapeMultiByte_m3_988 ();
extern "C" void Uri_GetSchemeDelimiter_m3_989 ();
extern "C" void Uri_GetDefaultPort_m3_990 ();
extern "C" void Uri_GetOpaqueWiseSchemeDelimiter_m3_991 ();
extern "C" void Uri_IsPredefinedScheme_m3_992 ();
extern "C" void Uri_get_Parser_m3_993 ();
extern "C" void Uri_NeedToEscapeDataChar_m3_994 ();
extern "C" void Uri_EscapeDataString_m3_995 ();
extern "C" void Uri_EnsureAbsoluteUri_m3_996 ();
extern "C" void Uri_op_Equality_m3_997 ();
extern "C" void UriFormatException__ctor_m3_998 ();
extern "C" void UriFormatException__ctor_m3_999 ();
extern "C" void UriFormatException__ctor_m3_1000 ();
extern "C" void UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m3_1001 ();
extern "C" void UriParser__ctor_m3_1002 ();
extern "C" void UriParser__cctor_m3_1003 ();
extern "C" void UriParser_InitializeAndValidate_m3_1004 ();
extern "C" void UriParser_OnRegister_m3_1005 ();
extern "C" void UriParser_set_SchemeName_m3_1006 ();
extern "C" void UriParser_get_DefaultPort_m3_1007 ();
extern "C" void UriParser_set_DefaultPort_m3_1008 ();
extern "C" void UriParser_CreateDefaults_m3_1009 ();
extern "C" void UriParser_InternalRegister_m3_1010 ();
extern "C" void UriParser_GetParser_m3_1011 ();
extern "C" void RemoteCertificateValidationCallback__ctor_m3_1012 ();
extern "C" void RemoteCertificateValidationCallback_Invoke_m3_1013 ();
extern "C" void RemoteCertificateValidationCallback_BeginInvoke_m3_1014 ();
extern "C" void RemoteCertificateValidationCallback_EndInvoke_m3_1015 ();
extern "C" void MatchEvaluator__ctor_m3_1016 ();
extern "C" void MatchEvaluator_Invoke_m3_1017 ();
extern "C" void MatchEvaluator_BeginInvoke_m3_1018 ();
extern "C" void MatchEvaluator_EndInvoke_m3_1019 ();
extern "C" void Locale_GetText_m4_49 ();
extern "C" void ModulusRing__ctor_m4_50 ();
extern "C" void ModulusRing_BarrettReduction_m4_51 ();
extern "C" void ModulusRing_Multiply_m4_52 ();
extern "C" void ModulusRing_Difference_m4_53 ();
extern "C" void ModulusRing_Pow_m4_54 ();
extern "C" void ModulusRing_Pow_m4_55 ();
extern "C" void Kernel_AddSameSign_m4_56 ();
extern "C" void Kernel_Subtract_m4_57 ();
extern "C" void Kernel_MinusEq_m4_58 ();
extern "C" void Kernel_PlusEq_m4_59 ();
extern "C" void Kernel_Compare_m4_60 ();
extern "C" void Kernel_SingleByteDivideInPlace_m4_61 ();
extern "C" void Kernel_DwordMod_m4_62 ();
extern "C" void Kernel_DwordDivMod_m4_63 ();
extern "C" void Kernel_multiByteDivide_m4_64 ();
extern "C" void Kernel_LeftShift_m4_65 ();
extern "C" void Kernel_RightShift_m4_66 ();
extern "C" void Kernel_Multiply_m4_67 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m4_68 ();
extern "C" void Kernel_modInverse_m4_69 ();
extern "C" void Kernel_modInverse_m4_70 ();
extern "C" void BigInteger__ctor_m4_71 ();
extern "C" void BigInteger__ctor_m4_72 ();
extern "C" void BigInteger__ctor_m4_73 ();
extern "C" void BigInteger__ctor_m4_74 ();
extern "C" void BigInteger__ctor_m4_75 ();
extern "C" void BigInteger__cctor_m4_76 ();
extern "C" void BigInteger_get_Rng_m4_77 ();
extern "C" void BigInteger_GenerateRandom_m4_78 ();
extern "C" void BigInteger_GenerateRandom_m4_79 ();
extern "C" void BigInteger_BitCount_m4_80 ();
extern "C" void BigInteger_TestBit_m4_81 ();
extern "C" void BigInteger_SetBit_m4_82 ();
extern "C" void BigInteger_SetBit_m4_83 ();
extern "C" void BigInteger_LowestSetBit_m4_84 ();
extern "C" void BigInteger_GetBytes_m4_85 ();
extern "C" void BigInteger_ToString_m4_86 ();
extern "C" void BigInteger_ToString_m4_87 ();
extern "C" void BigInteger_Normalize_m4_88 ();
extern "C" void BigInteger_Clear_m4_89 ();
extern "C" void BigInteger_GetHashCode_m4_90 ();
extern "C" void BigInteger_ToString_m4_91 ();
extern "C" void BigInteger_Equals_m4_92 ();
extern "C" void BigInteger_ModInverse_m4_93 ();
extern "C" void BigInteger_ModPow_m4_94 ();
extern "C" void BigInteger_GeneratePseudoPrime_m4_95 ();
extern "C" void BigInteger_Incr2_m4_96 ();
extern "C" void BigInteger_op_Implicit_m4_97 ();
extern "C" void BigInteger_op_Implicit_m4_98 ();
extern "C" void BigInteger_op_Addition_m4_99 ();
extern "C" void BigInteger_op_Subtraction_m4_100 ();
extern "C" void BigInteger_op_Modulus_m4_101 ();
extern "C" void BigInteger_op_Modulus_m4_102 ();
extern "C" void BigInteger_op_Division_m4_103 ();
extern "C" void BigInteger_op_Multiply_m4_104 ();
extern "C" void BigInteger_op_LeftShift_m4_105 ();
extern "C" void BigInteger_op_RightShift_m4_106 ();
extern "C" void BigInteger_op_Equality_m4_107 ();
extern "C" void BigInteger_op_Inequality_m4_108 ();
extern "C" void BigInteger_op_Equality_m4_109 ();
extern "C" void BigInteger_op_Inequality_m4_110 ();
extern "C" void BigInteger_op_GreaterThan_m4_111 ();
extern "C" void BigInteger_op_LessThan_m4_112 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m4_113 ();
extern "C" void BigInteger_op_LessThanOrEqual_m4_114 ();
extern "C" void PrimalityTests_GetSPPRounds_m4_115 ();
extern "C" void PrimalityTests_RabinMillerTest_m4_116 ();
extern "C" void PrimeGeneratorBase__ctor_m4_117 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m4_118 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m4_119 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m4_120 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m4_121 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m4_122 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m4_123 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m4_124 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m4_125 ();
extern "C" void ASN1__ctor_m4_9 ();
extern "C" void ASN1__ctor_m4_10 ();
extern "C" void ASN1__ctor_m4_2 ();
extern "C" void ASN1_get_Count_m4_5 ();
extern "C" void ASN1_get_Tag_m4_3 ();
extern "C" void ASN1_get_Length_m4_17 ();
extern "C" void ASN1_get_Value_m4_4 ();
extern "C" void ASN1_set_Value_m4_126 ();
extern "C" void ASN1_CompareArray_m4_127 ();
extern "C" void ASN1_CompareValue_m4_16 ();
extern "C" void ASN1_Add_m4_11 ();
extern "C" void ASN1_GetBytes_m4_128 ();
extern "C" void ASN1_Decode_m4_129 ();
extern "C" void ASN1_DecodeTLV_m4_130 ();
extern "C" void ASN1_get_Item_m4_6 ();
extern "C" void ASN1_Element_m4_131 ();
extern "C" void ASN1_ToString_m4_132 ();
extern "C" void ASN1Convert_FromInt32_m4_12 ();
extern "C" void ASN1Convert_FromOid_m4_133 ();
extern "C" void ASN1Convert_ToInt32_m4_8 ();
extern "C" void ASN1Convert_ToOid_m4_40 ();
extern "C" void ASN1Convert_ToDateTime_m4_134 ();
extern "C" void BitConverterLE_GetUIntBytes_m4_135 ();
extern "C" void BitConverterLE_GetBytes_m4_136 ();
extern "C" void ContentInfo__ctor_m4_137 ();
extern "C" void ContentInfo__ctor_m4_138 ();
extern "C" void ContentInfo__ctor_m4_139 ();
extern "C" void ContentInfo__ctor_m4_140 ();
extern "C" void ContentInfo_get_ASN1_m4_141 ();
extern "C" void ContentInfo_get_Content_m4_142 ();
extern "C" void ContentInfo_set_Content_m4_143 ();
extern "C" void ContentInfo_get_ContentType_m4_144 ();
extern "C" void ContentInfo_set_ContentType_m4_145 ();
extern "C" void ContentInfo_GetASN1_m4_146 ();
extern "C" void EncryptedData__ctor_m4_147 ();
extern "C" void EncryptedData__ctor_m4_148 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m4_149 ();
extern "C" void EncryptedData_get_EncryptedContent_m4_150 ();
extern "C" void ARC4Managed__ctor_m4_151 ();
extern "C" void ARC4Managed_Finalize_m4_152 ();
extern "C" void ARC4Managed_Dispose_m4_153 ();
extern "C" void ARC4Managed_get_Key_m4_154 ();
extern "C" void ARC4Managed_set_Key_m4_155 ();
extern "C" void ARC4Managed_get_CanReuseTransform_m4_156 ();
extern "C" void ARC4Managed_CreateEncryptor_m4_157 ();
extern "C" void ARC4Managed_CreateDecryptor_m4_158 ();
extern "C" void ARC4Managed_GenerateIV_m4_159 ();
extern "C" void ARC4Managed_GenerateKey_m4_160 ();
extern "C" void ARC4Managed_KeySetup_m4_161 ();
extern "C" void ARC4Managed_CheckInput_m4_162 ();
extern "C" void ARC4Managed_TransformBlock_m4_163 ();
extern "C" void ARC4Managed_InternalTransformBlock_m4_164 ();
extern "C" void ARC4Managed_TransformFinalBlock_m4_165 ();
extern "C" void CryptoConvert_ToHex_m4_48 ();
extern "C" void KeyBuilder_get_Rng_m4_166 ();
extern "C" void KeyBuilder_Key_m4_167 ();
extern "C" void MD2__ctor_m4_168 ();
extern "C" void MD2_Create_m4_169 ();
extern "C" void MD2_Create_m4_170 ();
extern "C" void MD2Managed__ctor_m4_171 ();
extern "C" void MD2Managed__cctor_m4_172 ();
extern "C" void MD2Managed_Padding_m4_173 ();
extern "C" void MD2Managed_Initialize_m4_174 ();
extern "C" void MD2Managed_HashCore_m4_175 ();
extern "C" void MD2Managed_HashFinal_m4_176 ();
extern "C" void MD2Managed_MD2Transform_m4_177 ();
extern "C" void PKCS1__cctor_m4_178 ();
extern "C" void PKCS1_Compare_m4_179 ();
extern "C" void PKCS1_I2OSP_m4_180 ();
extern "C" void PKCS1_OS2IP_m4_181 ();
extern "C" void PKCS1_RSASP1_m4_182 ();
extern "C" void PKCS1_RSAVP1_m4_183 ();
extern "C" void PKCS1_Sign_v15_m4_184 ();
extern "C" void PKCS1_Verify_v15_m4_185 ();
extern "C" void PKCS1_Verify_v15_m4_186 ();
extern "C" void PKCS1_Encode_v15_m4_187 ();
extern "C" void PrivateKeyInfo__ctor_m4_188 ();
extern "C" void PrivateKeyInfo__ctor_m4_189 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m4_190 ();
extern "C" void PrivateKeyInfo_Decode_m4_191 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m4_192 ();
extern "C" void PrivateKeyInfo_Normalize_m4_193 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m4_194 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m4_195 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m4_196 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m4_197 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m4_198 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m4_199 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m4_200 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m4_201 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m4_202 ();
extern "C" void RC4__ctor_m4_203 ();
extern "C" void RC4__cctor_m4_204 ();
extern "C" void RC4_get_IV_m4_205 ();
extern "C" void RC4_set_IV_m4_206 ();
extern "C" void KeyGeneratedEventHandler__ctor_m4_207 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m4_208 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m4_209 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m4_210 ();
extern "C" void RSAManaged__ctor_m4_211 ();
extern "C" void RSAManaged__ctor_m4_212 ();
extern "C" void RSAManaged_Finalize_m4_213 ();
extern "C" void RSAManaged_GenerateKeyPair_m4_214 ();
extern "C" void RSAManaged_get_KeySize_m4_215 ();
extern "C" void RSAManaged_get_PublicOnly_m4_0 ();
extern "C" void RSAManaged_DecryptValue_m4_216 ();
extern "C" void RSAManaged_EncryptValue_m4_217 ();
extern "C" void RSAManaged_ExportParameters_m4_218 ();
extern "C" void RSAManaged_ImportParameters_m4_219 ();
extern "C" void RSAManaged_Dispose_m4_220 ();
extern "C" void RSAManaged_ToXmlString_m4_221 ();
extern "C" void RSAManaged_GetPaddedValue_m4_222 ();
extern "C" void SafeBag__ctor_m4_223 ();
extern "C" void SafeBag_get_BagOID_m4_224 ();
extern "C" void SafeBag_get_ASN1_m4_225 ();
extern "C" void DeriveBytes__ctor_m4_226 ();
extern "C" void DeriveBytes__cctor_m4_227 ();
extern "C" void DeriveBytes_set_HashName_m4_228 ();
extern "C" void DeriveBytes_set_IterationCount_m4_229 ();
extern "C" void DeriveBytes_set_Password_m4_230 ();
extern "C" void DeriveBytes_set_Salt_m4_231 ();
extern "C" void DeriveBytes_Adjust_m4_232 ();
extern "C" void DeriveBytes_Derive_m4_233 ();
extern "C" void DeriveBytes_DeriveKey_m4_234 ();
extern "C" void DeriveBytes_DeriveIV_m4_235 ();
extern "C" void DeriveBytes_DeriveMAC_m4_236 ();
extern "C" void PKCS12__ctor_m4_237 ();
extern "C" void PKCS12__ctor_m4_18 ();
extern "C" void PKCS12__ctor_m4_19 ();
extern "C" void PKCS12__cctor_m4_238 ();
extern "C" void PKCS12_Decode_m4_239 ();
extern "C" void PKCS12_Finalize_m4_240 ();
extern "C" void PKCS12_set_Password_m4_241 ();
extern "C" void PKCS12_get_IterationCount_m4_242 ();
extern "C" void PKCS12_set_IterationCount_m4_243 ();
extern "C" void PKCS12_get_Keys_m4_22 ();
extern "C" void PKCS12_get_Certificates_m4_20 ();
extern "C" void PKCS12_get_RNG_m4_244 ();
extern "C" void PKCS12_Compare_m4_245 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m4_246 ();
extern "C" void PKCS12_Decrypt_m4_247 ();
extern "C" void PKCS12_Decrypt_m4_248 ();
extern "C" void PKCS12_Encrypt_m4_249 ();
extern "C" void PKCS12_GetExistingParameters_m4_250 ();
extern "C" void PKCS12_AddPrivateKey_m4_251 ();
extern "C" void PKCS12_ReadSafeBag_m4_252 ();
extern "C" void PKCS12_CertificateSafeBag_m4_253 ();
extern "C" void PKCS12_MAC_m4_254 ();
extern "C" void PKCS12_GetBytes_m4_255 ();
extern "C" void PKCS12_EncryptedContentInfo_m4_256 ();
extern "C" void PKCS12_AddCertificate_m4_257 ();
extern "C" void PKCS12_AddCertificate_m4_258 ();
extern "C" void PKCS12_RemoveCertificate_m4_259 ();
extern "C" void PKCS12_RemoveCertificate_m4_260 ();
extern "C" void PKCS12_Clone_m4_261 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m4_262 ();
extern "C" void X501__cctor_m4_263 ();
extern "C" void X501_ToString_m4_264 ();
extern "C" void X501_ToString_m4_7 ();
extern "C" void X501_AppendEntry_m4_265 ();
extern "C" void X509Certificate__ctor_m4_24 ();
extern "C" void X509Certificate__cctor_m4_266 ();
extern "C" void X509Certificate_Parse_m4_267 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m4_268 ();
extern "C" void X509Certificate_get_DSA_m4_1 ();
extern "C" void X509Certificate_set_DSA_m4_23 ();
extern "C" void X509Certificate_get_Extensions_m4_26 ();
extern "C" void X509Certificate_get_Hash_m4_269 ();
extern "C" void X509Certificate_get_IssuerName_m4_270 ();
extern "C" void X509Certificate_get_KeyAlgorithm_m4_271 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m4_272 ();
extern "C" void X509Certificate_set_KeyAlgorithmParameters_m4_273 ();
extern "C" void X509Certificate_get_PublicKey_m4_274 ();
extern "C" void X509Certificate_get_RSA_m4_275 ();
extern "C" void X509Certificate_set_RSA_m4_276 ();
extern "C" void X509Certificate_get_RawData_m4_277 ();
extern "C" void X509Certificate_get_SerialNumber_m4_278 ();
extern "C" void X509Certificate_get_Signature_m4_279 ();
extern "C" void X509Certificate_get_SignatureAlgorithm_m4_280 ();
extern "C" void X509Certificate_get_SubjectName_m4_281 ();
extern "C" void X509Certificate_get_ValidFrom_m4_282 ();
extern "C" void X509Certificate_get_ValidUntil_m4_283 ();
extern "C" void X509Certificate_get_Version_m4_15 ();
extern "C" void X509Certificate_get_IsCurrent_m4_284 ();
extern "C" void X509Certificate_WasCurrent_m4_285 ();
extern "C" void X509Certificate_VerifySignature_m4_286 ();
extern "C" void X509Certificate_VerifySignature_m4_287 ();
extern "C" void X509Certificate_VerifySignature_m4_25 ();
extern "C" void X509Certificate_get_IsSelfSigned_m4_288 ();
extern "C" void X509Certificate_GetIssuerName_m4_13 ();
extern "C" void X509Certificate_GetSubjectName_m4_14 ();
extern "C" void X509Certificate_GetObjectData_m4_289 ();
extern "C" void X509Certificate_PEM_m4_290 ();
extern "C" void X509CertificateEnumerator__ctor_m4_291 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m4_292 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m4_293 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m4_294 ();
extern "C" void X509CertificateEnumerator_get_Current_m4_47 ();
extern "C" void X509CertificateEnumerator_MoveNext_m4_295 ();
extern "C" void X509CertificateEnumerator_Reset_m4_296 ();
extern "C" void X509CertificateCollection__ctor_m4_297 ();
extern "C" void X509CertificateCollection__ctor_m4_298 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m4_299 ();
extern "C" void X509CertificateCollection_get_Item_m4_21 ();
extern "C" void X509CertificateCollection_Add_m4_300 ();
extern "C" void X509CertificateCollection_AddRange_m4_301 ();
extern "C" void X509CertificateCollection_Contains_m4_302 ();
extern "C" void X509CertificateCollection_GetEnumerator_m4_46 ();
extern "C" void X509CertificateCollection_GetHashCode_m4_303 ();
extern "C" void X509CertificateCollection_IndexOf_m4_304 ();
extern "C" void X509CertificateCollection_Remove_m4_305 ();
extern "C" void X509CertificateCollection_Compare_m4_306 ();
extern "C" void X509Chain__ctor_m4_307 ();
extern "C" void X509Chain__ctor_m4_308 ();
extern "C" void X509Chain_get_Status_m4_309 ();
extern "C" void X509Chain_get_TrustAnchors_m4_310 ();
extern "C" void X509Chain_Build_m4_311 ();
extern "C" void X509Chain_IsValid_m4_312 ();
extern "C" void X509Chain_FindCertificateParent_m4_313 ();
extern "C" void X509Chain_FindCertificateRoot_m4_314 ();
extern "C" void X509Chain_IsTrusted_m4_315 ();
extern "C" void X509Chain_IsParent_m4_316 ();
extern "C" void X509CrlEntry__ctor_m4_317 ();
extern "C" void X509CrlEntry_get_SerialNumber_m4_318 ();
extern "C" void X509CrlEntry_get_RevocationDate_m4_33 ();
extern "C" void X509CrlEntry_get_Extensions_m4_39 ();
extern "C" void X509Crl__ctor_m4_319 ();
extern "C" void X509Crl_Parse_m4_320 ();
extern "C" void X509Crl_get_Extensions_m4_28 ();
extern "C" void X509Crl_get_Hash_m4_321 ();
extern "C" void X509Crl_get_IssuerName_m4_36 ();
extern "C" void X509Crl_get_NextUpdate_m4_34 ();
extern "C" void X509Crl_Compare_m4_322 ();
extern "C" void X509Crl_GetCrlEntry_m4_32 ();
extern "C" void X509Crl_GetCrlEntry_m4_323 ();
extern "C" void X509Crl_GetHashName_m4_324 ();
extern "C" void X509Crl_VerifySignature_m4_325 ();
extern "C" void X509Crl_VerifySignature_m4_326 ();
extern "C" void X509Crl_VerifySignature_m4_31 ();
extern "C" void X509Extension__ctor_m4_327 ();
extern "C" void X509Extension__ctor_m4_328 ();
extern "C" void X509Extension_Decode_m4_329 ();
extern "C" void X509Extension_Encode_m4_330 ();
extern "C" void X509Extension_get_Oid_m4_38 ();
extern "C" void X509Extension_get_Critical_m4_37 ();
extern "C" void X509Extension_get_Value_m4_41 ();
extern "C" void X509Extension_Equals_m4_331 ();
extern "C" void X509Extension_GetHashCode_m4_332 ();
extern "C" void X509Extension_WriteLine_m4_333 ();
extern "C" void X509Extension_ToString_m4_334 ();
extern "C" void X509ExtensionCollection__ctor_m4_335 ();
extern "C" void X509ExtensionCollection__ctor_m4_336 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m4_337 ();
extern "C" void X509ExtensionCollection_IndexOf_m4_338 ();
extern "C" void X509ExtensionCollection_get_Item_m4_27 ();
extern "C" void X509Store__ctor_m4_339 ();
extern "C" void X509Store_get_Certificates_m4_45 ();
extern "C" void X509Store_get_Crls_m4_35 ();
extern "C" void X509Store_Load_m4_340 ();
extern "C" void X509Store_LoadCertificate_m4_341 ();
extern "C" void X509Store_LoadCrl_m4_342 ();
extern "C" void X509Store_CheckStore_m4_343 ();
extern "C" void X509Store_BuildCertificatesCollection_m4_344 ();
extern "C" void X509Store_BuildCrlsCollection_m4_345 ();
extern "C" void X509StoreManager_get_CurrentUser_m4_42 ();
extern "C" void X509StoreManager_get_LocalMachine_m4_43 ();
extern "C" void X509StoreManager_get_TrustedRootCertificates_m4_346 ();
extern "C" void X509Stores__ctor_m4_347 ();
extern "C" void X509Stores_get_TrustedRoot_m4_348 ();
extern "C" void X509Stores_Open_m4_44 ();
extern "C" void AuthorityKeyIdentifierExtension__ctor_m4_29 ();
extern "C" void AuthorityKeyIdentifierExtension_Decode_m4_349 ();
extern "C" void AuthorityKeyIdentifierExtension_get_Identifier_m4_30 ();
extern "C" void AuthorityKeyIdentifierExtension_ToString_m4_350 ();
extern "C" void BasicConstraintsExtension__ctor_m4_351 ();
extern "C" void BasicConstraintsExtension_Decode_m4_352 ();
extern "C" void BasicConstraintsExtension_Encode_m4_353 ();
extern "C" void BasicConstraintsExtension_get_CertificateAuthority_m4_354 ();
extern "C" void BasicConstraintsExtension_ToString_m4_355 ();
extern "C" void ExtendedKeyUsageExtension__ctor_m4_356 ();
extern "C" void ExtendedKeyUsageExtension_Decode_m4_357 ();
extern "C" void ExtendedKeyUsageExtension_Encode_m4_358 ();
extern "C" void ExtendedKeyUsageExtension_get_KeyPurpose_m4_359 ();
extern "C" void ExtendedKeyUsageExtension_ToString_m4_360 ();
extern "C" void GeneralNames__ctor_m4_361 ();
extern "C" void GeneralNames_get_DNSNames_m4_362 ();
extern "C" void GeneralNames_get_IPAddresses_m4_363 ();
extern "C" void GeneralNames_ToString_m4_364 ();
extern "C" void KeyUsageExtension__ctor_m4_365 ();
extern "C" void KeyUsageExtension_Decode_m4_366 ();
extern "C" void KeyUsageExtension_Encode_m4_367 ();
extern "C" void KeyUsageExtension_Support_m4_368 ();
extern "C" void KeyUsageExtension_ToString_m4_369 ();
extern "C" void NetscapeCertTypeExtension__ctor_m4_370 ();
extern "C" void NetscapeCertTypeExtension_Decode_m4_371 ();
extern "C" void NetscapeCertTypeExtension_Support_m4_372 ();
extern "C" void NetscapeCertTypeExtension_ToString_m4_373 ();
extern "C" void SubjectAltNameExtension__ctor_m4_374 ();
extern "C" void SubjectAltNameExtension_Decode_m4_375 ();
extern "C" void SubjectAltNameExtension_get_DNSNames_m4_376 ();
extern "C" void SubjectAltNameExtension_get_IPAddresses_m4_377 ();
extern "C" void SubjectAltNameExtension_ToString_m4_378 ();
extern "C" void HMAC__ctor_m4_379 ();
extern "C" void HMAC_get_Key_m4_380 ();
extern "C" void HMAC_set_Key_m4_381 ();
extern "C" void HMAC_Initialize_m4_382 ();
extern "C" void HMAC_HashFinal_m4_383 ();
extern "C" void HMAC_HashCore_m4_384 ();
extern "C" void HMAC_initializePad_m4_385 ();
extern "C" void MD5SHA1__ctor_m4_386 ();
extern "C" void MD5SHA1_Initialize_m4_387 ();
extern "C" void MD5SHA1_HashFinal_m4_388 ();
extern "C" void MD5SHA1_HashCore_m4_389 ();
extern "C" void MD5SHA1_CreateSignature_m4_390 ();
extern "C" void MD5SHA1_VerifySignature_m4_391 ();
extern "C" void Alert__ctor_m4_392 ();
extern "C" void Alert__ctor_m4_393 ();
extern "C" void Alert_get_Level_m4_394 ();
extern "C" void Alert_get_Description_m4_395 ();
extern "C" void Alert_get_IsWarning_m4_396 ();
extern "C" void Alert_get_IsCloseNotify_m4_397 ();
extern "C" void Alert_inferAlertLevel_m4_398 ();
extern "C" void Alert_GetAlertMessage_m4_399 ();
extern "C" void CipherSuite__ctor_m4_400 ();
extern "C" void CipherSuite__cctor_m4_401 ();
extern "C" void CipherSuite_get_EncryptionCipher_m4_402 ();
extern "C" void CipherSuite_get_DecryptionCipher_m4_403 ();
extern "C" void CipherSuite_get_ClientHMAC_m4_404 ();
extern "C" void CipherSuite_get_ServerHMAC_m4_405 ();
extern "C" void CipherSuite_get_CipherAlgorithmType_m4_406 ();
extern "C" void CipherSuite_get_HashAlgorithmName_m4_407 ();
extern "C" void CipherSuite_get_HashAlgorithmType_m4_408 ();
extern "C" void CipherSuite_get_HashSize_m4_409 ();
extern "C" void CipherSuite_get_ExchangeAlgorithmType_m4_410 ();
extern "C" void CipherSuite_get_CipherMode_m4_411 ();
extern "C" void CipherSuite_get_Code_m4_412 ();
extern "C" void CipherSuite_get_Name_m4_413 ();
extern "C" void CipherSuite_get_IsExportable_m4_414 ();
extern "C" void CipherSuite_get_KeyMaterialSize_m4_415 ();
extern "C" void CipherSuite_get_KeyBlockSize_m4_416 ();
extern "C" void CipherSuite_get_ExpandedKeyMaterialSize_m4_417 ();
extern "C" void CipherSuite_get_EffectiveKeyBits_m4_418 ();
extern "C" void CipherSuite_get_IvSize_m4_419 ();
extern "C" void CipherSuite_get_Context_m4_420 ();
extern "C" void CipherSuite_set_Context_m4_421 ();
extern "C" void CipherSuite_Write_m4_422 ();
extern "C" void CipherSuite_Write_m4_423 ();
extern "C" void CipherSuite_InitializeCipher_m4_424 ();
extern "C" void CipherSuite_EncryptRecord_m4_425 ();
extern "C" void CipherSuite_DecryptRecord_m4_426 ();
extern "C" void CipherSuite_CreatePremasterSecret_m4_427 ();
extern "C" void CipherSuite_PRF_m4_428 ();
extern "C" void CipherSuite_Expand_m4_429 ();
extern "C" void CipherSuite_createEncryptionCipher_m4_430 ();
extern "C" void CipherSuite_createDecryptionCipher_m4_431 ();
extern "C" void CipherSuiteCollection__ctor_m4_432 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_get_Item_m4_433 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_set_Item_m4_434 ();
extern "C" void CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m4_435 ();
extern "C" void CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m4_436 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Contains_m4_437 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_IndexOf_m4_438 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Insert_m4_439 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Remove_m4_440 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_RemoveAt_m4_441 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Add_m4_442 ();
extern "C" void CipherSuiteCollection_get_Item_m4_443 ();
extern "C" void CipherSuiteCollection_get_Item_m4_444 ();
extern "C" void CipherSuiteCollection_set_Item_m4_445 ();
extern "C" void CipherSuiteCollection_get_Item_m4_446 ();
extern "C" void CipherSuiteCollection_get_Count_m4_447 ();
extern "C" void CipherSuiteCollection_CopyTo_m4_448 ();
extern "C" void CipherSuiteCollection_Clear_m4_449 ();
extern "C" void CipherSuiteCollection_IndexOf_m4_450 ();
extern "C" void CipherSuiteCollection_IndexOf_m4_451 ();
extern "C" void CipherSuiteCollection_Add_m4_452 ();
extern "C" void CipherSuiteCollection_add_m4_453 ();
extern "C" void CipherSuiteCollection_add_m4_454 ();
extern "C" void CipherSuiteCollection_cultureAwareCompare_m4_455 ();
extern "C" void CipherSuiteFactory_GetSupportedCiphers_m4_456 ();
extern "C" void CipherSuiteFactory_GetTls1SupportedCiphers_m4_457 ();
extern "C" void CipherSuiteFactory_GetSsl3SupportedCiphers_m4_458 ();
extern "C" void ClientContext__ctor_m4_459 ();
extern "C" void ClientContext_get_SslStream_m4_460 ();
extern "C" void ClientContext_get_ClientHelloProtocol_m4_461 ();
extern "C" void ClientContext_set_ClientHelloProtocol_m4_462 ();
extern "C" void ClientContext_Clear_m4_463 ();
extern "C" void ClientRecordProtocol__ctor_m4_464 ();
extern "C" void ClientRecordProtocol_GetMessage_m4_465 ();
extern "C" void ClientRecordProtocol_ProcessHandshakeMessage_m4_466 ();
extern "C" void ClientRecordProtocol_createClientHandshakeMessage_m4_467 ();
extern "C" void ClientRecordProtocol_createServerHandshakeMessage_m4_468 ();
extern "C" void ClientSessionInfo__ctor_m4_469 ();
extern "C" void ClientSessionInfo__cctor_m4_470 ();
extern "C" void ClientSessionInfo_Finalize_m4_471 ();
extern "C" void ClientSessionInfo_get_HostName_m4_472 ();
extern "C" void ClientSessionInfo_get_Id_m4_473 ();
extern "C" void ClientSessionInfo_get_Valid_m4_474 ();
extern "C" void ClientSessionInfo_GetContext_m4_475 ();
extern "C" void ClientSessionInfo_SetContext_m4_476 ();
extern "C" void ClientSessionInfo_KeepAlive_m4_477 ();
extern "C" void ClientSessionInfo_Dispose_m4_478 ();
extern "C" void ClientSessionInfo_Dispose_m4_479 ();
extern "C" void ClientSessionInfo_CheckDisposed_m4_480 ();
extern "C" void ClientSessionCache__cctor_m4_481 ();
extern "C" void ClientSessionCache_Add_m4_482 ();
extern "C" void ClientSessionCache_FromHost_m4_483 ();
extern "C" void ClientSessionCache_FromContext_m4_484 ();
extern "C" void ClientSessionCache_SetContextInCache_m4_485 ();
extern "C" void ClientSessionCache_SetContextFromCache_m4_486 ();
extern "C" void Context__ctor_m4_487 ();
extern "C" void Context_get_AbbreviatedHandshake_m4_488 ();
extern "C" void Context_set_AbbreviatedHandshake_m4_489 ();
extern "C" void Context_get_ProtocolNegotiated_m4_490 ();
extern "C" void Context_set_ProtocolNegotiated_m4_491 ();
extern "C" void Context_get_SecurityProtocol_m4_492 ();
extern "C" void Context_set_SecurityProtocol_m4_493 ();
extern "C" void Context_get_SecurityProtocolFlags_m4_494 ();
extern "C" void Context_get_Protocol_m4_495 ();
extern "C" void Context_get_SessionId_m4_496 ();
extern "C" void Context_set_SessionId_m4_497 ();
extern "C" void Context_get_CompressionMethod_m4_498 ();
extern "C" void Context_set_CompressionMethod_m4_499 ();
extern "C" void Context_get_ServerSettings_m4_500 ();
extern "C" void Context_get_ClientSettings_m4_501 ();
extern "C" void Context_get_LastHandshakeMsg_m4_502 ();
extern "C" void Context_set_LastHandshakeMsg_m4_503 ();
extern "C" void Context_get_HandshakeState_m4_504 ();
extern "C" void Context_set_HandshakeState_m4_505 ();
extern "C" void Context_get_ReceivedConnectionEnd_m4_506 ();
extern "C" void Context_set_ReceivedConnectionEnd_m4_507 ();
extern "C" void Context_get_SentConnectionEnd_m4_508 ();
extern "C" void Context_set_SentConnectionEnd_m4_509 ();
extern "C" void Context_get_SupportedCiphers_m4_510 ();
extern "C" void Context_set_SupportedCiphers_m4_511 ();
extern "C" void Context_get_HandshakeMessages_m4_512 ();
extern "C" void Context_get_WriteSequenceNumber_m4_513 ();
extern "C" void Context_set_WriteSequenceNumber_m4_514 ();
extern "C" void Context_get_ReadSequenceNumber_m4_515 ();
extern "C" void Context_set_ReadSequenceNumber_m4_516 ();
extern "C" void Context_get_ClientRandom_m4_517 ();
extern "C" void Context_set_ClientRandom_m4_518 ();
extern "C" void Context_get_ServerRandom_m4_519 ();
extern "C" void Context_set_ServerRandom_m4_520 ();
extern "C" void Context_get_RandomCS_m4_521 ();
extern "C" void Context_set_RandomCS_m4_522 ();
extern "C" void Context_get_RandomSC_m4_523 ();
extern "C" void Context_set_RandomSC_m4_524 ();
extern "C" void Context_get_MasterSecret_m4_525 ();
extern "C" void Context_set_MasterSecret_m4_526 ();
extern "C" void Context_get_ClientWriteKey_m4_527 ();
extern "C" void Context_set_ClientWriteKey_m4_528 ();
extern "C" void Context_get_ServerWriteKey_m4_529 ();
extern "C" void Context_set_ServerWriteKey_m4_530 ();
extern "C" void Context_get_ClientWriteIV_m4_531 ();
extern "C" void Context_set_ClientWriteIV_m4_532 ();
extern "C" void Context_get_ServerWriteIV_m4_533 ();
extern "C" void Context_set_ServerWriteIV_m4_534 ();
extern "C" void Context_get_RecordProtocol_m4_535 ();
extern "C" void Context_set_RecordProtocol_m4_536 ();
extern "C" void Context_GetUnixTime_m4_537 ();
extern "C" void Context_GetSecureRandomBytes_m4_538 ();
extern "C" void Context_Clear_m4_539 ();
extern "C" void Context_ClearKeyInfo_m4_540 ();
extern "C" void Context_DecodeProtocolCode_m4_541 ();
extern "C" void Context_ChangeProtocol_m4_542 ();
extern "C" void Context_get_Current_m4_543 ();
extern "C" void Context_get_Negotiating_m4_544 ();
extern "C" void Context_get_Read_m4_545 ();
extern "C" void Context_get_Write_m4_546 ();
extern "C" void Context_StartSwitchingSecurityParameters_m4_547 ();
extern "C" void Context_EndSwitchingSecurityParameters_m4_548 ();
extern "C" void HttpsClientStream__ctor_m4_549 ();
extern "C" void HttpsClientStream_get_TrustFailure_m4_550 ();
extern "C" void HttpsClientStream_RaiseServerCertificateValidation_m4_551 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__0_m4_552 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__1_m4_553 ();
extern "C" void ReceiveRecordAsyncResult__ctor_m4_554 ();
extern "C" void ReceiveRecordAsyncResult_get_Record_m4_555 ();
extern "C" void ReceiveRecordAsyncResult_get_ResultingBuffer_m4_556 ();
extern "C" void ReceiveRecordAsyncResult_get_InitialBuffer_m4_557 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncState_m4_558 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncException_m4_559 ();
extern "C" void ReceiveRecordAsyncResult_get_CompletedWithError_m4_560 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncWaitHandle_m4_561 ();
extern "C" void ReceiveRecordAsyncResult_get_IsCompleted_m4_562 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m4_563 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m4_564 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m4_565 ();
extern "C" void SendRecordAsyncResult__ctor_m4_566 ();
extern "C" void SendRecordAsyncResult_get_Message_m4_567 ();
extern "C" void SendRecordAsyncResult_get_AsyncState_m4_568 ();
extern "C" void SendRecordAsyncResult_get_AsyncException_m4_569 ();
extern "C" void SendRecordAsyncResult_get_CompletedWithError_m4_570 ();
extern "C" void SendRecordAsyncResult_get_AsyncWaitHandle_m4_571 ();
extern "C" void SendRecordAsyncResult_get_IsCompleted_m4_572 ();
extern "C" void SendRecordAsyncResult_SetComplete_m4_573 ();
extern "C" void SendRecordAsyncResult_SetComplete_m4_574 ();
extern "C" void RecordProtocol__ctor_m4_575 ();
extern "C" void RecordProtocol__cctor_m4_576 ();
extern "C" void RecordProtocol_get_Context_m4_577 ();
extern "C" void RecordProtocol_SendRecord_m4_578 ();
extern "C" void RecordProtocol_ProcessChangeCipherSpec_m4_579 ();
extern "C" void RecordProtocol_GetMessage_m4_580 ();
extern "C" void RecordProtocol_BeginReceiveRecord_m4_581 ();
extern "C" void RecordProtocol_InternalReceiveRecordCallback_m4_582 ();
extern "C" void RecordProtocol_EndReceiveRecord_m4_583 ();
extern "C" void RecordProtocol_ReceiveRecord_m4_584 ();
extern "C" void RecordProtocol_ReadRecordBuffer_m4_585 ();
extern "C" void RecordProtocol_ReadClientHelloV2_m4_586 ();
extern "C" void RecordProtocol_ReadStandardRecordBuffer_m4_587 ();
extern "C" void RecordProtocol_ProcessAlert_m4_588 ();
extern "C" void RecordProtocol_SendAlert_m4_589 ();
extern "C" void RecordProtocol_SendAlert_m4_590 ();
extern "C" void RecordProtocol_SendAlert_m4_591 ();
extern "C" void RecordProtocol_SendChangeCipherSpec_m4_592 ();
extern "C" void RecordProtocol_BeginSendRecord_m4_593 ();
extern "C" void RecordProtocol_InternalSendRecordCallback_m4_594 ();
extern "C" void RecordProtocol_BeginSendRecord_m4_595 ();
extern "C" void RecordProtocol_EndSendRecord_m4_596 ();
extern "C" void RecordProtocol_SendRecord_m4_597 ();
extern "C" void RecordProtocol_EncodeRecord_m4_598 ();
extern "C" void RecordProtocol_EncodeRecord_m4_599 ();
extern "C" void RecordProtocol_encryptRecordFragment_m4_600 ();
extern "C" void RecordProtocol_decryptRecordFragment_m4_601 ();
extern "C" void RecordProtocol_Compare_m4_602 ();
extern "C" void RecordProtocol_ProcessCipherSpecV2Buffer_m4_603 ();
extern "C" void RecordProtocol_MapV2CipherCode_m4_604 ();
extern "C" void RSASslSignatureDeformatter__ctor_m4_605 ();
extern "C" void RSASslSignatureDeformatter_VerifySignature_m4_606 ();
extern "C" void RSASslSignatureDeformatter_SetHashAlgorithm_m4_607 ();
extern "C" void RSASslSignatureDeformatter_SetKey_m4_608 ();
extern "C" void RSASslSignatureFormatter__ctor_m4_609 ();
extern "C" void RSASslSignatureFormatter_CreateSignature_m4_610 ();
extern "C" void RSASslSignatureFormatter_SetHashAlgorithm_m4_611 ();
extern "C" void RSASslSignatureFormatter_SetKey_m4_612 ();
extern "C" void SecurityParameters__ctor_m4_613 ();
extern "C" void SecurityParameters_get_Cipher_m4_614 ();
extern "C" void SecurityParameters_set_Cipher_m4_615 ();
extern "C" void SecurityParameters_get_ClientWriteMAC_m4_616 ();
extern "C" void SecurityParameters_set_ClientWriteMAC_m4_617 ();
extern "C" void SecurityParameters_get_ServerWriteMAC_m4_618 ();
extern "C" void SecurityParameters_set_ServerWriteMAC_m4_619 ();
extern "C" void SecurityParameters_Clear_m4_620 ();
extern "C" void ValidationResult_get_Trusted_m4_621 ();
extern "C" void ValidationResult_get_ErrorCode_m4_622 ();
extern "C" void SslClientStream__ctor_m4_623 ();
extern "C" void SslClientStream__ctor_m4_624 ();
extern "C" void SslClientStream__ctor_m4_625 ();
extern "C" void SslClientStream__ctor_m4_626 ();
extern "C" void SslClientStream__ctor_m4_627 ();
extern "C" void SslClientStream_add_ServerCertValidation_m4_628 ();
extern "C" void SslClientStream_remove_ServerCertValidation_m4_629 ();
extern "C" void SslClientStream_add_ClientCertSelection_m4_630 ();
extern "C" void SslClientStream_remove_ClientCertSelection_m4_631 ();
extern "C" void SslClientStream_add_PrivateKeySelection_m4_632 ();
extern "C" void SslClientStream_remove_PrivateKeySelection_m4_633 ();
extern "C" void SslClientStream_add_ServerCertValidation2_m4_634 ();
extern "C" void SslClientStream_remove_ServerCertValidation2_m4_635 ();
extern "C" void SslClientStream_get_InputBuffer_m4_636 ();
extern "C" void SslClientStream_get_ClientCertificates_m4_637 ();
extern "C" void SslClientStream_get_SelectedClientCertificate_m4_638 ();
extern "C" void SslClientStream_get_ServerCertValidationDelegate_m4_639 ();
extern "C" void SslClientStream_set_ServerCertValidationDelegate_m4_640 ();
extern "C" void SslClientStream_get_ClientCertSelectionDelegate_m4_641 ();
extern "C" void SslClientStream_set_ClientCertSelectionDelegate_m4_642 ();
extern "C" void SslClientStream_get_PrivateKeyCertSelectionDelegate_m4_643 ();
extern "C" void SslClientStream_set_PrivateKeyCertSelectionDelegate_m4_644 ();
extern "C" void SslClientStream_Finalize_m4_645 ();
extern "C" void SslClientStream_Dispose_m4_646 ();
extern "C" void SslClientStream_OnBeginNegotiateHandshake_m4_647 ();
extern "C" void SslClientStream_SafeReceiveRecord_m4_648 ();
extern "C" void SslClientStream_OnNegotiateHandshakeCallback_m4_649 ();
extern "C" void SslClientStream_OnLocalCertificateSelection_m4_650 ();
extern "C" void SslClientStream_get_HaveRemoteValidation2Callback_m4_651 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation2_m4_652 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation_m4_653 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation_m4_654 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation2_m4_655 ();
extern "C" void SslClientStream_RaiseClientCertificateSelection_m4_656 ();
extern "C" void SslClientStream_OnLocalPrivateKeySelection_m4_657 ();
extern "C" void SslClientStream_RaisePrivateKeySelection_m4_658 ();
extern "C" void SslCipherSuite__ctor_m4_659 ();
extern "C" void SslCipherSuite_ComputeServerRecordMAC_m4_660 ();
extern "C" void SslCipherSuite_ComputeClientRecordMAC_m4_661 ();
extern "C" void SslCipherSuite_ComputeMasterSecret_m4_662 ();
extern "C" void SslCipherSuite_ComputeKeys_m4_663 ();
extern "C" void SslCipherSuite_prf_m4_664 ();
extern "C" void SslHandshakeHash__ctor_m4_665 ();
extern "C" void SslHandshakeHash_Initialize_m4_666 ();
extern "C" void SslHandshakeHash_HashFinal_m4_667 ();
extern "C" void SslHandshakeHash_HashCore_m4_668 ();
extern "C" void SslHandshakeHash_CreateSignature_m4_669 ();
extern "C" void SslHandshakeHash_initializePad_m4_670 ();
extern "C" void InternalAsyncResult__ctor_m4_671 ();
extern "C" void InternalAsyncResult_get_ProceedAfterHandshake_m4_672 ();
extern "C" void InternalAsyncResult_get_FromWrite_m4_673 ();
extern "C" void InternalAsyncResult_get_Buffer_m4_674 ();
extern "C" void InternalAsyncResult_get_Offset_m4_675 ();
extern "C" void InternalAsyncResult_get_Count_m4_676 ();
extern "C" void InternalAsyncResult_get_BytesRead_m4_677 ();
extern "C" void InternalAsyncResult_get_AsyncState_m4_678 ();
extern "C" void InternalAsyncResult_get_AsyncException_m4_679 ();
extern "C" void InternalAsyncResult_get_CompletedWithError_m4_680 ();
extern "C" void InternalAsyncResult_get_AsyncWaitHandle_m4_681 ();
extern "C" void InternalAsyncResult_get_IsCompleted_m4_682 ();
extern "C" void InternalAsyncResult_SetComplete_m4_683 ();
extern "C" void InternalAsyncResult_SetComplete_m4_684 ();
extern "C" void InternalAsyncResult_SetComplete_m4_685 ();
extern "C" void InternalAsyncResult_SetComplete_m4_686 ();
extern "C" void SslStreamBase__ctor_m4_687 ();
extern "C" void SslStreamBase__cctor_m4_688 ();
extern "C" void SslStreamBase_AsyncHandshakeCallback_m4_689 ();
extern "C" void SslStreamBase_get_MightNeedHandshake_m4_690 ();
extern "C" void SslStreamBase_NegotiateHandshake_m4_691 ();
extern "C" void SslStreamBase_RaiseLocalCertificateSelection_m4_692 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation_m4_693 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation2_m4_694 ();
extern "C" void SslStreamBase_RaiseLocalPrivateKeySelection_m4_695 ();
extern "C" void SslStreamBase_get_CheckCertRevocationStatus_m4_696 ();
extern "C" void SslStreamBase_set_CheckCertRevocationStatus_m4_697 ();
extern "C" void SslStreamBase_get_CipherAlgorithm_m4_698 ();
extern "C" void SslStreamBase_get_CipherStrength_m4_699 ();
extern "C" void SslStreamBase_get_HashAlgorithm_m4_700 ();
extern "C" void SslStreamBase_get_HashStrength_m4_701 ();
extern "C" void SslStreamBase_get_KeyExchangeStrength_m4_702 ();
extern "C" void SslStreamBase_get_KeyExchangeAlgorithm_m4_703 ();
extern "C" void SslStreamBase_get_SecurityProtocol_m4_704 ();
extern "C" void SslStreamBase_get_ServerCertificate_m4_705 ();
extern "C" void SslStreamBase_get_ServerCertificates_m4_706 ();
extern "C" void SslStreamBase_BeginNegotiateHandshake_m4_707 ();
extern "C" void SslStreamBase_EndNegotiateHandshake_m4_708 ();
extern "C" void SslStreamBase_BeginRead_m4_709 ();
extern "C" void SslStreamBase_InternalBeginRead_m4_710 ();
extern "C" void SslStreamBase_InternalReadCallback_m4_711 ();
extern "C" void SslStreamBase_InternalBeginWrite_m4_712 ();
extern "C" void SslStreamBase_InternalWriteCallback_m4_713 ();
extern "C" void SslStreamBase_BeginWrite_m4_714 ();
extern "C" void SslStreamBase_EndRead_m4_715 ();
extern "C" void SslStreamBase_EndWrite_m4_716 ();
extern "C" void SslStreamBase_Close_m4_717 ();
extern "C" void SslStreamBase_Flush_m4_718 ();
extern "C" void SslStreamBase_Read_m4_719 ();
extern "C" void SslStreamBase_Read_m4_720 ();
extern "C" void SslStreamBase_Seek_m4_721 ();
extern "C" void SslStreamBase_SetLength_m4_722 ();
extern "C" void SslStreamBase_Write_m4_723 ();
extern "C" void SslStreamBase_Write_m4_724 ();
extern "C" void SslStreamBase_get_CanRead_m4_725 ();
extern "C" void SslStreamBase_get_CanSeek_m4_726 ();
extern "C" void SslStreamBase_get_CanWrite_m4_727 ();
extern "C" void SslStreamBase_get_Length_m4_728 ();
extern "C" void SslStreamBase_get_Position_m4_729 ();
extern "C" void SslStreamBase_set_Position_m4_730 ();
extern "C" void SslStreamBase_Finalize_m4_731 ();
extern "C" void SslStreamBase_Dispose_m4_732 ();
extern "C" void SslStreamBase_resetBuffer_m4_733 ();
extern "C" void SslStreamBase_checkDisposed_m4_734 ();
extern "C" void TlsCipherSuite__ctor_m4_735 ();
extern "C" void TlsCipherSuite_ComputeServerRecordMAC_m4_736 ();
extern "C" void TlsCipherSuite_ComputeClientRecordMAC_m4_737 ();
extern "C" void TlsCipherSuite_ComputeMasterSecret_m4_738 ();
extern "C" void TlsCipherSuite_ComputeKeys_m4_739 ();
extern "C" void TlsClientSettings__ctor_m4_740 ();
extern "C" void TlsClientSettings_get_TargetHost_m4_741 ();
extern "C" void TlsClientSettings_set_TargetHost_m4_742 ();
extern "C" void TlsClientSettings_get_Certificates_m4_743 ();
extern "C" void TlsClientSettings_set_Certificates_m4_744 ();
extern "C" void TlsClientSettings_get_ClientCertificate_m4_745 ();
extern "C" void TlsClientSettings_set_ClientCertificate_m4_746 ();
extern "C" void TlsClientSettings_UpdateCertificateRSA_m4_747 ();
extern "C" void TlsException__ctor_m4_748 ();
extern "C" void TlsException__ctor_m4_749 ();
extern "C" void TlsException__ctor_m4_750 ();
extern "C" void TlsException__ctor_m4_751 ();
extern "C" void TlsException__ctor_m4_752 ();
extern "C" void TlsException__ctor_m4_753 ();
extern "C" void TlsException_get_Alert_m4_754 ();
extern "C" void TlsServerSettings__ctor_m4_755 ();
extern "C" void TlsServerSettings_get_ServerKeyExchange_m4_756 ();
extern "C" void TlsServerSettings_set_ServerKeyExchange_m4_757 ();
extern "C" void TlsServerSettings_get_Certificates_m4_758 ();
extern "C" void TlsServerSettings_set_Certificates_m4_759 ();
extern "C" void TlsServerSettings_get_CertificateRSA_m4_760 ();
extern "C" void TlsServerSettings_get_RsaParameters_m4_761 ();
extern "C" void TlsServerSettings_set_RsaParameters_m4_762 ();
extern "C" void TlsServerSettings_set_SignedParams_m4_763 ();
extern "C" void TlsServerSettings_get_CertificateRequest_m4_764 ();
extern "C" void TlsServerSettings_set_CertificateRequest_m4_765 ();
extern "C" void TlsServerSettings_set_CertificateTypes_m4_766 ();
extern "C" void TlsServerSettings_set_DistinguisedNames_m4_767 ();
extern "C" void TlsServerSettings_UpdateCertificateRSA_m4_768 ();
extern "C" void TlsStream__ctor_m4_769 ();
extern "C" void TlsStream__ctor_m4_770 ();
extern "C" void TlsStream_get_EOF_m4_771 ();
extern "C" void TlsStream_get_CanWrite_m4_772 ();
extern "C" void TlsStream_get_CanRead_m4_773 ();
extern "C" void TlsStream_get_CanSeek_m4_774 ();
extern "C" void TlsStream_get_Position_m4_775 ();
extern "C" void TlsStream_set_Position_m4_776 ();
extern "C" void TlsStream_get_Length_m4_777 ();
extern "C" void TlsStream_ReadSmallValue_m4_778 ();
extern "C" void TlsStream_ReadByte_m4_779 ();
extern "C" void TlsStream_ReadInt16_m4_780 ();
extern "C" void TlsStream_ReadInt24_m4_781 ();
extern "C" void TlsStream_ReadBytes_m4_782 ();
extern "C" void TlsStream_Write_m4_783 ();
extern "C" void TlsStream_Write_m4_784 ();
extern "C" void TlsStream_WriteInt24_m4_785 ();
extern "C" void TlsStream_Write_m4_786 ();
extern "C" void TlsStream_Write_m4_787 ();
extern "C" void TlsStream_Reset_m4_788 ();
extern "C" void TlsStream_ToArray_m4_789 ();
extern "C" void TlsStream_Flush_m4_790 ();
extern "C" void TlsStream_SetLength_m4_791 ();
extern "C" void TlsStream_Seek_m4_792 ();
extern "C" void TlsStream_Read_m4_793 ();
extern "C" void TlsStream_Write_m4_794 ();
extern "C" void HandshakeMessage__ctor_m4_795 ();
extern "C" void HandshakeMessage__ctor_m4_796 ();
extern "C" void HandshakeMessage__ctor_m4_797 ();
extern "C" void HandshakeMessage_get_Context_m4_798 ();
extern "C" void HandshakeMessage_get_HandshakeType_m4_799 ();
extern "C" void HandshakeMessage_get_ContentType_m4_800 ();
extern "C" void HandshakeMessage_Process_m4_801 ();
extern "C" void HandshakeMessage_Update_m4_802 ();
extern "C" void HandshakeMessage_EncodeMessage_m4_803 ();
extern "C" void HandshakeMessage_Compare_m4_804 ();
extern "C" void TlsClientCertificate__ctor_m4_805 ();
extern "C" void TlsClientCertificate_get_ClientCertificate_m4_806 ();
extern "C" void TlsClientCertificate_Update_m4_807 ();
extern "C" void TlsClientCertificate_GetClientCertificate_m4_808 ();
extern "C" void TlsClientCertificate_SendCertificates_m4_809 ();
extern "C" void TlsClientCertificate_ProcessAsSsl3_m4_810 ();
extern "C" void TlsClientCertificate_ProcessAsTls1_m4_811 ();
extern "C" void TlsClientCertificate_FindParentCertificate_m4_812 ();
extern "C" void TlsClientCertificateVerify__ctor_m4_813 ();
extern "C" void TlsClientCertificateVerify_Update_m4_814 ();
extern "C" void TlsClientCertificateVerify_ProcessAsSsl3_m4_815 ();
extern "C" void TlsClientCertificateVerify_ProcessAsTls1_m4_816 ();
extern "C" void TlsClientCertificateVerify_getClientCertRSA_m4_817 ();
extern "C" void TlsClientCertificateVerify_getUnsignedBigInteger_m4_818 ();
extern "C" void TlsClientFinished__ctor_m4_819 ();
extern "C" void TlsClientFinished__cctor_m4_820 ();
extern "C" void TlsClientFinished_Update_m4_821 ();
extern "C" void TlsClientFinished_ProcessAsSsl3_m4_822 ();
extern "C" void TlsClientFinished_ProcessAsTls1_m4_823 ();
extern "C" void TlsClientHello__ctor_m4_824 ();
extern "C" void TlsClientHello_Update_m4_825 ();
extern "C" void TlsClientHello_ProcessAsSsl3_m4_826 ();
extern "C" void TlsClientHello_ProcessAsTls1_m4_827 ();
extern "C" void TlsClientKeyExchange__ctor_m4_828 ();
extern "C" void TlsClientKeyExchange_ProcessAsSsl3_m4_829 ();
extern "C" void TlsClientKeyExchange_ProcessAsTls1_m4_830 ();
extern "C" void TlsClientKeyExchange_ProcessCommon_m4_831 ();
extern "C" void TlsServerCertificate__ctor_m4_832 ();
extern "C" void TlsServerCertificate_Update_m4_833 ();
extern "C" void TlsServerCertificate_ProcessAsSsl3_m4_834 ();
extern "C" void TlsServerCertificate_ProcessAsTls1_m4_835 ();
extern "C" void TlsServerCertificate_checkCertificateUsage_m4_836 ();
extern "C" void TlsServerCertificate_validateCertificates_m4_837 ();
extern "C" void TlsServerCertificate_checkServerIdentity_m4_838 ();
extern "C" void TlsServerCertificate_checkDomainName_m4_839 ();
extern "C" void TlsServerCertificate_Match_m4_840 ();
extern "C" void TlsServerCertificateRequest__ctor_m4_841 ();
extern "C" void TlsServerCertificateRequest_Update_m4_842 ();
extern "C" void TlsServerCertificateRequest_ProcessAsSsl3_m4_843 ();
extern "C" void TlsServerCertificateRequest_ProcessAsTls1_m4_844 ();
extern "C" void TlsServerFinished__ctor_m4_845 ();
extern "C" void TlsServerFinished__cctor_m4_846 ();
extern "C" void TlsServerFinished_Update_m4_847 ();
extern "C" void TlsServerFinished_ProcessAsSsl3_m4_848 ();
extern "C" void TlsServerFinished_ProcessAsTls1_m4_849 ();
extern "C" void TlsServerHello__ctor_m4_850 ();
extern "C" void TlsServerHello_Update_m4_851 ();
extern "C" void TlsServerHello_ProcessAsSsl3_m4_852 ();
extern "C" void TlsServerHello_ProcessAsTls1_m4_853 ();
extern "C" void TlsServerHello_processProtocol_m4_854 ();
extern "C" void TlsServerHelloDone__ctor_m4_855 ();
extern "C" void TlsServerHelloDone_ProcessAsSsl3_m4_856 ();
extern "C" void TlsServerHelloDone_ProcessAsTls1_m4_857 ();
extern "C" void TlsServerKeyExchange__ctor_m4_858 ();
extern "C" void TlsServerKeyExchange_Update_m4_859 ();
extern "C" void TlsServerKeyExchange_ProcessAsSsl3_m4_860 ();
extern "C" void TlsServerKeyExchange_ProcessAsTls1_m4_861 ();
extern "C" void TlsServerKeyExchange_verifySignature_m4_862 ();
extern "C" void PrimalityTest__ctor_m4_863 ();
extern "C" void PrimalityTest_Invoke_m4_864 ();
extern "C" void PrimalityTest_BeginInvoke_m4_865 ();
extern "C" void PrimalityTest_EndInvoke_m4_866 ();
extern "C" void CertificateValidationCallback__ctor_m4_867 ();
extern "C" void CertificateValidationCallback_Invoke_m4_868 ();
extern "C" void CertificateValidationCallback_BeginInvoke_m4_869 ();
extern "C" void CertificateValidationCallback_EndInvoke_m4_870 ();
extern "C" void CertificateValidationCallback2__ctor_m4_871 ();
extern "C" void CertificateValidationCallback2_Invoke_m4_872 ();
extern "C" void CertificateValidationCallback2_BeginInvoke_m4_873 ();
extern "C" void CertificateValidationCallback2_EndInvoke_m4_874 ();
extern "C" void CertificateSelectionCallback__ctor_m4_875 ();
extern "C" void CertificateSelectionCallback_Invoke_m4_876 ();
extern "C" void CertificateSelectionCallback_BeginInvoke_m4_877 ();
extern "C" void CertificateSelectionCallback_EndInvoke_m4_878 ();
extern "C" void PrivateKeySelectionCallback__ctor_m4_879 ();
extern "C" void PrivateKeySelectionCallback_Invoke_m4_880 ();
extern "C" void PrivateKeySelectionCallback_BeginInvoke_m4_881 ();
extern "C" void PrivateKeySelectionCallback_EndInvoke_m4_882 ();
extern "C" void Hashtable__ctor_m5_0 ();
extern "C" void Hashtable__ctor_m5_1 ();
extern "C" void Hashtable_get_Item_m5_2 ();
extern "C" void Hashtable_set_Item_m5_3 ();
extern "C" void Hashtable_GetEnumerator_m5_4 ();
extern "C" void Hashtable_ToString_m5_5 ();
extern "C" void DictionaryEntryEnumerator__ctor_m5_6 ();
extern "C" void DictionaryEntryEnumerator_MoveNext_m5_7 ();
extern "C" void DictionaryEntryEnumerator_Reset_m5_8 ();
extern "C" void DictionaryEntryEnumerator_System_Collections_IEnumerator_get_Current_m5_9 ();
extern "C" void DictionaryEntryEnumerator_get_Current_m5_10 ();
extern "C" void DictionaryEntryEnumerator_Dispose_m5_11 ();
extern "C" void BigInteger__ctor_m5_12 ();
extern "C" void BigInteger__ctor_m5_13 ();
extern "C" void BigInteger__ctor_m5_14 ();
extern "C" void BigInteger__ctor_m5_15 ();
extern "C" void BigInteger__ctor_m5_16 ();
extern "C" void BigInteger_op_Implicit_m5_17 ();
extern "C" void BigInteger_op_Implicit_m5_18 ();
extern "C" void BigInteger_op_Addition_m5_19 ();
extern "C" void BigInteger_op_Subtraction_m5_20 ();
extern "C" void BigInteger_op_Multiply_m5_21 ();
extern "C" void BigInteger_op_LeftShift_m5_22 ();
extern "C" void BigInteger_shiftLeft_m5_23 ();
extern "C" void BigInteger_shiftRight_m5_24 ();
extern "C" void BigInteger_op_UnaryNegation_m5_25 ();
extern "C" void BigInteger_op_Equality_m5_26 ();
extern "C" void BigInteger_Equals_m5_27 ();
extern "C" void BigInteger_GetHashCode_m5_28 ();
extern "C" void BigInteger_op_GreaterThan_m5_29 ();
extern "C" void BigInteger_op_LessThan_m5_30 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m5_31 ();
extern "C" void BigInteger_multiByteDivide_m5_32 ();
extern "C" void BigInteger_singleByteDivide_m5_33 ();
extern "C" void BigInteger_op_Division_m5_34 ();
extern "C" void BigInteger_op_Modulus_m5_35 ();
extern "C" void BigInteger_ToString_m5_36 ();
extern "C" void BigInteger_ToString_m5_37 ();
extern "C" void BigInteger_ModPow_m5_38 ();
extern "C" void BigInteger_BarrettReduction_m5_39 ();
extern "C" void BigInteger_GenerateRandom_m5_40 ();
extern "C" void BigInteger_genRandomBits_m5_41 ();
extern "C" void BigInteger_bitCount_m5_42 ();
extern "C" void BigInteger_GetBytes_m5_43 ();
extern "C" void BigInteger__cctor_m5_44 ();
extern "C" void DiffieHellmanCryptoProvider__ctor_m5_45 ();
extern "C" void DiffieHellmanCryptoProvider_get_PublicKey_m5_46 ();
extern "C" void DiffieHellmanCryptoProvider_get_SharedKey_m5_47 ();
extern "C" void DiffieHellmanCryptoProvider_DeriveSharedKey_m5_48 ();
extern "C" void DiffieHellmanCryptoProvider_Encrypt_m5_49 ();
extern "C" void DiffieHellmanCryptoProvider_Encrypt_m5_50 ();
extern "C" void DiffieHellmanCryptoProvider_Decrypt_m5_51 ();
extern "C" void DiffieHellmanCryptoProvider_Dispose_m5_52 ();
extern "C" void DiffieHellmanCryptoProvider_Dispose_m5_53 ();
extern "C" void DiffieHellmanCryptoProvider_CalculatePublicKey_m5_54 ();
extern "C" void DiffieHellmanCryptoProvider_CalculateSharedKey_m5_55 ();
extern "C" void DiffieHellmanCryptoProvider_GenerateRandomSecret_m5_56 ();
extern "C" void DiffieHellmanCryptoProvider__cctor_m5_57 ();
extern "C" void EnetChannel__ctor_m5_58 ();
extern "C" void EnetChannel_ContainsUnreliableSequenceNumber_m5_59 ();
extern "C" void EnetChannel_ContainsReliableSequenceNumber_m5_60 ();
extern "C" void EnetChannel_FetchReliableSequenceNumber_m5_61 ();
extern "C" void EnetChannel_clearAll_m5_62 ();
extern "C" void MyAction__ctor_m5_63 ();
extern "C" void MyAction_Invoke_m5_64 ();
extern "C" void MyAction_BeginInvoke_m5_65 ();
extern "C" void MyAction_EndInvoke_m5_66 ();
extern "C" void U3CU3Ec__DisplayClass2__ctor_m5_67 ();
extern "C" void U3CU3Ec__DisplayClass2_U3CEnqueueDebugReturnU3Eb__0_m5_68 ();
extern "C" void U3CU3Ec__DisplayClass6__ctor_m5_69 ();
extern "C" void U3CU3Ec__DisplayClass6_U3CEnqueueStatusCallbackU3Eb__4_m5_70 ();
extern "C" void PeerBase_get_TrafficStatsEnabledTime_m5_71 ();
extern "C" void PeerBase_get_TrafficStatsEnabled_m5_72 ();
extern "C" void PeerBase_set_TrafficStatsEnabled_m5_73 ();
extern "C" void PeerBase_get_ServerAddress_m5_74 ();
extern "C" void PeerBase_set_ServerAddress_m5_75 ();
extern "C" void PeerBase_get_Listener_m5_76 ();
extern "C" void PeerBase_set_Listener_m5_77 ();
extern "C" void PeerBase_get_QuickResendAttempts_m5_78 ();
extern "C" void PeerBase_set_QuickResendAttempts_m5_79 ();
extern "C" void PeerBase_get_NetworkSimulationSettings_m5_80 ();
extern "C" void PeerBase_CommandLogResize_m5_81 ();
extern "C" void PeerBase_CommandLogInit_m5_82 ();
extern "C" void PeerBase_InitOnce_m5_83 ();
extern "C" void PeerBase_EnqueueOperation_m5_84 ();
extern "C" void PeerBase_SendAcksOnly_m5_85 ();
extern "C" void PeerBase_InitCallback_m5_86 ();
extern "C" void PeerBase_get_IsSendingOnlyAcks_m5_87 ();
extern "C" void PeerBase_set_IsSendingOnlyAcks_m5_88 ();
extern "C" void PeerBase_ExchangeKeysForEncryption_m5_89 ();
extern "C" void PeerBase_DeriveSharedKey_m5_90 ();
extern "C" void PeerBase_EnqueueActionForDispatch_m5_91 ();
extern "C" void PeerBase_EnqueueDebugReturn_m5_92 ();
extern "C" void PeerBase_EnqueueStatusCallback_m5_93 ();
extern "C" void PeerBase_InitPeerBase_m5_94 ();
extern "C" void PeerBase_DeserializeMessageAndCallback_m5_95 ();
extern "C" void PeerBase_SendNetworkSimulated_m5_96 ();
extern "C" void PeerBase_ReceiveNetworkSimulated_m5_97 ();
extern "C" void PeerBase_NetworkSimRun_m5_98 ();
extern "C" void PeerBase_UpdateRoundTripTimeAndVariance_m5_99 ();
extern "C" void PeerBase_InitializeTrafficStats_m5_100 ();
extern "C" void PeerBase__ctor_m5_101 ();
extern "C" void PeerBase__cctor_m5_102 ();
extern "C" void U3CU3Ec__DisplayClassb__ctor_m5_103 ();
extern "C" void U3CU3Ec__DisplayClassd__ctor_m5_104 ();
extern "C" void U3CU3Ec__DisplayClassd_U3CSendDataU3Eb__a_m5_105 ();
extern "C" void U3CU3Ec__DisplayClass11__ctor_m5_106 ();
extern "C" void U3CU3Ec__DisplayClass11_U3CReceiveIncomingCommandsU3Eb__f_m5_107 ();
extern "C" void EnetPeer__ctor_m5_108 ();
extern "C" void EnetPeer_InitPeerBase_m5_109 ();
extern "C" void EnetPeer_Connect_m5_110 ();
extern "C" void EnetPeer_Disconnect_m5_111 ();
extern "C" void EnetPeer_StopConnection_m5_112 ();
extern "C" void EnetPeer_FetchServerTimestamp_m5_113 ();
extern "C" void EnetPeer_DispatchIncomingCommands_m5_114 ();
extern "C" void EnetPeer_SendAcksOnly_m5_115 ();
extern "C" void EnetPeer_SendOutgoingCommands_m5_116 ();
extern "C" void EnetPeer_AreReliableCommandsInTransit_m5_117 ();
extern "C" void EnetPeer_EnqueueOperation_m5_118 ();
extern "C" void EnetPeer_CreateAndEnqueueCommand_m5_119 ();
extern "C" void EnetPeer_SerializeOperationToMessage_m5_120 ();
extern "C" void EnetPeer_SerializeToBuffer_m5_121 ();
extern "C" void EnetPeer_SendData_m5_122 ();
extern "C" void EnetPeer_QueueSentCommand_m5_123 ();
extern "C" void EnetPeer_QueueOutgoingReliableCommand_m5_124 ();
extern "C" void EnetPeer_QueueOutgoingUnreliableCommand_m5_125 ();
extern "C" void EnetPeer_QueueOutgoingAcknowledgement_m5_126 ();
extern "C" void EnetPeer_ReceiveIncomingCommands_m5_127 ();
extern "C" void EnetPeer_ExecuteCommand_m5_128 ();
extern "C" void EnetPeer_QueueIncomingCommand_m5_129 ();
extern "C" void EnetPeer_RemoveSentReliableCommand_m5_130 ();
extern "C" void EnetPeer_U3CExecuteCommandU3Eb__13_m5_131 ();
extern "C" void EnetPeer__cctor_m5_132 ();
extern "C" void U3CU3Ec__DisplayClass3__ctor_m5_133 ();
extern "C" void U3CU3Ec__DisplayClass3_U3CHandleReceivedDatagramU3Eb__1_m5_134 ();
extern "C" void U3CU3Ec__DisplayClass5__ctor_m5_135 ();
extern "C" void U3CU3Ec__DisplayClass5_U3CHandleReceivedDatagramU3Eb__0_m5_136 ();
extern "C" void IPhotonSocket_get_Listener_m5_137 ();
extern "C" void IPhotonSocket_get_Protocol_m5_138 ();
extern "C" void IPhotonSocket_set_Protocol_m5_139 ();
extern "C" void IPhotonSocket_get_State_m5_140 ();
extern "C" void IPhotonSocket_set_State_m5_141 ();
extern "C" void IPhotonSocket_get_ServerAddress_m5_142 ();
extern "C" void IPhotonSocket_set_ServerAddress_m5_143 ();
extern "C" void IPhotonSocket_get_ServerPort_m5_144 ();
extern "C" void IPhotonSocket_set_ServerPort_m5_145 ();
extern "C" void IPhotonSocket_get_Connected_m5_146 ();
extern "C" void IPhotonSocket_get_MTU_m5_147 ();
extern "C" void IPhotonSocket__ctor_m5_148 ();
extern "C" void IPhotonSocket_Connect_m5_149 ();
extern "C" void IPhotonSocket_HandleReceivedDatagram_m5_150 ();
extern "C" void IPhotonSocket_ReportDebugOfLevel_m5_151 ();
extern "C" void IPhotonSocket_EnqueueDebugReturn_m5_152 ();
extern "C" void IPhotonSocket_HandleException_m5_153 ();
extern "C" void IPhotonSocket_TryParseAddress_m5_154 ();
extern "C" void IPhotonSocket_GetIpAddress_m5_155 ();
extern "C" void IPhotonSocket_U3CHandleExceptionU3Eb__7_m5_156 ();
extern "C" void NCommand__ctor_m5_157 ();
extern "C" void NCommand_CreateAck_m5_158 ();
extern "C" void NCommand__ctor_m5_159 ();
extern "C" void NCommand_Serialize_m5_160 ();
extern "C" void NCommand_CompareTo_m5_161 ();
extern "C" void NCommand_ToString_m5_162 ();
extern "C" void SimulationItem__ctor_m5_163 ();
extern "C" void SimulationItem_get_Delay_m5_164 ();
extern "C" void SimulationItem_set_Delay_m5_165 ();
extern "C" void NetworkSimulationSet_get_IsSimulationEnabled_m5_166 ();
extern "C" void NetworkSimulationSet_set_IsSimulationEnabled_m5_167 ();
extern "C" void NetworkSimulationSet_get_OutgoingLag_m5_168 ();
extern "C" void NetworkSimulationSet_set_OutgoingLag_m5_169 ();
extern "C" void NetworkSimulationSet_get_OutgoingJitter_m5_170 ();
extern "C" void NetworkSimulationSet_set_OutgoingJitter_m5_171 ();
extern "C" void NetworkSimulationSet_get_OutgoingLossPercentage_m5_172 ();
extern "C" void NetworkSimulationSet_set_OutgoingLossPercentage_m5_173 ();
extern "C" void NetworkSimulationSet_get_IncomingLag_m5_174 ();
extern "C" void NetworkSimulationSet_set_IncomingLag_m5_175 ();
extern "C" void NetworkSimulationSet_get_IncomingJitter_m5_176 ();
extern "C" void NetworkSimulationSet_set_IncomingJitter_m5_177 ();
extern "C" void NetworkSimulationSet_get_IncomingLossPercentage_m5_178 ();
extern "C" void NetworkSimulationSet_set_IncomingLossPercentage_m5_179 ();
extern "C" void NetworkSimulationSet_get_LostPackagesOut_m5_180 ();
extern "C" void NetworkSimulationSet_set_LostPackagesOut_m5_181 ();
extern "C" void NetworkSimulationSet_get_LostPackagesIn_m5_182 ();
extern "C" void NetworkSimulationSet_set_LostPackagesIn_m5_183 ();
extern "C" void NetworkSimulationSet_ToString_m5_184 ();
extern "C" void NetworkSimulationSet__ctor_m5_185 ();
extern "C" void OakleyGroups__cctor_m5_186 ();
extern "C" void PhotonCodes__cctor_m5_187 ();
extern "C" void CmdLogItem__ctor_m5_188 ();
extern "C" void CmdLogItem__ctor_m5_189 ();
extern "C" void CmdLogItem_ToString_m5_190 ();
extern "C" void CmdLogReceivedReliable__ctor_m5_191 ();
extern "C" void CmdLogReceivedReliable_ToString_m5_192 ();
extern "C" void CmdLogReceivedAck__ctor_m5_193 ();
extern "C" void CmdLogReceivedAck_ToString_m5_194 ();
extern "C" void CmdLogSentReliable__ctor_m5_195 ();
extern "C" void CmdLogSentReliable_ToString_m5_196 ();
extern "C" void PhotonPeer_set_SocketImplementation_m5_197 ();
extern "C" void PhotonPeer_set_DebugOut_m5_198 ();
extern "C" void PhotonPeer_get_DebugOut_m5_199 ();
extern "C" void PhotonPeer_get_Listener_m5_200 ();
extern "C" void PhotonPeer_set_Listener_m5_201 ();
extern "C" void PhotonPeer_get_TrafficStatsEnabled_m5_202 ();
extern "C" void PhotonPeer_set_TrafficStatsEnabled_m5_203 ();
extern "C" void PhotonPeer_get_TrafficStatsElapsedMs_m5_204 ();
extern "C" void PhotonPeer_TrafficStatsReset_m5_205 ();
extern "C" void PhotonPeer_get_TrafficStatsIncoming_m5_206 ();
extern "C" void PhotonPeer_get_TrafficStatsOutgoing_m5_207 ();
extern "C" void PhotonPeer_get_TrafficStatsGameLevel_m5_208 ();
extern "C" void PhotonPeer_get_QuickResendAttempts_m5_209 ();
extern "C" void PhotonPeer_set_QuickResendAttempts_m5_210 ();
extern "C" void PhotonPeer_get_PeerState_m5_211 ();
extern "C" void PhotonPeer_get_LimitOfUnreliableCommands_m5_212 ();
extern "C" void PhotonPeer_set_LimitOfUnreliableCommands_m5_213 ();
extern "C" void PhotonPeer_get_CrcEnabled_m5_214 ();
extern "C" void PhotonPeer_set_CrcEnabled_m5_215 ();
extern "C" void PhotonPeer_get_PacketLossByCrc_m5_216 ();
extern "C" void PhotonPeer_get_ResentReliableCommands_m5_217 ();
extern "C" void PhotonPeer_get_SentCountAllowance_m5_218 ();
extern "C" void PhotonPeer_set_SentCountAllowance_m5_219 ();
extern "C" void PhotonPeer_set_TimePingInterval_m5_220 ();
extern "C" void PhotonPeer_get_DisconnectTimeout_m5_221 ();
extern "C" void PhotonPeer_set_DisconnectTimeout_m5_222 ();
extern "C" void PhotonPeer_get_ServerTimeInMilliSeconds_m5_223 ();
extern "C" void PhotonPeer_get_RoundTripTime_m5_224 ();
extern "C" void PhotonPeer_get_RoundTripTimeVariance_m5_225 ();
extern "C" void PhotonPeer_get_TimestampOfLastSocketReceive_m5_226 ();
extern "C" void PhotonPeer_get_ServerAddress_m5_227 ();
extern "C" void PhotonPeer_get_UsedProtocol_m5_228 ();
extern "C" void PhotonPeer_get_IsSimulationEnabled_m5_229 ();
extern "C" void PhotonPeer_set_IsSimulationEnabled_m5_230 ();
extern "C" void PhotonPeer_get_NetworkSimulationSettings_m5_231 ();
extern "C" void PhotonPeer_get_IsEncryptionAvailable_m5_232 ();
extern "C" void PhotonPeer_set_IsSendingOnlyAcks_m5_233 ();
extern "C" void PhotonPeer__ctor_m5_234 ();
extern "C" void PhotonPeer__ctor_m5_235 ();
extern "C" void PhotonPeer_Connect_m5_236 ();
extern "C" void PhotonPeer_Disconnect_m5_237 ();
extern "C" void PhotonPeer_StopThread_m5_238 ();
extern "C" void PhotonPeer_FetchServerTimestamp_m5_239 ();
extern "C" void PhotonPeer_EstablishEncryption_m5_240 ();
extern "C" void PhotonPeer_Service_m5_241 ();
extern "C" void PhotonPeer_SendOutgoingCommands_m5_242 ();
extern "C" void PhotonPeer_SendAcksOnly_m5_243 ();
extern "C" void PhotonPeer_DispatchIncomingCommands_m5_244 ();
extern "C" void PhotonPeer_VitalStatsToString_m5_245 ();
extern "C" void PhotonPeer_OpCustom_m5_246 ();
extern "C" void PhotonPeer_OpCustom_m5_247 ();
extern "C" void PhotonPeer_OpCustom_m5_248 ();
extern "C" void PhotonPeer_RegisterType_m5_249 ();
extern "C" void PhotonPing_StartPing_m5_250 ();
extern "C" void PhotonPing_Done_m5_251 ();
extern "C" void PhotonPing_Dispose_m5_252 ();
extern "C" void PhotonPing_Init_m5_253 ();
extern "C" void PhotonPing__ctor_m5_254 ();
extern "C" void PingMono_StartPing_m5_255 ();
extern "C" void PingMono_Done_m5_256 ();
extern "C" void PingMono_Dispose_m5_257 ();
extern "C" void PingMono__ctor_m5_258 ();
extern "C" void PingNativeDynamic__ctor_m5_259 ();
extern "C" void OperationRequest__ctor_m5_260 ();
extern "C" void OperationResponse_get_Item_m5_261 ();
extern "C" void OperationResponse_ToString_m5_262 ();
extern "C" void OperationResponse_ToStringFull_m5_263 ();
extern "C" void OperationResponse__ctor_m5_264 ();
extern "C" void EventData_get_Item_m5_265 ();
extern "C" void EventData_ToString_m5_266 ();
extern "C" void EventData__ctor_m5_267 ();
extern "C" void SerializeMethod__ctor_m5_268 ();
extern "C" void SerializeMethod_Invoke_m5_269 ();
extern "C" void SerializeMethod_BeginInvoke_m5_270 ();
extern "C" void SerializeMethod_EndInvoke_m5_271 ();
extern "C" void SerializeStreamMethod__ctor_m5_272 ();
extern "C" void SerializeStreamMethod_Invoke_m5_273 ();
extern "C" void SerializeStreamMethod_BeginInvoke_m5_274 ();
extern "C" void SerializeStreamMethod_EndInvoke_m5_275 ();
extern "C" void DeserializeMethod__ctor_m5_276 ();
extern "C" void DeserializeMethod_Invoke_m5_277 ();
extern "C" void DeserializeMethod_BeginInvoke_m5_278 ();
extern "C" void DeserializeMethod_EndInvoke_m5_279 ();
extern "C" void DeserializeStreamMethod__ctor_m5_280 ();
extern "C" void DeserializeStreamMethod_Invoke_m5_281 ();
extern "C" void DeserializeStreamMethod_BeginInvoke_m5_282 ();
extern "C" void DeserializeStreamMethod_EndInvoke_m5_283 ();
extern "C" void CustomType__ctor_m5_284 ();
extern "C" void Protocol_TryRegisterType_m5_285 ();
extern "C" void Protocol_SerializeCustom_m5_286 ();
extern "C" void Protocol_DeserializeCustom_m5_287 ();
extern "C" void Protocol_GetTypeOfCode_m5_288 ();
extern "C" void Protocol_GetCodeOfType_m5_289 ();
extern "C" void Protocol_CreateArrayByType_m5_290 ();
extern "C" void Protocol_SerializeOperationRequest_m5_291 ();
extern "C" void Protocol_SerializeOperationRequest_m5_292 ();
extern "C" void Protocol_DeserializeOperationRequest_m5_293 ();
extern "C" void Protocol_SerializeOperationResponse_m5_294 ();
extern "C" void Protocol_DeserializeOperationResponse_m5_295 ();
extern "C" void Protocol_SerializeEventData_m5_296 ();
extern "C" void Protocol_DeserializeEventData_m5_297 ();
extern "C" void Protocol_SerializeParameterTable_m5_298 ();
extern "C" void Protocol_DeserializeParameterTable_m5_299 ();
extern "C" void Protocol_Serialize_m5_300 ();
extern "C" void Protocol_SerializeByte_m5_301 ();
extern "C" void Protocol_SerializeBoolean_m5_302 ();
extern "C" void Protocol_SerializeShort_m5_303 ();
extern "C" void Protocol_Serialize_m5_304 ();
extern "C" void Protocol_SerializeInteger_m5_305 ();
extern "C" void Protocol_Serialize_m5_306 ();
extern "C" void Protocol_SerializeLong_m5_307 ();
extern "C" void Protocol_SerializeFloat_m5_308 ();
extern "C" void Protocol_Serialize_m5_309 ();
extern "C" void Protocol_SerializeDouble_m5_310 ();
extern "C" void Protocol_SerializeString_m5_311 ();
extern "C" void Protocol_SerializeArray_m5_312 ();
extern "C" void Protocol_SerializeByteArray_m5_313 ();
extern "C" void Protocol_SerializeIntArrayOptimized_m5_314 ();
extern "C" void Protocol_SerializeObjectArray_m5_315 ();
extern "C" void Protocol_SerializeHashTable_m5_316 ();
extern "C" void Protocol_SerializeDictionary_m5_317 ();
extern "C" void Protocol_SerializeDictionaryHeader_m5_318 ();
extern "C" void Protocol_SerializeDictionaryHeader_m5_319 ();
extern "C" void Protocol_SerializeDictionaryElements_m5_320 ();
extern "C" void Protocol_Deserialize_m5_321 ();
extern "C" void Protocol_DeserializeByte_m5_322 ();
extern "C" void Protocol_DeserializeBoolean_m5_323 ();
extern "C" void Protocol_DeserializeShort_m5_324 ();
extern "C" void Protocol_Deserialize_m5_325 ();
extern "C" void Protocol_DeserializeInteger_m5_326 ();
extern "C" void Protocol_Deserialize_m5_327 ();
extern "C" void Protocol_DeserializeLong_m5_328 ();
extern "C" void Protocol_DeserializeFloat_m5_329 ();
extern "C" void Protocol_Deserialize_m5_330 ();
extern "C" void Protocol_DeserializeDouble_m5_331 ();
extern "C" void Protocol_DeserializeString_m5_332 ();
extern "C" void Protocol_DeserializeArray_m5_333 ();
extern "C" void Protocol_DeserializeByteArray_m5_334 ();
extern "C" void Protocol_DeserializeIntArray_m5_335 ();
extern "C" void Protocol_DeserializeStringArray_m5_336 ();
extern "C" void Protocol_DeserializeObjectArray_m5_337 ();
extern "C" void Protocol_DeserializeHashTable_m5_338 ();
extern "C" void Protocol_DeserializeDictionary_m5_339 ();
extern "C" void Protocol_DeserializeDictionaryArray_m5_340 ();
extern "C" void Protocol_DeserializeDictionaryType_m5_341 ();
extern "C" void Protocol__cctor_m5_342 ();
extern "C" void SocketTcp__ctor_m5_343 ();
extern "C" void SocketTcp_Dispose_m5_344 ();
extern "C" void SocketTcp_Connect_m5_345 ();
extern "C" void SocketTcp_Disconnect_m5_346 ();
extern "C" void SocketTcp_Send_m5_347 ();
extern "C" void SocketTcp_DnsAndConnect_m5_348 ();
extern "C" void SocketTcp_ReceiveLoop_m5_349 ();
extern "C" void SocketUdp__ctor_m5_350 ();
extern "C" void SocketUdp_Dispose_m5_351 ();
extern "C" void SocketUdp_Connect_m5_352 ();
extern "C" void SocketUdp_Disconnect_m5_353 ();
extern "C" void SocketUdp_Send_m5_354 ();
extern "C" void SocketUdp_DnsAndConnect_m5_355 ();
extern "C" void SocketUdp_ReceiveLoop_m5_356 ();
extern "C" void IntegerMillisecondsDelegate__ctor_m5_357 ();
extern "C" void IntegerMillisecondsDelegate_Invoke_m5_358 ();
extern "C" void IntegerMillisecondsDelegate_BeginInvoke_m5_359 ();
extern "C" void IntegerMillisecondsDelegate_EndInvoke_m5_360 ();
extern "C" void ThreadSafeRandom_Next_m5_361 ();
extern "C" void ThreadSafeRandom__cctor_m5_362 ();
extern "C" void U3CU3Ec__DisplayClass1__ctor_m5_363 ();
extern "C" void U3CU3Ec__DisplayClass1_U3CCallInBackgroundU3Eb__0_m5_364 ();
extern "C" void SupportClass_CalculateCrc_m5_365 ();
extern "C" void SupportClass_GetMethods_m5_366 ();
extern "C" void SupportClass_GetTickCount_m5_367 ();
extern "C" void SupportClass_CallInBackground_m5_368 ();
extern "C" void SupportClass_CallInBackground_m5_369 ();
extern "C" void SupportClass_WriteStackTrace_m5_370 ();
extern "C" void SupportClass_WriteStackTrace_m5_371 ();
extern "C" void SupportClass_DictionaryToString_m5_372 ();
extern "C" void SupportClass_DictionaryToString_m5_373 ();
extern "C" void SupportClass__cctor_m5_374 ();
extern "C" void SupportClass_U3C_cctorU3Eb__3_m5_375 ();
extern "C" void U3CU3Ec__DisplayClass3__ctor_m5_376 ();
extern "C" void U3CU3Ec__DisplayClass3_U3CSendDataU3Eb__1_m5_377 ();
extern "C" void TPeer__ctor_m5_378 ();
extern "C" void TPeer_InitPeerBase_m5_379 ();
extern "C" void TPeer_Connect_m5_380 ();
extern "C" void TPeer_Disconnect_m5_381 ();
extern "C" void TPeer_StopConnection_m5_382 ();
extern "C" void TPeer_FetchServerTimestamp_m5_383 ();
extern "C" void TPeer_EnqueueInit_m5_384 ();
extern "C" void TPeer_DispatchIncomingCommands_m5_385 ();
extern "C" void TPeer_SendOutgoingCommands_m5_386 ();
extern "C" void TPeer_SendAcksOnly_m5_387 ();
extern "C" void TPeer_EnqueueOperation_m5_388 ();
extern "C" void TPeer_SerializeOperationToMessage_m5_389 ();
extern "C" void TPeer_EnqueueMessageAsPayload_m5_390 ();
extern "C" void TPeer_SendPing_m5_391 ();
extern "C" void TPeer_SendData_m5_392 ();
extern "C" void TPeer_ReceiveIncomingCommands_m5_393 ();
extern "C" void TPeer_ReadPingResult_m5_394 ();
extern "C" void TPeer_ReadPingResult_m5_395 ();
extern "C" void TPeer__cctor_m5_396 ();
extern "C" void TrafficStatsGameLevel_get_OperationByteCount_m5_397 ();
extern "C" void TrafficStatsGameLevel_set_OperationByteCount_m5_398 ();
extern "C" void TrafficStatsGameLevel_get_OperationCount_m5_399 ();
extern "C" void TrafficStatsGameLevel_set_OperationCount_m5_400 ();
extern "C" void TrafficStatsGameLevel_get_ResultByteCount_m5_401 ();
extern "C" void TrafficStatsGameLevel_set_ResultByteCount_m5_402 ();
extern "C" void TrafficStatsGameLevel_get_ResultCount_m5_403 ();
extern "C" void TrafficStatsGameLevel_set_ResultCount_m5_404 ();
extern "C" void TrafficStatsGameLevel_get_EventByteCount_m5_405 ();
extern "C" void TrafficStatsGameLevel_set_EventByteCount_m5_406 ();
extern "C" void TrafficStatsGameLevel_get_EventCount_m5_407 ();
extern "C" void TrafficStatsGameLevel_set_EventCount_m5_408 ();
extern "C" void TrafficStatsGameLevel_get_LongestOpResponseCallback_m5_409 ();
extern "C" void TrafficStatsGameLevel_set_LongestOpResponseCallback_m5_410 ();
extern "C" void TrafficStatsGameLevel_get_LongestOpResponseCallbackOpCode_m5_411 ();
extern "C" void TrafficStatsGameLevel_set_LongestOpResponseCallbackOpCode_m5_412 ();
extern "C" void TrafficStatsGameLevel_get_LongestEventCallback_m5_413 ();
extern "C" void TrafficStatsGameLevel_set_LongestEventCallback_m5_414 ();
extern "C" void TrafficStatsGameLevel_get_LongestEventCallbackCode_m5_415 ();
extern "C" void TrafficStatsGameLevel_set_LongestEventCallbackCode_m5_416 ();
extern "C" void TrafficStatsGameLevel_get_LongestDeltaBetweenDispatching_m5_417 ();
extern "C" void TrafficStatsGameLevel_set_LongestDeltaBetweenDispatching_m5_418 ();
extern "C" void TrafficStatsGameLevel_get_LongestDeltaBetweenSending_m5_419 ();
extern "C" void TrafficStatsGameLevel_set_LongestDeltaBetweenSending_m5_420 ();
extern "C" void TrafficStatsGameLevel_get_DispatchIncomingCommandsCalls_m5_421 ();
extern "C" void TrafficStatsGameLevel_set_DispatchIncomingCommandsCalls_m5_422 ();
extern "C" void TrafficStatsGameLevel_get_SendOutgoingCommandsCalls_m5_423 ();
extern "C" void TrafficStatsGameLevel_set_SendOutgoingCommandsCalls_m5_424 ();
extern "C" void TrafficStatsGameLevel_get_TotalMessageCount_m5_425 ();
extern "C" void TrafficStatsGameLevel_get_TotalIncomingMessageCount_m5_426 ();
extern "C" void TrafficStatsGameLevel_get_TotalOutgoingMessageCount_m5_427 ();
extern "C" void TrafficStatsGameLevel_CountOperation_m5_428 ();
extern "C" void TrafficStatsGameLevel_CountResult_m5_429 ();
extern "C" void TrafficStatsGameLevel_CountEvent_m5_430 ();
extern "C" void TrafficStatsGameLevel_TimeForResponseCallback_m5_431 ();
extern "C" void TrafficStatsGameLevel_TimeForEventCallback_m5_432 ();
extern "C" void TrafficStatsGameLevel_DispatchIncomingCommandsCalled_m5_433 ();
extern "C" void TrafficStatsGameLevel_SendOutgoingCommandsCalled_m5_434 ();
extern "C" void TrafficStatsGameLevel_ToString_m5_435 ();
extern "C" void TrafficStatsGameLevel_ToStringVitalStats_m5_436 ();
extern "C" void TrafficStatsGameLevel__ctor_m5_437 ();
extern "C" void TrafficStats_get_PackageHeaderSize_m5_438 ();
extern "C" void TrafficStats_set_PackageHeaderSize_m5_439 ();
extern "C" void TrafficStats_get_ReliableCommandCount_m5_440 ();
extern "C" void TrafficStats_set_ReliableCommandCount_m5_441 ();
extern "C" void TrafficStats_get_UnreliableCommandCount_m5_442 ();
extern "C" void TrafficStats_set_UnreliableCommandCount_m5_443 ();
extern "C" void TrafficStats_get_FragmentCommandCount_m5_444 ();
extern "C" void TrafficStats_set_FragmentCommandCount_m5_445 ();
extern "C" void TrafficStats_get_ControlCommandCount_m5_446 ();
extern "C" void TrafficStats_set_ControlCommandCount_m5_447 ();
extern "C" void TrafficStats_get_TotalPacketCount_m5_448 ();
extern "C" void TrafficStats_set_TotalPacketCount_m5_449 ();
extern "C" void TrafficStats_get_TotalCommandsInPackets_m5_450 ();
extern "C" void TrafficStats_set_TotalCommandsInPackets_m5_451 ();
extern "C" void TrafficStats_get_ReliableCommandBytes_m5_452 ();
extern "C" void TrafficStats_set_ReliableCommandBytes_m5_453 ();
extern "C" void TrafficStats_get_UnreliableCommandBytes_m5_454 ();
extern "C" void TrafficStats_set_UnreliableCommandBytes_m5_455 ();
extern "C" void TrafficStats_get_FragmentCommandBytes_m5_456 ();
extern "C" void TrafficStats_set_FragmentCommandBytes_m5_457 ();
extern "C" void TrafficStats_get_ControlCommandBytes_m5_458 ();
extern "C" void TrafficStats_set_ControlCommandBytes_m5_459 ();
extern "C" void TrafficStats__ctor_m5_460 ();
extern "C" void TrafficStats_get_TotalCommandBytes_m5_461 ();
extern "C" void TrafficStats_get_TotalPacketBytes_m5_462 ();
extern "C" void TrafficStats_set_TimestampOfLastAck_m5_463 ();
extern "C" void TrafficStats_set_TimestampOfLastReliableCommand_m5_464 ();
extern "C" void TrafficStats_CountControlCommand_m5_465 ();
extern "C" void TrafficStats_CountReliableOpCommand_m5_466 ();
extern "C" void TrafficStats_CountUnreliableOpCommand_m5_467 ();
extern "C" void TrafficStats_CountFragmentOpCommand_m5_468 ();
extern "C" void TrafficStats_ToString_m5_469 ();
extern "C" void AssetBundleCreateRequest__ctor_m6_0 ();
extern "C" void AssetBundleCreateRequest_get_assetBundle_m6_1 ();
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m6_2 ();
extern "C" void AssetBundleRequest__ctor_m6_3 ();
extern "C" void AssetBundleRequest_get_asset_m6_4 ();
extern "C" void AssetBundleRequest_get_allAssets_m6_5 ();
extern "C" void AssetBundle_LoadAsset_m6_6 ();
extern "C" void AssetBundle_LoadAsset_Internal_m6_7 ();
extern "C" void AssetBundle_LoadAssetWithSubAssets_Internal_m6_8 ();
extern "C" void SystemInfo_get_operatingSystem_m6_9 ();
extern "C" void WaitForSeconds__ctor_m6_10 ();
extern "C" void WaitForFixedUpdate__ctor_m6_11 ();
extern "C" void WaitForEndOfFrame__ctor_m6_12 ();
extern "C" void Coroutine__ctor_m6_13 ();
extern "C" void Coroutine_ReleaseCoroutine_m6_14 ();
extern "C" void Coroutine_Finalize_m6_15 ();
extern "C" void ScriptableObject__ctor_m6_16 ();
extern "C" void ScriptableObject_Internal_CreateScriptableObject_m6_17 ();
extern "C" void ScriptableObject_CreateInstance_m6_18 ();
extern "C" void ScriptableObject_CreateInstance_m6_19 ();
extern "C" void ScriptableObject_CreateInstanceFromType_m6_20 ();
extern "C" void UnhandledExceptionHandler__ctor_m6_21 ();
extern "C" void UnhandledExceptionHandler_RegisterUECatcher_m6_22 ();
extern "C" void UnhandledExceptionHandler_HandleUnhandledException_m6_23 ();
extern "C" void UnhandledExceptionHandler_PrintException_m6_24 ();
extern "C" void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m6_25 ();
extern "C" void GameCenterPlatform__ctor_m6_26 ();
extern "C" void GameCenterPlatform__cctor_m6_27 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m6_28 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m6_29 ();
extern "C" void GameCenterPlatform_Internal_Authenticate_m6_30 ();
extern "C" void GameCenterPlatform_Internal_Authenticated_m6_31 ();
extern "C" void GameCenterPlatform_Internal_UserName_m6_32 ();
extern "C" void GameCenterPlatform_Internal_UserID_m6_33 ();
extern "C" void GameCenterPlatform_Internal_Underage_m6_34 ();
extern "C" void GameCenterPlatform_Internal_UserImage_m6_35 ();
extern "C" void GameCenterPlatform_Internal_LoadFriends_m6_36 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievementDescriptions_m6_37 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievements_m6_38 ();
extern "C" void GameCenterPlatform_Internal_ReportProgress_m6_39 ();
extern "C" void GameCenterPlatform_Internal_ReportScore_m6_40 ();
extern "C" void GameCenterPlatform_Internal_LoadScores_m6_41 ();
extern "C" void GameCenterPlatform_Internal_ShowAchievementsUI_m6_42 ();
extern "C" void GameCenterPlatform_Internal_ShowLeaderboardUI_m6_43 ();
extern "C" void GameCenterPlatform_Internal_LoadUsers_m6_44 ();
extern "C" void GameCenterPlatform_Internal_ResetAllAchievements_m6_45 ();
extern "C" void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6_46 ();
extern "C" void GameCenterPlatform_ResetAllAchievements_m6_47 ();
extern "C" void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m6_48 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m6_49 ();
extern "C" void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6_50 ();
extern "C" void GameCenterPlatform_ClearAchievementDescriptions_m6_51 ();
extern "C" void GameCenterPlatform_SetAchievementDescription_m6_52 ();
extern "C" void GameCenterPlatform_SetAchievementDescriptionImage_m6_53 ();
extern "C" void GameCenterPlatform_TriggerAchievementDescriptionCallback_m6_54 ();
extern "C" void GameCenterPlatform_AuthenticateCallbackWrapper_m6_55 ();
extern "C" void GameCenterPlatform_ClearFriends_m6_56 ();
extern "C" void GameCenterPlatform_SetFriends_m6_57 ();
extern "C" void GameCenterPlatform_SetFriendImage_m6_58 ();
extern "C" void GameCenterPlatform_TriggerFriendsCallbackWrapper_m6_59 ();
extern "C" void GameCenterPlatform_AchievementCallbackWrapper_m6_60 ();
extern "C" void GameCenterPlatform_ProgressCallbackWrapper_m6_61 ();
extern "C" void GameCenterPlatform_ScoreCallbackWrapper_m6_62 ();
extern "C" void GameCenterPlatform_ScoreLoaderCallbackWrapper_m6_63 ();
extern "C" void GameCenterPlatform_get_localUser_m6_64 ();
extern "C" void GameCenterPlatform_PopulateLocalUser_m6_65 ();
extern "C" void GameCenterPlatform_LoadAchievementDescriptions_m6_66 ();
extern "C" void GameCenterPlatform_ReportProgress_m6_67 ();
extern "C" void GameCenterPlatform_LoadAchievements_m6_68 ();
extern "C" void GameCenterPlatform_ReportScore_m6_69 ();
extern "C" void GameCenterPlatform_LoadScores_m6_70 ();
extern "C" void GameCenterPlatform_LoadScores_m6_71 ();
extern "C" void GameCenterPlatform_LeaderboardCallbackWrapper_m6_72 ();
extern "C" void GameCenterPlatform_GetLoading_m6_73 ();
extern "C" void GameCenterPlatform_VerifyAuthentication_m6_74 ();
extern "C" void GameCenterPlatform_ShowAchievementsUI_m6_75 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m6_76 ();
extern "C" void GameCenterPlatform_ClearUsers_m6_77 ();
extern "C" void GameCenterPlatform_SetUser_m6_78 ();
extern "C" void GameCenterPlatform_SetUserImage_m6_79 ();
extern "C" void GameCenterPlatform_TriggerUsersCallbackWrapper_m6_80 ();
extern "C" void GameCenterPlatform_LoadUsers_m6_81 ();
extern "C" void GameCenterPlatform_SafeSetUserImage_m6_82 ();
extern "C" void GameCenterPlatform_SafeClearArray_m6_83 ();
extern "C" void GameCenterPlatform_CreateLeaderboard_m6_84 ();
extern "C" void GameCenterPlatform_CreateAchievement_m6_85 ();
extern "C" void GameCenterPlatform_TriggerResetAchievementCallback_m6_86 ();
extern "C" void GcLeaderboard__ctor_m6_87 ();
extern "C" void GcLeaderboard_Finalize_m6_88 ();
extern "C" void GcLeaderboard_Contains_m6_89 ();
extern "C" void GcLeaderboard_SetScores_m6_90 ();
extern "C" void GcLeaderboard_SetLocalScore_m6_91 ();
extern "C" void GcLeaderboard_SetMaxRange_m6_92 ();
extern "C" void GcLeaderboard_SetTitle_m6_93 ();
extern "C" void GcLeaderboard_Internal_LoadScores_m6_94 ();
extern "C" void GcLeaderboard_Internal_LoadScoresWithUsers_m6_95 ();
extern "C" void GcLeaderboard_Loading_m6_96 ();
extern "C" void GcLeaderboard_Dispose_m6_97 ();
extern "C" void BoneWeight_get_weight0_m6_98 ();
extern "C" void BoneWeight_set_weight0_m6_99 ();
extern "C" void BoneWeight_get_weight1_m6_100 ();
extern "C" void BoneWeight_set_weight1_m6_101 ();
extern "C" void BoneWeight_get_weight2_m6_102 ();
extern "C" void BoneWeight_set_weight2_m6_103 ();
extern "C" void BoneWeight_get_weight3_m6_104 ();
extern "C" void BoneWeight_set_weight3_m6_105 ();
extern "C" void BoneWeight_get_boneIndex0_m6_106 ();
extern "C" void BoneWeight_set_boneIndex0_m6_107 ();
extern "C" void BoneWeight_get_boneIndex1_m6_108 ();
extern "C" void BoneWeight_set_boneIndex1_m6_109 ();
extern "C" void BoneWeight_get_boneIndex2_m6_110 ();
extern "C" void BoneWeight_set_boneIndex2_m6_111 ();
extern "C" void BoneWeight_get_boneIndex3_m6_112 ();
extern "C" void BoneWeight_set_boneIndex3_m6_113 ();
extern "C" void BoneWeight_GetHashCode_m6_114 ();
extern "C" void BoneWeight_Equals_m6_115 ();
extern "C" void BoneWeight_op_Equality_m6_116 ();
extern "C" void BoneWeight_op_Inequality_m6_117 ();
extern "C" void Renderer_set_enabled_m6_118 ();
extern "C" void Renderer_get_material_m6_119 ();
extern "C" void Renderer_set_material_m6_120 ();
extern "C" void Screen_get_width_m6_121 ();
extern "C" void Screen_get_height_m6_122 ();
extern "C" void GUILayer_HitTest_m6_123 ();
extern "C" void GUILayer_INTERNAL_CALL_HitTest_m6_124 ();
extern "C" void Texture__ctor_m6_125 ();
extern "C" void Texture_Internal_GetWidth_m6_126 ();
extern "C" void Texture_Internal_GetHeight_m6_127 ();
extern "C" void Texture_get_width_m6_128 ();
extern "C" void Texture_get_height_m6_129 ();
extern "C" void Texture2D__ctor_m6_130 ();
extern "C" void Texture2D_Internal_Create_m6_131 ();
extern "C" void RenderTexture_Internal_GetWidth_m6_132 ();
extern "C" void RenderTexture_Internal_GetHeight_m6_133 ();
extern "C" void RenderTexture_get_width_m6_134 ();
extern "C" void RenderTexture_get_height_m6_135 ();
extern "C" void StateChanged__ctor_m6_136 ();
extern "C" void StateChanged_Invoke_m6_137 ();
extern "C" void StateChanged_BeginInvoke_m6_138 ();
extern "C" void StateChanged_EndInvoke_m6_139 ();
extern "C" void CullingGroup_Finalize_m6_140 ();
extern "C" void CullingGroup_Dispose_m6_141 ();
extern "C" void CullingGroup_SendEvents_m6_142 ();
extern "C" void CullingGroup_FinalizerFailure_m6_143 ();
extern "C" void GradientColorKey__ctor_m6_144 ();
extern "C" void GradientAlphaKey__ctor_m6_145 ();
extern "C" void Gradient__ctor_m6_146 ();
extern "C" void Gradient_Init_m6_147 ();
extern "C" void Gradient_Cleanup_m6_148 ();
extern "C" void Gradient_Finalize_m6_149 ();
extern "C" void TouchScreenKeyboard__ctor_m6_150 ();
extern "C" void TouchScreenKeyboard_Destroy_m6_151 ();
extern "C" void TouchScreenKeyboard_Finalize_m6_152 ();
extern "C" void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6_153 ();
extern "C" void TouchScreenKeyboard_get_isSupported_m6_154 ();
extern "C" void TouchScreenKeyboard_Open_m6_155 ();
extern "C" void TouchScreenKeyboard_Open_m6_156 ();
extern "C" void TouchScreenKeyboard_get_text_m6_157 ();
extern "C" void TouchScreenKeyboard_get_done_m6_158 ();
extern "C" void Gizmos_DrawWireSphere_m6_159 ();
extern "C" void Gizmos_INTERNAL_CALL_DrawWireSphere_m6_160 ();
extern "C" void Gizmos_DrawSphere_m6_161 ();
extern "C" void Gizmos_INTERNAL_CALL_DrawSphere_m6_162 ();
extern "C" void Gizmos_DrawWireCube_m6_163 ();
extern "C" void Gizmos_INTERNAL_CALL_DrawWireCube_m6_164 ();
extern "C" void Gizmos_DrawCube_m6_165 ();
extern "C" void Gizmos_INTERNAL_CALL_DrawCube_m6_166 ();
extern "C" void Gizmos_set_color_m6_167 ();
extern "C" void Gizmos_INTERNAL_set_color_m6_168 ();
extern "C" void LayerMask_get_value_m6_169 ();
extern "C" void LayerMask_set_value_m6_170 ();
extern "C" void LayerMask_LayerToName_m6_171 ();
extern "C" void LayerMask_NameToLayer_m6_172 ();
extern "C" void LayerMask_GetMask_m6_173 ();
extern "C" void LayerMask_op_Implicit_m6_174 ();
extern "C" void LayerMask_op_Implicit_m6_175 ();
extern "C" void Vector2__ctor_m6_176 ();
extern "C" void Vector2_ToString_m6_177 ();
extern "C" void Vector2_GetHashCode_m6_178 ();
extern "C" void Vector2_Equals_m6_179 ();
extern "C" void Vector2_get_sqrMagnitude_m6_180 ();
extern "C" void Vector2_SqrMagnitude_m6_181 ();
extern "C" void Vector2_get_zero_m6_182 ();
extern "C" void Vector2_get_up_m6_183 ();
extern "C" void Vector2_op_Addition_m6_184 ();
extern "C" void Vector2_op_Subtraction_m6_185 ();
extern "C" void Vector2_op_UnaryNegation_m6_186 ();
extern "C" void Vector2_op_Multiply_m6_187 ();
extern "C" void Vector2_op_Equality_m6_188 ();
extern "C" void Vector2_op_Implicit_m6_189 ();
extern "C" void Vector2_op_Implicit_m6_190 ();
extern "C" void Vector3__ctor_m6_191 ();
extern "C" void Vector3_Lerp_m6_192 ();
extern "C" void Vector3_Slerp_m6_193 ();
extern "C" void Vector3_INTERNAL_CALL_Slerp_m6_194 ();
extern "C" void Vector3_MoveTowards_m6_195 ();
extern "C" void Vector3_RotateTowards_m6_196 ();
extern "C" void Vector3_INTERNAL_CALL_RotateTowards_m6_197 ();
extern "C" void Vector3_GetHashCode_m6_198 ();
extern "C" void Vector3_Equals_m6_199 ();
extern "C" void Vector3_Normalize_m6_200 ();
extern "C" void Vector3_get_normalized_m6_201 ();
extern "C" void Vector3_ToString_m6_202 ();
extern "C" void Vector3_ToString_m6_203 ();
extern "C" void Vector3_Dot_m6_204 ();
extern "C" void Vector3_Angle_m6_205 ();
extern "C" void Vector3_Distance_m6_206 ();
extern "C" void Vector3_Magnitude_m6_207 ();
extern "C" void Vector3_get_magnitude_m6_208 ();
extern "C" void Vector3_SqrMagnitude_m6_209 ();
extern "C" void Vector3_get_sqrMagnitude_m6_210 ();
extern "C" void Vector3_Min_m6_211 ();
extern "C" void Vector3_Max_m6_212 ();
extern "C" void Vector3_get_zero_m6_213 ();
extern "C" void Vector3_get_one_m6_214 ();
extern "C" void Vector3_get_forward_m6_215 ();
extern "C" void Vector3_get_back_m6_216 ();
extern "C" void Vector3_get_up_m6_217 ();
extern "C" void Vector3_get_down_m6_218 ();
extern "C" void Vector3_get_left_m6_219 ();
extern "C" void Vector3_get_right_m6_220 ();
extern "C" void Vector3_op_Addition_m6_221 ();
extern "C" void Vector3_op_Subtraction_m6_222 ();
extern "C" void Vector3_op_UnaryNegation_m6_223 ();
extern "C" void Vector3_op_Multiply_m6_224 ();
extern "C" void Vector3_op_Multiply_m6_225 ();
extern "C" void Vector3_op_Division_m6_226 ();
extern "C" void Vector3_op_Equality_m6_227 ();
extern "C" void Vector3_op_Inequality_m6_228 ();
extern "C" void Color__ctor_m6_229 ();
extern "C" void Color__ctor_m6_230 ();
extern "C" void Color_ToString_m6_231 ();
extern "C" void Color_GetHashCode_m6_232 ();
extern "C" void Color_Equals_m6_233 ();
extern "C" void Color_get_red_m6_234 ();
extern "C" void Color_get_green_m6_235 ();
extern "C" void Color_get_white_m6_236 ();
extern "C" void Color_get_yellow_m6_237 ();
extern "C" void Color_get_cyan_m6_238 ();
extern "C" void Color_op_Multiply_m6_239 ();
extern "C" void Color_op_Implicit_m6_240 ();
extern "C" void Color32__ctor_m6_241 ();
extern "C" void Color32_ToString_m6_242 ();
extern "C" void Quaternion__ctor_m6_243 ();
extern "C" void Quaternion_get_identity_m6_244 ();
extern "C" void Quaternion_Dot_m6_245 ();
extern "C" void Quaternion_LookRotation_m6_246 ();
extern "C" void Quaternion_INTERNAL_CALL_LookRotation_m6_247 ();
extern "C" void Quaternion_Slerp_m6_248 ();
extern "C" void Quaternion_INTERNAL_CALL_Slerp_m6_249 ();
extern "C" void Quaternion_SlerpUnclamped_m6_250 ();
extern "C" void Quaternion_INTERNAL_CALL_SlerpUnclamped_m6_251 ();
extern "C" void Quaternion_Lerp_m6_252 ();
extern "C" void Quaternion_INTERNAL_CALL_Lerp_m6_253 ();
extern "C" void Quaternion_RotateTowards_m6_254 ();
extern "C" void Quaternion_Inverse_m6_255 ();
extern "C" void Quaternion_INTERNAL_CALL_Inverse_m6_256 ();
extern "C" void Quaternion_ToString_m6_257 ();
extern "C" void Quaternion_Angle_m6_258 ();
extern "C" void Quaternion_get_eulerAngles_m6_259 ();
extern "C" void Quaternion_Euler_m6_260 ();
extern "C" void Quaternion_Internal_ToEulerRad_m6_261 ();
extern "C" void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m6_262 ();
extern "C" void Quaternion_Internal_FromEulerRad_m6_263 ();
extern "C" void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6_264 ();
extern "C" void Quaternion_GetHashCode_m6_265 ();
extern "C" void Quaternion_Equals_m6_266 ();
extern "C" void Quaternion_op_Multiply_m6_267 ();
extern "C" void Quaternion_op_Multiply_m6_268 ();
extern "C" void Quaternion_op_Inequality_m6_269 ();
extern "C" void Rect__ctor_m6_270 ();
extern "C" void Rect__ctor_m6_271 ();
extern "C" void Rect_MinMaxRect_m6_272 ();
extern "C" void Rect_get_x_m6_273 ();
extern "C" void Rect_set_x_m6_274 ();
extern "C" void Rect_get_y_m6_275 ();
extern "C" void Rect_set_y_m6_276 ();
extern "C" void Rect_get_width_m6_277 ();
extern "C" void Rect_set_width_m6_278 ();
extern "C" void Rect_get_height_m6_279 ();
extern "C" void Rect_set_height_m6_280 ();
extern "C" void Rect_get_xMin_m6_281 ();
extern "C" void Rect_get_yMin_m6_282 ();
extern "C" void Rect_get_xMax_m6_283 ();
extern "C" void Rect_get_yMax_m6_284 ();
extern "C" void Rect_ToString_m6_285 ();
extern "C" void Rect_Contains_m6_286 ();
extern "C" void Rect_Contains_m6_287 ();
extern "C" void Rect_GetHashCode_m6_288 ();
extern "C" void Rect_Equals_m6_289 ();
extern "C" void Rect_op_Equality_m6_290 ();
extern "C" void Matrix4x4_get_Item_m6_291 ();
extern "C" void Matrix4x4_set_Item_m6_292 ();
extern "C" void Matrix4x4_get_Item_m6_293 ();
extern "C" void Matrix4x4_set_Item_m6_294 ();
extern "C" void Matrix4x4_GetHashCode_m6_295 ();
extern "C" void Matrix4x4_Equals_m6_296 ();
extern "C" void Matrix4x4_Inverse_m6_297 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Inverse_m6_298 ();
extern "C" void Matrix4x4_Transpose_m6_299 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Transpose_m6_300 ();
extern "C" void Matrix4x4_Invert_m6_301 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Invert_m6_302 ();
extern "C" void Matrix4x4_get_inverse_m6_303 ();
extern "C" void Matrix4x4_get_transpose_m6_304 ();
extern "C" void Matrix4x4_get_isIdentity_m6_305 ();
extern "C" void Matrix4x4_GetColumn_m6_306 ();
extern "C" void Matrix4x4_GetRow_m6_307 ();
extern "C" void Matrix4x4_SetColumn_m6_308 ();
extern "C" void Matrix4x4_SetRow_m6_309 ();
extern "C" void Matrix4x4_MultiplyPoint_m6_310 ();
extern "C" void Matrix4x4_MultiplyPoint3x4_m6_311 ();
extern "C" void Matrix4x4_MultiplyVector_m6_312 ();
extern "C" void Matrix4x4_Scale_m6_313 ();
extern "C" void Matrix4x4_get_zero_m6_314 ();
extern "C" void Matrix4x4_get_identity_m6_315 ();
extern "C" void Matrix4x4_SetTRS_m6_316 ();
extern "C" void Matrix4x4_TRS_m6_317 ();
extern "C" void Matrix4x4_INTERNAL_CALL_TRS_m6_318 ();
extern "C" void Matrix4x4_ToString_m6_319 ();
extern "C" void Matrix4x4_ToString_m6_320 ();
extern "C" void Matrix4x4_Ortho_m6_321 ();
extern "C" void Matrix4x4_Perspective_m6_322 ();
extern "C" void Matrix4x4_op_Multiply_m6_323 ();
extern "C" void Matrix4x4_op_Multiply_m6_324 ();
extern "C" void Matrix4x4_op_Equality_m6_325 ();
extern "C" void Matrix4x4_op_Inequality_m6_326 ();
extern "C" void Bounds__ctor_m6_327 ();
extern "C" void Bounds_GetHashCode_m6_328 ();
extern "C" void Bounds_Equals_m6_329 ();
extern "C" void Bounds_get_center_m6_330 ();
extern "C" void Bounds_set_center_m6_331 ();
extern "C" void Bounds_get_size_m6_332 ();
extern "C" void Bounds_set_size_m6_333 ();
extern "C" void Bounds_get_extents_m6_334 ();
extern "C" void Bounds_set_extents_m6_335 ();
extern "C" void Bounds_get_min_m6_336 ();
extern "C" void Bounds_set_min_m6_337 ();
extern "C" void Bounds_get_max_m6_338 ();
extern "C" void Bounds_set_max_m6_339 ();
extern "C" void Bounds_SetMinMax_m6_340 ();
extern "C" void Bounds_Encapsulate_m6_341 ();
extern "C" void Bounds_Encapsulate_m6_342 ();
extern "C" void Bounds_Expand_m6_343 ();
extern "C" void Bounds_Expand_m6_344 ();
extern "C" void Bounds_Intersects_m6_345 ();
extern "C" void Bounds_Internal_Contains_m6_346 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_Contains_m6_347 ();
extern "C" void Bounds_Contains_m6_348 ();
extern "C" void Bounds_Internal_SqrDistance_m6_349 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_SqrDistance_m6_350 ();
extern "C" void Bounds_SqrDistance_m6_351 ();
extern "C" void Bounds_Internal_IntersectRay_m6_352 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_IntersectRay_m6_353 ();
extern "C" void Bounds_IntersectRay_m6_354 ();
extern "C" void Bounds_IntersectRay_m6_355 ();
extern "C" void Bounds_Internal_GetClosestPoint_m6_356 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6_357 ();
extern "C" void Bounds_ClosestPoint_m6_358 ();
extern "C" void Bounds_ToString_m6_359 ();
extern "C" void Bounds_ToString_m6_360 ();
extern "C" void Bounds_op_Equality_m6_361 ();
extern "C" void Bounds_op_Inequality_m6_362 ();
extern "C" void Vector4__ctor_m6_363 ();
extern "C" void Vector4_GetHashCode_m6_364 ();
extern "C" void Vector4_Equals_m6_365 ();
extern "C" void Vector4_ToString_m6_366 ();
extern "C" void Vector4_Dot_m6_367 ();
extern "C" void Vector4_SqrMagnitude_m6_368 ();
extern "C" void Vector4_op_Subtraction_m6_369 ();
extern "C" void Vector4_op_Equality_m6_370 ();
extern "C" void Ray__ctor_m6_371 ();
extern "C" void Ray_get_origin_m6_372 ();
extern "C" void Ray_get_direction_m6_373 ();
extern "C" void Ray_GetPoint_m6_374 ();
extern "C" void Ray_ToString_m6_375 ();
extern "C" void MathfInternal__cctor_m6_376 ();
extern "C" void Mathf__cctor_m6_377 ();
extern "C" void Mathf_Acos_m6_378 ();
extern "C" void Mathf_Sqrt_m6_379 ();
extern "C" void Mathf_Abs_m6_380 ();
extern "C" void Mathf_Min_m6_381 ();
extern "C" void Mathf_Min_m6_382 ();
extern "C" void Mathf_Max_m6_383 ();
extern "C" void Mathf_Max_m6_384 ();
extern "C" void Mathf_Floor_m6_385 ();
extern "C" void Mathf_Round_m6_386 ();
extern "C" void Mathf_FloorToInt_m6_387 ();
extern "C" void Mathf_Sign_m6_388 ();
extern "C" void Mathf_Clamp_m6_389 ();
extern "C" void Mathf_Clamp_m6_390 ();
extern "C" void Mathf_Clamp01_m6_391 ();
extern "C" void Mathf_Lerp_m6_392 ();
extern "C" void Mathf_MoveTowards_m6_393 ();
extern "C" void Mathf_Approximately_m6_394 ();
extern "C" void Mathf_SmoothDamp_m6_395 ();
extern "C" void Mathf_SmoothDamp_m6_396 ();
extern "C" void Mathf_SmoothDampAngle_m6_397 ();
extern "C" void Mathf_SmoothDampAngle_m6_398 ();
extern "C" void Mathf_Repeat_m6_399 ();
extern "C" void Mathf_DeltaAngle_m6_400 ();
extern "C" void ReapplyDrivenProperties__ctor_m6_401 ();
extern "C" void ReapplyDrivenProperties_Invoke_m6_402 ();
extern "C" void ReapplyDrivenProperties_BeginInvoke_m6_403 ();
extern "C" void ReapplyDrivenProperties_EndInvoke_m6_404 ();
extern "C" void RectTransform_SendReapplyDrivenProperties_m6_405 ();
extern "C" void ResourceRequest__ctor_m6_406 ();
extern "C" void ResourceRequest_get_asset_m6_407 ();
extern "C" void Resources_FindObjectsOfTypeAll_m6_408 ();
extern "C" void Resources_Load_m6_409 ();
extern "C" void SerializePrivateVariables__ctor_m6_410 ();
extern "C" void SerializeField__ctor_m6_411 ();
extern "C" void Shader_PropertyToID_m6_412 ();
extern "C" void Material__ctor_m6_413 ();
extern "C" void Material_set_color_m6_414 ();
extern "C" void Material_SetColor_m6_415 ();
extern "C" void Material_SetColor_m6_416 ();
extern "C" void Material_INTERNAL_CALL_SetColor_m6_417 ();
extern "C" void Material_Internal_CreateWithMaterial_m6_418 ();
extern "C" void SphericalHarmonicsL2_Clear_m6_419 ();
extern "C" void SphericalHarmonicsL2_ClearInternal_m6_420 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6_421 ();
extern "C" void SphericalHarmonicsL2_AddAmbientLight_m6_422 ();
extern "C" void SphericalHarmonicsL2_AddAmbientLightInternal_m6_423 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6_424 ();
extern "C" void SphericalHarmonicsL2_AddDirectionalLight_m6_425 ();
extern "C" void SphericalHarmonicsL2_AddDirectionalLightInternal_m6_426 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6_427 ();
extern "C" void SphericalHarmonicsL2_get_Item_m6_428 ();
extern "C" void SphericalHarmonicsL2_set_Item_m6_429 ();
extern "C" void SphericalHarmonicsL2_GetHashCode_m6_430 ();
extern "C" void SphericalHarmonicsL2_Equals_m6_431 ();
extern "C" void SphericalHarmonicsL2_op_Multiply_m6_432 ();
extern "C" void SphericalHarmonicsL2_op_Multiply_m6_433 ();
extern "C" void SphericalHarmonicsL2_op_Addition_m6_434 ();
extern "C" void SphericalHarmonicsL2_op_Equality_m6_435 ();
extern "C" void SphericalHarmonicsL2_op_Inequality_m6_436 ();
extern "C" void UnityString_Format_m6_437 ();
extern "C" void AsyncOperation__ctor_m6_438 ();
extern "C" void AsyncOperation_InternalDestroy_m6_439 ();
extern "C" void AsyncOperation_Finalize_m6_440 ();
extern "C" void LogCallback__ctor_m6_441 ();
extern "C" void LogCallback_Invoke_m6_442 ();
extern "C" void LogCallback_BeginInvoke_m6_443 ();
extern "C" void LogCallback_EndInvoke_m6_444 ();
extern "C" void Application_Quit_m6_445 ();
extern "C" void Application_get_loadedLevel_m6_446 ();
extern "C" void Application_get_loadedLevelName_m6_447 ();
extern "C" void Application_LoadLevel_m6_448 ();
extern "C" void Application_LoadLevel_m6_449 ();
extern "C" void Application_LoadLevelAsync_m6_450 ();
extern "C" void Application_get_isPlaying_m6_451 ();
extern "C" void Application_get_platform_m6_452 ();
extern "C" void Application_set_runInBackground_m6_453 ();
extern "C" void Application_OpenURL_m6_454 ();
extern "C" void Application_CallLogCallback_m6_455 ();
extern "C" void Behaviour__ctor_m6_456 ();
extern "C" void Behaviour_get_enabled_m6_457 ();
extern "C" void Behaviour_set_enabled_m6_458 ();
extern "C" void CameraCallback__ctor_m6_459 ();
extern "C" void CameraCallback_Invoke_m6_460 ();
extern "C" void CameraCallback_BeginInvoke_m6_461 ();
extern "C" void CameraCallback_EndInvoke_m6_462 ();
extern "C" void Camera_get_nearClipPlane_m6_463 ();
extern "C" void Camera_get_farClipPlane_m6_464 ();
extern "C" void Camera_get_cullingMask_m6_465 ();
extern "C" void Camera_get_eventMask_m6_466 ();
extern "C" void Camera_get_pixelRect_m6_467 ();
extern "C" void Camera_INTERNAL_get_pixelRect_m6_468 ();
extern "C" void Camera_get_targetTexture_m6_469 ();
extern "C" void Camera_get_clearFlags_m6_470 ();
extern "C" void Camera_ViewportPointToRay_m6_471 ();
extern "C" void Camera_INTERNAL_CALL_ViewportPointToRay_m6_472 ();
extern "C" void Camera_ScreenPointToRay_m6_473 ();
extern "C" void Camera_INTERNAL_CALL_ScreenPointToRay_m6_474 ();
extern "C" void Camera_get_main_m6_475 ();
extern "C" void Camera_get_allCamerasCount_m6_476 ();
extern "C" void Camera_GetAllCameras_m6_477 ();
extern "C" void Camera_FireOnPreCull_m6_478 ();
extern "C" void Camera_FireOnPreRender_m6_479 ();
extern "C" void Camera_FireOnPostRender_m6_480 ();
extern "C" void Camera_RaycastTry_m6_481 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry_m6_482 ();
extern "C" void Camera_RaycastTry2D_m6_483 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry2D_m6_484 ();
extern "C" void Debug_DrawLine_m6_485 ();
extern "C" void Debug_DrawLine_m6_486 ();
extern "C" void Debug_INTERNAL_CALL_DrawLine_m6_487 ();
extern "C" void Debug_Internal_Log_m6_488 ();
extern "C" void Debug_Log_m6_489 ();
extern "C" void Debug_LogError_m6_490 ();
extern "C" void Debug_LogWarning_m6_491 ();
extern "C" void Debug_LogWarning_m6_492 ();
extern "C" void DisplaysUpdatedDelegate__ctor_m6_493 ();
extern "C" void DisplaysUpdatedDelegate_Invoke_m6_494 ();
extern "C" void DisplaysUpdatedDelegate_BeginInvoke_m6_495 ();
extern "C" void DisplaysUpdatedDelegate_EndInvoke_m6_496 ();
extern "C" void Display__ctor_m6_497 ();
extern "C" void Display__ctor_m6_498 ();
extern "C" void Display__cctor_m6_499 ();
extern "C" void Display_add_onDisplaysUpdated_m6_500 ();
extern "C" void Display_remove_onDisplaysUpdated_m6_501 ();
extern "C" void Display_get_renderingWidth_m6_502 ();
extern "C" void Display_get_renderingHeight_m6_503 ();
extern "C" void Display_get_systemWidth_m6_504 ();
extern "C" void Display_get_systemHeight_m6_505 ();
extern "C" void Display_get_colorBuffer_m6_506 ();
extern "C" void Display_get_depthBuffer_m6_507 ();
extern "C" void Display_Activate_m6_508 ();
extern "C" void Display_Activate_m6_509 ();
extern "C" void Display_SetParams_m6_510 ();
extern "C" void Display_SetRenderingResolution_m6_511 ();
extern "C" void Display_MultiDisplayLicense_m6_512 ();
extern "C" void Display_RelativeMouseAt_m6_513 ();
extern "C" void Display_get_main_m6_514 ();
extern "C" void Display_RecreateDisplayList_m6_515 ();
extern "C" void Display_FireDisplaysUpdated_m6_516 ();
extern "C" void Display_GetSystemExtImpl_m6_517 ();
extern "C" void Display_GetRenderingExtImpl_m6_518 ();
extern "C" void Display_GetRenderingBuffersImpl_m6_519 ();
extern "C" void Display_SetRenderingResolutionImpl_m6_520 ();
extern "C" void Display_ActivateDisplayImpl_m6_521 ();
extern "C" void Display_SetParamsImpl_m6_522 ();
extern "C" void Display_MultiDisplayLicenseImpl_m6_523 ();
extern "C" void Display_RelativeMouseAtImpl_m6_524 ();
extern "C" void MonoBehaviour__ctor_m6_525 ();
extern "C" void MonoBehaviour_Internal_CancelInvokeAll_m6_526 ();
extern "C" void MonoBehaviour_Invoke_m6_527 ();
extern "C" void MonoBehaviour_InvokeRepeating_m6_528 ();
extern "C" void MonoBehaviour_CancelInvoke_m6_529 ();
extern "C" void MonoBehaviour_StartCoroutine_m6_530 ();
extern "C" void MonoBehaviour_StartCoroutine_Auto_m6_531 ();
extern "C" void Touch_get_position_m6_532 ();
extern "C" void Touch_get_phase_m6_533 ();
extern "C" void Input__cctor_m6_534 ();
extern "C" void Input_GetKeyInt_m6_535 ();
extern "C" void Input_GetKeyDownInt_m6_536 ();
extern "C" void Input_GetAxis_m6_537 ();
extern "C" void Input_GetAxisRaw_m6_538 ();
extern "C" void Input_GetButton_m6_539 ();
extern "C" void Input_GetButtonDown_m6_540 ();
extern "C" void Input_GetKey_m6_541 ();
extern "C" void Input_GetKeyDown_m6_542 ();
extern "C" void Input_GetMouseButton_m6_543 ();
extern "C" void Input_GetMouseButtonDown_m6_544 ();
extern "C" void Input_GetMouseButtonUp_m6_545 ();
extern "C" void Input_get_mousePosition_m6_546 ();
extern "C" void Input_INTERNAL_get_mousePosition_m6_547 ();
extern "C" void Input_GetTouch_m6_548 ();
extern "C" void Input_get_touchCount_m6_549 ();
extern "C" void Input_get_compositionString_m6_550 ();
extern "C" void Input_set_compositionCursorPos_m6_551 ();
extern "C" void Input_INTERNAL_set_compositionCursorPos_m6_552 ();
extern "C" void Object__ctor_m6_553 ();
extern "C" void Object_Internal_CloneSingle_m6_554 ();
extern "C" void Object_Internal_InstantiateSingle_m6_555 ();
extern "C" void Object_INTERNAL_CALL_Internal_InstantiateSingle_m6_556 ();
extern "C" void Object_Destroy_m6_557 ();
extern "C" void Object_Destroy_m6_558 ();
extern "C" void Object_DestroyImmediate_m6_559 ();
extern "C" void Object_DestroyImmediate_m6_560 ();
extern "C" void Object_FindObjectsOfType_m6_561 ();
extern "C" void Object_get_name_m6_562 ();
extern "C" void Object_set_name_m6_563 ();
extern "C" void Object_DontDestroyOnLoad_m6_564 ();
extern "C" void Object_set_hideFlags_m6_565 ();
extern "C" void Object_DestroyObject_m6_566 ();
extern "C" void Object_DestroyObject_m6_567 ();
extern "C" void Object_ToString_m6_568 ();
extern "C" void Object_Equals_m6_569 ();
extern "C" void Object_GetHashCode_m6_570 ();
extern "C" void Object_CompareBaseObjects_m6_571 ();
extern "C" void Object_IsNativeObjectAlive_m6_572 ();
extern "C" void Object_GetInstanceID_m6_573 ();
extern "C" void Object_GetCachedPtr_m6_574 ();
extern "C" void Object_Instantiate_m6_575 ();
extern "C" void Object_CheckNullArgument_m6_576 ();
extern "C" void Object_FindObjectOfType_m6_577 ();
extern "C" void Object_op_Implicit_m6_578 ();
extern "C" void Object_op_Equality_m6_579 ();
extern "C" void Object_op_Inequality_m6_580 ();
extern "C" void Component__ctor_m6_581 ();
extern "C" void Component_get_transform_m6_582 ();
extern "C" void Component_get_gameObject_m6_583 ();
extern "C" void Component_GetComponentFastPath_m6_584 ();
extern "C" void Component_GetComponentInChildren_m6_585 ();
extern "C" void Component_SendMessage_m6_586 ();
extern "C" void Component_SendMessage_m6_587 ();
extern "C" void Component_SendMessage_m6_588 ();
extern "C" void GameObject__ctor_m6_589 ();
extern "C" void GameObject__ctor_m6_590 ();
extern "C" void GameObject_GetComponent_m6_591 ();
extern "C" void GameObject_GetComponentFastPath_m6_592 ();
extern "C" void GameObject_GetComponentInChildren_m6_593 ();
extern "C" void GameObject_GetComponentsInternal_m6_594 ();
extern "C" void GameObject_get_transform_m6_595 ();
extern "C" void GameObject_SetActive_m6_596 ();
extern "C" void GameObject_get_activeInHierarchy_m6_597 ();
extern "C" void GameObject_set_tag_m6_598 ();
extern "C" void GameObject_FindGameObjectsWithTag_m6_599 ();
extern "C" void GameObject_SendMessage_m6_600 ();
extern "C" void GameObject_SendMessage_m6_601 ();
extern "C" void GameObject_SendMessage_m6_602 ();
extern "C" void GameObject_Internal_AddComponentWithType_m6_603 ();
extern "C" void GameObject_AddComponent_m6_604 ();
extern "C" void GameObject_Internal_CreateGameObject_m6_605 ();
extern "C" void GameObject_Find_m6_606 ();
extern "C" void Enumerator__ctor_m6_607 ();
extern "C" void Enumerator_get_Current_m6_608 ();
extern "C" void Enumerator_MoveNext_m6_609 ();
extern "C" void Enumerator_Reset_m6_610 ();
extern "C" void Transform_get_position_m6_611 ();
extern "C" void Transform_set_position_m6_612 ();
extern "C" void Transform_INTERNAL_get_position_m6_613 ();
extern "C" void Transform_INTERNAL_set_position_m6_614 ();
extern "C" void Transform_get_localPosition_m6_615 ();
extern "C" void Transform_set_localPosition_m6_616 ();
extern "C" void Transform_INTERNAL_get_localPosition_m6_617 ();
extern "C" void Transform_INTERNAL_set_localPosition_m6_618 ();
extern "C" void Transform_get_eulerAngles_m6_619 ();
extern "C" void Transform_get_right_m6_620 ();
extern "C" void Transform_get_up_m6_621 ();
extern "C" void Transform_get_forward_m6_622 ();
extern "C" void Transform_get_rotation_m6_623 ();
extern "C" void Transform_set_rotation_m6_624 ();
extern "C" void Transform_INTERNAL_get_rotation_m6_625 ();
extern "C" void Transform_INTERNAL_set_rotation_m6_626 ();
extern "C" void Transform_get_localRotation_m6_627 ();
extern "C" void Transform_set_localRotation_m6_628 ();
extern "C" void Transform_INTERNAL_get_localRotation_m6_629 ();
extern "C" void Transform_INTERNAL_set_localRotation_m6_630 ();
extern "C" void Transform_get_localScale_m6_631 ();
extern "C" void Transform_set_localScale_m6_632 ();
extern "C" void Transform_INTERNAL_get_localScale_m6_633 ();
extern "C" void Transform_INTERNAL_set_localScale_m6_634 ();
extern "C" void Transform_get_parent_m6_635 ();
extern "C" void Transform_set_parent_m6_636 ();
extern "C" void Transform_get_parentInternal_m6_637 ();
extern "C" void Transform_set_parentInternal_m6_638 ();
extern "C" void Transform_Rotate_m6_639 ();
extern "C" void Transform_Rotate_m6_640 ();
extern "C" void Transform_Rotate_m6_641 ();
extern "C" void Transform_LookAt_m6_642 ();
extern "C" void Transform_LookAt_m6_643 ();
extern "C" void Transform_LookAt_m6_644 ();
extern "C" void Transform_INTERNAL_CALL_LookAt_m6_645 ();
extern "C" void Transform_TransformDirection_m6_646 ();
extern "C" void Transform_INTERNAL_CALL_TransformDirection_m6_647 ();
extern "C" void Transform_get_root_m6_648 ();
extern "C" void Transform_get_childCount_m6_649 ();
extern "C" void Transform_GetEnumerator_m6_650 ();
extern "C" void Transform_GetChild_m6_651 ();
extern "C" void Time_get_time_m6_652 ();
extern "C" void Time_get_timeSinceLevelLoad_m6_653 ();
extern "C" void Time_get_deltaTime_m6_654 ();
extern "C" void Time_get_frameCount_m6_655 ();
extern "C" void Time_get_realtimeSinceStartup_m6_656 ();
extern "C" void Random_Range_m6_657 ();
extern "C" void Random_Range_m6_658 ();
extern "C" void Random_RandomRangeInt_m6_659 ();
extern "C" void Random_get_insideUnitSphere_m6_660 ();
extern "C" void Random_INTERNAL_get_insideUnitSphere_m6_661 ();
extern "C" void YieldInstruction__ctor_m6_662 ();
extern "C" void PlayerPrefsException__ctor_m6_663 ();
extern "C" void PlayerPrefs_TrySetSetString_m6_664 ();
extern "C" void PlayerPrefs_SetString_m6_665 ();
extern "C" void PlayerPrefs_GetString_m6_666 ();
extern "C" void PlayerPrefs_GetString_m6_667 ();
extern "C" void PlayerPrefs_DeleteKey_m6_668 ();
extern "C" void UnityAdsInternal__ctor_m6_669 ();
extern "C" void UnityAdsInternal_add_onCampaignsAvailable_m6_670 ();
extern "C" void UnityAdsInternal_remove_onCampaignsAvailable_m6_671 ();
extern "C" void UnityAdsInternal_add_onCampaignsFetchFailed_m6_672 ();
extern "C" void UnityAdsInternal_remove_onCampaignsFetchFailed_m6_673 ();
extern "C" void UnityAdsInternal_add_onShow_m6_674 ();
extern "C" void UnityAdsInternal_remove_onShow_m6_675 ();
extern "C" void UnityAdsInternal_add_onHide_m6_676 ();
extern "C" void UnityAdsInternal_remove_onHide_m6_677 ();
extern "C" void UnityAdsInternal_add_onVideoCompleted_m6_678 ();
extern "C" void UnityAdsInternal_remove_onVideoCompleted_m6_679 ();
extern "C" void UnityAdsInternal_add_onVideoStarted_m6_680 ();
extern "C" void UnityAdsInternal_remove_onVideoStarted_m6_681 ();
extern "C" void UnityAdsInternal_RegisterNative_m6_682 ();
extern "C" void UnityAdsInternal_Init_m6_683 ();
extern "C" void UnityAdsInternal_Show_m6_684 ();
extern "C" void UnityAdsInternal_CanShowAds_m6_685 ();
extern "C" void UnityAdsInternal_SetLogLevel_m6_686 ();
extern "C" void UnityAdsInternal_SetCampaignDataURL_m6_687 ();
extern "C" void UnityAdsInternal_RemoveAllEventHandlers_m6_688 ();
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsAvailable_m6_689 ();
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m6_690 ();
extern "C" void UnityAdsInternal_CallUnityAdsShow_m6_691 ();
extern "C" void UnityAdsInternal_CallUnityAdsHide_m6_692 ();
extern "C" void UnityAdsInternal_CallUnityAdsVideoCompleted_m6_693 ();
extern "C" void UnityAdsInternal_CallUnityAdsVideoStarted_m6_694 ();
extern "C" void Particle_get_position_m6_695 ();
extern "C" void Particle_set_position_m6_696 ();
extern "C" void Particle_get_velocity_m6_697 ();
extern "C" void Particle_set_velocity_m6_698 ();
extern "C" void Particle_get_energy_m6_699 ();
extern "C" void Particle_set_energy_m6_700 ();
extern "C" void Particle_get_startEnergy_m6_701 ();
extern "C" void Particle_set_startEnergy_m6_702 ();
extern "C" void Particle_get_size_m6_703 ();
extern "C" void Particle_set_size_m6_704 ();
extern "C" void Particle_get_rotation_m6_705 ();
extern "C" void Particle_set_rotation_m6_706 ();
extern "C" void Particle_get_angularVelocity_m6_707 ();
extern "C" void Particle_set_angularVelocity_m6_708 ();
extern "C" void Particle_get_color_m6_709 ();
extern "C" void Particle_set_color_m6_710 ();
extern "C" void ControllerColliderHit_get_moveDirection_m6_711 ();
extern "C" void Physics_Raycast_m6_712 ();
extern "C" void Physics_Raycast_m6_713 ();
extern "C" void Physics_Raycast_m6_714 ();
extern "C" void Physics_Internal_Raycast_m6_715 ();
extern "C" void Physics_INTERNAL_CALL_Internal_Raycast_m6_716 ();
extern "C" void Rigidbody_get_velocity_m6_717 ();
extern "C" void Rigidbody_set_velocity_m6_718 ();
extern "C" void Rigidbody_INTERNAL_get_velocity_m6_719 ();
extern "C" void Rigidbody_INTERNAL_set_velocity_m6_720 ();
extern "C" void Rigidbody_get_angularVelocity_m6_721 ();
extern "C" void Rigidbody_set_angularVelocity_m6_722 ();
extern "C" void Rigidbody_INTERNAL_get_angularVelocity_m6_723 ();
extern "C" void Rigidbody_INTERNAL_set_angularVelocity_m6_724 ();
extern "C" void Rigidbody_set_isKinematic_m6_725 ();
extern "C" void Rigidbody_AddForce_m6_726 ();
extern "C" void Rigidbody_INTERNAL_CALL_AddForce_m6_727 ();
extern "C" void Collider_get_bounds_m6_728 ();
extern "C" void Collider_INTERNAL_get_bounds_m6_729 ();
extern "C" void RaycastHit_get_point_m6_730 ();
extern "C" void RaycastHit_get_collider_m6_731 ();
extern "C" void CharacterController_Move_m6_732 ();
extern "C" void CharacterController_INTERNAL_CALL_Move_m6_733 ();
extern "C" void Physics2D__cctor_m6_734 ();
extern "C" void Physics2D_Internal_Raycast_m6_735 ();
extern "C" void Physics2D_INTERNAL_CALL_Internal_Raycast_m6_736 ();
extern "C" void Physics2D_Raycast_m6_737 ();
extern "C" void Physics2D_Raycast_m6_738 ();
extern "C" void RaycastHit2D_get_collider_m6_739 ();
extern "C" void Rigidbody2D_get_velocity_m6_740 ();
extern "C" void Rigidbody2D_set_velocity_m6_741 ();
extern "C" void Rigidbody2D_INTERNAL_get_velocity_m6_742 ();
extern "C" void Rigidbody2D_INTERNAL_set_velocity_m6_743 ();
extern "C" void Rigidbody2D_get_angularVelocity_m6_744 ();
extern "C" void Rigidbody2D_set_angularVelocity_m6_745 ();
extern "C" void Rigidbody2D_set_isKinematic_m6_746 ();
extern "C" void Rigidbody2D_AddForce_m6_747 ();
extern "C" void Rigidbody2D_INTERNAL_CALL_AddForce_m6_748 ();
extern "C" void AudioConfigurationChangeHandler__ctor_m6_749 ();
extern "C" void AudioConfigurationChangeHandler_Invoke_m6_750 ();
extern "C" void AudioConfigurationChangeHandler_BeginInvoke_m6_751 ();
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m6_752 ();
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m6_753 ();
extern "C" void PCMReaderCallback__ctor_m6_754 ();
extern "C" void PCMReaderCallback_Invoke_m6_755 ();
extern "C" void PCMReaderCallback_BeginInvoke_m6_756 ();
extern "C" void PCMReaderCallback_EndInvoke_m6_757 ();
extern "C" void PCMSetPositionCallback__ctor_m6_758 ();
extern "C" void PCMSetPositionCallback_Invoke_m6_759 ();
extern "C" void PCMSetPositionCallback_BeginInvoke_m6_760 ();
extern "C" void PCMSetPositionCallback_EndInvoke_m6_761 ();
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m6_762 ();
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m6_763 ();
extern "C" void AudioSource_set_clip_m6_764 ();
extern "C" void AudioSource_Play_m6_765 ();
extern "C" void AudioSource_Play_m6_766 ();
extern "C" void WebCamDevice_get_name_m6_767 ();
extern "C" void WebCamDevice_get_isFrontFacing_m6_768 ();
extern "C" void AnimationEvent__ctor_m6_769 ();
extern "C" void AnimationEvent_get_data_m6_770 ();
extern "C" void AnimationEvent_set_data_m6_771 ();
extern "C" void AnimationEvent_get_stringParameter_m6_772 ();
extern "C" void AnimationEvent_set_stringParameter_m6_773 ();
extern "C" void AnimationEvent_get_floatParameter_m6_774 ();
extern "C" void AnimationEvent_set_floatParameter_m6_775 ();
extern "C" void AnimationEvent_get_intParameter_m6_776 ();
extern "C" void AnimationEvent_set_intParameter_m6_777 ();
extern "C" void AnimationEvent_get_objectReferenceParameter_m6_778 ();
extern "C" void AnimationEvent_set_objectReferenceParameter_m6_779 ();
extern "C" void AnimationEvent_get_functionName_m6_780 ();
extern "C" void AnimationEvent_set_functionName_m6_781 ();
extern "C" void AnimationEvent_get_time_m6_782 ();
extern "C" void AnimationEvent_set_time_m6_783 ();
extern "C" void AnimationEvent_get_messageOptions_m6_784 ();
extern "C" void AnimationEvent_set_messageOptions_m6_785 ();
extern "C" void AnimationEvent_get_isFiredByLegacy_m6_786 ();
extern "C" void AnimationEvent_get_isFiredByAnimator_m6_787 ();
extern "C" void AnimationEvent_get_animationState_m6_788 ();
extern "C" void AnimationEvent_get_animatorStateInfo_m6_789 ();
extern "C" void AnimationEvent_get_animatorClipInfo_m6_790 ();
extern "C" void AnimationEvent_GetHash_m6_791 ();
extern "C" void Keyframe__ctor_m6_792 ();
extern "C" void AnimationCurve__ctor_m6_793 ();
extern "C" void AnimationCurve__ctor_m6_794 ();
extern "C" void AnimationCurve_Cleanup_m6_795 ();
extern "C" void AnimationCurve_Finalize_m6_796 ();
extern "C" void AnimationCurve_Init_m6_797 ();
extern "C" void Enumerator__ctor_m6_798 ();
extern "C" void Enumerator_get_Current_m6_799 ();
extern "C" void Enumerator_MoveNext_m6_800 ();
extern "C" void Enumerator_Reset_m6_801 ();
extern "C" void Animation_get_Item_m6_802 ();
extern "C" void Animation_CrossFade_m6_803 ();
extern "C" void Animation_CrossFade_m6_804 ();
extern "C" void Animation_GetEnumerator_m6_805 ();
extern "C" void Animation_GetState_m6_806 ();
extern "C" void Animation_GetStateAtIndex_m6_807 ();
extern "C" void Animation_GetStateCount_m6_808 ();
extern "C" void AnimationState_set_wrapMode_m6_809 ();
extern "C" void AnimationState_set_time_m6_810 ();
extern "C" void AnimationState_set_speed_m6_811 ();
extern "C" void AnimatorStateInfo_IsName_m6_812 ();
extern "C" void AnimatorStateInfo_get_fullPathHash_m6_813 ();
extern "C" void AnimatorStateInfo_get_nameHash_m6_814 ();
extern "C" void AnimatorStateInfo_get_shortNameHash_m6_815 ();
extern "C" void AnimatorStateInfo_get_normalizedTime_m6_816 ();
extern "C" void AnimatorStateInfo_get_length_m6_817 ();
extern "C" void AnimatorStateInfo_get_speed_m6_818 ();
extern "C" void AnimatorStateInfo_get_speedMultiplier_m6_819 ();
extern "C" void AnimatorStateInfo_get_tagHash_m6_820 ();
extern "C" void AnimatorStateInfo_IsTag_m6_821 ();
extern "C" void AnimatorStateInfo_get_loop_m6_822 ();
extern "C" void AnimatorTransitionInfo_IsName_m6_823 ();
extern "C" void AnimatorTransitionInfo_IsUserName_m6_824 ();
extern "C" void AnimatorTransitionInfo_get_fullPathHash_m6_825 ();
extern "C" void AnimatorTransitionInfo_get_nameHash_m6_826 ();
extern "C" void AnimatorTransitionInfo_get_userNameHash_m6_827 ();
extern "C" void AnimatorTransitionInfo_get_normalizedTime_m6_828 ();
extern "C" void AnimatorTransitionInfo_get_anyState_m6_829 ();
extern "C" void AnimatorTransitionInfo_get_entry_m6_830 ();
extern "C" void AnimatorTransitionInfo_get_exit_m6_831 ();
extern "C" void Animator_GetFloat_m6_832 ();
extern "C" void Animator_SetFloat_m6_833 ();
extern "C" void Animator_SetFloat_m6_834 ();
extern "C" void Animator_GetBool_m6_835 ();
extern "C" void Animator_SetBool_m6_836 ();
extern "C" void Animator_GetInteger_m6_837 ();
extern "C" void Animator_SetInteger_m6_838 ();
extern "C" void Animator_SetTrigger_m6_839 ();
extern "C" void Animator_set_applyRootMotion_m6_840 ();
extern "C" void Animator_get_layerCount_m6_841 ();
extern "C" void Animator_GetLayerWeight_m6_842 ();
extern "C" void Animator_SetLayerWeight_m6_843 ();
extern "C" void Animator_GetCurrentAnimatorStateInfo_m6_844 ();
extern "C" void Animator_StringToHash_m6_845 ();
extern "C" void Animator_SetFloatString_m6_846 ();
extern "C" void Animator_GetFloatString_m6_847 ();
extern "C" void Animator_SetBoolString_m6_848 ();
extern "C" void Animator_GetBoolString_m6_849 ();
extern "C" void Animator_SetIntegerString_m6_850 ();
extern "C" void Animator_GetIntegerString_m6_851 ();
extern "C" void Animator_SetTriggerString_m6_852 ();
extern "C" void Animator_SetFloatStringDamp_m6_853 ();
extern "C" void HumanBone_get_boneName_m6_854 ();
extern "C" void HumanBone_set_boneName_m6_855 ();
extern "C" void HumanBone_get_humanName_m6_856 ();
extern "C" void HumanBone_set_humanName_m6_857 ();
extern "C" void TextMesh_set_text_m6_858 ();
extern "C" void TextMesh_set_font_m6_859 ();
extern "C" void TextMesh_set_anchor_m6_860 ();
extern "C" void TextMesh_set_characterSize_m6_861 ();
extern "C" void CharacterInfo_get_advance_m6_862 ();
extern "C" void CharacterInfo_set_advance_m6_863 ();
extern "C" void CharacterInfo_get_glyphWidth_m6_864 ();
extern "C" void CharacterInfo_set_glyphWidth_m6_865 ();
extern "C" void CharacterInfo_get_glyphHeight_m6_866 ();
extern "C" void CharacterInfo_set_glyphHeight_m6_867 ();
extern "C" void CharacterInfo_get_bearing_m6_868 ();
extern "C" void CharacterInfo_set_bearing_m6_869 ();
extern "C" void CharacterInfo_get_minY_m6_870 ();
extern "C" void CharacterInfo_set_minY_m6_871 ();
extern "C" void CharacterInfo_get_maxY_m6_872 ();
extern "C" void CharacterInfo_set_maxY_m6_873 ();
extern "C" void CharacterInfo_get_minX_m6_874 ();
extern "C" void CharacterInfo_set_minX_m6_875 ();
extern "C" void CharacterInfo_get_maxX_m6_876 ();
extern "C" void CharacterInfo_set_maxX_m6_877 ();
extern "C" void CharacterInfo_get_uvBottomLeftUnFlipped_m6_878 ();
extern "C" void CharacterInfo_set_uvBottomLeftUnFlipped_m6_879 ();
extern "C" void CharacterInfo_get_uvBottomRightUnFlipped_m6_880 ();
extern "C" void CharacterInfo_set_uvBottomRightUnFlipped_m6_881 ();
extern "C" void CharacterInfo_get_uvTopRightUnFlipped_m6_882 ();
extern "C" void CharacterInfo_set_uvTopRightUnFlipped_m6_883 ();
extern "C" void CharacterInfo_get_uvTopLeftUnFlipped_m6_884 ();
extern "C" void CharacterInfo_set_uvTopLeftUnFlipped_m6_885 ();
extern "C" void CharacterInfo_get_uvBottomLeft_m6_886 ();
extern "C" void CharacterInfo_set_uvBottomLeft_m6_887 ();
extern "C" void CharacterInfo_get_uvBottomRight_m6_888 ();
extern "C" void CharacterInfo_set_uvBottomRight_m6_889 ();
extern "C" void CharacterInfo_get_uvTopRight_m6_890 ();
extern "C" void CharacterInfo_set_uvTopRight_m6_891 ();
extern "C" void CharacterInfo_get_uvTopLeft_m6_892 ();
extern "C" void CharacterInfo_set_uvTopLeft_m6_893 ();
extern "C" void FontTextureRebuildCallback__ctor_m6_894 ();
extern "C" void FontTextureRebuildCallback_Invoke_m6_895 ();
extern "C" void FontTextureRebuildCallback_BeginInvoke_m6_896 ();
extern "C" void FontTextureRebuildCallback_EndInvoke_m6_897 ();
extern "C" void Font__ctor_m6_898 ();
extern "C" void Font__ctor_m6_899 ();
extern "C" void Font__ctor_m6_900 ();
extern "C" void Font_add_textureRebuilt_m6_901 ();
extern "C" void Font_remove_textureRebuilt_m6_902 ();
extern "C" void Font_add_m_FontTextureRebuildCallback_m6_903 ();
extern "C" void Font_remove_m_FontTextureRebuildCallback_m6_904 ();
extern "C" void Font_GetOSInstalledFontNames_m6_905 ();
extern "C" void Font_Internal_CreateFont_m6_906 ();
extern "C" void Font_Internal_CreateDynamicFont_m6_907 ();
extern "C" void Font_CreateDynamicFontFromOSFont_m6_908 ();
extern "C" void Font_CreateDynamicFontFromOSFont_m6_909 ();
extern "C" void Font_get_material_m6_910 ();
extern "C" void Font_set_material_m6_911 ();
extern "C" void Font_HasCharacter_m6_912 ();
extern "C" void Font_get_fontNames_m6_913 ();
extern "C" void Font_set_fontNames_m6_914 ();
extern "C" void Font_get_characterInfo_m6_915 ();
extern "C" void Font_set_characterInfo_m6_916 ();
extern "C" void Font_RequestCharactersInTexture_m6_917 ();
extern "C" void Font_RequestCharactersInTexture_m6_918 ();
extern "C" void Font_RequestCharactersInTexture_m6_919 ();
extern "C" void Font_InvokeTextureRebuilt_Internal_m6_920 ();
extern "C" void Font_get_textureRebuildCallback_m6_921 ();
extern "C" void Font_set_textureRebuildCallback_m6_922 ();
extern "C" void Font_GetMaxVertsForString_m6_923 ();
extern "C" void Font_GetCharacterInfo_m6_924 ();
extern "C" void Font_GetCharacterInfo_m6_925 ();
extern "C" void Font_GetCharacterInfo_m6_926 ();
extern "C" void Font_get_dynamic_m6_927 ();
extern "C" void Font_get_ascent_m6_928 ();
extern "C" void Font_get_lineHeight_m6_929 ();
extern "C" void Font_get_fontSize_m6_930 ();
extern "C" void TextGenerator__ctor_m6_931 ();
extern "C" void TextGenerator__ctor_m6_932 ();
extern "C" void TextGenerator_System_IDisposable_Dispose_m6_933 ();
extern "C" void TextGenerator_Init_m6_934 ();
extern "C" void TextGenerator_Dispose_cpp_m6_935 ();
extern "C" void TextGenerator_Populate_Internal_m6_936 ();
extern "C" void TextGenerator_Populate_Internal_cpp_m6_937 ();
extern "C" void TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6_938 ();
extern "C" void TextGenerator_get_rectExtents_m6_939 ();
extern "C" void TextGenerator_INTERNAL_get_rectExtents_m6_940 ();
extern "C" void TextGenerator_get_vertexCount_m6_941 ();
extern "C" void TextGenerator_GetVerticesInternal_m6_942 ();
extern "C" void TextGenerator_GetVerticesArray_m6_943 ();
extern "C" void TextGenerator_get_characterCount_m6_944 ();
extern "C" void TextGenerator_get_characterCountVisible_m6_945 ();
extern "C" void TextGenerator_GetCharactersInternal_m6_946 ();
extern "C" void TextGenerator_GetCharactersArray_m6_947 ();
extern "C" void TextGenerator_get_lineCount_m6_948 ();
extern "C" void TextGenerator_GetLinesInternal_m6_949 ();
extern "C" void TextGenerator_GetLinesArray_m6_950 ();
extern "C" void TextGenerator_get_fontSizeUsedForBestFit_m6_951 ();
extern "C" void TextGenerator_Finalize_m6_952 ();
extern "C" void TextGenerator_ValidatedSettings_m6_953 ();
extern "C" void TextGenerator_Invalidate_m6_954 ();
extern "C" void TextGenerator_GetCharacters_m6_955 ();
extern "C" void TextGenerator_GetLines_m6_956 ();
extern "C" void TextGenerator_GetVertices_m6_957 ();
extern "C" void TextGenerator_GetPreferredWidth_m6_958 ();
extern "C" void TextGenerator_GetPreferredHeight_m6_959 ();
extern "C" void TextGenerator_Populate_m6_960 ();
extern "C" void TextGenerator_PopulateAlways_m6_961 ();
extern "C" void TextGenerator_get_verts_m6_962 ();
extern "C" void TextGenerator_get_characters_m6_963 ();
extern "C" void TextGenerator_get_lines_m6_964 ();
extern "C" void UIVertex__cctor_m6_965 ();
extern "C" void Event__ctor_m6_966 ();
extern "C" void Event__ctor_m6_967 ();
extern "C" void Event__ctor_m6_968 ();
extern "C" void Event_Finalize_m6_969 ();
extern "C" void Event_get_mousePosition_m6_970 ();
extern "C" void Event_set_mousePosition_m6_971 ();
extern "C" void Event_get_delta_m6_972 ();
extern "C" void Event_set_delta_m6_973 ();
extern "C" void Event_get_mouseRay_m6_974 ();
extern "C" void Event_set_mouseRay_m6_975 ();
extern "C" void Event_get_shift_m6_976 ();
extern "C" void Event_set_shift_m6_977 ();
extern "C" void Event_get_control_m6_978 ();
extern "C" void Event_set_control_m6_979 ();
extern "C" void Event_get_alt_m6_980 ();
extern "C" void Event_set_alt_m6_981 ();
extern "C" void Event_get_command_m6_982 ();
extern "C" void Event_set_command_m6_983 ();
extern "C" void Event_get_capsLock_m6_984 ();
extern "C" void Event_set_capsLock_m6_985 ();
extern "C" void Event_get_numeric_m6_986 ();
extern "C" void Event_set_numeric_m6_987 ();
extern "C" void Event_get_functionKey_m6_988 ();
extern "C" void Event_get_current_m6_989 ();
extern "C" void Event_set_current_m6_990 ();
extern "C" void Event_Internal_MakeMasterEventCurrent_m6_991 ();
extern "C" void Event_get_isKey_m6_992 ();
extern "C" void Event_get_isMouse_m6_993 ();
extern "C" void Event_KeyboardEvent_m6_994 ();
extern "C" void Event_GetHashCode_m6_995 ();
extern "C" void Event_Equals_m6_996 ();
extern "C" void Event_ToString_m6_997 ();
extern "C" void Event_Init_m6_998 ();
extern "C" void Event_Cleanup_m6_999 ();
extern "C" void Event_InitCopy_m6_1000 ();
extern "C" void Event_InitPtr_m6_1001 ();
extern "C" void Event_get_rawType_m6_1002 ();
extern "C" void Event_get_type_m6_1003 ();
extern "C" void Event_set_type_m6_1004 ();
extern "C" void Event_GetTypeForControl_m6_1005 ();
extern "C" void Event_Internal_SetMousePosition_m6_1006 ();
extern "C" void Event_INTERNAL_CALL_Internal_SetMousePosition_m6_1007 ();
extern "C" void Event_Internal_GetMousePosition_m6_1008 ();
extern "C" void Event_Internal_SetMouseDelta_m6_1009 ();
extern "C" void Event_INTERNAL_CALL_Internal_SetMouseDelta_m6_1010 ();
extern "C" void Event_Internal_GetMouseDelta_m6_1011 ();
extern "C" void Event_get_button_m6_1012 ();
extern "C" void Event_set_button_m6_1013 ();
extern "C" void Event_get_modifiers_m6_1014 ();
extern "C" void Event_set_modifiers_m6_1015 ();
extern "C" void Event_get_pressure_m6_1016 ();
extern "C" void Event_set_pressure_m6_1017 ();
extern "C" void Event_get_clickCount_m6_1018 ();
extern "C" void Event_set_clickCount_m6_1019 ();
extern "C" void Event_get_character_m6_1020 ();
extern "C" void Event_set_character_m6_1021 ();
extern "C" void Event_get_commandName_m6_1022 ();
extern "C" void Event_set_commandName_m6_1023 ();
extern "C" void Event_get_keyCode_m6_1024 ();
extern "C" void Event_set_keyCode_m6_1025 ();
extern "C" void Event_Internal_SetNativeEvent_m6_1026 ();
extern "C" void Event_Use_m6_1027 ();
extern "C" void Event_PopEvent_m6_1028 ();
extern "C" void Event_GetEventCount_m6_1029 ();
extern "C" void ScrollViewState__ctor_m6_1030 ();
extern "C" void WindowFunction__ctor_m6_1031 ();
extern "C" void WindowFunction_Invoke_m6_1032 ();
extern "C" void WindowFunction_BeginInvoke_m6_1033 ();
extern "C" void WindowFunction_EndInvoke_m6_1034 ();
extern "C" void GUI__cctor_m6_1035 ();
extern "C" void GUI_get_nextScrollStepTime_m6_1036 ();
extern "C" void GUI_set_nextScrollStepTime_m6_1037 ();
extern "C" void GUI_get_scrollTroughSide_m6_1038 ();
extern "C" void GUI_set_scrollTroughSide_m6_1039 ();
extern "C" void GUI_set_skin_m6_1040 ();
extern "C" void GUI_get_skin_m6_1041 ();
extern "C" void GUI_DoSetSkin_m6_1042 ();
extern "C" void GUI_set_tooltip_m6_1043 ();
extern "C" void GUI_Label_m6_1044 ();
extern "C" void GUI_Label_m6_1045 ();
extern "C" void GUI_Label_m6_1046 ();
extern "C" void GUI_Box_m6_1047 ();
extern "C" void GUI_Box_m6_1048 ();
extern "C" void GUI_Button_m6_1049 ();
extern "C" void GUI_Button_m6_1050 ();
extern "C" void GUI_DoRepeatButton_m6_1051 ();
extern "C" void GUI_PasswordFieldGetStrToShow_m6_1052 ();
extern "C" void GUI_DoTextField_m6_1053 ();
extern "C" void GUI_DoTextField_m6_1054 ();
extern "C" void GUI_DoTextField_m6_1055 ();
extern "C" void GUI_HandleTextFieldEventForTouchscreen_m6_1056 ();
extern "C" void GUI_HandleTextFieldEventForDesktop_m6_1057 ();
extern "C" void GUI_Toggle_m6_1058 ();
extern "C" void GUI_Toolbar_m6_1059 ();
extern "C" void GUI_FindStyles_m6_1060 ();
extern "C" void GUI_CalcTotalHorizSpacing_m6_1061 ();
extern "C" void GUI_DoButtonGrid_m6_1062 ();
extern "C" void GUI_CalcMouseRects_m6_1063 ();
extern "C" void GUI_GetButtonGridMouseSelection_m6_1064 ();
extern "C" void GUI_HorizontalSlider_m6_1065 ();
extern "C" void GUI_Slider_m6_1066 ();
extern "C" void GUI_HorizontalScrollbar_m6_1067 ();
extern "C" void GUI_ScrollerRepeatButton_m6_1068 ();
extern "C" void GUI_VerticalScrollbar_m6_1069 ();
extern "C" void GUI_Scroller_m6_1070 ();
extern "C" void GUI_BeginGroup_m6_1071 ();
extern "C" void GUI_EndGroup_m6_1072 ();
extern "C" void GUI_BeginScrollView_m6_1073 ();
extern "C" void GUI_EndScrollView_m6_1074 ();
extern "C" void GUI_Window_m6_1075 ();
extern "C" void GUI_CallWindowDelegate_m6_1076 ();
extern "C" void GUI_DragWindow_m6_1077 ();
extern "C" void GUI_set_color_m6_1078 ();
extern "C" void GUI_INTERNAL_set_color_m6_1079 ();
extern "C" void GUI_get_changed_m6_1080 ();
extern "C" void GUI_set_changed_m6_1081 ();
extern "C" void GUI_get_enabled_m6_1082 ();
extern "C" void GUI_Internal_SetTooltip_m6_1083 ();
extern "C" void GUI_DoLabel_m6_1084 ();
extern "C" void GUI_INTERNAL_CALL_DoLabel_m6_1085 ();
extern "C" void GUI_DoButton_m6_1086 ();
extern "C" void GUI_INTERNAL_CALL_DoButton_m6_1087 ();
extern "C" void GUI_SetNextControlName_m6_1088 ();
extern "C" void GUI_GetNameOfFocusedControl_m6_1089 ();
extern "C" void GUI_FocusControl_m6_1090 ();
extern "C" void GUI_DoToggle_m6_1091 ();
extern "C" void GUI_INTERNAL_CALL_DoToggle_m6_1092 ();
extern "C" void GUI_get_usePageScrollbars_m6_1093 ();
extern "C" void GUI_InternalRepaintEditorWindow_m6_1094 ();
extern "C" void GUI_DoWindow_m6_1095 ();
extern "C" void GUI_INTERNAL_CALL_DoWindow_m6_1096 ();
extern "C" void GUI_DragWindow_m6_1097 ();
extern "C" void GUI_INTERNAL_CALL_DragWindow_m6_1098 ();
extern "C" void GUIContent__ctor_m6_1099 ();
extern "C" void GUIContent__ctor_m6_1100 ();
extern "C" void GUIContent__ctor_m6_1101 ();
extern "C" void GUIContent__cctor_m6_1102 ();
extern "C" void GUIContent_get_text_m6_1103 ();
extern "C" void GUIContent_set_text_m6_1104 ();
extern "C" void GUIContent_get_tooltip_m6_1105 ();
extern "C" void GUIContent_Temp_m6_1106 ();
extern "C" void GUIContent_Temp_m6_1107 ();
extern "C" void GUIContent_ClearStaticCache_m6_1108 ();
extern "C" void GUIContent_Temp_m6_1109 ();
extern "C" void LayoutedWindow__ctor_m6_1110 ();
extern "C" void LayoutedWindow_DoWindow_m6_1111 ();
extern "C" void GUILayout_Label_m6_1112 ();
extern "C" void GUILayout_Label_m6_1113 ();
extern "C" void GUILayout_DoLabel_m6_1114 ();
extern "C" void GUILayout_Button_m6_1115 ();
extern "C" void GUILayout_DoButton_m6_1116 ();
extern "C" void GUILayout_TextField_m6_1117 ();
extern "C" void GUILayout_DoTextField_m6_1118 ();
extern "C" void GUILayout_Toggle_m6_1119 ();
extern "C" void GUILayout_DoToggle_m6_1120 ();
extern "C" void GUILayout_Toolbar_m6_1121 ();
extern "C" void GUILayout_Toolbar_m6_1122 ();
extern "C" void GUILayout_HorizontalSlider_m6_1123 ();
extern "C" void GUILayout_DoHorizontalSlider_m6_1124 ();
extern "C" void GUILayout_Space_m6_1125 ();
extern "C" void GUILayout_FlexibleSpace_m6_1126 ();
extern "C" void GUILayout_BeginHorizontal_m6_1127 ();
extern "C" void GUILayout_BeginHorizontal_m6_1128 ();
extern "C" void GUILayout_EndHorizontal_m6_1129 ();
extern "C" void GUILayout_BeginVertical_m6_1130 ();
extern "C" void GUILayout_BeginVertical_m6_1131 ();
extern "C" void GUILayout_EndVertical_m6_1132 ();
extern "C" void GUILayout_BeginArea_m6_1133 ();
extern "C" void GUILayout_BeginArea_m6_1134 ();
extern "C" void GUILayout_BeginArea_m6_1135 ();
extern "C" void GUILayout_EndArea_m6_1136 ();
extern "C" void GUILayout_BeginScrollView_m6_1137 ();
extern "C" void GUILayout_BeginScrollView_m6_1138 ();
extern "C" void GUILayout_EndScrollView_m6_1139 ();
extern "C" void GUILayout_EndScrollView_m6_1140 ();
extern "C" void GUILayout_Window_m6_1141 ();
extern "C" void GUILayout_DoWindow_m6_1142 ();
extern "C" void GUILayout_Width_m6_1143 ();
extern "C" void GUILayout_MinWidth_m6_1144 ();
extern "C" void GUILayout_Height_m6_1145 ();
extern "C" void GUILayout_ExpandWidth_m6_1146 ();
extern "C" void GUILayout_ExpandHeight_m6_1147 ();
extern "C" void LayoutCache__ctor_m6_1148 ();
extern "C" void GUILayoutUtility__cctor_m6_1149 ();
extern "C" void GUILayoutUtility_SelectIDList_m6_1150 ();
extern "C" void GUILayoutUtility_Begin_m6_1151 ();
extern "C" void GUILayoutUtility_BeginWindow_m6_1152 ();
extern "C" void GUILayoutUtility_EndGroup_m6_1153 ();
extern "C" void GUILayoutUtility_Layout_m6_1154 ();
extern "C" void GUILayoutUtility_LayoutFromEditorWindow_m6_1155 ();
extern "C" void GUILayoutUtility_LayoutFreeGroup_m6_1156 ();
extern "C" void GUILayoutUtility_LayoutSingleGroup_m6_1157 ();
extern "C" void GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m6_1158 ();
extern "C" void GUILayoutUtility_BeginLayoutGroup_m6_1159 ();
extern "C" void GUILayoutUtility_EndLayoutGroup_m6_1160 ();
extern "C" void GUILayoutUtility_BeginLayoutArea_m6_1161 ();
extern "C" void GUILayoutUtility_GetRect_m6_1162 ();
extern "C" void GUILayoutUtility_DoGetRect_m6_1163 ();
extern "C" void GUILayoutUtility_GetRect_m6_1164 ();
extern "C" void GUILayoutUtility_DoGetRect_m6_1165 ();
extern "C" void GUILayoutUtility_get_spaceStyle_m6_1166 ();
extern "C" void GUILayoutUtility_Internal_GetWindowRect_m6_1167 ();
extern "C" void GUILayoutUtility_Internal_MoveWindow_m6_1168 ();
extern "C" void GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6_1169 ();
extern "C" void GUILayoutEntry__ctor_m6_1170 ();
extern "C" void GUILayoutEntry__ctor_m6_1171 ();
extern "C" void GUILayoutEntry__cctor_m6_1172 ();
extern "C" void GUILayoutEntry_get_style_m6_1173 ();
extern "C" void GUILayoutEntry_set_style_m6_1174 ();
extern "C" void GUILayoutEntry_get_margin_m6_1175 ();
extern "C" void GUILayoutEntry_CalcWidth_m6_1176 ();
extern "C" void GUILayoutEntry_CalcHeight_m6_1177 ();
extern "C" void GUILayoutEntry_SetHorizontal_m6_1178 ();
extern "C" void GUILayoutEntry_SetVertical_m6_1179 ();
extern "C" void GUILayoutEntry_ApplyStyleSettings_m6_1180 ();
extern "C" void GUILayoutEntry_ApplyOptions_m6_1181 ();
extern "C" void GUILayoutEntry_ToString_m6_1182 ();
extern "C" void GUILayoutGroup__ctor_m6_1183 ();
extern "C" void GUILayoutGroup_get_margin_m6_1184 ();
extern "C" void GUILayoutGroup_ApplyOptions_m6_1185 ();
extern "C" void GUILayoutGroup_ApplyStyleSettings_m6_1186 ();
extern "C" void GUILayoutGroup_ResetCursor_m6_1187 ();
extern "C" void GUILayoutGroup_GetNext_m6_1188 ();
extern "C" void GUILayoutGroup_Add_m6_1189 ();
extern "C" void GUILayoutGroup_CalcWidth_m6_1190 ();
extern "C" void GUILayoutGroup_SetHorizontal_m6_1191 ();
extern "C" void GUILayoutGroup_CalcHeight_m6_1192 ();
extern "C" void GUILayoutGroup_SetVertical_m6_1193 ();
extern "C" void GUILayoutGroup_ToString_m6_1194 ();
extern "C" void GUIScrollGroup__ctor_m6_1195 ();
extern "C" void GUIScrollGroup_CalcWidth_m6_1196 ();
extern "C" void GUIScrollGroup_SetHorizontal_m6_1197 ();
extern "C" void GUIScrollGroup_CalcHeight_m6_1198 ();
extern "C" void GUIScrollGroup_SetVertical_m6_1199 ();
extern "C" void GUIWordWrapSizer__ctor_m6_1200 ();
extern "C" void GUIWordWrapSizer_CalcWidth_m6_1201 ();
extern "C" void GUIWordWrapSizer_CalcHeight_m6_1202 ();
extern "C" void GUILayoutOption__ctor_m6_1203 ();
extern "C" void GUISettings__ctor_m6_1204 ();
extern "C" void GUISettings_get_doubleClickSelectsWord_m6_1205 ();
extern "C" void GUISettings_get_tripleClickSelectsLine_m6_1206 ();
extern "C" void GUISettings_get_cursorColor_m6_1207 ();
extern "C" void GUISettings_get_cursorFlashSpeed_m6_1208 ();
extern "C" void GUISettings_get_selectionColor_m6_1209 ();
extern "C" void GUISettings_Internal_GetCursorFlashSpeed_m6_1210 ();
extern "C" void SkinChangedDelegate__ctor_m6_1211 ();
extern "C" void SkinChangedDelegate_Invoke_m6_1212 ();
extern "C" void SkinChangedDelegate_BeginInvoke_m6_1213 ();
extern "C" void SkinChangedDelegate_EndInvoke_m6_1214 ();
extern "C" void GUISkin__ctor_m6_1215 ();
extern "C" void GUISkin_OnEnable_m6_1216 ();
extern "C" void GUISkin_get_font_m6_1217 ();
extern "C" void GUISkin_set_font_m6_1218 ();
extern "C" void GUISkin_get_box_m6_1219 ();
extern "C" void GUISkin_set_box_m6_1220 ();
extern "C" void GUISkin_get_label_m6_1221 ();
extern "C" void GUISkin_set_label_m6_1222 ();
extern "C" void GUISkin_get_textField_m6_1223 ();
extern "C" void GUISkin_set_textField_m6_1224 ();
extern "C" void GUISkin_get_textArea_m6_1225 ();
extern "C" void GUISkin_set_textArea_m6_1226 ();
extern "C" void GUISkin_get_button_m6_1227 ();
extern "C" void GUISkin_set_button_m6_1228 ();
extern "C" void GUISkin_get_toggle_m6_1229 ();
extern "C" void GUISkin_set_toggle_m6_1230 ();
extern "C" void GUISkin_get_window_m6_1231 ();
extern "C" void GUISkin_set_window_m6_1232 ();
extern "C" void GUISkin_get_horizontalSlider_m6_1233 ();
extern "C" void GUISkin_set_horizontalSlider_m6_1234 ();
extern "C" void GUISkin_get_horizontalSliderThumb_m6_1235 ();
extern "C" void GUISkin_set_horizontalSliderThumb_m6_1236 ();
extern "C" void GUISkin_get_verticalSlider_m6_1237 ();
extern "C" void GUISkin_set_verticalSlider_m6_1238 ();
extern "C" void GUISkin_get_verticalSliderThumb_m6_1239 ();
extern "C" void GUISkin_set_verticalSliderThumb_m6_1240 ();
extern "C" void GUISkin_get_horizontalScrollbar_m6_1241 ();
extern "C" void GUISkin_set_horizontalScrollbar_m6_1242 ();
extern "C" void GUISkin_get_horizontalScrollbarThumb_m6_1243 ();
extern "C" void GUISkin_set_horizontalScrollbarThumb_m6_1244 ();
extern "C" void GUISkin_get_horizontalScrollbarLeftButton_m6_1245 ();
extern "C" void GUISkin_set_horizontalScrollbarLeftButton_m6_1246 ();
extern "C" void GUISkin_get_horizontalScrollbarRightButton_m6_1247 ();
extern "C" void GUISkin_set_horizontalScrollbarRightButton_m6_1248 ();
extern "C" void GUISkin_get_verticalScrollbar_m6_1249 ();
extern "C" void GUISkin_set_verticalScrollbar_m6_1250 ();
extern "C" void GUISkin_get_verticalScrollbarThumb_m6_1251 ();
extern "C" void GUISkin_set_verticalScrollbarThumb_m6_1252 ();
extern "C" void GUISkin_get_verticalScrollbarUpButton_m6_1253 ();
extern "C" void GUISkin_set_verticalScrollbarUpButton_m6_1254 ();
extern "C" void GUISkin_get_verticalScrollbarDownButton_m6_1255 ();
extern "C" void GUISkin_set_verticalScrollbarDownButton_m6_1256 ();
extern "C" void GUISkin_get_scrollView_m6_1257 ();
extern "C" void GUISkin_set_scrollView_m6_1258 ();
extern "C" void GUISkin_get_customStyles_m6_1259 ();
extern "C" void GUISkin_set_customStyles_m6_1260 ();
extern "C" void GUISkin_get_settings_m6_1261 ();
extern "C" void GUISkin_get_error_m6_1262 ();
extern "C" void GUISkin_Apply_m6_1263 ();
extern "C" void GUISkin_BuildStyleCache_m6_1264 ();
extern "C" void GUISkin_GetStyle_m6_1265 ();
extern "C" void GUISkin_FindStyle_m6_1266 ();
extern "C" void GUISkin_MakeCurrent_m6_1267 ();
extern "C" void GUISkin_GetEnumerator_m6_1268 ();
extern "C" void GUIStyleState__ctor_m6_1269 ();
extern "C" void GUIStyleState__ctor_m6_1270 ();
extern "C" void GUIStyleState_Finalize_m6_1271 ();
extern "C" void GUIStyleState_Init_m6_1272 ();
extern "C" void GUIStyleState_Cleanup_m6_1273 ();
extern "C" void GUIStyleState_GetBackgroundInternal_m6_1274 ();
extern "C" void GUIStyleState_set_textColor_m6_1275 ();
extern "C" void GUIStyleState_INTERNAL_set_textColor_m6_1276 ();
extern "C" void RectOffset__ctor_m6_1277 ();
extern "C" void RectOffset__ctor_m6_1278 ();
extern "C" void RectOffset__ctor_m6_1279 ();
extern "C" void RectOffset_Finalize_m6_1280 ();
extern "C" void RectOffset_ToString_m6_1281 ();
extern "C" void RectOffset_Init_m6_1282 ();
extern "C" void RectOffset_Cleanup_m6_1283 ();
extern "C" void RectOffset_get_left_m6_1284 ();
extern "C" void RectOffset_set_left_m6_1285 ();
extern "C" void RectOffset_get_right_m6_1286 ();
extern "C" void RectOffset_set_right_m6_1287 ();
extern "C" void RectOffset_get_top_m6_1288 ();
extern "C" void RectOffset_set_top_m6_1289 ();
extern "C" void RectOffset_get_bottom_m6_1290 ();
extern "C" void RectOffset_set_bottom_m6_1291 ();
extern "C" void RectOffset_get_horizontal_m6_1292 ();
extern "C" void RectOffset_get_vertical_m6_1293 ();
extern "C" void RectOffset_Add_m6_1294 ();
extern "C" void RectOffset_INTERNAL_CALL_Add_m6_1295 ();
extern "C" void RectOffset_Remove_m6_1296 ();
extern "C" void RectOffset_INTERNAL_CALL_Remove_m6_1297 ();
extern "C" void GUIStyle__ctor_m6_1298 ();
extern "C" void GUIStyle__ctor_m6_1299 ();
extern "C" void GUIStyle__cctor_m6_1300 ();
extern "C" void GUIStyle_Finalize_m6_1301 ();
extern "C" void GUIStyle_get_normal_m6_1302 ();
extern "C" void GUIStyle_get_margin_m6_1303 ();
extern "C" void GUIStyle_get_padding_m6_1304 ();
extern "C" void GUIStyle_set_padding_m6_1305 ();
extern "C" void GUIStyle_get_font_m6_1306 ();
extern "C" void GUIStyle_get_lineHeight_m6_1307 ();
extern "C" void GUIStyle_Internal_Draw_m6_1308 ();
extern "C" void GUIStyle_Draw_m6_1309 ();
extern "C" void GUIStyle_Draw_m6_1310 ();
extern "C" void GUIStyle_Draw_m6_1311 ();
extern "C" void GUIStyle_Draw_m6_1312 ();
extern "C" void GUIStyle_DrawCursor_m6_1313 ();
extern "C" void GUIStyle_DrawWithTextSelection_m6_1314 ();
extern "C" void GUIStyle_DrawWithTextSelection_m6_1315 ();
extern "C" void GUIStyle_get_none_m6_1316 ();
extern "C" void GUIStyle_GetCursorPixelPosition_m6_1317 ();
extern "C" void GUIStyle_GetCursorStringIndex_m6_1318 ();
extern "C" void GUIStyle_CalcSize_m6_1319 ();
extern "C" void GUIStyle_CalcHeight_m6_1320 ();
extern "C" void GUIStyle_get_isHeightDependantOnWidth_m6_1321 ();
extern "C" void GUIStyle_CalcMinMaxWidth_m6_1322 ();
extern "C" void GUIStyle_ToString_m6_1323 ();
extern "C" void GUIStyle_Init_m6_1324 ();
extern "C" void GUIStyle_InitCopy_m6_1325 ();
extern "C" void GUIStyle_Cleanup_m6_1326 ();
extern "C" void GUIStyle_get_name_m6_1327 ();
extern "C" void GUIStyle_set_name_m6_1328 ();
extern "C" void GUIStyle_GetStyleStatePtr_m6_1329 ();
extern "C" void GUIStyle_GetRectOffsetPtr_m6_1330 ();
extern "C" void GUIStyle_AssignRectOffset_m6_1331 ();
extern "C" void GUIStyle_get_imagePosition_m6_1332 ();
extern "C" void GUIStyle_set_alignment_m6_1333 ();
extern "C" void GUIStyle_get_wordWrap_m6_1334 ();
extern "C" void GUIStyle_set_wordWrap_m6_1335 ();
extern "C" void GUIStyle_get_contentOffset_m6_1336 ();
extern "C" void GUIStyle_set_contentOffset_m6_1337 ();
extern "C" void GUIStyle_INTERNAL_get_contentOffset_m6_1338 ();
extern "C" void GUIStyle_INTERNAL_set_contentOffset_m6_1339 ();
extern "C" void GUIStyle_set_Internal_clipOffset_m6_1340 ();
extern "C" void GUIStyle_INTERNAL_set_Internal_clipOffset_m6_1341 ();
extern "C" void GUIStyle_get_fixedWidth_m6_1342 ();
extern "C" void GUIStyle_get_fixedHeight_m6_1343 ();
extern "C" void GUIStyle_get_stretchWidth_m6_1344 ();
extern "C" void GUIStyle_set_stretchWidth_m6_1345 ();
extern "C" void GUIStyle_get_stretchHeight_m6_1346 ();
extern "C" void GUIStyle_set_stretchHeight_m6_1347 ();
extern "C" void GUIStyle_Internal_GetLineHeight_m6_1348 ();
extern "C" void GUIStyle_GetFontInternal_m6_1349 ();
extern "C" void GUIStyle_Internal_Draw_m6_1350 ();
extern "C" void GUIStyle_Internal_Draw2_m6_1351 ();
extern "C" void GUIStyle_INTERNAL_CALL_Internal_Draw2_m6_1352 ();
extern "C" void GUIStyle_Internal_GetCursorFlashOffset_m6_1353 ();
extern "C" void GUIStyle_Internal_DrawCursor_m6_1354 ();
extern "C" void GUIStyle_INTERNAL_CALL_Internal_DrawCursor_m6_1355 ();
extern "C" void GUIStyle_Internal_DrawWithTextSelection_m6_1356 ();
extern "C" void GUIStyle_SetDefaultFont_m6_1357 ();
extern "C" void GUIStyle_Internal_GetCursorPixelPosition_m6_1358 ();
extern "C" void GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6_1359 ();
extern "C" void GUIStyle_Internal_GetCursorStringIndex_m6_1360 ();
extern "C" void GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex_m6_1361 ();
extern "C" void GUIStyle_Internal_CalcSize_m6_1362 ();
extern "C" void GUIStyle_Internal_CalcHeight_m6_1363 ();
extern "C" void GUIStyle_Internal_CalcMinMaxWidth_m6_1364 ();
extern "C" void GUIUtility__cctor_m6_1365 ();
extern "C" void GUIUtility_get_pixelsPerPoint_m6_1366 ();
extern "C" void GUIUtility_GetControlID_m6_1367 ();
extern "C" void GUIUtility_GetControlID_m6_1368 ();
extern "C" void GUIUtility_GetStateObject_m6_1369 ();
extern "C" void GUIUtility_get_hotControl_m6_1370 ();
extern "C" void GUIUtility_set_hotControl_m6_1371 ();
extern "C" void GUIUtility_GetDefaultSkin_m6_1372 ();
extern "C" void GUIUtility_BeginGUI_m6_1373 ();
extern "C" void GUIUtility_EndGUI_m6_1374 ();
extern "C" void GUIUtility_EndGUIFromException_m6_1375 ();
extern "C" void GUIUtility_CheckOnGUI_m6_1376 ();
extern "C" void GUIUtility_Internal_GetPixelsPerPoint_m6_1377 ();
extern "C" void GUIUtility_GetControlID_m6_1378 ();
extern "C" void GUIUtility_Internal_GetNextControlID2_m6_1379 ();
extern "C" void GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6_1380 ();
extern "C" void GUIUtility_Internal_GetHotControl_m6_1381 ();
extern "C" void GUIUtility_Internal_SetHotControl_m6_1382 ();
extern "C" void GUIUtility_get_keyboardControl_m6_1383 ();
extern "C" void GUIUtility_set_keyboardControl_m6_1384 ();
extern "C" void GUIUtility_get_systemCopyBuffer_m6_1385 ();
extern "C" void GUIUtility_set_systemCopyBuffer_m6_1386 ();
extern "C" void GUIUtility_Internal_GetDefaultSkin_m6_1387 ();
extern "C" void GUIUtility_Internal_ExitGUI_m6_1388 ();
extern "C" void GUIUtility_Internal_GetGUIDepth_m6_1389 ();
extern "C" void GUIUtility_get_mouseUsed_m6_1390 ();
extern "C" void GUIUtility_set_mouseUsed_m6_1391 ();
extern "C" void GUIUtility_set_textFieldInput_m6_1392 ();
extern "C" void GUIClip_Push_m6_1393 ();
extern "C" void GUIClip_INTERNAL_CALL_Push_m6_1394 ();
extern "C" void GUIClip_Pop_m6_1395 ();
extern "C" void WrapperlessIcall__ctor_m6_1396 ();
extern "C" void IL2CPPStructAlignmentAttribute__ctor_m6_1397 ();
extern "C" void AttributeHelperEngine__cctor_m6_1398 ();
extern "C" void AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6_1399 ();
extern "C" void AttributeHelperEngine_GetRequiredComponents_m6_1400 ();
extern "C" void AttributeHelperEngine_CheckIsEditorScript_m6_1401 ();
extern "C" void RequireComponent__ctor_m6_1402 ();
extern "C" void AddComponentMenu__ctor_m6_1403 ();
extern "C" void ExecuteInEditMode__ctor_m6_1404 ();
extern "C" void HideInInspector__ctor_m6_1405 ();
extern "C" void SetupCoroutine__ctor_m6_1406 ();
extern "C" void SetupCoroutine_InvokeMember_m6_1407 ();
extern "C" void SetupCoroutine_InvokeStatic_m6_1408 ();
extern "C" void WritableAttribute__ctor_m6_1409 ();
extern "C" void AssemblyIsEditorAssembly__ctor_m6_1410 ();
extern "C" void GcUserProfileData_ToUserProfile_m6_1411 ();
extern "C" void GcUserProfileData_AddToArray_m6_1412 ();
extern "C" void GcAchievementDescriptionData_ToAchievementDescription_m6_1413 ();
extern "C" void GcAchievementData_ToAchievement_m6_1414 ();
extern "C" void GcScoreData_ToScore_m6_1415 ();
extern "C" void Resolution_get_width_m6_1416 ();
extern "C" void Resolution_set_width_m6_1417 ();
extern "C" void Resolution_get_height_m6_1418 ();
extern "C" void Resolution_set_height_m6_1419 ();
extern "C" void Resolution_get_refreshRate_m6_1420 ();
extern "C" void Resolution_set_refreshRate_m6_1421 ();
extern "C" void Resolution_ToString_m6_1422 ();
extern "C" void GUIStateObjects__cctor_m6_1423 ();
extern "C" void GUIStateObjects_GetStateObject_m6_1424 ();
extern "C" void LocalUser__ctor_m6_1425 ();
extern "C" void LocalUser_SetFriends_m6_1426 ();
extern "C" void LocalUser_SetAuthenticated_m6_1427 ();
extern "C" void LocalUser_SetUnderage_m6_1428 ();
extern "C" void LocalUser_get_authenticated_m6_1429 ();
extern "C" void UserProfile__ctor_m6_1430 ();
extern "C" void UserProfile__ctor_m6_1431 ();
extern "C" void UserProfile_ToString_m6_1432 ();
extern "C" void UserProfile_SetUserName_m6_1433 ();
extern "C" void UserProfile_SetUserID_m6_1434 ();
extern "C" void UserProfile_SetImage_m6_1435 ();
extern "C" void UserProfile_get_userName_m6_1436 ();
extern "C" void UserProfile_get_id_m6_1437 ();
extern "C" void UserProfile_get_isFriend_m6_1438 ();
extern "C" void UserProfile_get_state_m6_1439 ();
extern "C" void Achievement__ctor_m6_1440 ();
extern "C" void Achievement__ctor_m6_1441 ();
extern "C" void Achievement__ctor_m6_1442 ();
extern "C" void Achievement_ToString_m6_1443 ();
extern "C" void Achievement_get_id_m6_1444 ();
extern "C" void Achievement_set_id_m6_1445 ();
extern "C" void Achievement_get_percentCompleted_m6_1446 ();
extern "C" void Achievement_set_percentCompleted_m6_1447 ();
extern "C" void Achievement_get_completed_m6_1448 ();
extern "C" void Achievement_get_hidden_m6_1449 ();
extern "C" void Achievement_get_lastReportedDate_m6_1450 ();
extern "C" void AchievementDescription__ctor_m6_1451 ();
extern "C" void AchievementDescription_ToString_m6_1452 ();
extern "C" void AchievementDescription_SetImage_m6_1453 ();
extern "C" void AchievementDescription_get_id_m6_1454 ();
extern "C" void AchievementDescription_set_id_m6_1455 ();
extern "C" void AchievementDescription_get_title_m6_1456 ();
extern "C" void AchievementDescription_get_achievedDescription_m6_1457 ();
extern "C" void AchievementDescription_get_unachievedDescription_m6_1458 ();
extern "C" void AchievementDescription_get_hidden_m6_1459 ();
extern "C" void AchievementDescription_get_points_m6_1460 ();
extern "C" void Score__ctor_m6_1461 ();
extern "C" void Score__ctor_m6_1462 ();
extern "C" void Score_ToString_m6_1463 ();
extern "C" void Score_get_leaderboardID_m6_1464 ();
extern "C" void Score_set_leaderboardID_m6_1465 ();
extern "C" void Score_get_value_m6_1466 ();
extern "C" void Score_set_value_m6_1467 ();
extern "C" void Leaderboard__ctor_m6_1468 ();
extern "C" void Leaderboard_ToString_m6_1469 ();
extern "C" void Leaderboard_SetLocalUserScore_m6_1470 ();
extern "C" void Leaderboard_SetMaxRange_m6_1471 ();
extern "C" void Leaderboard_SetScores_m6_1472 ();
extern "C" void Leaderboard_SetTitle_m6_1473 ();
extern "C" void Leaderboard_GetUserFilter_m6_1474 ();
extern "C" void Leaderboard_get_id_m6_1475 ();
extern "C" void Leaderboard_set_id_m6_1476 ();
extern "C" void Leaderboard_get_userScope_m6_1477 ();
extern "C" void Leaderboard_set_userScope_m6_1478 ();
extern "C" void Leaderboard_get_range_m6_1479 ();
extern "C" void Leaderboard_set_range_m6_1480 ();
extern "C" void Leaderboard_get_timeScope_m6_1481 ();
extern "C" void Leaderboard_set_timeScope_m6_1482 ();
extern "C" void HitInfo_SendMessage_m6_1483 ();
extern "C" void HitInfo_Compare_m6_1484 ();
extern "C" void HitInfo_op_Implicit_m6_1485 ();
extern "C" void SendMouseEvents__cctor_m6_1486 ();
extern "C" void SendMouseEvents_SetMouseMoved_m6_1487 ();
extern "C" void SendMouseEvents_DoSendMouseEvents_m6_1488 ();
extern "C" void SendMouseEvents_SendEvents_m6_1489 ();
extern "C" void Range__ctor_m6_1490 ();
extern "C" void SliderState__ctor_m6_1491 ();
extern "C" void SliderHandler__ctor_m6_1492 ();
extern "C" void SliderHandler_Handle_m6_1493 ();
extern "C" void SliderHandler_OnMouseDown_m6_1494 ();
extern "C" void SliderHandler_OnMouseDrag_m6_1495 ();
extern "C" void SliderHandler_OnMouseUp_m6_1496 ();
extern "C" void SliderHandler_OnRepaint_m6_1497 ();
extern "C" void SliderHandler_CurrentEventType_m6_1498 ();
extern "C" void SliderHandler_CurrentScrollTroughSide_m6_1499 ();
extern "C" void SliderHandler_IsEmptySlider_m6_1500 ();
extern "C" void SliderHandler_SupportsPageMovements_m6_1501 ();
extern "C" void SliderHandler_PageMovementValue_m6_1502 ();
extern "C" void SliderHandler_PageUpMovementBound_m6_1503 ();
extern "C" void SliderHandler_CurrentEvent_m6_1504 ();
extern "C" void SliderHandler_ValueForCurrentMousePosition_m6_1505 ();
extern "C" void SliderHandler_Clamp_m6_1506 ();
extern "C" void SliderHandler_ThumbSelectionRect_m6_1507 ();
extern "C" void SliderHandler_StartDraggingWithValue_m6_1508 ();
extern "C" void SliderHandler_SliderState_m6_1509 ();
extern "C" void SliderHandler_ThumbRect_m6_1510 ();
extern "C" void SliderHandler_VerticalThumbRect_m6_1511 ();
extern "C" void SliderHandler_HorizontalThumbRect_m6_1512 ();
extern "C" void SliderHandler_ClampedCurrentValue_m6_1513 ();
extern "C" void SliderHandler_MousePosition_m6_1514 ();
extern "C" void SliderHandler_ValuesPerPixel_m6_1515 ();
extern "C" void SliderHandler_ThumbSize_m6_1516 ();
extern "C" void SliderHandler_MaxValue_m6_1517 ();
extern "C" void SliderHandler_MinValue_m6_1518 ();
extern "C" void StackTraceUtility__ctor_m6_1519 ();
extern "C" void StackTraceUtility__cctor_m6_1520 ();
extern "C" void StackTraceUtility_SetProjectFolder_m6_1521 ();
extern "C" void StackTraceUtility_ExtractStackTrace_m6_1522 ();
extern "C" void StackTraceUtility_IsSystemStacktraceType_m6_1523 ();
extern "C" void StackTraceUtility_ExtractStringFromException_m6_1524 ();
extern "C" void StackTraceUtility_ExtractStringFromExceptionInternal_m6_1525 ();
extern "C" void StackTraceUtility_PostprocessStacktrace_m6_1526 ();
extern "C" void StackTraceUtility_ExtractFormattedStackTrace_m6_1527 ();
extern "C" void UnityException__ctor_m6_1528 ();
extern "C" void UnityException__ctor_m6_1529 ();
extern "C" void UnityException__ctor_m6_1530 ();
extern "C" void UnityException__ctor_m6_1531 ();
extern "C" void SharedBetweenAnimatorsAttribute__ctor_m6_1532 ();
extern "C" void StateMachineBehaviour__ctor_m6_1533 ();
extern "C" void SystemClock__cctor_m6_1534 ();
extern "C" void SystemClock_get_now_m6_1535 ();
extern "C" void TextEditor__ctor_m6_1536 ();
extern "C" void TextEditor_get_position_m6_1537 ();
extern "C" void TextEditor_set_position_m6_1538 ();
extern "C" void TextEditor_get_cursorIndex_m6_1539 ();
extern "C" void TextEditor_set_cursorIndex_m6_1540 ();
extern "C" void TextEditor_get_selectIndex_m6_1541 ();
extern "C" void TextEditor_set_selectIndex_m6_1542 ();
extern "C" void TextEditor_ClearCursorPos_m6_1543 ();
extern "C" void TextEditor_OnFocus_m6_1544 ();
extern "C" void TextEditor_OnLostFocus_m6_1545 ();
extern "C" void TextEditor_GrabGraphicalCursorPos_m6_1546 ();
extern "C" void TextEditor_HandleKeyEvent_m6_1547 ();
extern "C" void TextEditor_DeleteLineBack_m6_1548 ();
extern "C" void TextEditor_DeleteWordBack_m6_1549 ();
extern "C" void TextEditor_DeleteWordForward_m6_1550 ();
extern "C" void TextEditor_Delete_m6_1551 ();
extern "C" void TextEditor_Backspace_m6_1552 ();
extern "C" void TextEditor_SelectAll_m6_1553 ();
extern "C" void TextEditor_SelectNone_m6_1554 ();
extern "C" void TextEditor_get_hasSelection_m6_1555 ();
extern "C" void TextEditor_DeleteSelection_m6_1556 ();
extern "C" void TextEditor_ReplaceSelection_m6_1557 ();
extern "C" void TextEditor_Insert_m6_1558 ();
extern "C" void TextEditor_MoveRight_m6_1559 ();
extern "C" void TextEditor_MoveLeft_m6_1560 ();
extern "C" void TextEditor_MoveUp_m6_1561 ();
extern "C" void TextEditor_MoveDown_m6_1562 ();
extern "C" void TextEditor_MoveLineStart_m6_1563 ();
extern "C" void TextEditor_MoveLineEnd_m6_1564 ();
extern "C" void TextEditor_MoveGraphicalLineStart_m6_1565 ();
extern "C" void TextEditor_MoveGraphicalLineEnd_m6_1566 ();
extern "C" void TextEditor_MoveTextStart_m6_1567 ();
extern "C" void TextEditor_MoveTextEnd_m6_1568 ();
extern "C" void TextEditor_IndexOfEndOfLine_m6_1569 ();
extern "C" void TextEditor_MoveParagraphForward_m6_1570 ();
extern "C" void TextEditor_MoveParagraphBackward_m6_1571 ();
extern "C" void TextEditor_MoveCursorToPosition_m6_1572 ();
extern "C" void TextEditor_SelectToPosition_m6_1573 ();
extern "C" void TextEditor_SelectLeft_m6_1574 ();
extern "C" void TextEditor_SelectRight_m6_1575 ();
extern "C" void TextEditor_SelectUp_m6_1576 ();
extern "C" void TextEditor_SelectDown_m6_1577 ();
extern "C" void TextEditor_SelectTextEnd_m6_1578 ();
extern "C" void TextEditor_SelectTextStart_m6_1579 ();
extern "C" void TextEditor_MouseDragSelectsWholeWords_m6_1580 ();
extern "C" void TextEditor_DblClickSnap_m6_1581 ();
extern "C" void TextEditor_GetGraphicalLineStart_m6_1582 ();
extern "C" void TextEditor_GetGraphicalLineEnd_m6_1583 ();
extern "C" void TextEditor_FindNextSeperator_m6_1584 ();
extern "C" void TextEditor_isLetterLikeChar_m6_1585 ();
extern "C" void TextEditor_FindPrevSeperator_m6_1586 ();
extern "C" void TextEditor_MoveWordRight_m6_1587 ();
extern "C" void TextEditor_MoveToStartOfNextWord_m6_1588 ();
extern "C" void TextEditor_MoveToEndOfPreviousWord_m6_1589 ();
extern "C" void TextEditor_SelectToStartOfNextWord_m6_1590 ();
extern "C" void TextEditor_SelectToEndOfPreviousWord_m6_1591 ();
extern "C" void TextEditor_ClassifyChar_m6_1592 ();
extern "C" void TextEditor_FindStartOfNextWord_m6_1593 ();
extern "C" void TextEditor_FindEndOfPreviousWord_m6_1594 ();
extern "C" void TextEditor_MoveWordLeft_m6_1595 ();
extern "C" void TextEditor_SelectWordRight_m6_1596 ();
extern "C" void TextEditor_SelectWordLeft_m6_1597 ();
extern "C" void TextEditor_ExpandSelectGraphicalLineStart_m6_1598 ();
extern "C" void TextEditor_ExpandSelectGraphicalLineEnd_m6_1599 ();
extern "C" void TextEditor_SelectGraphicalLineStart_m6_1600 ();
extern "C" void TextEditor_SelectGraphicalLineEnd_m6_1601 ();
extern "C" void TextEditor_SelectParagraphForward_m6_1602 ();
extern "C" void TextEditor_SelectParagraphBackward_m6_1603 ();
extern "C" void TextEditor_SelectCurrentWord_m6_1604 ();
extern "C" void TextEditor_FindEndOfClassification_m6_1605 ();
extern "C" void TextEditor_SelectCurrentParagraph_m6_1606 ();
extern "C" void TextEditor_UpdateScrollOffsetIfNeeded_m6_1607 ();
extern "C" void TextEditor_UpdateScrollOffset_m6_1608 ();
extern "C" void TextEditor_DrawCursor_m6_1609 ();
extern "C" void TextEditor_PerformOperation_m6_1610 ();
extern "C" void TextEditor_SaveBackup_m6_1611 ();
extern "C" void TextEditor_Cut_m6_1612 ();
extern "C" void TextEditor_Copy_m6_1613 ();
extern "C" void TextEditor_ReplaceNewlinesWithSpaces_m6_1614 ();
extern "C" void TextEditor_Paste_m6_1615 ();
extern "C" void TextEditor_MapKey_m6_1616 ();
extern "C" void TextEditor_InitKeyActions_m6_1617 ();
extern "C" void TextEditor_DetectFocusChange_m6_1618 ();
extern "C" void TextGenerationSettings_CompareColors_m6_1619 ();
extern "C" void TextGenerationSettings_CompareVector2_m6_1620 ();
extern "C" void TextGenerationSettings_Equals_m6_1621 ();
extern "C" void TrackedReference_Equals_m6_1622 ();
extern "C" void TrackedReference_GetHashCode_m6_1623 ();
extern "C" void TrackedReference_op_Equality_m6_1624 ();
extern "C" void ArgumentCache__ctor_m6_1625 ();
extern "C" void ArgumentCache_TidyAssemblyTypeName_m6_1626 ();
extern "C" void ArgumentCache_OnBeforeSerialize_m6_1627 ();
extern "C" void ArgumentCache_OnAfterDeserialize_m6_1628 ();
extern "C" void PersistentCall__ctor_m6_1629 ();
extern "C" void PersistentCallGroup__ctor_m6_1630 ();
extern "C" void InvokableCallList__ctor_m6_1631 ();
extern "C" void InvokableCallList_ClearPersistent_m6_1632 ();
extern "C" void UnityEventBase__ctor_m6_1633 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6_1634 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6_1635 ();
extern "C" void UnityEventBase_DirtyPersistentCalls_m6_1636 ();
extern "C" void UnityEventBase_ToString_m6_1637 ();
extern "C" void UnityEvent__ctor_m6_1638 ();
extern "C" void DefaultValueAttribute__ctor_m6_1639 ();
extern "C" void DefaultValueAttribute_get_Value_m6_1640 ();
extern "C" void DefaultValueAttribute_Equals_m6_1641 ();
extern "C" void DefaultValueAttribute_GetHashCode_m6_1642 ();
extern "C" void ExcludeFromDocsAttribute__ctor_m6_1643 ();
extern "C" void FormerlySerializedAsAttribute__ctor_m6_1644 ();
extern "C" void TypeInferenceRuleAttribute__ctor_m6_1645 ();
extern "C" void TypeInferenceRuleAttribute__ctor_m6_1646 ();
extern "C" void TypeInferenceRuleAttribute_ToString_m6_1647 ();
extern "C" void GenericStack__ctor_m6_1648 ();
extern "C" void UnityAdsDelegate__ctor_m6_1649 ();
extern "C" void UnityAdsDelegate_Invoke_m6_1650 ();
extern "C" void UnityAdsDelegate_BeginInvoke_m6_1651 ();
extern "C" void UnityAdsDelegate_EndInvoke_m6_1652 ();
extern "C" void ChatChannel__ctor_m7_0 ();
extern "C" void ChatChannel_get_IsPrivate_m7_1 ();
extern "C" void ChatChannel_set_IsPrivate_m7_2 ();
extern "C" void ChatChannel_get_MessageCount_m7_3 ();
extern "C" void ChatChannel_Add_m7_4 ();
extern "C" void ChatChannel_Add_m7_5 ();
extern "C" void ChatChannel_ClearMessages_m7_6 ();
extern "C" void ChatChannel_ToStringMessages_m7_7 ();
extern "C" void ChatClient__ctor_m7_8 ();
extern "C" void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_DebugReturn_m7_9 ();
extern "C" void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnEvent_m7_10 ();
extern "C" void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnOperationResponse_m7_11 ();
extern "C" void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnStatusChanged_m7_12 ();
extern "C" void ChatClient_get_NameServerAddress_m7_13 ();
extern "C" void ChatClient_set_NameServerAddress_m7_14 ();
extern "C" void ChatClient_get_FrontendAddress_m7_15 ();
extern "C" void ChatClient_set_FrontendAddress_m7_16 ();
extern "C" void ChatClient_get_ChatRegion_m7_17 ();
extern "C" void ChatClient_set_ChatRegion_m7_18 ();
extern "C" void ChatClient_get_State_m7_19 ();
extern "C" void ChatClient_set_State_m7_20 ();
extern "C" void ChatClient_get_DisconnectedCause_m7_21 ();
extern "C" void ChatClient_set_DisconnectedCause_m7_22 ();
extern "C" void ChatClient_get_CanChat_m7_23 ();
extern "C" void ChatClient_get_HasPeer_m7_24 ();
extern "C" void ChatClient_get_AppVersion_m7_25 ();
extern "C" void ChatClient_set_AppVersion_m7_26 ();
extern "C" void ChatClient_get_AppId_m7_27 ();
extern "C" void ChatClient_set_AppId_m7_28 ();
extern "C" void ChatClient_get_AuthValues_m7_29 ();
extern "C" void ChatClient_set_AuthValues_m7_30 ();
extern "C" void ChatClient_get_UserId_m7_31 ();
extern "C" void ChatClient_set_UserId_m7_32 ();
extern "C" void ChatClient_Connect_m7_33 ();
extern "C" void ChatClient_Service_m7_34 ();
extern "C" void ChatClient_Disconnect_m7_35 ();
extern "C" void ChatClient_StopThread_m7_36 ();
extern "C" void ChatClient_Subscribe_m7_37 ();
extern "C" void ChatClient_Subscribe_m7_38 ();
extern "C" void ChatClient_Unsubscribe_m7_39 ();
extern "C" void ChatClient_PublishMessage_m7_40 ();
extern "C" void ChatClient_SendPrivateMessage_m7_41 ();
extern "C" void ChatClient_SendPrivateMessage_m7_42 ();
extern "C" void ChatClient_SetOnlineStatus_m7_43 ();
extern "C" void ChatClient_SetOnlineStatus_m7_44 ();
extern "C" void ChatClient_SetOnlineStatus_m7_45 ();
extern "C" void ChatClient_AddFriends_m7_46 ();
extern "C" void ChatClient_RemoveFriends_m7_47 ();
extern "C" void ChatClient_GetPrivateChannelNameByUser_m7_48 ();
extern "C" void ChatClient_TryGetChannel_m7_49 ();
extern "C" void ChatClient_TryGetChannel_m7_50 ();
extern "C" void ChatClient_SendAcksOnly_m7_51 ();
extern "C" void ChatClient_set_DebugOut_m7_52 ();
extern "C" void ChatClient_get_DebugOut_m7_53 ();
extern "C" void ChatClient_SendChannelOperation_m7_54 ();
extern "C" void ChatClient_HandlePrivateMessageEvent_m7_55 ();
extern "C" void ChatClient_HandleChatMessagesEvent_m7_56 ();
extern "C" void ChatClient_HandleSubscribeEvent_m7_57 ();
extern "C" void ChatClient_HandleUnsubscribeEvent_m7_58 ();
extern "C" void ChatClient_HandleAuthResponse_m7_59 ();
extern "C" void ChatClient_HandleStatusUpdate_m7_60 ();
extern "C" void ChatClient_ConnectToFrontEnd_m7_61 ();
extern "C" void ChatClient_AuthenticateOnFrontEnd_m7_62 ();
extern "C" void ChatEventCode__ctor_m7_63 ();
extern "C" void ChatOperationCode__ctor_m7_64 ();
extern "C" void ChatParameterCode__ctor_m7_65 ();
extern "C" void ChatPeer__ctor_m7_66 ();
extern "C" void ChatPeer__cctor_m7_67 ();
extern "C" void ChatPeer_get_NameServerAddress_m7_68 ();
extern "C" void ChatPeer_get_IsProtocolSecure_m7_69 ();
extern "C" void ChatPeer_GetNameServerAddress_m7_70 ();
extern "C" void ChatPeer_Connect_m7_71 ();
extern "C" void ChatPeer_AuthenticateOnNameServer_m7_72 ();
extern "C" void AuthenticationValues__ctor_m7_73 ();
extern "C" void AuthenticationValues__ctor_m7_74 ();
extern "C" void AuthenticationValues_get_AuthType_m7_75 ();
extern "C" void AuthenticationValues_set_AuthType_m7_76 ();
extern "C" void AuthenticationValues_get_AuthGetParameters_m7_77 ();
extern "C" void AuthenticationValues_set_AuthGetParameters_m7_78 ();
extern "C" void AuthenticationValues_get_AuthPostData_m7_79 ();
extern "C" void AuthenticationValues_set_AuthPostData_m7_80 ();
extern "C" void AuthenticationValues_get_Token_m7_81 ();
extern "C" void AuthenticationValues_set_Token_m7_82 ();
extern "C" void AuthenticationValues_get_UserId_m7_83 ();
extern "C" void AuthenticationValues_set_UserId_m7_84 ();
extern "C" void AuthenticationValues_SetAuthPostData_m7_85 ();
extern "C" void AuthenticationValues_SetAuthPostData_m7_86 ();
extern "C" void AuthenticationValues_AddAuthParameter_m7_87 ();
extern "C" void AuthenticationValues_ToString_m7_88 ();
extern "C" void ParameterCode__ctor_m7_89 ();
extern "C" void ErrorCode__ctor_m7_90 ();
extern "C" void Demo2DJumpAndRun__ctor_m8_0 ();
extern "C" void Demo2DJumpAndRun_OnJoinedRoom_m8_1 ();
extern "C" void JumpAndRunMovement__ctor_m8_2 ();
extern "C" void JumpAndRunMovement_Awake_m8_3 ();
extern "C" void JumpAndRunMovement_Update_m8_4 ();
extern "C" void JumpAndRunMovement_FixedUpdate_m8_5 ();
extern "C" void JumpAndRunMovement_UpdateFacingDirection_m8_6 ();
extern "C" void JumpAndRunMovement_UpdateJumping_m8_7 ();
extern "C" void JumpAndRunMovement_DoJump_m8_8 ();
extern "C" void JumpAndRunMovement_UpdateMovement_m8_9 ();
extern "C" void JumpAndRunMovement_UpdateIsRunning_m8_10 ();
extern "C" void JumpAndRunMovement_UpdateIsGrounded_m8_11 ();
extern "C" void DemoBoxesGui__ctor_m8_12 ();
extern "C" void DemoBoxesGui_OnGUI_m8_13 ();
extern "C" void OnAwakePhysicsSettings__ctor_m8_14 ();
extern "C" void OnAwakePhysicsSettings_Awake_m8_15 ();
extern "C" void ClickAndDrag__ctor_m8_16 ();
extern "C" void ClickAndDrag_Update_m8_17 ();
extern "C" void DemoOwnershipGui__ctor_m8_18 ();
extern "C" void DemoOwnershipGui_OnOwnershipRequest_m8_19 ();
extern "C" void DemoOwnershipGui_OnGUI_m8_20 ();
extern "C" void InstantiateCube__ctor_m8_21 ();
extern "C" void InstantiateCube_OnClick_m8_22 ();
extern "C" void MaterialPerOwner__ctor_m8_23 ();
extern "C" void MaterialPerOwner_Start_m8_24 ();
extern "C" void MaterialPerOwner_Update_m8_25 ();
extern "C" void OnClickDisableObj__ctor_m8_26 ();
extern "C" void OnClickDisableObj_OnClick_m8_27 ();
extern "C" void OnClickRequestOwnership__ctor_m8_28 ();
extern "C" void OnClickRequestOwnership_OnClick_m8_29 ();
extern "C" void OnClickRequestOwnership_ColorRpc_m8_30 ();
extern "C" void OnClickRightDestroy__ctor_m8_31 ();
extern "C" void OnClickRightDestroy_OnPressRight_m8_32 ();
extern "C" void ChatGui__ctor_m8_33 ();
extern "C" void ChatGui__cctor_m8_34 ();
extern "C" void ChatGui_get_UserName_m8_35 ();
extern "C" void ChatGui_set_UserName_m8_36 ();
extern "C" void ChatGui_get_Instance_m8_37 ();
extern "C" void ChatGui_Awake_m8_38 ();
extern "C" void ChatGui_Start_m8_39 ();
extern "C" void ChatGui_OnApplicationQuit_m8_40 ();
extern "C" void ChatGui_OnDestroy_m8_41 ();
extern "C" void ChatGui_Update_m8_42 ();
extern "C" void ChatGui_OnGUI_m8_43 ();
extern "C" void ChatGui_GuiSendsMsg_m8_44 ();
extern "C" void ChatGui_PostHelpToCurrentChannel_m8_45 ();
extern "C" void ChatGui_OnConnected_m8_46 ();
extern "C" void ChatGui_DebugReturn_m8_47 ();
extern "C" void ChatGui_OnDisconnected_m8_48 ();
extern "C" void ChatGui_OnChatStateChange_m8_49 ();
extern "C" void ChatGui_OnSubscribed_m8_50 ();
extern "C" void ChatGui_OnUnsubscribed_m8_51 ();
extern "C" void ChatGui_OnGetMessages_m8_52 ();
extern "C" void ChatGui_OnPrivateMessage_m8_53 ();
extern "C" void ChatGui_OnStatusUpdate_m8_54 ();
extern "C" void NamePickGui__ctor_m8_55 ();
extern "C" void NamePickGui_Awake_m8_56 ();
extern "C" void NamePickGui_OnGUI_m8_57 ();
extern "C" void NamePickGui_StartChat_m8_58 ();
extern "C" void GUICustomAuth__ctor_m8_59 ();
extern "C" void GUICustomAuth_Start_m8_60 ();
extern "C" void GUICustomAuth_OnJoinedLobby_m8_61 ();
extern "C" void GUICustomAuth_OnConnectedToMaster_m8_62 ();
extern "C" void GUICustomAuth_OnCustomAuthenticationFailed_m8_63 ();
extern "C" void GUICustomAuth_SetStateAuthInput_m8_64 ();
extern "C" void GUICustomAuth_SetStateAuthHelp_m8_65 ();
extern "C" void GUICustomAuth_SetStateAuthOrNot_m8_66 ();
extern "C" void GUICustomAuth_SetStateAuthFailed_m8_67 ();
extern "C" void GUICustomAuth_ConnectWithNickname_m8_68 ();
extern "C" void GUICustomAuth_OnGUI_m8_69 ();
extern "C" void GUIFriendFinding__ctor_m8_70 ();
extern "C" void GUIFriendFinding_Start_m8_71 ();
extern "C" void GUIFriendFinding_FetchFriendsFromCommunity_m8_72 ();
extern "C" void GUIFriendFinding_OnUpdatedFriendList_m8_73 ();
extern "C" void GUIFriendFinding_OnGUI_m8_74 ();
extern "C" void GUIFriendsInRoom__ctor_m8_75 ();
extern "C" void GUIFriendsInRoom_Start_m8_76 ();
extern "C" void GUIFriendsInRoom_OnGUI_m8_77 ();
extern "C" void OnClickCallMethod__ctor_m8_78 ();
extern "C" void OnClickCallMethod_OnClick_m8_79 ();
extern "C" void HubGui__ctor_m8_80 ();
extern "C" void HubGui_Start_m8_81 ();
extern "C" void HubGui_OnGUI_m8_82 ();
extern "C" void MoveCam__ctor_m8_83 ();
extern "C" void MoveCam_Start_m8_84 ();
extern "C" void MoveCam_Update_m8_85 ();
extern "C" void ToHubButton__ctor_m8_86 ();
extern "C" void ToHubButton_get_Instance_m8_87 ();
extern "C" void ToHubButton_Awake_m8_88 ();
extern "C" void ToHubButton_Start_m8_89 ();
extern "C" void ToHubButton_OnGUI_m8_90 ();
extern "C" void DemoMecanimGUI__ctor_m8_91 ();
extern "C" void DemoMecanimGUI_Awake_m8_92 ();
extern "C" void DemoMecanimGUI_Update_m8_93 ();
extern "C" void DemoMecanimGUI_FindRemoteAnimator_m8_94 ();
extern "C" void DemoMecanimGUI_OnGUI_m8_95 ();
extern "C" void DemoMecanimGUI_OnJoinedRoom_m8_96 ();
extern "C" void DemoMecanimGUI_CreatePlayerObject_m8_97 ();
extern "C" void MessageOverlay__ctor_m8_98 ();
extern "C" void MessageOverlay_Start_m8_99 ();
extern "C" void MessageOverlay_OnJoinedRoom_m8_100 ();
extern "C" void MessageOverlay_OnLeftRoom_m8_101 ();
extern "C" void MessageOverlay_SetActive_m8_102 ();
extern "C" void OnCollideSwitchTeam__ctor_m8_103 ();
extern "C" void OnCollideSwitchTeam_OnTriggerEnter_m8_104 ();
extern "C" void OnPickedUpScript__ctor_m8_105 ();
extern "C" void OnPickedUpScript_OnPickedUp_m8_106 ();
extern "C" void PickupCamera__ctor_m8_107 ();
extern "C" void PickupCamera_OnEnable_m8_108 ();
extern "C" void PickupCamera_DebugDrawStuff_m8_109 ();
extern "C" void PickupCamera_AngleDistance_m8_110 ();
extern "C" void PickupCamera_Apply_m8_111 ();
extern "C" void PickupCamera_LateUpdate_m8_112 ();
extern "C" void PickupCamera_Cut_m8_113 ();
extern "C" void PickupCamera_SetUpRotation_m8_114 ();
extern "C" void PickupCamera_GetCenterOffset_m8_115 ();
extern "C" void PickupController__ctor_m8_116 ();
extern "C" void PickupController_Awake_m8_117 ();
extern "C" void PickupController_Update_m8_118 ();
extern "C" void PickupController_OnPhotonSerializeView_m8_119 ();
extern "C" void PickupController_UpdateSmoothedMovementDirection_m8_120 ();
extern "C" void PickupController_ApplyJumping_m8_121 ();
extern "C" void PickupController_ApplyGravity_m8_122 ();
extern "C" void PickupController_CalculateJumpVerticalSpeed_m8_123 ();
extern "C" void PickupController_DidJump_m8_124 ();
extern "C" void PickupController_OnControllerColliderHit_m8_125 ();
extern "C" void PickupController_GetSpeed_m8_126 ();
extern "C" void PickupController_IsJumping_m8_127 ();
extern "C" void PickupController_IsGrounded_m8_128 ();
extern "C" void PickupController_GetDirection_m8_129 ();
extern "C" void PickupController_IsMovingBackwards_m8_130 ();
extern "C" void PickupController_GetLockCameraTimer_m8_131 ();
extern "C" void PickupController_IsMoving_m8_132 ();
extern "C" void PickupController_HasJumpReachedApex_m8_133 ();
extern "C" void PickupController_IsGroundedWithTimeout_m8_134 ();
extern "C" void PickupController_Reset_m8_135 ();
extern "C" void PickupDemoGui__ctor_m8_136 ();
extern "C" void PickupDemoGui_OnGUI_m8_137 ();
extern "C" void PickupTriggerForward__ctor_m8_138 ();
extern "C" void PickupTriggerForward_OnTriggerEnter_m8_139 ();
extern "C" void DemoRPGMovement__ctor_m8_140 ();
extern "C" void DemoRPGMovement_OnJoinedRoom_m8_141 ();
extern "C" void DemoRPGMovement_CreatePlayerObject_m8_142 ();
extern "C" void RPGCamera__ctor_m8_143 ();
extern "C" void RPGCamera_Start_m8_144 ();
extern "C" void RPGCamera_LateUpdate_m8_145 ();
extern "C" void RPGCamera_UpdateDistance_m8_146 ();
extern "C" void RPGCamera_UpdateZoom_m8_147 ();
extern "C" void RPGCamera_UpdatePosition_m8_148 ();
extern "C" void RPGCamera_UpdateRotation_m8_149 ();
extern "C" void RPGMovement__ctor_m8_150 ();
extern "C" void RPGMovement_Start_m8_151 ();
extern "C" void RPGMovement_Update_m8_152 ();
extern "C" void RPGMovement_UpdateAnimation_m8_153 ();
extern "C" void RPGMovement_ResetSpeedValues_m8_154 ();
extern "C" void RPGMovement_ApplySynchronizedValues_m8_155 ();
extern "C" void RPGMovement_ApplyGravityToCharacterController_m8_156 ();
extern "C" void RPGMovement_MoveCharacterController_m8_157 ();
extern "C" void RPGMovement_UpdateForwardMovement_m8_158 ();
extern "C" void RPGMovement_UpdateBackwardMovement_m8_159 ();
extern "C" void RPGMovement_UpdateStrafeMovement_m8_160 ();
extern "C" void RPGMovement_UpdateRotateMovement_m8_161 ();
extern "C" void CubeExtra__ctor_m8_162 ();
extern "C" void CubeExtra_Awake_m8_163 ();
extern "C" void CubeExtra_OnPhotonSerializeView_m8_164 ();
extern "C" void CubeExtra_Update_m8_165 ();
extern "C" void CubeInter__ctor_m8_166 ();
extern "C" void CubeInter_Awake_m8_167 ();
extern "C" void CubeInter_OnPhotonSerializeView_m8_168 ();
extern "C" void CubeInter_Update_m8_169 ();
extern "C" void CubeLerp__ctor_m8_170 ();
extern "C" void CubeLerp_Awake_m8_171 ();
extern "C" void CubeLerp_OnPhotonSerializeView_m8_172 ();
extern "C" void CubeLerp_Update_m8_173 ();
extern "C" void IELdemo__ctor_m8_174 ();
extern "C" void IELdemo_Awake_m8_175 ();
extern "C" void IELdemo_OnConnectedToMaster_m8_176 ();
extern "C" void IELdemo_OnPhotonRandomJoinFailed_m8_177 ();
extern "C" void IELdemo_OnJoinedRoom_m8_178 ();
extern "C" void IELdemo_OnCreatedRoom_m8_179 ();
extern "C" void IELdemo_Update_m8_180 ();
extern "C" void IELdemo_OnGUI_m8_181 ();
extern "C" void ThirdPersonCamera__ctor_m8_182 ();
extern "C" void ThirdPersonCamera_OnEnable_m8_183 ();
extern "C" void ThirdPersonCamera_DebugDrawStuff_m8_184 ();
extern "C" void ThirdPersonCamera_AngleDistance_m8_185 ();
extern "C" void ThirdPersonCamera_Apply_m8_186 ();
extern "C" void ThirdPersonCamera_LateUpdate_m8_187 ();
extern "C" void ThirdPersonCamera_Cut_m8_188 ();
extern "C" void ThirdPersonCamera_SetUpRotation_m8_189 ();
extern "C" void ThirdPersonCamera_GetCenterOffset_m8_190 ();
extern "C" void ThirdPersonController__ctor_m8_191 ();
extern "C" void ThirdPersonController_Awake_m8_192 ();
extern "C" void ThirdPersonController_UpdateSmoothedMovementDirection_m8_193 ();
extern "C" void ThirdPersonController_ApplyJumping_m8_194 ();
extern "C" void ThirdPersonController_ApplyGravity_m8_195 ();
extern "C" void ThirdPersonController_CalculateJumpVerticalSpeed_m8_196 ();
extern "C" void ThirdPersonController_DidJump_m8_197 ();
extern "C" void ThirdPersonController_Update_m8_198 ();
extern "C" void ThirdPersonController_OnControllerColliderHit_m8_199 ();
extern "C" void ThirdPersonController_GetSpeed_m8_200 ();
extern "C" void ThirdPersonController_IsJumping_m8_201 ();
extern "C" void ThirdPersonController_IsGrounded_m8_202 ();
extern "C" void ThirdPersonController_GetDirection_m8_203 ();
extern "C" void ThirdPersonController_IsMovingBackwards_m8_204 ();
extern "C" void ThirdPersonController_GetLockCameraTimer_m8_205 ();
extern "C" void ThirdPersonController_IsMoving_m8_206 ();
extern "C" void ThirdPersonController_HasJumpReachedApex_m8_207 ();
extern "C" void ThirdPersonController_IsGroundedWithTimeout_m8_208 ();
extern "C" void ThirdPersonController_Reset_m8_209 ();
extern "C" void ThirdPersonNetwork__ctor_m8_210 ();
extern "C" void ThirdPersonNetwork_Awake_m8_211 ();
extern "C" void ThirdPersonNetwork_OnPhotonSerializeView_m8_212 ();
extern "C" void ThirdPersonNetwork_Update_m8_213 ();
extern "C" void WorkerInGame__ctor_m8_214 ();
extern "C" void WorkerInGame_Awake_m8_215 ();
extern "C" void WorkerInGame_OnGUI_m8_216 ();
extern "C" void WorkerInGame_OnMasterClientSwitched_m8_217 ();
extern "C" void WorkerInGame_OnLeftRoom_m8_218 ();
extern "C" void WorkerInGame_OnDisconnectedFromPhoton_m8_219 ();
extern "C" void WorkerInGame_OnPhotonInstantiate_m8_220 ();
extern "C" void WorkerInGame_OnPhotonPlayerConnected_m8_221 ();
extern "C" void WorkerInGame_OnPhotonPlayerDisconnected_m8_222 ();
extern "C" void WorkerInGame_OnFailedToConnectToPhoton_m8_223 ();
extern "C" void WorkerMenu__ctor_m8_224 ();
extern "C" void WorkerMenu__cctor_m8_225 ();
extern "C" void WorkerMenu_get_ErrorDialog_m8_226 ();
extern "C" void WorkerMenu_set_ErrorDialog_m8_227 ();
extern "C" void WorkerMenu_Awake_m8_228 ();
extern "C" void WorkerMenu_OnGUI_m8_229 ();
extern "C" void WorkerMenu_OnJoinedRoom_m8_230 ();
extern "C" void WorkerMenu_OnPhotonCreateRoomFailed_m8_231 ();
extern "C" void WorkerMenu_OnPhotonJoinRoomFailed_m8_232 ();
extern "C" void WorkerMenu_OnPhotonRandomJoinFailed_m8_233 ();
extern "C" void WorkerMenu_OnCreatedRoom_m8_234 ();
extern "C" void WorkerMenu_OnDisconnectedFromPhoton_m8_235 ();
extern "C" void WorkerMenu_OnFailedToConnectToPhoton_m8_236 ();
extern "C" void AudioRpc__ctor_m8_237 ();
extern "C" void AudioRpc_Awake_m8_238 ();
extern "C" void AudioRpc_Marco_m8_239 ();
extern "C" void AudioRpc_Polo_m8_240 ();
extern "C" void AudioRpc_OnApplicationFocus_m8_241 ();
extern "C" void ClickDetector__ctor_m8_242 ();
extern "C" void ClickDetector_Update_m8_243 ();
extern "C" void ClickDetector_RaycastObject_m8_244 ();
extern "C" void GameLogic__ctor_m8_245 ();
extern "C" void GameLogic__cctor_m8_246 ();
extern "C" void GameLogic_Start_m8_247 ();
extern "C" void GameLogic_OnJoinedRoom_m8_248 ();
extern "C" void GameLogic_OnPhotonPlayerConnected_m8_249 ();
extern "C" void GameLogic_TagPlayer_m8_250 ();
extern "C" void GameLogic_TaggedPlayer_m8_251 ();
extern "C" void GameLogic_OnPhotonPlayerDisconnected_m8_252 ();
extern "C" void GameLogic_OnMasterClientSwitched_m8_253 ();
extern "C" void myThirdPersonController__ctor_m8_254 ();
extern "C" void NetworkCharacter__ctor_m8_255 ();
extern "C" void NetworkCharacter_Update_m8_256 ();
extern "C" void NetworkCharacter_OnPhotonSerializeView_m8_257 ();
extern "C" void RandomMatchmaker__ctor_m8_258 ();
extern "C" void RandomMatchmaker_Start_m8_259 ();
extern "C" void RandomMatchmaker_OnJoinedLobby_m8_260 ();
extern "C" void RandomMatchmaker_OnConnectedToMaster_m8_261 ();
extern "C" void RandomMatchmaker_OnPhotonRandomJoinFailed_m8_262 ();
extern "C" void RandomMatchmaker_OnJoinedRoom_m8_263 ();
extern "C" void RandomMatchmaker_OnGUI_m8_264 ();
extern "C" void IdleRunJump__ctor_m8_265 ();
extern "C" void IdleRunJump_Start_m8_266 ();
extern "C" void IdleRunJump_Update_m8_267 ();
extern "C" void PlayerDiamond__ctor_m8_268 ();
extern "C" void PlayerDiamond_get_PhotonView_m8_269 ();
extern "C" void PlayerDiamond_get_DiamondRenderer_m8_270 ();
extern "C" void PlayerDiamond_Start_m8_271 ();
extern "C" void PlayerDiamond_Update_m8_272 ();
extern "C" void PlayerDiamond_UpdateDiamondPosition_m8_273 ();
extern "C" void PlayerDiamond_UpdateDiamondRotation_m8_274 ();
extern "C" void PlayerDiamond_UpdateDiamondVisibility_m8_275 ();
extern "C" void PlayerVariables__ctor_m8_276 ();
extern "C" void PlayerVariables__cctor_m8_277 ();
extern "C" void PlayerVariables_GetColor_m8_278 ();
extern "C" void PlayerVariables_GetColorName_m8_279 ();
extern "C" void PlayerVariables_GetMaterial_m8_280 ();
extern "C" void CustomTypes__cctor_m8_281 ();
extern "C" void CustomTypes_Register_m8_282 ();
extern "C" void CustomTypes_SerializeVector3_m8_283 ();
extern "C" void CustomTypes_DeserializeVector3_m8_284 ();
extern "C" void CustomTypes_SerializeVector2_m8_285 ();
extern "C" void CustomTypes_DeserializeVector2_m8_286 ();
extern "C" void CustomTypes_SerializeQuaternion_m8_287 ();
extern "C" void CustomTypes_DeserializeQuaternion_m8_288 ();
extern "C" void CustomTypes_SerializePhotonPlayer_m8_289 ();
extern "C" void CustomTypes_DeserializePhotonPlayer_m8_290 ();
extern "C" void Extensions_GetPhotonViewsInChildren_m8_291 ();
extern "C" void Extensions_GetPhotonView_m8_292 ();
extern "C" void Extensions_AlmostEquals_m8_293 ();
extern "C" void Extensions_AlmostEquals_m8_294 ();
extern "C" void Extensions_AlmostEquals_m8_295 ();
extern "C" void Extensions_AlmostEquals_m8_296 ();
extern "C" void Extensions_Merge_m8_297 ();
extern "C" void Extensions_MergeStringKeys_m8_298 ();
extern "C" void Extensions_ToStringFull_m8_299 ();
extern "C" void Extensions_StripToStringKeys_m8_300 ();
extern "C" void Extensions_StripKeysWithNullValues_m8_301 ();
extern "C" void Extensions_Contains_m8_302 ();
extern "C" void GameObjectExtensions_GetActive_m8_303 ();
extern "C" void FriendInfo__ctor_m8_304 ();
extern "C" void FriendInfo_get_Name_m8_305 ();
extern "C" void FriendInfo_set_Name_m8_306 ();
extern "C" void FriendInfo_get_IsOnline_m8_307 ();
extern "C" void FriendInfo_set_IsOnline_m8_308 ();
extern "C" void FriendInfo_get_Room_m8_309 ();
extern "C" void FriendInfo_set_Room_m8_310 ();
extern "C" void FriendInfo_get_IsInRoom_m8_311 ();
extern "C" void FriendInfo_ToString_m8_312 ();
extern "C" void GizmoTypeDrawer__ctor_m8_313 ();
extern "C" void GizmoTypeDrawer_Draw_m8_314 ();
extern "C" void EnterRoomParams__ctor_m8_315 ();
extern "C" void OpJoinRandomRoomParams__ctor_m8_316 ();
extern "C" void LoadbalancingPeer__ctor_m8_317 ();
extern "C" void LoadbalancingPeer_get_IsProtocolSecure_m8_318 ();
extern "C" void LoadbalancingPeer_OpGetRegions_m8_319 ();
extern "C" void LoadbalancingPeer_OpJoinLobby_m8_320 ();
extern "C" void LoadbalancingPeer_OpLeaveLobby_m8_321 ();
extern "C" void LoadbalancingPeer_RoomOptionsToOpParameters_m8_322 ();
extern "C" void LoadbalancingPeer_OpCreateRoom_m8_323 ();
extern "C" void LoadbalancingPeer_OpJoinRoom_m8_324 ();
extern "C" void LoadbalancingPeer_OpJoinRandomRoom_m8_325 ();
extern "C" void LoadbalancingPeer_OpLeaveRoom_m8_326 ();
extern "C" void LoadbalancingPeer_OpFindFriends_m8_327 ();
extern "C" void LoadbalancingPeer_OpSetCustomPropertiesOfActor_m8_328 ();
extern "C" void LoadbalancingPeer_OpSetPropertiesOfActor_m8_329 ();
extern "C" void LoadbalancingPeer_OpSetPropertyOfRoom_m8_330 ();
extern "C" void LoadbalancingPeer_OpSetCustomPropertiesOfRoom_m8_331 ();
extern "C" void LoadbalancingPeer_OpSetPropertiesOfRoom_m8_332 ();
extern "C" void LoadbalancingPeer_OpAuthenticate_m8_333 ();
extern "C" void LoadbalancingPeer_OpChangeGroups_m8_334 ();
extern "C" void LoadbalancingPeer_OpRaiseEvent_m8_335 ();
extern "C" void ErrorCode__ctor_m8_336 ();
extern "C" void ActorProperties__ctor_m8_337 ();
extern "C" void GamePropertyKey__ctor_m8_338 ();
extern "C" void EventCode__ctor_m8_339 ();
extern "C" void ParameterCode__ctor_m8_340 ();
extern "C" void OperationCode__ctor_m8_341 ();
extern "C" void RaiseEventOptions__ctor_m8_342 ();
extern "C" void RaiseEventOptions__cctor_m8_343 ();
extern "C" void TypedLobby__ctor_m8_344 ();
extern "C" void TypedLobby__ctor_m8_345 ();
extern "C" void TypedLobby__cctor_m8_346 ();
extern "C" void TypedLobby_get_IsDefault_m8_347 ();
extern "C" void TypedLobby_ToString_m8_348 ();
extern "C" void TypedLobbyInfo__ctor_m8_349 ();
extern "C" void TypedLobbyInfo_ToString_m8_350 ();
extern "C" void AuthenticationValues__ctor_m8_351 ();
extern "C" void AuthenticationValues__ctor_m8_352 ();
extern "C" void AuthenticationValues_get_AuthType_m8_353 ();
extern "C" void AuthenticationValues_set_AuthType_m8_354 ();
extern "C" void AuthenticationValues_get_AuthGetParameters_m8_355 ();
extern "C" void AuthenticationValues_set_AuthGetParameters_m8_356 ();
extern "C" void AuthenticationValues_get_AuthPostData_m8_357 ();
extern "C" void AuthenticationValues_set_AuthPostData_m8_358 ();
extern "C" void AuthenticationValues_get_Token_m8_359 ();
extern "C" void AuthenticationValues_set_Token_m8_360 ();
extern "C" void AuthenticationValues_get_UserId_m8_361 ();
extern "C" void AuthenticationValues_set_UserId_m8_362 ();
extern "C" void AuthenticationValues_SetAuthPostData_m8_363 ();
extern "C" void AuthenticationValues_SetAuthPostData_m8_364 ();
extern "C" void AuthenticationValues_AddAuthParameter_m8_365 ();
extern "C" void AuthenticationValues_ToString_m8_366 ();
extern "C" void NetworkingPeer__ctor_m8_367 ();
extern "C" void NetworkingPeer__cctor_m8_368 ();
extern "C" void NetworkingPeer_get_mAppVersionPun_m8_369 ();
extern "C" void NetworkingPeer_get_CustomAuthenticationValues_m8_370 ();
extern "C" void NetworkingPeer_set_CustomAuthenticationValues_m8_371 ();
extern "C" void NetworkingPeer_get_NameServerAddress_m8_372 ();
extern "C" void NetworkingPeer_get_MasterServerAddress_m8_373 ();
extern "C" void NetworkingPeer_set_MasterServerAddress_m8_374 ();
extern "C" void NetworkingPeer_get_mGameserver_m8_375 ();
extern "C" void NetworkingPeer_set_mGameserver_m8_376 ();
extern "C" void NetworkingPeer_get_server_m8_377 ();
extern "C" void NetworkingPeer_set_server_m8_378 ();
extern "C" void NetworkingPeer_get_State_m8_379 ();
extern "C" void NetworkingPeer_set_State_m8_380 ();
extern "C" void NetworkingPeer_get_IsUsingNameServer_m8_381 ();
extern "C" void NetworkingPeer_set_IsUsingNameServer_m8_382 ();
extern "C" void NetworkingPeer_get_IsAuthorizeSecretAvailable_m8_383 ();
extern "C" void NetworkingPeer_get_AvailableRegions_m8_384 ();
extern "C" void NetworkingPeer_set_AvailableRegions_m8_385 ();
extern "C" void NetworkingPeer_get_CloudRegion_m8_386 ();
extern "C" void NetworkingPeer_set_CloudRegion_m8_387 ();
extern "C" void NetworkingPeer_get_requestLobbyStatistics_m8_388 ();
extern "C" void NetworkingPeer_get_lobby_m8_389 ();
extern "C" void NetworkingPeer_set_lobby_m8_390 ();
extern "C" void NetworkingPeer_get_mPlayersOnMasterCount_m8_391 ();
extern "C" void NetworkingPeer_set_mPlayersOnMasterCount_m8_392 ();
extern "C" void NetworkingPeer_get_mGameCount_m8_393 ();
extern "C" void NetworkingPeer_set_mGameCount_m8_394 ();
extern "C" void NetworkingPeer_get_mPlayersInRoomsCount_m8_395 ();
extern "C" void NetworkingPeer_set_mPlayersInRoomsCount_m8_396 ();
extern "C" void NetworkingPeer_get_FriendsListAge_m8_397 ();
extern "C" void NetworkingPeer_get_PlayerName_m8_398 ();
extern "C" void NetworkingPeer_set_PlayerName_m8_399 ();
extern "C" void NetworkingPeer_get_CurrentGame_m8_400 ();
extern "C" void NetworkingPeer_set_CurrentGame_m8_401 ();
extern "C" void NetworkingPeer_get_mLocalActor_m8_402 ();
extern "C" void NetworkingPeer_set_mLocalActor_m8_403 ();
extern "C" void NetworkingPeer_get_mMasterClientId_m8_404 ();
extern "C" void NetworkingPeer_set_mMasterClientId_m8_405 ();
extern "C" void NetworkingPeer_GetNameServerAddress_m8_406 ();
extern "C" void NetworkingPeer_Connect_m8_407 ();
extern "C" void NetworkingPeer_Connect_m8_408 ();
extern "C" void NetworkingPeer_ConnectToNameServer_m8_409 ();
extern "C" void NetworkingPeer_ConnectToRegionMaster_m8_410 ();
extern "C" void NetworkingPeer_GetRegions_m8_411 ();
extern "C" void NetworkingPeer_Disconnect_m8_412 ();
extern "C" void NetworkingPeer_DisconnectToReconnect_m8_413 ();
extern "C" void NetworkingPeer_LeftLobbyCleanup_m8_414 ();
extern "C" void NetworkingPeer_LeftRoomCleanup_m8_415 ();
extern "C" void NetworkingPeer_LocalCleanupAnythingInstantiated_m8_416 ();
extern "C" void NetworkingPeer_ReadoutProperties_m8_417 ();
extern "C" void NetworkingPeer_AddNewPlayer_m8_418 ();
extern "C" void NetworkingPeer_RemovePlayer_m8_419 ();
extern "C" void NetworkingPeer_RebuildPlayerListCopies_m8_420 ();
extern "C" void NetworkingPeer_ResetPhotonViewsOnSerialize_m8_421 ();
extern "C" void NetworkingPeer_HandleEventLeave_m8_422 ();
extern "C" void NetworkingPeer_CheckMasterClient_m8_423 ();
extern "C" void NetworkingPeer_UpdateMasterClient_m8_424 ();
extern "C" void NetworkingPeer_ReturnLowestPlayerId_m8_425 ();
extern "C" void NetworkingPeer_SetMasterClient_m8_426 ();
extern "C" void NetworkingPeer_SetMasterClient_m8_427 ();
extern "C" void NetworkingPeer_GetActorPropertiesForActorNr_m8_428 ();
extern "C" void NetworkingPeer_GetPlayerWithId_m8_429 ();
extern "C" void NetworkingPeer_SendPlayerName_m8_430 ();
extern "C" void NetworkingPeer_GameEnteredOnGameServer_m8_431 ();
extern "C" void NetworkingPeer_GetLocalActorProperties_m8_432 ();
extern "C" void NetworkingPeer_ChangeLocalID_m8_433 ();
extern "C" void NetworkingPeer_OpCreateGame_m8_434 ();
extern "C" void NetworkingPeer_OpJoinRoom_m8_435 ();
extern "C" void NetworkingPeer_OpJoinRandomRoom_m8_436 ();
extern "C" void NetworkingPeer_OpLeave_m8_437 ();
extern "C" void NetworkingPeer_OpRaiseEvent_m8_438 ();
extern "C" void NetworkingPeer_DebugReturn_m8_439 ();
extern "C" void NetworkingPeer_OnOperationResponse_m8_440 ();
extern "C" void NetworkingPeer_OpFindFriends_m8_441 ();
extern "C" void NetworkingPeer_OnStatusChanged_m8_442 ();
extern "C" void NetworkingPeer_OnEvent_m8_443 ();
extern "C" void NetworkingPeer_UpdatedActorList_m8_444 ();
extern "C" void NetworkingPeer_SendVacantViewIds_m8_445 ();
extern "C" void NetworkingPeer_SendMonoMessage_m8_446 ();
extern "C" void NetworkingPeer_ExecuteRpc_m8_447 ();
extern "C" void NetworkingPeer_CheckTypeMatch_m8_448 ();
extern "C" void NetworkingPeer_SendInstantiate_m8_449 ();
extern "C" void NetworkingPeer_DoInstantiate_m8_450 ();
extern "C" void NetworkingPeer_StoreInstantiationData_m8_451 ();
extern "C" void NetworkingPeer_FetchInstantiationData_m8_452 ();
extern "C" void NetworkingPeer_RemoveInstantiationData_m8_453 ();
extern "C" void NetworkingPeer_DestroyPlayerObjects_m8_454 ();
extern "C" void NetworkingPeer_DestroyAll_m8_455 ();
extern "C" void NetworkingPeer_RemoveInstantiatedGO_m8_456 ();
extern "C" void NetworkingPeer_GetInstantiatedObjectsId_m8_457 ();
extern "C" void NetworkingPeer_ServerCleanInstantiateAndDestroy_m8_458 ();
extern "C" void NetworkingPeer_SendDestroyOfPlayer_m8_459 ();
extern "C" void NetworkingPeer_SendDestroyOfAll_m8_460 ();
extern "C" void NetworkingPeer_OpRemoveFromServerInstantiationsOfPlayer_m8_461 ();
extern "C" void NetworkingPeer_RequestOwnership_m8_462 ();
extern "C" void NetworkingPeer_TransferOwnership_m8_463 ();
extern "C" void NetworkingPeer_LocalCleanPhotonView_m8_464 ();
extern "C" void NetworkingPeer_GetPhotonView_m8_465 ();
extern "C" void NetworkingPeer_RegisterPhotonView_m8_466 ();
extern "C" void NetworkingPeer_OpCleanRpcBuffer_m8_467 ();
extern "C" void NetworkingPeer_OpRemoveCompleteCacheOfPlayer_m8_468 ();
extern "C" void NetworkingPeer_OpRemoveCompleteCache_m8_469 ();
extern "C" void NetworkingPeer_RemoveCacheOfLeftPlayers_m8_470 ();
extern "C" void NetworkingPeer_CleanRpcBufferIfMine_m8_471 ();
extern "C" void NetworkingPeer_OpCleanRpcBuffer_m8_472 ();
extern "C" void NetworkingPeer_RemoveRPCsInGroup_m8_473 ();
extern "C" void NetworkingPeer_SetLevelPrefix_m8_474 ();
extern "C" void NetworkingPeer_RPC_m8_475 ();
extern "C" void NetworkingPeer_RPC_m8_476 ();
extern "C" void NetworkingPeer_SetReceivingEnabled_m8_477 ();
extern "C" void NetworkingPeer_SetReceivingEnabled_m8_478 ();
extern "C" void NetworkingPeer_SetSendingEnabled_m8_479 ();
extern "C" void NetworkingPeer_SetSendingEnabled_m8_480 ();
extern "C" void NetworkingPeer_NewSceneLoaded_m8_481 ();
extern "C" void NetworkingPeer_RunViewUpdate_m8_482 ();
extern "C" void NetworkingPeer_OnSerializeWrite_m8_483 ();
extern "C" void NetworkingPeer_OnSerializeRead_m8_484 ();
extern "C" void NetworkingPeer_AlmostEquals_m8_485 ();
extern "C" void NetworkingPeer_DeltaCompressionWrite_m8_486 ();
extern "C" void NetworkingPeer_DeltaCompressionRead_m8_487 ();
extern "C" void NetworkingPeer_ObjectIsSameWithInprecision_m8_488 ();
extern "C" void NetworkingPeer_GetMethod_m8_489 ();
extern "C" void NetworkingPeer_LoadLevelIfSynced_m8_490 ();
extern "C" void NetworkingPeer_SetLevelInPropsIfSynced_m8_491 ();
extern "C" void NetworkingPeer_SetApp_m8_492 ();
extern "C" void NetworkingPeer_WebRpc_m8_493 ();
extern "C" void MonoBehaviour__ctor_m8_494 ();
extern "C" void MonoBehaviour_get_photonView_m8_495 ();
extern "C" void MonoBehaviour_get_networkView_m8_496 ();
extern "C" void PunBehaviour__ctor_m8_497 ();
extern "C" void PunBehaviour_OnConnectedToPhoton_m8_498 ();
extern "C" void PunBehaviour_OnLeftRoom_m8_499 ();
extern "C" void PunBehaviour_OnMasterClientSwitched_m8_500 ();
extern "C" void PunBehaviour_OnPhotonCreateRoomFailed_m8_501 ();
extern "C" void PunBehaviour_OnPhotonJoinRoomFailed_m8_502 ();
extern "C" void PunBehaviour_OnCreatedRoom_m8_503 ();
extern "C" void PunBehaviour_OnJoinedLobby_m8_504 ();
extern "C" void PunBehaviour_OnLeftLobby_m8_505 ();
extern "C" void PunBehaviour_OnFailedToConnectToPhoton_m8_506 ();
extern "C" void PunBehaviour_OnDisconnectedFromPhoton_m8_507 ();
extern "C" void PunBehaviour_OnConnectionFail_m8_508 ();
extern "C" void PunBehaviour_OnPhotonInstantiate_m8_509 ();
extern "C" void PunBehaviour_OnReceivedRoomListUpdate_m8_510 ();
extern "C" void PunBehaviour_OnJoinedRoom_m8_511 ();
extern "C" void PunBehaviour_OnPhotonPlayerConnected_m8_512 ();
extern "C" void PunBehaviour_OnPhotonPlayerDisconnected_m8_513 ();
extern "C" void PunBehaviour_OnPhotonRandomJoinFailed_m8_514 ();
extern "C" void PunBehaviour_OnConnectedToMaster_m8_515 ();
extern "C" void PunBehaviour_OnPhotonMaxCccuReached_m8_516 ();
extern "C" void PunBehaviour_OnPhotonCustomRoomPropertiesChanged_m8_517 ();
extern "C" void PunBehaviour_OnPhotonPlayerPropertiesChanged_m8_518 ();
extern "C" void PunBehaviour_OnUpdatedFriendList_m8_519 ();
extern "C" void PunBehaviour_OnCustomAuthenticationFailed_m8_520 ();
extern "C" void PunBehaviour_OnWebRpcResponse_m8_521 ();
extern "C" void PunBehaviour_OnOwnershipRequest_m8_522 ();
extern "C" void PunBehaviour_OnLobbyStatisticsUpdate_m8_523 ();
extern "C" void PhotonMessageInfo__ctor_m8_524 ();
extern "C" void PhotonMessageInfo__ctor_m8_525 ();
extern "C" void PhotonMessageInfo_get_timestamp_m8_526 ();
extern "C" void PhotonMessageInfo_ToString_m8_527 ();
extern "C" void RoomOptions__ctor_m8_528 ();
extern "C" void RoomOptions_get_isVisible_m8_529 ();
extern "C" void RoomOptions_set_isVisible_m8_530 ();
extern "C" void RoomOptions_get_isOpen_m8_531 ();
extern "C" void RoomOptions_set_isOpen_m8_532 ();
extern "C" void RoomOptions_get_cleanupCacheOnLeave_m8_533 ();
extern "C" void RoomOptions_set_cleanupCacheOnLeave_m8_534 ();
extern "C" void RoomOptions_get_suppressRoomEvents_m8_535 ();
extern "C" void PunEvent__ctor_m8_536 ();
extern "C" void PhotonStream__ctor_m8_537 ();
extern "C" void PhotonStream_get_isWriting_m8_538 ();
extern "C" void PhotonStream_get_isReading_m8_539 ();
extern "C" void PhotonStream_get_Count_m8_540 ();
extern "C" void PhotonStream_ReceiveNext_m8_541 ();
extern "C" void PhotonStream_PeekNext_m8_542 ();
extern "C" void PhotonStream_SendNext_m8_543 ();
extern "C" void PhotonStream_ToArray_m8_544 ();
extern "C" void PhotonStream_Serialize_m8_545 ();
extern "C" void PhotonStream_Serialize_m8_546 ();
extern "C" void PhotonStream_Serialize_m8_547 ();
extern "C" void PhotonStream_Serialize_m8_548 ();
extern "C" void PhotonStream_Serialize_m8_549 ();
extern "C" void PhotonStream_Serialize_m8_550 ();
extern "C" void PhotonStream_Serialize_m8_551 ();
extern "C" void PhotonStream_Serialize_m8_552 ();
extern "C" void PhotonStream_Serialize_m8_553 ();
extern "C" void PhotonStream_Serialize_m8_554 ();
extern "C" void WebRpcResponse__ctor_m8_555 ();
extern "C" void WebRpcResponse_get_Name_m8_556 ();
extern "C" void WebRpcResponse_set_Name_m8_557 ();
extern "C" void WebRpcResponse_get_ReturnCode_m8_558 ();
extern "C" void WebRpcResponse_set_ReturnCode_m8_559 ();
extern "C" void WebRpcResponse_get_DebugMessage_m8_560 ();
extern "C" void WebRpcResponse_set_DebugMessage_m8_561 ();
extern "C" void WebRpcResponse_get_Parameters_m8_562 ();
extern "C" void WebRpcResponse_set_Parameters_m8_563 ();
extern "C" void WebRpcResponse_ToStringFull_m8_564 ();
extern "C" void U3CPingAvailableRegionsCoroutineU3Ec__Iterator0__ctor_m8_565 ();
extern "C" void U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_566 ();
extern "C" void U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m8_567 ();
extern "C" void U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_MoveNext_m8_568 ();
extern "C" void U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_Dispose_m8_569 ();
extern "C" void U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_Reset_m8_570 ();
extern "C" void PhotonHandler__ctor_m8_571 ();
extern "C" void PhotonHandler__cctor_m8_572 ();
extern "C" void PhotonHandler_Awake_m8_573 ();
extern "C" void PhotonHandler_OnApplicationQuit_m8_574 ();
extern "C" void PhotonHandler_OnApplicationPause_m8_575 ();
extern "C" void PhotonHandler_OnDestroy_m8_576 ();
extern "C" void PhotonHandler_Update_m8_577 ();
extern "C" void PhotonHandler_OnLevelWasLoaded_m8_578 ();
extern "C" void PhotonHandler_OnJoinedRoom_m8_579 ();
extern "C" void PhotonHandler_OnCreatedRoom_m8_580 ();
extern "C" void PhotonHandler_StartFallbackSendAckThread_m8_581 ();
extern "C" void PhotonHandler_StopFallbackSendAckThread_m8_582 ();
extern "C" void PhotonHandler_FallbackSendAckThread_m8_583 ();
extern "C" void PhotonHandler_get_BestRegionCodeInPreferences_m8_584 ();
extern "C" void PhotonHandler_set_BestRegionCodeInPreferences_m8_585 ();
extern "C" void PhotonHandler_PingAvailableRegionsAndConnectToBest_m8_586 ();
extern "C" void PhotonHandler_PingAvailableRegionsCoroutine_m8_587 ();
extern "C" void PhotonLagSimulationGui__ctor_m8_588 ();
extern "C" void PhotonLagSimulationGui_get_Peer_m8_589 ();
extern "C" void PhotonLagSimulationGui_set_Peer_m8_590 ();
extern "C" void PhotonLagSimulationGui_Start_m8_591 ();
extern "C" void PhotonLagSimulationGui_OnGUI_m8_592 ();
extern "C" void PhotonLagSimulationGui_NetSimHasNoPeerWindow_m8_593 ();
extern "C" void PhotonLagSimulationGui_NetSimWindow_m8_594 ();
extern "C" void EventCallback__ctor_m8_595 ();
extern "C" void EventCallback_Invoke_m8_596 ();
extern "C" void EventCallback_BeginInvoke_m8_597 ();
extern "C" void EventCallback_EndInvoke_m8_598 ();
extern "C" void PhotonNetwork__cctor_m8_599 ();
extern "C" void PhotonNetwork_get_gameVersion_m8_600 ();
extern "C" void PhotonNetwork_set_gameVersion_m8_601 ();
extern "C" void PhotonNetwork_get_ServerAddress_m8_602 ();
extern "C" void PhotonNetwork_get_connected_m8_603 ();
extern "C" void PhotonNetwork_get_connecting_m8_604 ();
extern "C" void PhotonNetwork_get_connectedAndReady_m8_605 ();
extern "C" void PhotonNetwork_get_connectionState_m8_606 ();
extern "C" void PhotonNetwork_get_connectionStateDetailed_m8_607 ();
extern "C" void PhotonNetwork_get_Server_m8_608 ();
extern "C" void PhotonNetwork_get_AuthValues_m8_609 ();
extern "C" void PhotonNetwork_set_AuthValues_m8_610 ();
extern "C" void PhotonNetwork_get_room_m8_611 ();
extern "C" void PhotonNetwork_get_player_m8_612 ();
extern "C" void PhotonNetwork_get_masterClient_m8_613 ();
extern "C" void PhotonNetwork_get_playerName_m8_614 ();
extern "C" void PhotonNetwork_set_playerName_m8_615 ();
extern "C" void PhotonNetwork_get_playerList_m8_616 ();
extern "C" void PhotonNetwork_get_otherPlayers_m8_617 ();
extern "C" void PhotonNetwork_get_Friends_m8_618 ();
extern "C" void PhotonNetwork_set_Friends_m8_619 ();
extern "C" void PhotonNetwork_get_FriendsListAge_m8_620 ();
extern "C" void PhotonNetwork_get_PrefabPool_m8_621 ();
extern "C" void PhotonNetwork_set_PrefabPool_m8_622 ();
extern "C" void PhotonNetwork_get_offlineMode_m8_623 ();
extern "C" void PhotonNetwork_set_offlineMode_m8_624 ();
extern "C" void PhotonNetwork_get_automaticallySyncScene_m8_625 ();
extern "C" void PhotonNetwork_set_automaticallySyncScene_m8_626 ();
extern "C" void PhotonNetwork_get_autoCleanUpPlayerObjects_m8_627 ();
extern "C" void PhotonNetwork_set_autoCleanUpPlayerObjects_m8_628 ();
extern "C" void PhotonNetwork_get_autoJoinLobby_m8_629 ();
extern "C" void PhotonNetwork_set_autoJoinLobby_m8_630 ();
extern "C" void PhotonNetwork_get_EnableLobbyStatistics_m8_631 ();
extern "C" void PhotonNetwork_set_EnableLobbyStatistics_m8_632 ();
extern "C" void PhotonNetwork_get_LobbyStatistics_m8_633 ();
extern "C" void PhotonNetwork_set_LobbyStatistics_m8_634 ();
extern "C" void PhotonNetwork_get_insideLobby_m8_635 ();
extern "C" void PhotonNetwork_get_lobby_m8_636 ();
extern "C" void PhotonNetwork_set_lobby_m8_637 ();
extern "C" void PhotonNetwork_get_sendRate_m8_638 ();
extern "C" void PhotonNetwork_set_sendRate_m8_639 ();
extern "C" void PhotonNetwork_get_sendRateOnSerialize_m8_640 ();
extern "C" void PhotonNetwork_set_sendRateOnSerialize_m8_641 ();
extern "C" void PhotonNetwork_get_isMessageQueueRunning_m8_642 ();
extern "C" void PhotonNetwork_set_isMessageQueueRunning_m8_643 ();
extern "C" void PhotonNetwork_get_unreliableCommandsLimit_m8_644 ();
extern "C" void PhotonNetwork_set_unreliableCommandsLimit_m8_645 ();
extern "C" void PhotonNetwork_get_time_m8_646 ();
extern "C" void PhotonNetwork_get_ServerTimestamp_m8_647 ();
extern "C" void PhotonNetwork_get_isMasterClient_m8_648 ();
extern "C" void PhotonNetwork_get_inRoom_m8_649 ();
extern "C" void PhotonNetwork_get_isNonMasterClientInRoom_m8_650 ();
extern "C" void PhotonNetwork_get_countOfPlayersOnMaster_m8_651 ();
extern "C" void PhotonNetwork_get_countOfPlayersInRooms_m8_652 ();
extern "C" void PhotonNetwork_get_countOfPlayers_m8_653 ();
extern "C" void PhotonNetwork_get_countOfRooms_m8_654 ();
extern "C" void PhotonNetwork_get_NetworkStatisticsEnabled_m8_655 ();
extern "C" void PhotonNetwork_set_NetworkStatisticsEnabled_m8_656 ();
extern "C" void PhotonNetwork_get_ResentReliableCommands_m8_657 ();
extern "C" void PhotonNetwork_get_CrcCheckEnabled_m8_658 ();
extern "C" void PhotonNetwork_set_CrcCheckEnabled_m8_659 ();
extern "C" void PhotonNetwork_get_PacketLossByCrcCheck_m8_660 ();
extern "C" void PhotonNetwork_get_MaxResendsBeforeDisconnect_m8_661 ();
extern "C" void PhotonNetwork_set_MaxResendsBeforeDisconnect_m8_662 ();
extern "C" void PhotonNetwork_get_QuickResends_m8_663 ();
extern "C" void PhotonNetwork_set_QuickResends_m8_664 ();
extern "C" void PhotonNetwork_SwitchToProtocol_m8_665 ();
extern "C" void PhotonNetwork_ConnectUsingSettings_m8_666 ();
extern "C" void PhotonNetwork_ConnectToMaster_m8_667 ();
extern "C" void PhotonNetwork_ConnectToBestCloudServer_m8_668 ();
extern "C" void PhotonNetwork_ConnectToRegion_m8_669 ();
extern "C" void PhotonNetwork_OverrideBestCloudServer_m8_670 ();
extern "C" void PhotonNetwork_RefreshCloudServerRating_m8_671 ();
extern "C" void PhotonNetwork_NetworkStatisticsReset_m8_672 ();
extern "C" void PhotonNetwork_NetworkStatisticsToString_m8_673 ();
extern "C" void PhotonNetwork_InitializeSecurity_m8_674 ();
extern "C" void PhotonNetwork_VerifyCanUseNetwork_m8_675 ();
extern "C" void PhotonNetwork_Disconnect_m8_676 ();
extern "C" void PhotonNetwork_FindFriends_m8_677 ();
extern "C" void PhotonNetwork_CreateRoom_m8_678 ();
extern "C" void PhotonNetwork_CreateRoom_m8_679 ();
extern "C" void PhotonNetwork_CreateRoom_m8_680 ();
extern "C" void PhotonNetwork_JoinRoom_m8_681 ();
extern "C" void PhotonNetwork_JoinOrCreateRoom_m8_682 ();
extern "C" void PhotonNetwork_JoinRandomRoom_m8_683 ();
extern "C" void PhotonNetwork_JoinRandomRoom_m8_684 ();
extern "C" void PhotonNetwork_JoinRandomRoom_m8_685 ();
extern "C" void PhotonNetwork_EnterOfflineRoom_m8_686 ();
extern "C" void PhotonNetwork_JoinLobby_m8_687 ();
extern "C" void PhotonNetwork_JoinLobby_m8_688 ();
extern "C" void PhotonNetwork_LeaveLobby_m8_689 ();
extern "C" void PhotonNetwork_LeaveRoom_m8_690 ();
extern "C" void PhotonNetwork_GetRoomList_m8_691 ();
extern "C" void PhotonNetwork_SetPlayerCustomProperties_m8_692 ();
extern "C" void PhotonNetwork_RemovePlayerCustomProperties_m8_693 ();
extern "C" void PhotonNetwork_RaiseEvent_m8_694 ();
extern "C" void PhotonNetwork_AllocateViewID_m8_695 ();
extern "C" void PhotonNetwork_AllocateSceneViewID_m8_696 ();
extern "C" void PhotonNetwork_AllocateViewID_m8_697 ();
extern "C" void PhotonNetwork_AllocateSceneViewIDs_m8_698 ();
extern "C" void PhotonNetwork_UnAllocateViewID_m8_699 ();
extern "C" void PhotonNetwork_Instantiate_m8_700 ();
extern "C" void PhotonNetwork_Instantiate_m8_701 ();
extern "C" void PhotonNetwork_InstantiateSceneObject_m8_702 ();
extern "C" void PhotonNetwork_GetPing_m8_703 ();
extern "C" void PhotonNetwork_FetchServerTimestamp_m8_704 ();
extern "C" void PhotonNetwork_SendOutgoingCommands_m8_705 ();
extern "C" void PhotonNetwork_CloseConnection_m8_706 ();
extern "C" void PhotonNetwork_SetMasterClient_m8_707 ();
extern "C" void PhotonNetwork_Destroy_m8_708 ();
extern "C" void PhotonNetwork_Destroy_m8_709 ();
extern "C" void PhotonNetwork_DestroyPlayerObjects_m8_710 ();
extern "C" void PhotonNetwork_DestroyPlayerObjects_m8_711 ();
extern "C" void PhotonNetwork_DestroyAll_m8_712 ();
extern "C" void PhotonNetwork_RemoveRPCs_m8_713 ();
extern "C" void PhotonNetwork_RemoveRPCs_m8_714 ();
extern "C" void PhotonNetwork_RemoveRPCsInGroup_m8_715 ();
extern "C" void PhotonNetwork_RPC_m8_716 ();
extern "C" void PhotonNetwork_RPC_m8_717 ();
extern "C" void PhotonNetwork_CacheSendMonoMessageTargets_m8_718 ();
extern "C" void PhotonNetwork_FindGameObjectsWithComponent_m8_719 ();
extern "C" void PhotonNetwork_SetReceivingEnabled_m8_720 ();
extern "C" void PhotonNetwork_SetReceivingEnabled_m8_721 ();
extern "C" void PhotonNetwork_SetSendingEnabled_m8_722 ();
extern "C" void PhotonNetwork_SetSendingEnabled_m8_723 ();
extern "C" void PhotonNetwork_SetLevelPrefix_m8_724 ();
extern "C" void PhotonNetwork_LoadLevel_m8_725 ();
extern "C" void PhotonNetwork_LoadLevel_m8_726 ();
extern "C" void PhotonNetwork_WebRpc_m8_727 ();
extern "C" void PhotonPlayer__ctor_m8_728 ();
extern "C" void PhotonPlayer__ctor_m8_729 ();
extern "C" void PhotonPlayer_get_ID_m8_730 ();
extern "C" void PhotonPlayer_get_name_m8_731 ();
extern "C" void PhotonPlayer_set_name_m8_732 ();
extern "C" void PhotonPlayer_get_isMasterClient_m8_733 ();
extern "C" void PhotonPlayer_get_customProperties_m8_734 ();
extern "C" void PhotonPlayer_set_customProperties_m8_735 ();
extern "C" void PhotonPlayer_get_allProperties_m8_736 ();
extern "C" void PhotonPlayer_Equals_m8_737 ();
extern "C" void PhotonPlayer_GetHashCode_m8_738 ();
extern "C" void PhotonPlayer_InternalChangeLocalID_m8_739 ();
extern "C" void PhotonPlayer_InternalCacheProperties_m8_740 ();
extern "C" void PhotonPlayer_SetCustomProperties_m8_741 ();
extern "C" void PhotonPlayer_Find_m8_742 ();
extern "C" void PhotonPlayer_Get_m8_743 ();
extern "C" void PhotonPlayer_GetNext_m8_744 ();
extern "C" void PhotonPlayer_GetNextFor_m8_745 ();
extern "C" void PhotonPlayer_GetNextFor_m8_746 ();
extern "C" void PhotonPlayer_ToString_m8_747 ();
extern "C" void PhotonPlayer_ToStringFull_m8_748 ();
extern "C" void PhotonStatsGui__ctor_m8_749 ();
extern "C" void PhotonStatsGui_Start_m8_750 ();
extern "C" void PhotonStatsGui_Update_m8_751 ();
extern "C" void PhotonStatsGui_OnGUI_m8_752 ();
extern "C" void PhotonStatsGui_TrafficStatsWindow_m8_753 ();
extern "C" void PhotonStreamQueue__ctor_m8_754 ();
extern "C" void PhotonStreamQueue_BeginWritePackage_m8_755 ();
extern "C" void PhotonStreamQueue_Reset_m8_756 ();
extern "C" void PhotonStreamQueue_SendNext_m8_757 ();
extern "C" void PhotonStreamQueue_HasQueuedObjects_m8_758 ();
extern "C" void PhotonStreamQueue_ReceiveNext_m8_759 ();
extern "C" void PhotonStreamQueue_Serialize_m8_760 ();
extern "C" void PhotonStreamQueue_Deserialize_m8_761 ();
extern "C" void PhotonView__ctor_m8_762 ();
extern "C" void PhotonView_get_prefix_m8_763 ();
extern "C" void PhotonView_set_prefix_m8_764 ();
extern "C" void PhotonView_get_instantiationData_m8_765 ();
extern "C" void PhotonView_set_instantiationData_m8_766 ();
extern "C" void PhotonView_get_viewID_m8_767 ();
extern "C" void PhotonView_set_viewID_m8_768 ();
extern "C" void PhotonView_get_isSceneView_m8_769 ();
extern "C" void PhotonView_get_owner_m8_770 ();
extern "C" void PhotonView_get_OwnerActorNr_m8_771 ();
extern "C" void PhotonView_get_isOwnerActive_m8_772 ();
extern "C" void PhotonView_get_CreatorActorNr_m8_773 ();
extern "C" void PhotonView_get_isMine_m8_774 ();
extern "C" void PhotonView_Awake_m8_775 ();
extern "C" void PhotonView_RequestOwnership_m8_776 ();
extern "C" void PhotonView_TransferOwnership_m8_777 ();
extern "C" void PhotonView_TransferOwnership_m8_778 ();
extern "C" void PhotonView_OnDestroy_m8_779 ();
extern "C" void PhotonView_SerializeView_m8_780 ();
extern "C" void PhotonView_DeserializeView_m8_781 ();
extern "C" void PhotonView_DeserializeComponent_m8_782 ();
extern "C" void PhotonView_SerializeComponent_m8_783 ();
extern "C" void PhotonView_ExecuteComponentOnSerialize_m8_784 ();
extern "C" void PhotonView_RefreshRpcMonoBehaviourCache_m8_785 ();
extern "C" void PhotonView_RPC_m8_786 ();
extern "C" void PhotonView_RpcSecure_m8_787 ();
extern "C" void PhotonView_RPC_m8_788 ();
extern "C" void PhotonView_RpcSecure_m8_789 ();
extern "C" void PhotonView_Get_m8_790 ();
extern "C" void PhotonView_Get_m8_791 ();
extern "C" void PhotonView_Find_m8_792 ();
extern "C" void PhotonView_ToString_m8_793 ();
extern "C" void U3CPingSocketU3Ec__Iterator1__ctor_m8_794 ();
extern "C" void U3CPingSocketU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_795 ();
extern "C" void U3CPingSocketU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m8_796 ();
extern "C" void U3CPingSocketU3Ec__Iterator1_MoveNext_m8_797 ();
extern "C" void U3CPingSocketU3Ec__Iterator1_Dispose_m8_798 ();
extern "C" void U3CPingSocketU3Ec__Iterator1_Reset_m8_799 ();
extern "C" void PhotonPingManager__ctor_m8_800 ();
extern "C" void PhotonPingManager__cctor_m8_801 ();
extern "C" void PhotonPingManager_get_BestRegion_m8_802 ();
extern "C" void PhotonPingManager_get_Done_m8_803 ();
extern "C" void PhotonPingManager_PingSocket_m8_804 ();
extern "C" void PhotonPingManager_ResolveHost_m8_805 ();
extern "C" void PunRPC__ctor_m8_806 ();
extern "C" void Room__ctor_m8_807 ();
extern "C" void Room_get_playerCount_m8_808 ();
extern "C" void Room_get_name_m8_809 ();
extern "C" void Room_set_name_m8_810 ();
extern "C" void Room_get_maxPlayers_m8_811 ();
extern "C" void Room_set_maxPlayers_m8_812 ();
extern "C" void Room_get_open_m8_813 ();
extern "C" void Room_set_open_m8_814 ();
extern "C" void Room_get_visible_m8_815 ();
extern "C" void Room_set_visible_m8_816 ();
extern "C" void Room_get_propertiesListedInLobby_m8_817 ();
extern "C" void Room_set_propertiesListedInLobby_m8_818 ();
extern "C" void Room_get_autoCleanUp_m8_819 ();
extern "C" void Room_get_masterClientId_m8_820 ();
extern "C" void Room_set_masterClientId_m8_821 ();
extern "C" void Room_SetCustomProperties_m8_822 ();
extern "C" void Room_SetPropertiesListedInLobby_m8_823 ();
extern "C" void Room_ToString_m8_824 ();
extern "C" void Room_ToStringFull_m8_825 ();
extern "C" void RoomInfo__ctor_m8_826 ();
extern "C" void RoomInfo_get_removedFromList_m8_827 ();
extern "C" void RoomInfo_set_removedFromList_m8_828 ();
extern "C" void RoomInfo_get_serverSideMasterClient_m8_829 ();
extern "C" void RoomInfo_set_serverSideMasterClient_m8_830 ();
extern "C" void RoomInfo_get_customProperties_m8_831 ();
extern "C" void RoomInfo_get_name_m8_832 ();
extern "C" void RoomInfo_get_playerCount_m8_833 ();
extern "C" void RoomInfo_set_playerCount_m8_834 ();
extern "C" void RoomInfo_get_isLocalClientInside_m8_835 ();
extern "C" void RoomInfo_set_isLocalClientInside_m8_836 ();
extern "C" void RoomInfo_get_maxPlayers_m8_837 ();
extern "C" void RoomInfo_get_open_m8_838 ();
extern "C" void RoomInfo_get_visible_m8_839 ();
extern "C" void RoomInfo_Equals_m8_840 ();
extern "C" void RoomInfo_GetHashCode_m8_841 ();
extern "C" void RoomInfo_ToString_m8_842 ();
extern "C" void RoomInfo_ToStringFull_m8_843 ();
extern "C" void RoomInfo_InternalCacheProperties_m8_844 ();
extern "C" void Region__ctor_m8_845 ();
extern "C" void Region_Parse_m8_846 ();
extern "C" void Region_ParseFlag_m8_847 ();
extern "C" void Region_ToString_m8_848 ();
extern "C" void ServerSettings__ctor_m8_849 ();
extern "C" void ServerSettings_UseCloudBestRegion_m8_850 ();
extern "C" void ServerSettings_UseCloud_m8_851 ();
extern "C" void ServerSettings_UseCloud_m8_852 ();
extern "C" void ServerSettings_UseMyServer_m8_853 ();
extern "C" void ServerSettings_ToString_m8_854 ();
extern "C" void SynchronizedParameter__ctor_m8_855 ();
extern "C" void SynchronizedLayer__ctor_m8_856 ();
extern "C" void U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3__ctor_m8_857 ();
extern "C" void U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_U3CU3Em__0_m8_858 ();
extern "C" void U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4__ctor_m8_859 ();
extern "C" void U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_U3CU3Em__1_m8_860 ();
extern "C" void U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5__ctor_m8_861 ();
extern "C" void U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_U3CU3Em__2_m8_862 ();
extern "C" void U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6__ctor_m8_863 ();
extern "C" void U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_U3CU3Em__3_m8_864 ();
extern "C" void U3CSetLayerSynchronizedU3Ec__AnonStorey7__ctor_m8_865 ();
extern "C" void U3CSetLayerSynchronizedU3Ec__AnonStorey7_U3CU3Em__4_m8_866 ();
extern "C" void U3CSetParameterSynchronizedU3Ec__AnonStorey8__ctor_m8_867 ();
extern "C" void U3CSetParameterSynchronizedU3Ec__AnonStorey8_U3CU3Em__5_m8_868 ();
extern "C" void PhotonAnimatorView__ctor_m8_869 ();
extern "C" void PhotonAnimatorView_Awake_m8_870 ();
extern "C" void PhotonAnimatorView_Update_m8_871 ();
extern "C" void PhotonAnimatorView_DoesLayerSynchronizeTypeExist_m8_872 ();
extern "C" void PhotonAnimatorView_DoesParameterSynchronizeTypeExist_m8_873 ();
extern "C" void PhotonAnimatorView_GetSynchronizedLayers_m8_874 ();
extern "C" void PhotonAnimatorView_GetSynchronizedParameters_m8_875 ();
extern "C" void PhotonAnimatorView_GetLayerSynchronizeType_m8_876 ();
extern "C" void PhotonAnimatorView_GetParameterSynchronizeType_m8_877 ();
extern "C" void PhotonAnimatorView_SetLayerSynchronized_m8_878 ();
extern "C" void PhotonAnimatorView_SetParameterSynchronized_m8_879 ();
extern "C" void PhotonAnimatorView_SerializeDataContinuously_m8_880 ();
extern "C" void PhotonAnimatorView_DeserializeDataContinuously_m8_881 ();
extern "C" void PhotonAnimatorView_SerializeDataDiscretly_m8_882 ();
extern "C" void PhotonAnimatorView_DeserializeDataDiscretly_m8_883 ();
extern "C" void PhotonAnimatorView_SerializeSynchronizationTypeState_m8_884 ();
extern "C" void PhotonAnimatorView_DeserializeSynchronizationTypeState_m8_885 ();
extern "C" void PhotonAnimatorView_OnPhotonSerializeView_m8_886 ();
extern "C" void PhotonRigidbody2DView__ctor_m8_887 ();
extern "C" void PhotonRigidbody2DView_Awake_m8_888 ();
extern "C" void PhotonRigidbody2DView_OnPhotonSerializeView_m8_889 ();
extern "C" void PhotonRigidbodyView__ctor_m8_890 ();
extern "C" void PhotonRigidbodyView_Awake_m8_891 ();
extern "C" void PhotonRigidbodyView_OnPhotonSerializeView_m8_892 ();
extern "C" void PhotonTransformView__ctor_m8_893 ();
extern "C" void PhotonTransformView_Awake_m8_894 ();
extern "C" void PhotonTransformView_Update_m8_895 ();
extern "C" void PhotonTransformView_UpdatePosition_m8_896 ();
extern "C" void PhotonTransformView_UpdateRotation_m8_897 ();
extern "C" void PhotonTransformView_UpdateScale_m8_898 ();
extern "C" void PhotonTransformView_SetSynchronizedValues_m8_899 ();
extern "C" void PhotonTransformView_OnPhotonSerializeView_m8_900 ();
extern "C" void PhotonTransformView_DoDrawEstimatedPositionError_m8_901 ();
extern "C" void PhotonTransformViewPositionControl__ctor_m8_902 ();
extern "C" void PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m8_903 ();
extern "C" void PhotonTransformViewPositionControl_SetSynchronizedValues_m8_904 ();
extern "C" void PhotonTransformViewPositionControl_UpdatePosition_m8_905 ();
extern "C" void PhotonTransformViewPositionControl_GetNetworkPosition_m8_906 ();
extern "C" void PhotonTransformViewPositionControl_GetExtrapolatedPositionOffset_m8_907 ();
extern "C" void PhotonTransformViewPositionControl_OnPhotonSerializeView_m8_908 ();
extern "C" void PhotonTransformViewPositionControl_SerializeData_m8_909 ();
extern "C" void PhotonTransformViewPositionControl_DeserializeData_m8_910 ();
extern "C" void PhotonTransformViewPositionModel__ctor_m8_911 ();
extern "C" void PhotonTransformViewRotationControl__ctor_m8_912 ();
extern "C" void PhotonTransformViewRotationControl_GetRotation_m8_913 ();
extern "C" void PhotonTransformViewRotationControl_OnPhotonSerializeView_m8_914 ();
extern "C" void PhotonTransformViewRotationModel__ctor_m8_915 ();
extern "C" void PhotonTransformViewScaleControl__ctor_m8_916 ();
extern "C" void PhotonTransformViewScaleControl_GetScale_m8_917 ();
extern "C" void PhotonTransformViewScaleControl_OnPhotonSerializeView_m8_918 ();
extern "C" void PhotonTransformViewScaleModel__ctor_m8_919 ();
extern "C" void ConnectAndJoinRandom__ctor_m8_920 ();
extern "C" void ConnectAndJoinRandom_Start_m8_921 ();
extern "C" void ConnectAndJoinRandom_Update_m8_922 ();
extern "C" void ConnectAndJoinRandom_OnConnectedToMaster_m8_923 ();
extern "C" void ConnectAndJoinRandom_OnJoinedLobby_m8_924 ();
extern "C" void ConnectAndJoinRandom_OnPhotonRandomJoinFailed_m8_925 ();
extern "C" void ConnectAndJoinRandom_OnFailedToConnectToPhoton_m8_926 ();
extern "C" void ConnectAndJoinRandom_OnJoinedRoom_m8_927 ();
extern "C" void HighlightOwnedGameObj__ctor_m8_928 ();
extern "C" void HighlightOwnedGameObj_Update_m8_929 ();
extern "C" void InRoomChat__ctor_m8_930 ();
extern "C" void InRoomChat__cctor_m8_931 ();
extern "C" void InRoomChat_Start_m8_932 ();
extern "C" void InRoomChat_OnGUI_m8_933 ();
extern "C" void InRoomChat_Chat_m8_934 ();
extern "C" void InRoomChat_AddLine_m8_935 ();
extern "C" void InRoomRoundTimer__ctor_m8_936 ();
extern "C" void InRoomRoundTimer_StartRoundNow_m8_937 ();
extern "C" void InRoomRoundTimer_OnJoinedRoom_m8_938 ();
extern "C" void InRoomRoundTimer_OnPhotonCustomRoomPropertiesChanged_m8_939 ();
extern "C" void InRoomRoundTimer_OnMasterClientSwitched_m8_940 ();
extern "C" void InRoomRoundTimer_Update_m8_941 ();
extern "C" void InRoomRoundTimer_OnGUI_m8_942 ();
extern "C" void InputToEvent__ctor_m8_943 ();
extern "C" void InputToEvent_get_goPointedAt_m8_944 ();
extern "C" void InputToEvent_set_goPointedAt_m8_945 ();
extern "C" void InputToEvent_get_DragVector_m8_946 ();
extern "C" void InputToEvent_Start_m8_947 ();
extern "C" void InputToEvent_Update_m8_948 ();
extern "C" void InputToEvent_Press_m8_949 ();
extern "C" void InputToEvent_Release_m8_950 ();
extern "C" void InputToEvent_RaycastObject_m8_951 ();
extern "C" void ManualPhotonViewAllocator__ctor_m8_952 ();
extern "C" void ManualPhotonViewAllocator_AllocateManualPhotonView_m8_953 ();
extern "C" void ManualPhotonViewAllocator_InstantiateRpc_m8_954 ();
extern "C" void MoveByKeys__ctor_m8_955 ();
extern "C" void MoveByKeys_Start_m8_956 ();
extern "C" void MoveByKeys_FixedUpdate_m8_957 ();
extern "C" void OnAwakeUsePhotonView__ctor_m8_958 ();
extern "C" void OnAwakeUsePhotonView_Awake_m8_959 ();
extern "C" void OnAwakeUsePhotonView_Start_m8_960 ();
extern "C" void OnAwakeUsePhotonView_OnAwakeRPC_m8_961 ();
extern "C" void OnAwakeUsePhotonView_OnAwakeRPC_m8_962 ();
extern "C" void U3CDestroyRpcU3Ec__Iterator2__ctor_m8_963 ();
extern "C" void U3CDestroyRpcU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_964 ();
extern "C" void U3CDestroyRpcU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m8_965 ();
extern "C" void U3CDestroyRpcU3Ec__Iterator2_MoveNext_m8_966 ();
extern "C" void U3CDestroyRpcU3Ec__Iterator2_Dispose_m8_967 ();
extern "C" void U3CDestroyRpcU3Ec__Iterator2_Reset_m8_968 ();
extern "C" void OnClickDestroy__ctor_m8_969 ();
extern "C" void OnClickDestroy_OnClick_m8_970 ();
extern "C" void OnClickDestroy_DestroyRpc_m8_971 ();
extern "C" void OnClickInstantiate__ctor_m8_972 ();
extern "C" void OnClickInstantiate_OnClick_m8_973 ();
extern "C" void OnClickInstantiate_OnGUI_m8_974 ();
extern "C" void OnClickLoadSomething__ctor_m8_975 ();
extern "C" void OnClickLoadSomething_OnClick_m8_976 ();
extern "C" void OnJoinedInstantiate__ctor_m8_977 ();
extern "C" void OnJoinedInstantiate_OnJoinedRoom_m8_978 ();
extern "C" void OnStartDelete__ctor_m8_979 ();
extern "C" void OnStartDelete_Start_m8_980 ();
extern "C" void PickupItem__ctor_m8_981 ();
extern "C" void PickupItem__cctor_m8_982 ();
extern "C" void PickupItem_get_ViewID_m8_983 ();
extern "C" void PickupItem_OnTriggerEnter_m8_984 ();
extern "C" void PickupItem_OnPhotonSerializeView_m8_985 ();
extern "C" void PickupItem_Pickup_m8_986 ();
extern "C" void PickupItem_Drop_m8_987 ();
extern "C" void PickupItem_Drop_m8_988 ();
extern "C" void PickupItem_PunPickup_m8_989 ();
extern "C" void PickupItem_PickedUp_m8_990 ();
extern "C" void PickupItem_PunRespawn_m8_991 ();
extern "C" void PickupItem_PunRespawn_m8_992 ();
extern "C" void PickupItemSimple__ctor_m8_993 ();
extern "C" void PickupItemSimple_OnTriggerEnter_m8_994 ();
extern "C" void PickupItemSimple_Pickup_m8_995 ();
extern "C" void PickupItemSimple_PunPickupSimple_m8_996 ();
extern "C" void PickupItemSimple_RespawnAfter_m8_997 ();
extern "C" void PickupItemSyncer__ctor_m8_998 ();
extern "C" void PickupItemSyncer_OnPhotonPlayerConnected_m8_999 ();
extern "C" void PickupItemSyncer_OnJoinedRoom_m8_1000 ();
extern "C" void PickupItemSyncer_AskForPickupItemSpawnTimes_m8_1001 ();
extern "C" void PickupItemSyncer_RequestForPickupTimes_m8_1002 ();
extern "C" void PickupItemSyncer_SendPickedUpItems_m8_1003 ();
extern "C" void PickupItemSyncer_PickupItemInit_m8_1004 ();
extern "C" void PointedAtGameObjectInfo__ctor_m8_1005 ();
extern "C" void PointedAtGameObjectInfo_OnGUI_m8_1006 ();
extern "C" void PunPlayerScores__ctor_m8_1007 ();
extern "C" void ScoreExtensions_SetScore_m8_1008 ();
extern "C" void ScoreExtensions_AddScore_m8_1009 ();
extern "C" void ScoreExtensions_GetScore_m8_1010 ();
extern "C" void PunTeams__ctor_m8_1011 ();
extern "C" void PunTeams_Start_m8_1012 ();
extern "C" void PunTeams_OnJoinedRoom_m8_1013 ();
extern "C" void PunTeams_OnPhotonPlayerPropertiesChanged_m8_1014 ();
extern "C" void PunTeams_UpdateTeams_m8_1015 ();
extern "C" void TeamExtensions_GetTeam_m8_1016 ();
extern "C" void TeamExtensions_SetTeam_m8_1017 ();
extern "C" void QuitOnEscapeOrBack__ctor_m8_1018 ();
extern "C" void QuitOnEscapeOrBack_Update_m8_1019 ();
extern "C" void ServerTime__ctor_m8_1020 ();
extern "C" void ServerTime_OnGUI_m8_1021 ();
extern "C" void ShowInfoOfPlayer__ctor_m8_1022 ();
extern "C" void ShowInfoOfPlayer_Start_m8_1023 ();
extern "C" void ShowInfoOfPlayer_Update_m8_1024 ();
extern "C" void ShowStatusWhenConnecting__ctor_m8_1025 ();
extern "C" void ShowStatusWhenConnecting_OnGUI_m8_1026 ();
extern "C" void ShowStatusWhenConnecting_GetConnectingDots_m8_1027 ();
extern "C" void SmoothSyncMovement__ctor_m8_1028 ();
extern "C" void SmoothSyncMovement_Awake_m8_1029 ();
extern "C" void SmoothSyncMovement_OnPhotonSerializeView_m8_1030 ();
extern "C" void SmoothSyncMovement_Update_m8_1031 ();
extern "C" void SupportLogger__ctor_m8_1032 ();
extern "C" void SupportLogger_Start_m8_1033 ();
extern "C" void SupportLogging__ctor_m8_1034 ();
extern "C" void SupportLogging_Start_m8_1035 ();
extern "C" void SupportLogging_OnApplicationPause_m8_1036 ();
extern "C" void SupportLogging_OnApplicationQuit_m8_1037 ();
extern "C" void SupportLogging_LogStats_m8_1038 ();
extern "C" void SupportLogging_LogBasics_m8_1039 ();
extern "C" void SupportLogging_OnConnectedToPhoton_m8_1040 ();
extern "C" void SupportLogging_OnFailedToConnectToPhoton_m8_1041 ();
extern "C" void SupportLogging_OnJoinedLobby_m8_1042 ();
extern "C" void SupportLogging_OnJoinedRoom_m8_1043 ();
extern "C" void SupportLogging_OnCreatedRoom_m8_1044 ();
extern "C" void SupportLogging_OnLeftRoom_m8_1045 ();
extern "C" void SupportLogging_OnDisconnectedFromPhoton_m8_1046 ();
extern "C" void TimeKeeper__ctor_m8_1047 ();
extern "C" void TimeKeeper_get_Interval_m8_1048 ();
extern "C" void TimeKeeper_set_Interval_m8_1049 ();
extern "C" void TimeKeeper_get_IsEnabled_m8_1050 ();
extern "C" void TimeKeeper_set_IsEnabled_m8_1051 ();
extern "C" void TimeKeeper_get_ShouldExecute_m8_1052 ();
extern "C" void TimeKeeper_set_ShouldExecute_m8_1053 ();
extern "C" void TimeKeeper_Reset_m8_1054 ();
extern const methodPointerType g_MethodPointers[10738] = 
{
	Object__ctor_m1_0,
	Object_Equals_m1_1,
	Object_Equals_m1_2,
	Object_Finalize_m1_3,
	Object_GetHashCode_m1_4,
	Object_GetType_m1_5,
	Object_MemberwiseClone_m1_6,
	Object_ToString_m1_7,
	Object_ReferenceEquals_m1_8,
	Object_InternalGetHashCode_m1_9,
	ValueType__ctor_m1_10,
	ValueType_InternalEquals_m1_11,
	ValueType_DefaultEquals_m1_12,
	ValueType_Equals_m1_13,
	ValueType_InternalGetHashCode_m1_14,
	ValueType_GetHashCode_m1_15,
	ValueType_ToString_m1_16,
	Attribute__ctor_m1_17,
	Attribute_CheckParameters_m1_18,
	Attribute_GetCustomAttribute_m1_19,
	Attribute_GetCustomAttribute_m1_20,
	Attribute_GetHashCode_m1_21,
	Attribute_IsDefined_m1_22,
	Attribute_IsDefined_m1_23,
	Attribute_IsDefined_m1_24,
	Attribute_IsDefined_m1_25,
	Attribute_Equals_m1_26,
	Int32_System_IConvertible_ToBoolean_m1_27,
	Int32_System_IConvertible_ToByte_m1_28,
	Int32_System_IConvertible_ToChar_m1_29,
	Int32_System_IConvertible_ToDateTime_m1_30,
	Int32_System_IConvertible_ToDecimal_m1_31,
	Int32_System_IConvertible_ToDouble_m1_32,
	Int32_System_IConvertible_ToInt16_m1_33,
	Int32_System_IConvertible_ToInt32_m1_34,
	Int32_System_IConvertible_ToInt64_m1_35,
	Int32_System_IConvertible_ToSByte_m1_36,
	Int32_System_IConvertible_ToSingle_m1_37,
	Int32_System_IConvertible_ToType_m1_38,
	Int32_System_IConvertible_ToUInt16_m1_39,
	Int32_System_IConvertible_ToUInt32_m1_40,
	Int32_System_IConvertible_ToUInt64_m1_41,
	Int32_CompareTo_m1_42,
	Int32_Equals_m1_43,
	Int32_GetHashCode_m1_44,
	Int32_CompareTo_m1_45,
	Int32_Equals_m1_46,
	Int32_ProcessTrailingWhitespace_m1_47,
	Int32_Parse_m1_48,
	Int32_Parse_m1_49,
	Int32_CheckStyle_m1_50,
	Int32_JumpOverWhite_m1_51,
	Int32_FindSign_m1_52,
	Int32_FindCurrency_m1_53,
	Int32_FindExponent_m1_54,
	Int32_FindOther_m1_55,
	Int32_ValidDigit_m1_56,
	Int32_GetFormatException_m1_57,
	Int32_Parse_m1_58,
	Int32_Parse_m1_59,
	Int32_Parse_m1_60,
	Int32_TryParse_m1_61,
	Int32_TryParse_m1_62,
	Int32_ToString_m1_63,
	Int32_ToString_m1_64,
	Int32_ToString_m1_65,
	Int32_ToString_m1_66,
	SerializableAttribute__ctor_m1_67,
	AttributeUsageAttribute__ctor_m1_68,
	AttributeUsageAttribute_get_AllowMultiple_m1_69,
	AttributeUsageAttribute_set_AllowMultiple_m1_70,
	AttributeUsageAttribute_get_Inherited_m1_71,
	AttributeUsageAttribute_set_Inherited_m1_72,
	ComVisibleAttribute__ctor_m1_73,
	Int64_System_IConvertible_ToBoolean_m1_74,
	Int64_System_IConvertible_ToByte_m1_75,
	Int64_System_IConvertible_ToChar_m1_76,
	Int64_System_IConvertible_ToDateTime_m1_77,
	Int64_System_IConvertible_ToDecimal_m1_78,
	Int64_System_IConvertible_ToDouble_m1_79,
	Int64_System_IConvertible_ToInt16_m1_80,
	Int64_System_IConvertible_ToInt32_m1_81,
	Int64_System_IConvertible_ToInt64_m1_82,
	Int64_System_IConvertible_ToSByte_m1_83,
	Int64_System_IConvertible_ToSingle_m1_84,
	Int64_System_IConvertible_ToType_m1_85,
	Int64_System_IConvertible_ToUInt16_m1_86,
	Int64_System_IConvertible_ToUInt32_m1_87,
	Int64_System_IConvertible_ToUInt64_m1_88,
	Int64_CompareTo_m1_89,
	Int64_Equals_m1_90,
	Int64_GetHashCode_m1_91,
	Int64_CompareTo_m1_92,
	Int64_Equals_m1_93,
	Int64_Parse_m1_94,
	Int64_Parse_m1_95,
	Int64_Parse_m1_96,
	Int64_Parse_m1_97,
	Int64_Parse_m1_98,
	Int64_TryParse_m1_99,
	Int64_TryParse_m1_100,
	Int64_ToString_m1_101,
	Int64_ToString_m1_102,
	Int64_ToString_m1_103,
	Int64_ToString_m1_104,
	UInt32_System_IConvertible_ToBoolean_m1_105,
	UInt32_System_IConvertible_ToByte_m1_106,
	UInt32_System_IConvertible_ToChar_m1_107,
	UInt32_System_IConvertible_ToDateTime_m1_108,
	UInt32_System_IConvertible_ToDecimal_m1_109,
	UInt32_System_IConvertible_ToDouble_m1_110,
	UInt32_System_IConvertible_ToInt16_m1_111,
	UInt32_System_IConvertible_ToInt32_m1_112,
	UInt32_System_IConvertible_ToInt64_m1_113,
	UInt32_System_IConvertible_ToSByte_m1_114,
	UInt32_System_IConvertible_ToSingle_m1_115,
	UInt32_System_IConvertible_ToType_m1_116,
	UInt32_System_IConvertible_ToUInt16_m1_117,
	UInt32_System_IConvertible_ToUInt32_m1_118,
	UInt32_System_IConvertible_ToUInt64_m1_119,
	UInt32_CompareTo_m1_120,
	UInt32_Equals_m1_121,
	UInt32_GetHashCode_m1_122,
	UInt32_CompareTo_m1_123,
	UInt32_Equals_m1_124,
	UInt32_Parse_m1_125,
	UInt32_Parse_m1_126,
	UInt32_Parse_m1_127,
	UInt32_Parse_m1_128,
	UInt32_TryParse_m1_129,
	UInt32_TryParse_m1_130,
	UInt32_ToString_m1_131,
	UInt32_ToString_m1_132,
	UInt32_ToString_m1_133,
	UInt32_ToString_m1_134,
	CLSCompliantAttribute__ctor_m1_135,
	UInt64_System_IConvertible_ToBoolean_m1_136,
	UInt64_System_IConvertible_ToByte_m1_137,
	UInt64_System_IConvertible_ToChar_m1_138,
	UInt64_System_IConvertible_ToDateTime_m1_139,
	UInt64_System_IConvertible_ToDecimal_m1_140,
	UInt64_System_IConvertible_ToDouble_m1_141,
	UInt64_System_IConvertible_ToInt16_m1_142,
	UInt64_System_IConvertible_ToInt32_m1_143,
	UInt64_System_IConvertible_ToInt64_m1_144,
	UInt64_System_IConvertible_ToSByte_m1_145,
	UInt64_System_IConvertible_ToSingle_m1_146,
	UInt64_System_IConvertible_ToType_m1_147,
	UInt64_System_IConvertible_ToUInt16_m1_148,
	UInt64_System_IConvertible_ToUInt32_m1_149,
	UInt64_System_IConvertible_ToUInt64_m1_150,
	UInt64_CompareTo_m1_151,
	UInt64_Equals_m1_152,
	UInt64_GetHashCode_m1_153,
	UInt64_CompareTo_m1_154,
	UInt64_Equals_m1_155,
	UInt64_Parse_m1_156,
	UInt64_Parse_m1_157,
	UInt64_Parse_m1_158,
	UInt64_TryParse_m1_159,
	UInt64_ToString_m1_160,
	UInt64_ToString_m1_161,
	UInt64_ToString_m1_162,
	UInt64_ToString_m1_163,
	Byte_System_IConvertible_ToType_m1_164,
	Byte_System_IConvertible_ToBoolean_m1_165,
	Byte_System_IConvertible_ToByte_m1_166,
	Byte_System_IConvertible_ToChar_m1_167,
	Byte_System_IConvertible_ToDateTime_m1_168,
	Byte_System_IConvertible_ToDecimal_m1_169,
	Byte_System_IConvertible_ToDouble_m1_170,
	Byte_System_IConvertible_ToInt16_m1_171,
	Byte_System_IConvertible_ToInt32_m1_172,
	Byte_System_IConvertible_ToInt64_m1_173,
	Byte_System_IConvertible_ToSByte_m1_174,
	Byte_System_IConvertible_ToSingle_m1_175,
	Byte_System_IConvertible_ToUInt16_m1_176,
	Byte_System_IConvertible_ToUInt32_m1_177,
	Byte_System_IConvertible_ToUInt64_m1_178,
	Byte_CompareTo_m1_179,
	Byte_Equals_m1_180,
	Byte_GetHashCode_m1_181,
	Byte_CompareTo_m1_182,
	Byte_Equals_m1_183,
	Byte_Parse_m1_184,
	Byte_Parse_m1_185,
	Byte_Parse_m1_186,
	Byte_TryParse_m1_187,
	Byte_TryParse_m1_188,
	Byte_ToString_m1_189,
	Byte_ToString_m1_190,
	Byte_ToString_m1_191,
	Byte_ToString_m1_192,
	SByte_System_IConvertible_ToBoolean_m1_193,
	SByte_System_IConvertible_ToByte_m1_194,
	SByte_System_IConvertible_ToChar_m1_195,
	SByte_System_IConvertible_ToDateTime_m1_196,
	SByte_System_IConvertible_ToDecimal_m1_197,
	SByte_System_IConvertible_ToDouble_m1_198,
	SByte_System_IConvertible_ToInt16_m1_199,
	SByte_System_IConvertible_ToInt32_m1_200,
	SByte_System_IConvertible_ToInt64_m1_201,
	SByte_System_IConvertible_ToSByte_m1_202,
	SByte_System_IConvertible_ToSingle_m1_203,
	SByte_System_IConvertible_ToType_m1_204,
	SByte_System_IConvertible_ToUInt16_m1_205,
	SByte_System_IConvertible_ToUInt32_m1_206,
	SByte_System_IConvertible_ToUInt64_m1_207,
	SByte_CompareTo_m1_208,
	SByte_Equals_m1_209,
	SByte_GetHashCode_m1_210,
	SByte_CompareTo_m1_211,
	SByte_Equals_m1_212,
	SByte_Parse_m1_213,
	SByte_Parse_m1_214,
	SByte_Parse_m1_215,
	SByte_TryParse_m1_216,
	SByte_ToString_m1_217,
	SByte_ToString_m1_218,
	SByte_ToString_m1_219,
	SByte_ToString_m1_220,
	Int16_System_IConvertible_ToBoolean_m1_221,
	Int16_System_IConvertible_ToByte_m1_222,
	Int16_System_IConvertible_ToChar_m1_223,
	Int16_System_IConvertible_ToDateTime_m1_224,
	Int16_System_IConvertible_ToDecimal_m1_225,
	Int16_System_IConvertible_ToDouble_m1_226,
	Int16_System_IConvertible_ToInt16_m1_227,
	Int16_System_IConvertible_ToInt32_m1_228,
	Int16_System_IConvertible_ToInt64_m1_229,
	Int16_System_IConvertible_ToSByte_m1_230,
	Int16_System_IConvertible_ToSingle_m1_231,
	Int16_System_IConvertible_ToType_m1_232,
	Int16_System_IConvertible_ToUInt16_m1_233,
	Int16_System_IConvertible_ToUInt32_m1_234,
	Int16_System_IConvertible_ToUInt64_m1_235,
	Int16_CompareTo_m1_236,
	Int16_Equals_m1_237,
	Int16_GetHashCode_m1_238,
	Int16_CompareTo_m1_239,
	Int16_Equals_m1_240,
	Int16_Parse_m1_241,
	Int16_Parse_m1_242,
	Int16_Parse_m1_243,
	Int16_TryParse_m1_244,
	Int16_ToString_m1_245,
	Int16_ToString_m1_246,
	Int16_ToString_m1_247,
	Int16_ToString_m1_248,
	UInt16_System_IConvertible_ToBoolean_m1_249,
	UInt16_System_IConvertible_ToByte_m1_250,
	UInt16_System_IConvertible_ToChar_m1_251,
	UInt16_System_IConvertible_ToDateTime_m1_252,
	UInt16_System_IConvertible_ToDecimal_m1_253,
	UInt16_System_IConvertible_ToDouble_m1_254,
	UInt16_System_IConvertible_ToInt16_m1_255,
	UInt16_System_IConvertible_ToInt32_m1_256,
	UInt16_System_IConvertible_ToInt64_m1_257,
	UInt16_System_IConvertible_ToSByte_m1_258,
	UInt16_System_IConvertible_ToSingle_m1_259,
	UInt16_System_IConvertible_ToType_m1_260,
	UInt16_System_IConvertible_ToUInt16_m1_261,
	UInt16_System_IConvertible_ToUInt32_m1_262,
	UInt16_System_IConvertible_ToUInt64_m1_263,
	UInt16_CompareTo_m1_264,
	UInt16_Equals_m1_265,
	UInt16_GetHashCode_m1_266,
	UInt16_CompareTo_m1_267,
	UInt16_Equals_m1_268,
	UInt16_Parse_m1_269,
	UInt16_Parse_m1_270,
	UInt16_TryParse_m1_271,
	UInt16_TryParse_m1_272,
	UInt16_ToString_m1_273,
	UInt16_ToString_m1_274,
	UInt16_ToString_m1_275,
	UInt16_ToString_m1_276,
	Char__cctor_m1_277,
	Char_System_IConvertible_ToType_m1_278,
	Char_System_IConvertible_ToBoolean_m1_279,
	Char_System_IConvertible_ToByte_m1_280,
	Char_System_IConvertible_ToChar_m1_281,
	Char_System_IConvertible_ToDateTime_m1_282,
	Char_System_IConvertible_ToDecimal_m1_283,
	Char_System_IConvertible_ToDouble_m1_284,
	Char_System_IConvertible_ToInt16_m1_285,
	Char_System_IConvertible_ToInt32_m1_286,
	Char_System_IConvertible_ToInt64_m1_287,
	Char_System_IConvertible_ToSByte_m1_288,
	Char_System_IConvertible_ToSingle_m1_289,
	Char_System_IConvertible_ToUInt16_m1_290,
	Char_System_IConvertible_ToUInt32_m1_291,
	Char_System_IConvertible_ToUInt64_m1_292,
	Char_GetDataTablePointers_m1_293,
	Char_CompareTo_m1_294,
	Char_Equals_m1_295,
	Char_CompareTo_m1_296,
	Char_Equals_m1_297,
	Char_GetHashCode_m1_298,
	Char_GetUnicodeCategory_m1_299,
	Char_IsDigit_m1_300,
	Char_IsLetter_m1_301,
	Char_IsLetterOrDigit_m1_302,
	Char_IsLower_m1_303,
	Char_IsSurrogate_m1_304,
	Char_IsWhiteSpace_m1_305,
	Char_IsWhiteSpace_m1_306,
	Char_CheckParameter_m1_307,
	Char_Parse_m1_308,
	Char_ToLower_m1_309,
	Char_ToLowerInvariant_m1_310,
	Char_ToLower_m1_311,
	Char_ToUpper_m1_312,
	Char_ToUpperInvariant_m1_313,
	Char_ToString_m1_314,
	Char_ToString_m1_315,
	String__ctor_m1_316,
	String__ctor_m1_317,
	String__ctor_m1_318,
	String__ctor_m1_319,
	String__cctor_m1_320,
	String_System_IConvertible_ToBoolean_m1_321,
	String_System_IConvertible_ToByte_m1_322,
	String_System_IConvertible_ToChar_m1_323,
	String_System_IConvertible_ToDateTime_m1_324,
	String_System_IConvertible_ToDecimal_m1_325,
	String_System_IConvertible_ToDouble_m1_326,
	String_System_IConvertible_ToInt16_m1_327,
	String_System_IConvertible_ToInt32_m1_328,
	String_System_IConvertible_ToInt64_m1_329,
	String_System_IConvertible_ToSByte_m1_330,
	String_System_IConvertible_ToSingle_m1_331,
	String_System_IConvertible_ToType_m1_332,
	String_System_IConvertible_ToUInt16_m1_333,
	String_System_IConvertible_ToUInt32_m1_334,
	String_System_IConvertible_ToUInt64_m1_335,
	String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m1_336,
	String_System_Collections_IEnumerable_GetEnumerator_m1_337,
	String_Equals_m1_338,
	String_Equals_m1_339,
	String_Equals_m1_340,
	String_get_Chars_m1_341,
	String_Clone_m1_342,
	String_CopyTo_m1_343,
	String_ToCharArray_m1_344,
	String_ToCharArray_m1_345,
	String_Split_m1_346,
	String_Split_m1_347,
	String_Split_m1_348,
	String_Split_m1_349,
	String_Split_m1_350,
	String_Substring_m1_351,
	String_Substring_m1_352,
	String_SubstringUnchecked_m1_353,
	String_Trim_m1_354,
	String_Trim_m1_355,
	String_TrimStart_m1_356,
	String_TrimEnd_m1_357,
	String_FindNotWhiteSpace_m1_358,
	String_FindNotInTable_m1_359,
	String_Compare_m1_360,
	String_Compare_m1_361,
	String_Compare_m1_362,
	String_Compare_m1_363,
	String_Compare_m1_364,
	String_Equals_m1_365,
	String_CompareTo_m1_366,
	String_CompareTo_m1_367,
	String_CompareOrdinal_m1_368,
	String_CompareOrdinalUnchecked_m1_369,
	String_CompareOrdinalCaseInsensitiveUnchecked_m1_370,
	String_EndsWith_m1_371,
	String_IndexOfAny_m1_372,
	String_IndexOfAny_m1_373,
	String_IndexOfAny_m1_374,
	String_IndexOfAnyUnchecked_m1_375,
	String_IndexOf_m1_376,
	String_IndexOf_m1_377,
	String_IndexOfOrdinal_m1_378,
	String_IndexOfOrdinalUnchecked_m1_379,
	String_IndexOfOrdinalIgnoreCaseUnchecked_m1_380,
	String_IndexOf_m1_381,
	String_IndexOf_m1_382,
	String_IndexOf_m1_383,
	String_IndexOfUnchecked_m1_384,
	String_IndexOf_m1_385,
	String_IndexOf_m1_386,
	String_IndexOf_m1_387,
	String_LastIndexOfAny_m1_388,
	String_LastIndexOfAnyUnchecked_m1_389,
	String_LastIndexOf_m1_390,
	String_LastIndexOf_m1_391,
	String_LastIndexOf_m1_392,
	String_LastIndexOfUnchecked_m1_393,
	String_LastIndexOf_m1_394,
	String_LastIndexOf_m1_395,
	String_Contains_m1_396,
	String_IsNullOrEmpty_m1_397,
	String_PadRight_m1_398,
	String_StartsWith_m1_399,
	String_Replace_m1_400,
	String_Replace_m1_401,
	String_ReplaceUnchecked_m1_402,
	String_ReplaceFallback_m1_403,
	String_Remove_m1_404,
	String_ToLower_m1_405,
	String_ToLower_m1_406,
	String_ToLowerInvariant_m1_407,
	String_ToString_m1_408,
	String_ToString_m1_409,
	String_Format_m1_410,
	String_Format_m1_411,
	String_Format_m1_412,
	String_Format_m1_413,
	String_Format_m1_414,
	String_FormatHelper_m1_415,
	String_Concat_m1_416,
	String_Concat_m1_417,
	String_Concat_m1_418,
	String_Concat_m1_419,
	String_Concat_m1_420,
	String_Concat_m1_421,
	String_Concat_m1_422,
	String_ConcatInternal_m1_423,
	String_Insert_m1_424,
	String_Join_m1_425,
	String_Join_m1_426,
	String_JoinUnchecked_m1_427,
	String_get_Length_m1_428,
	String_ParseFormatSpecifier_m1_429,
	String_ParseDecimal_m1_430,
	String_InternalSetChar_m1_431,
	String_InternalSetLength_m1_432,
	String_GetHashCode_m1_433,
	String_GetCaseInsensitiveHashCode_m1_434,
	String_CreateString_m1_435,
	String_CreateString_m1_436,
	String_CreateString_m1_437,
	String_CreateString_m1_438,
	String_CreateString_m1_439,
	String_CreateString_m1_440,
	String_CreateString_m1_441,
	String_CreateString_m1_442,
	String_memcpy4_m1_443,
	String_memcpy2_m1_444,
	String_memcpy1_m1_445,
	String_memcpy_m1_446,
	String_CharCopy_m1_447,
	String_CharCopyReverse_m1_448,
	String_CharCopy_m1_449,
	String_CharCopy_m1_450,
	String_CharCopyReverse_m1_451,
	String_InternalSplit_m1_452,
	String_InternalAllocateStr_m1_453,
	String_op_Equality_m1_454,
	String_op_Inequality_m1_455,
	Single_System_IConvertible_ToBoolean_m1_456,
	Single_System_IConvertible_ToByte_m1_457,
	Single_System_IConvertible_ToChar_m1_458,
	Single_System_IConvertible_ToDateTime_m1_459,
	Single_System_IConvertible_ToDecimal_m1_460,
	Single_System_IConvertible_ToDouble_m1_461,
	Single_System_IConvertible_ToInt16_m1_462,
	Single_System_IConvertible_ToInt32_m1_463,
	Single_System_IConvertible_ToInt64_m1_464,
	Single_System_IConvertible_ToSByte_m1_465,
	Single_System_IConvertible_ToSingle_m1_466,
	Single_System_IConvertible_ToType_m1_467,
	Single_System_IConvertible_ToUInt16_m1_468,
	Single_System_IConvertible_ToUInt32_m1_469,
	Single_System_IConvertible_ToUInt64_m1_470,
	Single_CompareTo_m1_471,
	Single_Equals_m1_472,
	Single_CompareTo_m1_473,
	Single_Equals_m1_474,
	Single_GetHashCode_m1_475,
	Single_IsInfinity_m1_476,
	Single_IsNaN_m1_477,
	Single_IsNegativeInfinity_m1_478,
	Single_IsPositiveInfinity_m1_479,
	Single_Parse_m1_480,
	Single_ToString_m1_481,
	Single_ToString_m1_482,
	Single_ToString_m1_483,
	Single_ToString_m1_484,
	Double_System_IConvertible_ToType_m1_485,
	Double_System_IConvertible_ToBoolean_m1_486,
	Double_System_IConvertible_ToByte_m1_487,
	Double_System_IConvertible_ToChar_m1_488,
	Double_System_IConvertible_ToDateTime_m1_489,
	Double_System_IConvertible_ToDecimal_m1_490,
	Double_System_IConvertible_ToDouble_m1_491,
	Double_System_IConvertible_ToInt16_m1_492,
	Double_System_IConvertible_ToInt32_m1_493,
	Double_System_IConvertible_ToInt64_m1_494,
	Double_System_IConvertible_ToSByte_m1_495,
	Double_System_IConvertible_ToSingle_m1_496,
	Double_System_IConvertible_ToUInt16_m1_497,
	Double_System_IConvertible_ToUInt32_m1_498,
	Double_System_IConvertible_ToUInt64_m1_499,
	Double_CompareTo_m1_500,
	Double_Equals_m1_501,
	Double_CompareTo_m1_502,
	Double_Equals_m1_503,
	Double_GetHashCode_m1_504,
	Double_IsInfinity_m1_505,
	Double_IsNaN_m1_506,
	Double_IsNegativeInfinity_m1_507,
	Double_IsPositiveInfinity_m1_508,
	Double_Parse_m1_509,
	Double_Parse_m1_510,
	Double_Parse_m1_511,
	Double_Parse_m1_512,
	Double_TryParseStringConstant_m1_513,
	Double_ParseImpl_m1_514,
	Double_ToString_m1_515,
	Double_ToString_m1_516,
	Double_ToString_m1_517,
	Decimal__ctor_m1_518,
	Decimal__ctor_m1_519,
	Decimal__ctor_m1_520,
	Decimal__ctor_m1_521,
	Decimal__ctor_m1_522,
	Decimal__ctor_m1_523,
	Decimal__ctor_m1_524,
	Decimal__cctor_m1_525,
	Decimal_System_IConvertible_ToType_m1_526,
	Decimal_System_IConvertible_ToBoolean_m1_527,
	Decimal_System_IConvertible_ToByte_m1_528,
	Decimal_System_IConvertible_ToChar_m1_529,
	Decimal_System_IConvertible_ToDateTime_m1_530,
	Decimal_System_IConvertible_ToDecimal_m1_531,
	Decimal_System_IConvertible_ToDouble_m1_532,
	Decimal_System_IConvertible_ToInt16_m1_533,
	Decimal_System_IConvertible_ToInt32_m1_534,
	Decimal_System_IConvertible_ToInt64_m1_535,
	Decimal_System_IConvertible_ToSByte_m1_536,
	Decimal_System_IConvertible_ToSingle_m1_537,
	Decimal_System_IConvertible_ToUInt16_m1_538,
	Decimal_System_IConvertible_ToUInt32_m1_539,
	Decimal_System_IConvertible_ToUInt64_m1_540,
	Decimal_GetBits_m1_541,
	Decimal_Add_m1_542,
	Decimal_Subtract_m1_543,
	Decimal_GetHashCode_m1_544,
	Decimal_u64_m1_545,
	Decimal_s64_m1_546,
	Decimal_Equals_m1_547,
	Decimal_Equals_m1_548,
	Decimal_IsZero_m1_549,
	Decimal_Floor_m1_550,
	Decimal_Multiply_m1_551,
	Decimal_Divide_m1_552,
	Decimal_Compare_m1_553,
	Decimal_CompareTo_m1_554,
	Decimal_CompareTo_m1_555,
	Decimal_Equals_m1_556,
	Decimal_Parse_m1_557,
	Decimal_ThrowAtPos_m1_558,
	Decimal_ThrowInvalidExp_m1_559,
	Decimal_stripStyles_m1_560,
	Decimal_Parse_m1_561,
	Decimal_PerformParse_m1_562,
	Decimal_ToString_m1_563,
	Decimal_ToString_m1_564,
	Decimal_ToString_m1_565,
	Decimal_decimal2UInt64_m1_566,
	Decimal_decimal2Int64_m1_567,
	Decimal_decimalIncr_m1_568,
	Decimal_string2decimal_m1_569,
	Decimal_decimalSetExponent_m1_570,
	Decimal_decimal2double_m1_571,
	Decimal_decimalFloorAndTrunc_m1_572,
	Decimal_decimalMult_m1_573,
	Decimal_decimalDiv_m1_574,
	Decimal_decimalCompare_m1_575,
	Decimal_op_Increment_m1_576,
	Decimal_op_Subtraction_m1_577,
	Decimal_op_Multiply_m1_578,
	Decimal_op_Division_m1_579,
	Decimal_op_Explicit_m1_580,
	Decimal_op_Explicit_m1_581,
	Decimal_op_Explicit_m1_582,
	Decimal_op_Explicit_m1_583,
	Decimal_op_Explicit_m1_584,
	Decimal_op_Explicit_m1_585,
	Decimal_op_Explicit_m1_586,
	Decimal_op_Explicit_m1_587,
	Decimal_op_Implicit_m1_588,
	Decimal_op_Implicit_m1_589,
	Decimal_op_Implicit_m1_590,
	Decimal_op_Implicit_m1_591,
	Decimal_op_Implicit_m1_592,
	Decimal_op_Implicit_m1_593,
	Decimal_op_Implicit_m1_594,
	Decimal_op_Implicit_m1_595,
	Decimal_op_Explicit_m1_596,
	Decimal_op_Explicit_m1_597,
	Decimal_op_Explicit_m1_598,
	Decimal_op_Explicit_m1_599,
	Decimal_op_Inequality_m1_600,
	Decimal_op_Equality_m1_601,
	Decimal_op_GreaterThan_m1_602,
	Decimal_op_LessThan_m1_603,
	Boolean__cctor_m1_604,
	Boolean_System_IConvertible_ToType_m1_605,
	Boolean_System_IConvertible_ToBoolean_m1_606,
	Boolean_System_IConvertible_ToByte_m1_607,
	Boolean_System_IConvertible_ToChar_m1_608,
	Boolean_System_IConvertible_ToDateTime_m1_609,
	Boolean_System_IConvertible_ToDecimal_m1_610,
	Boolean_System_IConvertible_ToDouble_m1_611,
	Boolean_System_IConvertible_ToInt16_m1_612,
	Boolean_System_IConvertible_ToInt32_m1_613,
	Boolean_System_IConvertible_ToInt64_m1_614,
	Boolean_System_IConvertible_ToSByte_m1_615,
	Boolean_System_IConvertible_ToSingle_m1_616,
	Boolean_System_IConvertible_ToUInt16_m1_617,
	Boolean_System_IConvertible_ToUInt32_m1_618,
	Boolean_System_IConvertible_ToUInt64_m1_619,
	Boolean_CompareTo_m1_620,
	Boolean_Equals_m1_621,
	Boolean_CompareTo_m1_622,
	Boolean_Equals_m1_623,
	Boolean_GetHashCode_m1_624,
	Boolean_Parse_m1_625,
	Boolean_ToString_m1_626,
	Boolean_ToString_m1_627,
	IntPtr__ctor_m1_628,
	IntPtr__ctor_m1_629,
	IntPtr__ctor_m1_630,
	IntPtr__ctor_m1_631,
	IntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m1_632,
	IntPtr_get_Size_m1_633,
	IntPtr_Equals_m1_634,
	IntPtr_GetHashCode_m1_635,
	IntPtr_ToInt64_m1_636,
	IntPtr_ToPointer_m1_637,
	IntPtr_ToString_m1_638,
	IntPtr_ToString_m1_639,
	IntPtr_op_Equality_m1_640,
	IntPtr_op_Inequality_m1_641,
	IntPtr_op_Explicit_m1_642,
	IntPtr_op_Explicit_m1_643,
	IntPtr_op_Explicit_m1_644,
	IntPtr_op_Explicit_m1_645,
	IntPtr_op_Explicit_m1_646,
	UIntPtr__ctor_m1_647,
	UIntPtr__ctor_m1_648,
	UIntPtr__ctor_m1_649,
	UIntPtr__cctor_m1_650,
	UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m1_651,
	UIntPtr_Equals_m1_652,
	UIntPtr_GetHashCode_m1_653,
	UIntPtr_ToUInt32_m1_654,
	UIntPtr_ToUInt64_m1_655,
	UIntPtr_ToPointer_m1_656,
	UIntPtr_ToString_m1_657,
	UIntPtr_get_Size_m1_658,
	UIntPtr_op_Equality_m1_659,
	UIntPtr_op_Inequality_m1_660,
	UIntPtr_op_Explicit_m1_661,
	UIntPtr_op_Explicit_m1_662,
	UIntPtr_op_Explicit_m1_663,
	UIntPtr_op_Explicit_m1_664,
	UIntPtr_op_Explicit_m1_665,
	UIntPtr_op_Explicit_m1_666,
	MulticastDelegate_GetObjectData_m1_667,
	MulticastDelegate_Equals_m1_668,
	MulticastDelegate_GetHashCode_m1_669,
	MulticastDelegate_GetInvocationList_m1_670,
	MulticastDelegate_CombineImpl_m1_671,
	MulticastDelegate_BaseEquals_m1_672,
	MulticastDelegate_KPM_m1_673,
	MulticastDelegate_RemoveImpl_m1_674,
	Delegate_get_Method_m1_675,
	Delegate_get_Target_m1_676,
	Delegate_CreateDelegate_internal_m1_677,
	Delegate_SetMulticastInvoke_m1_678,
	Delegate_arg_type_match_m1_679,
	Delegate_return_type_match_m1_680,
	Delegate_CreateDelegate_m1_681,
	Delegate_CreateDelegate_m1_682,
	Delegate_CreateDelegate_m1_683,
	Delegate_CreateDelegate_m1_684,
	Delegate_GetCandidateMethod_m1_685,
	Delegate_CreateDelegate_m1_686,
	Delegate_CreateDelegate_m1_687,
	Delegate_CreateDelegate_m1_688,
	Delegate_CreateDelegate_m1_689,
	Delegate_Clone_m1_690,
	Delegate_Equals_m1_691,
	Delegate_GetHashCode_m1_692,
	Delegate_GetObjectData_m1_693,
	Delegate_GetInvocationList_m1_694,
	Delegate_Combine_m1_695,
	Delegate_Combine_m1_696,
	Delegate_CombineImpl_m1_697,
	Delegate_Remove_m1_698,
	Delegate_RemoveImpl_m1_699,
	Enum__ctor_m1_700,
	Enum__cctor_m1_701,
	Enum_System_IConvertible_ToBoolean_m1_702,
	Enum_System_IConvertible_ToByte_m1_703,
	Enum_System_IConvertible_ToChar_m1_704,
	Enum_System_IConvertible_ToDateTime_m1_705,
	Enum_System_IConvertible_ToDecimal_m1_706,
	Enum_System_IConvertible_ToDouble_m1_707,
	Enum_System_IConvertible_ToInt16_m1_708,
	Enum_System_IConvertible_ToInt32_m1_709,
	Enum_System_IConvertible_ToInt64_m1_710,
	Enum_System_IConvertible_ToSByte_m1_711,
	Enum_System_IConvertible_ToSingle_m1_712,
	Enum_System_IConvertible_ToType_m1_713,
	Enum_System_IConvertible_ToUInt16_m1_714,
	Enum_System_IConvertible_ToUInt32_m1_715,
	Enum_System_IConvertible_ToUInt64_m1_716,
	Enum_GetTypeCode_m1_717,
	Enum_get_value_m1_718,
	Enum_get_Value_m1_719,
	Enum_GetValues_m1_720,
	Enum_FindPosition_m1_721,
	Enum_GetName_m1_722,
	Enum_IsDefined_m1_723,
	Enum_get_underlying_type_m1_724,
	Enum_GetUnderlyingType_m1_725,
	Enum_Parse_m1_726,
	Enum_FindName_m1_727,
	Enum_GetValue_m1_728,
	Enum_Parse_m1_729,
	Enum_compare_value_to_m1_730,
	Enum_CompareTo_m1_731,
	Enum_ToString_m1_732,
	Enum_ToString_m1_733,
	Enum_ToString_m1_734,
	Enum_ToString_m1_735,
	Enum_ToObject_m1_736,
	Enum_ToObject_m1_737,
	Enum_ToObject_m1_738,
	Enum_ToObject_m1_739,
	Enum_ToObject_m1_740,
	Enum_ToObject_m1_741,
	Enum_ToObject_m1_742,
	Enum_ToObject_m1_743,
	Enum_ToObject_m1_744,
	Enum_Equals_m1_745,
	Enum_get_hashcode_m1_746,
	Enum_GetHashCode_m1_747,
	Enum_FormatSpecifier_X_m1_748,
	Enum_FormatFlags_m1_749,
	Enum_Format_m1_750,
	SimpleEnumerator__ctor_m1_751,
	SimpleEnumerator_get_Current_m1_752,
	SimpleEnumerator_MoveNext_m1_753,
	SimpleEnumerator_Reset_m1_754,
	SimpleEnumerator_Clone_m1_755,
	Swapper__ctor_m1_756,
	Swapper_Invoke_m1_757,
	Swapper_BeginInvoke_m1_758,
	Swapper_EndInvoke_m1_759,
	Array__ctor_m1_760,
	Array_System_Collections_IList_get_Item_m1_761,
	Array_System_Collections_IList_set_Item_m1_762,
	Array_System_Collections_IList_Add_m1_763,
	Array_System_Collections_IList_Clear_m1_764,
	Array_System_Collections_IList_Contains_m1_765,
	Array_System_Collections_IList_IndexOf_m1_766,
	Array_System_Collections_IList_Insert_m1_767,
	Array_System_Collections_IList_Remove_m1_768,
	Array_System_Collections_IList_RemoveAt_m1_769,
	Array_System_Collections_ICollection_get_Count_m1_770,
	Array_InternalArray__ICollection_get_Count_m1_771,
	Array_InternalArray__ICollection_get_IsReadOnly_m1_772,
	Array_InternalArray__ICollection_Clear_m1_773,
	Array_InternalArray__RemoveAt_m1_774,
	Array_get_Length_m1_775,
	Array_get_LongLength_m1_776,
	Array_get_Rank_m1_777,
	Array_GetRank_m1_778,
	Array_GetLength_m1_779,
	Array_GetLongLength_m1_780,
	Array_GetLowerBound_m1_781,
	Array_GetValue_m1_782,
	Array_SetValue_m1_783,
	Array_GetValueImpl_m1_784,
	Array_SetValueImpl_m1_785,
	Array_FastCopy_m1_786,
	Array_CreateInstanceImpl_m1_787,
	Array_get_IsSynchronized_m1_788,
	Array_get_SyncRoot_m1_789,
	Array_get_IsFixedSize_m1_790,
	Array_get_IsReadOnly_m1_791,
	Array_GetEnumerator_m1_792,
	Array_GetUpperBound_m1_793,
	Array_GetValue_m1_794,
	Array_GetValue_m1_795,
	Array_GetValue_m1_796,
	Array_GetValue_m1_797,
	Array_GetValue_m1_798,
	Array_GetValue_m1_799,
	Array_SetValue_m1_800,
	Array_SetValue_m1_801,
	Array_SetValue_m1_802,
	Array_SetValue_m1_803,
	Array_SetValue_m1_804,
	Array_SetValue_m1_805,
	Array_CreateInstance_m1_806,
	Array_CreateInstance_m1_807,
	Array_CreateInstance_m1_808,
	Array_CreateInstance_m1_809,
	Array_CreateInstance_m1_810,
	Array_GetIntArray_m1_811,
	Array_CreateInstance_m1_812,
	Array_GetValue_m1_813,
	Array_SetValue_m1_814,
	Array_BinarySearch_m1_815,
	Array_BinarySearch_m1_816,
	Array_BinarySearch_m1_817,
	Array_BinarySearch_m1_818,
	Array_DoBinarySearch_m1_819,
	Array_Clear_m1_820,
	Array_ClearInternal_m1_821,
	Array_Clone_m1_822,
	Array_Copy_m1_823,
	Array_Copy_m1_824,
	Array_Copy_m1_825,
	Array_Copy_m1_826,
	Array_IndexOf_m1_827,
	Array_IndexOf_m1_828,
	Array_IndexOf_m1_829,
	Array_Initialize_m1_830,
	Array_LastIndexOf_m1_831,
	Array_LastIndexOf_m1_832,
	Array_LastIndexOf_m1_833,
	Array_get_swapper_m1_834,
	Array_Reverse_m1_835,
	Array_Reverse_m1_836,
	Array_Sort_m1_837,
	Array_Sort_m1_838,
	Array_Sort_m1_839,
	Array_Sort_m1_840,
	Array_Sort_m1_841,
	Array_Sort_m1_842,
	Array_Sort_m1_843,
	Array_Sort_m1_844,
	Array_int_swapper_m1_845,
	Array_obj_swapper_m1_846,
	Array_slow_swapper_m1_847,
	Array_double_swapper_m1_848,
	Array_new_gap_m1_849,
	Array_combsort_m1_850,
	Array_combsort_m1_851,
	Array_combsort_m1_852,
	Array_qsort_m1_853,
	Array_swap_m1_854,
	Array_compare_m1_855,
	Array_CopyTo_m1_856,
	Array_CopyTo_m1_857,
	Array_ConstrainedCopy_m1_858,
	Type__ctor_m1_859,
	Type__cctor_m1_860,
	Type_FilterName_impl_m1_861,
	Type_FilterNameIgnoreCase_impl_m1_862,
	Type_FilterAttribute_impl_m1_863,
	Type_get_Attributes_m1_864,
	Type_get_DeclaringType_m1_865,
	Type_get_HasElementType_m1_866,
	Type_get_IsAbstract_m1_867,
	Type_get_IsArray_m1_868,
	Type_get_IsByRef_m1_869,
	Type_get_IsClass_m1_870,
	Type_get_IsContextful_m1_871,
	Type_get_IsEnum_m1_872,
	Type_get_IsExplicitLayout_m1_873,
	Type_get_IsInterface_m1_874,
	Type_get_IsMarshalByRef_m1_875,
	Type_get_IsPointer_m1_876,
	Type_get_IsPrimitive_m1_877,
	Type_get_IsSealed_m1_878,
	Type_get_IsSerializable_m1_879,
	Type_get_IsValueType_m1_880,
	Type_get_MemberType_m1_881,
	Type_get_ReflectedType_m1_882,
	Type_get_TypeHandle_m1_883,
	Type_Equals_m1_884,
	Type_Equals_m1_885,
	Type_EqualsInternal_m1_886,
	Type_internal_from_handle_m1_887,
	Type_internal_from_name_m1_888,
	Type_GetType_m1_889,
	Type_GetType_m1_890,
	Type_GetTypeCodeInternal_m1_891,
	Type_GetTypeCode_m1_892,
	Type_GetTypeFromHandle_m1_893,
	Type_GetTypeHandle_m1_894,
	Type_type_is_subtype_of_m1_895,
	Type_type_is_assignable_from_m1_896,
	Type_IsSubclassOf_m1_897,
	Type_IsAssignableFrom_m1_898,
	Type_IsInstanceOfType_m1_899,
	Type_GetHashCode_m1_900,
	Type_GetMethod_m1_901,
	Type_GetMethod_m1_902,
	Type_GetMethod_m1_903,
	Type_GetMethod_m1_904,
	Type_GetProperty_m1_905,
	Type_GetProperty_m1_906,
	Type_GetProperty_m1_907,
	Type_GetProperty_m1_908,
	Type_IsArrayImpl_m1_909,
	Type_IsValueTypeImpl_m1_910,
	Type_IsContextfulImpl_m1_911,
	Type_IsMarshalByRefImpl_m1_912,
	Type_GetConstructor_m1_913,
	Type_GetConstructor_m1_914,
	Type_GetConstructor_m1_915,
	Type_ToString_m1_916,
	Type_get_IsSystemType_m1_917,
	Type_GetGenericArguments_m1_918,
	Type_get_ContainsGenericParameters_m1_919,
	Type_get_IsGenericTypeDefinition_m1_920,
	Type_GetGenericTypeDefinition_impl_m1_921,
	Type_GetGenericTypeDefinition_m1_922,
	Type_get_IsGenericType_m1_923,
	Type_MakeGenericType_m1_924,
	Type_MakeGenericType_m1_925,
	Type_get_IsGenericParameter_m1_926,
	Type_get_IsNested_m1_927,
	Type_GetPseudoCustomAttributes_m1_928,
	MemberInfo__ctor_m1_929,
	MemberInfo_get_Module_m1_930,
	Exception__ctor_m1_931,
	Exception__ctor_m1_932,
	Exception__ctor_m1_933,
	Exception__ctor_m1_934,
	Exception_get_InnerException_m1_935,
	Exception_set_HResult_m1_936,
	Exception_get_ClassName_m1_937,
	Exception_get_Message_m1_938,
	Exception_get_Source_m1_939,
	Exception_get_StackTrace_m1_940,
	Exception_GetObjectData_m1_941,
	Exception_ToString_m1_942,
	Exception_GetFullNameForStackTrace_m1_943,
	Exception_GetType_m1_944,
	RuntimeFieldHandle__ctor_m1_945,
	RuntimeFieldHandle_get_Value_m1_946,
	RuntimeFieldHandle_GetObjectData_m1_947,
	RuntimeFieldHandle_Equals_m1_948,
	RuntimeFieldHandle_GetHashCode_m1_949,
	RuntimeTypeHandle__ctor_m1_950,
	RuntimeTypeHandle_get_Value_m1_951,
	RuntimeTypeHandle_GetObjectData_m1_952,
	RuntimeTypeHandle_Equals_m1_953,
	RuntimeTypeHandle_GetHashCode_m1_954,
	ParamArrayAttribute__ctor_m1_955,
	OutAttribute__ctor_m1_956,
	ObsoleteAttribute__ctor_m1_957,
	ObsoleteAttribute__ctor_m1_958,
	ObsoleteAttribute__ctor_m1_959,
	DllImportAttribute__ctor_m1_960,
	DllImportAttribute_get_Value_m1_961,
	MarshalAsAttribute__ctor_m1_962,
	InAttribute__ctor_m1_963,
	ConditionalAttribute__ctor_m1_964,
	GuidAttribute__ctor_m1_965,
	ComImportAttribute__ctor_m1_966,
	OptionalAttribute__ctor_m1_967,
	CompilerGeneratedAttribute__ctor_m1_968,
	InternalsVisibleToAttribute__ctor_m1_969,
	RuntimeCompatibilityAttribute__ctor_m1_970,
	RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1_971,
	DebuggerHiddenAttribute__ctor_m1_972,
	DefaultMemberAttribute__ctor_m1_973,
	DefaultMemberAttribute_get_MemberName_m1_974,
	DecimalConstantAttribute__ctor_m1_975,
	FieldOffsetAttribute__ctor_m1_976,
	AsyncCallback__ctor_m1_977,
	AsyncCallback_Invoke_m1_978,
	AsyncCallback_BeginInvoke_m1_979,
	AsyncCallback_EndInvoke_m1_980,
	TypedReference_Equals_m1_981,
	TypedReference_GetHashCode_m1_982,
	ArgIterator_Equals_m1_983,
	ArgIterator_GetHashCode_m1_984,
	MarshalByRefObject__ctor_m1_985,
	MarshalByRefObject_get_ObjectIdentity_m1_986,
	RuntimeHelpers_InitializeArray_m1_987,
	RuntimeHelpers_InitializeArray_m1_988,
	RuntimeHelpers_get_OffsetToStringData_m1_989,
	Locale_GetText_m1_990,
	Locale_GetText_m1_991,
	MonoTODOAttribute__ctor_m1_992,
	MonoTODOAttribute__ctor_m1_993,
	MonoDocumentationNoteAttribute__ctor_m1_994,
	SafeHandleZeroOrMinusOneIsInvalid__ctor_m1_995,
	SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m1_996,
	SafeWaitHandle__ctor_m1_997,
	SafeWaitHandle_ReleaseHandle_m1_998,
	TableRange__ctor_m1_999,
	CodePointIndexer__ctor_m1_1000,
	CodePointIndexer_ToIndex_m1_1001,
	TailoringInfo__ctor_m1_1002,
	Contraction__ctor_m1_1003,
	ContractionComparer__ctor_m1_1004,
	ContractionComparer__cctor_m1_1005,
	ContractionComparer_Compare_m1_1006,
	Level2Map__ctor_m1_1007,
	Level2MapComparer__ctor_m1_1008,
	Level2MapComparer__cctor_m1_1009,
	Level2MapComparer_Compare_m1_1010,
	MSCompatUnicodeTable__cctor_m1_1011,
	MSCompatUnicodeTable_GetTailoringInfo_m1_1012,
	MSCompatUnicodeTable_BuildTailoringTables_m1_1013,
	MSCompatUnicodeTable_SetCJKReferences_m1_1014,
	MSCompatUnicodeTable_Category_m1_1015,
	MSCompatUnicodeTable_Level1_m1_1016,
	MSCompatUnicodeTable_Level2_m1_1017,
	MSCompatUnicodeTable_Level3_m1_1018,
	MSCompatUnicodeTable_IsIgnorable_m1_1019,
	MSCompatUnicodeTable_IsIgnorableNonSpacing_m1_1020,
	MSCompatUnicodeTable_ToKanaTypeInsensitive_m1_1021,
	MSCompatUnicodeTable_ToWidthCompat_m1_1022,
	MSCompatUnicodeTable_HasSpecialWeight_m1_1023,
	MSCompatUnicodeTable_IsHalfWidthKana_m1_1024,
	MSCompatUnicodeTable_IsHiragana_m1_1025,
	MSCompatUnicodeTable_IsJapaneseSmallLetter_m1_1026,
	MSCompatUnicodeTable_get_IsReady_m1_1027,
	MSCompatUnicodeTable_GetResource_m1_1028,
	MSCompatUnicodeTable_UInt32FromBytePtr_m1_1029,
	MSCompatUnicodeTable_FillCJK_m1_1030,
	MSCompatUnicodeTable_FillCJKCore_m1_1031,
	MSCompatUnicodeTableUtil__cctor_m1_1032,
	Context__ctor_m1_1033,
	PreviousInfo__ctor_m1_1034,
	SimpleCollator__ctor_m1_1035,
	SimpleCollator__cctor_m1_1036,
	SimpleCollator_SetCJKTable_m1_1037,
	SimpleCollator_GetNeutralCulture_m1_1038,
	SimpleCollator_Category_m1_1039,
	SimpleCollator_Level1_m1_1040,
	SimpleCollator_Level2_m1_1041,
	SimpleCollator_IsHalfKana_m1_1042,
	SimpleCollator_GetContraction_m1_1043,
	SimpleCollator_GetContraction_m1_1044,
	SimpleCollator_GetTailContraction_m1_1045,
	SimpleCollator_GetTailContraction_m1_1046,
	SimpleCollator_FilterOptions_m1_1047,
	SimpleCollator_GetExtenderType_m1_1048,
	SimpleCollator_ToDashTypeValue_m1_1049,
	SimpleCollator_FilterExtender_m1_1050,
	SimpleCollator_IsIgnorable_m1_1051,
	SimpleCollator_IsSafe_m1_1052,
	SimpleCollator_GetSortKey_m1_1053,
	SimpleCollator_GetSortKey_m1_1054,
	SimpleCollator_GetSortKey_m1_1055,
	SimpleCollator_FillSortKeyRaw_m1_1056,
	SimpleCollator_FillSurrogateSortKeyRaw_m1_1057,
	SimpleCollator_CompareOrdinal_m1_1058,
	SimpleCollator_CompareQuick_m1_1059,
	SimpleCollator_CompareOrdinalIgnoreCase_m1_1060,
	SimpleCollator_Compare_m1_1061,
	SimpleCollator_ClearBuffer_m1_1062,
	SimpleCollator_QuickCheckPossible_m1_1063,
	SimpleCollator_CompareInternal_m1_1064,
	SimpleCollator_CompareFlagPair_m1_1065,
	SimpleCollator_IsPrefix_m1_1066,
	SimpleCollator_IsPrefix_m1_1067,
	SimpleCollator_IsPrefix_m1_1068,
	SimpleCollator_IsSuffix_m1_1069,
	SimpleCollator_IsSuffix_m1_1070,
	SimpleCollator_QuickIndexOf_m1_1071,
	SimpleCollator_IndexOf_m1_1072,
	SimpleCollator_IndexOfOrdinal_m1_1073,
	SimpleCollator_IndexOfOrdinalIgnoreCase_m1_1074,
	SimpleCollator_IndexOfSortKey_m1_1075,
	SimpleCollator_IndexOf_m1_1076,
	SimpleCollator_LastIndexOf_m1_1077,
	SimpleCollator_LastIndexOfOrdinal_m1_1078,
	SimpleCollator_LastIndexOfOrdinalIgnoreCase_m1_1079,
	SimpleCollator_LastIndexOfSortKey_m1_1080,
	SimpleCollator_LastIndexOf_m1_1081,
	SimpleCollator_MatchesForward_m1_1082,
	SimpleCollator_MatchesForwardCore_m1_1083,
	SimpleCollator_MatchesPrimitive_m1_1084,
	SimpleCollator_MatchesBackward_m1_1085,
	SimpleCollator_MatchesBackwardCore_m1_1086,
	SortKey__ctor_m1_1087,
	SortKey__ctor_m1_1088,
	SortKey_Compare_m1_1089,
	SortKey_get_OriginalString_m1_1090,
	SortKey_get_KeyData_m1_1091,
	SortKey_Equals_m1_1092,
	SortKey_GetHashCode_m1_1093,
	SortKey_ToString_m1_1094,
	SortKeyBuffer__ctor_m1_1095,
	SortKeyBuffer_Reset_m1_1096,
	SortKeyBuffer_Initialize_m1_1097,
	SortKeyBuffer_AppendCJKExtension_m1_1098,
	SortKeyBuffer_AppendKana_m1_1099,
	SortKeyBuffer_AppendNormal_m1_1100,
	SortKeyBuffer_AppendLevel5_m1_1101,
	SortKeyBuffer_AppendBufferPrimitive_m1_1102,
	SortKeyBuffer_GetResultAndReset_m1_1103,
	SortKeyBuffer_GetOptimizedLength_m1_1104,
	SortKeyBuffer_GetResult_m1_1105,
	PrimeGeneratorBase__ctor_m1_1106,
	PrimeGeneratorBase_get_Confidence_m1_1107,
	PrimeGeneratorBase_get_PrimalityTest_m1_1108,
	PrimeGeneratorBase_get_TrialDivisionBounds_m1_1109,
	SequentialSearchPrimeGeneratorBase__ctor_m1_1110,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m1_1111,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m1_1112,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m1_1113,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m1_1114,
	PrimalityTests_GetSPPRounds_m1_1115,
	PrimalityTests_Test_m1_1116,
	PrimalityTests_RabinMillerTest_m1_1117,
	PrimalityTests_SmallPrimeSppTest_m1_1118,
	ModulusRing__ctor_m1_1119,
	ModulusRing_BarrettReduction_m1_1120,
	ModulusRing_Multiply_m1_1121,
	ModulusRing_Difference_m1_1122,
	ModulusRing_Pow_m1_1123,
	ModulusRing_Pow_m1_1124,
	Kernel_AddSameSign_m1_1125,
	Kernel_Subtract_m1_1126,
	Kernel_MinusEq_m1_1127,
	Kernel_PlusEq_m1_1128,
	Kernel_Compare_m1_1129,
	Kernel_SingleByteDivideInPlace_m1_1130,
	Kernel_DwordMod_m1_1131,
	Kernel_DwordDivMod_m1_1132,
	Kernel_multiByteDivide_m1_1133,
	Kernel_LeftShift_m1_1134,
	Kernel_RightShift_m1_1135,
	Kernel_MultiplyByDword_m1_1136,
	Kernel_Multiply_m1_1137,
	Kernel_MultiplyMod2p32pmod_m1_1138,
	Kernel_modInverse_m1_1139,
	Kernel_modInverse_m1_1140,
	BigInteger__ctor_m1_1141,
	BigInteger__ctor_m1_1142,
	BigInteger__ctor_m1_1143,
	BigInteger__ctor_m1_1144,
	BigInteger__ctor_m1_1145,
	BigInteger__cctor_m1_1146,
	BigInteger_get_Rng_m1_1147,
	BigInteger_GenerateRandom_m1_1148,
	BigInteger_GenerateRandom_m1_1149,
	BigInteger_Randomize_m1_1150,
	BigInteger_Randomize_m1_1151,
	BigInteger_BitCount_m1_1152,
	BigInteger_TestBit_m1_1153,
	BigInteger_TestBit_m1_1154,
	BigInteger_SetBit_m1_1155,
	BigInteger_SetBit_m1_1156,
	BigInteger_LowestSetBit_m1_1157,
	BigInteger_GetBytes_m1_1158,
	BigInteger_ToString_m1_1159,
	BigInteger_ToString_m1_1160,
	BigInteger_Normalize_m1_1161,
	BigInteger_Clear_m1_1162,
	BigInteger_GetHashCode_m1_1163,
	BigInteger_ToString_m1_1164,
	BigInteger_Equals_m1_1165,
	BigInteger_ModInverse_m1_1166,
	BigInteger_ModPow_m1_1167,
	BigInteger_IsProbablePrime_m1_1168,
	BigInteger_GeneratePseudoPrime_m1_1169,
	BigInteger_Incr2_m1_1170,
	BigInteger_op_Implicit_m1_1171,
	BigInteger_op_Implicit_m1_1172,
	BigInteger_op_Addition_m1_1173,
	BigInteger_op_Subtraction_m1_1174,
	BigInteger_op_Modulus_m1_1175,
	BigInteger_op_Modulus_m1_1176,
	BigInteger_op_Division_m1_1177,
	BigInteger_op_Multiply_m1_1178,
	BigInteger_op_Multiply_m1_1179,
	BigInteger_op_LeftShift_m1_1180,
	BigInteger_op_RightShift_m1_1181,
	BigInteger_op_Equality_m1_1182,
	BigInteger_op_Inequality_m1_1183,
	BigInteger_op_Equality_m1_1184,
	BigInteger_op_Inequality_m1_1185,
	BigInteger_op_GreaterThan_m1_1186,
	BigInteger_op_LessThan_m1_1187,
	BigInteger_op_GreaterThanOrEqual_m1_1188,
	BigInteger_op_LessThanOrEqual_m1_1189,
	CryptoConvert_ToInt32LE_m1_1190,
	CryptoConvert_ToUInt32LE_m1_1191,
	CryptoConvert_GetBytesLE_m1_1192,
	CryptoConvert_ToCapiPrivateKeyBlob_m1_1193,
	CryptoConvert_FromCapiPublicKeyBlob_m1_1194,
	CryptoConvert_FromCapiPublicKeyBlob_m1_1195,
	CryptoConvert_ToCapiPublicKeyBlob_m1_1196,
	CryptoConvert_ToCapiKeyBlob_m1_1197,
	KeyBuilder_get_Rng_m1_1198,
	KeyBuilder_Key_m1_1199,
	KeyBuilder_IV_m1_1200,
	BlockProcessor__ctor_m1_1201,
	BlockProcessor_Finalize_m1_1202,
	BlockProcessor_Initialize_m1_1203,
	BlockProcessor_Core_m1_1204,
	BlockProcessor_Core_m1_1205,
	BlockProcessor_Final_m1_1206,
	KeyGeneratedEventHandler__ctor_m1_1207,
	KeyGeneratedEventHandler_Invoke_m1_1208,
	KeyGeneratedEventHandler_BeginInvoke_m1_1209,
	KeyGeneratedEventHandler_EndInvoke_m1_1210,
	DSAManaged__ctor_m1_1211,
	DSAManaged_add_KeyGenerated_m1_1212,
	DSAManaged_remove_KeyGenerated_m1_1213,
	DSAManaged_Finalize_m1_1214,
	DSAManaged_Generate_m1_1215,
	DSAManaged_GenerateKeyPair_m1_1216,
	DSAManaged_add_m1_1217,
	DSAManaged_GenerateParams_m1_1218,
	DSAManaged_get_Random_m1_1219,
	DSAManaged_get_KeySize_m1_1220,
	DSAManaged_get_PublicOnly_m1_1221,
	DSAManaged_NormalizeArray_m1_1222,
	DSAManaged_ExportParameters_m1_1223,
	DSAManaged_ImportParameters_m1_1224,
	DSAManaged_CreateSignature_m1_1225,
	DSAManaged_VerifySignature_m1_1226,
	DSAManaged_Dispose_m1_1227,
	KeyPairPersistence__ctor_m1_1228,
	KeyPairPersistence__ctor_m1_1229,
	KeyPairPersistence__cctor_m1_1230,
	KeyPairPersistence_get_Filename_m1_1231,
	KeyPairPersistence_get_KeyValue_m1_1232,
	KeyPairPersistence_set_KeyValue_m1_1233,
	KeyPairPersistence_Load_m1_1234,
	KeyPairPersistence_Save_m1_1235,
	KeyPairPersistence_Remove_m1_1236,
	KeyPairPersistence_get_UserPath_m1_1237,
	KeyPairPersistence_get_MachinePath_m1_1238,
	KeyPairPersistence__CanSecure_m1_1239,
	KeyPairPersistence__ProtectUser_m1_1240,
	KeyPairPersistence__ProtectMachine_m1_1241,
	KeyPairPersistence__IsUserProtected_m1_1242,
	KeyPairPersistence__IsMachineProtected_m1_1243,
	KeyPairPersistence_CanSecure_m1_1244,
	KeyPairPersistence_ProtectUser_m1_1245,
	KeyPairPersistence_ProtectMachine_m1_1246,
	KeyPairPersistence_IsUserProtected_m1_1247,
	KeyPairPersistence_IsMachineProtected_m1_1248,
	KeyPairPersistence_get_CanChange_m1_1249,
	KeyPairPersistence_get_UseDefaultKeyContainer_m1_1250,
	KeyPairPersistence_get_UseMachineKeyStore_m1_1251,
	KeyPairPersistence_get_ContainerName_m1_1252,
	KeyPairPersistence_Copy_m1_1253,
	KeyPairPersistence_FromXml_m1_1254,
	KeyPairPersistence_ToXml_m1_1255,
	MACAlgorithm__ctor_m1_1256,
	MACAlgorithm_Initialize_m1_1257,
	MACAlgorithm_Core_m1_1258,
	MACAlgorithm_Final_m1_1259,
	PKCS1__cctor_m1_1260,
	PKCS1_Compare_m1_1261,
	PKCS1_I2OSP_m1_1262,
	PKCS1_OS2IP_m1_1263,
	PKCS1_RSAEP_m1_1264,
	PKCS1_RSASP1_m1_1265,
	PKCS1_RSAVP1_m1_1266,
	PKCS1_Encrypt_v15_m1_1267,
	PKCS1_Sign_v15_m1_1268,
	PKCS1_Verify_v15_m1_1269,
	PKCS1_Verify_v15_m1_1270,
	PKCS1_Encode_v15_m1_1271,
	PrivateKeyInfo__ctor_m1_1272,
	PrivateKeyInfo__ctor_m1_1273,
	PrivateKeyInfo_get_PrivateKey_m1_1274,
	PrivateKeyInfo_Decode_m1_1275,
	PrivateKeyInfo_RemoveLeadingZero_m1_1276,
	PrivateKeyInfo_Normalize_m1_1277,
	PrivateKeyInfo_DecodeRSA_m1_1278,
	PrivateKeyInfo_DecodeDSA_m1_1279,
	EncryptedPrivateKeyInfo__ctor_m1_1280,
	EncryptedPrivateKeyInfo__ctor_m1_1281,
	EncryptedPrivateKeyInfo_get_Algorithm_m1_1282,
	EncryptedPrivateKeyInfo_get_EncryptedData_m1_1283,
	EncryptedPrivateKeyInfo_get_Salt_m1_1284,
	EncryptedPrivateKeyInfo_get_IterationCount_m1_1285,
	EncryptedPrivateKeyInfo_Decode_m1_1286,
	KeyGeneratedEventHandler__ctor_m1_1287,
	KeyGeneratedEventHandler_Invoke_m1_1288,
	KeyGeneratedEventHandler_BeginInvoke_m1_1289,
	KeyGeneratedEventHandler_EndInvoke_m1_1290,
	RSAManaged__ctor_m1_1291,
	RSAManaged_add_KeyGenerated_m1_1292,
	RSAManaged_remove_KeyGenerated_m1_1293,
	RSAManaged_Finalize_m1_1294,
	RSAManaged_GenerateKeyPair_m1_1295,
	RSAManaged_get_KeySize_m1_1296,
	RSAManaged_get_PublicOnly_m1_1297,
	RSAManaged_DecryptValue_m1_1298,
	RSAManaged_EncryptValue_m1_1299,
	RSAManaged_ExportParameters_m1_1300,
	RSAManaged_ImportParameters_m1_1301,
	RSAManaged_Dispose_m1_1302,
	RSAManaged_ToXmlString_m1_1303,
	RSAManaged_get_IsCrtPossible_m1_1304,
	RSAManaged_GetPaddedValue_m1_1305,
	SymmetricTransform__ctor_m1_1306,
	SymmetricTransform_System_IDisposable_Dispose_m1_1307,
	SymmetricTransform_Finalize_m1_1308,
	SymmetricTransform_Dispose_m1_1309,
	SymmetricTransform_get_CanReuseTransform_m1_1310,
	SymmetricTransform_Transform_m1_1311,
	SymmetricTransform_CBC_m1_1312,
	SymmetricTransform_CFB_m1_1313,
	SymmetricTransform_OFB_m1_1314,
	SymmetricTransform_CTS_m1_1315,
	SymmetricTransform_CheckInput_m1_1316,
	SymmetricTransform_TransformBlock_m1_1317,
	SymmetricTransform_get_KeepLastBlock_m1_1318,
	SymmetricTransform_InternalTransformBlock_m1_1319,
	SymmetricTransform_Random_m1_1320,
	SymmetricTransform_ThrowBadPaddingException_m1_1321,
	SymmetricTransform_FinalEncrypt_m1_1322,
	SymmetricTransform_FinalDecrypt_m1_1323,
	SymmetricTransform_TransformFinalBlock_m1_1324,
	SafeBag__ctor_m1_1325,
	SafeBag_get_BagOID_m1_1326,
	SafeBag_get_ASN1_m1_1327,
	DeriveBytes__ctor_m1_1328,
	DeriveBytes__cctor_m1_1329,
	DeriveBytes_set_HashName_m1_1330,
	DeriveBytes_set_IterationCount_m1_1331,
	DeriveBytes_set_Password_m1_1332,
	DeriveBytes_set_Salt_m1_1333,
	DeriveBytes_Adjust_m1_1334,
	DeriveBytes_Derive_m1_1335,
	DeriveBytes_DeriveKey_m1_1336,
	DeriveBytes_DeriveIV_m1_1337,
	DeriveBytes_DeriveMAC_m1_1338,
	PKCS12__ctor_m1_1339,
	PKCS12__ctor_m1_1340,
	PKCS12__ctor_m1_1341,
	PKCS12__cctor_m1_1342,
	PKCS12_Decode_m1_1343,
	PKCS12_Finalize_m1_1344,
	PKCS12_set_Password_m1_1345,
	PKCS12_get_IterationCount_m1_1346,
	PKCS12_set_IterationCount_m1_1347,
	PKCS12_get_Certificates_m1_1348,
	PKCS12_get_RNG_m1_1349,
	PKCS12_Compare_m1_1350,
	PKCS12_GetSymmetricAlgorithm_m1_1351,
	PKCS12_Decrypt_m1_1352,
	PKCS12_Decrypt_m1_1353,
	PKCS12_Encrypt_m1_1354,
	PKCS12_GetExistingParameters_m1_1355,
	PKCS12_AddPrivateKey_m1_1356,
	PKCS12_ReadSafeBag_m1_1357,
	PKCS12_CertificateSafeBag_m1_1358,
	PKCS12_MAC_m1_1359,
	PKCS12_GetBytes_m1_1360,
	PKCS12_EncryptedContentInfo_m1_1361,
	PKCS12_AddCertificate_m1_1362,
	PKCS12_AddCertificate_m1_1363,
	PKCS12_RemoveCertificate_m1_1364,
	PKCS12_RemoveCertificate_m1_1365,
	PKCS12_Clone_m1_1366,
	PKCS12_get_MaximumPasswordLength_m1_1367,
	X501__cctor_m1_1368,
	X501_ToString_m1_1369,
	X501_ToString_m1_1370,
	X501_AppendEntry_m1_1371,
	X509Certificate__ctor_m1_1372,
	X509Certificate__cctor_m1_1373,
	X509Certificate_Parse_m1_1374,
	X509Certificate_GetUnsignedBigInteger_m1_1375,
	X509Certificate_get_DSA_m1_1376,
	X509Certificate_get_IssuerName_m1_1377,
	X509Certificate_get_KeyAlgorithmParameters_m1_1378,
	X509Certificate_get_PublicKey_m1_1379,
	X509Certificate_get_RawData_m1_1380,
	X509Certificate_get_SubjectName_m1_1381,
	X509Certificate_get_ValidFrom_m1_1382,
	X509Certificate_get_ValidUntil_m1_1383,
	X509Certificate_GetIssuerName_m1_1384,
	X509Certificate_GetSubjectName_m1_1385,
	X509Certificate_GetObjectData_m1_1386,
	X509Certificate_PEM_m1_1387,
	X509CertificateEnumerator__ctor_m1_1388,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m1_1389,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m1_1390,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m1_1391,
	X509CertificateEnumerator_get_Current_m1_1392,
	X509CertificateEnumerator_MoveNext_m1_1393,
	X509CertificateEnumerator_Reset_m1_1394,
	X509CertificateCollection__ctor_m1_1395,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m1_1396,
	X509CertificateCollection_get_Item_m1_1397,
	X509CertificateCollection_Add_m1_1398,
	X509CertificateCollection_GetEnumerator_m1_1399,
	X509CertificateCollection_GetHashCode_m1_1400,
	X509Extension__ctor_m1_1401,
	X509Extension_Decode_m1_1402,
	X509Extension_Equals_m1_1403,
	X509Extension_GetHashCode_m1_1404,
	X509Extension_WriteLine_m1_1405,
	X509Extension_ToString_m1_1406,
	X509ExtensionCollection__ctor_m1_1407,
	X509ExtensionCollection__ctor_m1_1408,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m1_1409,
	ASN1__ctor_m1_1410,
	ASN1__ctor_m1_1411,
	ASN1__ctor_m1_1412,
	ASN1_get_Count_m1_1413,
	ASN1_get_Tag_m1_1414,
	ASN1_get_Length_m1_1415,
	ASN1_get_Value_m1_1416,
	ASN1_set_Value_m1_1417,
	ASN1_CompareArray_m1_1418,
	ASN1_CompareValue_m1_1419,
	ASN1_Add_m1_1420,
	ASN1_GetBytes_m1_1421,
	ASN1_Decode_m1_1422,
	ASN1_DecodeTLV_m1_1423,
	ASN1_get_Item_m1_1424,
	ASN1_Element_m1_1425,
	ASN1_ToString_m1_1426,
	ASN1Convert_FromInt32_m1_1427,
	ASN1Convert_FromOid_m1_1428,
	ASN1Convert_ToInt32_m1_1429,
	ASN1Convert_ToOid_m1_1430,
	ASN1Convert_ToDateTime_m1_1431,
	BitConverterLE_GetUIntBytes_m1_1432,
	BitConverterLE_GetBytes_m1_1433,
	BitConverterLE_UShortFromBytes_m1_1434,
	BitConverterLE_UIntFromBytes_m1_1435,
	BitConverterLE_ULongFromBytes_m1_1436,
	BitConverterLE_ToInt16_m1_1437,
	BitConverterLE_ToInt32_m1_1438,
	BitConverterLE_ToSingle_m1_1439,
	BitConverterLE_ToDouble_m1_1440,
	ContentInfo__ctor_m1_1441,
	ContentInfo__ctor_m1_1442,
	ContentInfo__ctor_m1_1443,
	ContentInfo__ctor_m1_1444,
	ContentInfo_get_ASN1_m1_1445,
	ContentInfo_get_Content_m1_1446,
	ContentInfo_set_Content_m1_1447,
	ContentInfo_get_ContentType_m1_1448,
	ContentInfo_set_ContentType_m1_1449,
	ContentInfo_GetASN1_m1_1450,
	EncryptedData__ctor_m1_1451,
	EncryptedData__ctor_m1_1452,
	EncryptedData_get_EncryptionAlgorithm_m1_1453,
	EncryptedData_get_EncryptedContent_m1_1454,
	StrongName__cctor_m1_1455,
	StrongName_get_PublicKey_m1_1456,
	StrongName_get_PublicKeyToken_m1_1457,
	StrongName_get_TokenAlgorithm_m1_1458,
	SecurityParser__ctor_m1_1459,
	SecurityParser_LoadXml_m1_1460,
	SecurityParser_ToXml_m1_1461,
	SecurityParser_OnStartParsing_m1_1462,
	SecurityParser_OnProcessingInstruction_m1_1463,
	SecurityParser_OnIgnorableWhitespace_m1_1464,
	SecurityParser_OnStartElement_m1_1465,
	SecurityParser_OnEndElement_m1_1466,
	SecurityParser_OnChars_m1_1467,
	SecurityParser_OnEndParsing_m1_1468,
	AttrListImpl__ctor_m1_1469,
	AttrListImpl_get_Length_m1_1470,
	AttrListImpl_GetName_m1_1471,
	AttrListImpl_GetValue_m1_1472,
	AttrListImpl_GetValue_m1_1473,
	AttrListImpl_get_Names_m1_1474,
	AttrListImpl_get_Values_m1_1475,
	AttrListImpl_Clear_m1_1476,
	AttrListImpl_Add_m1_1477,
	SmallXmlParser__ctor_m1_1478,
	SmallXmlParser_Error_m1_1479,
	SmallXmlParser_UnexpectedEndError_m1_1480,
	SmallXmlParser_IsNameChar_m1_1481,
	SmallXmlParser_IsWhitespace_m1_1482,
	SmallXmlParser_SkipWhitespaces_m1_1483,
	SmallXmlParser_HandleWhitespaces_m1_1484,
	SmallXmlParser_SkipWhitespaces_m1_1485,
	SmallXmlParser_Peek_m1_1486,
	SmallXmlParser_Read_m1_1487,
	SmallXmlParser_Expect_m1_1488,
	SmallXmlParser_ReadUntil_m1_1489,
	SmallXmlParser_ReadName_m1_1490,
	SmallXmlParser_Parse_m1_1491,
	SmallXmlParser_Cleanup_m1_1492,
	SmallXmlParser_ReadContent_m1_1493,
	SmallXmlParser_HandleBufferedContent_m1_1494,
	SmallXmlParser_ReadCharacters_m1_1495,
	SmallXmlParser_ReadReference_m1_1496,
	SmallXmlParser_ReadCharacterReference_m1_1497,
	SmallXmlParser_ReadAttribute_m1_1498,
	SmallXmlParser_ReadCDATASection_m1_1499,
	SmallXmlParser_ReadComment_m1_1500,
	SmallXmlParserException__ctor_m1_1501,
	Runtime_GetDisplayName_m1_1502,
	KeyNotFoundException__ctor_m1_1503,
	KeyNotFoundException__ctor_m1_1504,
	SimpleEnumerator__ctor_m1_1505,
	SimpleEnumerator__cctor_m1_1506,
	SimpleEnumerator_Clone_m1_1507,
	SimpleEnumerator_MoveNext_m1_1508,
	SimpleEnumerator_get_Current_m1_1509,
	SimpleEnumerator_Reset_m1_1510,
	ArrayListWrapper__ctor_m1_1511,
	ArrayListWrapper_get_Item_m1_1512,
	ArrayListWrapper_set_Item_m1_1513,
	ArrayListWrapper_get_Count_m1_1514,
	ArrayListWrapper_get_Capacity_m1_1515,
	ArrayListWrapper_set_Capacity_m1_1516,
	ArrayListWrapper_get_IsReadOnly_m1_1517,
	ArrayListWrapper_get_IsSynchronized_m1_1518,
	ArrayListWrapper_get_SyncRoot_m1_1519,
	ArrayListWrapper_Add_m1_1520,
	ArrayListWrapper_Clear_m1_1521,
	ArrayListWrapper_Contains_m1_1522,
	ArrayListWrapper_IndexOf_m1_1523,
	ArrayListWrapper_IndexOf_m1_1524,
	ArrayListWrapper_IndexOf_m1_1525,
	ArrayListWrapper_Insert_m1_1526,
	ArrayListWrapper_InsertRange_m1_1527,
	ArrayListWrapper_Remove_m1_1528,
	ArrayListWrapper_RemoveAt_m1_1529,
	ArrayListWrapper_CopyTo_m1_1530,
	ArrayListWrapper_CopyTo_m1_1531,
	ArrayListWrapper_CopyTo_m1_1532,
	ArrayListWrapper_GetEnumerator_m1_1533,
	ArrayListWrapper_AddRange_m1_1534,
	ArrayListWrapper_Clone_m1_1535,
	ArrayListWrapper_Sort_m1_1536,
	ArrayListWrapper_Sort_m1_1537,
	ArrayListWrapper_ToArray_m1_1538,
	ArrayListWrapper_ToArray_m1_1539,
	SynchronizedArrayListWrapper__ctor_m1_1540,
	SynchronizedArrayListWrapper_get_Item_m1_1541,
	SynchronizedArrayListWrapper_set_Item_m1_1542,
	SynchronizedArrayListWrapper_get_Count_m1_1543,
	SynchronizedArrayListWrapper_get_Capacity_m1_1544,
	SynchronizedArrayListWrapper_set_Capacity_m1_1545,
	SynchronizedArrayListWrapper_get_IsReadOnly_m1_1546,
	SynchronizedArrayListWrapper_get_IsSynchronized_m1_1547,
	SynchronizedArrayListWrapper_get_SyncRoot_m1_1548,
	SynchronizedArrayListWrapper_Add_m1_1549,
	SynchronizedArrayListWrapper_Clear_m1_1550,
	SynchronizedArrayListWrapper_Contains_m1_1551,
	SynchronizedArrayListWrapper_IndexOf_m1_1552,
	SynchronizedArrayListWrapper_IndexOf_m1_1553,
	SynchronizedArrayListWrapper_IndexOf_m1_1554,
	SynchronizedArrayListWrapper_Insert_m1_1555,
	SynchronizedArrayListWrapper_InsertRange_m1_1556,
	SynchronizedArrayListWrapper_Remove_m1_1557,
	SynchronizedArrayListWrapper_RemoveAt_m1_1558,
	SynchronizedArrayListWrapper_CopyTo_m1_1559,
	SynchronizedArrayListWrapper_CopyTo_m1_1560,
	SynchronizedArrayListWrapper_CopyTo_m1_1561,
	SynchronizedArrayListWrapper_GetEnumerator_m1_1562,
	SynchronizedArrayListWrapper_AddRange_m1_1563,
	SynchronizedArrayListWrapper_Clone_m1_1564,
	SynchronizedArrayListWrapper_Sort_m1_1565,
	SynchronizedArrayListWrapper_Sort_m1_1566,
	SynchronizedArrayListWrapper_ToArray_m1_1567,
	SynchronizedArrayListWrapper_ToArray_m1_1568,
	FixedSizeArrayListWrapper__ctor_m1_1569,
	FixedSizeArrayListWrapper_get_ErrorMessage_m1_1570,
	FixedSizeArrayListWrapper_get_Capacity_m1_1571,
	FixedSizeArrayListWrapper_set_Capacity_m1_1572,
	FixedSizeArrayListWrapper_Add_m1_1573,
	FixedSizeArrayListWrapper_AddRange_m1_1574,
	FixedSizeArrayListWrapper_Clear_m1_1575,
	FixedSizeArrayListWrapper_Insert_m1_1576,
	FixedSizeArrayListWrapper_InsertRange_m1_1577,
	FixedSizeArrayListWrapper_Remove_m1_1578,
	FixedSizeArrayListWrapper_RemoveAt_m1_1579,
	ReadOnlyArrayListWrapper__ctor_m1_1580,
	ReadOnlyArrayListWrapper_get_ErrorMessage_m1_1581,
	ReadOnlyArrayListWrapper_get_IsReadOnly_m1_1582,
	ReadOnlyArrayListWrapper_get_Item_m1_1583,
	ReadOnlyArrayListWrapper_set_Item_m1_1584,
	ReadOnlyArrayListWrapper_Sort_m1_1585,
	ReadOnlyArrayListWrapper_Sort_m1_1586,
	ArrayList__ctor_m1_1587,
	ArrayList__ctor_m1_1588,
	ArrayList__ctor_m1_1589,
	ArrayList__ctor_m1_1590,
	ArrayList__cctor_m1_1591,
	ArrayList_get_Item_m1_1592,
	ArrayList_set_Item_m1_1593,
	ArrayList_get_Count_m1_1594,
	ArrayList_get_Capacity_m1_1595,
	ArrayList_set_Capacity_m1_1596,
	ArrayList_get_IsReadOnly_m1_1597,
	ArrayList_get_IsSynchronized_m1_1598,
	ArrayList_get_SyncRoot_m1_1599,
	ArrayList_EnsureCapacity_m1_1600,
	ArrayList_Shift_m1_1601,
	ArrayList_Add_m1_1602,
	ArrayList_Clear_m1_1603,
	ArrayList_Contains_m1_1604,
	ArrayList_IndexOf_m1_1605,
	ArrayList_IndexOf_m1_1606,
	ArrayList_IndexOf_m1_1607,
	ArrayList_Insert_m1_1608,
	ArrayList_InsertRange_m1_1609,
	ArrayList_Remove_m1_1610,
	ArrayList_RemoveAt_m1_1611,
	ArrayList_CopyTo_m1_1612,
	ArrayList_CopyTo_m1_1613,
	ArrayList_CopyTo_m1_1614,
	ArrayList_GetEnumerator_m1_1615,
	ArrayList_AddRange_m1_1616,
	ArrayList_Sort_m1_1617,
	ArrayList_Sort_m1_1618,
	ArrayList_ToArray_m1_1619,
	ArrayList_ToArray_m1_1620,
	ArrayList_Clone_m1_1621,
	ArrayList_ThrowNewArgumentOutOfRangeException_m1_1622,
	ArrayList_Synchronized_m1_1623,
	ArrayList_ReadOnly_m1_1624,
	BitArrayEnumerator__ctor_m1_1625,
	BitArrayEnumerator_Clone_m1_1626,
	BitArrayEnumerator_get_Current_m1_1627,
	BitArrayEnumerator_MoveNext_m1_1628,
	BitArrayEnumerator_Reset_m1_1629,
	BitArrayEnumerator_checkVersion_m1_1630,
	BitArray__ctor_m1_1631,
	BitArray__ctor_m1_1632,
	BitArray_getByte_m1_1633,
	BitArray_get_Count_m1_1634,
	BitArray_get_Item_m1_1635,
	BitArray_set_Item_m1_1636,
	BitArray_get_Length_m1_1637,
	BitArray_get_SyncRoot_m1_1638,
	BitArray_Clone_m1_1639,
	BitArray_CopyTo_m1_1640,
	BitArray_Get_m1_1641,
	BitArray_Set_m1_1642,
	BitArray_GetEnumerator_m1_1643,
	CaseInsensitiveComparer__ctor_m1_1644,
	CaseInsensitiveComparer__ctor_m1_1645,
	CaseInsensitiveComparer__cctor_m1_1646,
	CaseInsensitiveComparer_get_DefaultInvariant_m1_1647,
	CaseInsensitiveComparer_Compare_m1_1648,
	CaseInsensitiveHashCodeProvider__ctor_m1_1649,
	CaseInsensitiveHashCodeProvider__ctor_m1_1650,
	CaseInsensitiveHashCodeProvider__cctor_m1_1651,
	CaseInsensitiveHashCodeProvider_AreEqual_m1_1652,
	CaseInsensitiveHashCodeProvider_AreEqual_m1_1653,
	CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m1_1654,
	CaseInsensitiveHashCodeProvider_GetHashCode_m1_1655,
	CollectionBase__ctor_m1_1656,
	CollectionBase_System_Collections_ICollection_CopyTo_m1_1657,
	CollectionBase_System_Collections_ICollection_get_SyncRoot_m1_1658,
	CollectionBase_System_Collections_IList_Add_m1_1659,
	CollectionBase_System_Collections_IList_Contains_m1_1660,
	CollectionBase_System_Collections_IList_IndexOf_m1_1661,
	CollectionBase_System_Collections_IList_Insert_m1_1662,
	CollectionBase_System_Collections_IList_Remove_m1_1663,
	CollectionBase_System_Collections_IList_get_Item_m1_1664,
	CollectionBase_System_Collections_IList_set_Item_m1_1665,
	CollectionBase_get_Count_m1_1666,
	CollectionBase_GetEnumerator_m1_1667,
	CollectionBase_Clear_m1_1668,
	CollectionBase_RemoveAt_m1_1669,
	CollectionBase_get_InnerList_m1_1670,
	CollectionBase_get_List_m1_1671,
	CollectionBase_OnClear_m1_1672,
	CollectionBase_OnClearComplete_m1_1673,
	CollectionBase_OnInsert_m1_1674,
	CollectionBase_OnInsertComplete_m1_1675,
	CollectionBase_OnRemove_m1_1676,
	CollectionBase_OnRemoveComplete_m1_1677,
	CollectionBase_OnSet_m1_1678,
	CollectionBase_OnSetComplete_m1_1679,
	CollectionBase_OnValidate_m1_1680,
	Comparer__ctor_m1_1681,
	Comparer__ctor_m1_1682,
	Comparer__cctor_m1_1683,
	Comparer_Compare_m1_1684,
	Comparer_GetObjectData_m1_1685,
	DictionaryEntry__ctor_m1_1686,
	DictionaryEntry_get_Key_m1_1687,
	DictionaryEntry_get_Value_m1_1688,
	KeyMarker__ctor_m1_1689,
	KeyMarker__cctor_m1_1690,
	Enumerator__ctor_m1_1691,
	Enumerator__cctor_m1_1692,
	Enumerator_FailFast_m1_1693,
	Enumerator_Reset_m1_1694,
	Enumerator_MoveNext_m1_1695,
	Enumerator_get_Entry_m1_1696,
	Enumerator_get_Key_m1_1697,
	Enumerator_get_Value_m1_1698,
	Enumerator_get_Current_m1_1699,
	HashKeys__ctor_m1_1700,
	HashKeys_get_Count_m1_1701,
	HashKeys_get_SyncRoot_m1_1702,
	HashKeys_CopyTo_m1_1703,
	HashKeys_GetEnumerator_m1_1704,
	HashValues__ctor_m1_1705,
	HashValues_get_Count_m1_1706,
	HashValues_get_SyncRoot_m1_1707,
	HashValues_CopyTo_m1_1708,
	HashValues_GetEnumerator_m1_1709,
	SyncHashtable__ctor_m1_1710,
	SyncHashtable__ctor_m1_1711,
	SyncHashtable_System_Collections_IEnumerable_GetEnumerator_m1_1712,
	SyncHashtable_GetObjectData_m1_1713,
	SyncHashtable_get_Count_m1_1714,
	SyncHashtable_get_SyncRoot_m1_1715,
	SyncHashtable_get_Keys_m1_1716,
	SyncHashtable_get_Values_m1_1717,
	SyncHashtable_get_Item_m1_1718,
	SyncHashtable_set_Item_m1_1719,
	SyncHashtable_CopyTo_m1_1720,
	SyncHashtable_Add_m1_1721,
	SyncHashtable_Clear_m1_1722,
	SyncHashtable_Contains_m1_1723,
	SyncHashtable_GetEnumerator_m1_1724,
	SyncHashtable_Remove_m1_1725,
	SyncHashtable_ContainsKey_m1_1726,
	SyncHashtable_Clone_m1_1727,
	Hashtable__ctor_m1_1728,
	Hashtable__ctor_m1_1729,
	Hashtable__ctor_m1_1730,
	Hashtable__ctor_m1_1731,
	Hashtable__ctor_m1_1732,
	Hashtable__ctor_m1_1733,
	Hashtable__ctor_m1_1734,
	Hashtable__ctor_m1_1735,
	Hashtable__ctor_m1_1736,
	Hashtable__ctor_m1_1737,
	Hashtable__ctor_m1_1738,
	Hashtable__ctor_m1_1739,
	Hashtable__cctor_m1_1740,
	Hashtable_System_Collections_IEnumerable_GetEnumerator_m1_1741,
	Hashtable_set_comparer_m1_1742,
	Hashtable_set_hcp_m1_1743,
	Hashtable_get_Count_m1_1744,
	Hashtable_get_SyncRoot_m1_1745,
	Hashtable_get_Keys_m1_1746,
	Hashtable_get_Values_m1_1747,
	Hashtable_get_Item_m1_1748,
	Hashtable_set_Item_m1_1749,
	Hashtable_CopyTo_m1_1750,
	Hashtable_Add_m1_1751,
	Hashtable_Clear_m1_1752,
	Hashtable_Contains_m1_1753,
	Hashtable_GetEnumerator_m1_1754,
	Hashtable_Remove_m1_1755,
	Hashtable_ContainsKey_m1_1756,
	Hashtable_Clone_m1_1757,
	Hashtable_GetObjectData_m1_1758,
	Hashtable_OnDeserialization_m1_1759,
	Hashtable_Synchronized_m1_1760,
	Hashtable_GetHash_m1_1761,
	Hashtable_KeyEquals_m1_1762,
	Hashtable_AdjustThreshold_m1_1763,
	Hashtable_SetTable_m1_1764,
	Hashtable_Find_m1_1765,
	Hashtable_Rehash_m1_1766,
	Hashtable_PutImpl_m1_1767,
	Hashtable_CopyToArray_m1_1768,
	Hashtable_TestPrime_m1_1769,
	Hashtable_CalcPrime_m1_1770,
	Hashtable_ToPrime_m1_1771,
	QueueEnumerator__ctor_m1_1772,
	QueueEnumerator_Clone_m1_1773,
	QueueEnumerator_get_Current_m1_1774,
	QueueEnumerator_MoveNext_m1_1775,
	QueueEnumerator_Reset_m1_1776,
	Queue__ctor_m1_1777,
	Queue__ctor_m1_1778,
	Queue__ctor_m1_1779,
	Queue_get_Count_m1_1780,
	Queue_get_SyncRoot_m1_1781,
	Queue_CopyTo_m1_1782,
	Queue_GetEnumerator_m1_1783,
	Queue_Clone_m1_1784,
	Enumerator__ctor_m1_1785,
	Enumerator__cctor_m1_1786,
	Enumerator_Reset_m1_1787,
	Enumerator_MoveNext_m1_1788,
	Enumerator_get_Entry_m1_1789,
	Enumerator_get_Key_m1_1790,
	Enumerator_get_Value_m1_1791,
	Enumerator_get_Current_m1_1792,
	Enumerator_Clone_m1_1793,
	ListKeys__ctor_m1_1794,
	ListKeys_get_Count_m1_1795,
	ListKeys_get_SyncRoot_m1_1796,
	ListKeys_CopyTo_m1_1797,
	ListKeys_get_Item_m1_1798,
	ListKeys_set_Item_m1_1799,
	ListKeys_Add_m1_1800,
	ListKeys_Clear_m1_1801,
	ListKeys_Contains_m1_1802,
	ListKeys_IndexOf_m1_1803,
	ListKeys_Insert_m1_1804,
	ListKeys_Remove_m1_1805,
	ListKeys_RemoveAt_m1_1806,
	ListKeys_GetEnumerator_m1_1807,
	SortedList__ctor_m1_1808,
	SortedList__ctor_m1_1809,
	SortedList__ctor_m1_1810,
	SortedList__ctor_m1_1811,
	SortedList__cctor_m1_1812,
	SortedList_System_Collections_IEnumerable_GetEnumerator_m1_1813,
	SortedList_get_Count_m1_1814,
	SortedList_get_SyncRoot_m1_1815,
	SortedList_get_IsFixedSize_m1_1816,
	SortedList_get_IsReadOnly_m1_1817,
	SortedList_get_Keys_m1_1818,
	SortedList_get_Item_m1_1819,
	SortedList_set_Item_m1_1820,
	SortedList_get_Capacity_m1_1821,
	SortedList_set_Capacity_m1_1822,
	SortedList_Add_m1_1823,
	SortedList_Contains_m1_1824,
	SortedList_GetEnumerator_m1_1825,
	SortedList_Remove_m1_1826,
	SortedList_CopyTo_m1_1827,
	SortedList_Clone_m1_1828,
	SortedList_RemoveAt_m1_1829,
	SortedList_IndexOfKey_m1_1830,
	SortedList_ContainsKey_m1_1831,
	SortedList_GetByIndex_m1_1832,
	SortedList_GetKey_m1_1833,
	SortedList_EnsureCapacity_m1_1834,
	SortedList_PutImpl_m1_1835,
	SortedList_GetImpl_m1_1836,
	SortedList_InitTable_m1_1837,
	SortedList_CopyToArray_m1_1838,
	SortedList_Find_m1_1839,
	Enumerator__ctor_m1_1840,
	Enumerator_Clone_m1_1841,
	Enumerator_get_Current_m1_1842,
	Enumerator_MoveNext_m1_1843,
	Enumerator_Reset_m1_1844,
	Stack__ctor_m1_1845,
	Stack__ctor_m1_1846,
	Stack__ctor_m1_1847,
	Stack_Resize_m1_1848,
	Stack_get_Count_m1_1849,
	Stack_get_SyncRoot_m1_1850,
	Stack_Clear_m1_1851,
	Stack_Clone_m1_1852,
	Stack_CopyTo_m1_1853,
	Stack_GetEnumerator_m1_1854,
	Stack_Peek_m1_1855,
	Stack_Pop_m1_1856,
	Stack_Push_m1_1857,
	DebuggableAttribute__ctor_m1_1858,
	DebuggerDisplayAttribute__ctor_m1_1859,
	DebuggerDisplayAttribute_set_Name_m1_1860,
	DebuggerStepThroughAttribute__ctor_m1_1861,
	DebuggerTypeProxyAttribute__ctor_m1_1862,
	StackFrame__ctor_m1_1863,
	StackFrame__ctor_m1_1864,
	StackFrame_get_frame_info_m1_1865,
	StackFrame_GetFileLineNumber_m1_1866,
	StackFrame_GetFileName_m1_1867,
	StackFrame_GetSecureFileName_m1_1868,
	StackFrame_GetILOffset_m1_1869,
	StackFrame_GetMethod_m1_1870,
	StackFrame_GetNativeOffset_m1_1871,
	StackFrame_GetInternalMethodName_m1_1872,
	StackFrame_ToString_m1_1873,
	StackTrace__ctor_m1_1874,
	StackTrace__ctor_m1_1875,
	StackTrace__ctor_m1_1876,
	StackTrace__ctor_m1_1877,
	StackTrace__ctor_m1_1878,
	StackTrace_init_frames_m1_1879,
	StackTrace_get_trace_m1_1880,
	StackTrace_get_FrameCount_m1_1881,
	StackTrace_GetFrame_m1_1882,
	StackTrace_ToString_m1_1883,
	Calendar__ctor_m1_1884,
	Calendar_Clone_m1_1885,
	Calendar_CheckReadOnly_m1_1886,
	Calendar_get_EraNames_m1_1887,
	CCMath_div_m1_1888,
	CCMath_mod_m1_1889,
	CCMath_div_mod_m1_1890,
	CCFixed_FromDateTime_m1_1891,
	CCFixed_day_of_week_m1_1892,
	CCGregorianCalendar_is_leap_year_m1_1893,
	CCGregorianCalendar_fixed_from_dmy_m1_1894,
	CCGregorianCalendar_year_from_fixed_m1_1895,
	CCGregorianCalendar_my_from_fixed_m1_1896,
	CCGregorianCalendar_dmy_from_fixed_m1_1897,
	CCGregorianCalendar_month_from_fixed_m1_1898,
	CCGregorianCalendar_day_from_fixed_m1_1899,
	CCGregorianCalendar_GetDayOfMonth_m1_1900,
	CCGregorianCalendar_GetMonth_m1_1901,
	CCGregorianCalendar_GetYear_m1_1902,
	CompareInfo__ctor_m1_1903,
	CompareInfo__ctor_m1_1904,
	CompareInfo__cctor_m1_1905,
	CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_1906,
	CompareInfo_get_UseManagedCollation_m1_1907,
	CompareInfo_construct_compareinfo_m1_1908,
	CompareInfo_free_internal_collator_m1_1909,
	CompareInfo_internal_compare_m1_1910,
	CompareInfo_assign_sortkey_m1_1911,
	CompareInfo_internal_index_m1_1912,
	CompareInfo_Finalize_m1_1913,
	CompareInfo_internal_compare_managed_m1_1914,
	CompareInfo_internal_compare_switch_m1_1915,
	CompareInfo_Compare_m1_1916,
	CompareInfo_Compare_m1_1917,
	CompareInfo_Compare_m1_1918,
	CompareInfo_Equals_m1_1919,
	CompareInfo_GetHashCode_m1_1920,
	CompareInfo_GetSortKey_m1_1921,
	CompareInfo_IndexOf_m1_1922,
	CompareInfo_internal_index_managed_m1_1923,
	CompareInfo_internal_index_switch_m1_1924,
	CompareInfo_IndexOf_m1_1925,
	CompareInfo_IsPrefix_m1_1926,
	CompareInfo_IsSuffix_m1_1927,
	CompareInfo_LastIndexOf_m1_1928,
	CompareInfo_LastIndexOf_m1_1929,
	CompareInfo_ToString_m1_1930,
	CompareInfo_get_LCID_m1_1931,
	CultureInfo__ctor_m1_1932,
	CultureInfo__ctor_m1_1933,
	CultureInfo__ctor_m1_1934,
	CultureInfo__ctor_m1_1935,
	CultureInfo__ctor_m1_1936,
	CultureInfo__cctor_m1_1937,
	CultureInfo_get_InvariantCulture_m1_1938,
	CultureInfo_get_CurrentCulture_m1_1939,
	CultureInfo_get_CurrentUICulture_m1_1940,
	CultureInfo_ConstructCurrentCulture_m1_1941,
	CultureInfo_ConstructCurrentUICulture_m1_1942,
	CultureInfo_get_LCID_m1_1943,
	CultureInfo_get_Name_m1_1944,
	CultureInfo_get_Parent_m1_1945,
	CultureInfo_get_TextInfo_m1_1946,
	CultureInfo_get_IcuName_m1_1947,
	CultureInfo_Clone_m1_1948,
	CultureInfo_Equals_m1_1949,
	CultureInfo_GetHashCode_m1_1950,
	CultureInfo_ToString_m1_1951,
	CultureInfo_get_CompareInfo_m1_1952,
	CultureInfo_get_IsNeutralCulture_m1_1953,
	CultureInfo_CheckNeutral_m1_1954,
	CultureInfo_get_NumberFormat_m1_1955,
	CultureInfo_set_NumberFormat_m1_1956,
	CultureInfo_get_DateTimeFormat_m1_1957,
	CultureInfo_set_DateTimeFormat_m1_1958,
	CultureInfo_get_IsReadOnly_m1_1959,
	CultureInfo_GetFormat_m1_1960,
	CultureInfo_Construct_m1_1961,
	CultureInfo_ConstructInternalLocaleFromName_m1_1962,
	CultureInfo_ConstructInternalLocaleFromLcid_m1_1963,
	CultureInfo_ConstructInternalLocaleFromCurrentLocale_m1_1964,
	CultureInfo_construct_internal_locale_from_lcid_m1_1965,
	CultureInfo_construct_internal_locale_from_name_m1_1966,
	CultureInfo_construct_internal_locale_from_current_locale_m1_1967,
	CultureInfo_construct_datetime_format_m1_1968,
	CultureInfo_construct_number_format_m1_1969,
	CultureInfo_ConstructInvariant_m1_1970,
	CultureInfo_CreateTextInfo_m1_1971,
	CultureInfo_CreateCulture_m1_1972,
	DateTimeFormatInfo__ctor_m1_1973,
	DateTimeFormatInfo__ctor_m1_1974,
	DateTimeFormatInfo__cctor_m1_1975,
	DateTimeFormatInfo_GetInstance_m1_1976,
	DateTimeFormatInfo_get_IsReadOnly_m1_1977,
	DateTimeFormatInfo_ReadOnly_m1_1978,
	DateTimeFormatInfo_Clone_m1_1979,
	DateTimeFormatInfo_GetFormat_m1_1980,
	DateTimeFormatInfo_GetAbbreviatedMonthName_m1_1981,
	DateTimeFormatInfo_GetEraName_m1_1982,
	DateTimeFormatInfo_GetMonthName_m1_1983,
	DateTimeFormatInfo_get_RawAbbreviatedDayNames_m1_1984,
	DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m1_1985,
	DateTimeFormatInfo_get_RawDayNames_m1_1986,
	DateTimeFormatInfo_get_RawMonthNames_m1_1987,
	DateTimeFormatInfo_get_AMDesignator_m1_1988,
	DateTimeFormatInfo_get_PMDesignator_m1_1989,
	DateTimeFormatInfo_get_DateSeparator_m1_1990,
	DateTimeFormatInfo_get_TimeSeparator_m1_1991,
	DateTimeFormatInfo_get_LongDatePattern_m1_1992,
	DateTimeFormatInfo_get_ShortDatePattern_m1_1993,
	DateTimeFormatInfo_get_ShortTimePattern_m1_1994,
	DateTimeFormatInfo_get_LongTimePattern_m1_1995,
	DateTimeFormatInfo_get_MonthDayPattern_m1_1996,
	DateTimeFormatInfo_get_YearMonthPattern_m1_1997,
	DateTimeFormatInfo_get_FullDateTimePattern_m1_1998,
	DateTimeFormatInfo_get_CurrentInfo_m1_1999,
	DateTimeFormatInfo_get_InvariantInfo_m1_2000,
	DateTimeFormatInfo_get_Calendar_m1_2001,
	DateTimeFormatInfo_set_Calendar_m1_2002,
	DateTimeFormatInfo_get_RFC1123Pattern_m1_2003,
	DateTimeFormatInfo_get_RoundtripPattern_m1_2004,
	DateTimeFormatInfo_get_SortableDateTimePattern_m1_2005,
	DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m1_2006,
	DateTimeFormatInfo_GetAllDateTimePatternsInternal_m1_2007,
	DateTimeFormatInfo_FillAllDateTimePatterns_m1_2008,
	DateTimeFormatInfo_GetAllRawDateTimePatterns_m1_2009,
	DateTimeFormatInfo_GetDayName_m1_2010,
	DateTimeFormatInfo_GetAbbreviatedDayName_m1_2011,
	DateTimeFormatInfo_FillInvariantPatterns_m1_2012,
	DateTimeFormatInfo_PopulateCombinedList_m1_2013,
	DaylightTime__ctor_m1_2014,
	DaylightTime_get_Start_m1_2015,
	DaylightTime_get_End_m1_2016,
	DaylightTime_get_Delta_m1_2017,
	GregorianCalendar__ctor_m1_2018,
	GregorianCalendar__ctor_m1_2019,
	GregorianCalendar_get_Eras_m1_2020,
	GregorianCalendar_set_CalendarType_m1_2021,
	GregorianCalendar_GetDayOfMonth_m1_2022,
	GregorianCalendar_GetDayOfWeek_m1_2023,
	GregorianCalendar_GetEra_m1_2024,
	GregorianCalendar_GetMonth_m1_2025,
	GregorianCalendar_GetYear_m1_2026,
	NumberFormatInfo__ctor_m1_2027,
	NumberFormatInfo__ctor_m1_2028,
	NumberFormatInfo__ctor_m1_2029,
	NumberFormatInfo__cctor_m1_2030,
	NumberFormatInfo_get_CurrencyDecimalDigits_m1_2031,
	NumberFormatInfo_get_CurrencyDecimalSeparator_m1_2032,
	NumberFormatInfo_get_CurrencyGroupSeparator_m1_2033,
	NumberFormatInfo_get_RawCurrencyGroupSizes_m1_2034,
	NumberFormatInfo_get_CurrencyNegativePattern_m1_2035,
	NumberFormatInfo_get_CurrencyPositivePattern_m1_2036,
	NumberFormatInfo_get_CurrencySymbol_m1_2037,
	NumberFormatInfo_get_CurrentInfo_m1_2038,
	NumberFormatInfo_get_InvariantInfo_m1_2039,
	NumberFormatInfo_get_NaNSymbol_m1_2040,
	NumberFormatInfo_get_NegativeInfinitySymbol_m1_2041,
	NumberFormatInfo_get_NegativeSign_m1_2042,
	NumberFormatInfo_get_NumberDecimalDigits_m1_2043,
	NumberFormatInfo_get_NumberDecimalSeparator_m1_2044,
	NumberFormatInfo_get_NumberGroupSeparator_m1_2045,
	NumberFormatInfo_get_RawNumberGroupSizes_m1_2046,
	NumberFormatInfo_get_NumberNegativePattern_m1_2047,
	NumberFormatInfo_set_NumberNegativePattern_m1_2048,
	NumberFormatInfo_get_PercentDecimalDigits_m1_2049,
	NumberFormatInfo_get_PercentDecimalSeparator_m1_2050,
	NumberFormatInfo_get_PercentGroupSeparator_m1_2051,
	NumberFormatInfo_get_RawPercentGroupSizes_m1_2052,
	NumberFormatInfo_get_PercentNegativePattern_m1_2053,
	NumberFormatInfo_get_PercentPositivePattern_m1_2054,
	NumberFormatInfo_get_PercentSymbol_m1_2055,
	NumberFormatInfo_get_PerMilleSymbol_m1_2056,
	NumberFormatInfo_get_PositiveInfinitySymbol_m1_2057,
	NumberFormatInfo_get_PositiveSign_m1_2058,
	NumberFormatInfo_GetFormat_m1_2059,
	NumberFormatInfo_Clone_m1_2060,
	NumberFormatInfo_GetInstance_m1_2061,
	TextInfo__ctor_m1_2062,
	TextInfo__ctor_m1_2063,
	TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_2064,
	TextInfo_get_ListSeparator_m1_2065,
	TextInfo_get_CultureName_m1_2066,
	TextInfo_Equals_m1_2067,
	TextInfo_GetHashCode_m1_2068,
	TextInfo_ToString_m1_2069,
	TextInfo_ToLower_m1_2070,
	TextInfo_ToUpper_m1_2071,
	TextInfo_ToLower_m1_2072,
	TextInfo_Clone_m1_2073,
	IsolatedStorageException__ctor_m1_2074,
	IsolatedStorageException__ctor_m1_2075,
	IsolatedStorageException__ctor_m1_2076,
	BinaryReader__ctor_m1_2077,
	BinaryReader__ctor_m1_2078,
	BinaryReader_System_IDisposable_Dispose_m1_2079,
	BinaryReader_get_BaseStream_m1_2080,
	BinaryReader_Close_m1_2081,
	BinaryReader_Dispose_m1_2082,
	BinaryReader_FillBuffer_m1_2083,
	BinaryReader_Read_m1_2084,
	BinaryReader_Read_m1_2085,
	BinaryReader_Read_m1_2086,
	BinaryReader_ReadCharBytes_m1_2087,
	BinaryReader_Read7BitEncodedInt_m1_2088,
	BinaryReader_ReadBoolean_m1_2089,
	BinaryReader_ReadByte_m1_2090,
	BinaryReader_ReadBytes_m1_2091,
	BinaryReader_ReadChar_m1_2092,
	BinaryReader_ReadDecimal_m1_2093,
	BinaryReader_ReadDouble_m1_2094,
	BinaryReader_ReadInt16_m1_2095,
	BinaryReader_ReadInt32_m1_2096,
	BinaryReader_ReadInt64_m1_2097,
	BinaryReader_ReadSByte_m1_2098,
	BinaryReader_ReadString_m1_2099,
	BinaryReader_ReadSingle_m1_2100,
	BinaryReader_ReadUInt16_m1_2101,
	BinaryReader_ReadUInt32_m1_2102,
	BinaryReader_ReadUInt64_m1_2103,
	BinaryReader_CheckBuffer_m1_2104,
	BinaryWriter__ctor_m1_2105,
	BinaryWriter__ctor_m1_2106,
	BinaryWriter__ctor_m1_2107,
	BinaryWriter__cctor_m1_2108,
	BinaryWriter_System_IDisposable_Dispose_m1_2109,
	BinaryWriter_Dispose_m1_2110,
	BinaryWriter_Write_m1_2111,
	Directory_CreateDirectory_m1_2112,
	Directory_CreateDirectoriesInternal_m1_2113,
	Directory_Exists_m1_2114,
	Directory_GetCurrentDirectory_m1_2115,
	Directory_GetFiles_m1_2116,
	Directory_GetFileSystemEntries_m1_2117,
	DirectoryInfo__ctor_m1_2118,
	DirectoryInfo__ctor_m1_2119,
	DirectoryInfo__ctor_m1_2120,
	DirectoryInfo_Initialize_m1_2121,
	DirectoryInfo_get_Exists_m1_2122,
	DirectoryInfo_get_Parent_m1_2123,
	DirectoryInfo_Create_m1_2124,
	DirectoryInfo_ToString_m1_2125,
	DirectoryNotFoundException__ctor_m1_2126,
	DirectoryNotFoundException__ctor_m1_2127,
	DirectoryNotFoundException__ctor_m1_2128,
	EndOfStreamException__ctor_m1_2129,
	EndOfStreamException__ctor_m1_2130,
	File_Delete_m1_2131,
	File_Exists_m1_2132,
	File_Open_m1_2133,
	File_OpenRead_m1_2134,
	File_OpenText_m1_2135,
	FileNotFoundException__ctor_m1_2136,
	FileNotFoundException__ctor_m1_2137,
	FileNotFoundException__ctor_m1_2138,
	FileNotFoundException_get_Message_m1_2139,
	FileNotFoundException_GetObjectData_m1_2140,
	FileNotFoundException_ToString_m1_2141,
	ReadDelegate__ctor_m1_2142,
	ReadDelegate_Invoke_m1_2143,
	ReadDelegate_BeginInvoke_m1_2144,
	ReadDelegate_EndInvoke_m1_2145,
	WriteDelegate__ctor_m1_2146,
	WriteDelegate_Invoke_m1_2147,
	WriteDelegate_BeginInvoke_m1_2148,
	WriteDelegate_EndInvoke_m1_2149,
	FileStream__ctor_m1_2150,
	FileStream__ctor_m1_2151,
	FileStream__ctor_m1_2152,
	FileStream__ctor_m1_2153,
	FileStream__ctor_m1_2154,
	FileStream_get_CanRead_m1_2155,
	FileStream_get_CanWrite_m1_2156,
	FileStream_get_CanSeek_m1_2157,
	FileStream_get_Length_m1_2158,
	FileStream_get_Position_m1_2159,
	FileStream_set_Position_m1_2160,
	FileStream_ReadByte_m1_2161,
	FileStream_WriteByte_m1_2162,
	FileStream_Read_m1_2163,
	FileStream_ReadInternal_m1_2164,
	FileStream_BeginRead_m1_2165,
	FileStream_EndRead_m1_2166,
	FileStream_Write_m1_2167,
	FileStream_WriteInternal_m1_2168,
	FileStream_BeginWrite_m1_2169,
	FileStream_EndWrite_m1_2170,
	FileStream_Seek_m1_2171,
	FileStream_SetLength_m1_2172,
	FileStream_Flush_m1_2173,
	FileStream_Finalize_m1_2174,
	FileStream_Dispose_m1_2175,
	FileStream_ReadSegment_m1_2176,
	FileStream_WriteSegment_m1_2177,
	FileStream_FlushBuffer_m1_2178,
	FileStream_FlushBuffer_m1_2179,
	FileStream_FlushBufferIfDirty_m1_2180,
	FileStream_RefillBuffer_m1_2181,
	FileStream_ReadData_m1_2182,
	FileStream_InitBuffer_m1_2183,
	FileStream_GetSecureFileName_m1_2184,
	FileStream_GetSecureFileName_m1_2185,
	FileStreamAsyncResult__ctor_m1_2186,
	FileStreamAsyncResult_CBWrapper_m1_2187,
	FileStreamAsyncResult_get_AsyncState_m1_2188,
	FileStreamAsyncResult_get_AsyncWaitHandle_m1_2189,
	FileStreamAsyncResult_get_IsCompleted_m1_2190,
	FileSystemInfo__ctor_m1_2191,
	FileSystemInfo__ctor_m1_2192,
	FileSystemInfo_GetObjectData_m1_2193,
	FileSystemInfo_get_FullName_m1_2194,
	FileSystemInfo_Refresh_m1_2195,
	FileSystemInfo_InternalRefresh_m1_2196,
	FileSystemInfo_CheckPath_m1_2197,
	IOException__ctor_m1_2198,
	IOException__ctor_m1_2199,
	IOException__ctor_m1_2200,
	IOException__ctor_m1_2201,
	IOException__ctor_m1_2202,
	MemoryStream__ctor_m1_2203,
	MemoryStream__ctor_m1_2204,
	MemoryStream__ctor_m1_2205,
	MemoryStream_InternalConstructor_m1_2206,
	MemoryStream_CheckIfClosedThrowDisposed_m1_2207,
	MemoryStream_get_CanRead_m1_2208,
	MemoryStream_get_CanSeek_m1_2209,
	MemoryStream_get_CanWrite_m1_2210,
	MemoryStream_set_Capacity_m1_2211,
	MemoryStream_get_Length_m1_2212,
	MemoryStream_get_Position_m1_2213,
	MemoryStream_set_Position_m1_2214,
	MemoryStream_Dispose_m1_2215,
	MemoryStream_Flush_m1_2216,
	MemoryStream_Read_m1_2217,
	MemoryStream_ReadByte_m1_2218,
	MemoryStream_Seek_m1_2219,
	MemoryStream_CalculateNewCapacity_m1_2220,
	MemoryStream_Expand_m1_2221,
	MemoryStream_SetLength_m1_2222,
	MemoryStream_ToArray_m1_2223,
	MemoryStream_Write_m1_2224,
	MemoryStream_WriteByte_m1_2225,
	MonoIO__cctor_m1_2226,
	MonoIO_GetException_m1_2227,
	MonoIO_GetException_m1_2228,
	MonoIO_CreateDirectory_m1_2229,
	MonoIO_GetFileSystemEntries_m1_2230,
	MonoIO_GetCurrentDirectory_m1_2231,
	MonoIO_DeleteFile_m1_2232,
	MonoIO_GetFileAttributes_m1_2233,
	MonoIO_GetFileType_m1_2234,
	MonoIO_ExistsFile_m1_2235,
	MonoIO_ExistsDirectory_m1_2236,
	MonoIO_GetFileStat_m1_2237,
	MonoIO_Open_m1_2238,
	MonoIO_Close_m1_2239,
	MonoIO_Read_m1_2240,
	MonoIO_Write_m1_2241,
	MonoIO_Seek_m1_2242,
	MonoIO_GetLength_m1_2243,
	MonoIO_SetLength_m1_2244,
	MonoIO_get_ConsoleOutput_m1_2245,
	MonoIO_get_ConsoleInput_m1_2246,
	MonoIO_get_ConsoleError_m1_2247,
	MonoIO_get_VolumeSeparatorChar_m1_2248,
	MonoIO_get_DirectorySeparatorChar_m1_2249,
	MonoIO_get_AltDirectorySeparatorChar_m1_2250,
	MonoIO_get_PathSeparator_m1_2251,
	Path__cctor_m1_2252,
	Path_Combine_m1_2253,
	Path_CleanPath_m1_2254,
	Path_GetDirectoryName_m1_2255,
	Path_GetFileName_m1_2256,
	Path_GetFullPath_m1_2257,
	Path_WindowsDriveAdjustment_m1_2258,
	Path_InsecureGetFullPath_m1_2259,
	Path_IsDsc_m1_2260,
	Path_GetPathRoot_m1_2261,
	Path_IsPathRooted_m1_2262,
	Path_GetInvalidPathChars_m1_2263,
	Path_GetServerAndShare_m1_2264,
	Path_SameRoot_m1_2265,
	Path_CanonicalizePath_m1_2266,
	PathTooLongException__ctor_m1_2267,
	PathTooLongException__ctor_m1_2268,
	PathTooLongException__ctor_m1_2269,
	SearchPattern__cctor_m1_2270,
	Stream__ctor_m1_2271,
	Stream__cctor_m1_2272,
	Stream_Dispose_m1_2273,
	Stream_Dispose_m1_2274,
	Stream_Close_m1_2275,
	Stream_ReadByte_m1_2276,
	Stream_WriteByte_m1_2277,
	Stream_BeginRead_m1_2278,
	Stream_BeginWrite_m1_2279,
	Stream_EndRead_m1_2280,
	Stream_EndWrite_m1_2281,
	NullStream__ctor_m1_2282,
	NullStream_get_CanRead_m1_2283,
	NullStream_get_CanSeek_m1_2284,
	NullStream_get_CanWrite_m1_2285,
	NullStream_get_Length_m1_2286,
	NullStream_get_Position_m1_2287,
	NullStream_set_Position_m1_2288,
	NullStream_Flush_m1_2289,
	NullStream_Read_m1_2290,
	NullStream_ReadByte_m1_2291,
	NullStream_Seek_m1_2292,
	NullStream_SetLength_m1_2293,
	NullStream_Write_m1_2294,
	NullStream_WriteByte_m1_2295,
	StreamAsyncResult__ctor_m1_2296,
	StreamAsyncResult_SetComplete_m1_2297,
	StreamAsyncResult_SetComplete_m1_2298,
	StreamAsyncResult_get_AsyncState_m1_2299,
	StreamAsyncResult_get_AsyncWaitHandle_m1_2300,
	StreamAsyncResult_get_IsCompleted_m1_2301,
	StreamAsyncResult_get_Exception_m1_2302,
	StreamAsyncResult_get_NBytes_m1_2303,
	StreamAsyncResult_get_Done_m1_2304,
	StreamAsyncResult_set_Done_m1_2305,
	NullStreamReader__ctor_m1_2306,
	NullStreamReader_Peek_m1_2307,
	NullStreamReader_Read_m1_2308,
	NullStreamReader_Read_m1_2309,
	NullStreamReader_ReadLine_m1_2310,
	NullStreamReader_ReadToEnd_m1_2311,
	StreamReader__ctor_m1_2312,
	StreamReader__ctor_m1_2313,
	StreamReader__ctor_m1_2314,
	StreamReader__ctor_m1_2315,
	StreamReader__ctor_m1_2316,
	StreamReader__cctor_m1_2317,
	StreamReader_Initialize_m1_2318,
	StreamReader_Dispose_m1_2319,
	StreamReader_DoChecks_m1_2320,
	StreamReader_ReadBuffer_m1_2321,
	StreamReader_Peek_m1_2322,
	StreamReader_Read_m1_2323,
	StreamReader_Read_m1_2324,
	StreamReader_FindNextEOL_m1_2325,
	StreamReader_ReadLine_m1_2326,
	StreamReader_ReadToEnd_m1_2327,
	StreamWriter__ctor_m1_2328,
	StreamWriter__ctor_m1_2329,
	StreamWriter__cctor_m1_2330,
	StreamWriter_Initialize_m1_2331,
	StreamWriter_set_AutoFlush_m1_2332,
	StreamWriter_Dispose_m1_2333,
	StreamWriter_Flush_m1_2334,
	StreamWriter_FlushBytes_m1_2335,
	StreamWriter_Decode_m1_2336,
	StreamWriter_Write_m1_2337,
	StreamWriter_LowLevelWrite_m1_2338,
	StreamWriter_LowLevelWrite_m1_2339,
	StreamWriter_Write_m1_2340,
	StreamWriter_Write_m1_2341,
	StreamWriter_Write_m1_2342,
	StreamWriter_Close_m1_2343,
	StreamWriter_Finalize_m1_2344,
	StringReader__ctor_m1_2345,
	StringReader_Dispose_m1_2346,
	StringReader_Peek_m1_2347,
	StringReader_Read_m1_2348,
	StringReader_Read_m1_2349,
	StringReader_ReadLine_m1_2350,
	StringReader_ReadToEnd_m1_2351,
	StringReader_CheckObjectDisposedException_m1_2352,
	NullTextReader__ctor_m1_2353,
	NullTextReader_ReadLine_m1_2354,
	TextReader__ctor_m1_2355,
	TextReader__cctor_m1_2356,
	TextReader_Dispose_m1_2357,
	TextReader_Dispose_m1_2358,
	TextReader_Peek_m1_2359,
	TextReader_Read_m1_2360,
	TextReader_Read_m1_2361,
	TextReader_ReadLine_m1_2362,
	TextReader_ReadToEnd_m1_2363,
	TextReader_Synchronized_m1_2364,
	SynchronizedReader__ctor_m1_2365,
	SynchronizedReader_Peek_m1_2366,
	SynchronizedReader_ReadLine_m1_2367,
	SynchronizedReader_ReadToEnd_m1_2368,
	SynchronizedReader_Read_m1_2369,
	SynchronizedReader_Read_m1_2370,
	NullTextWriter__ctor_m1_2371,
	NullTextWriter_Write_m1_2372,
	NullTextWriter_Write_m1_2373,
	NullTextWriter_Write_m1_2374,
	TextWriter__ctor_m1_2375,
	TextWriter__cctor_m1_2376,
	TextWriter_Close_m1_2377,
	TextWriter_Dispose_m1_2378,
	TextWriter_Dispose_m1_2379,
	TextWriter_Flush_m1_2380,
	TextWriter_Synchronized_m1_2381,
	TextWriter_Write_m1_2382,
	TextWriter_Write_m1_2383,
	TextWriter_Write_m1_2384,
	TextWriter_Write_m1_2385,
	TextWriter_Write_m1_2386,
	TextWriter_WriteLine_m1_2387,
	TextWriter_WriteLine_m1_2388,
	TextWriter_WriteLine_m1_2389,
	SynchronizedWriter__ctor_m1_2390,
	SynchronizedWriter_Close_m1_2391,
	SynchronizedWriter_Flush_m1_2392,
	SynchronizedWriter_Write_m1_2393,
	SynchronizedWriter_Write_m1_2394,
	SynchronizedWriter_Write_m1_2395,
	SynchronizedWriter_Write_m1_2396,
	SynchronizedWriter_Write_m1_2397,
	SynchronizedWriter_WriteLine_m1_2398,
	SynchronizedWriter_WriteLine_m1_2399,
	SynchronizedWriter_WriteLine_m1_2400,
	UnexceptionalStreamReader__ctor_m1_2401,
	UnexceptionalStreamReader__cctor_m1_2402,
	UnexceptionalStreamReader_Peek_m1_2403,
	UnexceptionalStreamReader_Read_m1_2404,
	UnexceptionalStreamReader_Read_m1_2405,
	UnexceptionalStreamReader_CheckEOL_m1_2406,
	UnexceptionalStreamReader_ReadLine_m1_2407,
	UnexceptionalStreamReader_ReadToEnd_m1_2408,
	UnexceptionalStreamWriter__ctor_m1_2409,
	UnexceptionalStreamWriter_Flush_m1_2410,
	UnexceptionalStreamWriter_Write_m1_2411,
	UnexceptionalStreamWriter_Write_m1_2412,
	UnexceptionalStreamWriter_Write_m1_2413,
	UnexceptionalStreamWriter_Write_m1_2414,
	UnmanagedMemoryStream_get_CanRead_m1_2415,
	UnmanagedMemoryStream_get_CanSeek_m1_2416,
	UnmanagedMemoryStream_get_CanWrite_m1_2417,
	UnmanagedMemoryStream_get_Length_m1_2418,
	UnmanagedMemoryStream_get_Position_m1_2419,
	UnmanagedMemoryStream_set_Position_m1_2420,
	UnmanagedMemoryStream_Read_m1_2421,
	UnmanagedMemoryStream_ReadByte_m1_2422,
	UnmanagedMemoryStream_Seek_m1_2423,
	UnmanagedMemoryStream_SetLength_m1_2424,
	UnmanagedMemoryStream_Flush_m1_2425,
	UnmanagedMemoryStream_Dispose_m1_2426,
	UnmanagedMemoryStream_Write_m1_2427,
	UnmanagedMemoryStream_WriteByte_m1_2428,
	AssemblyBuilder_get_Location_m1_2429,
	AssemblyBuilder_GetModulesInternal_m1_2430,
	AssemblyBuilder_GetTypes_m1_2431,
	AssemblyBuilder_get_IsCompilerContext_m1_2432,
	AssemblyBuilder_not_supported_m1_2433,
	AssemblyBuilder_UnprotectedGetName_m1_2434,
	ConstructorBuilder__ctor_m1_2435,
	ConstructorBuilder_get_CallingConvention_m1_2436,
	ConstructorBuilder_get_TypeBuilder_m1_2437,
	ConstructorBuilder_GetParameters_m1_2438,
	ConstructorBuilder_GetParametersInternal_m1_2439,
	ConstructorBuilder_GetParameterCount_m1_2440,
	ConstructorBuilder_Invoke_m1_2441,
	ConstructorBuilder_Invoke_m1_2442,
	ConstructorBuilder_get_MethodHandle_m1_2443,
	ConstructorBuilder_get_Attributes_m1_2444,
	ConstructorBuilder_get_ReflectedType_m1_2445,
	ConstructorBuilder_get_DeclaringType_m1_2446,
	ConstructorBuilder_get_Name_m1_2447,
	ConstructorBuilder_IsDefined_m1_2448,
	ConstructorBuilder_GetCustomAttributes_m1_2449,
	ConstructorBuilder_GetCustomAttributes_m1_2450,
	ConstructorBuilder_GetILGenerator_m1_2451,
	ConstructorBuilder_GetILGenerator_m1_2452,
	ConstructorBuilder_GetToken_m1_2453,
	ConstructorBuilder_get_Module_m1_2454,
	ConstructorBuilder_ToString_m1_2455,
	ConstructorBuilder_fixup_m1_2456,
	ConstructorBuilder_get_next_table_index_m1_2457,
	ConstructorBuilder_get_IsCompilerContext_m1_2458,
	ConstructorBuilder_not_supported_m1_2459,
	ConstructorBuilder_not_created_m1_2460,
	EnumBuilder_get_Assembly_m1_2461,
	EnumBuilder_get_AssemblyQualifiedName_m1_2462,
	EnumBuilder_get_BaseType_m1_2463,
	EnumBuilder_get_DeclaringType_m1_2464,
	EnumBuilder_get_FullName_m1_2465,
	EnumBuilder_get_Module_m1_2466,
	EnumBuilder_get_Name_m1_2467,
	EnumBuilder_get_Namespace_m1_2468,
	EnumBuilder_get_ReflectedType_m1_2469,
	EnumBuilder_get_TypeHandle_m1_2470,
	EnumBuilder_get_UnderlyingSystemType_m1_2471,
	EnumBuilder_GetAttributeFlagsImpl_m1_2472,
	EnumBuilder_GetConstructorImpl_m1_2473,
	EnumBuilder_GetConstructors_m1_2474,
	EnumBuilder_GetCustomAttributes_m1_2475,
	EnumBuilder_GetCustomAttributes_m1_2476,
	EnumBuilder_GetElementType_m1_2477,
	EnumBuilder_GetEvent_m1_2478,
	EnumBuilder_GetField_m1_2479,
	EnumBuilder_GetFields_m1_2480,
	EnumBuilder_GetInterfaces_m1_2481,
	EnumBuilder_GetMethodImpl_m1_2482,
	EnumBuilder_GetMethods_m1_2483,
	EnumBuilder_GetPropertyImpl_m1_2484,
	EnumBuilder_HasElementTypeImpl_m1_2485,
	EnumBuilder_InvokeMember_m1_2486,
	EnumBuilder_IsArrayImpl_m1_2487,
	EnumBuilder_IsByRefImpl_m1_2488,
	EnumBuilder_IsPointerImpl_m1_2489,
	EnumBuilder_IsPrimitiveImpl_m1_2490,
	EnumBuilder_IsValueTypeImpl_m1_2491,
	EnumBuilder_IsDefined_m1_2492,
	EnumBuilder_CreateNotSupportedException_m1_2493,
	FieldBuilder_get_Attributes_m1_2494,
	FieldBuilder_get_DeclaringType_m1_2495,
	FieldBuilder_get_FieldHandle_m1_2496,
	FieldBuilder_get_FieldType_m1_2497,
	FieldBuilder_get_Name_m1_2498,
	FieldBuilder_get_ReflectedType_m1_2499,
	FieldBuilder_GetCustomAttributes_m1_2500,
	FieldBuilder_GetCustomAttributes_m1_2501,
	FieldBuilder_GetValue_m1_2502,
	FieldBuilder_IsDefined_m1_2503,
	FieldBuilder_GetFieldOffset_m1_2504,
	FieldBuilder_SetValue_m1_2505,
	FieldBuilder_get_UMarshal_m1_2506,
	FieldBuilder_CreateNotSupportedException_m1_2507,
	FieldBuilder_get_Module_m1_2508,
	GenericTypeParameterBuilder_IsSubclassOf_m1_2509,
	GenericTypeParameterBuilder_GetAttributeFlagsImpl_m1_2510,
	GenericTypeParameterBuilder_GetConstructorImpl_m1_2511,
	GenericTypeParameterBuilder_GetConstructors_m1_2512,
	GenericTypeParameterBuilder_GetEvent_m1_2513,
	GenericTypeParameterBuilder_GetField_m1_2514,
	GenericTypeParameterBuilder_GetFields_m1_2515,
	GenericTypeParameterBuilder_GetInterfaces_m1_2516,
	GenericTypeParameterBuilder_GetMethods_m1_2517,
	GenericTypeParameterBuilder_GetMethodImpl_m1_2518,
	GenericTypeParameterBuilder_GetPropertyImpl_m1_2519,
	GenericTypeParameterBuilder_HasElementTypeImpl_m1_2520,
	GenericTypeParameterBuilder_IsAssignableFrom_m1_2521,
	GenericTypeParameterBuilder_IsInstanceOfType_m1_2522,
	GenericTypeParameterBuilder_IsArrayImpl_m1_2523,
	GenericTypeParameterBuilder_IsByRefImpl_m1_2524,
	GenericTypeParameterBuilder_IsPointerImpl_m1_2525,
	GenericTypeParameterBuilder_IsPrimitiveImpl_m1_2526,
	GenericTypeParameterBuilder_IsValueTypeImpl_m1_2527,
	GenericTypeParameterBuilder_InvokeMember_m1_2528,
	GenericTypeParameterBuilder_GetElementType_m1_2529,
	GenericTypeParameterBuilder_get_UnderlyingSystemType_m1_2530,
	GenericTypeParameterBuilder_get_Assembly_m1_2531,
	GenericTypeParameterBuilder_get_AssemblyQualifiedName_m1_2532,
	GenericTypeParameterBuilder_get_BaseType_m1_2533,
	GenericTypeParameterBuilder_get_FullName_m1_2534,
	GenericTypeParameterBuilder_IsDefined_m1_2535,
	GenericTypeParameterBuilder_GetCustomAttributes_m1_2536,
	GenericTypeParameterBuilder_GetCustomAttributes_m1_2537,
	GenericTypeParameterBuilder_get_Name_m1_2538,
	GenericTypeParameterBuilder_get_Namespace_m1_2539,
	GenericTypeParameterBuilder_get_Module_m1_2540,
	GenericTypeParameterBuilder_get_DeclaringType_m1_2541,
	GenericTypeParameterBuilder_get_ReflectedType_m1_2542,
	GenericTypeParameterBuilder_get_TypeHandle_m1_2543,
	GenericTypeParameterBuilder_GetGenericArguments_m1_2544,
	GenericTypeParameterBuilder_GetGenericTypeDefinition_m1_2545,
	GenericTypeParameterBuilder_get_ContainsGenericParameters_m1_2546,
	GenericTypeParameterBuilder_get_IsGenericParameter_m1_2547,
	GenericTypeParameterBuilder_get_IsGenericType_m1_2548,
	GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m1_2549,
	GenericTypeParameterBuilder_not_supported_m1_2550,
	GenericTypeParameterBuilder_ToString_m1_2551,
	GenericTypeParameterBuilder_Equals_m1_2552,
	GenericTypeParameterBuilder_GetHashCode_m1_2553,
	GenericTypeParameterBuilder_MakeGenericType_m1_2554,
	ILGenerator__ctor_m1_2555,
	ILGenerator__cctor_m1_2556,
	ILGenerator_add_token_fixup_m1_2557,
	ILGenerator_make_room_m1_2558,
	ILGenerator_emit_int_m1_2559,
	ILGenerator_ll_emit_m1_2560,
	ILGenerator_Emit_m1_2561,
	ILGenerator_Emit_m1_2562,
	ILGenerator_label_fixup_m1_2563,
	ILGenerator_Mono_GetCurrentOffset_m1_2564,
	MethodBuilder_get_ContainsGenericParameters_m1_2565,
	MethodBuilder_get_MethodHandle_m1_2566,
	MethodBuilder_get_ReturnType_m1_2567,
	MethodBuilder_get_ReflectedType_m1_2568,
	MethodBuilder_get_DeclaringType_m1_2569,
	MethodBuilder_get_Name_m1_2570,
	MethodBuilder_get_Attributes_m1_2571,
	MethodBuilder_get_CallingConvention_m1_2572,
	MethodBuilder_GetBaseDefinition_m1_2573,
	MethodBuilder_GetParameters_m1_2574,
	MethodBuilder_GetParameterCount_m1_2575,
	MethodBuilder_Invoke_m1_2576,
	MethodBuilder_IsDefined_m1_2577,
	MethodBuilder_GetCustomAttributes_m1_2578,
	MethodBuilder_GetCustomAttributes_m1_2579,
	MethodBuilder_check_override_m1_2580,
	MethodBuilder_fixup_m1_2581,
	MethodBuilder_ToString_m1_2582,
	MethodBuilder_Equals_m1_2583,
	MethodBuilder_GetHashCode_m1_2584,
	MethodBuilder_get_next_table_index_m1_2585,
	MethodBuilder_NotSupported_m1_2586,
	MethodBuilder_MakeGenericMethod_m1_2587,
	MethodBuilder_get_IsGenericMethodDefinition_m1_2588,
	MethodBuilder_get_IsGenericMethod_m1_2589,
	MethodBuilder_GetGenericArguments_m1_2590,
	MethodBuilder_get_Module_m1_2591,
	MethodToken__ctor_m1_2592,
	MethodToken__cctor_m1_2593,
	MethodToken_Equals_m1_2594,
	MethodToken_GetHashCode_m1_2595,
	MethodToken_get_Token_m1_2596,
	ModuleBuilder__cctor_m1_2597,
	ModuleBuilder_get_next_table_index_m1_2598,
	ModuleBuilder_GetTypes_m1_2599,
	ModuleBuilder_getToken_m1_2600,
	ModuleBuilder_GetToken_m1_2601,
	ModuleBuilder_RegisterToken_m1_2602,
	ModuleBuilder_GetTokenGenerator_m1_2603,
	ModuleBuilderTokenGenerator__ctor_m1_2604,
	ModuleBuilderTokenGenerator_GetToken_m1_2605,
	OpCode__ctor_m1_2606,
	OpCode_GetHashCode_m1_2607,
	OpCode_Equals_m1_2608,
	OpCode_ToString_m1_2609,
	OpCode_get_Name_m1_2610,
	OpCode_get_Size_m1_2611,
	OpCode_get_StackBehaviourPop_m1_2612,
	OpCode_get_StackBehaviourPush_m1_2613,
	OpCodeNames__cctor_m1_2614,
	OpCodes__cctor_m1_2615,
	ParameterBuilder_get_Attributes_m1_2616,
	ParameterBuilder_get_Name_m1_2617,
	ParameterBuilder_get_Position_m1_2618,
	TypeBuilder_GetAttributeFlagsImpl_m1_2619,
	TypeBuilder_setup_internal_class_m1_2620,
	TypeBuilder_create_generic_class_m1_2621,
	TypeBuilder_get_Assembly_m1_2622,
	TypeBuilder_get_AssemblyQualifiedName_m1_2623,
	TypeBuilder_get_BaseType_m1_2624,
	TypeBuilder_get_DeclaringType_m1_2625,
	TypeBuilder_get_UnderlyingSystemType_m1_2626,
	TypeBuilder_get_FullName_m1_2627,
	TypeBuilder_get_Module_m1_2628,
	TypeBuilder_get_Name_m1_2629,
	TypeBuilder_get_Namespace_m1_2630,
	TypeBuilder_get_ReflectedType_m1_2631,
	TypeBuilder_GetConstructorImpl_m1_2632,
	TypeBuilder_IsDefined_m1_2633,
	TypeBuilder_GetCustomAttributes_m1_2634,
	TypeBuilder_GetCustomAttributes_m1_2635,
	TypeBuilder_DefineConstructor_m1_2636,
	TypeBuilder_DefineConstructor_m1_2637,
	TypeBuilder_DefineDefaultConstructor_m1_2638,
	TypeBuilder_create_runtime_class_m1_2639,
	TypeBuilder_is_nested_in_m1_2640,
	TypeBuilder_has_ctor_method_m1_2641,
	TypeBuilder_CreateType_m1_2642,
	TypeBuilder_GetConstructors_m1_2643,
	TypeBuilder_GetConstructorsInternal_m1_2644,
	TypeBuilder_GetElementType_m1_2645,
	TypeBuilder_GetEvent_m1_2646,
	TypeBuilder_GetField_m1_2647,
	TypeBuilder_GetFields_m1_2648,
	TypeBuilder_GetInterfaces_m1_2649,
	TypeBuilder_GetMethodsByName_m1_2650,
	TypeBuilder_GetMethods_m1_2651,
	TypeBuilder_GetMethodImpl_m1_2652,
	TypeBuilder_GetPropertyImpl_m1_2653,
	TypeBuilder_HasElementTypeImpl_m1_2654,
	TypeBuilder_InvokeMember_m1_2655,
	TypeBuilder_IsArrayImpl_m1_2656,
	TypeBuilder_IsByRefImpl_m1_2657,
	TypeBuilder_IsPointerImpl_m1_2658,
	TypeBuilder_IsPrimitiveImpl_m1_2659,
	TypeBuilder_IsValueTypeImpl_m1_2660,
	TypeBuilder_MakeGenericType_m1_2661,
	TypeBuilder_get_TypeHandle_m1_2662,
	TypeBuilder_SetParent_m1_2663,
	TypeBuilder_get_next_table_index_m1_2664,
	TypeBuilder_get_IsCompilerContext_m1_2665,
	TypeBuilder_get_is_created_m1_2666,
	TypeBuilder_not_supported_m1_2667,
	TypeBuilder_check_not_created_m1_2668,
	TypeBuilder_check_created_m1_2669,
	TypeBuilder_ToString_m1_2670,
	TypeBuilder_IsAssignableFrom_m1_2671,
	TypeBuilder_IsSubclassOf_m1_2672,
	TypeBuilder_IsAssignableTo_m1_2673,
	TypeBuilder_GetGenericArguments_m1_2674,
	TypeBuilder_GetGenericTypeDefinition_m1_2675,
	TypeBuilder_get_ContainsGenericParameters_m1_2676,
	TypeBuilder_get_IsGenericParameter_m1_2677,
	TypeBuilder_get_IsGenericTypeDefinition_m1_2678,
	TypeBuilder_get_IsGenericType_m1_2679,
	UnmanagedMarshal_ToMarshalAsAttribute_m1_2680,
	AmbiguousMatchException__ctor_m1_2681,
	AmbiguousMatchException__ctor_m1_2682,
	AmbiguousMatchException__ctor_m1_2683,
	ResolveEventHolder__ctor_m1_2684,
	Assembly__ctor_m1_2685,
	Assembly_get_code_base_m1_2686,
	Assembly_get_fullname_m1_2687,
	Assembly_get_location_m1_2688,
	Assembly_GetCodeBase_m1_2689,
	Assembly_get_FullName_m1_2690,
	Assembly_get_Location_m1_2691,
	Assembly_IsDefined_m1_2692,
	Assembly_GetCustomAttributes_m1_2693,
	Assembly_GetManifestResourceInternal_m1_2694,
	Assembly_GetTypes_m1_2695,
	Assembly_GetTypes_m1_2696,
	Assembly_GetType_m1_2697,
	Assembly_GetType_m1_2698,
	Assembly_InternalGetType_m1_2699,
	Assembly_GetType_m1_2700,
	Assembly_FillName_m1_2701,
	Assembly_GetName_m1_2702,
	Assembly_GetName_m1_2703,
	Assembly_UnprotectedGetName_m1_2704,
	Assembly_ToString_m1_2705,
	Assembly_Load_m1_2706,
	Assembly_GetModule_m1_2707,
	Assembly_GetModulesInternal_m1_2708,
	Assembly_GetModules_m1_2709,
	Assembly_GetExecutingAssembly_m1_2710,
	AssemblyCompanyAttribute__ctor_m1_2711,
	AssemblyConfigurationAttribute__ctor_m1_2712,
	AssemblyCopyrightAttribute__ctor_m1_2713,
	AssemblyDefaultAliasAttribute__ctor_m1_2714,
	AssemblyDelaySignAttribute__ctor_m1_2715,
	AssemblyDescriptionAttribute__ctor_m1_2716,
	AssemblyFileVersionAttribute__ctor_m1_2717,
	AssemblyInformationalVersionAttribute__ctor_m1_2718,
	AssemblyKeyFileAttribute__ctor_m1_2719,
	AssemblyName__ctor_m1_2720,
	AssemblyName__ctor_m1_2721,
	AssemblyName_get_Name_m1_2722,
	AssemblyName_get_Flags_m1_2723,
	AssemblyName_get_FullName_m1_2724,
	AssemblyName_get_Version_m1_2725,
	AssemblyName_set_Version_m1_2726,
	AssemblyName_ToString_m1_2727,
	AssemblyName_get_IsPublicKeyValid_m1_2728,
	AssemblyName_InternalGetPublicKeyToken_m1_2729,
	AssemblyName_ComputePublicKeyToken_m1_2730,
	AssemblyName_SetPublicKey_m1_2731,
	AssemblyName_SetPublicKeyToken_m1_2732,
	AssemblyName_GetObjectData_m1_2733,
	AssemblyName_Clone_m1_2734,
	AssemblyName_OnDeserialization_m1_2735,
	AssemblyProductAttribute__ctor_m1_2736,
	AssemblyTitleAttribute__ctor_m1_2737,
	AssemblyTrademarkAttribute__ctor_m1_2738,
	Default__ctor_m1_2739,
	Default_BindToMethod_m1_2740,
	Default_ReorderParameters_m1_2741,
	Default_IsArrayAssignable_m1_2742,
	Default_ChangeType_m1_2743,
	Default_ReorderArgumentArray_m1_2744,
	Default_check_type_m1_2745,
	Default_check_arguments_m1_2746,
	Default_SelectMethod_m1_2747,
	Default_SelectMethod_m1_2748,
	Default_GetBetterMethod_m1_2749,
	Default_CompareCloserType_m1_2750,
	Default_SelectProperty_m1_2751,
	Default_check_arguments_with_score_m1_2752,
	Default_check_type_with_score_m1_2753,
	Binder__ctor_m1_2754,
	Binder__cctor_m1_2755,
	Binder_get_DefaultBinder_m1_2756,
	Binder_ConvertArgs_m1_2757,
	Binder_GetDerivedLevel_m1_2758,
	Binder_FindMostDerivedMatch_m1_2759,
	ConstructorInfo__ctor_m1_2760,
	ConstructorInfo__cctor_m1_2761,
	ConstructorInfo_get_MemberType_m1_2762,
	ConstructorInfo_Invoke_m1_2763,
	CustomAttributeData__ctor_m1_2764,
	CustomAttributeData_get_Constructor_m1_2765,
	CustomAttributeData_get_ConstructorArguments_m1_2766,
	CustomAttributeData_get_NamedArguments_m1_2767,
	CustomAttributeData_GetCustomAttributes_m1_2768,
	CustomAttributeData_GetCustomAttributes_m1_2769,
	CustomAttributeData_GetCustomAttributes_m1_2770,
	CustomAttributeData_GetCustomAttributes_m1_2771,
	CustomAttributeData_ToString_m1_2772,
	CustomAttributeData_Equals_m1_2773,
	CustomAttributeData_GetHashCode_m1_2774,
	CustomAttributeNamedArgument_ToString_m1_2775,
	CustomAttributeNamedArgument_Equals_m1_2776,
	CustomAttributeNamedArgument_GetHashCode_m1_2777,
	CustomAttributeTypedArgument_ToString_m1_2778,
	CustomAttributeTypedArgument_Equals_m1_2779,
	CustomAttributeTypedArgument_GetHashCode_m1_2780,
	AddEventAdapter__ctor_m1_2781,
	AddEventAdapter_Invoke_m1_2782,
	AddEventAdapter_BeginInvoke_m1_2783,
	AddEventAdapter_EndInvoke_m1_2784,
	EventInfo__ctor_m1_2785,
	EventInfo_get_EventHandlerType_m1_2786,
	EventInfo_get_MemberType_m1_2787,
	FieldInfo__ctor_m1_2788,
	FieldInfo_get_MemberType_m1_2789,
	FieldInfo_get_IsLiteral_m1_2790,
	FieldInfo_get_IsStatic_m1_2791,
	FieldInfo_get_IsNotSerialized_m1_2792,
	FieldInfo_SetValue_m1_2793,
	FieldInfo_internal_from_handle_type_m1_2794,
	FieldInfo_GetFieldFromHandle_m1_2795,
	FieldInfo_GetFieldOffset_m1_2796,
	FieldInfo_GetUnmanagedMarshal_m1_2797,
	FieldInfo_get_UMarshal_m1_2798,
	FieldInfo_GetPseudoCustomAttributes_m1_2799,
	MemberInfoSerializationHolder__ctor_m1_2800,
	MemberInfoSerializationHolder_Serialize_m1_2801,
	MemberInfoSerializationHolder_Serialize_m1_2802,
	MemberInfoSerializationHolder_GetObjectData_m1_2803,
	MemberInfoSerializationHolder_GetRealObject_m1_2804,
	MethodBase__ctor_m1_2805,
	MethodBase_GetMethodFromHandleNoGenericCheck_m1_2806,
	MethodBase_GetMethodFromIntPtr_m1_2807,
	MethodBase_GetMethodFromHandle_m1_2808,
	MethodBase_GetMethodFromHandleInternalType_m1_2809,
	MethodBase_GetParameterCount_m1_2810,
	MethodBase_Invoke_m1_2811,
	MethodBase_get_CallingConvention_m1_2812,
	MethodBase_get_IsPublic_m1_2813,
	MethodBase_get_IsStatic_m1_2814,
	MethodBase_get_IsVirtual_m1_2815,
	MethodBase_get_IsAbstract_m1_2816,
	MethodBase_get_next_table_index_m1_2817,
	MethodBase_GetGenericArguments_m1_2818,
	MethodBase_get_ContainsGenericParameters_m1_2819,
	MethodBase_get_IsGenericMethodDefinition_m1_2820,
	MethodBase_get_IsGenericMethod_m1_2821,
	MethodInfo__ctor_m1_2822,
	MethodInfo_get_MemberType_m1_2823,
	MethodInfo_get_ReturnType_m1_2824,
	MethodInfo_MakeGenericMethod_m1_2825,
	MethodInfo_GetGenericArguments_m1_2826,
	MethodInfo_get_IsGenericMethod_m1_2827,
	MethodInfo_get_IsGenericMethodDefinition_m1_2828,
	MethodInfo_get_ContainsGenericParameters_m1_2829,
	Missing__ctor_m1_2830,
	Missing__cctor_m1_2831,
	Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m1_2832,
	Module__ctor_m1_2833,
	Module__cctor_m1_2834,
	Module_get_Assembly_m1_2835,
	Module_get_ScopeName_m1_2836,
	Module_GetCustomAttributes_m1_2837,
	Module_GetObjectData_m1_2838,
	Module_InternalGetTypes_m1_2839,
	Module_GetTypes_m1_2840,
	Module_IsDefined_m1_2841,
	Module_IsResource_m1_2842,
	Module_ToString_m1_2843,
	Module_filter_by_type_name_m1_2844,
	Module_filter_by_type_name_ignore_case_m1_2845,
	MonoEventInfo_get_event_info_m1_2846,
	MonoEventInfo_GetEventInfo_m1_2847,
	MonoEvent__ctor_m1_2848,
	MonoEvent_get_Attributes_m1_2849,
	MonoEvent_GetAddMethod_m1_2850,
	MonoEvent_get_DeclaringType_m1_2851,
	MonoEvent_get_ReflectedType_m1_2852,
	MonoEvent_get_Name_m1_2853,
	MonoEvent_ToString_m1_2854,
	MonoEvent_IsDefined_m1_2855,
	MonoEvent_GetCustomAttributes_m1_2856,
	MonoEvent_GetCustomAttributes_m1_2857,
	MonoEvent_GetObjectData_m1_2858,
	MonoField__ctor_m1_2859,
	MonoField_get_Attributes_m1_2860,
	MonoField_get_FieldHandle_m1_2861,
	MonoField_get_FieldType_m1_2862,
	MonoField_GetParentType_m1_2863,
	MonoField_get_ReflectedType_m1_2864,
	MonoField_get_DeclaringType_m1_2865,
	MonoField_get_Name_m1_2866,
	MonoField_IsDefined_m1_2867,
	MonoField_GetCustomAttributes_m1_2868,
	MonoField_GetCustomAttributes_m1_2869,
	MonoField_GetFieldOffset_m1_2870,
	MonoField_GetValueInternal_m1_2871,
	MonoField_GetValue_m1_2872,
	MonoField_ToString_m1_2873,
	MonoField_SetValueInternal_m1_2874,
	MonoField_SetValue_m1_2875,
	MonoField_GetObjectData_m1_2876,
	MonoField_CheckGeneric_m1_2877,
	MonoGenericMethod__ctor_m1_2878,
	MonoGenericMethod_get_ReflectedType_m1_2879,
	MonoGenericCMethod__ctor_m1_2880,
	MonoGenericCMethod_get_ReflectedType_m1_2881,
	MonoMethodInfo_get_method_info_m1_2882,
	MonoMethodInfo_GetMethodInfo_m1_2883,
	MonoMethodInfo_GetDeclaringType_m1_2884,
	MonoMethodInfo_GetReturnType_m1_2885,
	MonoMethodInfo_GetAttributes_m1_2886,
	MonoMethodInfo_GetCallingConvention_m1_2887,
	MonoMethodInfo_get_parameter_info_m1_2888,
	MonoMethodInfo_GetParametersInfo_m1_2889,
	MonoMethod__ctor_m1_2890,
	MonoMethod_get_name_m1_2891,
	MonoMethod_get_base_definition_m1_2892,
	MonoMethod_GetBaseDefinition_m1_2893,
	MonoMethod_get_ReturnType_m1_2894,
	MonoMethod_GetParameters_m1_2895,
	MonoMethod_InternalInvoke_m1_2896,
	MonoMethod_Invoke_m1_2897,
	MonoMethod_get_MethodHandle_m1_2898,
	MonoMethod_get_Attributes_m1_2899,
	MonoMethod_get_CallingConvention_m1_2900,
	MonoMethod_get_ReflectedType_m1_2901,
	MonoMethod_get_DeclaringType_m1_2902,
	MonoMethod_get_Name_m1_2903,
	MonoMethod_IsDefined_m1_2904,
	MonoMethod_GetCustomAttributes_m1_2905,
	MonoMethod_GetCustomAttributes_m1_2906,
	MonoMethod_GetDllImportAttribute_m1_2907,
	MonoMethod_GetPseudoCustomAttributes_m1_2908,
	MonoMethod_ShouldPrintFullName_m1_2909,
	MonoMethod_ToString_m1_2910,
	MonoMethod_GetObjectData_m1_2911,
	MonoMethod_MakeGenericMethod_m1_2912,
	MonoMethod_MakeGenericMethod_impl_m1_2913,
	MonoMethod_GetGenericArguments_m1_2914,
	MonoMethod_get_IsGenericMethodDefinition_m1_2915,
	MonoMethod_get_IsGenericMethod_m1_2916,
	MonoMethod_get_ContainsGenericParameters_m1_2917,
	MonoCMethod__ctor_m1_2918,
	MonoCMethod_GetParameters_m1_2919,
	MonoCMethod_InternalInvoke_m1_2920,
	MonoCMethod_Invoke_m1_2921,
	MonoCMethod_Invoke_m1_2922,
	MonoCMethod_get_MethodHandle_m1_2923,
	MonoCMethod_get_Attributes_m1_2924,
	MonoCMethod_get_CallingConvention_m1_2925,
	MonoCMethod_get_ReflectedType_m1_2926,
	MonoCMethod_get_DeclaringType_m1_2927,
	MonoCMethod_get_Name_m1_2928,
	MonoCMethod_IsDefined_m1_2929,
	MonoCMethod_GetCustomAttributes_m1_2930,
	MonoCMethod_GetCustomAttributes_m1_2931,
	MonoCMethod_ToString_m1_2932,
	MonoCMethod_GetObjectData_m1_2933,
	MonoPropertyInfo_get_property_info_m1_2934,
	MonoPropertyInfo_GetTypeModifiers_m1_2935,
	GetterAdapter__ctor_m1_2936,
	GetterAdapter_Invoke_m1_2937,
	GetterAdapter_BeginInvoke_m1_2938,
	GetterAdapter_EndInvoke_m1_2939,
	MonoProperty__ctor_m1_2940,
	MonoProperty_CachePropertyInfo_m1_2941,
	MonoProperty_get_Attributes_m1_2942,
	MonoProperty_get_CanRead_m1_2943,
	MonoProperty_get_CanWrite_m1_2944,
	MonoProperty_get_PropertyType_m1_2945,
	MonoProperty_get_ReflectedType_m1_2946,
	MonoProperty_get_DeclaringType_m1_2947,
	MonoProperty_get_Name_m1_2948,
	MonoProperty_GetAccessors_m1_2949,
	MonoProperty_GetGetMethod_m1_2950,
	MonoProperty_GetIndexParameters_m1_2951,
	MonoProperty_GetSetMethod_m1_2952,
	MonoProperty_IsDefined_m1_2953,
	MonoProperty_GetCustomAttributes_m1_2954,
	MonoProperty_GetCustomAttributes_m1_2955,
	MonoProperty_CreateGetterDelegate_m1_2956,
	MonoProperty_GetValue_m1_2957,
	MonoProperty_GetValue_m1_2958,
	MonoProperty_SetValue_m1_2959,
	MonoProperty_ToString_m1_2960,
	MonoProperty_GetOptionalCustomModifiers_m1_2961,
	MonoProperty_GetRequiredCustomModifiers_m1_2962,
	MonoProperty_GetObjectData_m1_2963,
	ParameterInfo__ctor_m1_2964,
	ParameterInfo__ctor_m1_2965,
	ParameterInfo__ctor_m1_2966,
	ParameterInfo_ToString_m1_2967,
	ParameterInfo_get_ParameterType_m1_2968,
	ParameterInfo_get_Attributes_m1_2969,
	ParameterInfo_get_IsIn_m1_2970,
	ParameterInfo_get_IsOptional_m1_2971,
	ParameterInfo_get_IsOut_m1_2972,
	ParameterInfo_get_IsRetval_m1_2973,
	ParameterInfo_get_Member_m1_2974,
	ParameterInfo_get_Name_m1_2975,
	ParameterInfo_get_Position_m1_2976,
	ParameterInfo_GetCustomAttributes_m1_2977,
	ParameterInfo_IsDefined_m1_2978,
	ParameterInfo_GetPseudoCustomAttributes_m1_2979,
	Pointer__ctor_m1_2980,
	Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m1_2981,
	PropertyInfo__ctor_m1_2982,
	PropertyInfo_get_MemberType_m1_2983,
	PropertyInfo_GetValue_m1_2984,
	PropertyInfo_SetValue_m1_2985,
	PropertyInfo_GetOptionalCustomModifiers_m1_2986,
	PropertyInfo_GetRequiredCustomModifiers_m1_2987,
	StrongNameKeyPair__ctor_m1_2988,
	StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m1_2989,
	StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_2990,
	TargetException__ctor_m1_2991,
	TargetException__ctor_m1_2992,
	TargetException__ctor_m1_2993,
	TargetInvocationException__ctor_m1_2994,
	TargetInvocationException__ctor_m1_2995,
	TargetParameterCountException__ctor_m1_2996,
	TargetParameterCountException__ctor_m1_2997,
	TargetParameterCountException__ctor_m1_2998,
	NeutralResourcesLanguageAttribute__ctor_m1_2999,
	ResourceManager__ctor_m1_3000,
	ResourceManager__cctor_m1_3001,
	ResourceInfo__ctor_m1_3002,
	ResourceCacheItem__ctor_m1_3003,
	ResourceEnumerator__ctor_m1_3004,
	ResourceEnumerator_get_Entry_m1_3005,
	ResourceEnumerator_get_Key_m1_3006,
	ResourceEnumerator_get_Value_m1_3007,
	ResourceEnumerator_get_Current_m1_3008,
	ResourceEnumerator_MoveNext_m1_3009,
	ResourceEnumerator_Reset_m1_3010,
	ResourceEnumerator_FillCache_m1_3011,
	ResourceReader__ctor_m1_3012,
	ResourceReader__ctor_m1_3013,
	ResourceReader_System_Collections_IEnumerable_GetEnumerator_m1_3014,
	ResourceReader_System_IDisposable_Dispose_m1_3015,
	ResourceReader_ReadHeaders_m1_3016,
	ResourceReader_CreateResourceInfo_m1_3017,
	ResourceReader_Read7BitEncodedInt_m1_3018,
	ResourceReader_ReadValueVer2_m1_3019,
	ResourceReader_ReadValueVer1_m1_3020,
	ResourceReader_ReadNonPredefinedValue_m1_3021,
	ResourceReader_LoadResourceValues_m1_3022,
	ResourceReader_Close_m1_3023,
	ResourceReader_GetEnumerator_m1_3024,
	ResourceReader_Dispose_m1_3025,
	ResourceSet__ctor_m1_3026,
	ResourceSet__ctor_m1_3027,
	ResourceSet__ctor_m1_3028,
	ResourceSet__ctor_m1_3029,
	ResourceSet_System_Collections_IEnumerable_GetEnumerator_m1_3030,
	ResourceSet_Dispose_m1_3031,
	ResourceSet_Dispose_m1_3032,
	ResourceSet_GetEnumerator_m1_3033,
	ResourceSet_GetObjectInternal_m1_3034,
	ResourceSet_GetObject_m1_3035,
	ResourceSet_GetObject_m1_3036,
	ResourceSet_ReadResources_m1_3037,
	RuntimeResourceSet__ctor_m1_3038,
	RuntimeResourceSet__ctor_m1_3039,
	RuntimeResourceSet__ctor_m1_3040,
	RuntimeResourceSet_GetObject_m1_3041,
	RuntimeResourceSet_GetObject_m1_3042,
	RuntimeResourceSet_CloneDisposableObjectIfPossible_m1_3043,
	SatelliteContractVersionAttribute__ctor_m1_3044,
	CompilationRelaxationsAttribute__ctor_m1_3045,
	CompilationRelaxationsAttribute__ctor_m1_3046,
	DefaultDependencyAttribute__ctor_m1_3047,
	StringFreezingAttribute__ctor_m1_3048,
	CriticalFinalizerObject__ctor_m1_3049,
	CriticalFinalizerObject_Finalize_m1_3050,
	ReliabilityContractAttribute__ctor_m1_3051,
	ClassInterfaceAttribute__ctor_m1_3052,
	ComDefaultInterfaceAttribute__ctor_m1_3053,
	DispIdAttribute__ctor_m1_3054,
	ExternalException__ctor_m1_3055,
	ExternalException__ctor_m1_3056,
	ExternalException__ctor_m1_3057,
	GCHandle__ctor_m1_3058,
	GCHandle_get_IsAllocated_m1_3059,
	GCHandle_get_Target_m1_3060,
	GCHandle_Alloc_m1_3061,
	GCHandle_Free_m1_3062,
	GCHandle_GetTarget_m1_3063,
	GCHandle_GetTargetHandle_m1_3064,
	GCHandle_FreeHandle_m1_3065,
	GCHandle_Equals_m1_3066,
	GCHandle_GetHashCode_m1_3067,
	InterfaceTypeAttribute__ctor_m1_3068,
	Marshal__cctor_m1_3069,
	Marshal_copy_from_unmanaged_m1_3070,
	Marshal_Copy_m1_3071,
	Marshal_Copy_m1_3072,
	Marshal_GetLastWin32Error_m1_3073,
	Marshal_ReadByte_m1_3074,
	Marshal_WriteByte_m1_3075,
	MarshalDirectiveException__ctor_m1_3076,
	MarshalDirectiveException__ctor_m1_3077,
	PreserveSigAttribute__ctor_m1_3078,
	SafeHandle__ctor_m1_3079,
	SafeHandle_Close_m1_3080,
	SafeHandle_DangerousAddRef_m1_3081,
	SafeHandle_DangerousGetHandle_m1_3082,
	SafeHandle_DangerousRelease_m1_3083,
	SafeHandle_Dispose_m1_3084,
	SafeHandle_Dispose_m1_3085,
	SafeHandle_SetHandle_m1_3086,
	SafeHandle_Finalize_m1_3087,
	TypeLibImportClassAttribute__ctor_m1_3088,
	TypeLibVersionAttribute__ctor_m1_3089,
	ActivationServices_get_ConstructionActivator_m1_3090,
	ActivationServices_CreateProxyFromAttributes_m1_3091,
	ActivationServices_CreateConstructionCall_m1_3092,
	ActivationServices_AllocateUninitializedClassInstance_m1_3093,
	ActivationServices_EnableProxyActivation_m1_3094,
	AppDomainLevelActivator__ctor_m1_3095,
	ConstructionLevelActivator__ctor_m1_3096,
	ContextLevelActivator__ctor_m1_3097,
	UrlAttribute_get_UrlValue_m1_3098,
	UrlAttribute_Equals_m1_3099,
	UrlAttribute_GetHashCode_m1_3100,
	UrlAttribute_GetPropertiesForNewContext_m1_3101,
	UrlAttribute_IsContextOK_m1_3102,
	ChannelInfo__ctor_m1_3103,
	ChannelInfo_get_ChannelData_m1_3104,
	ChannelServices__cctor_m1_3105,
	ChannelServices_CreateClientChannelSinkChain_m1_3106,
	ChannelServices_CreateClientChannelSinkChain_m1_3107,
	ChannelServices_RegisterChannel_m1_3108,
	ChannelServices_RegisterChannel_m1_3109,
	ChannelServices_RegisterChannelConfig_m1_3110,
	ChannelServices_CreateProvider_m1_3111,
	ChannelServices_GetCurrentChannelInfo_m1_3112,
	CrossAppDomainData__ctor_m1_3113,
	CrossAppDomainData_get_DomainID_m1_3114,
	CrossAppDomainData_get_ProcessID_m1_3115,
	CrossAppDomainChannel__ctor_m1_3116,
	CrossAppDomainChannel__cctor_m1_3117,
	CrossAppDomainChannel_RegisterCrossAppDomainChannel_m1_3118,
	CrossAppDomainChannel_get_ChannelName_m1_3119,
	CrossAppDomainChannel_get_ChannelPriority_m1_3120,
	CrossAppDomainChannel_get_ChannelData_m1_3121,
	CrossAppDomainChannel_StartListening_m1_3122,
	CrossAppDomainChannel_CreateMessageSink_m1_3123,
	CrossAppDomainSink__ctor_m1_3124,
	CrossAppDomainSink__cctor_m1_3125,
	CrossAppDomainSink_GetSink_m1_3126,
	CrossAppDomainSink_get_TargetDomainId_m1_3127,
	SinkProviderData__ctor_m1_3128,
	SinkProviderData_get_Children_m1_3129,
	SinkProviderData_get_Properties_m1_3130,
	Context__ctor_m1_3131,
	Context__cctor_m1_3132,
	Context_Finalize_m1_3133,
	Context_get_DefaultContext_m1_3134,
	Context_get_ContextID_m1_3135,
	Context_get_ContextProperties_m1_3136,
	Context_get_IsDefaultContext_m1_3137,
	Context_get_NeedsContextSink_m1_3138,
	Context_RegisterDynamicProperty_m1_3139,
	Context_UnregisterDynamicProperty_m1_3140,
	Context_GetDynamicPropertyCollection_m1_3141,
	Context_NotifyGlobalDynamicSinks_m1_3142,
	Context_get_HasGlobalDynamicSinks_m1_3143,
	Context_NotifyDynamicSinks_m1_3144,
	Context_get_HasDynamicSinks_m1_3145,
	Context_get_HasExitSinks_m1_3146,
	Context_GetProperty_m1_3147,
	Context_SetProperty_m1_3148,
	Context_Freeze_m1_3149,
	Context_ToString_m1_3150,
	Context_GetServerContextSinkChain_m1_3151,
	Context_GetClientContextSinkChain_m1_3152,
	Context_CreateServerObjectSinkChain_m1_3153,
	Context_CreateEnvoySink_m1_3154,
	Context_SwitchToContext_m1_3155,
	Context_CreateNewContext_m1_3156,
	Context_DoCallBack_m1_3157,
	Context_AllocateDataSlot_m1_3158,
	Context_AllocateNamedDataSlot_m1_3159,
	Context_FreeNamedDataSlot_m1_3160,
	Context_GetData_m1_3161,
	Context_GetNamedDataSlot_m1_3162,
	Context_SetData_m1_3163,
	DynamicPropertyReg__ctor_m1_3164,
	DynamicPropertyCollection__ctor_m1_3165,
	DynamicPropertyCollection_get_HasProperties_m1_3166,
	DynamicPropertyCollection_RegisterDynamicProperty_m1_3167,
	DynamicPropertyCollection_UnregisterDynamicProperty_m1_3168,
	DynamicPropertyCollection_NotifyMessage_m1_3169,
	DynamicPropertyCollection_FindProperty_m1_3170,
	ContextCallbackObject__ctor_m1_3171,
	ContextCallbackObject_DoCallBack_m1_3172,
	ContextAttribute__ctor_m1_3173,
	ContextAttribute_get_Name_m1_3174,
	ContextAttribute_Equals_m1_3175,
	ContextAttribute_Freeze_m1_3176,
	ContextAttribute_GetHashCode_m1_3177,
	ContextAttribute_GetPropertiesForNewContext_m1_3178,
	ContextAttribute_IsContextOK_m1_3179,
	ContextAttribute_IsNewContextOK_m1_3180,
	CrossContextChannel__ctor_m1_3181,
	SynchronizationAttribute__ctor_m1_3182,
	SynchronizationAttribute__ctor_m1_3183,
	SynchronizationAttribute_set_Locked_m1_3184,
	SynchronizationAttribute_ReleaseLock_m1_3185,
	SynchronizationAttribute_GetPropertiesForNewContext_m1_3186,
	SynchronizationAttribute_GetClientContextSink_m1_3187,
	SynchronizationAttribute_GetServerContextSink_m1_3188,
	SynchronizationAttribute_IsContextOK_m1_3189,
	SynchronizationAttribute_ExitContext_m1_3190,
	SynchronizationAttribute_EnterContext_m1_3191,
	SynchronizedClientContextSink__ctor_m1_3192,
	SynchronizedServerContextSink__ctor_m1_3193,
	RenewalDelegate__ctor_m1_3194,
	RenewalDelegate_Invoke_m1_3195,
	RenewalDelegate_BeginInvoke_m1_3196,
	RenewalDelegate_EndInvoke_m1_3197,
	LeaseManager__ctor_m1_3198,
	LeaseManager_SetPollTime_m1_3199,
	LeaseSink__ctor_m1_3200,
	LifetimeServices__cctor_m1_3201,
	LifetimeServices_set_LeaseManagerPollTime_m1_3202,
	LifetimeServices_set_LeaseTime_m1_3203,
	LifetimeServices_set_RenewOnCallTime_m1_3204,
	LifetimeServices_set_SponsorshipTimeout_m1_3205,
	ArgInfo__ctor_m1_3206,
	ArgInfo_GetInOutArgs_m1_3207,
	AsyncResult__ctor_m1_3208,
	AsyncResult_get_AsyncState_m1_3209,
	AsyncResult_get_AsyncWaitHandle_m1_3210,
	AsyncResult_get_CompletedSynchronously_m1_3211,
	AsyncResult_get_IsCompleted_m1_3212,
	AsyncResult_get_EndInvokeCalled_m1_3213,
	AsyncResult_set_EndInvokeCalled_m1_3214,
	AsyncResult_get_AsyncDelegate_m1_3215,
	AsyncResult_get_NextSink_m1_3216,
	AsyncResult_AsyncProcessMessage_m1_3217,
	AsyncResult_GetReplyMessage_m1_3218,
	AsyncResult_SetMessageCtrl_m1_3219,
	AsyncResult_SetCompletedSynchronously_m1_3220,
	AsyncResult_EndInvoke_m1_3221,
	AsyncResult_SyncProcessMessage_m1_3222,
	AsyncResult_get_CallMessage_m1_3223,
	AsyncResult_set_CallMessage_m1_3224,
	ClientContextTerminatorSink__ctor_m1_3225,
	ConstructionCall__ctor_m1_3226,
	ConstructionCall__ctor_m1_3227,
	ConstructionCall_InitDictionary_m1_3228,
	ConstructionCall_set_IsContextOk_m1_3229,
	ConstructionCall_get_ActivationType_m1_3230,
	ConstructionCall_get_ActivationTypeName_m1_3231,
	ConstructionCall_get_Activator_m1_3232,
	ConstructionCall_set_Activator_m1_3233,
	ConstructionCall_get_CallSiteActivationAttributes_m1_3234,
	ConstructionCall_SetActivationAttributes_m1_3235,
	ConstructionCall_get_ContextProperties_m1_3236,
	ConstructionCall_InitMethodProperty_m1_3237,
	ConstructionCall_GetObjectData_m1_3238,
	ConstructionCall_get_Properties_m1_3239,
	ConstructionCallDictionary__ctor_m1_3240,
	ConstructionCallDictionary__cctor_m1_3241,
	ConstructionCallDictionary_GetMethodProperty_m1_3242,
	ConstructionCallDictionary_SetMethodProperty_m1_3243,
	EnvoyTerminatorSink__ctor_m1_3244,
	EnvoyTerminatorSink__cctor_m1_3245,
	Header__ctor_m1_3246,
	Header__ctor_m1_3247,
	Header__ctor_m1_3248,
	LogicalCallContext__ctor_m1_3249,
	LogicalCallContext__ctor_m1_3250,
	LogicalCallContext_GetObjectData_m1_3251,
	LogicalCallContext_SetData_m1_3252,
	LogicalCallContext_Clone_m1_3253,
	CallContextRemotingData__ctor_m1_3254,
	CallContextRemotingData_Clone_m1_3255,
	MethodCall__ctor_m1_3256,
	MethodCall__ctor_m1_3257,
	MethodCall__ctor_m1_3258,
	MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m1_3259,
	MethodCall_InitMethodProperty_m1_3260,
	MethodCall_GetObjectData_m1_3261,
	MethodCall_get_Args_m1_3262,
	MethodCall_get_LogicalCallContext_m1_3263,
	MethodCall_get_MethodBase_m1_3264,
	MethodCall_get_MethodName_m1_3265,
	MethodCall_get_MethodSignature_m1_3266,
	MethodCall_get_Properties_m1_3267,
	MethodCall_InitDictionary_m1_3268,
	MethodCall_get_TypeName_m1_3269,
	MethodCall_get_Uri_m1_3270,
	MethodCall_set_Uri_m1_3271,
	MethodCall_Init_m1_3272,
	MethodCall_ResolveMethod_m1_3273,
	MethodCall_CastTo_m1_3274,
	MethodCall_GetTypeNameFromAssemblyQualifiedName_m1_3275,
	MethodCall_get_GenericArguments_m1_3276,
	MethodCallDictionary__ctor_m1_3277,
	MethodCallDictionary__cctor_m1_3278,
	DictionaryEnumerator__ctor_m1_3279,
	DictionaryEnumerator_get_Current_m1_3280,
	DictionaryEnumerator_MoveNext_m1_3281,
	DictionaryEnumerator_Reset_m1_3282,
	DictionaryEnumerator_get_Entry_m1_3283,
	DictionaryEnumerator_get_Key_m1_3284,
	DictionaryEnumerator_get_Value_m1_3285,
	MethodDictionary__ctor_m1_3286,
	MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m1_3287,
	MethodDictionary_set_MethodKeys_m1_3288,
	MethodDictionary_AllocInternalProperties_m1_3289,
	MethodDictionary_GetInternalProperties_m1_3290,
	MethodDictionary_IsOverridenKey_m1_3291,
	MethodDictionary_get_Item_m1_3292,
	MethodDictionary_set_Item_m1_3293,
	MethodDictionary_GetMethodProperty_m1_3294,
	MethodDictionary_SetMethodProperty_m1_3295,
	MethodDictionary_get_Keys_m1_3296,
	MethodDictionary_get_Values_m1_3297,
	MethodDictionary_Add_m1_3298,
	MethodDictionary_Contains_m1_3299,
	MethodDictionary_Remove_m1_3300,
	MethodDictionary_get_Count_m1_3301,
	MethodDictionary_get_SyncRoot_m1_3302,
	MethodDictionary_CopyTo_m1_3303,
	MethodDictionary_GetEnumerator_m1_3304,
	MethodReturnDictionary__ctor_m1_3305,
	MethodReturnDictionary__cctor_m1_3306,
	MonoMethodMessage_get_Args_m1_3307,
	MonoMethodMessage_get_LogicalCallContext_m1_3308,
	MonoMethodMessage_get_MethodBase_m1_3309,
	MonoMethodMessage_get_MethodName_m1_3310,
	MonoMethodMessage_get_MethodSignature_m1_3311,
	MonoMethodMessage_get_TypeName_m1_3312,
	MonoMethodMessage_get_Uri_m1_3313,
	MonoMethodMessage_set_Uri_m1_3314,
	MonoMethodMessage_get_Exception_m1_3315,
	MonoMethodMessage_get_OutArgCount_m1_3316,
	MonoMethodMessage_get_OutArgs_m1_3317,
	MonoMethodMessage_get_ReturnValue_m1_3318,
	RemotingSurrogate__ctor_m1_3319,
	RemotingSurrogate_SetObjectData_m1_3320,
	ObjRefSurrogate__ctor_m1_3321,
	ObjRefSurrogate_SetObjectData_m1_3322,
	RemotingSurrogateSelector__ctor_m1_3323,
	RemotingSurrogateSelector__cctor_m1_3324,
	RemotingSurrogateSelector_GetSurrogate_m1_3325,
	ReturnMessage__ctor_m1_3326,
	ReturnMessage__ctor_m1_3327,
	ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m1_3328,
	ReturnMessage_get_Args_m1_3329,
	ReturnMessage_get_LogicalCallContext_m1_3330,
	ReturnMessage_get_MethodBase_m1_3331,
	ReturnMessage_get_MethodName_m1_3332,
	ReturnMessage_get_MethodSignature_m1_3333,
	ReturnMessage_get_Properties_m1_3334,
	ReturnMessage_get_TypeName_m1_3335,
	ReturnMessage_get_Uri_m1_3336,
	ReturnMessage_set_Uri_m1_3337,
	ReturnMessage_get_Exception_m1_3338,
	ReturnMessage_get_OutArgs_m1_3339,
	ReturnMessage_get_ReturnValue_m1_3340,
	ServerContextTerminatorSink__ctor_m1_3341,
	ServerObjectTerminatorSink__ctor_m1_3342,
	StackBuilderSink__ctor_m1_3343,
	SoapAttribute__ctor_m1_3344,
	SoapAttribute_get_UseAttribute_m1_3345,
	SoapAttribute_get_XmlNamespace_m1_3346,
	SoapAttribute_SetReflectionObject_m1_3347,
	SoapFieldAttribute__ctor_m1_3348,
	SoapFieldAttribute_get_XmlElementName_m1_3349,
	SoapFieldAttribute_IsInteropXmlElement_m1_3350,
	SoapFieldAttribute_SetReflectionObject_m1_3351,
	SoapMethodAttribute__ctor_m1_3352,
	SoapMethodAttribute_get_UseAttribute_m1_3353,
	SoapMethodAttribute_get_XmlNamespace_m1_3354,
	SoapMethodAttribute_SetReflectionObject_m1_3355,
	SoapParameterAttribute__ctor_m1_3356,
	SoapTypeAttribute__ctor_m1_3357,
	SoapTypeAttribute_get_UseAttribute_m1_3358,
	SoapTypeAttribute_get_XmlElementName_m1_3359,
	SoapTypeAttribute_get_XmlNamespace_m1_3360,
	SoapTypeAttribute_get_XmlTypeName_m1_3361,
	SoapTypeAttribute_get_XmlTypeNamespace_m1_3362,
	SoapTypeAttribute_get_IsInteropXmlElement_m1_3363,
	SoapTypeAttribute_get_IsInteropXmlType_m1_3364,
	SoapTypeAttribute_SetReflectionObject_m1_3365,
	ProxyAttribute_CreateInstance_m1_3366,
	ProxyAttribute_CreateProxy_m1_3367,
	ProxyAttribute_GetPropertiesForNewContext_m1_3368,
	ProxyAttribute_IsContextOK_m1_3369,
	RealProxy__ctor_m1_3370,
	RealProxy__ctor_m1_3371,
	RealProxy__ctor_m1_3372,
	RealProxy_InternalGetProxyType_m1_3373,
	RealProxy_GetProxiedType_m1_3374,
	RealProxy_get_ObjectIdentity_m1_3375,
	RealProxy_InternalGetTransparentProxy_m1_3376,
	RealProxy_GetTransparentProxy_m1_3377,
	RealProxy_SetTargetDomain_m1_3378,
	RemotingProxy__ctor_m1_3379,
	RemotingProxy__ctor_m1_3380,
	RemotingProxy__cctor_m1_3381,
	RemotingProxy_get_TypeName_m1_3382,
	RemotingProxy_Finalize_m1_3383,
	TrackingServices__cctor_m1_3384,
	TrackingServices_NotifyUnmarshaledObject_m1_3385,
	ActivatedClientTypeEntry__ctor_m1_3386,
	ActivatedClientTypeEntry_get_ApplicationUrl_m1_3387,
	ActivatedClientTypeEntry_get_ContextAttributes_m1_3388,
	ActivatedClientTypeEntry_get_ObjectType_m1_3389,
	ActivatedClientTypeEntry_ToString_m1_3390,
	ActivatedServiceTypeEntry__ctor_m1_3391,
	ActivatedServiceTypeEntry_get_ObjectType_m1_3392,
	ActivatedServiceTypeEntry_ToString_m1_3393,
	EnvoyInfo__ctor_m1_3394,
	EnvoyInfo_get_EnvoySinks_m1_3395,
	Identity__ctor_m1_3396,
	Identity_get_ChannelSink_m1_3397,
	Identity_set_ChannelSink_m1_3398,
	Identity_get_ObjectUri_m1_3399,
	Identity_get_Disposed_m1_3400,
	Identity_set_Disposed_m1_3401,
	Identity_get_ClientDynamicProperties_m1_3402,
	Identity_get_ServerDynamicProperties_m1_3403,
	ClientIdentity__ctor_m1_3404,
	ClientIdentity_get_ClientProxy_m1_3405,
	ClientIdentity_set_ClientProxy_m1_3406,
	ClientIdentity_CreateObjRef_m1_3407,
	ClientIdentity_get_TargetUri_m1_3408,
	InternalRemotingServices__cctor_m1_3409,
	InternalRemotingServices_GetCachedSoapAttribute_m1_3410,
	ObjRef__ctor_m1_3411,
	ObjRef__ctor_m1_3412,
	ObjRef__cctor_m1_3413,
	ObjRef_get_IsReferenceToWellKnow_m1_3414,
	ObjRef_get_ChannelInfo_m1_3415,
	ObjRef_get_EnvoyInfo_m1_3416,
	ObjRef_set_EnvoyInfo_m1_3417,
	ObjRef_get_TypeInfo_m1_3418,
	ObjRef_set_TypeInfo_m1_3419,
	ObjRef_get_URI_m1_3420,
	ObjRef_set_URI_m1_3421,
	ObjRef_GetObjectData_m1_3422,
	ObjRef_GetRealObject_m1_3423,
	ObjRef_UpdateChannelInfo_m1_3424,
	ObjRef_get_ServerType_m1_3425,
	RemotingConfiguration__cctor_m1_3426,
	RemotingConfiguration_get_ApplicationName_m1_3427,
	RemotingConfiguration_set_ApplicationName_m1_3428,
	RemotingConfiguration_get_ProcessId_m1_3429,
	RemotingConfiguration_LoadDefaultDelayedChannels_m1_3430,
	RemotingConfiguration_IsRemotelyActivatedClientType_m1_3431,
	RemotingConfiguration_RegisterActivatedClientType_m1_3432,
	RemotingConfiguration_RegisterActivatedServiceType_m1_3433,
	RemotingConfiguration_RegisterWellKnownClientType_m1_3434,
	RemotingConfiguration_RegisterWellKnownServiceType_m1_3435,
	RemotingConfiguration_RegisterChannelTemplate_m1_3436,
	RemotingConfiguration_RegisterClientProviderTemplate_m1_3437,
	RemotingConfiguration_RegisterServerProviderTemplate_m1_3438,
	RemotingConfiguration_RegisterChannels_m1_3439,
	RemotingConfiguration_RegisterTypes_m1_3440,
	RemotingConfiguration_SetCustomErrorsMode_m1_3441,
	ConfigHandler__ctor_m1_3442,
	ConfigHandler_ValidatePath_m1_3443,
	ConfigHandler_CheckPath_m1_3444,
	ConfigHandler_OnStartParsing_m1_3445,
	ConfigHandler_OnProcessingInstruction_m1_3446,
	ConfigHandler_OnIgnorableWhitespace_m1_3447,
	ConfigHandler_OnStartElement_m1_3448,
	ConfigHandler_ParseElement_m1_3449,
	ConfigHandler_OnEndElement_m1_3450,
	ConfigHandler_ReadCustomProviderData_m1_3451,
	ConfigHandler_ReadLifetine_m1_3452,
	ConfigHandler_ParseTime_m1_3453,
	ConfigHandler_ReadChannel_m1_3454,
	ConfigHandler_ReadProvider_m1_3455,
	ConfigHandler_ReadClientActivated_m1_3456,
	ConfigHandler_ReadServiceActivated_m1_3457,
	ConfigHandler_ReadClientWellKnown_m1_3458,
	ConfigHandler_ReadServiceWellKnown_m1_3459,
	ConfigHandler_ReadInteropXml_m1_3460,
	ConfigHandler_ReadPreload_m1_3461,
	ConfigHandler_GetNotNull_m1_3462,
	ConfigHandler_ExtractAssembly_m1_3463,
	ConfigHandler_OnChars_m1_3464,
	ConfigHandler_OnEndParsing_m1_3465,
	ChannelData__ctor_m1_3466,
	ChannelData_get_ServerProviders_m1_3467,
	ChannelData_get_ClientProviders_m1_3468,
	ChannelData_get_CustomProperties_m1_3469,
	ChannelData_CopyFrom_m1_3470,
	ProviderData__ctor_m1_3471,
	ProviderData_CopyFrom_m1_3472,
	FormatterData__ctor_m1_3473,
	RemotingException__ctor_m1_3474,
	RemotingException__ctor_m1_3475,
	RemotingException__ctor_m1_3476,
	RemotingException__ctor_m1_3477,
	RemotingServices__cctor_m1_3478,
	RemotingServices_GetVirtualMethod_m1_3479,
	RemotingServices_IsTransparentProxy_m1_3480,
	RemotingServices_GetServerTypeForUri_m1_3481,
	RemotingServices_Unmarshal_m1_3482,
	RemotingServices_Unmarshal_m1_3483,
	RemotingServices_GetRealProxy_m1_3484,
	RemotingServices_GetMethodBaseFromMethodMessage_m1_3485,
	RemotingServices_GetMethodBaseFromName_m1_3486,
	RemotingServices_FindInterfaceMethod_m1_3487,
	RemotingServices_CreateClientProxy_m1_3488,
	RemotingServices_CreateClientProxy_m1_3489,
	RemotingServices_CreateClientProxyForContextBound_m1_3490,
	RemotingServices_GetIdentityForUri_m1_3491,
	RemotingServices_RemoveAppNameFromUri_m1_3492,
	RemotingServices_GetOrCreateClientIdentity_m1_3493,
	RemotingServices_GetClientChannelSinkChain_m1_3494,
	RemotingServices_CreateWellKnownServerIdentity_m1_3495,
	RemotingServices_RegisterServerIdentity_m1_3496,
	RemotingServices_GetProxyForRemoteObject_m1_3497,
	RemotingServices_GetRemoteObject_m1_3498,
	RemotingServices_RegisterInternalChannels_m1_3499,
	RemotingServices_DisposeIdentity_m1_3500,
	RemotingServices_GetNormalizedUri_m1_3501,
	ServerIdentity__ctor_m1_3502,
	ServerIdentity_get_ObjectType_m1_3503,
	ServerIdentity_CreateObjRef_m1_3504,
	ClientActivatedIdentity_GetServerObject_m1_3505,
	SingletonIdentity__ctor_m1_3506,
	SingleCallIdentity__ctor_m1_3507,
	TypeInfo__ctor_m1_3508,
	SoapServices__cctor_m1_3509,
	SoapServices_get_XmlNsForClrTypeWithAssembly_m1_3510,
	SoapServices_get_XmlNsForClrTypeWithNs_m1_3511,
	SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m1_3512,
	SoapServices_CodeXmlNamespaceForClrTypeNamespace_m1_3513,
	SoapServices_GetNameKey_m1_3514,
	SoapServices_GetAssemblyName_m1_3515,
	SoapServices_GetXmlElementForInteropType_m1_3516,
	SoapServices_GetXmlNamespaceForMethodCall_m1_3517,
	SoapServices_GetXmlNamespaceForMethodResponse_m1_3518,
	SoapServices_GetXmlTypeForInteropType_m1_3519,
	SoapServices_PreLoad_m1_3520,
	SoapServices_PreLoad_m1_3521,
	SoapServices_RegisterInteropXmlElement_m1_3522,
	SoapServices_RegisterInteropXmlType_m1_3523,
	SoapServices_EncodeNs_m1_3524,
	TypeEntry__ctor_m1_3525,
	TypeEntry_get_AssemblyName_m1_3526,
	TypeEntry_set_AssemblyName_m1_3527,
	TypeEntry_get_TypeName_m1_3528,
	TypeEntry_set_TypeName_m1_3529,
	TypeInfo__ctor_m1_3530,
	TypeInfo_get_TypeName_m1_3531,
	WellKnownClientTypeEntry__ctor_m1_3532,
	WellKnownClientTypeEntry_get_ApplicationUrl_m1_3533,
	WellKnownClientTypeEntry_get_ObjectType_m1_3534,
	WellKnownClientTypeEntry_get_ObjectUrl_m1_3535,
	WellKnownClientTypeEntry_ToString_m1_3536,
	WellKnownServiceTypeEntry__ctor_m1_3537,
	WellKnownServiceTypeEntry_get_Mode_m1_3538,
	WellKnownServiceTypeEntry_get_ObjectType_m1_3539,
	WellKnownServiceTypeEntry_get_ObjectUri_m1_3540,
	WellKnownServiceTypeEntry_ToString_m1_3541,
	BinaryCommon__cctor_m1_3542,
	BinaryCommon_IsPrimitive_m1_3543,
	BinaryCommon_GetTypeFromCode_m1_3544,
	BinaryCommon_SwapBytes_m1_3545,
	BinaryFormatter__ctor_m1_3546,
	BinaryFormatter__ctor_m1_3547,
	BinaryFormatter_get_DefaultSurrogateSelector_m1_3548,
	BinaryFormatter_set_AssemblyFormat_m1_3549,
	BinaryFormatter_get_Binder_m1_3550,
	BinaryFormatter_get_Context_m1_3551,
	BinaryFormatter_get_SurrogateSelector_m1_3552,
	BinaryFormatter_get_FilterLevel_m1_3553,
	BinaryFormatter_Deserialize_m1_3554,
	BinaryFormatter_NoCheckDeserialize_m1_3555,
	BinaryFormatter_ReadBinaryHeader_m1_3556,
	MessageFormatter_ReadMethodCall_m1_3557,
	MessageFormatter_ReadMethodResponse_m1_3558,
	TypeMetadata__ctor_m1_3559,
	ArrayNullFiller__ctor_m1_3560,
	ObjectReader__ctor_m1_3561,
	ObjectReader_ReadObjectGraph_m1_3562,
	ObjectReader_ReadObjectGraph_m1_3563,
	ObjectReader_ReadNextObject_m1_3564,
	ObjectReader_ReadNextObject_m1_3565,
	ObjectReader_get_CurrentObject_m1_3566,
	ObjectReader_ReadObject_m1_3567,
	ObjectReader_ReadAssembly_m1_3568,
	ObjectReader_ReadObjectInstance_m1_3569,
	ObjectReader_ReadRefTypeObjectInstance_m1_3570,
	ObjectReader_ReadObjectContent_m1_3571,
	ObjectReader_RegisterObject_m1_3572,
	ObjectReader_ReadStringIntance_m1_3573,
	ObjectReader_ReadGenericArray_m1_3574,
	ObjectReader_ReadBoxedPrimitiveTypeValue_m1_3575,
	ObjectReader_ReadArrayOfPrimitiveType_m1_3576,
	ObjectReader_BlockRead_m1_3577,
	ObjectReader_ReadArrayOfObject_m1_3578,
	ObjectReader_ReadArrayOfString_m1_3579,
	ObjectReader_ReadSimpleArray_m1_3580,
	ObjectReader_ReadTypeMetadata_m1_3581,
	ObjectReader_ReadValue_m1_3582,
	ObjectReader_SetObjectValue_m1_3583,
	ObjectReader_RecordFixup_m1_3584,
	ObjectReader_GetDeserializationType_m1_3585,
	ObjectReader_ReadType_m1_3586,
	ObjectReader_ReadPrimitiveTypeValue_m1_3587,
	FormatterConverter__ctor_m1_3588,
	FormatterConverter_Convert_m1_3589,
	FormatterConverter_ToBoolean_m1_3590,
	FormatterConverter_ToInt16_m1_3591,
	FormatterConverter_ToInt32_m1_3592,
	FormatterConverter_ToInt64_m1_3593,
	FormatterConverter_ToString_m1_3594,
	FormatterConverter_ToUInt32_m1_3595,
	FormatterServices_GetUninitializedObject_m1_3596,
	FormatterServices_GetSafeUninitializedObject_m1_3597,
	ObjectManager__ctor_m1_3598,
	ObjectManager_DoFixups_m1_3599,
	ObjectManager_GetObjectRecord_m1_3600,
	ObjectManager_GetObject_m1_3601,
	ObjectManager_RaiseDeserializationEvent_m1_3602,
	ObjectManager_RaiseOnDeserializingEvent_m1_3603,
	ObjectManager_RaiseOnDeserializedEvent_m1_3604,
	ObjectManager_AddFixup_m1_3605,
	ObjectManager_RecordArrayElementFixup_m1_3606,
	ObjectManager_RecordArrayElementFixup_m1_3607,
	ObjectManager_RecordDelayedFixup_m1_3608,
	ObjectManager_RecordFixup_m1_3609,
	ObjectManager_RegisterObjectInternal_m1_3610,
	ObjectManager_RegisterObject_m1_3611,
	BaseFixupRecord__ctor_m1_3612,
	BaseFixupRecord_DoFixup_m1_3613,
	ArrayFixupRecord__ctor_m1_3614,
	ArrayFixupRecord_FixupImpl_m1_3615,
	MultiArrayFixupRecord__ctor_m1_3616,
	MultiArrayFixupRecord_FixupImpl_m1_3617,
	FixupRecord__ctor_m1_3618,
	FixupRecord_FixupImpl_m1_3619,
	DelayedFixupRecord__ctor_m1_3620,
	DelayedFixupRecord_FixupImpl_m1_3621,
	ObjectRecord__ctor_m1_3622,
	ObjectRecord_SetMemberValue_m1_3623,
	ObjectRecord_SetArrayValue_m1_3624,
	ObjectRecord_SetMemberValue_m1_3625,
	ObjectRecord_get_IsInstanceReady_m1_3626,
	ObjectRecord_get_IsUnsolvedObjectReference_m1_3627,
	ObjectRecord_get_IsRegistered_m1_3628,
	ObjectRecord_DoFixups_m1_3629,
	ObjectRecord_RemoveFixup_m1_3630,
	ObjectRecord_UnchainFixup_m1_3631,
	ObjectRecord_ChainFixup_m1_3632,
	ObjectRecord_LoadData_m1_3633,
	ObjectRecord_get_HasPendingFixups_m1_3634,
	SerializationBinder__ctor_m1_3635,
	CallbackHandler__ctor_m1_3636,
	CallbackHandler_Invoke_m1_3637,
	CallbackHandler_BeginInvoke_m1_3638,
	CallbackHandler_EndInvoke_m1_3639,
	SerializationCallbacks__ctor_m1_3640,
	SerializationCallbacks__cctor_m1_3641,
	SerializationCallbacks_get_HasDeserializedCallbacks_m1_3642,
	SerializationCallbacks_GetMethodsByAttribute_m1_3643,
	SerializationCallbacks_Invoke_m1_3644,
	SerializationCallbacks_RaiseOnDeserializing_m1_3645,
	SerializationCallbacks_RaiseOnDeserialized_m1_3646,
	SerializationCallbacks_GetSerializationCallbacks_m1_3647,
	SerializationEntry__ctor_m1_3648,
	SerializationEntry_get_Name_m1_3649,
	SerializationEntry_get_Value_m1_3650,
	SerializationException__ctor_m1_3651,
	SerializationException__ctor_m1_3652,
	SerializationException__ctor_m1_3653,
	SerializationInfo__ctor_m1_3654,
	SerializationInfo_AddValue_m1_3655,
	SerializationInfo_GetValue_m1_3656,
	SerializationInfo_SetType_m1_3657,
	SerializationInfo_GetEnumerator_m1_3658,
	SerializationInfo_AddValue_m1_3659,
	SerializationInfo_AddValue_m1_3660,
	SerializationInfo_AddValue_m1_3661,
	SerializationInfo_AddValue_m1_3662,
	SerializationInfo_AddValue_m1_3663,
	SerializationInfo_AddValue_m1_3664,
	SerializationInfo_AddValue_m1_3665,
	SerializationInfo_AddValue_m1_3666,
	SerializationInfo_AddValue_m1_3667,
	SerializationInfo_GetBoolean_m1_3668,
	SerializationInfo_GetInt16_m1_3669,
	SerializationInfo_GetInt32_m1_3670,
	SerializationInfo_GetInt64_m1_3671,
	SerializationInfo_GetString_m1_3672,
	SerializationInfo_GetUInt32_m1_3673,
	SerializationInfoEnumerator__ctor_m1_3674,
	SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m1_3675,
	SerializationInfoEnumerator_get_Current_m1_3676,
	SerializationInfoEnumerator_get_Name_m1_3677,
	SerializationInfoEnumerator_get_Value_m1_3678,
	SerializationInfoEnumerator_MoveNext_m1_3679,
	SerializationInfoEnumerator_Reset_m1_3680,
	StreamingContext__ctor_m1_3681,
	StreamingContext__ctor_m1_3682,
	StreamingContext_get_State_m1_3683,
	StreamingContext_Equals_m1_3684,
	StreamingContext_GetHashCode_m1_3685,
	X509Certificate__ctor_m1_3686,
	X509Certificate__ctor_m1_3687,
	X509Certificate__ctor_m1_3688,
	X509Certificate__ctor_m1_3689,
	X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_3690,
	X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m1_3691,
	X509Certificate_tostr_m1_3692,
	X509Certificate_Equals_m1_3693,
	X509Certificate_GetCertHash_m1_3694,
	X509Certificate_GetCertHashString_m1_3695,
	X509Certificate_GetEffectiveDateString_m1_3696,
	X509Certificate_GetExpirationDateString_m1_3697,
	X509Certificate_GetHashCode_m1_3698,
	X509Certificate_GetIssuerName_m1_3699,
	X509Certificate_GetName_m1_3700,
	X509Certificate_GetPublicKey_m1_3701,
	X509Certificate_GetRawCertData_m1_3702,
	X509Certificate_ToString_m1_3703,
	X509Certificate_ToString_m1_3704,
	X509Certificate_get_Issuer_m1_3705,
	X509Certificate_get_Subject_m1_3706,
	X509Certificate_Equals_m1_3707,
	X509Certificate_Import_m1_3708,
	X509Certificate_Reset_m1_3709,
	AsymmetricAlgorithm__ctor_m1_3710,
	AsymmetricAlgorithm_System_IDisposable_Dispose_m1_3711,
	AsymmetricAlgorithm_get_KeySize_m1_3712,
	AsymmetricAlgorithm_set_KeySize_m1_3713,
	AsymmetricAlgorithm_Clear_m1_3714,
	AsymmetricAlgorithm_GetNamedParam_m1_3715,
	AsymmetricKeyExchangeFormatter__ctor_m1_3716,
	AsymmetricSignatureDeformatter__ctor_m1_3717,
	AsymmetricSignatureFormatter__ctor_m1_3718,
	Base64Constants__cctor_m1_3719,
	CryptoConfig__cctor_m1_3720,
	CryptoConfig_Initialize_m1_3721,
	CryptoConfig_CreateFromName_m1_3722,
	CryptoConfig_CreateFromName_m1_3723,
	CryptoConfig_MapNameToOID_m1_3724,
	CryptoConfig_EncodeOID_m1_3725,
	CryptoConfig_EncodeLongNumber_m1_3726,
	CryptographicException__ctor_m1_3727,
	CryptographicException__ctor_m1_3728,
	CryptographicException__ctor_m1_3729,
	CryptographicException__ctor_m1_3730,
	CryptographicException__ctor_m1_3731,
	CryptographicUnexpectedOperationException__ctor_m1_3732,
	CryptographicUnexpectedOperationException__ctor_m1_3733,
	CryptographicUnexpectedOperationException__ctor_m1_3734,
	CspParameters__ctor_m1_3735,
	CspParameters__ctor_m1_3736,
	CspParameters__ctor_m1_3737,
	CspParameters__ctor_m1_3738,
	CspParameters_get_Flags_m1_3739,
	CspParameters_set_Flags_m1_3740,
	DES__ctor_m1_3741,
	DES__cctor_m1_3742,
	DES_Create_m1_3743,
	DES_Create_m1_3744,
	DES_IsWeakKey_m1_3745,
	DES_IsSemiWeakKey_m1_3746,
	DES_get_Key_m1_3747,
	DES_set_Key_m1_3748,
	DESTransform__ctor_m1_3749,
	DESTransform__cctor_m1_3750,
	DESTransform_CipherFunct_m1_3751,
	DESTransform_Permutation_m1_3752,
	DESTransform_BSwap_m1_3753,
	DESTransform_SetKey_m1_3754,
	DESTransform_ProcessBlock_m1_3755,
	DESTransform_ECB_m1_3756,
	DESTransform_GetStrongKey_m1_3757,
	DESCryptoServiceProvider__ctor_m1_3758,
	DESCryptoServiceProvider_CreateDecryptor_m1_3759,
	DESCryptoServiceProvider_CreateEncryptor_m1_3760,
	DESCryptoServiceProvider_GenerateIV_m1_3761,
	DESCryptoServiceProvider_GenerateKey_m1_3762,
	DSA__ctor_m1_3763,
	DSA_Create_m1_3764,
	DSA_Create_m1_3765,
	DSA_ZeroizePrivateKey_m1_3766,
	DSA_FromXmlString_m1_3767,
	DSA_ToXmlString_m1_3768,
	DSACryptoServiceProvider__ctor_m1_3769,
	DSACryptoServiceProvider__ctor_m1_3770,
	DSACryptoServiceProvider__ctor_m1_3771,
	DSACryptoServiceProvider__cctor_m1_3772,
	DSACryptoServiceProvider_Finalize_m1_3773,
	DSACryptoServiceProvider_get_KeySize_m1_3774,
	DSACryptoServiceProvider_get_PublicOnly_m1_3775,
	DSACryptoServiceProvider_ExportParameters_m1_3776,
	DSACryptoServiceProvider_ImportParameters_m1_3777,
	DSACryptoServiceProvider_CreateSignature_m1_3778,
	DSACryptoServiceProvider_VerifySignature_m1_3779,
	DSACryptoServiceProvider_Dispose_m1_3780,
	DSACryptoServiceProvider_OnKeyGenerated_m1_3781,
	DSASignatureDeformatter__ctor_m1_3782,
	DSASignatureDeformatter__ctor_m1_3783,
	DSASignatureDeformatter_SetHashAlgorithm_m1_3784,
	DSASignatureDeformatter_SetKey_m1_3785,
	DSASignatureDeformatter_VerifySignature_m1_3786,
	DSASignatureFormatter__ctor_m1_3787,
	DSASignatureFormatter_CreateSignature_m1_3788,
	DSASignatureFormatter_SetHashAlgorithm_m1_3789,
	DSASignatureFormatter_SetKey_m1_3790,
	HMAC__ctor_m1_3791,
	HMAC_get_BlockSizeValue_m1_3792,
	HMAC_set_BlockSizeValue_m1_3793,
	HMAC_set_HashName_m1_3794,
	HMAC_get_Key_m1_3795,
	HMAC_set_Key_m1_3796,
	HMAC_get_Block_m1_3797,
	HMAC_KeySetup_m1_3798,
	HMAC_Dispose_m1_3799,
	HMAC_HashCore_m1_3800,
	HMAC_HashFinal_m1_3801,
	HMAC_Initialize_m1_3802,
	HMAC_Create_m1_3803,
	HMAC_Create_m1_3804,
	HMACMD5__ctor_m1_3805,
	HMACMD5__ctor_m1_3806,
	HMACRIPEMD160__ctor_m1_3807,
	HMACRIPEMD160__ctor_m1_3808,
	HMACSHA1__ctor_m1_3809,
	HMACSHA1__ctor_m1_3810,
	HMACSHA256__ctor_m1_3811,
	HMACSHA256__ctor_m1_3812,
	HMACSHA384__ctor_m1_3813,
	HMACSHA384__ctor_m1_3814,
	HMACSHA384__cctor_m1_3815,
	HMACSHA384_set_ProduceLegacyHmacValues_m1_3816,
	HMACSHA512__ctor_m1_3817,
	HMACSHA512__ctor_m1_3818,
	HMACSHA512__cctor_m1_3819,
	HMACSHA512_set_ProduceLegacyHmacValues_m1_3820,
	HashAlgorithm__ctor_m1_3821,
	HashAlgorithm_System_IDisposable_Dispose_m1_3822,
	HashAlgorithm_get_CanReuseTransform_m1_3823,
	HashAlgorithm_ComputeHash_m1_3824,
	HashAlgorithm_ComputeHash_m1_3825,
	HashAlgorithm_Create_m1_3826,
	HashAlgorithm_get_Hash_m1_3827,
	HashAlgorithm_get_HashSize_m1_3828,
	HashAlgorithm_Dispose_m1_3829,
	HashAlgorithm_TransformBlock_m1_3830,
	HashAlgorithm_TransformFinalBlock_m1_3831,
	KeySizes__ctor_m1_3832,
	KeySizes_get_MaxSize_m1_3833,
	KeySizes_get_MinSize_m1_3834,
	KeySizes_get_SkipSize_m1_3835,
	KeySizes_IsLegal_m1_3836,
	KeySizes_IsLegalKeySize_m1_3837,
	KeyedHashAlgorithm__ctor_m1_3838,
	KeyedHashAlgorithm_Finalize_m1_3839,
	KeyedHashAlgorithm_get_Key_m1_3840,
	KeyedHashAlgorithm_set_Key_m1_3841,
	KeyedHashAlgorithm_Dispose_m1_3842,
	KeyedHashAlgorithm_ZeroizeKey_m1_3843,
	MACTripleDES__ctor_m1_3844,
	MACTripleDES_Setup_m1_3845,
	MACTripleDES_Finalize_m1_3846,
	MACTripleDES_Dispose_m1_3847,
	MACTripleDES_Initialize_m1_3848,
	MACTripleDES_HashCore_m1_3849,
	MACTripleDES_HashFinal_m1_3850,
	MD5__ctor_m1_3851,
	MD5_Create_m1_3852,
	MD5_Create_m1_3853,
	MD5CryptoServiceProvider__ctor_m1_3854,
	MD5CryptoServiceProvider__cctor_m1_3855,
	MD5CryptoServiceProvider_Finalize_m1_3856,
	MD5CryptoServiceProvider_Dispose_m1_3857,
	MD5CryptoServiceProvider_HashCore_m1_3858,
	MD5CryptoServiceProvider_HashFinal_m1_3859,
	MD5CryptoServiceProvider_Initialize_m1_3860,
	MD5CryptoServiceProvider_ProcessBlock_m1_3861,
	MD5CryptoServiceProvider_ProcessFinalBlock_m1_3862,
	MD5CryptoServiceProvider_AddLength_m1_3863,
	RC2__ctor_m1_3864,
	RC2_Create_m1_3865,
	RC2_Create_m1_3866,
	RC2_get_EffectiveKeySize_m1_3867,
	RC2_get_KeySize_m1_3868,
	RC2_set_KeySize_m1_3869,
	RC2CryptoServiceProvider__ctor_m1_3870,
	RC2CryptoServiceProvider_get_EffectiveKeySize_m1_3871,
	RC2CryptoServiceProvider_CreateDecryptor_m1_3872,
	RC2CryptoServiceProvider_CreateEncryptor_m1_3873,
	RC2CryptoServiceProvider_GenerateIV_m1_3874,
	RC2CryptoServiceProvider_GenerateKey_m1_3875,
	RC2Transform__ctor_m1_3876,
	RC2Transform__cctor_m1_3877,
	RC2Transform_ECB_m1_3878,
	RIPEMD160__ctor_m1_3879,
	RIPEMD160Managed__ctor_m1_3880,
	RIPEMD160Managed_Initialize_m1_3881,
	RIPEMD160Managed_HashCore_m1_3882,
	RIPEMD160Managed_HashFinal_m1_3883,
	RIPEMD160Managed_Finalize_m1_3884,
	RIPEMD160Managed_ProcessBlock_m1_3885,
	RIPEMD160Managed_Compress_m1_3886,
	RIPEMD160Managed_CompressFinal_m1_3887,
	RIPEMD160Managed_ROL_m1_3888,
	RIPEMD160Managed_F_m1_3889,
	RIPEMD160Managed_G_m1_3890,
	RIPEMD160Managed_H_m1_3891,
	RIPEMD160Managed_I_m1_3892,
	RIPEMD160Managed_J_m1_3893,
	RIPEMD160Managed_FF_m1_3894,
	RIPEMD160Managed_GG_m1_3895,
	RIPEMD160Managed_HH_m1_3896,
	RIPEMD160Managed_II_m1_3897,
	RIPEMD160Managed_JJ_m1_3898,
	RIPEMD160Managed_FFF_m1_3899,
	RIPEMD160Managed_GGG_m1_3900,
	RIPEMD160Managed_HHH_m1_3901,
	RIPEMD160Managed_III_m1_3902,
	RIPEMD160Managed_JJJ_m1_3903,
	RNGCryptoServiceProvider__ctor_m1_3904,
	RNGCryptoServiceProvider__cctor_m1_3905,
	RNGCryptoServiceProvider_Check_m1_3906,
	RNGCryptoServiceProvider_RngOpen_m1_3907,
	RNGCryptoServiceProvider_RngInitialize_m1_3908,
	RNGCryptoServiceProvider_RngGetBytes_m1_3909,
	RNGCryptoServiceProvider_RngClose_m1_3910,
	RNGCryptoServiceProvider_GetBytes_m1_3911,
	RNGCryptoServiceProvider_GetNonZeroBytes_m1_3912,
	RNGCryptoServiceProvider_Finalize_m1_3913,
	RSA__ctor_m1_3914,
	RSA_Create_m1_3915,
	RSA_Create_m1_3916,
	RSA_ZeroizePrivateKey_m1_3917,
	RSA_FromXmlString_m1_3918,
	RSA_ToXmlString_m1_3919,
	RSACryptoServiceProvider__ctor_m1_3920,
	RSACryptoServiceProvider__ctor_m1_3921,
	RSACryptoServiceProvider__ctor_m1_3922,
	RSACryptoServiceProvider__cctor_m1_3923,
	RSACryptoServiceProvider_Common_m1_3924,
	RSACryptoServiceProvider_Finalize_m1_3925,
	RSACryptoServiceProvider_get_KeySize_m1_3926,
	RSACryptoServiceProvider_get_PublicOnly_m1_3927,
	RSACryptoServiceProvider_DecryptValue_m1_3928,
	RSACryptoServiceProvider_EncryptValue_m1_3929,
	RSACryptoServiceProvider_ExportParameters_m1_3930,
	RSACryptoServiceProvider_ImportParameters_m1_3931,
	RSACryptoServiceProvider_Dispose_m1_3932,
	RSACryptoServiceProvider_OnKeyGenerated_m1_3933,
	RSAPKCS1KeyExchangeFormatter__ctor_m1_3934,
	RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m1_3935,
	RSAPKCS1KeyExchangeFormatter_SetRSAKey_m1_3936,
	RSAPKCS1SignatureDeformatter__ctor_m1_3937,
	RSAPKCS1SignatureDeformatter__ctor_m1_3938,
	RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m1_3939,
	RSAPKCS1SignatureDeformatter_SetKey_m1_3940,
	RSAPKCS1SignatureDeformatter_VerifySignature_m1_3941,
	RSAPKCS1SignatureFormatter__ctor_m1_3942,
	RSAPKCS1SignatureFormatter_CreateSignature_m1_3943,
	RSAPKCS1SignatureFormatter_SetHashAlgorithm_m1_3944,
	RSAPKCS1SignatureFormatter_SetKey_m1_3945,
	RandomNumberGenerator__ctor_m1_3946,
	RandomNumberGenerator_Create_m1_3947,
	RandomNumberGenerator_Create_m1_3948,
	Rijndael__ctor_m1_3949,
	Rijndael_Create_m1_3950,
	Rijndael_Create_m1_3951,
	RijndaelManaged__ctor_m1_3952,
	RijndaelManaged_GenerateIV_m1_3953,
	RijndaelManaged_GenerateKey_m1_3954,
	RijndaelManaged_CreateDecryptor_m1_3955,
	RijndaelManaged_CreateEncryptor_m1_3956,
	RijndaelTransform__ctor_m1_3957,
	RijndaelTransform__cctor_m1_3958,
	RijndaelTransform_Clear_m1_3959,
	RijndaelTransform_ECB_m1_3960,
	RijndaelTransform_SubByte_m1_3961,
	RijndaelTransform_Encrypt128_m1_3962,
	RijndaelTransform_Encrypt192_m1_3963,
	RijndaelTransform_Encrypt256_m1_3964,
	RijndaelTransform_Decrypt128_m1_3965,
	RijndaelTransform_Decrypt192_m1_3966,
	RijndaelTransform_Decrypt256_m1_3967,
	RijndaelManagedTransform__ctor_m1_3968,
	RijndaelManagedTransform_System_IDisposable_Dispose_m1_3969,
	RijndaelManagedTransform_get_CanReuseTransform_m1_3970,
	RijndaelManagedTransform_TransformBlock_m1_3971,
	RijndaelManagedTransform_TransformFinalBlock_m1_3972,
	SHA1__ctor_m1_3973,
	SHA1_Create_m1_3974,
	SHA1_Create_m1_3975,
	SHA1Internal__ctor_m1_3976,
	SHA1Internal_HashCore_m1_3977,
	SHA1Internal_HashFinal_m1_3978,
	SHA1Internal_Initialize_m1_3979,
	SHA1Internal_ProcessBlock_m1_3980,
	SHA1Internal_InitialiseBuff_m1_3981,
	SHA1Internal_FillBuff_m1_3982,
	SHA1Internal_ProcessFinalBlock_m1_3983,
	SHA1Internal_AddLength_m1_3984,
	SHA1CryptoServiceProvider__ctor_m1_3985,
	SHA1CryptoServiceProvider_Finalize_m1_3986,
	SHA1CryptoServiceProvider_Dispose_m1_3987,
	SHA1CryptoServiceProvider_HashCore_m1_3988,
	SHA1CryptoServiceProvider_HashFinal_m1_3989,
	SHA1CryptoServiceProvider_Initialize_m1_3990,
	SHA1Managed__ctor_m1_3991,
	SHA1Managed_HashCore_m1_3992,
	SHA1Managed_HashFinal_m1_3993,
	SHA1Managed_Initialize_m1_3994,
	SHA256__ctor_m1_3995,
	SHA256_Create_m1_3996,
	SHA256_Create_m1_3997,
	SHA256Managed__ctor_m1_3998,
	SHA256Managed_HashCore_m1_3999,
	SHA256Managed_HashFinal_m1_4000,
	SHA256Managed_Initialize_m1_4001,
	SHA256Managed_ProcessBlock_m1_4002,
	SHA256Managed_ProcessFinalBlock_m1_4003,
	SHA256Managed_AddLength_m1_4004,
	SHA384__ctor_m1_4005,
	SHA384Managed__ctor_m1_4006,
	SHA384Managed_Initialize_m1_4007,
	SHA384Managed_Initialize_m1_4008,
	SHA384Managed_HashCore_m1_4009,
	SHA384Managed_HashFinal_m1_4010,
	SHA384Managed_update_m1_4011,
	SHA384Managed_processWord_m1_4012,
	SHA384Managed_unpackWord_m1_4013,
	SHA384Managed_adjustByteCounts_m1_4014,
	SHA384Managed_processLength_m1_4015,
	SHA384Managed_processBlock_m1_4016,
	SHA512__ctor_m1_4017,
	SHA512Managed__ctor_m1_4018,
	SHA512Managed_Initialize_m1_4019,
	SHA512Managed_Initialize_m1_4020,
	SHA512Managed_HashCore_m1_4021,
	SHA512Managed_HashFinal_m1_4022,
	SHA512Managed_update_m1_4023,
	SHA512Managed_processWord_m1_4024,
	SHA512Managed_unpackWord_m1_4025,
	SHA512Managed_adjustByteCounts_m1_4026,
	SHA512Managed_processLength_m1_4027,
	SHA512Managed_processBlock_m1_4028,
	SHA512Managed_rotateRight_m1_4029,
	SHA512Managed_Ch_m1_4030,
	SHA512Managed_Maj_m1_4031,
	SHA512Managed_Sum0_m1_4032,
	SHA512Managed_Sum1_m1_4033,
	SHA512Managed_Sigma0_m1_4034,
	SHA512Managed_Sigma1_m1_4035,
	SHAConstants__cctor_m1_4036,
	SignatureDescription__ctor_m1_4037,
	SignatureDescription_set_DeformatterAlgorithm_m1_4038,
	SignatureDescription_set_DigestAlgorithm_m1_4039,
	SignatureDescription_set_FormatterAlgorithm_m1_4040,
	SignatureDescription_set_KeyAlgorithm_m1_4041,
	DSASignatureDescription__ctor_m1_4042,
	RSAPKCS1SHA1SignatureDescription__ctor_m1_4043,
	SymmetricAlgorithm__ctor_m1_4044,
	SymmetricAlgorithm_System_IDisposable_Dispose_m1_4045,
	SymmetricAlgorithm_Finalize_m1_4046,
	SymmetricAlgorithm_Clear_m1_4047,
	SymmetricAlgorithm_Dispose_m1_4048,
	SymmetricAlgorithm_get_BlockSize_m1_4049,
	SymmetricAlgorithm_set_BlockSize_m1_4050,
	SymmetricAlgorithm_get_FeedbackSize_m1_4051,
	SymmetricAlgorithm_get_IV_m1_4052,
	SymmetricAlgorithm_set_IV_m1_4053,
	SymmetricAlgorithm_get_Key_m1_4054,
	SymmetricAlgorithm_set_Key_m1_4055,
	SymmetricAlgorithm_get_KeySize_m1_4056,
	SymmetricAlgorithm_set_KeySize_m1_4057,
	SymmetricAlgorithm_get_LegalKeySizes_m1_4058,
	SymmetricAlgorithm_get_Mode_m1_4059,
	SymmetricAlgorithm_set_Mode_m1_4060,
	SymmetricAlgorithm_get_Padding_m1_4061,
	SymmetricAlgorithm_set_Padding_m1_4062,
	SymmetricAlgorithm_CreateDecryptor_m1_4063,
	SymmetricAlgorithm_CreateEncryptor_m1_4064,
	SymmetricAlgorithm_Create_m1_4065,
	ToBase64Transform_System_IDisposable_Dispose_m1_4066,
	ToBase64Transform_Finalize_m1_4067,
	ToBase64Transform_get_CanReuseTransform_m1_4068,
	ToBase64Transform_get_InputBlockSize_m1_4069,
	ToBase64Transform_get_OutputBlockSize_m1_4070,
	ToBase64Transform_Dispose_m1_4071,
	ToBase64Transform_TransformBlock_m1_4072,
	ToBase64Transform_InternalTransformBlock_m1_4073,
	ToBase64Transform_TransformFinalBlock_m1_4074,
	ToBase64Transform_InternalTransformFinalBlock_m1_4075,
	TripleDES__ctor_m1_4076,
	TripleDES_get_Key_m1_4077,
	TripleDES_set_Key_m1_4078,
	TripleDES_IsWeakKey_m1_4079,
	TripleDES_Create_m1_4080,
	TripleDES_Create_m1_4081,
	TripleDESCryptoServiceProvider__ctor_m1_4082,
	TripleDESCryptoServiceProvider_GenerateIV_m1_4083,
	TripleDESCryptoServiceProvider_GenerateKey_m1_4084,
	TripleDESCryptoServiceProvider_CreateDecryptor_m1_4085,
	TripleDESCryptoServiceProvider_CreateEncryptor_m1_4086,
	TripleDESTransform__ctor_m1_4087,
	TripleDESTransform_ECB_m1_4088,
	TripleDESTransform_GetStrongKey_m1_4089,
	SecurityPermission__ctor_m1_4090,
	SecurityPermission_set_Flags_m1_4091,
	SecurityPermission_IsUnrestricted_m1_4092,
	SecurityPermission_IsSubsetOf_m1_4093,
	SecurityPermission_ToXml_m1_4094,
	SecurityPermission_IsEmpty_m1_4095,
	SecurityPermission_Cast_m1_4096,
	StrongNamePublicKeyBlob_Equals_m1_4097,
	StrongNamePublicKeyBlob_GetHashCode_m1_4098,
	StrongNamePublicKeyBlob_ToString_m1_4099,
	ApplicationTrust__ctor_m1_4100,
	EvidenceEnumerator__ctor_m1_4101,
	EvidenceEnumerator_MoveNext_m1_4102,
	EvidenceEnumerator_Reset_m1_4103,
	EvidenceEnumerator_get_Current_m1_4104,
	Evidence__ctor_m1_4105,
	Evidence_get_Count_m1_4106,
	Evidence_get_SyncRoot_m1_4107,
	Evidence_get_HostEvidenceList_m1_4108,
	Evidence_get_AssemblyEvidenceList_m1_4109,
	Evidence_CopyTo_m1_4110,
	Evidence_Equals_m1_4111,
	Evidence_GetEnumerator_m1_4112,
	Evidence_GetHashCode_m1_4113,
	Hash__ctor_m1_4114,
	Hash__ctor_m1_4115,
	Hash_GetObjectData_m1_4116,
	Hash_ToString_m1_4117,
	Hash_GetData_m1_4118,
	StrongName_get_Name_m1_4119,
	StrongName_get_PublicKey_m1_4120,
	StrongName_get_Version_m1_4121,
	StrongName_Equals_m1_4122,
	StrongName_GetHashCode_m1_4123,
	StrongName_ToString_m1_4124,
	WindowsIdentity__ctor_m1_4125,
	WindowsIdentity__cctor_m1_4126,
	WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_4127,
	WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m1_4128,
	WindowsIdentity_Dispose_m1_4129,
	WindowsIdentity_GetCurrentToken_m1_4130,
	WindowsIdentity_GetTokenName_m1_4131,
	CodeAccessPermission__ctor_m1_4132,
	CodeAccessPermission_Equals_m1_4133,
	CodeAccessPermission_GetHashCode_m1_4134,
	CodeAccessPermission_ToString_m1_4135,
	CodeAccessPermission_Element_m1_4136,
	CodeAccessPermission_ThrowInvalidPermission_m1_4137,
	PermissionSet__ctor_m1_4138,
	PermissionSet__ctor_m1_4139,
	PermissionSet_set_DeclarativeSecurity_m1_4140,
	PermissionSet_CreateFromBinaryFormat_m1_4141,
	SecurityContext__ctor_m1_4142,
	SecurityContext__ctor_m1_4143,
	SecurityContext_Capture_m1_4144,
	SecurityContext_get_FlowSuppressed_m1_4145,
	SecurityContext_get_CompressedStack_m1_4146,
	SecurityAttribute__ctor_m1_4147,
	SecurityAttribute_get_Name_m1_4148,
	SecurityAttribute_get_Value_m1_4149,
	SecurityElement__ctor_m1_4150,
	SecurityElement__ctor_m1_4151,
	SecurityElement__cctor_m1_4152,
	SecurityElement_get_Children_m1_4153,
	SecurityElement_get_Tag_m1_4154,
	SecurityElement_set_Text_m1_4155,
	SecurityElement_AddAttribute_m1_4156,
	SecurityElement_AddChild_m1_4157,
	SecurityElement_Escape_m1_4158,
	SecurityElement_Unescape_m1_4159,
	SecurityElement_IsValidAttributeName_m1_4160,
	SecurityElement_IsValidAttributeValue_m1_4161,
	SecurityElement_IsValidTag_m1_4162,
	SecurityElement_IsValidText_m1_4163,
	SecurityElement_SearchForChildByTag_m1_4164,
	SecurityElement_ToString_m1_4165,
	SecurityElement_ToXml_m1_4166,
	SecurityElement_GetAttribute_m1_4167,
	SecurityException__ctor_m1_4168,
	SecurityException__ctor_m1_4169,
	SecurityException__ctor_m1_4170,
	SecurityException_get_Demanded_m1_4171,
	SecurityException_get_FirstPermissionThatFailed_m1_4172,
	SecurityException_get_PermissionState_m1_4173,
	SecurityException_get_PermissionType_m1_4174,
	SecurityException_get_GrantedSet_m1_4175,
	SecurityException_get_RefusedSet_m1_4176,
	SecurityException_GetObjectData_m1_4177,
	SecurityException_ToString_m1_4178,
	SecurityFrame__ctor_m1_4179,
	SecurityFrame__GetSecurityStack_m1_4180,
	SecurityFrame_InitFromRuntimeFrame_m1_4181,
	SecurityFrame_get_Assembly_m1_4182,
	SecurityFrame_get_Domain_m1_4183,
	SecurityFrame_ToString_m1_4184,
	SecurityFrame_GetStack_m1_4185,
	SecurityManager__cctor_m1_4186,
	SecurityManager_get_SecurityEnabled_m1_4187,
	SecurityManager_Decode_m1_4188,
	SecurityManager_Decode_m1_4189,
	SecuritySafeCriticalAttribute__ctor_m1_4190,
	SuppressUnmanagedCodeSecurityAttribute__ctor_m1_4191,
	UnverifiableCodeAttribute__ctor_m1_4192,
	ASCIIEncoding__ctor_m1_4193,
	ASCIIEncoding_GetByteCount_m1_4194,
	ASCIIEncoding_GetByteCount_m1_4195,
	ASCIIEncoding_GetBytes_m1_4196,
	ASCIIEncoding_GetBytes_m1_4197,
	ASCIIEncoding_GetBytes_m1_4198,
	ASCIIEncoding_GetBytes_m1_4199,
	ASCIIEncoding_GetCharCount_m1_4200,
	ASCIIEncoding_GetChars_m1_4201,
	ASCIIEncoding_GetChars_m1_4202,
	ASCIIEncoding_GetMaxByteCount_m1_4203,
	ASCIIEncoding_GetMaxCharCount_m1_4204,
	ASCIIEncoding_GetString_m1_4205,
	ASCIIEncoding_GetBytes_m1_4206,
	ASCIIEncoding_GetByteCount_m1_4207,
	ASCIIEncoding_GetDecoder_m1_4208,
	Decoder__ctor_m1_4209,
	Decoder_set_Fallback_m1_4210,
	Decoder_get_FallbackBuffer_m1_4211,
	DecoderExceptionFallback__ctor_m1_4212,
	DecoderExceptionFallback_CreateFallbackBuffer_m1_4213,
	DecoderExceptionFallback_Equals_m1_4214,
	DecoderExceptionFallback_GetHashCode_m1_4215,
	DecoderExceptionFallbackBuffer__ctor_m1_4216,
	DecoderExceptionFallbackBuffer_get_Remaining_m1_4217,
	DecoderExceptionFallbackBuffer_Fallback_m1_4218,
	DecoderExceptionFallbackBuffer_GetNextChar_m1_4219,
	DecoderFallback__ctor_m1_4220,
	DecoderFallback__cctor_m1_4221,
	DecoderFallback_get_ExceptionFallback_m1_4222,
	DecoderFallback_get_ReplacementFallback_m1_4223,
	DecoderFallback_get_StandardSafeFallback_m1_4224,
	DecoderFallbackBuffer__ctor_m1_4225,
	DecoderFallbackBuffer_Reset_m1_4226,
	DecoderFallbackException__ctor_m1_4227,
	DecoderFallbackException__ctor_m1_4228,
	DecoderFallbackException__ctor_m1_4229,
	DecoderReplacementFallback__ctor_m1_4230,
	DecoderReplacementFallback__ctor_m1_4231,
	DecoderReplacementFallback_get_DefaultString_m1_4232,
	DecoderReplacementFallback_CreateFallbackBuffer_m1_4233,
	DecoderReplacementFallback_Equals_m1_4234,
	DecoderReplacementFallback_GetHashCode_m1_4235,
	DecoderReplacementFallbackBuffer__ctor_m1_4236,
	DecoderReplacementFallbackBuffer_get_Remaining_m1_4237,
	DecoderReplacementFallbackBuffer_Fallback_m1_4238,
	DecoderReplacementFallbackBuffer_GetNextChar_m1_4239,
	DecoderReplacementFallbackBuffer_Reset_m1_4240,
	EncoderExceptionFallback__ctor_m1_4241,
	EncoderExceptionFallback_CreateFallbackBuffer_m1_4242,
	EncoderExceptionFallback_Equals_m1_4243,
	EncoderExceptionFallback_GetHashCode_m1_4244,
	EncoderExceptionFallbackBuffer__ctor_m1_4245,
	EncoderExceptionFallbackBuffer_get_Remaining_m1_4246,
	EncoderExceptionFallbackBuffer_Fallback_m1_4247,
	EncoderExceptionFallbackBuffer_Fallback_m1_4248,
	EncoderExceptionFallbackBuffer_GetNextChar_m1_4249,
	EncoderFallback__ctor_m1_4250,
	EncoderFallback__cctor_m1_4251,
	EncoderFallback_get_ExceptionFallback_m1_4252,
	EncoderFallback_get_ReplacementFallback_m1_4253,
	EncoderFallback_get_StandardSafeFallback_m1_4254,
	EncoderFallbackBuffer__ctor_m1_4255,
	EncoderFallbackException__ctor_m1_4256,
	EncoderFallbackException__ctor_m1_4257,
	EncoderFallbackException__ctor_m1_4258,
	EncoderFallbackException__ctor_m1_4259,
	EncoderReplacementFallback__ctor_m1_4260,
	EncoderReplacementFallback__ctor_m1_4261,
	EncoderReplacementFallback_get_DefaultString_m1_4262,
	EncoderReplacementFallback_CreateFallbackBuffer_m1_4263,
	EncoderReplacementFallback_Equals_m1_4264,
	EncoderReplacementFallback_GetHashCode_m1_4265,
	EncoderReplacementFallbackBuffer__ctor_m1_4266,
	EncoderReplacementFallbackBuffer_get_Remaining_m1_4267,
	EncoderReplacementFallbackBuffer_Fallback_m1_4268,
	EncoderReplacementFallbackBuffer_Fallback_m1_4269,
	EncoderReplacementFallbackBuffer_Fallback_m1_4270,
	EncoderReplacementFallbackBuffer_GetNextChar_m1_4271,
	ForwardingDecoder__ctor_m1_4272,
	ForwardingDecoder_GetChars_m1_4273,
	Encoding__ctor_m1_4274,
	Encoding__ctor_m1_4275,
	Encoding__cctor_m1_4276,
	Encoding___m1_4277,
	Encoding_get_IsReadOnly_m1_4278,
	Encoding_get_DecoderFallback_m1_4279,
	Encoding_set_DecoderFallback_m1_4280,
	Encoding_get_EncoderFallback_m1_4281,
	Encoding_SetFallbackInternal_m1_4282,
	Encoding_Equals_m1_4283,
	Encoding_GetByteCount_m1_4284,
	Encoding_GetByteCount_m1_4285,
	Encoding_GetBytes_m1_4286,
	Encoding_GetBytes_m1_4287,
	Encoding_GetBytes_m1_4288,
	Encoding_GetBytes_m1_4289,
	Encoding_GetChars_m1_4290,
	Encoding_GetDecoder_m1_4291,
	Encoding_InvokeI18N_m1_4292,
	Encoding_GetEncoding_m1_4293,
	Encoding_Clone_m1_4294,
	Encoding_GetEncoding_m1_4295,
	Encoding_GetHashCode_m1_4296,
	Encoding_GetPreamble_m1_4297,
	Encoding_GetString_m1_4298,
	Encoding_GetString_m1_4299,
	Encoding_get_ASCII_m1_4300,
	Encoding_get_BigEndianUnicode_m1_4301,
	Encoding_InternalCodePage_m1_4302,
	Encoding_get_Default_m1_4303,
	Encoding_get_ISOLatin1_m1_4304,
	Encoding_get_UTF7_m1_4305,
	Encoding_get_UTF8_m1_4306,
	Encoding_get_UTF8Unmarked_m1_4307,
	Encoding_get_UTF8UnmarkedUnsafe_m1_4308,
	Encoding_get_Unicode_m1_4309,
	Encoding_get_UTF32_m1_4310,
	Encoding_get_BigEndianUTF32_m1_4311,
	Encoding_GetByteCount_m1_4312,
	Encoding_GetBytes_m1_4313,
	Latin1Encoding__ctor_m1_4314,
	Latin1Encoding_GetByteCount_m1_4315,
	Latin1Encoding_GetByteCount_m1_4316,
	Latin1Encoding_GetBytes_m1_4317,
	Latin1Encoding_GetBytes_m1_4318,
	Latin1Encoding_GetBytes_m1_4319,
	Latin1Encoding_GetBytes_m1_4320,
	Latin1Encoding_GetCharCount_m1_4321,
	Latin1Encoding_GetChars_m1_4322,
	Latin1Encoding_GetMaxByteCount_m1_4323,
	Latin1Encoding_GetMaxCharCount_m1_4324,
	Latin1Encoding_GetString_m1_4325,
	Latin1Encoding_GetString_m1_4326,
	StringBuilder__ctor_m1_4327,
	StringBuilder__ctor_m1_4328,
	StringBuilder__ctor_m1_4329,
	StringBuilder__ctor_m1_4330,
	StringBuilder__ctor_m1_4331,
	StringBuilder__ctor_m1_4332,
	StringBuilder__ctor_m1_4333,
	StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m1_4334,
	StringBuilder_get_Capacity_m1_4335,
	StringBuilder_set_Capacity_m1_4336,
	StringBuilder_get_Length_m1_4337,
	StringBuilder_set_Length_m1_4338,
	StringBuilder_get_Chars_m1_4339,
	StringBuilder_set_Chars_m1_4340,
	StringBuilder_ToString_m1_4341,
	StringBuilder_ToString_m1_4342,
	StringBuilder_Remove_m1_4343,
	StringBuilder_Replace_m1_4344,
	StringBuilder_Replace_m1_4345,
	StringBuilder_Append_m1_4346,
	StringBuilder_Append_m1_4347,
	StringBuilder_Append_m1_4348,
	StringBuilder_Append_m1_4349,
	StringBuilder_Append_m1_4350,
	StringBuilder_Append_m1_4351,
	StringBuilder_Append_m1_4352,
	StringBuilder_Append_m1_4353,
	StringBuilder_AppendLine_m1_4354,
	StringBuilder_AppendFormat_m1_4355,
	StringBuilder_AppendFormat_m1_4356,
	StringBuilder_AppendFormat_m1_4357,
	StringBuilder_AppendFormat_m1_4358,
	StringBuilder_AppendFormat_m1_4359,
	StringBuilder_Insert_m1_4360,
	StringBuilder_Insert_m1_4361,
	StringBuilder_Insert_m1_4362,
	StringBuilder_InternalEnsureCapacity_m1_4363,
	UTF32Decoder__ctor_m1_4364,
	UTF32Decoder_GetChars_m1_4365,
	UTF32Encoding__ctor_m1_4366,
	UTF32Encoding__ctor_m1_4367,
	UTF32Encoding__ctor_m1_4368,
	UTF32Encoding_GetByteCount_m1_4369,
	UTF32Encoding_GetBytes_m1_4370,
	UTF32Encoding_GetCharCount_m1_4371,
	UTF32Encoding_GetChars_m1_4372,
	UTF32Encoding_GetMaxByteCount_m1_4373,
	UTF32Encoding_GetMaxCharCount_m1_4374,
	UTF32Encoding_GetDecoder_m1_4375,
	UTF32Encoding_GetPreamble_m1_4376,
	UTF32Encoding_Equals_m1_4377,
	UTF32Encoding_GetHashCode_m1_4378,
	UTF32Encoding_GetByteCount_m1_4379,
	UTF32Encoding_GetByteCount_m1_4380,
	UTF32Encoding_GetBytes_m1_4381,
	UTF32Encoding_GetBytes_m1_4382,
	UTF32Encoding_GetString_m1_4383,
	UTF7Decoder__ctor_m1_4384,
	UTF7Decoder_GetChars_m1_4385,
	UTF7Encoding__ctor_m1_4386,
	UTF7Encoding__ctor_m1_4387,
	UTF7Encoding__cctor_m1_4388,
	UTF7Encoding_GetHashCode_m1_4389,
	UTF7Encoding_Equals_m1_4390,
	UTF7Encoding_InternalGetByteCount_m1_4391,
	UTF7Encoding_GetByteCount_m1_4392,
	UTF7Encoding_InternalGetBytes_m1_4393,
	UTF7Encoding_GetBytes_m1_4394,
	UTF7Encoding_InternalGetCharCount_m1_4395,
	UTF7Encoding_GetCharCount_m1_4396,
	UTF7Encoding_InternalGetChars_m1_4397,
	UTF7Encoding_GetChars_m1_4398,
	UTF7Encoding_GetMaxByteCount_m1_4399,
	UTF7Encoding_GetMaxCharCount_m1_4400,
	UTF7Encoding_GetDecoder_m1_4401,
	UTF7Encoding_GetByteCount_m1_4402,
	UTF7Encoding_GetByteCount_m1_4403,
	UTF7Encoding_GetBytes_m1_4404,
	UTF7Encoding_GetBytes_m1_4405,
	UTF7Encoding_GetString_m1_4406,
	UTF8Decoder__ctor_m1_4407,
	UTF8Decoder_GetChars_m1_4408,
	UTF8Encoding__ctor_m1_4409,
	UTF8Encoding__ctor_m1_4410,
	UTF8Encoding__ctor_m1_4411,
	UTF8Encoding_InternalGetByteCount_m1_4412,
	UTF8Encoding_InternalGetByteCount_m1_4413,
	UTF8Encoding_GetByteCount_m1_4414,
	UTF8Encoding_GetByteCount_m1_4415,
	UTF8Encoding_InternalGetBytes_m1_4416,
	UTF8Encoding_InternalGetBytes_m1_4417,
	UTF8Encoding_GetBytes_m1_4418,
	UTF8Encoding_GetBytes_m1_4419,
	UTF8Encoding_GetBytes_m1_4420,
	UTF8Encoding_InternalGetCharCount_m1_4421,
	UTF8Encoding_InternalGetCharCount_m1_4422,
	UTF8Encoding_Fallback_m1_4423,
	UTF8Encoding_Fallback_m1_4424,
	UTF8Encoding_GetCharCount_m1_4425,
	UTF8Encoding_InternalGetChars_m1_4426,
	UTF8Encoding_InternalGetChars_m1_4427,
	UTF8Encoding_GetChars_m1_4428,
	UTF8Encoding_GetMaxByteCount_m1_4429,
	UTF8Encoding_GetMaxCharCount_m1_4430,
	UTF8Encoding_GetDecoder_m1_4431,
	UTF8Encoding_GetPreamble_m1_4432,
	UTF8Encoding_Equals_m1_4433,
	UTF8Encoding_GetHashCode_m1_4434,
	UTF8Encoding_GetByteCount_m1_4435,
	UTF8Encoding_GetString_m1_4436,
	UnicodeDecoder__ctor_m1_4437,
	UnicodeDecoder_GetChars_m1_4438,
	UnicodeEncoding__ctor_m1_4439,
	UnicodeEncoding__ctor_m1_4440,
	UnicodeEncoding__ctor_m1_4441,
	UnicodeEncoding_GetByteCount_m1_4442,
	UnicodeEncoding_GetByteCount_m1_4443,
	UnicodeEncoding_GetByteCount_m1_4444,
	UnicodeEncoding_GetBytes_m1_4445,
	UnicodeEncoding_GetBytes_m1_4446,
	UnicodeEncoding_GetBytes_m1_4447,
	UnicodeEncoding_GetBytesInternal_m1_4448,
	UnicodeEncoding_GetCharCount_m1_4449,
	UnicodeEncoding_GetChars_m1_4450,
	UnicodeEncoding_GetString_m1_4451,
	UnicodeEncoding_GetCharsInternal_m1_4452,
	UnicodeEncoding_GetMaxByteCount_m1_4453,
	UnicodeEncoding_GetMaxCharCount_m1_4454,
	UnicodeEncoding_GetDecoder_m1_4455,
	UnicodeEncoding_GetPreamble_m1_4456,
	UnicodeEncoding_Equals_m1_4457,
	UnicodeEncoding_GetHashCode_m1_4458,
	UnicodeEncoding_CopyChars_m1_4459,
	CompressedStack__ctor_m1_4460,
	CompressedStack__ctor_m1_4461,
	CompressedStack_CreateCopy_m1_4462,
	CompressedStack_Capture_m1_4463,
	CompressedStack_GetObjectData_m1_4464,
	CompressedStack_IsEmpty_m1_4465,
	EventWaitHandle__ctor_m1_4466,
	EventWaitHandle_IsManualReset_m1_4467,
	EventWaitHandle_Reset_m1_4468,
	EventWaitHandle_Set_m1_4469,
	ExecutionContext__ctor_m1_4470,
	ExecutionContext__ctor_m1_4471,
	ExecutionContext__ctor_m1_4472,
	ExecutionContext_Capture_m1_4473,
	ExecutionContext_GetObjectData_m1_4474,
	ExecutionContext_get_SecurityContext_m1_4475,
	ExecutionContext_set_SecurityContext_m1_4476,
	ExecutionContext_get_FlowSuppressed_m1_4477,
	ExecutionContext_IsFlowSuppressed_m1_4478,
	Interlocked_CompareExchange_m1_4479,
	ManualResetEvent__ctor_m1_4480,
	Monitor_Enter_m1_4481,
	Monitor_Exit_m1_4482,
	Monitor_Monitor_pulse_m1_4483,
	Monitor_Monitor_test_synchronised_m1_4484,
	Monitor_Pulse_m1_4485,
	Monitor_Monitor_wait_m1_4486,
	Monitor_Wait_m1_4487,
	Mutex__ctor_m1_4488,
	Mutex_CreateMutex_internal_m1_4489,
	Mutex_ReleaseMutex_internal_m1_4490,
	Mutex_ReleaseMutex_m1_4491,
	NativeEventCalls_CreateEvent_internal_m1_4492,
	NativeEventCalls_SetEvent_internal_m1_4493,
	NativeEventCalls_ResetEvent_internal_m1_4494,
	NativeEventCalls_CloseEvent_internal_m1_4495,
	SynchronizationLockException__ctor_m1_4496,
	SynchronizationLockException__ctor_m1_4497,
	SynchronizationLockException__ctor_m1_4498,
	Thread__ctor_m1_4499,
	Thread__cctor_m1_4500,
	Thread_get_CurrentContext_m1_4501,
	Thread_CurrentThread_internal_m1_4502,
	Thread_get_CurrentThread_m1_4503,
	Thread_FreeLocalSlotValues_m1_4504,
	Thread_GetDomainID_m1_4505,
	Thread_ResetAbort_internal_m1_4506,
	Thread_ResetAbort_m1_4507,
	Thread_Sleep_internal_m1_4508,
	Thread_Sleep_m1_4509,
	Thread_Thread_internal_m1_4510,
	Thread_Thread_init_m1_4511,
	Thread_GetCachedCurrentCulture_m1_4512,
	Thread_GetSerializedCurrentCulture_m1_4513,
	Thread_SetCachedCurrentCulture_m1_4514,
	Thread_GetCachedCurrentUICulture_m1_4515,
	Thread_GetSerializedCurrentUICulture_m1_4516,
	Thread_SetCachedCurrentUICulture_m1_4517,
	Thread_get_CurrentCulture_m1_4518,
	Thread_get_CurrentUICulture_m1_4519,
	Thread_set_IsBackground_m1_4520,
	Thread_SetName_internal_m1_4521,
	Thread_set_Name_m1_4522,
	Thread_Abort_internal_m1_4523,
	Thread_Abort_m1_4524,
	Thread_Start_m1_4525,
	Thread_Thread_free_internal_m1_4526,
	Thread_Finalize_m1_4527,
	Thread_SetState_m1_4528,
	Thread_ClrState_m1_4529,
	Thread_GetNewManagedId_m1_4530,
	Thread_GetNewManagedId_internal_m1_4531,
	Thread_get_ExecutionContext_m1_4532,
	Thread_get_ManagedThreadId_m1_4533,
	Thread_GetHashCode_m1_4534,
	Thread_GetCompressedStack_m1_4535,
	ThreadAbortException__ctor_m1_4536,
	ThreadAbortException__ctor_m1_4537,
	ThreadInterruptedException__ctor_m1_4538,
	ThreadInterruptedException__ctor_m1_4539,
	ThreadPool_QueueUserWorkItem_m1_4540,
	ThreadStateException__ctor_m1_4541,
	ThreadStateException__ctor_m1_4542,
	TimerComparer__ctor_m1_4543,
	TimerComparer_Compare_m1_4544,
	Scheduler__ctor_m1_4545,
	Scheduler__cctor_m1_4546,
	Scheduler_get_Instance_m1_4547,
	Scheduler_Remove_m1_4548,
	Scheduler_Change_m1_4549,
	Scheduler_Add_m1_4550,
	Scheduler_InternalRemove_m1_4551,
	Scheduler_SchedulerThread_m1_4552,
	Scheduler_ShrinkIfNeeded_m1_4553,
	Timer__cctor_m1_4554,
	Timer_Change_m1_4555,
	Timer_Dispose_m1_4556,
	Timer_Change_m1_4557,
	WaitHandle__ctor_m1_4558,
	WaitHandle__cctor_m1_4559,
	WaitHandle_System_IDisposable_Dispose_m1_4560,
	WaitHandle_get_Handle_m1_4561,
	WaitHandle_set_Handle_m1_4562,
	WaitHandle_WaitOne_internal_m1_4563,
	WaitHandle_Dispose_m1_4564,
	WaitHandle_WaitOne_m1_4565,
	WaitHandle_WaitOne_m1_4566,
	WaitHandle_CheckDisposed_m1_4567,
	WaitHandle_Finalize_m1_4568,
	AccessViolationException__ctor_m1_4569,
	AccessViolationException__ctor_m1_4570,
	ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m1_4571,
	ActivationContext_Finalize_m1_4572,
	ActivationContext_Dispose_m1_4573,
	ActivationContext_Dispose_m1_4574,
	Activator_CreateInstance_m1_4575,
	Activator_CreateInstance_m1_4576,
	Activator_CreateInstance_m1_4577,
	Activator_CreateInstance_m1_4578,
	Activator_CreateInstance_m1_4579,
	Activator_CheckType_m1_4580,
	Activator_CheckAbstractType_m1_4581,
	Activator_CreateInstanceInternal_m1_4582,
	AppDomain_add_UnhandledException_m1_4583,
	AppDomain_remove_UnhandledException_m1_4584,
	AppDomain_getFriendlyName_m1_4585,
	AppDomain_getCurDomain_m1_4586,
	AppDomain_get_CurrentDomain_m1_4587,
	AppDomain_LoadAssembly_m1_4588,
	AppDomain_Load_m1_4589,
	AppDomain_Load_m1_4590,
	AppDomain_InternalSetContext_m1_4591,
	AppDomain_InternalGetContext_m1_4592,
	AppDomain_InternalGetDefaultContext_m1_4593,
	AppDomain_InternalGetProcessGuid_m1_4594,
	AppDomain_GetProcessGuid_m1_4595,
	AppDomain_ToString_m1_4596,
	AppDomain_DoTypeResolve_m1_4597,
	AppDomainSetup__ctor_m1_4598,
	ApplicationException__ctor_m1_4599,
	ApplicationException__ctor_m1_4600,
	ApplicationException__ctor_m1_4601,
	ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m1_4602,
	ApplicationIdentity_ToString_m1_4603,
	ArgumentException__ctor_m1_4604,
	ArgumentException__ctor_m1_4605,
	ArgumentException__ctor_m1_4606,
	ArgumentException__ctor_m1_4607,
	ArgumentException__ctor_m1_4608,
	ArgumentException__ctor_m1_4609,
	ArgumentException_get_ParamName_m1_4610,
	ArgumentException_get_Message_m1_4611,
	ArgumentException_GetObjectData_m1_4612,
	ArgumentNullException__ctor_m1_4613,
	ArgumentNullException__ctor_m1_4614,
	ArgumentNullException__ctor_m1_4615,
	ArgumentNullException__ctor_m1_4616,
	ArgumentOutOfRangeException__ctor_m1_4617,
	ArgumentOutOfRangeException__ctor_m1_4618,
	ArgumentOutOfRangeException__ctor_m1_4619,
	ArgumentOutOfRangeException__ctor_m1_4620,
	ArgumentOutOfRangeException__ctor_m1_4621,
	ArgumentOutOfRangeException_get_Message_m1_4622,
	ArgumentOutOfRangeException_GetObjectData_m1_4623,
	ArithmeticException__ctor_m1_4624,
	ArithmeticException__ctor_m1_4625,
	ArithmeticException__ctor_m1_4626,
	ArrayTypeMismatchException__ctor_m1_4627,
	ArrayTypeMismatchException__ctor_m1_4628,
	ArrayTypeMismatchException__ctor_m1_4629,
	BitConverter__cctor_m1_4630,
	BitConverter_AmILittleEndian_m1_4631,
	BitConverter_DoubleWordsAreSwapped_m1_4632,
	BitConverter_DoubleToInt64Bits_m1_4633,
	BitConverter_GetBytes_m1_4634,
	BitConverter_GetBytes_m1_4635,
	BitConverter_GetBytes_m1_4636,
	BitConverter_PutBytes_m1_4637,
	BitConverter_ToInt64_m1_4638,
	BitConverter_ToUInt16_m1_4639,
	BitConverter_ToUInt32_m1_4640,
	BitConverter_ToUInt64_m1_4641,
	BitConverter_ToSingle_m1_4642,
	BitConverter_ToDouble_m1_4643,
	BitConverter_ToString_m1_4644,
	BitConverter_ToString_m1_4645,
	Buffer_ByteLength_m1_4646,
	Buffer_BlockCopy_m1_4647,
	Buffer_ByteLengthInternal_m1_4648,
	Buffer_BlockCopyInternal_m1_4649,
	CharEnumerator__ctor_m1_4650,
	CharEnumerator_System_Collections_IEnumerator_get_Current_m1_4651,
	CharEnumerator_System_IDisposable_Dispose_m1_4652,
	CharEnumerator_get_Current_m1_4653,
	CharEnumerator_Clone_m1_4654,
	CharEnumerator_MoveNext_m1_4655,
	CharEnumerator_Reset_m1_4656,
	Console__cctor_m1_4657,
	Console_SetEncodings_m1_4658,
	Console_get_Error_m1_4659,
	Console_Open_m1_4660,
	Console_OpenStandardError_m1_4661,
	Console_OpenStandardInput_m1_4662,
	Console_OpenStandardOutput_m1_4663,
	Console_WriteLine_m1_4664,
	Console_WriteLine_m1_4665,
	ContextBoundObject__ctor_m1_4666,
	Convert__cctor_m1_4667,
	Convert_InternalFromBase64String_m1_4668,
	Convert_FromBase64String_m1_4669,
	Convert_ToBase64String_m1_4670,
	Convert_ToBase64String_m1_4671,
	Convert_ToBoolean_m1_4672,
	Convert_ToBoolean_m1_4673,
	Convert_ToBoolean_m1_4674,
	Convert_ToBoolean_m1_4675,
	Convert_ToBoolean_m1_4676,
	Convert_ToBoolean_m1_4677,
	Convert_ToBoolean_m1_4678,
	Convert_ToBoolean_m1_4679,
	Convert_ToBoolean_m1_4680,
	Convert_ToBoolean_m1_4681,
	Convert_ToBoolean_m1_4682,
	Convert_ToBoolean_m1_4683,
	Convert_ToBoolean_m1_4684,
	Convert_ToBoolean_m1_4685,
	Convert_ToByte_m1_4686,
	Convert_ToByte_m1_4687,
	Convert_ToByte_m1_4688,
	Convert_ToByte_m1_4689,
	Convert_ToByte_m1_4690,
	Convert_ToByte_m1_4691,
	Convert_ToByte_m1_4692,
	Convert_ToByte_m1_4693,
	Convert_ToByte_m1_4694,
	Convert_ToByte_m1_4695,
	Convert_ToByte_m1_4696,
	Convert_ToByte_m1_4697,
	Convert_ToByte_m1_4698,
	Convert_ToByte_m1_4699,
	Convert_ToByte_m1_4700,
	Convert_ToChar_m1_4701,
	Convert_ToChar_m1_4702,
	Convert_ToChar_m1_4703,
	Convert_ToChar_m1_4704,
	Convert_ToChar_m1_4705,
	Convert_ToChar_m1_4706,
	Convert_ToChar_m1_4707,
	Convert_ToChar_m1_4708,
	Convert_ToChar_m1_4709,
	Convert_ToChar_m1_4710,
	Convert_ToChar_m1_4711,
	Convert_ToDateTime_m1_4712,
	Convert_ToDateTime_m1_4713,
	Convert_ToDateTime_m1_4714,
	Convert_ToDateTime_m1_4715,
	Convert_ToDateTime_m1_4716,
	Convert_ToDateTime_m1_4717,
	Convert_ToDateTime_m1_4718,
	Convert_ToDateTime_m1_4719,
	Convert_ToDateTime_m1_4720,
	Convert_ToDateTime_m1_4721,
	Convert_ToDecimal_m1_4722,
	Convert_ToDecimal_m1_4723,
	Convert_ToDecimal_m1_4724,
	Convert_ToDecimal_m1_4725,
	Convert_ToDecimal_m1_4726,
	Convert_ToDecimal_m1_4727,
	Convert_ToDecimal_m1_4728,
	Convert_ToDecimal_m1_4729,
	Convert_ToDecimal_m1_4730,
	Convert_ToDecimal_m1_4731,
	Convert_ToDecimal_m1_4732,
	Convert_ToDecimal_m1_4733,
	Convert_ToDecimal_m1_4734,
	Convert_ToDouble_m1_4735,
	Convert_ToDouble_m1_4736,
	Convert_ToDouble_m1_4737,
	Convert_ToDouble_m1_4738,
	Convert_ToDouble_m1_4739,
	Convert_ToDouble_m1_4740,
	Convert_ToDouble_m1_4741,
	Convert_ToDouble_m1_4742,
	Convert_ToDouble_m1_4743,
	Convert_ToDouble_m1_4744,
	Convert_ToDouble_m1_4745,
	Convert_ToDouble_m1_4746,
	Convert_ToDouble_m1_4747,
	Convert_ToDouble_m1_4748,
	Convert_ToInt16_m1_4749,
	Convert_ToInt16_m1_4750,
	Convert_ToInt16_m1_4751,
	Convert_ToInt16_m1_4752,
	Convert_ToInt16_m1_4753,
	Convert_ToInt16_m1_4754,
	Convert_ToInt16_m1_4755,
	Convert_ToInt16_m1_4756,
	Convert_ToInt16_m1_4757,
	Convert_ToInt16_m1_4758,
	Convert_ToInt16_m1_4759,
	Convert_ToInt16_m1_4760,
	Convert_ToInt16_m1_4761,
	Convert_ToInt16_m1_4762,
	Convert_ToInt16_m1_4763,
	Convert_ToInt16_m1_4764,
	Convert_ToInt32_m1_4765,
	Convert_ToInt32_m1_4766,
	Convert_ToInt32_m1_4767,
	Convert_ToInt32_m1_4768,
	Convert_ToInt32_m1_4769,
	Convert_ToInt32_m1_4770,
	Convert_ToInt32_m1_4771,
	Convert_ToInt32_m1_4772,
	Convert_ToInt32_m1_4773,
	Convert_ToInt32_m1_4774,
	Convert_ToInt32_m1_4775,
	Convert_ToInt32_m1_4776,
	Convert_ToInt32_m1_4777,
	Convert_ToInt32_m1_4778,
	Convert_ToInt32_m1_4779,
	Convert_ToInt64_m1_4780,
	Convert_ToInt64_m1_4781,
	Convert_ToInt64_m1_4782,
	Convert_ToInt64_m1_4783,
	Convert_ToInt64_m1_4784,
	Convert_ToInt64_m1_4785,
	Convert_ToInt64_m1_4786,
	Convert_ToInt64_m1_4787,
	Convert_ToInt64_m1_4788,
	Convert_ToInt64_m1_4789,
	Convert_ToInt64_m1_4790,
	Convert_ToInt64_m1_4791,
	Convert_ToInt64_m1_4792,
	Convert_ToInt64_m1_4793,
	Convert_ToInt64_m1_4794,
	Convert_ToInt64_m1_4795,
	Convert_ToInt64_m1_4796,
	Convert_ToSByte_m1_4797,
	Convert_ToSByte_m1_4798,
	Convert_ToSByte_m1_4799,
	Convert_ToSByte_m1_4800,
	Convert_ToSByte_m1_4801,
	Convert_ToSByte_m1_4802,
	Convert_ToSByte_m1_4803,
	Convert_ToSByte_m1_4804,
	Convert_ToSByte_m1_4805,
	Convert_ToSByte_m1_4806,
	Convert_ToSByte_m1_4807,
	Convert_ToSByte_m1_4808,
	Convert_ToSByte_m1_4809,
	Convert_ToSByte_m1_4810,
	Convert_ToSingle_m1_4811,
	Convert_ToSingle_m1_4812,
	Convert_ToSingle_m1_4813,
	Convert_ToSingle_m1_4814,
	Convert_ToSingle_m1_4815,
	Convert_ToSingle_m1_4816,
	Convert_ToSingle_m1_4817,
	Convert_ToSingle_m1_4818,
	Convert_ToSingle_m1_4819,
	Convert_ToSingle_m1_4820,
	Convert_ToSingle_m1_4821,
	Convert_ToSingle_m1_4822,
	Convert_ToSingle_m1_4823,
	Convert_ToSingle_m1_4824,
	Convert_ToString_m1_4825,
	Convert_ToString_m1_4826,
	Convert_ToString_m1_4827,
	Convert_ToUInt16_m1_4828,
	Convert_ToUInt16_m1_4829,
	Convert_ToUInt16_m1_4830,
	Convert_ToUInt16_m1_4831,
	Convert_ToUInt16_m1_4832,
	Convert_ToUInt16_m1_4833,
	Convert_ToUInt16_m1_4834,
	Convert_ToUInt16_m1_4835,
	Convert_ToUInt16_m1_4836,
	Convert_ToUInt16_m1_4837,
	Convert_ToUInt16_m1_4838,
	Convert_ToUInt16_m1_4839,
	Convert_ToUInt16_m1_4840,
	Convert_ToUInt16_m1_4841,
	Convert_ToUInt32_m1_4842,
	Convert_ToUInt32_m1_4843,
	Convert_ToUInt32_m1_4844,
	Convert_ToUInt32_m1_4845,
	Convert_ToUInt32_m1_4846,
	Convert_ToUInt32_m1_4847,
	Convert_ToUInt32_m1_4848,
	Convert_ToUInt32_m1_4849,
	Convert_ToUInt32_m1_4850,
	Convert_ToUInt32_m1_4851,
	Convert_ToUInt32_m1_4852,
	Convert_ToUInt32_m1_4853,
	Convert_ToUInt32_m1_4854,
	Convert_ToUInt32_m1_4855,
	Convert_ToUInt32_m1_4856,
	Convert_ToUInt64_m1_4857,
	Convert_ToUInt64_m1_4858,
	Convert_ToUInt64_m1_4859,
	Convert_ToUInt64_m1_4860,
	Convert_ToUInt64_m1_4861,
	Convert_ToUInt64_m1_4862,
	Convert_ToUInt64_m1_4863,
	Convert_ToUInt64_m1_4864,
	Convert_ToUInt64_m1_4865,
	Convert_ToUInt64_m1_4866,
	Convert_ToUInt64_m1_4867,
	Convert_ToUInt64_m1_4868,
	Convert_ToUInt64_m1_4869,
	Convert_ToUInt64_m1_4870,
	Convert_ToUInt64_m1_4871,
	Convert_ChangeType_m1_4872,
	Convert_EndianSwap_m1_4873,
	Convert_ConvertToBase2_m1_4874,
	Convert_ConvertToBase8_m1_4875,
	Convert_ConvertToBase16_m1_4876,
	Convert_ToType_m1_4877,
	DBNull__ctor_m1_4878,
	DBNull__ctor_m1_4879,
	DBNull__cctor_m1_4880,
	DBNull_System_IConvertible_ToBoolean_m1_4881,
	DBNull_System_IConvertible_ToByte_m1_4882,
	DBNull_System_IConvertible_ToChar_m1_4883,
	DBNull_System_IConvertible_ToDateTime_m1_4884,
	DBNull_System_IConvertible_ToDecimal_m1_4885,
	DBNull_System_IConvertible_ToDouble_m1_4886,
	DBNull_System_IConvertible_ToInt16_m1_4887,
	DBNull_System_IConvertible_ToInt32_m1_4888,
	DBNull_System_IConvertible_ToInt64_m1_4889,
	DBNull_System_IConvertible_ToSByte_m1_4890,
	DBNull_System_IConvertible_ToSingle_m1_4891,
	DBNull_System_IConvertible_ToType_m1_4892,
	DBNull_System_IConvertible_ToUInt16_m1_4893,
	DBNull_System_IConvertible_ToUInt32_m1_4894,
	DBNull_System_IConvertible_ToUInt64_m1_4895,
	DBNull_GetObjectData_m1_4896,
	DBNull_ToString_m1_4897,
	DBNull_ToString_m1_4898,
	DateTime__ctor_m1_4899,
	DateTime__ctor_m1_4900,
	DateTime__ctor_m1_4901,
	DateTime__ctor_m1_4902,
	DateTime__ctor_m1_4903,
	DateTime__ctor_m1_4904,
	DateTime__ctor_m1_4905,
	DateTime__cctor_m1_4906,
	DateTime_System_IConvertible_ToBoolean_m1_4907,
	DateTime_System_IConvertible_ToByte_m1_4908,
	DateTime_System_IConvertible_ToChar_m1_4909,
	DateTime_System_IConvertible_ToDateTime_m1_4910,
	DateTime_System_IConvertible_ToDecimal_m1_4911,
	DateTime_System_IConvertible_ToDouble_m1_4912,
	DateTime_System_IConvertible_ToInt16_m1_4913,
	DateTime_System_IConvertible_ToInt32_m1_4914,
	DateTime_System_IConvertible_ToInt64_m1_4915,
	DateTime_System_IConvertible_ToSByte_m1_4916,
	DateTime_System_IConvertible_ToSingle_m1_4917,
	DateTime_System_IConvertible_ToType_m1_4918,
	DateTime_System_IConvertible_ToUInt16_m1_4919,
	DateTime_System_IConvertible_ToUInt32_m1_4920,
	DateTime_System_IConvertible_ToUInt64_m1_4921,
	DateTime_AbsoluteDays_m1_4922,
	DateTime_FromTicks_m1_4923,
	DateTime_get_Month_m1_4924,
	DateTime_get_Day_m1_4925,
	DateTime_get_DayOfWeek_m1_4926,
	DateTime_get_Hour_m1_4927,
	DateTime_get_Minute_m1_4928,
	DateTime_get_Second_m1_4929,
	DateTime_GetTimeMonotonic_m1_4930,
	DateTime_GetNow_m1_4931,
	DateTime_get_Now_m1_4932,
	DateTime_get_Ticks_m1_4933,
	DateTime_get_Today_m1_4934,
	DateTime_get_UtcNow_m1_4935,
	DateTime_get_Year_m1_4936,
	DateTime_get_Kind_m1_4937,
	DateTime_Add_m1_4938,
	DateTime_AddTicks_m1_4939,
	DateTime_AddMilliseconds_m1_4940,
	DateTime_AddSeconds_m1_4941,
	DateTime_Compare_m1_4942,
	DateTime_CompareTo_m1_4943,
	DateTime_CompareTo_m1_4944,
	DateTime_Equals_m1_4945,
	DateTime_FromBinary_m1_4946,
	DateTime_SpecifyKind_m1_4947,
	DateTime_DaysInMonth_m1_4948,
	DateTime_Equals_m1_4949,
	DateTime_CheckDateTimeKind_m1_4950,
	DateTime_GetHashCode_m1_4951,
	DateTime_IsLeapYear_m1_4952,
	DateTime_Parse_m1_4953,
	DateTime_Parse_m1_4954,
	DateTime_CoreParse_m1_4955,
	DateTime_YearMonthDayFormats_m1_4956,
	DateTime__ParseNumber_m1_4957,
	DateTime__ParseEnum_m1_4958,
	DateTime__ParseString_m1_4959,
	DateTime__ParseAmPm_m1_4960,
	DateTime__ParseTimeSeparator_m1_4961,
	DateTime__ParseDateSeparator_m1_4962,
	DateTime_IsLetter_m1_4963,
	DateTime__DoParse_m1_4964,
	DateTime_ParseExact_m1_4965,
	DateTime_ParseExact_m1_4966,
	DateTime_CheckStyle_m1_4967,
	DateTime_ParseExact_m1_4968,
	DateTime_Subtract_m1_4969,
	DateTime_ToString_m1_4970,
	DateTime_ToString_m1_4971,
	DateTime_ToString_m1_4972,
	DateTime_ToLocalTime_m1_4973,
	DateTime_ToUniversalTime_m1_4974,
	DateTime_op_Addition_m1_4975,
	DateTime_op_Equality_m1_4976,
	DateTime_op_GreaterThan_m1_4977,
	DateTime_op_GreaterThanOrEqual_m1_4978,
	DateTime_op_Inequality_m1_4979,
	DateTime_op_LessThan_m1_4980,
	DateTime_op_LessThanOrEqual_m1_4981,
	DateTime_op_Subtraction_m1_4982,
	DateTimeOffset__ctor_m1_4983,
	DateTimeOffset__ctor_m1_4984,
	DateTimeOffset__ctor_m1_4985,
	DateTimeOffset__ctor_m1_4986,
	DateTimeOffset__cctor_m1_4987,
	DateTimeOffset_System_IComparable_CompareTo_m1_4988,
	DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m1_4989,
	DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_4990,
	DateTimeOffset_CompareTo_m1_4991,
	DateTimeOffset_Equals_m1_4992,
	DateTimeOffset_Equals_m1_4993,
	DateTimeOffset_GetHashCode_m1_4994,
	DateTimeOffset_ToString_m1_4995,
	DateTimeOffset_ToString_m1_4996,
	DateTimeOffset_get_DateTime_m1_4997,
	DateTimeOffset_get_Offset_m1_4998,
	DateTimeOffset_get_UtcDateTime_m1_4999,
	DateTimeUtils_CountRepeat_m1_5000,
	DateTimeUtils_ZeroPad_m1_5001,
	DateTimeUtils_ParseQuotedString_m1_5002,
	DateTimeUtils_GetStandardPattern_m1_5003,
	DateTimeUtils_GetStandardPattern_m1_5004,
	DateTimeUtils_ToString_m1_5005,
	DateTimeUtils_ToString_m1_5006,
	DelegateEntry__ctor_m1_5007,
	DelegateEntry_DeserializeDelegate_m1_5008,
	DelegateSerializationHolder__ctor_m1_5009,
	DelegateSerializationHolder_GetDelegateData_m1_5010,
	DelegateSerializationHolder_GetObjectData_m1_5011,
	DelegateSerializationHolder_GetRealObject_m1_5012,
	DivideByZeroException__ctor_m1_5013,
	DivideByZeroException__ctor_m1_5014,
	DllNotFoundException__ctor_m1_5015,
	DllNotFoundException__ctor_m1_5016,
	EntryPointNotFoundException__ctor_m1_5017,
	EntryPointNotFoundException__ctor_m1_5018,
	SByteComparer__ctor_m1_5019,
	SByteComparer_Compare_m1_5020,
	SByteComparer_Compare_m1_5021,
	ShortComparer__ctor_m1_5022,
	ShortComparer_Compare_m1_5023,
	ShortComparer_Compare_m1_5024,
	IntComparer__ctor_m1_5025,
	IntComparer_Compare_m1_5026,
	IntComparer_Compare_m1_5027,
	LongComparer__ctor_m1_5028,
	LongComparer_Compare_m1_5029,
	LongComparer_Compare_m1_5030,
	MonoEnumInfo__ctor_m1_5031,
	MonoEnumInfo__cctor_m1_5032,
	MonoEnumInfo_get_enum_info_m1_5033,
	MonoEnumInfo_get_Cache_m1_5034,
	MonoEnumInfo_GetInfo_m1_5035,
	Environment_get_SocketSecurityEnabled_m1_5036,
	Environment_get_NewLine_m1_5037,
	Environment_get_Platform_m1_5038,
	Environment_GetOSVersionString_m1_5039,
	Environment_get_OSVersion_m1_5040,
	Environment_get_TickCount_m1_5041,
	Environment_internalGetEnvironmentVariable_m1_5042,
	Environment_GetEnvironmentVariable_m1_5043,
	Environment_GetWindowsFolderPath_m1_5044,
	Environment_GetFolderPath_m1_5045,
	Environment_ReadXdgUserDir_m1_5046,
	Environment_InternalGetFolderPath_m1_5047,
	Environment_get_IsRunningOnWindows_m1_5048,
	Environment_GetMachineConfigPath_m1_5049,
	Environment_internalGetHome_m1_5050,
	EventArgs__ctor_m1_5051,
	EventArgs__cctor_m1_5052,
	ExecutionEngineException__ctor_m1_5053,
	ExecutionEngineException__ctor_m1_5054,
	FieldAccessException__ctor_m1_5055,
	FieldAccessException__ctor_m1_5056,
	FieldAccessException__ctor_m1_5057,
	FlagsAttribute__ctor_m1_5058,
	FormatException__ctor_m1_5059,
	FormatException__ctor_m1_5060,
	FormatException__ctor_m1_5061,
	GC_SuppressFinalize_m1_5062,
	Guid__ctor_m1_5063,
	Guid__ctor_m1_5064,
	Guid__cctor_m1_5065,
	Guid_CheckNull_m1_5066,
	Guid_CheckLength_m1_5067,
	Guid_CheckArray_m1_5068,
	Guid_Compare_m1_5069,
	Guid_CompareTo_m1_5070,
	Guid_Equals_m1_5071,
	Guid_CompareTo_m1_5072,
	Guid_Equals_m1_5073,
	Guid_GetHashCode_m1_5074,
	Guid_ToHex_m1_5075,
	Guid_NewGuid_m1_5076,
	Guid_AppendInt_m1_5077,
	Guid_AppendShort_m1_5078,
	Guid_AppendByte_m1_5079,
	Guid_BaseToString_m1_5080,
	Guid_ToString_m1_5081,
	Guid_ToString_m1_5082,
	Guid_ToString_m1_5083,
	IndexOutOfRangeException__ctor_m1_5084,
	IndexOutOfRangeException__ctor_m1_5085,
	IndexOutOfRangeException__ctor_m1_5086,
	InvalidCastException__ctor_m1_5087,
	InvalidCastException__ctor_m1_5088,
	InvalidCastException__ctor_m1_5089,
	InvalidOperationException__ctor_m1_5090,
	InvalidOperationException__ctor_m1_5091,
	InvalidOperationException__ctor_m1_5092,
	InvalidOperationException__ctor_m1_5093,
	LocalDataStoreSlot__ctor_m1_5094,
	LocalDataStoreSlot__cctor_m1_5095,
	LocalDataStoreSlot_Finalize_m1_5096,
	Math_Abs_m1_5097,
	Math_Abs_m1_5098,
	Math_Abs_m1_5099,
	Math_Floor_m1_5100,
	Math_Max_m1_5101,
	Math_Min_m1_5102,
	Math_Round_m1_5103,
	Math_Round_m1_5104,
	Math_Acos_m1_5105,
	Math_Pow_m1_5106,
	Math_Sqrt_m1_5107,
	MemberAccessException__ctor_m1_5108,
	MemberAccessException__ctor_m1_5109,
	MemberAccessException__ctor_m1_5110,
	MethodAccessException__ctor_m1_5111,
	MethodAccessException__ctor_m1_5112,
	MissingFieldException__ctor_m1_5113,
	MissingFieldException__ctor_m1_5114,
	MissingFieldException__ctor_m1_5115,
	MissingFieldException_get_Message_m1_5116,
	MissingMemberException__ctor_m1_5117,
	MissingMemberException__ctor_m1_5118,
	MissingMemberException__ctor_m1_5119,
	MissingMemberException__ctor_m1_5120,
	MissingMemberException_GetObjectData_m1_5121,
	MissingMemberException_get_Message_m1_5122,
	MissingMethodException__ctor_m1_5123,
	MissingMethodException__ctor_m1_5124,
	MissingMethodException__ctor_m1_5125,
	MissingMethodException__ctor_m1_5126,
	MissingMethodException_get_Message_m1_5127,
	MonoAsyncCall__ctor_m1_5128,
	AttributeInfo__ctor_m1_5129,
	AttributeInfo_get_Usage_m1_5130,
	AttributeInfo_get_InheritanceLevel_m1_5131,
	MonoCustomAttrs__cctor_m1_5132,
	MonoCustomAttrs_IsUserCattrProvider_m1_5133,
	MonoCustomAttrs_GetCustomAttributesInternal_m1_5134,
	MonoCustomAttrs_GetPseudoCustomAttributes_m1_5135,
	MonoCustomAttrs_GetCustomAttributesBase_m1_5136,
	MonoCustomAttrs_GetCustomAttribute_m1_5137,
	MonoCustomAttrs_GetCustomAttributes_m1_5138,
	MonoCustomAttrs_GetCustomAttributes_m1_5139,
	MonoCustomAttrs_GetCustomAttributesDataInternal_m1_5140,
	MonoCustomAttrs_GetCustomAttributesData_m1_5141,
	MonoCustomAttrs_IsDefined_m1_5142,
	MonoCustomAttrs_IsDefinedInternal_m1_5143,
	MonoCustomAttrs_GetBasePropertyDefinition_m1_5144,
	MonoCustomAttrs_GetBase_m1_5145,
	MonoCustomAttrs_RetrieveAttributeUsage_m1_5146,
	MonoTouchAOTHelper__cctor_m1_5147,
	MonoTypeInfo__ctor_m1_5148,
	MonoType_get_attributes_m1_5149,
	MonoType_GetDefaultConstructor_m1_5150,
	MonoType_GetAttributeFlagsImpl_m1_5151,
	MonoType_GetConstructorImpl_m1_5152,
	MonoType_GetConstructors_internal_m1_5153,
	MonoType_GetConstructors_m1_5154,
	MonoType_InternalGetEvent_m1_5155,
	MonoType_GetEvent_m1_5156,
	MonoType_GetField_m1_5157,
	MonoType_GetFields_internal_m1_5158,
	MonoType_GetFields_m1_5159,
	MonoType_GetInterfaces_m1_5160,
	MonoType_GetMethodsByName_m1_5161,
	MonoType_GetMethods_m1_5162,
	MonoType_GetMethodImpl_m1_5163,
	MonoType_GetPropertiesByName_m1_5164,
	MonoType_GetPropertyImpl_m1_5165,
	MonoType_HasElementTypeImpl_m1_5166,
	MonoType_IsArrayImpl_m1_5167,
	MonoType_IsByRefImpl_m1_5168,
	MonoType_IsPointerImpl_m1_5169,
	MonoType_IsPrimitiveImpl_m1_5170,
	MonoType_IsSubclassOf_m1_5171,
	MonoType_InvokeMember_m1_5172,
	MonoType_GetElementType_m1_5173,
	MonoType_get_UnderlyingSystemType_m1_5174,
	MonoType_get_Assembly_m1_5175,
	MonoType_get_AssemblyQualifiedName_m1_5176,
	MonoType_getFullName_m1_5177,
	MonoType_get_BaseType_m1_5178,
	MonoType_get_FullName_m1_5179,
	MonoType_IsDefined_m1_5180,
	MonoType_GetCustomAttributes_m1_5181,
	MonoType_GetCustomAttributes_m1_5182,
	MonoType_get_MemberType_m1_5183,
	MonoType_get_Name_m1_5184,
	MonoType_get_Namespace_m1_5185,
	MonoType_get_Module_m1_5186,
	MonoType_get_DeclaringType_m1_5187,
	MonoType_get_ReflectedType_m1_5188,
	MonoType_get_TypeHandle_m1_5189,
	MonoType_GetObjectData_m1_5190,
	MonoType_ToString_m1_5191,
	MonoType_GetGenericArguments_m1_5192,
	MonoType_get_ContainsGenericParameters_m1_5193,
	MonoType_get_IsGenericParameter_m1_5194,
	MonoType_GetGenericTypeDefinition_m1_5195,
	MonoType_CheckMethodSecurity_m1_5196,
	MonoType_ReorderParamArrayArguments_m1_5197,
	MulticastNotSupportedException__ctor_m1_5198,
	MulticastNotSupportedException__ctor_m1_5199,
	MulticastNotSupportedException__ctor_m1_5200,
	NonSerializedAttribute__ctor_m1_5201,
	NotImplementedException__ctor_m1_5202,
	NotImplementedException__ctor_m1_5203,
	NotImplementedException__ctor_m1_5204,
	NotSupportedException__ctor_m1_5205,
	NotSupportedException__ctor_m1_5206,
	NotSupportedException__ctor_m1_5207,
	NullReferenceException__ctor_m1_5208,
	NullReferenceException__ctor_m1_5209,
	NullReferenceException__ctor_m1_5210,
	CustomInfo__ctor_m1_5211,
	CustomInfo_GetActiveSection_m1_5212,
	CustomInfo_Parse_m1_5213,
	CustomInfo_Format_m1_5214,
	NumberFormatter__ctor_m1_5215,
	NumberFormatter__cctor_m1_5216,
	NumberFormatter_GetFormatterTables_m1_5217,
	NumberFormatter_GetTenPowerOf_m1_5218,
	NumberFormatter_InitDecHexDigits_m1_5219,
	NumberFormatter_InitDecHexDigits_m1_5220,
	NumberFormatter_InitDecHexDigits_m1_5221,
	NumberFormatter_FastToDecHex_m1_5222,
	NumberFormatter_ToDecHex_m1_5223,
	NumberFormatter_FastDecHexLen_m1_5224,
	NumberFormatter_DecHexLen_m1_5225,
	NumberFormatter_DecHexLen_m1_5226,
	NumberFormatter_ScaleOrder_m1_5227,
	NumberFormatter_InitialFloatingPrecision_m1_5228,
	NumberFormatter_ParsePrecision_m1_5229,
	NumberFormatter_Init_m1_5230,
	NumberFormatter_InitHex_m1_5231,
	NumberFormatter_Init_m1_5232,
	NumberFormatter_Init_m1_5233,
	NumberFormatter_Init_m1_5234,
	NumberFormatter_Init_m1_5235,
	NumberFormatter_Init_m1_5236,
	NumberFormatter_Init_m1_5237,
	NumberFormatter_ResetCharBuf_m1_5238,
	NumberFormatter_Resize_m1_5239,
	NumberFormatter_Append_m1_5240,
	NumberFormatter_Append_m1_5241,
	NumberFormatter_Append_m1_5242,
	NumberFormatter_GetNumberFormatInstance_m1_5243,
	NumberFormatter_set_CurrentCulture_m1_5244,
	NumberFormatter_get_IntegerDigits_m1_5245,
	NumberFormatter_get_DecimalDigits_m1_5246,
	NumberFormatter_get_IsFloatingSource_m1_5247,
	NumberFormatter_get_IsZero_m1_5248,
	NumberFormatter_get_IsZeroInteger_m1_5249,
	NumberFormatter_RoundPos_m1_5250,
	NumberFormatter_RoundDecimal_m1_5251,
	NumberFormatter_RoundBits_m1_5252,
	NumberFormatter_RemoveTrailingZeros_m1_5253,
	NumberFormatter_AddOneToDecHex_m1_5254,
	NumberFormatter_AddOneToDecHex_m1_5255,
	NumberFormatter_CountTrailingZeros_m1_5256,
	NumberFormatter_CountTrailingZeros_m1_5257,
	NumberFormatter_GetInstance_m1_5258,
	NumberFormatter_Release_m1_5259,
	NumberFormatter_SetThreadCurrentCulture_m1_5260,
	NumberFormatter_NumberToString_m1_5261,
	NumberFormatter_NumberToString_m1_5262,
	NumberFormatter_NumberToString_m1_5263,
	NumberFormatter_NumberToString_m1_5264,
	NumberFormatter_NumberToString_m1_5265,
	NumberFormatter_NumberToString_m1_5266,
	NumberFormatter_NumberToString_m1_5267,
	NumberFormatter_NumberToString_m1_5268,
	NumberFormatter_NumberToString_m1_5269,
	NumberFormatter_NumberToString_m1_5270,
	NumberFormatter_NumberToString_m1_5271,
	NumberFormatter_NumberToString_m1_5272,
	NumberFormatter_NumberToString_m1_5273,
	NumberFormatter_NumberToString_m1_5274,
	NumberFormatter_NumberToString_m1_5275,
	NumberFormatter_NumberToString_m1_5276,
	NumberFormatter_NumberToString_m1_5277,
	NumberFormatter_FastIntegerToString_m1_5278,
	NumberFormatter_IntegerToString_m1_5279,
	NumberFormatter_NumberToString_m1_5280,
	NumberFormatter_FormatCurrency_m1_5281,
	NumberFormatter_FormatDecimal_m1_5282,
	NumberFormatter_FormatHexadecimal_m1_5283,
	NumberFormatter_FormatFixedPoint_m1_5284,
	NumberFormatter_FormatRoundtrip_m1_5285,
	NumberFormatter_FormatRoundtrip_m1_5286,
	NumberFormatter_FormatGeneral_m1_5287,
	NumberFormatter_FormatNumber_m1_5288,
	NumberFormatter_FormatPercent_m1_5289,
	NumberFormatter_FormatExponential_m1_5290,
	NumberFormatter_FormatExponential_m1_5291,
	NumberFormatter_FormatCustom_m1_5292,
	NumberFormatter_ZeroTrimEnd_m1_5293,
	NumberFormatter_IsZeroOnly_m1_5294,
	NumberFormatter_AppendNonNegativeNumber_m1_5295,
	NumberFormatter_AppendIntegerString_m1_5296,
	NumberFormatter_AppendIntegerString_m1_5297,
	NumberFormatter_AppendDecimalString_m1_5298,
	NumberFormatter_AppendDecimalString_m1_5299,
	NumberFormatter_AppendIntegerStringWithGroupSeparator_m1_5300,
	NumberFormatter_AppendExponent_m1_5301,
	NumberFormatter_AppendOneDigit_m1_5302,
	NumberFormatter_FastAppendDigits_m1_5303,
	NumberFormatter_AppendDigits_m1_5304,
	NumberFormatter_AppendDigits_m1_5305,
	NumberFormatter_Multiply10_m1_5306,
	NumberFormatter_Divide10_m1_5307,
	NumberFormatter_GetClone_m1_5308,
	ObjectDisposedException__ctor_m1_5309,
	ObjectDisposedException__ctor_m1_5310,
	ObjectDisposedException__ctor_m1_5311,
	ObjectDisposedException_get_Message_m1_5312,
	ObjectDisposedException_GetObjectData_m1_5313,
	OperatingSystem__ctor_m1_5314,
	OperatingSystem_get_Platform_m1_5315,
	OperatingSystem_Clone_m1_5316,
	OperatingSystem_GetObjectData_m1_5317,
	OperatingSystem_ToString_m1_5318,
	OutOfMemoryException__ctor_m1_5319,
	OutOfMemoryException__ctor_m1_5320,
	OverflowException__ctor_m1_5321,
	OverflowException__ctor_m1_5322,
	OverflowException__ctor_m1_5323,
	Random__ctor_m1_5324,
	Random__ctor_m1_5325,
	Random_Sample_m1_5326,
	Random_Next_m1_5327,
	Random_Next_m1_5328,
	Random_NextDouble_m1_5329,
	RankException__ctor_m1_5330,
	RankException__ctor_m1_5331,
	RankException__ctor_m1_5332,
	ResolveEventArgs__ctor_m1_5333,
	RuntimeMethodHandle__ctor_m1_5334,
	RuntimeMethodHandle__ctor_m1_5335,
	RuntimeMethodHandle_get_Value_m1_5336,
	RuntimeMethodHandle_GetObjectData_m1_5337,
	RuntimeMethodHandle_Equals_m1_5338,
	RuntimeMethodHandle_GetHashCode_m1_5339,
	StringComparer__ctor_m1_5340,
	StringComparer__cctor_m1_5341,
	StringComparer_get_InvariantCultureIgnoreCase_m1_5342,
	StringComparer_get_OrdinalIgnoreCase_m1_5343,
	StringComparer_Compare_m1_5344,
	StringComparer_Equals_m1_5345,
	StringComparer_GetHashCode_m1_5346,
	CultureAwareComparer__ctor_m1_5347,
	CultureAwareComparer_Compare_m1_5348,
	CultureAwareComparer_Equals_m1_5349,
	CultureAwareComparer_GetHashCode_m1_5350,
	OrdinalComparer__ctor_m1_5351,
	OrdinalComparer_Compare_m1_5352,
	OrdinalComparer_Equals_m1_5353,
	OrdinalComparer_GetHashCode_m1_5354,
	SystemException__ctor_m1_5355,
	SystemException__ctor_m1_5356,
	SystemException__ctor_m1_5357,
	SystemException__ctor_m1_5358,
	ThreadStaticAttribute__ctor_m1_5359,
	TimeSpan__ctor_m1_5360,
	TimeSpan__ctor_m1_5361,
	TimeSpan__ctor_m1_5362,
	TimeSpan__cctor_m1_5363,
	TimeSpan_CalculateTicks_m1_5364,
	TimeSpan_get_Days_m1_5365,
	TimeSpan_get_Hours_m1_5366,
	TimeSpan_get_Milliseconds_m1_5367,
	TimeSpan_get_Minutes_m1_5368,
	TimeSpan_get_Seconds_m1_5369,
	TimeSpan_get_Ticks_m1_5370,
	TimeSpan_get_TotalDays_m1_5371,
	TimeSpan_get_TotalHours_m1_5372,
	TimeSpan_get_TotalMilliseconds_m1_5373,
	TimeSpan_get_TotalMinutes_m1_5374,
	TimeSpan_get_TotalSeconds_m1_5375,
	TimeSpan_Add_m1_5376,
	TimeSpan_Compare_m1_5377,
	TimeSpan_CompareTo_m1_5378,
	TimeSpan_CompareTo_m1_5379,
	TimeSpan_Equals_m1_5380,
	TimeSpan_Duration_m1_5381,
	TimeSpan_Equals_m1_5382,
	TimeSpan_FromDays_m1_5383,
	TimeSpan_FromHours_m1_5384,
	TimeSpan_FromMinutes_m1_5385,
	TimeSpan_FromSeconds_m1_5386,
	TimeSpan_FromMilliseconds_m1_5387,
	TimeSpan_From_m1_5388,
	TimeSpan_FromTicks_m1_5389,
	TimeSpan_GetHashCode_m1_5390,
	TimeSpan_Negate_m1_5391,
	TimeSpan_Subtract_m1_5392,
	TimeSpan_ToString_m1_5393,
	TimeSpan_op_Addition_m1_5394,
	TimeSpan_op_Equality_m1_5395,
	TimeSpan_op_GreaterThan_m1_5396,
	TimeSpan_op_GreaterThanOrEqual_m1_5397,
	TimeSpan_op_Inequality_m1_5398,
	TimeSpan_op_LessThan_m1_5399,
	TimeSpan_op_LessThanOrEqual_m1_5400,
	TimeSpan_op_Subtraction_m1_5401,
	TimeZone__ctor_m1_5402,
	TimeZone__cctor_m1_5403,
	TimeZone_get_CurrentTimeZone_m1_5404,
	TimeZone_IsDaylightSavingTime_m1_5405,
	TimeZone_IsDaylightSavingTime_m1_5406,
	TimeZone_ToLocalTime_m1_5407,
	TimeZone_ToUniversalTime_m1_5408,
	TimeZone_GetLocalTimeDiff_m1_5409,
	TimeZone_GetLocalTimeDiff_m1_5410,
	CurrentSystemTimeZone__ctor_m1_5411,
	CurrentSystemTimeZone__ctor_m1_5412,
	CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_5413,
	CurrentSystemTimeZone_GetTimeZoneData_m1_5414,
	CurrentSystemTimeZone_GetDaylightChanges_m1_5415,
	CurrentSystemTimeZone_GetUtcOffset_m1_5416,
	CurrentSystemTimeZone_OnDeserialization_m1_5417,
	CurrentSystemTimeZone_GetDaylightTimeFromData_m1_5418,
	TypeInitializationException__ctor_m1_5419,
	TypeInitializationException_GetObjectData_m1_5420,
	TypeLoadException__ctor_m1_5421,
	TypeLoadException__ctor_m1_5422,
	TypeLoadException__ctor_m1_5423,
	TypeLoadException_get_Message_m1_5424,
	TypeLoadException_GetObjectData_m1_5425,
	UnauthorizedAccessException__ctor_m1_5426,
	UnauthorizedAccessException__ctor_m1_5427,
	UnauthorizedAccessException__ctor_m1_5428,
	UnhandledExceptionEventArgs__ctor_m1_5429,
	UnhandledExceptionEventArgs_get_ExceptionObject_m1_5430,
	UnhandledExceptionEventArgs_get_IsTerminating_m1_5431,
	UnitySerializationHolder__ctor_m1_5432,
	UnitySerializationHolder_GetTypeData_m1_5433,
	UnitySerializationHolder_GetDBNullData_m1_5434,
	UnitySerializationHolder_GetModuleData_m1_5435,
	UnitySerializationHolder_GetObjectData_m1_5436,
	UnitySerializationHolder_GetRealObject_m1_5437,
	Version__ctor_m1_5438,
	Version__ctor_m1_5439,
	Version__ctor_m1_5440,
	Version__ctor_m1_5441,
	Version__ctor_m1_5442,
	Version_CheckedSet_m1_5443,
	Version_get_Build_m1_5444,
	Version_get_Major_m1_5445,
	Version_get_Minor_m1_5446,
	Version_get_Revision_m1_5447,
	Version_Clone_m1_5448,
	Version_CompareTo_m1_5449,
	Version_Equals_m1_5450,
	Version_CompareTo_m1_5451,
	Version_Equals_m1_5452,
	Version_GetHashCode_m1_5453,
	Version_ToString_m1_5454,
	Version_CreateFromString_m1_5455,
	Version_op_Equality_m1_5456,
	Version_op_Inequality_m1_5457,
	WeakReference__ctor_m1_5458,
	WeakReference__ctor_m1_5459,
	WeakReference__ctor_m1_5460,
	WeakReference__ctor_m1_5461,
	WeakReference_AllocateHandle_m1_5462,
	WeakReference_get_Target_m1_5463,
	WeakReference_get_TrackResurrection_m1_5464,
	WeakReference_Finalize_m1_5465,
	WeakReference_GetObjectData_m1_5466,
	PrimalityTest__ctor_m1_5467,
	PrimalityTest_Invoke_m1_5468,
	PrimalityTest_BeginInvoke_m1_5469,
	PrimalityTest_EndInvoke_m1_5470,
	MemberFilter__ctor_m1_5471,
	MemberFilter_Invoke_m1_5472,
	MemberFilter_BeginInvoke_m1_5473,
	MemberFilter_EndInvoke_m1_5474,
	TypeFilter__ctor_m1_5475,
	TypeFilter_Invoke_m1_5476,
	TypeFilter_BeginInvoke_m1_5477,
	TypeFilter_EndInvoke_m1_5478,
	CrossContextDelegate__ctor_m1_5479,
	CrossContextDelegate_Invoke_m1_5480,
	CrossContextDelegate_BeginInvoke_m1_5481,
	CrossContextDelegate_EndInvoke_m1_5482,
	HeaderHandler__ctor_m1_5483,
	HeaderHandler_Invoke_m1_5484,
	HeaderHandler_BeginInvoke_m1_5485,
	HeaderHandler_EndInvoke_m1_5486,
	ThreadStart__ctor_m1_5487,
	ThreadStart_Invoke_m1_5488,
	ThreadStart_BeginInvoke_m1_5489,
	ThreadStart_EndInvoke_m1_5490,
	TimerCallback__ctor_m1_5491,
	TimerCallback_Invoke_m1_5492,
	TimerCallback_BeginInvoke_m1_5493,
	TimerCallback_EndInvoke_m1_5494,
	WaitCallback__ctor_m1_5495,
	WaitCallback_Invoke_m1_5496,
	WaitCallback_BeginInvoke_m1_5497,
	WaitCallback_EndInvoke_m1_5498,
	AppDomainInitializer__ctor_m1_5499,
	AppDomainInitializer_Invoke_m1_5500,
	AppDomainInitializer_BeginInvoke_m1_5501,
	AppDomainInitializer_EndInvoke_m1_5502,
	AssemblyLoadEventHandler__ctor_m1_5503,
	AssemblyLoadEventHandler_Invoke_m1_5504,
	AssemblyLoadEventHandler_BeginInvoke_m1_5505,
	AssemblyLoadEventHandler_EndInvoke_m1_5506,
	EventHandler__ctor_m1_5507,
	EventHandler_Invoke_m1_5508,
	EventHandler_BeginInvoke_m1_5509,
	EventHandler_EndInvoke_m1_5510,
	ResolveEventHandler__ctor_m1_5511,
	ResolveEventHandler_Invoke_m1_5512,
	ResolveEventHandler_BeginInvoke_m1_5513,
	ResolveEventHandler_EndInvoke_m1_5514,
	UnhandledExceptionEventHandler__ctor_m1_5515,
	UnhandledExceptionEventHandler_Invoke_m1_5516,
	UnhandledExceptionEventHandler_BeginInvoke_m1_5517,
	UnhandledExceptionEventHandler_EndInvoke_m1_5518,
	ExtensionAttribute__ctor_m2_0,
	Locale_GetText_m2_1,
	Locale_GetText_m2_2,
	MonoTODOAttribute__ctor_m2_3,
	KeyBuilder_get_Rng_m2_4,
	KeyBuilder_Key_m2_5,
	KeyBuilder_IV_m2_6,
	SymmetricTransform__ctor_m2_7,
	SymmetricTransform_System_IDisposable_Dispose_m2_8,
	SymmetricTransform_Finalize_m2_9,
	SymmetricTransform_Dispose_m2_10,
	SymmetricTransform_get_CanReuseTransform_m2_11,
	SymmetricTransform_Transform_m2_12,
	SymmetricTransform_CBC_m2_13,
	SymmetricTransform_CFB_m2_14,
	SymmetricTransform_OFB_m2_15,
	SymmetricTransform_CTS_m2_16,
	SymmetricTransform_CheckInput_m2_17,
	SymmetricTransform_TransformBlock_m2_18,
	SymmetricTransform_get_KeepLastBlock_m2_19,
	SymmetricTransform_InternalTransformBlock_m2_20,
	SymmetricTransform_Random_m2_21,
	SymmetricTransform_ThrowBadPaddingException_m2_22,
	SymmetricTransform_FinalEncrypt_m2_23,
	SymmetricTransform_FinalDecrypt_m2_24,
	SymmetricTransform_TransformFinalBlock_m2_25,
	Aes__ctor_m2_26,
	AesManaged__ctor_m2_27,
	AesManaged_GenerateIV_m2_28,
	AesManaged_GenerateKey_m2_29,
	AesManaged_CreateDecryptor_m2_30,
	AesManaged_CreateEncryptor_m2_31,
	AesManaged_get_IV_m2_32,
	AesManaged_set_IV_m2_33,
	AesManaged_get_Key_m2_34,
	AesManaged_set_Key_m2_35,
	AesManaged_get_KeySize_m2_36,
	AesManaged_set_KeySize_m2_37,
	AesManaged_CreateDecryptor_m2_38,
	AesManaged_CreateEncryptor_m2_39,
	AesManaged_Dispose_m2_40,
	AesTransform__ctor_m2_41,
	AesTransform__cctor_m2_42,
	AesTransform_ECB_m2_43,
	AesTransform_SubByte_m2_44,
	AesTransform_Encrypt128_m2_45,
	AesTransform_Decrypt128_m2_46,
	Locale_GetText_m3_0,
	Locale_GetText_m3_1,
	MonoTODOAttribute__ctor_m3_2,
	MonoTODOAttribute__ctor_m3_3,
	HybridDictionary__ctor_m3_4,
	HybridDictionary__ctor_m3_5,
	HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m3_6,
	HybridDictionary_get_inner_m3_7,
	HybridDictionary_get_Count_m3_8,
	HybridDictionary_get_Item_m3_9,
	HybridDictionary_set_Item_m3_10,
	HybridDictionary_get_Keys_m3_11,
	HybridDictionary_get_SyncRoot_m3_12,
	HybridDictionary_Add_m3_13,
	HybridDictionary_Contains_m3_14,
	HybridDictionary_CopyTo_m3_15,
	HybridDictionary_GetEnumerator_m3_16,
	HybridDictionary_Remove_m3_17,
	HybridDictionary_Switch_m3_18,
	DictionaryNode__ctor_m3_19,
	DictionaryNodeEnumerator__ctor_m3_20,
	DictionaryNodeEnumerator_FailFast_m3_21,
	DictionaryNodeEnumerator_MoveNext_m3_22,
	DictionaryNodeEnumerator_Reset_m3_23,
	DictionaryNodeEnumerator_get_Current_m3_24,
	DictionaryNodeEnumerator_get_DictionaryNode_m3_25,
	DictionaryNodeEnumerator_get_Entry_m3_26,
	DictionaryNodeEnumerator_get_Key_m3_27,
	DictionaryNodeEnumerator_get_Value_m3_28,
	DictionaryNodeCollectionEnumerator__ctor_m3_29,
	DictionaryNodeCollectionEnumerator_get_Current_m3_30,
	DictionaryNodeCollectionEnumerator_MoveNext_m3_31,
	DictionaryNodeCollectionEnumerator_Reset_m3_32,
	DictionaryNodeCollection__ctor_m3_33,
	DictionaryNodeCollection_get_Count_m3_34,
	DictionaryNodeCollection_get_SyncRoot_m3_35,
	DictionaryNodeCollection_CopyTo_m3_36,
	DictionaryNodeCollection_GetEnumerator_m3_37,
	ListDictionary__ctor_m3_38,
	ListDictionary__ctor_m3_39,
	ListDictionary_System_Collections_IEnumerable_GetEnumerator_m3_40,
	ListDictionary_FindEntry_m3_41,
	ListDictionary_FindEntry_m3_42,
	ListDictionary_AddImpl_m3_43,
	ListDictionary_get_Count_m3_44,
	ListDictionary_get_SyncRoot_m3_45,
	ListDictionary_CopyTo_m3_46,
	ListDictionary_get_Item_m3_47,
	ListDictionary_set_Item_m3_48,
	ListDictionary_get_Keys_m3_49,
	ListDictionary_Add_m3_50,
	ListDictionary_Clear_m3_51,
	ListDictionary_Contains_m3_52,
	ListDictionary_GetEnumerator_m3_53,
	ListDictionary_Remove_m3_54,
	_Item__ctor_m3_55,
	_KeysEnumerator__ctor_m3_56,
	_KeysEnumerator_get_Current_m3_57,
	_KeysEnumerator_MoveNext_m3_58,
	_KeysEnumerator_Reset_m3_59,
	KeysCollection__ctor_m3_60,
	KeysCollection_System_Collections_ICollection_CopyTo_m3_61,
	KeysCollection_System_Collections_ICollection_get_SyncRoot_m3_62,
	KeysCollection_get_Count_m3_63,
	KeysCollection_GetEnumerator_m3_64,
	NameObjectCollectionBase__ctor_m3_65,
	NameObjectCollectionBase__ctor_m3_66,
	NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m3_67,
	NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m3_68,
	NameObjectCollectionBase_Init_m3_69,
	NameObjectCollectionBase_get_Keys_m3_70,
	NameObjectCollectionBase_GetEnumerator_m3_71,
	NameObjectCollectionBase_GetObjectData_m3_72,
	NameObjectCollectionBase_get_Count_m3_73,
	NameObjectCollectionBase_OnDeserialization_m3_74,
	NameObjectCollectionBase_get_IsReadOnly_m3_75,
	NameObjectCollectionBase_BaseAdd_m3_76,
	NameObjectCollectionBase_BaseGet_m3_77,
	NameObjectCollectionBase_BaseGet_m3_78,
	NameObjectCollectionBase_BaseGetKey_m3_79,
	NameObjectCollectionBase_FindFirstMatchedItem_m3_80,
	NameValueCollection__ctor_m3_81,
	NameValueCollection__ctor_m3_82,
	NameValueCollection_Add_m3_83,
	NameValueCollection_Get_m3_84,
	NameValueCollection_AsSingleString_m3_85,
	NameValueCollection_GetKey_m3_86,
	NameValueCollection_InvalidateCachedArrays_m3_87,
	EditorBrowsableAttribute__ctor_m3_88,
	EditorBrowsableAttribute_get_State_m3_89,
	EditorBrowsableAttribute_Equals_m3_90,
	EditorBrowsableAttribute_GetHashCode_m3_91,
	TypeConverterAttribute__ctor_m3_92,
	TypeConverterAttribute__ctor_m3_93,
	TypeConverterAttribute__cctor_m3_94,
	TypeConverterAttribute_Equals_m3_95,
	TypeConverterAttribute_GetHashCode_m3_96,
	TypeConverterAttribute_get_ConverterTypeName_m3_97,
	Win32Exception__ctor_m3_98,
	Win32Exception__ctor_m3_99,
	Win32Exception__ctor_m3_100,
	Win32Exception__ctor_m3_101,
	Win32Exception_get_NativeErrorCode_m3_102,
	Win32Exception_GetObjectData_m3_103,
	Win32Exception_W32ErrorMessage_m3_104,
	Debug_WriteLine_m3_105,
	Stopwatch__ctor_m3_106,
	Stopwatch__cctor_m3_107,
	Stopwatch_GetTimestamp_m3_108,
	Stopwatch_get_Elapsed_m3_109,
	Stopwatch_get_ElapsedMilliseconds_m3_110,
	Stopwatch_get_ElapsedTicks_m3_111,
	Stopwatch_Reset_m3_112,
	Stopwatch_Start_m3_113,
	Stopwatch_Stop_m3_114,
	LingerOption__ctor_m3_115,
	Socket__ctor_m3_116,
	Socket__cctor_m3_117,
	Socket_Available_internal_m3_118,
	Socket_get_Available_m3_119,
	Socket_set_ReceiveTimeout_m3_120,
	Socket_Connect_m3_121,
	Socket_Connect_m3_122,
	Socket_Connect_m3_123,
	Socket_Poll_m3_124,
	Socket_Receive_m3_125,
	Socket_Receive_m3_126,
	Socket_Receive_m3_127,
	Socket_RecvFrom_internal_m3_128,
	Socket_ReceiveFrom_nochecks_exc_m3_129,
	Socket_Send_m3_130,
	Socket_Send_m3_131,
	Socket_CheckProtocolSupport_m3_132,
	Socket_get_SupportsIPv4_m3_133,
	Socket_get_SupportsIPv6_m3_134,
	Socket_Socket_internal_m3_135,
	Socket_Finalize_m3_136,
	Socket_get_AddressFamily_m3_137,
	Socket_get_Connected_m3_138,
	Socket_set_NoDelay_m3_139,
	Socket_Linger_m3_140,
	Socket_Dispose_m3_141,
	Socket_Dispose_m3_142,
	Socket_Close_internal_m3_143,
	Socket_Close_m3_144,
	Socket_Connect_internal_real_m3_145,
	Socket_Connect_internal_m3_146,
	Socket_Connect_internal_m3_147,
	Socket_CheckEndPoint_m3_148,
	Socket_GetUnityCrossDomainHelperMethod_m3_149,
	Socket_Connect_m3_150,
	Socket_Connect_m3_151,
	Socket_Poll_internal_m3_152,
	Socket_Receive_internal_m3_153,
	Socket_Receive_nochecks_m3_154,
	Socket_GetSocketOption_obj_internal_m3_155,
	Socket_Send_internal_m3_156,
	Socket_Send_nochecks_m3_157,
	Socket_GetSocketOption_m3_158,
	Socket_Shutdown_internal_m3_159,
	Socket_SetSocketOption_internal_m3_160,
	Socket_SetSocketOption_m3_161,
	Socket_ThrowIfUpd_m3_162,
	SocketException__ctor_m3_163,
	SocketException__ctor_m3_164,
	SocketException__ctor_m3_165,
	SocketException__ctor_m3_166,
	SocketException_WSAGetLastError_internal_m3_167,
	SocketException_get_SocketErrorCode_m3_168,
	SocketException_get_Message_m3_169,
	DefaultCertificatePolicy__ctor_m3_170,
	DefaultCertificatePolicy_CheckValidationResult_m3_171,
	Dns__cctor_m3_172,
	Dns_GetHostByName_internal_m3_173,
	Dns_GetHostByAddr_internal_m3_174,
	Dns_hostent_to_IPHostEntry_m3_175,
	Dns_GetHostByAddressFromString_m3_176,
	Dns_GetHostEntry_m3_177,
	Dns_GetHostEntry_m3_178,
	Dns_GetHostAddresses_m3_179,
	Dns_GetHostByName_m3_180,
	EndPoint__ctor_m3_181,
	EndPoint_get_AddressFamily_m3_182,
	EndPoint_Create_m3_183,
	EndPoint_Serialize_m3_184,
	EndPoint_NotImplemented_m3_185,
	FileWebRequest__ctor_m3_186,
	FileWebRequest__ctor_m3_187,
	FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m3_188,
	FileWebRequest_GetObjectData_m3_189,
	FileWebRequestCreator__ctor_m3_190,
	FileWebRequestCreator_Create_m3_191,
	FtpRequestCreator__ctor_m3_192,
	FtpRequestCreator_Create_m3_193,
	FtpWebRequest__ctor_m3_194,
	FtpWebRequest__cctor_m3_195,
	FtpWebRequest_U3CcallbackU3Em__B_m3_196,
	GlobalProxySelection_get_Select_m3_197,
	HttpRequestCreator__ctor_m3_198,
	HttpRequestCreator_Create_m3_199,
	HttpVersion__cctor_m3_200,
	HttpWebRequest__ctor_m3_201,
	HttpWebRequest__ctor_m3_202,
	HttpWebRequest__cctor_m3_203,
	HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m3_204,
	HttpWebRequest_get_Address_m3_205,
	HttpWebRequest_get_ServicePoint_m3_206,
	HttpWebRequest_GetServicePoint_m3_207,
	HttpWebRequest_GetObjectData_m3_208,
	IPAddress__ctor_m3_209,
	IPAddress__ctor_m3_210,
	IPAddress__cctor_m3_211,
	IPAddress_SwapShort_m3_212,
	IPAddress_HostToNetworkOrder_m3_213,
	IPAddress_NetworkToHostOrder_m3_214,
	IPAddress_Parse_m3_215,
	IPAddress_TryParse_m3_216,
	IPAddress_ParseIPV4_m3_217,
	IPAddress_ParseIPV6_m3_218,
	IPAddress_get_InternalIPv4Address_m3_219,
	IPAddress_get_ScopeId_m3_220,
	IPAddress_GetAddressBytes_m3_221,
	IPAddress_get_AddressFamily_m3_222,
	IPAddress_IsLoopback_m3_223,
	IPAddress_ToString_m3_224,
	IPAddress_ToString_m3_225,
	IPAddress_Equals_m3_226,
	IPAddress_GetHashCode_m3_227,
	IPAddress_Hash_m3_228,
	IPEndPoint__ctor_m3_229,
	IPEndPoint__ctor_m3_230,
	IPEndPoint_get_Address_m3_231,
	IPEndPoint_set_Address_m3_232,
	IPEndPoint_get_AddressFamily_m3_233,
	IPEndPoint_get_Port_m3_234,
	IPEndPoint_set_Port_m3_235,
	IPEndPoint_Create_m3_236,
	IPEndPoint_Serialize_m3_237,
	IPEndPoint_ToString_m3_238,
	IPEndPoint_Equals_m3_239,
	IPEndPoint_GetHashCode_m3_240,
	IPHostEntry__ctor_m3_241,
	IPHostEntry_get_AddressList_m3_242,
	IPHostEntry_set_AddressList_m3_243,
	IPHostEntry_set_Aliases_m3_244,
	IPHostEntry_set_HostName_m3_245,
	IPv6Address__ctor_m3_246,
	IPv6Address__ctor_m3_247,
	IPv6Address__ctor_m3_248,
	IPv6Address__cctor_m3_249,
	IPv6Address_Parse_m3_250,
	IPv6Address_Fill_m3_251,
	IPv6Address_TryParse_m3_252,
	IPv6Address_TryParse_m3_253,
	IPv6Address_get_Address_m3_254,
	IPv6Address_get_ScopeId_m3_255,
	IPv6Address_set_ScopeId_m3_256,
	IPv6Address_IsLoopback_m3_257,
	IPv6Address_SwapUShort_m3_258,
	IPv6Address_AsIPv4Int_m3_259,
	IPv6Address_IsIPv4Compatible_m3_260,
	IPv6Address_IsIPv4Mapped_m3_261,
	IPv6Address_ToString_m3_262,
	IPv6Address_ToString_m3_263,
	IPv6Address_Equals_m3_264,
	IPv6Address_GetHashCode_m3_265,
	IPv6Address_Hash_m3_266,
	ServicePoint__ctor_m3_267,
	ServicePoint_get_Address_m3_268,
	ServicePoint_get_CurrentConnections_m3_269,
	ServicePoint_get_IdleSince_m3_270,
	ServicePoint_set_IdleSince_m3_271,
	ServicePoint_set_Expect100Continue_m3_272,
	ServicePoint_set_UseNagleAlgorithm_m3_273,
	ServicePoint_set_SendContinue_m3_274,
	ServicePoint_set_UsesProxy_m3_275,
	ServicePoint_set_UseConnect_m3_276,
	ServicePoint_get_AvailableForRecycling_m3_277,
	SPKey__ctor_m3_278,
	SPKey_GetHashCode_m3_279,
	SPKey_Equals_m3_280,
	ServicePointManager__cctor_m3_281,
	ServicePointManager_get_CertificatePolicy_m3_282,
	ServicePointManager_get_CheckCertificateRevocationList_m3_283,
	ServicePointManager_get_SecurityProtocol_m3_284,
	ServicePointManager_get_ServerCertificateValidationCallback_m3_285,
	ServicePointManager_FindServicePoint_m3_286,
	ServicePointManager_RecycleServicePoints_m3_287,
	SocketAddress__ctor_m3_288,
	SocketAddress_get_Family_m3_289,
	SocketAddress_get_Size_m3_290,
	SocketAddress_get_Item_m3_291,
	SocketAddress_set_Item_m3_292,
	SocketAddress_ToString_m3_293,
	SocketAddress_Equals_m3_294,
	SocketAddress_GetHashCode_m3_295,
	WebHeaderCollection__ctor_m3_296,
	WebHeaderCollection__ctor_m3_297,
	WebHeaderCollection__ctor_m3_298,
	WebHeaderCollection__cctor_m3_299,
	WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m3_300,
	WebHeaderCollection_Add_m3_301,
	WebHeaderCollection_AddWithoutValidate_m3_302,
	WebHeaderCollection_IsRestricted_m3_303,
	WebHeaderCollection_OnDeserialization_m3_304,
	WebHeaderCollection_ToString_m3_305,
	WebHeaderCollection_GetObjectData_m3_306,
	WebHeaderCollection_get_Count_m3_307,
	WebHeaderCollection_get_Keys_m3_308,
	WebHeaderCollection_Get_m3_309,
	WebHeaderCollection_GetKey_m3_310,
	WebHeaderCollection_GetEnumerator_m3_311,
	WebHeaderCollection_IsHeaderValue_m3_312,
	WebHeaderCollection_IsHeaderName_m3_313,
	WebProxy__ctor_m3_314,
	WebProxy__ctor_m3_315,
	WebProxy__ctor_m3_316,
	WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m3_317,
	WebProxy_get_UseDefaultCredentials_m3_318,
	WebProxy_GetProxy_m3_319,
	WebProxy_IsBypassed_m3_320,
	WebProxy_GetObjectData_m3_321,
	WebProxy_CheckBypassList_m3_322,
	WebRequest__ctor_m3_323,
	WebRequest__ctor_m3_324,
	WebRequest__cctor_m3_325,
	WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m3_326,
	WebRequest_AddDynamicPrefix_m3_327,
	WebRequest_GetMustImplement_m3_328,
	WebRequest_get_DefaultWebProxy_m3_329,
	WebRequest_GetDefaultWebProxy_m3_330,
	WebRequest_GetObjectData_m3_331,
	WebRequest_AddPrefix_m3_332,
	PublicKey__ctor_m3_333,
	PublicKey_get_EncodedKeyValue_m3_334,
	PublicKey_get_EncodedParameters_m3_335,
	PublicKey_get_Key_m3_336,
	PublicKey_get_Oid_m3_337,
	PublicKey_GetUnsignedBigInteger_m3_338,
	PublicKey_DecodeDSA_m3_339,
	PublicKey_DecodeRSA_m3_340,
	X500DistinguishedName__ctor_m3_341,
	X500DistinguishedName_Decode_m3_342,
	X500DistinguishedName_GetSeparator_m3_343,
	X500DistinguishedName_DecodeRawData_m3_344,
	X500DistinguishedName_Canonize_m3_345,
	X500DistinguishedName_AreEqual_m3_346,
	X509BasicConstraintsExtension__ctor_m3_347,
	X509BasicConstraintsExtension__ctor_m3_348,
	X509BasicConstraintsExtension__ctor_m3_349,
	X509BasicConstraintsExtension_get_CertificateAuthority_m3_350,
	X509BasicConstraintsExtension_get_HasPathLengthConstraint_m3_351,
	X509BasicConstraintsExtension_get_PathLengthConstraint_m3_352,
	X509BasicConstraintsExtension_CopyFrom_m3_353,
	X509BasicConstraintsExtension_Decode_m3_354,
	X509BasicConstraintsExtension_Encode_m3_355,
	X509BasicConstraintsExtension_ToString_m3_356,
	X509Certificate2__ctor_m3_357,
	X509Certificate2__cctor_m3_358,
	X509Certificate2_get_Extensions_m3_359,
	X509Certificate2_get_IssuerName_m3_360,
	X509Certificate2_get_NotAfter_m3_361,
	X509Certificate2_get_NotBefore_m3_362,
	X509Certificate2_get_PrivateKey_m3_363,
	X509Certificate2_get_PublicKey_m3_364,
	X509Certificate2_get_SerialNumber_m3_365,
	X509Certificate2_get_SignatureAlgorithm_m3_366,
	X509Certificate2_get_SubjectName_m3_367,
	X509Certificate2_get_Thumbprint_m3_368,
	X509Certificate2_get_Version_m3_369,
	X509Certificate2_GetNameInfo_m3_370,
	X509Certificate2_Find_m3_371,
	X509Certificate2_GetValueAsString_m3_372,
	X509Certificate2_ImportPkcs12_m3_373,
	X509Certificate2_Import_m3_374,
	X509Certificate2_Reset_m3_375,
	X509Certificate2_ToString_m3_376,
	X509Certificate2_ToString_m3_377,
	X509Certificate2_AppendBuffer_m3_378,
	X509Certificate2_Verify_m3_379,
	X509Certificate2_get_MonoCertificate_m3_380,
	X509Certificate2Collection__ctor_m3_381,
	X509Certificate2Collection__ctor_m3_382,
	X509Certificate2Collection_get_Item_m3_383,
	X509Certificate2Collection_Add_m3_384,
	X509Certificate2Collection_AddRange_m3_385,
	X509Certificate2Collection_Contains_m3_386,
	X509Certificate2Collection_Find_m3_387,
	X509Certificate2Collection_GetEnumerator_m3_388,
	X509Certificate2Enumerator__ctor_m3_389,
	X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m3_390,
	X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m3_391,
	X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m3_392,
	X509Certificate2Enumerator_get_Current_m3_393,
	X509Certificate2Enumerator_MoveNext_m3_394,
	X509Certificate2Enumerator_Reset_m3_395,
	X509CertificateEnumerator__ctor_m3_396,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m3_397,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m3_398,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m3_399,
	X509CertificateEnumerator_get_Current_m3_400,
	X509CertificateEnumerator_MoveNext_m3_401,
	X509CertificateEnumerator_Reset_m3_402,
	X509CertificateCollection__ctor_m3_403,
	X509CertificateCollection__ctor_m3_404,
	X509CertificateCollection_get_Item_m3_405,
	X509CertificateCollection_AddRange_m3_406,
	X509CertificateCollection_GetEnumerator_m3_407,
	X509CertificateCollection_GetHashCode_m3_408,
	X509Chain__ctor_m3_409,
	X509Chain__ctor_m3_410,
	X509Chain__cctor_m3_411,
	X509Chain_get_ChainPolicy_m3_412,
	X509Chain_Build_m3_413,
	X509Chain_Reset_m3_414,
	X509Chain_get_Roots_m3_415,
	X509Chain_get_CertificateAuthorities_m3_416,
	X509Chain_get_CertificateCollection_m3_417,
	X509Chain_BuildChainFrom_m3_418,
	X509Chain_SelectBestFromCollection_m3_419,
	X509Chain_FindParent_m3_420,
	X509Chain_IsChainComplete_m3_421,
	X509Chain_IsSelfIssued_m3_422,
	X509Chain_ValidateChain_m3_423,
	X509Chain_Process_m3_424,
	X509Chain_PrepareForNextCertificate_m3_425,
	X509Chain_WrapUp_m3_426,
	X509Chain_ProcessCertificateExtensions_m3_427,
	X509Chain_IsSignedWith_m3_428,
	X509Chain_GetSubjectKeyIdentifier_m3_429,
	X509Chain_GetAuthorityKeyIdentifier_m3_430,
	X509Chain_GetAuthorityKeyIdentifier_m3_431,
	X509Chain_GetAuthorityKeyIdentifier_m3_432,
	X509Chain_CheckRevocationOnChain_m3_433,
	X509Chain_CheckRevocation_m3_434,
	X509Chain_CheckRevocation_m3_435,
	X509Chain_FindCrl_m3_436,
	X509Chain_ProcessCrlExtensions_m3_437,
	X509Chain_ProcessCrlEntryExtensions_m3_438,
	X509ChainElement__ctor_m3_439,
	X509ChainElement_get_Certificate_m3_440,
	X509ChainElement_get_ChainElementStatus_m3_441,
	X509ChainElement_get_StatusFlags_m3_442,
	X509ChainElement_set_StatusFlags_m3_443,
	X509ChainElement_Count_m3_444,
	X509ChainElement_Set_m3_445,
	X509ChainElement_UncompressFlags_m3_446,
	X509ChainElementCollection__ctor_m3_447,
	X509ChainElementCollection_System_Collections_ICollection_CopyTo_m3_448,
	X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m3_449,
	X509ChainElementCollection_get_Count_m3_450,
	X509ChainElementCollection_get_Item_m3_451,
	X509ChainElementCollection_get_SyncRoot_m3_452,
	X509ChainElementCollection_GetEnumerator_m3_453,
	X509ChainElementCollection_Add_m3_454,
	X509ChainElementCollection_Clear_m3_455,
	X509ChainElementCollection_Contains_m3_456,
	X509ChainElementEnumerator__ctor_m3_457,
	X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m3_458,
	X509ChainElementEnumerator_get_Current_m3_459,
	X509ChainElementEnumerator_MoveNext_m3_460,
	X509ChainElementEnumerator_Reset_m3_461,
	X509ChainPolicy__ctor_m3_462,
	X509ChainPolicy_get_ExtraStore_m3_463,
	X509ChainPolicy_get_RevocationFlag_m3_464,
	X509ChainPolicy_get_RevocationMode_m3_465,
	X509ChainPolicy_get_VerificationFlags_m3_466,
	X509ChainPolicy_get_VerificationTime_m3_467,
	X509ChainPolicy_Reset_m3_468,
	X509ChainStatus__ctor_m3_469,
	X509ChainStatus_get_Status_m3_470,
	X509ChainStatus_set_Status_m3_471,
	X509ChainStatus_set_StatusInformation_m3_472,
	X509ChainStatus_GetInformation_m3_473,
	X509EnhancedKeyUsageExtension__ctor_m3_474,
	X509EnhancedKeyUsageExtension_CopyFrom_m3_475,
	X509EnhancedKeyUsageExtension_Decode_m3_476,
	X509EnhancedKeyUsageExtension_ToString_m3_477,
	X509Extension__ctor_m3_478,
	X509Extension__ctor_m3_479,
	X509Extension_get_Critical_m3_480,
	X509Extension_set_Critical_m3_481,
	X509Extension_CopyFrom_m3_482,
	X509Extension_FormatUnkownData_m3_483,
	X509ExtensionCollection__ctor_m3_484,
	X509ExtensionCollection_System_Collections_ICollection_CopyTo_m3_485,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m3_486,
	X509ExtensionCollection_get_Count_m3_487,
	X509ExtensionCollection_get_SyncRoot_m3_488,
	X509ExtensionCollection_get_Item_m3_489,
	X509ExtensionCollection_GetEnumerator_m3_490,
	X509ExtensionEnumerator__ctor_m3_491,
	X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m3_492,
	X509ExtensionEnumerator_get_Current_m3_493,
	X509ExtensionEnumerator_MoveNext_m3_494,
	X509ExtensionEnumerator_Reset_m3_495,
	X509KeyUsageExtension__ctor_m3_496,
	X509KeyUsageExtension__ctor_m3_497,
	X509KeyUsageExtension__ctor_m3_498,
	X509KeyUsageExtension_get_KeyUsages_m3_499,
	X509KeyUsageExtension_CopyFrom_m3_500,
	X509KeyUsageExtension_GetValidFlags_m3_501,
	X509KeyUsageExtension_Decode_m3_502,
	X509KeyUsageExtension_Encode_m3_503,
	X509KeyUsageExtension_ToString_m3_504,
	X509Store__ctor_m3_505,
	X509Store_get_Certificates_m3_506,
	X509Store_get_Factory_m3_507,
	X509Store_get_Store_m3_508,
	X509Store_Close_m3_509,
	X509Store_Open_m3_510,
	X509SubjectKeyIdentifierExtension__ctor_m3_511,
	X509SubjectKeyIdentifierExtension__ctor_m3_512,
	X509SubjectKeyIdentifierExtension__ctor_m3_513,
	X509SubjectKeyIdentifierExtension__ctor_m3_514,
	X509SubjectKeyIdentifierExtension__ctor_m3_515,
	X509SubjectKeyIdentifierExtension__ctor_m3_516,
	X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m3_517,
	X509SubjectKeyIdentifierExtension_CopyFrom_m3_518,
	X509SubjectKeyIdentifierExtension_FromHexChar_m3_519,
	X509SubjectKeyIdentifierExtension_FromHexChars_m3_520,
	X509SubjectKeyIdentifierExtension_FromHex_m3_521,
	X509SubjectKeyIdentifierExtension_Decode_m3_522,
	X509SubjectKeyIdentifierExtension_Encode_m3_523,
	X509SubjectKeyIdentifierExtension_ToString_m3_524,
	AsnEncodedData__ctor_m3_525,
	AsnEncodedData__ctor_m3_526,
	AsnEncodedData__ctor_m3_527,
	AsnEncodedData_get_Oid_m3_528,
	AsnEncodedData_set_Oid_m3_529,
	AsnEncodedData_get_RawData_m3_530,
	AsnEncodedData_set_RawData_m3_531,
	AsnEncodedData_CopyFrom_m3_532,
	AsnEncodedData_ToString_m3_533,
	AsnEncodedData_Default_m3_534,
	AsnEncodedData_BasicConstraintsExtension_m3_535,
	AsnEncodedData_EnhancedKeyUsageExtension_m3_536,
	AsnEncodedData_KeyUsageExtension_m3_537,
	AsnEncodedData_SubjectKeyIdentifierExtension_m3_538,
	AsnEncodedData_SubjectAltName_m3_539,
	AsnEncodedData_NetscapeCertType_m3_540,
	Oid__ctor_m3_541,
	Oid__ctor_m3_542,
	Oid__ctor_m3_543,
	Oid__ctor_m3_544,
	Oid_get_FriendlyName_m3_545,
	Oid_get_Value_m3_546,
	Oid_GetName_m3_547,
	OidCollection__ctor_m3_548,
	OidCollection_System_Collections_ICollection_CopyTo_m3_549,
	OidCollection_System_Collections_IEnumerable_GetEnumerator_m3_550,
	OidCollection_get_Count_m3_551,
	OidCollection_get_Item_m3_552,
	OidCollection_get_SyncRoot_m3_553,
	OidCollection_Add_m3_554,
	OidEnumerator__ctor_m3_555,
	OidEnumerator_System_Collections_IEnumerator_get_Current_m3_556,
	OidEnumerator_MoveNext_m3_557,
	OidEnumerator_Reset_m3_558,
	MatchAppendEvaluator__ctor_m3_559,
	MatchAppendEvaluator_Invoke_m3_560,
	MatchAppendEvaluator_BeginInvoke_m3_561,
	MatchAppendEvaluator_EndInvoke_m3_562,
	BaseMachine__ctor_m3_563,
	BaseMachine_Replace_m3_564,
	BaseMachine_Scan_m3_565,
	BaseMachine_LTRReplace_m3_566,
	BaseMachine_RTLReplace_m3_567,
	Capture__ctor_m3_568,
	Capture__ctor_m3_569,
	Capture_get_Index_m3_570,
	Capture_get_Length_m3_571,
	Capture_get_Value_m3_572,
	Capture_ToString_m3_573,
	Capture_get_Text_m3_574,
	CaptureCollection__ctor_m3_575,
	CaptureCollection_get_Count_m3_576,
	CaptureCollection_SetValue_m3_577,
	CaptureCollection_get_SyncRoot_m3_578,
	CaptureCollection_CopyTo_m3_579,
	CaptureCollection_GetEnumerator_m3_580,
	Group__ctor_m3_581,
	Group__ctor_m3_582,
	Group__ctor_m3_583,
	Group__cctor_m3_584,
	Group_get_Captures_m3_585,
	Group_get_Success_m3_586,
	GroupCollection__ctor_m3_587,
	GroupCollection_get_Count_m3_588,
	GroupCollection_get_Item_m3_589,
	GroupCollection_SetValue_m3_590,
	GroupCollection_get_SyncRoot_m3_591,
	GroupCollection_CopyTo_m3_592,
	GroupCollection_GetEnumerator_m3_593,
	Match__ctor_m3_594,
	Match__ctor_m3_595,
	Match__ctor_m3_596,
	Match__cctor_m3_597,
	Match_get_Empty_m3_598,
	Match_get_Groups_m3_599,
	Match_NextMatch_m3_600,
	Match_get_Regex_m3_601,
	Enumerator__ctor_m3_602,
	Enumerator_System_Collections_IEnumerator_Reset_m3_603,
	Enumerator_System_Collections_IEnumerator_get_Current_m3_604,
	Enumerator_System_Collections_IEnumerator_MoveNext_m3_605,
	MatchCollection__ctor_m3_606,
	MatchCollection_get_Count_m3_607,
	MatchCollection_get_Item_m3_608,
	MatchCollection_get_SyncRoot_m3_609,
	MatchCollection_CopyTo_m3_610,
	MatchCollection_GetEnumerator_m3_611,
	MatchCollection_TryToGet_m3_612,
	MatchCollection_get_FullList_m3_613,
	Regex__ctor_m3_614,
	Regex__ctor_m3_615,
	Regex__ctor_m3_616,
	Regex__ctor_m3_617,
	Regex__cctor_m3_618,
	Regex_System_Runtime_Serialization_ISerializable_GetObjectData_m3_619,
	Regex_Replace_m3_620,
	Regex_Replace_m3_621,
	Regex_validate_options_m3_622,
	Regex_Init_m3_623,
	Regex_InitNewRegex_m3_624,
	Regex_CreateMachineFactory_m3_625,
	Regex_get_Options_m3_626,
	Regex_get_RightToLeft_m3_627,
	Regex_GroupNumberFromName_m3_628,
	Regex_GetGroupIndex_m3_629,
	Regex_default_startat_m3_630,
	Regex_IsMatch_m3_631,
	Regex_IsMatch_m3_632,
	Regex_Match_m3_633,
	Regex_Matches_m3_634,
	Regex_Matches_m3_635,
	Regex_Replace_m3_636,
	Regex_Replace_m3_637,
	Regex_ToString_m3_638,
	Regex_get_GroupCount_m3_639,
	Regex_get_Gap_m3_640,
	Regex_CreateMachine_m3_641,
	Regex_GetGroupNamesArray_m3_642,
	Regex_get_GroupNumbers_m3_643,
	Key__ctor_m3_644,
	Key_GetHashCode_m3_645,
	Key_Equals_m3_646,
	Key_ToString_m3_647,
	FactoryCache__ctor_m3_648,
	FactoryCache_Add_m3_649,
	FactoryCache_Cleanup_m3_650,
	FactoryCache_Lookup_m3_651,
	Node__ctor_m3_652,
	MRUList__ctor_m3_653,
	MRUList_Use_m3_654,
	MRUList_Evict_m3_655,
	CategoryUtils_CategoryFromName_m3_656,
	CategoryUtils_IsCategory_m3_657,
	CategoryUtils_IsCategory_m3_658,
	LinkRef__ctor_m3_659,
	InterpreterFactory__ctor_m3_660,
	InterpreterFactory_NewInstance_m3_661,
	InterpreterFactory_get_GroupCount_m3_662,
	InterpreterFactory_get_Gap_m3_663,
	InterpreterFactory_set_Gap_m3_664,
	InterpreterFactory_get_Mapping_m3_665,
	InterpreterFactory_set_Mapping_m3_666,
	InterpreterFactory_get_NamesMapping_m3_667,
	InterpreterFactory_set_NamesMapping_m3_668,
	PatternLinkStack__ctor_m3_669,
	PatternLinkStack_set_BaseAddress_m3_670,
	PatternLinkStack_get_OffsetAddress_m3_671,
	PatternLinkStack_set_OffsetAddress_m3_672,
	PatternLinkStack_GetOffset_m3_673,
	PatternLinkStack_GetCurrent_m3_674,
	PatternLinkStack_SetCurrent_m3_675,
	PatternCompiler__ctor_m3_676,
	PatternCompiler_EncodeOp_m3_677,
	PatternCompiler_GetMachineFactory_m3_678,
	PatternCompiler_EmitFalse_m3_679,
	PatternCompiler_EmitTrue_m3_680,
	PatternCompiler_EmitCount_m3_681,
	PatternCompiler_EmitCharacter_m3_682,
	PatternCompiler_EmitCategory_m3_683,
	PatternCompiler_EmitNotCategory_m3_684,
	PatternCompiler_EmitRange_m3_685,
	PatternCompiler_EmitSet_m3_686,
	PatternCompiler_EmitString_m3_687,
	PatternCompiler_EmitPosition_m3_688,
	PatternCompiler_EmitOpen_m3_689,
	PatternCompiler_EmitClose_m3_690,
	PatternCompiler_EmitBalanceStart_m3_691,
	PatternCompiler_EmitBalance_m3_692,
	PatternCompiler_EmitReference_m3_693,
	PatternCompiler_EmitIfDefined_m3_694,
	PatternCompiler_EmitSub_m3_695,
	PatternCompiler_EmitTest_m3_696,
	PatternCompiler_EmitBranch_m3_697,
	PatternCompiler_EmitJump_m3_698,
	PatternCompiler_EmitRepeat_m3_699,
	PatternCompiler_EmitUntil_m3_700,
	PatternCompiler_EmitFastRepeat_m3_701,
	PatternCompiler_EmitIn_m3_702,
	PatternCompiler_EmitAnchor_m3_703,
	PatternCompiler_EmitInfo_m3_704,
	PatternCompiler_NewLink_m3_705,
	PatternCompiler_ResolveLink_m3_706,
	PatternCompiler_EmitBranchEnd_m3_707,
	PatternCompiler_EmitAlternationEnd_m3_708,
	PatternCompiler_MakeFlags_m3_709,
	PatternCompiler_Emit_m3_710,
	PatternCompiler_Emit_m3_711,
	PatternCompiler_Emit_m3_712,
	PatternCompiler_get_CurrentAddress_m3_713,
	PatternCompiler_BeginLink_m3_714,
	PatternCompiler_EmitLink_m3_715,
	LinkStack__ctor_m3_716,
	LinkStack_Push_m3_717,
	LinkStack_Pop_m3_718,
	Mark_get_IsDefined_m3_719,
	Mark_get_Index_m3_720,
	Mark_get_Length_m3_721,
	IntStack_Pop_m3_722,
	IntStack_Push_m3_723,
	IntStack_get_Count_m3_724,
	IntStack_set_Count_m3_725,
	RepeatContext__ctor_m3_726,
	RepeatContext_get_Count_m3_727,
	RepeatContext_set_Count_m3_728,
	RepeatContext_get_Start_m3_729,
	RepeatContext_set_Start_m3_730,
	RepeatContext_get_IsMinimum_m3_731,
	RepeatContext_get_IsMaximum_m3_732,
	RepeatContext_get_IsLazy_m3_733,
	RepeatContext_get_Expression_m3_734,
	RepeatContext_get_Previous_m3_735,
	Interpreter__ctor_m3_736,
	Interpreter_ReadProgramCount_m3_737,
	Interpreter_Scan_m3_738,
	Interpreter_Reset_m3_739,
	Interpreter_Eval_m3_740,
	Interpreter_EvalChar_m3_741,
	Interpreter_TryMatch_m3_742,
	Interpreter_IsPosition_m3_743,
	Interpreter_IsWordChar_m3_744,
	Interpreter_GetString_m3_745,
	Interpreter_Open_m3_746,
	Interpreter_Close_m3_747,
	Interpreter_Balance_m3_748,
	Interpreter_Checkpoint_m3_749,
	Interpreter_Backtrack_m3_750,
	Interpreter_ResetGroups_m3_751,
	Interpreter_GetLastDefined_m3_752,
	Interpreter_CreateMark_m3_753,
	Interpreter_GetGroupInfo_m3_754,
	Interpreter_PopulateGroup_m3_755,
	Interpreter_GenerateMatch_m3_756,
	Interval__ctor_m3_757,
	Interval_get_Empty_m3_758,
	Interval_get_IsDiscontiguous_m3_759,
	Interval_get_IsSingleton_m3_760,
	Interval_get_IsEmpty_m3_761,
	Interval_get_Size_m3_762,
	Interval_IsDisjoint_m3_763,
	Interval_IsAdjacent_m3_764,
	Interval_Contains_m3_765,
	Interval_Contains_m3_766,
	Interval_Intersects_m3_767,
	Interval_Merge_m3_768,
	Interval_CompareTo_m3_769,
	Enumerator__ctor_m3_770,
	Enumerator_get_Current_m3_771,
	Enumerator_MoveNext_m3_772,
	Enumerator_Reset_m3_773,
	CostDelegate__ctor_m3_774,
	CostDelegate_Invoke_m3_775,
	CostDelegate_BeginInvoke_m3_776,
	CostDelegate_EndInvoke_m3_777,
	IntervalCollection__ctor_m3_778,
	IntervalCollection_get_Item_m3_779,
	IntervalCollection_Add_m3_780,
	IntervalCollection_Normalize_m3_781,
	IntervalCollection_GetMetaCollection_m3_782,
	IntervalCollection_Optimize_m3_783,
	IntervalCollection_get_Count_m3_784,
	IntervalCollection_get_SyncRoot_m3_785,
	IntervalCollection_CopyTo_m3_786,
	IntervalCollection_GetEnumerator_m3_787,
	Parser__ctor_m3_788,
	Parser_ParseDecimal_m3_789,
	Parser_ParseOctal_m3_790,
	Parser_ParseHex_m3_791,
	Parser_ParseNumber_m3_792,
	Parser_ParseName_m3_793,
	Parser_ParseRegularExpression_m3_794,
	Parser_GetMapping_m3_795,
	Parser_ParseGroup_m3_796,
	Parser_ParseGroupingConstruct_m3_797,
	Parser_ParseAssertionType_m3_798,
	Parser_ParseOptions_m3_799,
	Parser_ParseCharacterClass_m3_800,
	Parser_ParseRepetitionBounds_m3_801,
	Parser_ParseUnicodeCategory_m3_802,
	Parser_ParseSpecial_m3_803,
	Parser_ParseEscape_m3_804,
	Parser_ParseName_m3_805,
	Parser_IsNameChar_m3_806,
	Parser_ParseNumber_m3_807,
	Parser_ParseDigit_m3_808,
	Parser_ConsumeWhitespace_m3_809,
	Parser_ResolveReferences_m3_810,
	Parser_HandleExplicitNumericGroups_m3_811,
	Parser_IsIgnoreCase_m3_812,
	Parser_IsMultiline_m3_813,
	Parser_IsExplicitCapture_m3_814,
	Parser_IsSingleline_m3_815,
	Parser_IsIgnorePatternWhitespace_m3_816,
	Parser_IsECMAScript_m3_817,
	Parser_NewParseException_m3_818,
	QuickSearch__ctor_m3_819,
	QuickSearch__cctor_m3_820,
	QuickSearch_get_Length_m3_821,
	QuickSearch_Search_m3_822,
	QuickSearch_SetupShiftTable_m3_823,
	QuickSearch_GetShiftDistance_m3_824,
	QuickSearch_GetChar_m3_825,
	ReplacementEvaluator__ctor_m3_826,
	ReplacementEvaluator_Evaluate_m3_827,
	ReplacementEvaluator_EvaluateAppend_m3_828,
	ReplacementEvaluator_get_NeedsGroupsOrCaptures_m3_829,
	ReplacementEvaluator_Ensure_m3_830,
	ReplacementEvaluator_AddFromReplacement_m3_831,
	ReplacementEvaluator_AddInt_m3_832,
	ReplacementEvaluator_Compile_m3_833,
	ReplacementEvaluator_CompileTerm_m3_834,
	ExpressionCollection__ctor_m3_835,
	ExpressionCollection_Add_m3_836,
	ExpressionCollection_get_Item_m3_837,
	ExpressionCollection_set_Item_m3_838,
	ExpressionCollection_OnValidate_m3_839,
	Expression__ctor_m3_840,
	Expression_GetFixedWidth_m3_841,
	Expression_GetAnchorInfo_m3_842,
	CompositeExpression__ctor_m3_843,
	CompositeExpression_get_Expressions_m3_844,
	CompositeExpression_GetWidth_m3_845,
	CompositeExpression_IsComplex_m3_846,
	Group__ctor_m3_847,
	Group_AppendExpression_m3_848,
	Group_Compile_m3_849,
	Group_GetWidth_m3_850,
	Group_GetAnchorInfo_m3_851,
	RegularExpression__ctor_m3_852,
	RegularExpression_set_GroupCount_m3_853,
	RegularExpression_Compile_m3_854,
	CapturingGroup__ctor_m3_855,
	CapturingGroup_get_Index_m3_856,
	CapturingGroup_set_Index_m3_857,
	CapturingGroup_get_Name_m3_858,
	CapturingGroup_set_Name_m3_859,
	CapturingGroup_get_IsNamed_m3_860,
	CapturingGroup_Compile_m3_861,
	CapturingGroup_IsComplex_m3_862,
	CapturingGroup_CompareTo_m3_863,
	BalancingGroup__ctor_m3_864,
	BalancingGroup_set_Balance_m3_865,
	BalancingGroup_Compile_m3_866,
	NonBacktrackingGroup__ctor_m3_867,
	NonBacktrackingGroup_Compile_m3_868,
	NonBacktrackingGroup_IsComplex_m3_869,
	Repetition__ctor_m3_870,
	Repetition_get_Expression_m3_871,
	Repetition_set_Expression_m3_872,
	Repetition_get_Minimum_m3_873,
	Repetition_Compile_m3_874,
	Repetition_GetWidth_m3_875,
	Repetition_GetAnchorInfo_m3_876,
	Assertion__ctor_m3_877,
	Assertion_get_TrueExpression_m3_878,
	Assertion_set_TrueExpression_m3_879,
	Assertion_get_FalseExpression_m3_880,
	Assertion_set_FalseExpression_m3_881,
	Assertion_GetWidth_m3_882,
	CaptureAssertion__ctor_m3_883,
	CaptureAssertion_set_CapturingGroup_m3_884,
	CaptureAssertion_Compile_m3_885,
	CaptureAssertion_IsComplex_m3_886,
	CaptureAssertion_get_Alternate_m3_887,
	ExpressionAssertion__ctor_m3_888,
	ExpressionAssertion_set_Reverse_m3_889,
	ExpressionAssertion_set_Negate_m3_890,
	ExpressionAssertion_get_TestExpression_m3_891,
	ExpressionAssertion_set_TestExpression_m3_892,
	ExpressionAssertion_Compile_m3_893,
	ExpressionAssertion_IsComplex_m3_894,
	Alternation__ctor_m3_895,
	Alternation_get_Alternatives_m3_896,
	Alternation_AddAlternative_m3_897,
	Alternation_Compile_m3_898,
	Alternation_GetWidth_m3_899,
	Literal__ctor_m3_900,
	Literal_CompileLiteral_m3_901,
	Literal_Compile_m3_902,
	Literal_GetWidth_m3_903,
	Literal_GetAnchorInfo_m3_904,
	Literal_IsComplex_m3_905,
	PositionAssertion__ctor_m3_906,
	PositionAssertion_Compile_m3_907,
	PositionAssertion_GetWidth_m3_908,
	PositionAssertion_IsComplex_m3_909,
	PositionAssertion_GetAnchorInfo_m3_910,
	Reference__ctor_m3_911,
	Reference_get_CapturingGroup_m3_912,
	Reference_set_CapturingGroup_m3_913,
	Reference_get_IgnoreCase_m3_914,
	Reference_Compile_m3_915,
	Reference_GetWidth_m3_916,
	Reference_IsComplex_m3_917,
	BackslashNumber__ctor_m3_918,
	BackslashNumber_ResolveReference_m3_919,
	BackslashNumber_Compile_m3_920,
	CharacterClass__ctor_m3_921,
	CharacterClass__ctor_m3_922,
	CharacterClass__cctor_m3_923,
	CharacterClass_AddCategory_m3_924,
	CharacterClass_AddCharacter_m3_925,
	CharacterClass_AddRange_m3_926,
	CharacterClass_Compile_m3_927,
	CharacterClass_GetWidth_m3_928,
	CharacterClass_IsComplex_m3_929,
	CharacterClass_GetIntervalCost_m3_930,
	AnchorInfo__ctor_m3_931,
	AnchorInfo__ctor_m3_932,
	AnchorInfo__ctor_m3_933,
	AnchorInfo_get_Offset_m3_934,
	AnchorInfo_get_Width_m3_935,
	AnchorInfo_get_Length_m3_936,
	AnchorInfo_get_IsUnknownWidth_m3_937,
	AnchorInfo_get_IsComplete_m3_938,
	AnchorInfo_get_Substring_m3_939,
	AnchorInfo_get_IgnoreCase_m3_940,
	AnchorInfo_get_Position_m3_941,
	AnchorInfo_get_IsSubstring_m3_942,
	AnchorInfo_get_IsPosition_m3_943,
	AnchorInfo_GetInterval_m3_944,
	DefaultUriParser__ctor_m3_945,
	DefaultUriParser__ctor_m3_946,
	UriScheme__ctor_m3_947,
	Uri__ctor_m3_948,
	Uri__ctor_m3_949,
	Uri__ctor_m3_950,
	Uri__cctor_m3_951,
	Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m3_952,
	Uri_get_AbsoluteUri_m3_953,
	Uri_get_Authority_m3_954,
	Uri_get_Host_m3_955,
	Uri_get_IsFile_m3_956,
	Uri_get_IsLoopback_m3_957,
	Uri_get_IsUnc_m3_958,
	Uri_get_Scheme_m3_959,
	Uri_get_IsAbsoluteUri_m3_960,
	Uri_CheckHostName_m3_961,
	Uri_IsIPv4Address_m3_962,
	Uri_IsDomainAddress_m3_963,
	Uri_CheckSchemeName_m3_964,
	Uri_IsAlpha_m3_965,
	Uri_Equals_m3_966,
	Uri_InternalEquals_m3_967,
	Uri_GetHashCode_m3_968,
	Uri_GetLeftPart_m3_969,
	Uri_FromHex_m3_970,
	Uri_HexEscape_m3_971,
	Uri_IsHexDigit_m3_972,
	Uri_IsHexEncoding_m3_973,
	Uri_AppendQueryAndFragment_m3_974,
	Uri_ToString_m3_975,
	Uri_EscapeString_m3_976,
	Uri_EscapeString_m3_977,
	Uri_ParseUri_m3_978,
	Uri_Unescape_m3_979,
	Uri_Unescape_m3_980,
	Uri_ParseAsWindowsUNC_m3_981,
	Uri_ParseAsWindowsAbsoluteFilePath_m3_982,
	Uri_ParseAsUnixAbsoluteFilePath_m3_983,
	Uri_Parse_m3_984,
	Uri_ParseNoExceptions_m3_985,
	Uri_CompactEscaped_m3_986,
	Uri_Reduce_m3_987,
	Uri_HexUnescapeMultiByte_m3_988,
	Uri_GetSchemeDelimiter_m3_989,
	Uri_GetDefaultPort_m3_990,
	Uri_GetOpaqueWiseSchemeDelimiter_m3_991,
	Uri_IsPredefinedScheme_m3_992,
	Uri_get_Parser_m3_993,
	Uri_NeedToEscapeDataChar_m3_994,
	Uri_EscapeDataString_m3_995,
	Uri_EnsureAbsoluteUri_m3_996,
	Uri_op_Equality_m3_997,
	UriFormatException__ctor_m3_998,
	UriFormatException__ctor_m3_999,
	UriFormatException__ctor_m3_1000,
	UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m3_1001,
	UriParser__ctor_m3_1002,
	UriParser__cctor_m3_1003,
	UriParser_InitializeAndValidate_m3_1004,
	UriParser_OnRegister_m3_1005,
	UriParser_set_SchemeName_m3_1006,
	UriParser_get_DefaultPort_m3_1007,
	UriParser_set_DefaultPort_m3_1008,
	UriParser_CreateDefaults_m3_1009,
	UriParser_InternalRegister_m3_1010,
	UriParser_GetParser_m3_1011,
	RemoteCertificateValidationCallback__ctor_m3_1012,
	RemoteCertificateValidationCallback_Invoke_m3_1013,
	RemoteCertificateValidationCallback_BeginInvoke_m3_1014,
	RemoteCertificateValidationCallback_EndInvoke_m3_1015,
	MatchEvaluator__ctor_m3_1016,
	MatchEvaluator_Invoke_m3_1017,
	MatchEvaluator_BeginInvoke_m3_1018,
	MatchEvaluator_EndInvoke_m3_1019,
	Locale_GetText_m4_49,
	ModulusRing__ctor_m4_50,
	ModulusRing_BarrettReduction_m4_51,
	ModulusRing_Multiply_m4_52,
	ModulusRing_Difference_m4_53,
	ModulusRing_Pow_m4_54,
	ModulusRing_Pow_m4_55,
	Kernel_AddSameSign_m4_56,
	Kernel_Subtract_m4_57,
	Kernel_MinusEq_m4_58,
	Kernel_PlusEq_m4_59,
	Kernel_Compare_m4_60,
	Kernel_SingleByteDivideInPlace_m4_61,
	Kernel_DwordMod_m4_62,
	Kernel_DwordDivMod_m4_63,
	Kernel_multiByteDivide_m4_64,
	Kernel_LeftShift_m4_65,
	Kernel_RightShift_m4_66,
	Kernel_Multiply_m4_67,
	Kernel_MultiplyMod2p32pmod_m4_68,
	Kernel_modInverse_m4_69,
	Kernel_modInverse_m4_70,
	BigInteger__ctor_m4_71,
	BigInteger__ctor_m4_72,
	BigInteger__ctor_m4_73,
	BigInteger__ctor_m4_74,
	BigInteger__ctor_m4_75,
	BigInteger__cctor_m4_76,
	BigInteger_get_Rng_m4_77,
	BigInteger_GenerateRandom_m4_78,
	BigInteger_GenerateRandom_m4_79,
	BigInteger_BitCount_m4_80,
	BigInteger_TestBit_m4_81,
	BigInteger_SetBit_m4_82,
	BigInteger_SetBit_m4_83,
	BigInteger_LowestSetBit_m4_84,
	BigInteger_GetBytes_m4_85,
	BigInteger_ToString_m4_86,
	BigInteger_ToString_m4_87,
	BigInteger_Normalize_m4_88,
	BigInteger_Clear_m4_89,
	BigInteger_GetHashCode_m4_90,
	BigInteger_ToString_m4_91,
	BigInteger_Equals_m4_92,
	BigInteger_ModInverse_m4_93,
	BigInteger_ModPow_m4_94,
	BigInteger_GeneratePseudoPrime_m4_95,
	BigInteger_Incr2_m4_96,
	BigInteger_op_Implicit_m4_97,
	BigInteger_op_Implicit_m4_98,
	BigInteger_op_Addition_m4_99,
	BigInteger_op_Subtraction_m4_100,
	BigInteger_op_Modulus_m4_101,
	BigInteger_op_Modulus_m4_102,
	BigInteger_op_Division_m4_103,
	BigInteger_op_Multiply_m4_104,
	BigInteger_op_LeftShift_m4_105,
	BigInteger_op_RightShift_m4_106,
	BigInteger_op_Equality_m4_107,
	BigInteger_op_Inequality_m4_108,
	BigInteger_op_Equality_m4_109,
	BigInteger_op_Inequality_m4_110,
	BigInteger_op_GreaterThan_m4_111,
	BigInteger_op_LessThan_m4_112,
	BigInteger_op_GreaterThanOrEqual_m4_113,
	BigInteger_op_LessThanOrEqual_m4_114,
	PrimalityTests_GetSPPRounds_m4_115,
	PrimalityTests_RabinMillerTest_m4_116,
	PrimeGeneratorBase__ctor_m4_117,
	PrimeGeneratorBase_get_Confidence_m4_118,
	PrimeGeneratorBase_get_PrimalityTest_m4_119,
	PrimeGeneratorBase_get_TrialDivisionBounds_m4_120,
	SequentialSearchPrimeGeneratorBase__ctor_m4_121,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m4_122,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m4_123,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m4_124,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m4_125,
	ASN1__ctor_m4_9,
	ASN1__ctor_m4_10,
	ASN1__ctor_m4_2,
	ASN1_get_Count_m4_5,
	ASN1_get_Tag_m4_3,
	ASN1_get_Length_m4_17,
	ASN1_get_Value_m4_4,
	ASN1_set_Value_m4_126,
	ASN1_CompareArray_m4_127,
	ASN1_CompareValue_m4_16,
	ASN1_Add_m4_11,
	ASN1_GetBytes_m4_128,
	ASN1_Decode_m4_129,
	ASN1_DecodeTLV_m4_130,
	ASN1_get_Item_m4_6,
	ASN1_Element_m4_131,
	ASN1_ToString_m4_132,
	ASN1Convert_FromInt32_m4_12,
	ASN1Convert_FromOid_m4_133,
	ASN1Convert_ToInt32_m4_8,
	ASN1Convert_ToOid_m4_40,
	ASN1Convert_ToDateTime_m4_134,
	BitConverterLE_GetUIntBytes_m4_135,
	BitConverterLE_GetBytes_m4_136,
	ContentInfo__ctor_m4_137,
	ContentInfo__ctor_m4_138,
	ContentInfo__ctor_m4_139,
	ContentInfo__ctor_m4_140,
	ContentInfo_get_ASN1_m4_141,
	ContentInfo_get_Content_m4_142,
	ContentInfo_set_Content_m4_143,
	ContentInfo_get_ContentType_m4_144,
	ContentInfo_set_ContentType_m4_145,
	ContentInfo_GetASN1_m4_146,
	EncryptedData__ctor_m4_147,
	EncryptedData__ctor_m4_148,
	EncryptedData_get_EncryptionAlgorithm_m4_149,
	EncryptedData_get_EncryptedContent_m4_150,
	ARC4Managed__ctor_m4_151,
	ARC4Managed_Finalize_m4_152,
	ARC4Managed_Dispose_m4_153,
	ARC4Managed_get_Key_m4_154,
	ARC4Managed_set_Key_m4_155,
	ARC4Managed_get_CanReuseTransform_m4_156,
	ARC4Managed_CreateEncryptor_m4_157,
	ARC4Managed_CreateDecryptor_m4_158,
	ARC4Managed_GenerateIV_m4_159,
	ARC4Managed_GenerateKey_m4_160,
	ARC4Managed_KeySetup_m4_161,
	ARC4Managed_CheckInput_m4_162,
	ARC4Managed_TransformBlock_m4_163,
	ARC4Managed_InternalTransformBlock_m4_164,
	ARC4Managed_TransformFinalBlock_m4_165,
	CryptoConvert_ToHex_m4_48,
	KeyBuilder_get_Rng_m4_166,
	KeyBuilder_Key_m4_167,
	MD2__ctor_m4_168,
	MD2_Create_m4_169,
	MD2_Create_m4_170,
	MD2Managed__ctor_m4_171,
	MD2Managed__cctor_m4_172,
	MD2Managed_Padding_m4_173,
	MD2Managed_Initialize_m4_174,
	MD2Managed_HashCore_m4_175,
	MD2Managed_HashFinal_m4_176,
	MD2Managed_MD2Transform_m4_177,
	PKCS1__cctor_m4_178,
	PKCS1_Compare_m4_179,
	PKCS1_I2OSP_m4_180,
	PKCS1_OS2IP_m4_181,
	PKCS1_RSASP1_m4_182,
	PKCS1_RSAVP1_m4_183,
	PKCS1_Sign_v15_m4_184,
	PKCS1_Verify_v15_m4_185,
	PKCS1_Verify_v15_m4_186,
	PKCS1_Encode_v15_m4_187,
	PrivateKeyInfo__ctor_m4_188,
	PrivateKeyInfo__ctor_m4_189,
	PrivateKeyInfo_get_PrivateKey_m4_190,
	PrivateKeyInfo_Decode_m4_191,
	PrivateKeyInfo_RemoveLeadingZero_m4_192,
	PrivateKeyInfo_Normalize_m4_193,
	PrivateKeyInfo_DecodeRSA_m4_194,
	PrivateKeyInfo_DecodeDSA_m4_195,
	EncryptedPrivateKeyInfo__ctor_m4_196,
	EncryptedPrivateKeyInfo__ctor_m4_197,
	EncryptedPrivateKeyInfo_get_Algorithm_m4_198,
	EncryptedPrivateKeyInfo_get_EncryptedData_m4_199,
	EncryptedPrivateKeyInfo_get_Salt_m4_200,
	EncryptedPrivateKeyInfo_get_IterationCount_m4_201,
	EncryptedPrivateKeyInfo_Decode_m4_202,
	RC4__ctor_m4_203,
	RC4__cctor_m4_204,
	RC4_get_IV_m4_205,
	RC4_set_IV_m4_206,
	KeyGeneratedEventHandler__ctor_m4_207,
	KeyGeneratedEventHandler_Invoke_m4_208,
	KeyGeneratedEventHandler_BeginInvoke_m4_209,
	KeyGeneratedEventHandler_EndInvoke_m4_210,
	RSAManaged__ctor_m4_211,
	RSAManaged__ctor_m4_212,
	RSAManaged_Finalize_m4_213,
	RSAManaged_GenerateKeyPair_m4_214,
	RSAManaged_get_KeySize_m4_215,
	RSAManaged_get_PublicOnly_m4_0,
	RSAManaged_DecryptValue_m4_216,
	RSAManaged_EncryptValue_m4_217,
	RSAManaged_ExportParameters_m4_218,
	RSAManaged_ImportParameters_m4_219,
	RSAManaged_Dispose_m4_220,
	RSAManaged_ToXmlString_m4_221,
	RSAManaged_GetPaddedValue_m4_222,
	SafeBag__ctor_m4_223,
	SafeBag_get_BagOID_m4_224,
	SafeBag_get_ASN1_m4_225,
	DeriveBytes__ctor_m4_226,
	DeriveBytes__cctor_m4_227,
	DeriveBytes_set_HashName_m4_228,
	DeriveBytes_set_IterationCount_m4_229,
	DeriveBytes_set_Password_m4_230,
	DeriveBytes_set_Salt_m4_231,
	DeriveBytes_Adjust_m4_232,
	DeriveBytes_Derive_m4_233,
	DeriveBytes_DeriveKey_m4_234,
	DeriveBytes_DeriveIV_m4_235,
	DeriveBytes_DeriveMAC_m4_236,
	PKCS12__ctor_m4_237,
	PKCS12__ctor_m4_18,
	PKCS12__ctor_m4_19,
	PKCS12__cctor_m4_238,
	PKCS12_Decode_m4_239,
	PKCS12_Finalize_m4_240,
	PKCS12_set_Password_m4_241,
	PKCS12_get_IterationCount_m4_242,
	PKCS12_set_IterationCount_m4_243,
	PKCS12_get_Keys_m4_22,
	PKCS12_get_Certificates_m4_20,
	PKCS12_get_RNG_m4_244,
	PKCS12_Compare_m4_245,
	PKCS12_GetSymmetricAlgorithm_m4_246,
	PKCS12_Decrypt_m4_247,
	PKCS12_Decrypt_m4_248,
	PKCS12_Encrypt_m4_249,
	PKCS12_GetExistingParameters_m4_250,
	PKCS12_AddPrivateKey_m4_251,
	PKCS12_ReadSafeBag_m4_252,
	PKCS12_CertificateSafeBag_m4_253,
	PKCS12_MAC_m4_254,
	PKCS12_GetBytes_m4_255,
	PKCS12_EncryptedContentInfo_m4_256,
	PKCS12_AddCertificate_m4_257,
	PKCS12_AddCertificate_m4_258,
	PKCS12_RemoveCertificate_m4_259,
	PKCS12_RemoveCertificate_m4_260,
	PKCS12_Clone_m4_261,
	PKCS12_get_MaximumPasswordLength_m4_262,
	X501__cctor_m4_263,
	X501_ToString_m4_264,
	X501_ToString_m4_7,
	X501_AppendEntry_m4_265,
	X509Certificate__ctor_m4_24,
	X509Certificate__cctor_m4_266,
	X509Certificate_Parse_m4_267,
	X509Certificate_GetUnsignedBigInteger_m4_268,
	X509Certificate_get_DSA_m4_1,
	X509Certificate_set_DSA_m4_23,
	X509Certificate_get_Extensions_m4_26,
	X509Certificate_get_Hash_m4_269,
	X509Certificate_get_IssuerName_m4_270,
	X509Certificate_get_KeyAlgorithm_m4_271,
	X509Certificate_get_KeyAlgorithmParameters_m4_272,
	X509Certificate_set_KeyAlgorithmParameters_m4_273,
	X509Certificate_get_PublicKey_m4_274,
	X509Certificate_get_RSA_m4_275,
	X509Certificate_set_RSA_m4_276,
	X509Certificate_get_RawData_m4_277,
	X509Certificate_get_SerialNumber_m4_278,
	X509Certificate_get_Signature_m4_279,
	X509Certificate_get_SignatureAlgorithm_m4_280,
	X509Certificate_get_SubjectName_m4_281,
	X509Certificate_get_ValidFrom_m4_282,
	X509Certificate_get_ValidUntil_m4_283,
	X509Certificate_get_Version_m4_15,
	X509Certificate_get_IsCurrent_m4_284,
	X509Certificate_WasCurrent_m4_285,
	X509Certificate_VerifySignature_m4_286,
	X509Certificate_VerifySignature_m4_287,
	X509Certificate_VerifySignature_m4_25,
	X509Certificate_get_IsSelfSigned_m4_288,
	X509Certificate_GetIssuerName_m4_13,
	X509Certificate_GetSubjectName_m4_14,
	X509Certificate_GetObjectData_m4_289,
	X509Certificate_PEM_m4_290,
	X509CertificateEnumerator__ctor_m4_291,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m4_292,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m4_293,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m4_294,
	X509CertificateEnumerator_get_Current_m4_47,
	X509CertificateEnumerator_MoveNext_m4_295,
	X509CertificateEnumerator_Reset_m4_296,
	X509CertificateCollection__ctor_m4_297,
	X509CertificateCollection__ctor_m4_298,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m4_299,
	X509CertificateCollection_get_Item_m4_21,
	X509CertificateCollection_Add_m4_300,
	X509CertificateCollection_AddRange_m4_301,
	X509CertificateCollection_Contains_m4_302,
	X509CertificateCollection_GetEnumerator_m4_46,
	X509CertificateCollection_GetHashCode_m4_303,
	X509CertificateCollection_IndexOf_m4_304,
	X509CertificateCollection_Remove_m4_305,
	X509CertificateCollection_Compare_m4_306,
	X509Chain__ctor_m4_307,
	X509Chain__ctor_m4_308,
	X509Chain_get_Status_m4_309,
	X509Chain_get_TrustAnchors_m4_310,
	X509Chain_Build_m4_311,
	X509Chain_IsValid_m4_312,
	X509Chain_FindCertificateParent_m4_313,
	X509Chain_FindCertificateRoot_m4_314,
	X509Chain_IsTrusted_m4_315,
	X509Chain_IsParent_m4_316,
	X509CrlEntry__ctor_m4_317,
	X509CrlEntry_get_SerialNumber_m4_318,
	X509CrlEntry_get_RevocationDate_m4_33,
	X509CrlEntry_get_Extensions_m4_39,
	X509Crl__ctor_m4_319,
	X509Crl_Parse_m4_320,
	X509Crl_get_Extensions_m4_28,
	X509Crl_get_Hash_m4_321,
	X509Crl_get_IssuerName_m4_36,
	X509Crl_get_NextUpdate_m4_34,
	X509Crl_Compare_m4_322,
	X509Crl_GetCrlEntry_m4_32,
	X509Crl_GetCrlEntry_m4_323,
	X509Crl_GetHashName_m4_324,
	X509Crl_VerifySignature_m4_325,
	X509Crl_VerifySignature_m4_326,
	X509Crl_VerifySignature_m4_31,
	X509Extension__ctor_m4_327,
	X509Extension__ctor_m4_328,
	X509Extension_Decode_m4_329,
	X509Extension_Encode_m4_330,
	X509Extension_get_Oid_m4_38,
	X509Extension_get_Critical_m4_37,
	X509Extension_get_Value_m4_41,
	X509Extension_Equals_m4_331,
	X509Extension_GetHashCode_m4_332,
	X509Extension_WriteLine_m4_333,
	X509Extension_ToString_m4_334,
	X509ExtensionCollection__ctor_m4_335,
	X509ExtensionCollection__ctor_m4_336,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m4_337,
	X509ExtensionCollection_IndexOf_m4_338,
	X509ExtensionCollection_get_Item_m4_27,
	X509Store__ctor_m4_339,
	X509Store_get_Certificates_m4_45,
	X509Store_get_Crls_m4_35,
	X509Store_Load_m4_340,
	X509Store_LoadCertificate_m4_341,
	X509Store_LoadCrl_m4_342,
	X509Store_CheckStore_m4_343,
	X509Store_BuildCertificatesCollection_m4_344,
	X509Store_BuildCrlsCollection_m4_345,
	X509StoreManager_get_CurrentUser_m4_42,
	X509StoreManager_get_LocalMachine_m4_43,
	X509StoreManager_get_TrustedRootCertificates_m4_346,
	X509Stores__ctor_m4_347,
	X509Stores_get_TrustedRoot_m4_348,
	X509Stores_Open_m4_44,
	AuthorityKeyIdentifierExtension__ctor_m4_29,
	AuthorityKeyIdentifierExtension_Decode_m4_349,
	AuthorityKeyIdentifierExtension_get_Identifier_m4_30,
	AuthorityKeyIdentifierExtension_ToString_m4_350,
	BasicConstraintsExtension__ctor_m4_351,
	BasicConstraintsExtension_Decode_m4_352,
	BasicConstraintsExtension_Encode_m4_353,
	BasicConstraintsExtension_get_CertificateAuthority_m4_354,
	BasicConstraintsExtension_ToString_m4_355,
	ExtendedKeyUsageExtension__ctor_m4_356,
	ExtendedKeyUsageExtension_Decode_m4_357,
	ExtendedKeyUsageExtension_Encode_m4_358,
	ExtendedKeyUsageExtension_get_KeyPurpose_m4_359,
	ExtendedKeyUsageExtension_ToString_m4_360,
	GeneralNames__ctor_m4_361,
	GeneralNames_get_DNSNames_m4_362,
	GeneralNames_get_IPAddresses_m4_363,
	GeneralNames_ToString_m4_364,
	KeyUsageExtension__ctor_m4_365,
	KeyUsageExtension_Decode_m4_366,
	KeyUsageExtension_Encode_m4_367,
	KeyUsageExtension_Support_m4_368,
	KeyUsageExtension_ToString_m4_369,
	NetscapeCertTypeExtension__ctor_m4_370,
	NetscapeCertTypeExtension_Decode_m4_371,
	NetscapeCertTypeExtension_Support_m4_372,
	NetscapeCertTypeExtension_ToString_m4_373,
	SubjectAltNameExtension__ctor_m4_374,
	SubjectAltNameExtension_Decode_m4_375,
	SubjectAltNameExtension_get_DNSNames_m4_376,
	SubjectAltNameExtension_get_IPAddresses_m4_377,
	SubjectAltNameExtension_ToString_m4_378,
	HMAC__ctor_m4_379,
	HMAC_get_Key_m4_380,
	HMAC_set_Key_m4_381,
	HMAC_Initialize_m4_382,
	HMAC_HashFinal_m4_383,
	HMAC_HashCore_m4_384,
	HMAC_initializePad_m4_385,
	MD5SHA1__ctor_m4_386,
	MD5SHA1_Initialize_m4_387,
	MD5SHA1_HashFinal_m4_388,
	MD5SHA1_HashCore_m4_389,
	MD5SHA1_CreateSignature_m4_390,
	MD5SHA1_VerifySignature_m4_391,
	Alert__ctor_m4_392,
	Alert__ctor_m4_393,
	Alert_get_Level_m4_394,
	Alert_get_Description_m4_395,
	Alert_get_IsWarning_m4_396,
	Alert_get_IsCloseNotify_m4_397,
	Alert_inferAlertLevel_m4_398,
	Alert_GetAlertMessage_m4_399,
	CipherSuite__ctor_m4_400,
	CipherSuite__cctor_m4_401,
	CipherSuite_get_EncryptionCipher_m4_402,
	CipherSuite_get_DecryptionCipher_m4_403,
	CipherSuite_get_ClientHMAC_m4_404,
	CipherSuite_get_ServerHMAC_m4_405,
	CipherSuite_get_CipherAlgorithmType_m4_406,
	CipherSuite_get_HashAlgorithmName_m4_407,
	CipherSuite_get_HashAlgorithmType_m4_408,
	CipherSuite_get_HashSize_m4_409,
	CipherSuite_get_ExchangeAlgorithmType_m4_410,
	CipherSuite_get_CipherMode_m4_411,
	CipherSuite_get_Code_m4_412,
	CipherSuite_get_Name_m4_413,
	CipherSuite_get_IsExportable_m4_414,
	CipherSuite_get_KeyMaterialSize_m4_415,
	CipherSuite_get_KeyBlockSize_m4_416,
	CipherSuite_get_ExpandedKeyMaterialSize_m4_417,
	CipherSuite_get_EffectiveKeyBits_m4_418,
	CipherSuite_get_IvSize_m4_419,
	CipherSuite_get_Context_m4_420,
	CipherSuite_set_Context_m4_421,
	CipherSuite_Write_m4_422,
	CipherSuite_Write_m4_423,
	CipherSuite_InitializeCipher_m4_424,
	CipherSuite_EncryptRecord_m4_425,
	CipherSuite_DecryptRecord_m4_426,
	CipherSuite_CreatePremasterSecret_m4_427,
	CipherSuite_PRF_m4_428,
	CipherSuite_Expand_m4_429,
	CipherSuite_createEncryptionCipher_m4_430,
	CipherSuite_createDecryptionCipher_m4_431,
	CipherSuiteCollection__ctor_m4_432,
	CipherSuiteCollection_System_Collections_IList_get_Item_m4_433,
	CipherSuiteCollection_System_Collections_IList_set_Item_m4_434,
	CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m4_435,
	CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m4_436,
	CipherSuiteCollection_System_Collections_IList_Contains_m4_437,
	CipherSuiteCollection_System_Collections_IList_IndexOf_m4_438,
	CipherSuiteCollection_System_Collections_IList_Insert_m4_439,
	CipherSuiteCollection_System_Collections_IList_Remove_m4_440,
	CipherSuiteCollection_System_Collections_IList_RemoveAt_m4_441,
	CipherSuiteCollection_System_Collections_IList_Add_m4_442,
	CipherSuiteCollection_get_Item_m4_443,
	CipherSuiteCollection_get_Item_m4_444,
	CipherSuiteCollection_set_Item_m4_445,
	CipherSuiteCollection_get_Item_m4_446,
	CipherSuiteCollection_get_Count_m4_447,
	CipherSuiteCollection_CopyTo_m4_448,
	CipherSuiteCollection_Clear_m4_449,
	CipherSuiteCollection_IndexOf_m4_450,
	CipherSuiteCollection_IndexOf_m4_451,
	CipherSuiteCollection_Add_m4_452,
	CipherSuiteCollection_add_m4_453,
	CipherSuiteCollection_add_m4_454,
	CipherSuiteCollection_cultureAwareCompare_m4_455,
	CipherSuiteFactory_GetSupportedCiphers_m4_456,
	CipherSuiteFactory_GetTls1SupportedCiphers_m4_457,
	CipherSuiteFactory_GetSsl3SupportedCiphers_m4_458,
	ClientContext__ctor_m4_459,
	ClientContext_get_SslStream_m4_460,
	ClientContext_get_ClientHelloProtocol_m4_461,
	ClientContext_set_ClientHelloProtocol_m4_462,
	ClientContext_Clear_m4_463,
	ClientRecordProtocol__ctor_m4_464,
	ClientRecordProtocol_GetMessage_m4_465,
	ClientRecordProtocol_ProcessHandshakeMessage_m4_466,
	ClientRecordProtocol_createClientHandshakeMessage_m4_467,
	ClientRecordProtocol_createServerHandshakeMessage_m4_468,
	ClientSessionInfo__ctor_m4_469,
	ClientSessionInfo__cctor_m4_470,
	ClientSessionInfo_Finalize_m4_471,
	ClientSessionInfo_get_HostName_m4_472,
	ClientSessionInfo_get_Id_m4_473,
	ClientSessionInfo_get_Valid_m4_474,
	ClientSessionInfo_GetContext_m4_475,
	ClientSessionInfo_SetContext_m4_476,
	ClientSessionInfo_KeepAlive_m4_477,
	ClientSessionInfo_Dispose_m4_478,
	ClientSessionInfo_Dispose_m4_479,
	ClientSessionInfo_CheckDisposed_m4_480,
	ClientSessionCache__cctor_m4_481,
	ClientSessionCache_Add_m4_482,
	ClientSessionCache_FromHost_m4_483,
	ClientSessionCache_FromContext_m4_484,
	ClientSessionCache_SetContextInCache_m4_485,
	ClientSessionCache_SetContextFromCache_m4_486,
	Context__ctor_m4_487,
	Context_get_AbbreviatedHandshake_m4_488,
	Context_set_AbbreviatedHandshake_m4_489,
	Context_get_ProtocolNegotiated_m4_490,
	Context_set_ProtocolNegotiated_m4_491,
	Context_get_SecurityProtocol_m4_492,
	Context_set_SecurityProtocol_m4_493,
	Context_get_SecurityProtocolFlags_m4_494,
	Context_get_Protocol_m4_495,
	Context_get_SessionId_m4_496,
	Context_set_SessionId_m4_497,
	Context_get_CompressionMethod_m4_498,
	Context_set_CompressionMethod_m4_499,
	Context_get_ServerSettings_m4_500,
	Context_get_ClientSettings_m4_501,
	Context_get_LastHandshakeMsg_m4_502,
	Context_set_LastHandshakeMsg_m4_503,
	Context_get_HandshakeState_m4_504,
	Context_set_HandshakeState_m4_505,
	Context_get_ReceivedConnectionEnd_m4_506,
	Context_set_ReceivedConnectionEnd_m4_507,
	Context_get_SentConnectionEnd_m4_508,
	Context_set_SentConnectionEnd_m4_509,
	Context_get_SupportedCiphers_m4_510,
	Context_set_SupportedCiphers_m4_511,
	Context_get_HandshakeMessages_m4_512,
	Context_get_WriteSequenceNumber_m4_513,
	Context_set_WriteSequenceNumber_m4_514,
	Context_get_ReadSequenceNumber_m4_515,
	Context_set_ReadSequenceNumber_m4_516,
	Context_get_ClientRandom_m4_517,
	Context_set_ClientRandom_m4_518,
	Context_get_ServerRandom_m4_519,
	Context_set_ServerRandom_m4_520,
	Context_get_RandomCS_m4_521,
	Context_set_RandomCS_m4_522,
	Context_get_RandomSC_m4_523,
	Context_set_RandomSC_m4_524,
	Context_get_MasterSecret_m4_525,
	Context_set_MasterSecret_m4_526,
	Context_get_ClientWriteKey_m4_527,
	Context_set_ClientWriteKey_m4_528,
	Context_get_ServerWriteKey_m4_529,
	Context_set_ServerWriteKey_m4_530,
	Context_get_ClientWriteIV_m4_531,
	Context_set_ClientWriteIV_m4_532,
	Context_get_ServerWriteIV_m4_533,
	Context_set_ServerWriteIV_m4_534,
	Context_get_RecordProtocol_m4_535,
	Context_set_RecordProtocol_m4_536,
	Context_GetUnixTime_m4_537,
	Context_GetSecureRandomBytes_m4_538,
	Context_Clear_m4_539,
	Context_ClearKeyInfo_m4_540,
	Context_DecodeProtocolCode_m4_541,
	Context_ChangeProtocol_m4_542,
	Context_get_Current_m4_543,
	Context_get_Negotiating_m4_544,
	Context_get_Read_m4_545,
	Context_get_Write_m4_546,
	Context_StartSwitchingSecurityParameters_m4_547,
	Context_EndSwitchingSecurityParameters_m4_548,
	HttpsClientStream__ctor_m4_549,
	HttpsClientStream_get_TrustFailure_m4_550,
	HttpsClientStream_RaiseServerCertificateValidation_m4_551,
	HttpsClientStream_U3CHttpsClientStreamU3Em__0_m4_552,
	HttpsClientStream_U3CHttpsClientStreamU3Em__1_m4_553,
	ReceiveRecordAsyncResult__ctor_m4_554,
	ReceiveRecordAsyncResult_get_Record_m4_555,
	ReceiveRecordAsyncResult_get_ResultingBuffer_m4_556,
	ReceiveRecordAsyncResult_get_InitialBuffer_m4_557,
	ReceiveRecordAsyncResult_get_AsyncState_m4_558,
	ReceiveRecordAsyncResult_get_AsyncException_m4_559,
	ReceiveRecordAsyncResult_get_CompletedWithError_m4_560,
	ReceiveRecordAsyncResult_get_AsyncWaitHandle_m4_561,
	ReceiveRecordAsyncResult_get_IsCompleted_m4_562,
	ReceiveRecordAsyncResult_SetComplete_m4_563,
	ReceiveRecordAsyncResult_SetComplete_m4_564,
	ReceiveRecordAsyncResult_SetComplete_m4_565,
	SendRecordAsyncResult__ctor_m4_566,
	SendRecordAsyncResult_get_Message_m4_567,
	SendRecordAsyncResult_get_AsyncState_m4_568,
	SendRecordAsyncResult_get_AsyncException_m4_569,
	SendRecordAsyncResult_get_CompletedWithError_m4_570,
	SendRecordAsyncResult_get_AsyncWaitHandle_m4_571,
	SendRecordAsyncResult_get_IsCompleted_m4_572,
	SendRecordAsyncResult_SetComplete_m4_573,
	SendRecordAsyncResult_SetComplete_m4_574,
	RecordProtocol__ctor_m4_575,
	RecordProtocol__cctor_m4_576,
	RecordProtocol_get_Context_m4_577,
	RecordProtocol_SendRecord_m4_578,
	RecordProtocol_ProcessChangeCipherSpec_m4_579,
	RecordProtocol_GetMessage_m4_580,
	RecordProtocol_BeginReceiveRecord_m4_581,
	RecordProtocol_InternalReceiveRecordCallback_m4_582,
	RecordProtocol_EndReceiveRecord_m4_583,
	RecordProtocol_ReceiveRecord_m4_584,
	RecordProtocol_ReadRecordBuffer_m4_585,
	RecordProtocol_ReadClientHelloV2_m4_586,
	RecordProtocol_ReadStandardRecordBuffer_m4_587,
	RecordProtocol_ProcessAlert_m4_588,
	RecordProtocol_SendAlert_m4_589,
	RecordProtocol_SendAlert_m4_590,
	RecordProtocol_SendAlert_m4_591,
	RecordProtocol_SendChangeCipherSpec_m4_592,
	RecordProtocol_BeginSendRecord_m4_593,
	RecordProtocol_InternalSendRecordCallback_m4_594,
	RecordProtocol_BeginSendRecord_m4_595,
	RecordProtocol_EndSendRecord_m4_596,
	RecordProtocol_SendRecord_m4_597,
	RecordProtocol_EncodeRecord_m4_598,
	RecordProtocol_EncodeRecord_m4_599,
	RecordProtocol_encryptRecordFragment_m4_600,
	RecordProtocol_decryptRecordFragment_m4_601,
	RecordProtocol_Compare_m4_602,
	RecordProtocol_ProcessCipherSpecV2Buffer_m4_603,
	RecordProtocol_MapV2CipherCode_m4_604,
	RSASslSignatureDeformatter__ctor_m4_605,
	RSASslSignatureDeformatter_VerifySignature_m4_606,
	RSASslSignatureDeformatter_SetHashAlgorithm_m4_607,
	RSASslSignatureDeformatter_SetKey_m4_608,
	RSASslSignatureFormatter__ctor_m4_609,
	RSASslSignatureFormatter_CreateSignature_m4_610,
	RSASslSignatureFormatter_SetHashAlgorithm_m4_611,
	RSASslSignatureFormatter_SetKey_m4_612,
	SecurityParameters__ctor_m4_613,
	SecurityParameters_get_Cipher_m4_614,
	SecurityParameters_set_Cipher_m4_615,
	SecurityParameters_get_ClientWriteMAC_m4_616,
	SecurityParameters_set_ClientWriteMAC_m4_617,
	SecurityParameters_get_ServerWriteMAC_m4_618,
	SecurityParameters_set_ServerWriteMAC_m4_619,
	SecurityParameters_Clear_m4_620,
	ValidationResult_get_Trusted_m4_621,
	ValidationResult_get_ErrorCode_m4_622,
	SslClientStream__ctor_m4_623,
	SslClientStream__ctor_m4_624,
	SslClientStream__ctor_m4_625,
	SslClientStream__ctor_m4_626,
	SslClientStream__ctor_m4_627,
	SslClientStream_add_ServerCertValidation_m4_628,
	SslClientStream_remove_ServerCertValidation_m4_629,
	SslClientStream_add_ClientCertSelection_m4_630,
	SslClientStream_remove_ClientCertSelection_m4_631,
	SslClientStream_add_PrivateKeySelection_m4_632,
	SslClientStream_remove_PrivateKeySelection_m4_633,
	SslClientStream_add_ServerCertValidation2_m4_634,
	SslClientStream_remove_ServerCertValidation2_m4_635,
	SslClientStream_get_InputBuffer_m4_636,
	SslClientStream_get_ClientCertificates_m4_637,
	SslClientStream_get_SelectedClientCertificate_m4_638,
	SslClientStream_get_ServerCertValidationDelegate_m4_639,
	SslClientStream_set_ServerCertValidationDelegate_m4_640,
	SslClientStream_get_ClientCertSelectionDelegate_m4_641,
	SslClientStream_set_ClientCertSelectionDelegate_m4_642,
	SslClientStream_get_PrivateKeyCertSelectionDelegate_m4_643,
	SslClientStream_set_PrivateKeyCertSelectionDelegate_m4_644,
	SslClientStream_Finalize_m4_645,
	SslClientStream_Dispose_m4_646,
	SslClientStream_OnBeginNegotiateHandshake_m4_647,
	SslClientStream_SafeReceiveRecord_m4_648,
	SslClientStream_OnNegotiateHandshakeCallback_m4_649,
	SslClientStream_OnLocalCertificateSelection_m4_650,
	SslClientStream_get_HaveRemoteValidation2Callback_m4_651,
	SslClientStream_OnRemoteCertificateValidation2_m4_652,
	SslClientStream_OnRemoteCertificateValidation_m4_653,
	SslClientStream_RaiseServerCertificateValidation_m4_654,
	SslClientStream_RaiseServerCertificateValidation2_m4_655,
	SslClientStream_RaiseClientCertificateSelection_m4_656,
	SslClientStream_OnLocalPrivateKeySelection_m4_657,
	SslClientStream_RaisePrivateKeySelection_m4_658,
	SslCipherSuite__ctor_m4_659,
	SslCipherSuite_ComputeServerRecordMAC_m4_660,
	SslCipherSuite_ComputeClientRecordMAC_m4_661,
	SslCipherSuite_ComputeMasterSecret_m4_662,
	SslCipherSuite_ComputeKeys_m4_663,
	SslCipherSuite_prf_m4_664,
	SslHandshakeHash__ctor_m4_665,
	SslHandshakeHash_Initialize_m4_666,
	SslHandshakeHash_HashFinal_m4_667,
	SslHandshakeHash_HashCore_m4_668,
	SslHandshakeHash_CreateSignature_m4_669,
	SslHandshakeHash_initializePad_m4_670,
	InternalAsyncResult__ctor_m4_671,
	InternalAsyncResult_get_ProceedAfterHandshake_m4_672,
	InternalAsyncResult_get_FromWrite_m4_673,
	InternalAsyncResult_get_Buffer_m4_674,
	InternalAsyncResult_get_Offset_m4_675,
	InternalAsyncResult_get_Count_m4_676,
	InternalAsyncResult_get_BytesRead_m4_677,
	InternalAsyncResult_get_AsyncState_m4_678,
	InternalAsyncResult_get_AsyncException_m4_679,
	InternalAsyncResult_get_CompletedWithError_m4_680,
	InternalAsyncResult_get_AsyncWaitHandle_m4_681,
	InternalAsyncResult_get_IsCompleted_m4_682,
	InternalAsyncResult_SetComplete_m4_683,
	InternalAsyncResult_SetComplete_m4_684,
	InternalAsyncResult_SetComplete_m4_685,
	InternalAsyncResult_SetComplete_m4_686,
	SslStreamBase__ctor_m4_687,
	SslStreamBase__cctor_m4_688,
	SslStreamBase_AsyncHandshakeCallback_m4_689,
	SslStreamBase_get_MightNeedHandshake_m4_690,
	SslStreamBase_NegotiateHandshake_m4_691,
	SslStreamBase_RaiseLocalCertificateSelection_m4_692,
	SslStreamBase_RaiseRemoteCertificateValidation_m4_693,
	SslStreamBase_RaiseRemoteCertificateValidation2_m4_694,
	SslStreamBase_RaiseLocalPrivateKeySelection_m4_695,
	SslStreamBase_get_CheckCertRevocationStatus_m4_696,
	SslStreamBase_set_CheckCertRevocationStatus_m4_697,
	SslStreamBase_get_CipherAlgorithm_m4_698,
	SslStreamBase_get_CipherStrength_m4_699,
	SslStreamBase_get_HashAlgorithm_m4_700,
	SslStreamBase_get_HashStrength_m4_701,
	SslStreamBase_get_KeyExchangeStrength_m4_702,
	SslStreamBase_get_KeyExchangeAlgorithm_m4_703,
	SslStreamBase_get_SecurityProtocol_m4_704,
	SslStreamBase_get_ServerCertificate_m4_705,
	SslStreamBase_get_ServerCertificates_m4_706,
	SslStreamBase_BeginNegotiateHandshake_m4_707,
	SslStreamBase_EndNegotiateHandshake_m4_708,
	SslStreamBase_BeginRead_m4_709,
	SslStreamBase_InternalBeginRead_m4_710,
	SslStreamBase_InternalReadCallback_m4_711,
	SslStreamBase_InternalBeginWrite_m4_712,
	SslStreamBase_InternalWriteCallback_m4_713,
	SslStreamBase_BeginWrite_m4_714,
	SslStreamBase_EndRead_m4_715,
	SslStreamBase_EndWrite_m4_716,
	SslStreamBase_Close_m4_717,
	SslStreamBase_Flush_m4_718,
	SslStreamBase_Read_m4_719,
	SslStreamBase_Read_m4_720,
	SslStreamBase_Seek_m4_721,
	SslStreamBase_SetLength_m4_722,
	SslStreamBase_Write_m4_723,
	SslStreamBase_Write_m4_724,
	SslStreamBase_get_CanRead_m4_725,
	SslStreamBase_get_CanSeek_m4_726,
	SslStreamBase_get_CanWrite_m4_727,
	SslStreamBase_get_Length_m4_728,
	SslStreamBase_get_Position_m4_729,
	SslStreamBase_set_Position_m4_730,
	SslStreamBase_Finalize_m4_731,
	SslStreamBase_Dispose_m4_732,
	SslStreamBase_resetBuffer_m4_733,
	SslStreamBase_checkDisposed_m4_734,
	TlsCipherSuite__ctor_m4_735,
	TlsCipherSuite_ComputeServerRecordMAC_m4_736,
	TlsCipherSuite_ComputeClientRecordMAC_m4_737,
	TlsCipherSuite_ComputeMasterSecret_m4_738,
	TlsCipherSuite_ComputeKeys_m4_739,
	TlsClientSettings__ctor_m4_740,
	TlsClientSettings_get_TargetHost_m4_741,
	TlsClientSettings_set_TargetHost_m4_742,
	TlsClientSettings_get_Certificates_m4_743,
	TlsClientSettings_set_Certificates_m4_744,
	TlsClientSettings_get_ClientCertificate_m4_745,
	TlsClientSettings_set_ClientCertificate_m4_746,
	TlsClientSettings_UpdateCertificateRSA_m4_747,
	TlsException__ctor_m4_748,
	TlsException__ctor_m4_749,
	TlsException__ctor_m4_750,
	TlsException__ctor_m4_751,
	TlsException__ctor_m4_752,
	TlsException__ctor_m4_753,
	TlsException_get_Alert_m4_754,
	TlsServerSettings__ctor_m4_755,
	TlsServerSettings_get_ServerKeyExchange_m4_756,
	TlsServerSettings_set_ServerKeyExchange_m4_757,
	TlsServerSettings_get_Certificates_m4_758,
	TlsServerSettings_set_Certificates_m4_759,
	TlsServerSettings_get_CertificateRSA_m4_760,
	TlsServerSettings_get_RsaParameters_m4_761,
	TlsServerSettings_set_RsaParameters_m4_762,
	TlsServerSettings_set_SignedParams_m4_763,
	TlsServerSettings_get_CertificateRequest_m4_764,
	TlsServerSettings_set_CertificateRequest_m4_765,
	TlsServerSettings_set_CertificateTypes_m4_766,
	TlsServerSettings_set_DistinguisedNames_m4_767,
	TlsServerSettings_UpdateCertificateRSA_m4_768,
	TlsStream__ctor_m4_769,
	TlsStream__ctor_m4_770,
	TlsStream_get_EOF_m4_771,
	TlsStream_get_CanWrite_m4_772,
	TlsStream_get_CanRead_m4_773,
	TlsStream_get_CanSeek_m4_774,
	TlsStream_get_Position_m4_775,
	TlsStream_set_Position_m4_776,
	TlsStream_get_Length_m4_777,
	TlsStream_ReadSmallValue_m4_778,
	TlsStream_ReadByte_m4_779,
	TlsStream_ReadInt16_m4_780,
	TlsStream_ReadInt24_m4_781,
	TlsStream_ReadBytes_m4_782,
	TlsStream_Write_m4_783,
	TlsStream_Write_m4_784,
	TlsStream_WriteInt24_m4_785,
	TlsStream_Write_m4_786,
	TlsStream_Write_m4_787,
	TlsStream_Reset_m4_788,
	TlsStream_ToArray_m4_789,
	TlsStream_Flush_m4_790,
	TlsStream_SetLength_m4_791,
	TlsStream_Seek_m4_792,
	TlsStream_Read_m4_793,
	TlsStream_Write_m4_794,
	HandshakeMessage__ctor_m4_795,
	HandshakeMessage__ctor_m4_796,
	HandshakeMessage__ctor_m4_797,
	HandshakeMessage_get_Context_m4_798,
	HandshakeMessage_get_HandshakeType_m4_799,
	HandshakeMessage_get_ContentType_m4_800,
	HandshakeMessage_Process_m4_801,
	HandshakeMessage_Update_m4_802,
	HandshakeMessage_EncodeMessage_m4_803,
	HandshakeMessage_Compare_m4_804,
	TlsClientCertificate__ctor_m4_805,
	TlsClientCertificate_get_ClientCertificate_m4_806,
	TlsClientCertificate_Update_m4_807,
	TlsClientCertificate_GetClientCertificate_m4_808,
	TlsClientCertificate_SendCertificates_m4_809,
	TlsClientCertificate_ProcessAsSsl3_m4_810,
	TlsClientCertificate_ProcessAsTls1_m4_811,
	TlsClientCertificate_FindParentCertificate_m4_812,
	TlsClientCertificateVerify__ctor_m4_813,
	TlsClientCertificateVerify_Update_m4_814,
	TlsClientCertificateVerify_ProcessAsSsl3_m4_815,
	TlsClientCertificateVerify_ProcessAsTls1_m4_816,
	TlsClientCertificateVerify_getClientCertRSA_m4_817,
	TlsClientCertificateVerify_getUnsignedBigInteger_m4_818,
	TlsClientFinished__ctor_m4_819,
	TlsClientFinished__cctor_m4_820,
	TlsClientFinished_Update_m4_821,
	TlsClientFinished_ProcessAsSsl3_m4_822,
	TlsClientFinished_ProcessAsTls1_m4_823,
	TlsClientHello__ctor_m4_824,
	TlsClientHello_Update_m4_825,
	TlsClientHello_ProcessAsSsl3_m4_826,
	TlsClientHello_ProcessAsTls1_m4_827,
	TlsClientKeyExchange__ctor_m4_828,
	TlsClientKeyExchange_ProcessAsSsl3_m4_829,
	TlsClientKeyExchange_ProcessAsTls1_m4_830,
	TlsClientKeyExchange_ProcessCommon_m4_831,
	TlsServerCertificate__ctor_m4_832,
	TlsServerCertificate_Update_m4_833,
	TlsServerCertificate_ProcessAsSsl3_m4_834,
	TlsServerCertificate_ProcessAsTls1_m4_835,
	TlsServerCertificate_checkCertificateUsage_m4_836,
	TlsServerCertificate_validateCertificates_m4_837,
	TlsServerCertificate_checkServerIdentity_m4_838,
	TlsServerCertificate_checkDomainName_m4_839,
	TlsServerCertificate_Match_m4_840,
	TlsServerCertificateRequest__ctor_m4_841,
	TlsServerCertificateRequest_Update_m4_842,
	TlsServerCertificateRequest_ProcessAsSsl3_m4_843,
	TlsServerCertificateRequest_ProcessAsTls1_m4_844,
	TlsServerFinished__ctor_m4_845,
	TlsServerFinished__cctor_m4_846,
	TlsServerFinished_Update_m4_847,
	TlsServerFinished_ProcessAsSsl3_m4_848,
	TlsServerFinished_ProcessAsTls1_m4_849,
	TlsServerHello__ctor_m4_850,
	TlsServerHello_Update_m4_851,
	TlsServerHello_ProcessAsSsl3_m4_852,
	TlsServerHello_ProcessAsTls1_m4_853,
	TlsServerHello_processProtocol_m4_854,
	TlsServerHelloDone__ctor_m4_855,
	TlsServerHelloDone_ProcessAsSsl3_m4_856,
	TlsServerHelloDone_ProcessAsTls1_m4_857,
	TlsServerKeyExchange__ctor_m4_858,
	TlsServerKeyExchange_Update_m4_859,
	TlsServerKeyExchange_ProcessAsSsl3_m4_860,
	TlsServerKeyExchange_ProcessAsTls1_m4_861,
	TlsServerKeyExchange_verifySignature_m4_862,
	PrimalityTest__ctor_m4_863,
	PrimalityTest_Invoke_m4_864,
	PrimalityTest_BeginInvoke_m4_865,
	PrimalityTest_EndInvoke_m4_866,
	CertificateValidationCallback__ctor_m4_867,
	CertificateValidationCallback_Invoke_m4_868,
	CertificateValidationCallback_BeginInvoke_m4_869,
	CertificateValidationCallback_EndInvoke_m4_870,
	CertificateValidationCallback2__ctor_m4_871,
	CertificateValidationCallback2_Invoke_m4_872,
	CertificateValidationCallback2_BeginInvoke_m4_873,
	CertificateValidationCallback2_EndInvoke_m4_874,
	CertificateSelectionCallback__ctor_m4_875,
	CertificateSelectionCallback_Invoke_m4_876,
	CertificateSelectionCallback_BeginInvoke_m4_877,
	CertificateSelectionCallback_EndInvoke_m4_878,
	PrivateKeySelectionCallback__ctor_m4_879,
	PrivateKeySelectionCallback_Invoke_m4_880,
	PrivateKeySelectionCallback_BeginInvoke_m4_881,
	PrivateKeySelectionCallback_EndInvoke_m4_882,
	Hashtable__ctor_m5_0,
	Hashtable__ctor_m5_1,
	Hashtable_get_Item_m5_2,
	Hashtable_set_Item_m5_3,
	Hashtable_GetEnumerator_m5_4,
	Hashtable_ToString_m5_5,
	DictionaryEntryEnumerator__ctor_m5_6,
	DictionaryEntryEnumerator_MoveNext_m5_7,
	DictionaryEntryEnumerator_Reset_m5_8,
	DictionaryEntryEnumerator_System_Collections_IEnumerator_get_Current_m5_9,
	DictionaryEntryEnumerator_get_Current_m5_10,
	DictionaryEntryEnumerator_Dispose_m5_11,
	BigInteger__ctor_m5_12,
	BigInteger__ctor_m5_13,
	BigInteger__ctor_m5_14,
	BigInteger__ctor_m5_15,
	BigInteger__ctor_m5_16,
	BigInteger_op_Implicit_m5_17,
	BigInteger_op_Implicit_m5_18,
	BigInteger_op_Addition_m5_19,
	BigInteger_op_Subtraction_m5_20,
	BigInteger_op_Multiply_m5_21,
	BigInteger_op_LeftShift_m5_22,
	BigInteger_shiftLeft_m5_23,
	BigInteger_shiftRight_m5_24,
	BigInteger_op_UnaryNegation_m5_25,
	BigInteger_op_Equality_m5_26,
	BigInteger_Equals_m5_27,
	BigInteger_GetHashCode_m5_28,
	BigInteger_op_GreaterThan_m5_29,
	BigInteger_op_LessThan_m5_30,
	BigInteger_op_GreaterThanOrEqual_m5_31,
	BigInteger_multiByteDivide_m5_32,
	BigInteger_singleByteDivide_m5_33,
	BigInteger_op_Division_m5_34,
	BigInteger_op_Modulus_m5_35,
	BigInteger_ToString_m5_36,
	BigInteger_ToString_m5_37,
	BigInteger_ModPow_m5_38,
	BigInteger_BarrettReduction_m5_39,
	BigInteger_GenerateRandom_m5_40,
	BigInteger_genRandomBits_m5_41,
	BigInteger_bitCount_m5_42,
	BigInteger_GetBytes_m5_43,
	BigInteger__cctor_m5_44,
	DiffieHellmanCryptoProvider__ctor_m5_45,
	DiffieHellmanCryptoProvider_get_PublicKey_m5_46,
	DiffieHellmanCryptoProvider_get_SharedKey_m5_47,
	DiffieHellmanCryptoProvider_DeriveSharedKey_m5_48,
	DiffieHellmanCryptoProvider_Encrypt_m5_49,
	DiffieHellmanCryptoProvider_Encrypt_m5_50,
	DiffieHellmanCryptoProvider_Decrypt_m5_51,
	DiffieHellmanCryptoProvider_Dispose_m5_52,
	DiffieHellmanCryptoProvider_Dispose_m5_53,
	DiffieHellmanCryptoProvider_CalculatePublicKey_m5_54,
	DiffieHellmanCryptoProvider_CalculateSharedKey_m5_55,
	DiffieHellmanCryptoProvider_GenerateRandomSecret_m5_56,
	DiffieHellmanCryptoProvider__cctor_m5_57,
	EnetChannel__ctor_m5_58,
	EnetChannel_ContainsUnreliableSequenceNumber_m5_59,
	EnetChannel_ContainsReliableSequenceNumber_m5_60,
	EnetChannel_FetchReliableSequenceNumber_m5_61,
	EnetChannel_clearAll_m5_62,
	MyAction__ctor_m5_63,
	MyAction_Invoke_m5_64,
	MyAction_BeginInvoke_m5_65,
	MyAction_EndInvoke_m5_66,
	U3CU3Ec__DisplayClass2__ctor_m5_67,
	U3CU3Ec__DisplayClass2_U3CEnqueueDebugReturnU3Eb__0_m5_68,
	U3CU3Ec__DisplayClass6__ctor_m5_69,
	U3CU3Ec__DisplayClass6_U3CEnqueueStatusCallbackU3Eb__4_m5_70,
	PeerBase_get_TrafficStatsEnabledTime_m5_71,
	PeerBase_get_TrafficStatsEnabled_m5_72,
	PeerBase_set_TrafficStatsEnabled_m5_73,
	PeerBase_get_ServerAddress_m5_74,
	PeerBase_set_ServerAddress_m5_75,
	PeerBase_get_Listener_m5_76,
	PeerBase_set_Listener_m5_77,
	PeerBase_get_QuickResendAttempts_m5_78,
	PeerBase_set_QuickResendAttempts_m5_79,
	PeerBase_get_NetworkSimulationSettings_m5_80,
	PeerBase_CommandLogResize_m5_81,
	PeerBase_CommandLogInit_m5_82,
	PeerBase_InitOnce_m5_83,
	PeerBase_EnqueueOperation_m5_84,
	PeerBase_SendAcksOnly_m5_85,
	PeerBase_InitCallback_m5_86,
	PeerBase_get_IsSendingOnlyAcks_m5_87,
	PeerBase_set_IsSendingOnlyAcks_m5_88,
	PeerBase_ExchangeKeysForEncryption_m5_89,
	PeerBase_DeriveSharedKey_m5_90,
	PeerBase_EnqueueActionForDispatch_m5_91,
	PeerBase_EnqueueDebugReturn_m5_92,
	PeerBase_EnqueueStatusCallback_m5_93,
	PeerBase_InitPeerBase_m5_94,
	PeerBase_DeserializeMessageAndCallback_m5_95,
	PeerBase_SendNetworkSimulated_m5_96,
	PeerBase_ReceiveNetworkSimulated_m5_97,
	PeerBase_NetworkSimRun_m5_98,
	PeerBase_UpdateRoundTripTimeAndVariance_m5_99,
	PeerBase_InitializeTrafficStats_m5_100,
	PeerBase__ctor_m5_101,
	PeerBase__cctor_m5_102,
	U3CU3Ec__DisplayClassb__ctor_m5_103,
	U3CU3Ec__DisplayClassd__ctor_m5_104,
	U3CU3Ec__DisplayClassd_U3CSendDataU3Eb__a_m5_105,
	U3CU3Ec__DisplayClass11__ctor_m5_106,
	U3CU3Ec__DisplayClass11_U3CReceiveIncomingCommandsU3Eb__f_m5_107,
	EnetPeer__ctor_m5_108,
	EnetPeer_InitPeerBase_m5_109,
	EnetPeer_Connect_m5_110,
	EnetPeer_Disconnect_m5_111,
	EnetPeer_StopConnection_m5_112,
	EnetPeer_FetchServerTimestamp_m5_113,
	EnetPeer_DispatchIncomingCommands_m5_114,
	EnetPeer_SendAcksOnly_m5_115,
	EnetPeer_SendOutgoingCommands_m5_116,
	EnetPeer_AreReliableCommandsInTransit_m5_117,
	EnetPeer_EnqueueOperation_m5_118,
	EnetPeer_CreateAndEnqueueCommand_m5_119,
	EnetPeer_SerializeOperationToMessage_m5_120,
	EnetPeer_SerializeToBuffer_m5_121,
	EnetPeer_SendData_m5_122,
	EnetPeer_QueueSentCommand_m5_123,
	EnetPeer_QueueOutgoingReliableCommand_m5_124,
	EnetPeer_QueueOutgoingUnreliableCommand_m5_125,
	EnetPeer_QueueOutgoingAcknowledgement_m5_126,
	EnetPeer_ReceiveIncomingCommands_m5_127,
	EnetPeer_ExecuteCommand_m5_128,
	EnetPeer_QueueIncomingCommand_m5_129,
	EnetPeer_RemoveSentReliableCommand_m5_130,
	EnetPeer_U3CExecuteCommandU3Eb__13_m5_131,
	EnetPeer__cctor_m5_132,
	U3CU3Ec__DisplayClass3__ctor_m5_133,
	U3CU3Ec__DisplayClass3_U3CHandleReceivedDatagramU3Eb__1_m5_134,
	U3CU3Ec__DisplayClass5__ctor_m5_135,
	U3CU3Ec__DisplayClass5_U3CHandleReceivedDatagramU3Eb__0_m5_136,
	IPhotonSocket_get_Listener_m5_137,
	IPhotonSocket_get_Protocol_m5_138,
	IPhotonSocket_set_Protocol_m5_139,
	IPhotonSocket_get_State_m5_140,
	IPhotonSocket_set_State_m5_141,
	IPhotonSocket_get_ServerAddress_m5_142,
	IPhotonSocket_set_ServerAddress_m5_143,
	IPhotonSocket_get_ServerPort_m5_144,
	IPhotonSocket_set_ServerPort_m5_145,
	IPhotonSocket_get_Connected_m5_146,
	IPhotonSocket_get_MTU_m5_147,
	IPhotonSocket__ctor_m5_148,
	IPhotonSocket_Connect_m5_149,
	IPhotonSocket_HandleReceivedDatagram_m5_150,
	IPhotonSocket_ReportDebugOfLevel_m5_151,
	IPhotonSocket_EnqueueDebugReturn_m5_152,
	IPhotonSocket_HandleException_m5_153,
	IPhotonSocket_TryParseAddress_m5_154,
	IPhotonSocket_GetIpAddress_m5_155,
	IPhotonSocket_U3CHandleExceptionU3Eb__7_m5_156,
	NCommand__ctor_m5_157,
	NCommand_CreateAck_m5_158,
	NCommand__ctor_m5_159,
	NCommand_Serialize_m5_160,
	NCommand_CompareTo_m5_161,
	NCommand_ToString_m5_162,
	SimulationItem__ctor_m5_163,
	SimulationItem_get_Delay_m5_164,
	SimulationItem_set_Delay_m5_165,
	NetworkSimulationSet_get_IsSimulationEnabled_m5_166,
	NetworkSimulationSet_set_IsSimulationEnabled_m5_167,
	NetworkSimulationSet_get_OutgoingLag_m5_168,
	NetworkSimulationSet_set_OutgoingLag_m5_169,
	NetworkSimulationSet_get_OutgoingJitter_m5_170,
	NetworkSimulationSet_set_OutgoingJitter_m5_171,
	NetworkSimulationSet_get_OutgoingLossPercentage_m5_172,
	NetworkSimulationSet_set_OutgoingLossPercentage_m5_173,
	NetworkSimulationSet_get_IncomingLag_m5_174,
	NetworkSimulationSet_set_IncomingLag_m5_175,
	NetworkSimulationSet_get_IncomingJitter_m5_176,
	NetworkSimulationSet_set_IncomingJitter_m5_177,
	NetworkSimulationSet_get_IncomingLossPercentage_m5_178,
	NetworkSimulationSet_set_IncomingLossPercentage_m5_179,
	NetworkSimulationSet_get_LostPackagesOut_m5_180,
	NetworkSimulationSet_set_LostPackagesOut_m5_181,
	NetworkSimulationSet_get_LostPackagesIn_m5_182,
	NetworkSimulationSet_set_LostPackagesIn_m5_183,
	NetworkSimulationSet_ToString_m5_184,
	NetworkSimulationSet__ctor_m5_185,
	OakleyGroups__cctor_m5_186,
	PhotonCodes__cctor_m5_187,
	CmdLogItem__ctor_m5_188,
	CmdLogItem__ctor_m5_189,
	CmdLogItem_ToString_m5_190,
	CmdLogReceivedReliable__ctor_m5_191,
	CmdLogReceivedReliable_ToString_m5_192,
	CmdLogReceivedAck__ctor_m5_193,
	CmdLogReceivedAck_ToString_m5_194,
	CmdLogSentReliable__ctor_m5_195,
	CmdLogSentReliable_ToString_m5_196,
	PhotonPeer_set_SocketImplementation_m5_197,
	PhotonPeer_set_DebugOut_m5_198,
	PhotonPeer_get_DebugOut_m5_199,
	PhotonPeer_get_Listener_m5_200,
	PhotonPeer_set_Listener_m5_201,
	PhotonPeer_get_TrafficStatsEnabled_m5_202,
	PhotonPeer_set_TrafficStatsEnabled_m5_203,
	PhotonPeer_get_TrafficStatsElapsedMs_m5_204,
	PhotonPeer_TrafficStatsReset_m5_205,
	PhotonPeer_get_TrafficStatsIncoming_m5_206,
	PhotonPeer_get_TrafficStatsOutgoing_m5_207,
	PhotonPeer_get_TrafficStatsGameLevel_m5_208,
	PhotonPeer_get_QuickResendAttempts_m5_209,
	PhotonPeer_set_QuickResendAttempts_m5_210,
	PhotonPeer_get_PeerState_m5_211,
	PhotonPeer_get_LimitOfUnreliableCommands_m5_212,
	PhotonPeer_set_LimitOfUnreliableCommands_m5_213,
	PhotonPeer_get_CrcEnabled_m5_214,
	PhotonPeer_set_CrcEnabled_m5_215,
	PhotonPeer_get_PacketLossByCrc_m5_216,
	PhotonPeer_get_ResentReliableCommands_m5_217,
	PhotonPeer_get_SentCountAllowance_m5_218,
	PhotonPeer_set_SentCountAllowance_m5_219,
	PhotonPeer_set_TimePingInterval_m5_220,
	PhotonPeer_get_DisconnectTimeout_m5_221,
	PhotonPeer_set_DisconnectTimeout_m5_222,
	PhotonPeer_get_ServerTimeInMilliSeconds_m5_223,
	PhotonPeer_get_RoundTripTime_m5_224,
	PhotonPeer_get_RoundTripTimeVariance_m5_225,
	PhotonPeer_get_TimestampOfLastSocketReceive_m5_226,
	PhotonPeer_get_ServerAddress_m5_227,
	PhotonPeer_get_UsedProtocol_m5_228,
	PhotonPeer_get_IsSimulationEnabled_m5_229,
	PhotonPeer_set_IsSimulationEnabled_m5_230,
	PhotonPeer_get_NetworkSimulationSettings_m5_231,
	PhotonPeer_get_IsEncryptionAvailable_m5_232,
	PhotonPeer_set_IsSendingOnlyAcks_m5_233,
	PhotonPeer__ctor_m5_234,
	PhotonPeer__ctor_m5_235,
	PhotonPeer_Connect_m5_236,
	PhotonPeer_Disconnect_m5_237,
	PhotonPeer_StopThread_m5_238,
	PhotonPeer_FetchServerTimestamp_m5_239,
	PhotonPeer_EstablishEncryption_m5_240,
	PhotonPeer_Service_m5_241,
	PhotonPeer_SendOutgoingCommands_m5_242,
	PhotonPeer_SendAcksOnly_m5_243,
	PhotonPeer_DispatchIncomingCommands_m5_244,
	PhotonPeer_VitalStatsToString_m5_245,
	PhotonPeer_OpCustom_m5_246,
	PhotonPeer_OpCustom_m5_247,
	PhotonPeer_OpCustom_m5_248,
	PhotonPeer_RegisterType_m5_249,
	PhotonPing_StartPing_m5_250,
	PhotonPing_Done_m5_251,
	PhotonPing_Dispose_m5_252,
	PhotonPing_Init_m5_253,
	PhotonPing__ctor_m5_254,
	PingMono_StartPing_m5_255,
	PingMono_Done_m5_256,
	PingMono_Dispose_m5_257,
	PingMono__ctor_m5_258,
	PingNativeDynamic__ctor_m5_259,
	OperationRequest__ctor_m5_260,
	OperationResponse_get_Item_m5_261,
	OperationResponse_ToString_m5_262,
	OperationResponse_ToStringFull_m5_263,
	OperationResponse__ctor_m5_264,
	EventData_get_Item_m5_265,
	EventData_ToString_m5_266,
	EventData__ctor_m5_267,
	SerializeMethod__ctor_m5_268,
	SerializeMethod_Invoke_m5_269,
	SerializeMethod_BeginInvoke_m5_270,
	SerializeMethod_EndInvoke_m5_271,
	SerializeStreamMethod__ctor_m5_272,
	SerializeStreamMethod_Invoke_m5_273,
	SerializeStreamMethod_BeginInvoke_m5_274,
	SerializeStreamMethod_EndInvoke_m5_275,
	DeserializeMethod__ctor_m5_276,
	DeserializeMethod_Invoke_m5_277,
	DeserializeMethod_BeginInvoke_m5_278,
	DeserializeMethod_EndInvoke_m5_279,
	DeserializeStreamMethod__ctor_m5_280,
	DeserializeStreamMethod_Invoke_m5_281,
	DeserializeStreamMethod_BeginInvoke_m5_282,
	DeserializeStreamMethod_EndInvoke_m5_283,
	CustomType__ctor_m5_284,
	Protocol_TryRegisterType_m5_285,
	Protocol_SerializeCustom_m5_286,
	Protocol_DeserializeCustom_m5_287,
	Protocol_GetTypeOfCode_m5_288,
	Protocol_GetCodeOfType_m5_289,
	Protocol_CreateArrayByType_m5_290,
	Protocol_SerializeOperationRequest_m5_291,
	Protocol_SerializeOperationRequest_m5_292,
	Protocol_DeserializeOperationRequest_m5_293,
	Protocol_SerializeOperationResponse_m5_294,
	Protocol_DeserializeOperationResponse_m5_295,
	Protocol_SerializeEventData_m5_296,
	Protocol_DeserializeEventData_m5_297,
	Protocol_SerializeParameterTable_m5_298,
	Protocol_DeserializeParameterTable_m5_299,
	Protocol_Serialize_m5_300,
	Protocol_SerializeByte_m5_301,
	Protocol_SerializeBoolean_m5_302,
	Protocol_SerializeShort_m5_303,
	Protocol_Serialize_m5_304,
	Protocol_SerializeInteger_m5_305,
	Protocol_Serialize_m5_306,
	Protocol_SerializeLong_m5_307,
	Protocol_SerializeFloat_m5_308,
	Protocol_Serialize_m5_309,
	Protocol_SerializeDouble_m5_310,
	Protocol_SerializeString_m5_311,
	Protocol_SerializeArray_m5_312,
	Protocol_SerializeByteArray_m5_313,
	Protocol_SerializeIntArrayOptimized_m5_314,
	Protocol_SerializeObjectArray_m5_315,
	Protocol_SerializeHashTable_m5_316,
	Protocol_SerializeDictionary_m5_317,
	Protocol_SerializeDictionaryHeader_m5_318,
	Protocol_SerializeDictionaryHeader_m5_319,
	Protocol_SerializeDictionaryElements_m5_320,
	Protocol_Deserialize_m5_321,
	Protocol_DeserializeByte_m5_322,
	Protocol_DeserializeBoolean_m5_323,
	Protocol_DeserializeShort_m5_324,
	Protocol_Deserialize_m5_325,
	Protocol_DeserializeInteger_m5_326,
	Protocol_Deserialize_m5_327,
	Protocol_DeserializeLong_m5_328,
	Protocol_DeserializeFloat_m5_329,
	Protocol_Deserialize_m5_330,
	Protocol_DeserializeDouble_m5_331,
	Protocol_DeserializeString_m5_332,
	Protocol_DeserializeArray_m5_333,
	Protocol_DeserializeByteArray_m5_334,
	Protocol_DeserializeIntArray_m5_335,
	Protocol_DeserializeStringArray_m5_336,
	Protocol_DeserializeObjectArray_m5_337,
	Protocol_DeserializeHashTable_m5_338,
	Protocol_DeserializeDictionary_m5_339,
	Protocol_DeserializeDictionaryArray_m5_340,
	Protocol_DeserializeDictionaryType_m5_341,
	Protocol__cctor_m5_342,
	SocketTcp__ctor_m5_343,
	SocketTcp_Dispose_m5_344,
	SocketTcp_Connect_m5_345,
	SocketTcp_Disconnect_m5_346,
	SocketTcp_Send_m5_347,
	SocketTcp_DnsAndConnect_m5_348,
	SocketTcp_ReceiveLoop_m5_349,
	SocketUdp__ctor_m5_350,
	SocketUdp_Dispose_m5_351,
	SocketUdp_Connect_m5_352,
	SocketUdp_Disconnect_m5_353,
	SocketUdp_Send_m5_354,
	SocketUdp_DnsAndConnect_m5_355,
	SocketUdp_ReceiveLoop_m5_356,
	IntegerMillisecondsDelegate__ctor_m5_357,
	IntegerMillisecondsDelegate_Invoke_m5_358,
	IntegerMillisecondsDelegate_BeginInvoke_m5_359,
	IntegerMillisecondsDelegate_EndInvoke_m5_360,
	ThreadSafeRandom_Next_m5_361,
	ThreadSafeRandom__cctor_m5_362,
	U3CU3Ec__DisplayClass1__ctor_m5_363,
	U3CU3Ec__DisplayClass1_U3CCallInBackgroundU3Eb__0_m5_364,
	SupportClass_CalculateCrc_m5_365,
	SupportClass_GetMethods_m5_366,
	SupportClass_GetTickCount_m5_367,
	SupportClass_CallInBackground_m5_368,
	SupportClass_CallInBackground_m5_369,
	SupportClass_WriteStackTrace_m5_370,
	SupportClass_WriteStackTrace_m5_371,
	SupportClass_DictionaryToString_m5_372,
	SupportClass_DictionaryToString_m5_373,
	SupportClass__cctor_m5_374,
	SupportClass_U3C_cctorU3Eb__3_m5_375,
	U3CU3Ec__DisplayClass3__ctor_m5_376,
	U3CU3Ec__DisplayClass3_U3CSendDataU3Eb__1_m5_377,
	TPeer__ctor_m5_378,
	TPeer_InitPeerBase_m5_379,
	TPeer_Connect_m5_380,
	TPeer_Disconnect_m5_381,
	TPeer_StopConnection_m5_382,
	TPeer_FetchServerTimestamp_m5_383,
	TPeer_EnqueueInit_m5_384,
	TPeer_DispatchIncomingCommands_m5_385,
	TPeer_SendOutgoingCommands_m5_386,
	TPeer_SendAcksOnly_m5_387,
	TPeer_EnqueueOperation_m5_388,
	TPeer_SerializeOperationToMessage_m5_389,
	TPeer_EnqueueMessageAsPayload_m5_390,
	TPeer_SendPing_m5_391,
	TPeer_SendData_m5_392,
	TPeer_ReceiveIncomingCommands_m5_393,
	TPeer_ReadPingResult_m5_394,
	TPeer_ReadPingResult_m5_395,
	TPeer__cctor_m5_396,
	TrafficStatsGameLevel_get_OperationByteCount_m5_397,
	TrafficStatsGameLevel_set_OperationByteCount_m5_398,
	TrafficStatsGameLevel_get_OperationCount_m5_399,
	TrafficStatsGameLevel_set_OperationCount_m5_400,
	TrafficStatsGameLevel_get_ResultByteCount_m5_401,
	TrafficStatsGameLevel_set_ResultByteCount_m5_402,
	TrafficStatsGameLevel_get_ResultCount_m5_403,
	TrafficStatsGameLevel_set_ResultCount_m5_404,
	TrafficStatsGameLevel_get_EventByteCount_m5_405,
	TrafficStatsGameLevel_set_EventByteCount_m5_406,
	TrafficStatsGameLevel_get_EventCount_m5_407,
	TrafficStatsGameLevel_set_EventCount_m5_408,
	TrafficStatsGameLevel_get_LongestOpResponseCallback_m5_409,
	TrafficStatsGameLevel_set_LongestOpResponseCallback_m5_410,
	TrafficStatsGameLevel_get_LongestOpResponseCallbackOpCode_m5_411,
	TrafficStatsGameLevel_set_LongestOpResponseCallbackOpCode_m5_412,
	TrafficStatsGameLevel_get_LongestEventCallback_m5_413,
	TrafficStatsGameLevel_set_LongestEventCallback_m5_414,
	TrafficStatsGameLevel_get_LongestEventCallbackCode_m5_415,
	TrafficStatsGameLevel_set_LongestEventCallbackCode_m5_416,
	TrafficStatsGameLevel_get_LongestDeltaBetweenDispatching_m5_417,
	TrafficStatsGameLevel_set_LongestDeltaBetweenDispatching_m5_418,
	TrafficStatsGameLevel_get_LongestDeltaBetweenSending_m5_419,
	TrafficStatsGameLevel_set_LongestDeltaBetweenSending_m5_420,
	TrafficStatsGameLevel_get_DispatchIncomingCommandsCalls_m5_421,
	TrafficStatsGameLevel_set_DispatchIncomingCommandsCalls_m5_422,
	TrafficStatsGameLevel_get_SendOutgoingCommandsCalls_m5_423,
	TrafficStatsGameLevel_set_SendOutgoingCommandsCalls_m5_424,
	TrafficStatsGameLevel_get_TotalMessageCount_m5_425,
	TrafficStatsGameLevel_get_TotalIncomingMessageCount_m5_426,
	TrafficStatsGameLevel_get_TotalOutgoingMessageCount_m5_427,
	TrafficStatsGameLevel_CountOperation_m5_428,
	TrafficStatsGameLevel_CountResult_m5_429,
	TrafficStatsGameLevel_CountEvent_m5_430,
	TrafficStatsGameLevel_TimeForResponseCallback_m5_431,
	TrafficStatsGameLevel_TimeForEventCallback_m5_432,
	TrafficStatsGameLevel_DispatchIncomingCommandsCalled_m5_433,
	TrafficStatsGameLevel_SendOutgoingCommandsCalled_m5_434,
	TrafficStatsGameLevel_ToString_m5_435,
	TrafficStatsGameLevel_ToStringVitalStats_m5_436,
	TrafficStatsGameLevel__ctor_m5_437,
	TrafficStats_get_PackageHeaderSize_m5_438,
	TrafficStats_set_PackageHeaderSize_m5_439,
	TrafficStats_get_ReliableCommandCount_m5_440,
	TrafficStats_set_ReliableCommandCount_m5_441,
	TrafficStats_get_UnreliableCommandCount_m5_442,
	TrafficStats_set_UnreliableCommandCount_m5_443,
	TrafficStats_get_FragmentCommandCount_m5_444,
	TrafficStats_set_FragmentCommandCount_m5_445,
	TrafficStats_get_ControlCommandCount_m5_446,
	TrafficStats_set_ControlCommandCount_m5_447,
	TrafficStats_get_TotalPacketCount_m5_448,
	TrafficStats_set_TotalPacketCount_m5_449,
	TrafficStats_get_TotalCommandsInPackets_m5_450,
	TrafficStats_set_TotalCommandsInPackets_m5_451,
	TrafficStats_get_ReliableCommandBytes_m5_452,
	TrafficStats_set_ReliableCommandBytes_m5_453,
	TrafficStats_get_UnreliableCommandBytes_m5_454,
	TrafficStats_set_UnreliableCommandBytes_m5_455,
	TrafficStats_get_FragmentCommandBytes_m5_456,
	TrafficStats_set_FragmentCommandBytes_m5_457,
	TrafficStats_get_ControlCommandBytes_m5_458,
	TrafficStats_set_ControlCommandBytes_m5_459,
	TrafficStats__ctor_m5_460,
	TrafficStats_get_TotalCommandBytes_m5_461,
	TrafficStats_get_TotalPacketBytes_m5_462,
	TrafficStats_set_TimestampOfLastAck_m5_463,
	TrafficStats_set_TimestampOfLastReliableCommand_m5_464,
	TrafficStats_CountControlCommand_m5_465,
	TrafficStats_CountReliableOpCommand_m5_466,
	TrafficStats_CountUnreliableOpCommand_m5_467,
	TrafficStats_CountFragmentOpCommand_m5_468,
	TrafficStats_ToString_m5_469,
	AssetBundleCreateRequest__ctor_m6_0,
	AssetBundleCreateRequest_get_assetBundle_m6_1,
	AssetBundleCreateRequest_DisableCompatibilityChecks_m6_2,
	AssetBundleRequest__ctor_m6_3,
	AssetBundleRequest_get_asset_m6_4,
	AssetBundleRequest_get_allAssets_m6_5,
	AssetBundle_LoadAsset_m6_6,
	AssetBundle_LoadAsset_Internal_m6_7,
	AssetBundle_LoadAssetWithSubAssets_Internal_m6_8,
	SystemInfo_get_operatingSystem_m6_9,
	WaitForSeconds__ctor_m6_10,
	WaitForFixedUpdate__ctor_m6_11,
	WaitForEndOfFrame__ctor_m6_12,
	Coroutine__ctor_m6_13,
	Coroutine_ReleaseCoroutine_m6_14,
	Coroutine_Finalize_m6_15,
	ScriptableObject__ctor_m6_16,
	ScriptableObject_Internal_CreateScriptableObject_m6_17,
	ScriptableObject_CreateInstance_m6_18,
	ScriptableObject_CreateInstance_m6_19,
	ScriptableObject_CreateInstanceFromType_m6_20,
	UnhandledExceptionHandler__ctor_m6_21,
	UnhandledExceptionHandler_RegisterUECatcher_m6_22,
	UnhandledExceptionHandler_HandleUnhandledException_m6_23,
	UnhandledExceptionHandler_PrintException_m6_24,
	UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m6_25,
	GameCenterPlatform__ctor_m6_26,
	GameCenterPlatform__cctor_m6_27,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m6_28,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m6_29,
	GameCenterPlatform_Internal_Authenticate_m6_30,
	GameCenterPlatform_Internal_Authenticated_m6_31,
	GameCenterPlatform_Internal_UserName_m6_32,
	GameCenterPlatform_Internal_UserID_m6_33,
	GameCenterPlatform_Internal_Underage_m6_34,
	GameCenterPlatform_Internal_UserImage_m6_35,
	GameCenterPlatform_Internal_LoadFriends_m6_36,
	GameCenterPlatform_Internal_LoadAchievementDescriptions_m6_37,
	GameCenterPlatform_Internal_LoadAchievements_m6_38,
	GameCenterPlatform_Internal_ReportProgress_m6_39,
	GameCenterPlatform_Internal_ReportScore_m6_40,
	GameCenterPlatform_Internal_LoadScores_m6_41,
	GameCenterPlatform_Internal_ShowAchievementsUI_m6_42,
	GameCenterPlatform_Internal_ShowLeaderboardUI_m6_43,
	GameCenterPlatform_Internal_LoadUsers_m6_44,
	GameCenterPlatform_Internal_ResetAllAchievements_m6_45,
	GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6_46,
	GameCenterPlatform_ResetAllAchievements_m6_47,
	GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m6_48,
	GameCenterPlatform_ShowLeaderboardUI_m6_49,
	GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6_50,
	GameCenterPlatform_ClearAchievementDescriptions_m6_51,
	GameCenterPlatform_SetAchievementDescription_m6_52,
	GameCenterPlatform_SetAchievementDescriptionImage_m6_53,
	GameCenterPlatform_TriggerAchievementDescriptionCallback_m6_54,
	GameCenterPlatform_AuthenticateCallbackWrapper_m6_55,
	GameCenterPlatform_ClearFriends_m6_56,
	GameCenterPlatform_SetFriends_m6_57,
	GameCenterPlatform_SetFriendImage_m6_58,
	GameCenterPlatform_TriggerFriendsCallbackWrapper_m6_59,
	GameCenterPlatform_AchievementCallbackWrapper_m6_60,
	GameCenterPlatform_ProgressCallbackWrapper_m6_61,
	GameCenterPlatform_ScoreCallbackWrapper_m6_62,
	GameCenterPlatform_ScoreLoaderCallbackWrapper_m6_63,
	GameCenterPlatform_get_localUser_m6_64,
	GameCenterPlatform_PopulateLocalUser_m6_65,
	GameCenterPlatform_LoadAchievementDescriptions_m6_66,
	GameCenterPlatform_ReportProgress_m6_67,
	GameCenterPlatform_LoadAchievements_m6_68,
	GameCenterPlatform_ReportScore_m6_69,
	GameCenterPlatform_LoadScores_m6_70,
	GameCenterPlatform_LoadScores_m6_71,
	GameCenterPlatform_LeaderboardCallbackWrapper_m6_72,
	GameCenterPlatform_GetLoading_m6_73,
	GameCenterPlatform_VerifyAuthentication_m6_74,
	GameCenterPlatform_ShowAchievementsUI_m6_75,
	GameCenterPlatform_ShowLeaderboardUI_m6_76,
	GameCenterPlatform_ClearUsers_m6_77,
	GameCenterPlatform_SetUser_m6_78,
	GameCenterPlatform_SetUserImage_m6_79,
	GameCenterPlatform_TriggerUsersCallbackWrapper_m6_80,
	GameCenterPlatform_LoadUsers_m6_81,
	GameCenterPlatform_SafeSetUserImage_m6_82,
	GameCenterPlatform_SafeClearArray_m6_83,
	GameCenterPlatform_CreateLeaderboard_m6_84,
	GameCenterPlatform_CreateAchievement_m6_85,
	GameCenterPlatform_TriggerResetAchievementCallback_m6_86,
	GcLeaderboard__ctor_m6_87,
	GcLeaderboard_Finalize_m6_88,
	GcLeaderboard_Contains_m6_89,
	GcLeaderboard_SetScores_m6_90,
	GcLeaderboard_SetLocalScore_m6_91,
	GcLeaderboard_SetMaxRange_m6_92,
	GcLeaderboard_SetTitle_m6_93,
	GcLeaderboard_Internal_LoadScores_m6_94,
	GcLeaderboard_Internal_LoadScoresWithUsers_m6_95,
	GcLeaderboard_Loading_m6_96,
	GcLeaderboard_Dispose_m6_97,
	BoneWeight_get_weight0_m6_98,
	BoneWeight_set_weight0_m6_99,
	BoneWeight_get_weight1_m6_100,
	BoneWeight_set_weight1_m6_101,
	BoneWeight_get_weight2_m6_102,
	BoneWeight_set_weight2_m6_103,
	BoneWeight_get_weight3_m6_104,
	BoneWeight_set_weight3_m6_105,
	BoneWeight_get_boneIndex0_m6_106,
	BoneWeight_set_boneIndex0_m6_107,
	BoneWeight_get_boneIndex1_m6_108,
	BoneWeight_set_boneIndex1_m6_109,
	BoneWeight_get_boneIndex2_m6_110,
	BoneWeight_set_boneIndex2_m6_111,
	BoneWeight_get_boneIndex3_m6_112,
	BoneWeight_set_boneIndex3_m6_113,
	BoneWeight_GetHashCode_m6_114,
	BoneWeight_Equals_m6_115,
	BoneWeight_op_Equality_m6_116,
	BoneWeight_op_Inequality_m6_117,
	Renderer_set_enabled_m6_118,
	Renderer_get_material_m6_119,
	Renderer_set_material_m6_120,
	Screen_get_width_m6_121,
	Screen_get_height_m6_122,
	GUILayer_HitTest_m6_123,
	GUILayer_INTERNAL_CALL_HitTest_m6_124,
	Texture__ctor_m6_125,
	Texture_Internal_GetWidth_m6_126,
	Texture_Internal_GetHeight_m6_127,
	Texture_get_width_m6_128,
	Texture_get_height_m6_129,
	Texture2D__ctor_m6_130,
	Texture2D_Internal_Create_m6_131,
	RenderTexture_Internal_GetWidth_m6_132,
	RenderTexture_Internal_GetHeight_m6_133,
	RenderTexture_get_width_m6_134,
	RenderTexture_get_height_m6_135,
	StateChanged__ctor_m6_136,
	StateChanged_Invoke_m6_137,
	StateChanged_BeginInvoke_m6_138,
	StateChanged_EndInvoke_m6_139,
	CullingGroup_Finalize_m6_140,
	CullingGroup_Dispose_m6_141,
	CullingGroup_SendEvents_m6_142,
	CullingGroup_FinalizerFailure_m6_143,
	GradientColorKey__ctor_m6_144,
	GradientAlphaKey__ctor_m6_145,
	Gradient__ctor_m6_146,
	Gradient_Init_m6_147,
	Gradient_Cleanup_m6_148,
	Gradient_Finalize_m6_149,
	TouchScreenKeyboard__ctor_m6_150,
	TouchScreenKeyboard_Destroy_m6_151,
	TouchScreenKeyboard_Finalize_m6_152,
	TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6_153,
	TouchScreenKeyboard_get_isSupported_m6_154,
	TouchScreenKeyboard_Open_m6_155,
	TouchScreenKeyboard_Open_m6_156,
	TouchScreenKeyboard_get_text_m6_157,
	TouchScreenKeyboard_get_done_m6_158,
	Gizmos_DrawWireSphere_m6_159,
	Gizmos_INTERNAL_CALL_DrawWireSphere_m6_160,
	Gizmos_DrawSphere_m6_161,
	Gizmos_INTERNAL_CALL_DrawSphere_m6_162,
	Gizmos_DrawWireCube_m6_163,
	Gizmos_INTERNAL_CALL_DrawWireCube_m6_164,
	Gizmos_DrawCube_m6_165,
	Gizmos_INTERNAL_CALL_DrawCube_m6_166,
	Gizmos_set_color_m6_167,
	Gizmos_INTERNAL_set_color_m6_168,
	LayerMask_get_value_m6_169,
	LayerMask_set_value_m6_170,
	LayerMask_LayerToName_m6_171,
	LayerMask_NameToLayer_m6_172,
	LayerMask_GetMask_m6_173,
	LayerMask_op_Implicit_m6_174,
	LayerMask_op_Implicit_m6_175,
	Vector2__ctor_m6_176,
	Vector2_ToString_m6_177,
	Vector2_GetHashCode_m6_178,
	Vector2_Equals_m6_179,
	Vector2_get_sqrMagnitude_m6_180,
	Vector2_SqrMagnitude_m6_181,
	Vector2_get_zero_m6_182,
	Vector2_get_up_m6_183,
	Vector2_op_Addition_m6_184,
	Vector2_op_Subtraction_m6_185,
	Vector2_op_UnaryNegation_m6_186,
	Vector2_op_Multiply_m6_187,
	Vector2_op_Equality_m6_188,
	Vector2_op_Implicit_m6_189,
	Vector2_op_Implicit_m6_190,
	Vector3__ctor_m6_191,
	Vector3_Lerp_m6_192,
	Vector3_Slerp_m6_193,
	Vector3_INTERNAL_CALL_Slerp_m6_194,
	Vector3_MoveTowards_m6_195,
	Vector3_RotateTowards_m6_196,
	Vector3_INTERNAL_CALL_RotateTowards_m6_197,
	Vector3_GetHashCode_m6_198,
	Vector3_Equals_m6_199,
	Vector3_Normalize_m6_200,
	Vector3_get_normalized_m6_201,
	Vector3_ToString_m6_202,
	Vector3_ToString_m6_203,
	Vector3_Dot_m6_204,
	Vector3_Angle_m6_205,
	Vector3_Distance_m6_206,
	Vector3_Magnitude_m6_207,
	Vector3_get_magnitude_m6_208,
	Vector3_SqrMagnitude_m6_209,
	Vector3_get_sqrMagnitude_m6_210,
	Vector3_Min_m6_211,
	Vector3_Max_m6_212,
	Vector3_get_zero_m6_213,
	Vector3_get_one_m6_214,
	Vector3_get_forward_m6_215,
	Vector3_get_back_m6_216,
	Vector3_get_up_m6_217,
	Vector3_get_down_m6_218,
	Vector3_get_left_m6_219,
	Vector3_get_right_m6_220,
	Vector3_op_Addition_m6_221,
	Vector3_op_Subtraction_m6_222,
	Vector3_op_UnaryNegation_m6_223,
	Vector3_op_Multiply_m6_224,
	Vector3_op_Multiply_m6_225,
	Vector3_op_Division_m6_226,
	Vector3_op_Equality_m6_227,
	Vector3_op_Inequality_m6_228,
	Color__ctor_m6_229,
	Color__ctor_m6_230,
	Color_ToString_m6_231,
	Color_GetHashCode_m6_232,
	Color_Equals_m6_233,
	Color_get_red_m6_234,
	Color_get_green_m6_235,
	Color_get_white_m6_236,
	Color_get_yellow_m6_237,
	Color_get_cyan_m6_238,
	Color_op_Multiply_m6_239,
	Color_op_Implicit_m6_240,
	Color32__ctor_m6_241,
	Color32_ToString_m6_242,
	Quaternion__ctor_m6_243,
	Quaternion_get_identity_m6_244,
	Quaternion_Dot_m6_245,
	Quaternion_LookRotation_m6_246,
	Quaternion_INTERNAL_CALL_LookRotation_m6_247,
	Quaternion_Slerp_m6_248,
	Quaternion_INTERNAL_CALL_Slerp_m6_249,
	Quaternion_SlerpUnclamped_m6_250,
	Quaternion_INTERNAL_CALL_SlerpUnclamped_m6_251,
	Quaternion_Lerp_m6_252,
	Quaternion_INTERNAL_CALL_Lerp_m6_253,
	Quaternion_RotateTowards_m6_254,
	Quaternion_Inverse_m6_255,
	Quaternion_INTERNAL_CALL_Inverse_m6_256,
	Quaternion_ToString_m6_257,
	Quaternion_Angle_m6_258,
	Quaternion_get_eulerAngles_m6_259,
	Quaternion_Euler_m6_260,
	Quaternion_Internal_ToEulerRad_m6_261,
	Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m6_262,
	Quaternion_Internal_FromEulerRad_m6_263,
	Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6_264,
	Quaternion_GetHashCode_m6_265,
	Quaternion_Equals_m6_266,
	Quaternion_op_Multiply_m6_267,
	Quaternion_op_Multiply_m6_268,
	Quaternion_op_Inequality_m6_269,
	Rect__ctor_m6_270,
	Rect__ctor_m6_271,
	Rect_MinMaxRect_m6_272,
	Rect_get_x_m6_273,
	Rect_set_x_m6_274,
	Rect_get_y_m6_275,
	Rect_set_y_m6_276,
	Rect_get_width_m6_277,
	Rect_set_width_m6_278,
	Rect_get_height_m6_279,
	Rect_set_height_m6_280,
	Rect_get_xMin_m6_281,
	Rect_get_yMin_m6_282,
	Rect_get_xMax_m6_283,
	Rect_get_yMax_m6_284,
	Rect_ToString_m6_285,
	Rect_Contains_m6_286,
	Rect_Contains_m6_287,
	Rect_GetHashCode_m6_288,
	Rect_Equals_m6_289,
	Rect_op_Equality_m6_290,
	Matrix4x4_get_Item_m6_291,
	Matrix4x4_set_Item_m6_292,
	Matrix4x4_get_Item_m6_293,
	Matrix4x4_set_Item_m6_294,
	Matrix4x4_GetHashCode_m6_295,
	Matrix4x4_Equals_m6_296,
	Matrix4x4_Inverse_m6_297,
	Matrix4x4_INTERNAL_CALL_Inverse_m6_298,
	Matrix4x4_Transpose_m6_299,
	Matrix4x4_INTERNAL_CALL_Transpose_m6_300,
	Matrix4x4_Invert_m6_301,
	Matrix4x4_INTERNAL_CALL_Invert_m6_302,
	Matrix4x4_get_inverse_m6_303,
	Matrix4x4_get_transpose_m6_304,
	Matrix4x4_get_isIdentity_m6_305,
	Matrix4x4_GetColumn_m6_306,
	Matrix4x4_GetRow_m6_307,
	Matrix4x4_SetColumn_m6_308,
	Matrix4x4_SetRow_m6_309,
	Matrix4x4_MultiplyPoint_m6_310,
	Matrix4x4_MultiplyPoint3x4_m6_311,
	Matrix4x4_MultiplyVector_m6_312,
	Matrix4x4_Scale_m6_313,
	Matrix4x4_get_zero_m6_314,
	Matrix4x4_get_identity_m6_315,
	Matrix4x4_SetTRS_m6_316,
	Matrix4x4_TRS_m6_317,
	Matrix4x4_INTERNAL_CALL_TRS_m6_318,
	Matrix4x4_ToString_m6_319,
	Matrix4x4_ToString_m6_320,
	Matrix4x4_Ortho_m6_321,
	Matrix4x4_Perspective_m6_322,
	Matrix4x4_op_Multiply_m6_323,
	Matrix4x4_op_Multiply_m6_324,
	Matrix4x4_op_Equality_m6_325,
	Matrix4x4_op_Inequality_m6_326,
	Bounds__ctor_m6_327,
	Bounds_GetHashCode_m6_328,
	Bounds_Equals_m6_329,
	Bounds_get_center_m6_330,
	Bounds_set_center_m6_331,
	Bounds_get_size_m6_332,
	Bounds_set_size_m6_333,
	Bounds_get_extents_m6_334,
	Bounds_set_extents_m6_335,
	Bounds_get_min_m6_336,
	Bounds_set_min_m6_337,
	Bounds_get_max_m6_338,
	Bounds_set_max_m6_339,
	Bounds_SetMinMax_m6_340,
	Bounds_Encapsulate_m6_341,
	Bounds_Encapsulate_m6_342,
	Bounds_Expand_m6_343,
	Bounds_Expand_m6_344,
	Bounds_Intersects_m6_345,
	Bounds_Internal_Contains_m6_346,
	Bounds_INTERNAL_CALL_Internal_Contains_m6_347,
	Bounds_Contains_m6_348,
	Bounds_Internal_SqrDistance_m6_349,
	Bounds_INTERNAL_CALL_Internal_SqrDistance_m6_350,
	Bounds_SqrDistance_m6_351,
	Bounds_Internal_IntersectRay_m6_352,
	Bounds_INTERNAL_CALL_Internal_IntersectRay_m6_353,
	Bounds_IntersectRay_m6_354,
	Bounds_IntersectRay_m6_355,
	Bounds_Internal_GetClosestPoint_m6_356,
	Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6_357,
	Bounds_ClosestPoint_m6_358,
	Bounds_ToString_m6_359,
	Bounds_ToString_m6_360,
	Bounds_op_Equality_m6_361,
	Bounds_op_Inequality_m6_362,
	Vector4__ctor_m6_363,
	Vector4_GetHashCode_m6_364,
	Vector4_Equals_m6_365,
	Vector4_ToString_m6_366,
	Vector4_Dot_m6_367,
	Vector4_SqrMagnitude_m6_368,
	Vector4_op_Subtraction_m6_369,
	Vector4_op_Equality_m6_370,
	Ray__ctor_m6_371,
	Ray_get_origin_m6_372,
	Ray_get_direction_m6_373,
	Ray_GetPoint_m6_374,
	Ray_ToString_m6_375,
	MathfInternal__cctor_m6_376,
	Mathf__cctor_m6_377,
	Mathf_Acos_m6_378,
	Mathf_Sqrt_m6_379,
	Mathf_Abs_m6_380,
	Mathf_Min_m6_381,
	Mathf_Min_m6_382,
	Mathf_Max_m6_383,
	Mathf_Max_m6_384,
	Mathf_Floor_m6_385,
	Mathf_Round_m6_386,
	Mathf_FloorToInt_m6_387,
	Mathf_Sign_m6_388,
	Mathf_Clamp_m6_389,
	Mathf_Clamp_m6_390,
	Mathf_Clamp01_m6_391,
	Mathf_Lerp_m6_392,
	Mathf_MoveTowards_m6_393,
	Mathf_Approximately_m6_394,
	Mathf_SmoothDamp_m6_395,
	Mathf_SmoothDamp_m6_396,
	Mathf_SmoothDampAngle_m6_397,
	Mathf_SmoothDampAngle_m6_398,
	Mathf_Repeat_m6_399,
	Mathf_DeltaAngle_m6_400,
	ReapplyDrivenProperties__ctor_m6_401,
	ReapplyDrivenProperties_Invoke_m6_402,
	ReapplyDrivenProperties_BeginInvoke_m6_403,
	ReapplyDrivenProperties_EndInvoke_m6_404,
	RectTransform_SendReapplyDrivenProperties_m6_405,
	ResourceRequest__ctor_m6_406,
	ResourceRequest_get_asset_m6_407,
	Resources_FindObjectsOfTypeAll_m6_408,
	Resources_Load_m6_409,
	SerializePrivateVariables__ctor_m6_410,
	SerializeField__ctor_m6_411,
	Shader_PropertyToID_m6_412,
	Material__ctor_m6_413,
	Material_set_color_m6_414,
	Material_SetColor_m6_415,
	Material_SetColor_m6_416,
	Material_INTERNAL_CALL_SetColor_m6_417,
	Material_Internal_CreateWithMaterial_m6_418,
	SphericalHarmonicsL2_Clear_m6_419,
	SphericalHarmonicsL2_ClearInternal_m6_420,
	SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6_421,
	SphericalHarmonicsL2_AddAmbientLight_m6_422,
	SphericalHarmonicsL2_AddAmbientLightInternal_m6_423,
	SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6_424,
	SphericalHarmonicsL2_AddDirectionalLight_m6_425,
	SphericalHarmonicsL2_AddDirectionalLightInternal_m6_426,
	SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6_427,
	SphericalHarmonicsL2_get_Item_m6_428,
	SphericalHarmonicsL2_set_Item_m6_429,
	SphericalHarmonicsL2_GetHashCode_m6_430,
	SphericalHarmonicsL2_Equals_m6_431,
	SphericalHarmonicsL2_op_Multiply_m6_432,
	SphericalHarmonicsL2_op_Multiply_m6_433,
	SphericalHarmonicsL2_op_Addition_m6_434,
	SphericalHarmonicsL2_op_Equality_m6_435,
	SphericalHarmonicsL2_op_Inequality_m6_436,
	UnityString_Format_m6_437,
	AsyncOperation__ctor_m6_438,
	AsyncOperation_InternalDestroy_m6_439,
	AsyncOperation_Finalize_m6_440,
	LogCallback__ctor_m6_441,
	LogCallback_Invoke_m6_442,
	LogCallback_BeginInvoke_m6_443,
	LogCallback_EndInvoke_m6_444,
	Application_Quit_m6_445,
	Application_get_loadedLevel_m6_446,
	Application_get_loadedLevelName_m6_447,
	Application_LoadLevel_m6_448,
	Application_LoadLevel_m6_449,
	Application_LoadLevelAsync_m6_450,
	Application_get_isPlaying_m6_451,
	Application_get_platform_m6_452,
	Application_set_runInBackground_m6_453,
	Application_OpenURL_m6_454,
	Application_CallLogCallback_m6_455,
	Behaviour__ctor_m6_456,
	Behaviour_get_enabled_m6_457,
	Behaviour_set_enabled_m6_458,
	CameraCallback__ctor_m6_459,
	CameraCallback_Invoke_m6_460,
	CameraCallback_BeginInvoke_m6_461,
	CameraCallback_EndInvoke_m6_462,
	Camera_get_nearClipPlane_m6_463,
	Camera_get_farClipPlane_m6_464,
	Camera_get_cullingMask_m6_465,
	Camera_get_eventMask_m6_466,
	Camera_get_pixelRect_m6_467,
	Camera_INTERNAL_get_pixelRect_m6_468,
	Camera_get_targetTexture_m6_469,
	Camera_get_clearFlags_m6_470,
	Camera_ViewportPointToRay_m6_471,
	Camera_INTERNAL_CALL_ViewportPointToRay_m6_472,
	Camera_ScreenPointToRay_m6_473,
	Camera_INTERNAL_CALL_ScreenPointToRay_m6_474,
	Camera_get_main_m6_475,
	Camera_get_allCamerasCount_m6_476,
	Camera_GetAllCameras_m6_477,
	Camera_FireOnPreCull_m6_478,
	Camera_FireOnPreRender_m6_479,
	Camera_FireOnPostRender_m6_480,
	Camera_RaycastTry_m6_481,
	Camera_INTERNAL_CALL_RaycastTry_m6_482,
	Camera_RaycastTry2D_m6_483,
	Camera_INTERNAL_CALL_RaycastTry2D_m6_484,
	Debug_DrawLine_m6_485,
	Debug_DrawLine_m6_486,
	Debug_INTERNAL_CALL_DrawLine_m6_487,
	Debug_Internal_Log_m6_488,
	Debug_Log_m6_489,
	Debug_LogError_m6_490,
	Debug_LogWarning_m6_491,
	Debug_LogWarning_m6_492,
	DisplaysUpdatedDelegate__ctor_m6_493,
	DisplaysUpdatedDelegate_Invoke_m6_494,
	DisplaysUpdatedDelegate_BeginInvoke_m6_495,
	DisplaysUpdatedDelegate_EndInvoke_m6_496,
	Display__ctor_m6_497,
	Display__ctor_m6_498,
	Display__cctor_m6_499,
	Display_add_onDisplaysUpdated_m6_500,
	Display_remove_onDisplaysUpdated_m6_501,
	Display_get_renderingWidth_m6_502,
	Display_get_renderingHeight_m6_503,
	Display_get_systemWidth_m6_504,
	Display_get_systemHeight_m6_505,
	Display_get_colorBuffer_m6_506,
	Display_get_depthBuffer_m6_507,
	Display_Activate_m6_508,
	Display_Activate_m6_509,
	Display_SetParams_m6_510,
	Display_SetRenderingResolution_m6_511,
	Display_MultiDisplayLicense_m6_512,
	Display_RelativeMouseAt_m6_513,
	Display_get_main_m6_514,
	Display_RecreateDisplayList_m6_515,
	Display_FireDisplaysUpdated_m6_516,
	Display_GetSystemExtImpl_m6_517,
	Display_GetRenderingExtImpl_m6_518,
	Display_GetRenderingBuffersImpl_m6_519,
	Display_SetRenderingResolutionImpl_m6_520,
	Display_ActivateDisplayImpl_m6_521,
	Display_SetParamsImpl_m6_522,
	Display_MultiDisplayLicenseImpl_m6_523,
	Display_RelativeMouseAtImpl_m6_524,
	MonoBehaviour__ctor_m6_525,
	MonoBehaviour_Internal_CancelInvokeAll_m6_526,
	MonoBehaviour_Invoke_m6_527,
	MonoBehaviour_InvokeRepeating_m6_528,
	MonoBehaviour_CancelInvoke_m6_529,
	MonoBehaviour_StartCoroutine_m6_530,
	MonoBehaviour_StartCoroutine_Auto_m6_531,
	Touch_get_position_m6_532,
	Touch_get_phase_m6_533,
	Input__cctor_m6_534,
	Input_GetKeyInt_m6_535,
	Input_GetKeyDownInt_m6_536,
	Input_GetAxis_m6_537,
	Input_GetAxisRaw_m6_538,
	Input_GetButton_m6_539,
	Input_GetButtonDown_m6_540,
	Input_GetKey_m6_541,
	Input_GetKeyDown_m6_542,
	Input_GetMouseButton_m6_543,
	Input_GetMouseButtonDown_m6_544,
	Input_GetMouseButtonUp_m6_545,
	Input_get_mousePosition_m6_546,
	Input_INTERNAL_get_mousePosition_m6_547,
	Input_GetTouch_m6_548,
	Input_get_touchCount_m6_549,
	Input_get_compositionString_m6_550,
	Input_set_compositionCursorPos_m6_551,
	Input_INTERNAL_set_compositionCursorPos_m6_552,
	Object__ctor_m6_553,
	Object_Internal_CloneSingle_m6_554,
	Object_Internal_InstantiateSingle_m6_555,
	Object_INTERNAL_CALL_Internal_InstantiateSingle_m6_556,
	Object_Destroy_m6_557,
	Object_Destroy_m6_558,
	Object_DestroyImmediate_m6_559,
	Object_DestroyImmediate_m6_560,
	Object_FindObjectsOfType_m6_561,
	Object_get_name_m6_562,
	Object_set_name_m6_563,
	Object_DontDestroyOnLoad_m6_564,
	Object_set_hideFlags_m6_565,
	Object_DestroyObject_m6_566,
	Object_DestroyObject_m6_567,
	Object_ToString_m6_568,
	Object_Equals_m6_569,
	Object_GetHashCode_m6_570,
	Object_CompareBaseObjects_m6_571,
	Object_IsNativeObjectAlive_m6_572,
	Object_GetInstanceID_m6_573,
	Object_GetCachedPtr_m6_574,
	Object_Instantiate_m6_575,
	Object_CheckNullArgument_m6_576,
	Object_FindObjectOfType_m6_577,
	Object_op_Implicit_m6_578,
	Object_op_Equality_m6_579,
	Object_op_Inequality_m6_580,
	Component__ctor_m6_581,
	Component_get_transform_m6_582,
	Component_get_gameObject_m6_583,
	Component_GetComponentFastPath_m6_584,
	Component_GetComponentInChildren_m6_585,
	Component_SendMessage_m6_586,
	Component_SendMessage_m6_587,
	Component_SendMessage_m6_588,
	GameObject__ctor_m6_589,
	GameObject__ctor_m6_590,
	GameObject_GetComponent_m6_591,
	GameObject_GetComponentFastPath_m6_592,
	GameObject_GetComponentInChildren_m6_593,
	GameObject_GetComponentsInternal_m6_594,
	GameObject_get_transform_m6_595,
	GameObject_SetActive_m6_596,
	GameObject_get_activeInHierarchy_m6_597,
	GameObject_set_tag_m6_598,
	GameObject_FindGameObjectsWithTag_m6_599,
	GameObject_SendMessage_m6_600,
	GameObject_SendMessage_m6_601,
	GameObject_SendMessage_m6_602,
	GameObject_Internal_AddComponentWithType_m6_603,
	GameObject_AddComponent_m6_604,
	GameObject_Internal_CreateGameObject_m6_605,
	GameObject_Find_m6_606,
	Enumerator__ctor_m6_607,
	Enumerator_get_Current_m6_608,
	Enumerator_MoveNext_m6_609,
	Enumerator_Reset_m6_610,
	Transform_get_position_m6_611,
	Transform_set_position_m6_612,
	Transform_INTERNAL_get_position_m6_613,
	Transform_INTERNAL_set_position_m6_614,
	Transform_get_localPosition_m6_615,
	Transform_set_localPosition_m6_616,
	Transform_INTERNAL_get_localPosition_m6_617,
	Transform_INTERNAL_set_localPosition_m6_618,
	Transform_get_eulerAngles_m6_619,
	Transform_get_right_m6_620,
	Transform_get_up_m6_621,
	Transform_get_forward_m6_622,
	Transform_get_rotation_m6_623,
	Transform_set_rotation_m6_624,
	Transform_INTERNAL_get_rotation_m6_625,
	Transform_INTERNAL_set_rotation_m6_626,
	Transform_get_localRotation_m6_627,
	Transform_set_localRotation_m6_628,
	Transform_INTERNAL_get_localRotation_m6_629,
	Transform_INTERNAL_set_localRotation_m6_630,
	Transform_get_localScale_m6_631,
	Transform_set_localScale_m6_632,
	Transform_INTERNAL_get_localScale_m6_633,
	Transform_INTERNAL_set_localScale_m6_634,
	Transform_get_parent_m6_635,
	Transform_set_parent_m6_636,
	Transform_get_parentInternal_m6_637,
	Transform_set_parentInternal_m6_638,
	Transform_Rotate_m6_639,
	Transform_Rotate_m6_640,
	Transform_Rotate_m6_641,
	Transform_LookAt_m6_642,
	Transform_LookAt_m6_643,
	Transform_LookAt_m6_644,
	Transform_INTERNAL_CALL_LookAt_m6_645,
	Transform_TransformDirection_m6_646,
	Transform_INTERNAL_CALL_TransformDirection_m6_647,
	Transform_get_root_m6_648,
	Transform_get_childCount_m6_649,
	Transform_GetEnumerator_m6_650,
	Transform_GetChild_m6_651,
	Time_get_time_m6_652,
	Time_get_timeSinceLevelLoad_m6_653,
	Time_get_deltaTime_m6_654,
	Time_get_frameCount_m6_655,
	Time_get_realtimeSinceStartup_m6_656,
	Random_Range_m6_657,
	Random_Range_m6_658,
	Random_RandomRangeInt_m6_659,
	Random_get_insideUnitSphere_m6_660,
	Random_INTERNAL_get_insideUnitSphere_m6_661,
	YieldInstruction__ctor_m6_662,
	PlayerPrefsException__ctor_m6_663,
	PlayerPrefs_TrySetSetString_m6_664,
	PlayerPrefs_SetString_m6_665,
	PlayerPrefs_GetString_m6_666,
	PlayerPrefs_GetString_m6_667,
	PlayerPrefs_DeleteKey_m6_668,
	UnityAdsInternal__ctor_m6_669,
	UnityAdsInternal_add_onCampaignsAvailable_m6_670,
	UnityAdsInternal_remove_onCampaignsAvailable_m6_671,
	UnityAdsInternal_add_onCampaignsFetchFailed_m6_672,
	UnityAdsInternal_remove_onCampaignsFetchFailed_m6_673,
	UnityAdsInternal_add_onShow_m6_674,
	UnityAdsInternal_remove_onShow_m6_675,
	UnityAdsInternal_add_onHide_m6_676,
	UnityAdsInternal_remove_onHide_m6_677,
	UnityAdsInternal_add_onVideoCompleted_m6_678,
	UnityAdsInternal_remove_onVideoCompleted_m6_679,
	UnityAdsInternal_add_onVideoStarted_m6_680,
	UnityAdsInternal_remove_onVideoStarted_m6_681,
	UnityAdsInternal_RegisterNative_m6_682,
	UnityAdsInternal_Init_m6_683,
	UnityAdsInternal_Show_m6_684,
	UnityAdsInternal_CanShowAds_m6_685,
	UnityAdsInternal_SetLogLevel_m6_686,
	UnityAdsInternal_SetCampaignDataURL_m6_687,
	UnityAdsInternal_RemoveAllEventHandlers_m6_688,
	UnityAdsInternal_CallUnityAdsCampaignsAvailable_m6_689,
	UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m6_690,
	UnityAdsInternal_CallUnityAdsShow_m6_691,
	UnityAdsInternal_CallUnityAdsHide_m6_692,
	UnityAdsInternal_CallUnityAdsVideoCompleted_m6_693,
	UnityAdsInternal_CallUnityAdsVideoStarted_m6_694,
	Particle_get_position_m6_695,
	Particle_set_position_m6_696,
	Particle_get_velocity_m6_697,
	Particle_set_velocity_m6_698,
	Particle_get_energy_m6_699,
	Particle_set_energy_m6_700,
	Particle_get_startEnergy_m6_701,
	Particle_set_startEnergy_m6_702,
	Particle_get_size_m6_703,
	Particle_set_size_m6_704,
	Particle_get_rotation_m6_705,
	Particle_set_rotation_m6_706,
	Particle_get_angularVelocity_m6_707,
	Particle_set_angularVelocity_m6_708,
	Particle_get_color_m6_709,
	Particle_set_color_m6_710,
	ControllerColliderHit_get_moveDirection_m6_711,
	Physics_Raycast_m6_712,
	Physics_Raycast_m6_713,
	Physics_Raycast_m6_714,
	Physics_Internal_Raycast_m6_715,
	Physics_INTERNAL_CALL_Internal_Raycast_m6_716,
	Rigidbody_get_velocity_m6_717,
	Rigidbody_set_velocity_m6_718,
	Rigidbody_INTERNAL_get_velocity_m6_719,
	Rigidbody_INTERNAL_set_velocity_m6_720,
	Rigidbody_get_angularVelocity_m6_721,
	Rigidbody_set_angularVelocity_m6_722,
	Rigidbody_INTERNAL_get_angularVelocity_m6_723,
	Rigidbody_INTERNAL_set_angularVelocity_m6_724,
	Rigidbody_set_isKinematic_m6_725,
	Rigidbody_AddForce_m6_726,
	Rigidbody_INTERNAL_CALL_AddForce_m6_727,
	Collider_get_bounds_m6_728,
	Collider_INTERNAL_get_bounds_m6_729,
	RaycastHit_get_point_m6_730,
	RaycastHit_get_collider_m6_731,
	CharacterController_Move_m6_732,
	CharacterController_INTERNAL_CALL_Move_m6_733,
	Physics2D__cctor_m6_734,
	Physics2D_Internal_Raycast_m6_735,
	Physics2D_INTERNAL_CALL_Internal_Raycast_m6_736,
	Physics2D_Raycast_m6_737,
	Physics2D_Raycast_m6_738,
	RaycastHit2D_get_collider_m6_739,
	Rigidbody2D_get_velocity_m6_740,
	Rigidbody2D_set_velocity_m6_741,
	Rigidbody2D_INTERNAL_get_velocity_m6_742,
	Rigidbody2D_INTERNAL_set_velocity_m6_743,
	Rigidbody2D_get_angularVelocity_m6_744,
	Rigidbody2D_set_angularVelocity_m6_745,
	Rigidbody2D_set_isKinematic_m6_746,
	Rigidbody2D_AddForce_m6_747,
	Rigidbody2D_INTERNAL_CALL_AddForce_m6_748,
	AudioConfigurationChangeHandler__ctor_m6_749,
	AudioConfigurationChangeHandler_Invoke_m6_750,
	AudioConfigurationChangeHandler_BeginInvoke_m6_751,
	AudioConfigurationChangeHandler_EndInvoke_m6_752,
	AudioSettings_InvokeOnAudioConfigurationChanged_m6_753,
	PCMReaderCallback__ctor_m6_754,
	PCMReaderCallback_Invoke_m6_755,
	PCMReaderCallback_BeginInvoke_m6_756,
	PCMReaderCallback_EndInvoke_m6_757,
	PCMSetPositionCallback__ctor_m6_758,
	PCMSetPositionCallback_Invoke_m6_759,
	PCMSetPositionCallback_BeginInvoke_m6_760,
	PCMSetPositionCallback_EndInvoke_m6_761,
	AudioClip_InvokePCMReaderCallback_Internal_m6_762,
	AudioClip_InvokePCMSetPositionCallback_Internal_m6_763,
	AudioSource_set_clip_m6_764,
	AudioSource_Play_m6_765,
	AudioSource_Play_m6_766,
	WebCamDevice_get_name_m6_767,
	WebCamDevice_get_isFrontFacing_m6_768,
	AnimationEvent__ctor_m6_769,
	AnimationEvent_get_data_m6_770,
	AnimationEvent_set_data_m6_771,
	AnimationEvent_get_stringParameter_m6_772,
	AnimationEvent_set_stringParameter_m6_773,
	AnimationEvent_get_floatParameter_m6_774,
	AnimationEvent_set_floatParameter_m6_775,
	AnimationEvent_get_intParameter_m6_776,
	AnimationEvent_set_intParameter_m6_777,
	AnimationEvent_get_objectReferenceParameter_m6_778,
	AnimationEvent_set_objectReferenceParameter_m6_779,
	AnimationEvent_get_functionName_m6_780,
	AnimationEvent_set_functionName_m6_781,
	AnimationEvent_get_time_m6_782,
	AnimationEvent_set_time_m6_783,
	AnimationEvent_get_messageOptions_m6_784,
	AnimationEvent_set_messageOptions_m6_785,
	AnimationEvent_get_isFiredByLegacy_m6_786,
	AnimationEvent_get_isFiredByAnimator_m6_787,
	AnimationEvent_get_animationState_m6_788,
	AnimationEvent_get_animatorStateInfo_m6_789,
	AnimationEvent_get_animatorClipInfo_m6_790,
	AnimationEvent_GetHash_m6_791,
	Keyframe__ctor_m6_792,
	AnimationCurve__ctor_m6_793,
	AnimationCurve__ctor_m6_794,
	AnimationCurve_Cleanup_m6_795,
	AnimationCurve_Finalize_m6_796,
	AnimationCurve_Init_m6_797,
	Enumerator__ctor_m6_798,
	Enumerator_get_Current_m6_799,
	Enumerator_MoveNext_m6_800,
	Enumerator_Reset_m6_801,
	Animation_get_Item_m6_802,
	Animation_CrossFade_m6_803,
	Animation_CrossFade_m6_804,
	Animation_GetEnumerator_m6_805,
	Animation_GetState_m6_806,
	Animation_GetStateAtIndex_m6_807,
	Animation_GetStateCount_m6_808,
	AnimationState_set_wrapMode_m6_809,
	AnimationState_set_time_m6_810,
	AnimationState_set_speed_m6_811,
	AnimatorStateInfo_IsName_m6_812,
	AnimatorStateInfo_get_fullPathHash_m6_813,
	AnimatorStateInfo_get_nameHash_m6_814,
	AnimatorStateInfo_get_shortNameHash_m6_815,
	AnimatorStateInfo_get_normalizedTime_m6_816,
	AnimatorStateInfo_get_length_m6_817,
	AnimatorStateInfo_get_speed_m6_818,
	AnimatorStateInfo_get_speedMultiplier_m6_819,
	AnimatorStateInfo_get_tagHash_m6_820,
	AnimatorStateInfo_IsTag_m6_821,
	AnimatorStateInfo_get_loop_m6_822,
	AnimatorTransitionInfo_IsName_m6_823,
	AnimatorTransitionInfo_IsUserName_m6_824,
	AnimatorTransitionInfo_get_fullPathHash_m6_825,
	AnimatorTransitionInfo_get_nameHash_m6_826,
	AnimatorTransitionInfo_get_userNameHash_m6_827,
	AnimatorTransitionInfo_get_normalizedTime_m6_828,
	AnimatorTransitionInfo_get_anyState_m6_829,
	AnimatorTransitionInfo_get_entry_m6_830,
	AnimatorTransitionInfo_get_exit_m6_831,
	Animator_GetFloat_m6_832,
	Animator_SetFloat_m6_833,
	Animator_SetFloat_m6_834,
	Animator_GetBool_m6_835,
	Animator_SetBool_m6_836,
	Animator_GetInteger_m6_837,
	Animator_SetInteger_m6_838,
	Animator_SetTrigger_m6_839,
	Animator_set_applyRootMotion_m6_840,
	Animator_get_layerCount_m6_841,
	Animator_GetLayerWeight_m6_842,
	Animator_SetLayerWeight_m6_843,
	Animator_GetCurrentAnimatorStateInfo_m6_844,
	Animator_StringToHash_m6_845,
	Animator_SetFloatString_m6_846,
	Animator_GetFloatString_m6_847,
	Animator_SetBoolString_m6_848,
	Animator_GetBoolString_m6_849,
	Animator_SetIntegerString_m6_850,
	Animator_GetIntegerString_m6_851,
	Animator_SetTriggerString_m6_852,
	Animator_SetFloatStringDamp_m6_853,
	HumanBone_get_boneName_m6_854,
	HumanBone_set_boneName_m6_855,
	HumanBone_get_humanName_m6_856,
	HumanBone_set_humanName_m6_857,
	TextMesh_set_text_m6_858,
	TextMesh_set_font_m6_859,
	TextMesh_set_anchor_m6_860,
	TextMesh_set_characterSize_m6_861,
	CharacterInfo_get_advance_m6_862,
	CharacterInfo_set_advance_m6_863,
	CharacterInfo_get_glyphWidth_m6_864,
	CharacterInfo_set_glyphWidth_m6_865,
	CharacterInfo_get_glyphHeight_m6_866,
	CharacterInfo_set_glyphHeight_m6_867,
	CharacterInfo_get_bearing_m6_868,
	CharacterInfo_set_bearing_m6_869,
	CharacterInfo_get_minY_m6_870,
	CharacterInfo_set_minY_m6_871,
	CharacterInfo_get_maxY_m6_872,
	CharacterInfo_set_maxY_m6_873,
	CharacterInfo_get_minX_m6_874,
	CharacterInfo_set_minX_m6_875,
	CharacterInfo_get_maxX_m6_876,
	CharacterInfo_set_maxX_m6_877,
	CharacterInfo_get_uvBottomLeftUnFlipped_m6_878,
	CharacterInfo_set_uvBottomLeftUnFlipped_m6_879,
	CharacterInfo_get_uvBottomRightUnFlipped_m6_880,
	CharacterInfo_set_uvBottomRightUnFlipped_m6_881,
	CharacterInfo_get_uvTopRightUnFlipped_m6_882,
	CharacterInfo_set_uvTopRightUnFlipped_m6_883,
	CharacterInfo_get_uvTopLeftUnFlipped_m6_884,
	CharacterInfo_set_uvTopLeftUnFlipped_m6_885,
	CharacterInfo_get_uvBottomLeft_m6_886,
	CharacterInfo_set_uvBottomLeft_m6_887,
	CharacterInfo_get_uvBottomRight_m6_888,
	CharacterInfo_set_uvBottomRight_m6_889,
	CharacterInfo_get_uvTopRight_m6_890,
	CharacterInfo_set_uvTopRight_m6_891,
	CharacterInfo_get_uvTopLeft_m6_892,
	CharacterInfo_set_uvTopLeft_m6_893,
	FontTextureRebuildCallback__ctor_m6_894,
	FontTextureRebuildCallback_Invoke_m6_895,
	FontTextureRebuildCallback_BeginInvoke_m6_896,
	FontTextureRebuildCallback_EndInvoke_m6_897,
	Font__ctor_m6_898,
	Font__ctor_m6_899,
	Font__ctor_m6_900,
	Font_add_textureRebuilt_m6_901,
	Font_remove_textureRebuilt_m6_902,
	Font_add_m_FontTextureRebuildCallback_m6_903,
	Font_remove_m_FontTextureRebuildCallback_m6_904,
	Font_GetOSInstalledFontNames_m6_905,
	Font_Internal_CreateFont_m6_906,
	Font_Internal_CreateDynamicFont_m6_907,
	Font_CreateDynamicFontFromOSFont_m6_908,
	Font_CreateDynamicFontFromOSFont_m6_909,
	Font_get_material_m6_910,
	Font_set_material_m6_911,
	Font_HasCharacter_m6_912,
	Font_get_fontNames_m6_913,
	Font_set_fontNames_m6_914,
	Font_get_characterInfo_m6_915,
	Font_set_characterInfo_m6_916,
	Font_RequestCharactersInTexture_m6_917,
	Font_RequestCharactersInTexture_m6_918,
	Font_RequestCharactersInTexture_m6_919,
	Font_InvokeTextureRebuilt_Internal_m6_920,
	Font_get_textureRebuildCallback_m6_921,
	Font_set_textureRebuildCallback_m6_922,
	Font_GetMaxVertsForString_m6_923,
	Font_GetCharacterInfo_m6_924,
	Font_GetCharacterInfo_m6_925,
	Font_GetCharacterInfo_m6_926,
	Font_get_dynamic_m6_927,
	Font_get_ascent_m6_928,
	Font_get_lineHeight_m6_929,
	Font_get_fontSize_m6_930,
	TextGenerator__ctor_m6_931,
	TextGenerator__ctor_m6_932,
	TextGenerator_System_IDisposable_Dispose_m6_933,
	TextGenerator_Init_m6_934,
	TextGenerator_Dispose_cpp_m6_935,
	TextGenerator_Populate_Internal_m6_936,
	TextGenerator_Populate_Internal_cpp_m6_937,
	TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6_938,
	TextGenerator_get_rectExtents_m6_939,
	TextGenerator_INTERNAL_get_rectExtents_m6_940,
	TextGenerator_get_vertexCount_m6_941,
	TextGenerator_GetVerticesInternal_m6_942,
	TextGenerator_GetVerticesArray_m6_943,
	TextGenerator_get_characterCount_m6_944,
	TextGenerator_get_characterCountVisible_m6_945,
	TextGenerator_GetCharactersInternal_m6_946,
	TextGenerator_GetCharactersArray_m6_947,
	TextGenerator_get_lineCount_m6_948,
	TextGenerator_GetLinesInternal_m6_949,
	TextGenerator_GetLinesArray_m6_950,
	TextGenerator_get_fontSizeUsedForBestFit_m6_951,
	TextGenerator_Finalize_m6_952,
	TextGenerator_ValidatedSettings_m6_953,
	TextGenerator_Invalidate_m6_954,
	TextGenerator_GetCharacters_m6_955,
	TextGenerator_GetLines_m6_956,
	TextGenerator_GetVertices_m6_957,
	TextGenerator_GetPreferredWidth_m6_958,
	TextGenerator_GetPreferredHeight_m6_959,
	TextGenerator_Populate_m6_960,
	TextGenerator_PopulateAlways_m6_961,
	TextGenerator_get_verts_m6_962,
	TextGenerator_get_characters_m6_963,
	TextGenerator_get_lines_m6_964,
	UIVertex__cctor_m6_965,
	Event__ctor_m6_966,
	Event__ctor_m6_967,
	Event__ctor_m6_968,
	Event_Finalize_m6_969,
	Event_get_mousePosition_m6_970,
	Event_set_mousePosition_m6_971,
	Event_get_delta_m6_972,
	Event_set_delta_m6_973,
	Event_get_mouseRay_m6_974,
	Event_set_mouseRay_m6_975,
	Event_get_shift_m6_976,
	Event_set_shift_m6_977,
	Event_get_control_m6_978,
	Event_set_control_m6_979,
	Event_get_alt_m6_980,
	Event_set_alt_m6_981,
	Event_get_command_m6_982,
	Event_set_command_m6_983,
	Event_get_capsLock_m6_984,
	Event_set_capsLock_m6_985,
	Event_get_numeric_m6_986,
	Event_set_numeric_m6_987,
	Event_get_functionKey_m6_988,
	Event_get_current_m6_989,
	Event_set_current_m6_990,
	Event_Internal_MakeMasterEventCurrent_m6_991,
	Event_get_isKey_m6_992,
	Event_get_isMouse_m6_993,
	Event_KeyboardEvent_m6_994,
	Event_GetHashCode_m6_995,
	Event_Equals_m6_996,
	Event_ToString_m6_997,
	Event_Init_m6_998,
	Event_Cleanup_m6_999,
	Event_InitCopy_m6_1000,
	Event_InitPtr_m6_1001,
	Event_get_rawType_m6_1002,
	Event_get_type_m6_1003,
	Event_set_type_m6_1004,
	Event_GetTypeForControl_m6_1005,
	Event_Internal_SetMousePosition_m6_1006,
	Event_INTERNAL_CALL_Internal_SetMousePosition_m6_1007,
	Event_Internal_GetMousePosition_m6_1008,
	Event_Internal_SetMouseDelta_m6_1009,
	Event_INTERNAL_CALL_Internal_SetMouseDelta_m6_1010,
	Event_Internal_GetMouseDelta_m6_1011,
	Event_get_button_m6_1012,
	Event_set_button_m6_1013,
	Event_get_modifiers_m6_1014,
	Event_set_modifiers_m6_1015,
	Event_get_pressure_m6_1016,
	Event_set_pressure_m6_1017,
	Event_get_clickCount_m6_1018,
	Event_set_clickCount_m6_1019,
	Event_get_character_m6_1020,
	Event_set_character_m6_1021,
	Event_get_commandName_m6_1022,
	Event_set_commandName_m6_1023,
	Event_get_keyCode_m6_1024,
	Event_set_keyCode_m6_1025,
	Event_Internal_SetNativeEvent_m6_1026,
	Event_Use_m6_1027,
	Event_PopEvent_m6_1028,
	Event_GetEventCount_m6_1029,
	ScrollViewState__ctor_m6_1030,
	WindowFunction__ctor_m6_1031,
	WindowFunction_Invoke_m6_1032,
	WindowFunction_BeginInvoke_m6_1033,
	WindowFunction_EndInvoke_m6_1034,
	GUI__cctor_m6_1035,
	GUI_get_nextScrollStepTime_m6_1036,
	GUI_set_nextScrollStepTime_m6_1037,
	GUI_get_scrollTroughSide_m6_1038,
	GUI_set_scrollTroughSide_m6_1039,
	GUI_set_skin_m6_1040,
	GUI_get_skin_m6_1041,
	GUI_DoSetSkin_m6_1042,
	GUI_set_tooltip_m6_1043,
	GUI_Label_m6_1044,
	GUI_Label_m6_1045,
	GUI_Label_m6_1046,
	GUI_Box_m6_1047,
	GUI_Box_m6_1048,
	GUI_Button_m6_1049,
	GUI_Button_m6_1050,
	GUI_DoRepeatButton_m6_1051,
	GUI_PasswordFieldGetStrToShow_m6_1052,
	GUI_DoTextField_m6_1053,
	GUI_DoTextField_m6_1054,
	GUI_DoTextField_m6_1055,
	GUI_HandleTextFieldEventForTouchscreen_m6_1056,
	GUI_HandleTextFieldEventForDesktop_m6_1057,
	GUI_Toggle_m6_1058,
	GUI_Toolbar_m6_1059,
	GUI_FindStyles_m6_1060,
	GUI_CalcTotalHorizSpacing_m6_1061,
	GUI_DoButtonGrid_m6_1062,
	GUI_CalcMouseRects_m6_1063,
	GUI_GetButtonGridMouseSelection_m6_1064,
	GUI_HorizontalSlider_m6_1065,
	GUI_Slider_m6_1066,
	GUI_HorizontalScrollbar_m6_1067,
	GUI_ScrollerRepeatButton_m6_1068,
	GUI_VerticalScrollbar_m6_1069,
	GUI_Scroller_m6_1070,
	GUI_BeginGroup_m6_1071,
	GUI_EndGroup_m6_1072,
	GUI_BeginScrollView_m6_1073,
	GUI_EndScrollView_m6_1074,
	GUI_Window_m6_1075,
	GUI_CallWindowDelegate_m6_1076,
	GUI_DragWindow_m6_1077,
	GUI_set_color_m6_1078,
	GUI_INTERNAL_set_color_m6_1079,
	GUI_get_changed_m6_1080,
	GUI_set_changed_m6_1081,
	GUI_get_enabled_m6_1082,
	GUI_Internal_SetTooltip_m6_1083,
	GUI_DoLabel_m6_1084,
	GUI_INTERNAL_CALL_DoLabel_m6_1085,
	GUI_DoButton_m6_1086,
	GUI_INTERNAL_CALL_DoButton_m6_1087,
	GUI_SetNextControlName_m6_1088,
	GUI_GetNameOfFocusedControl_m6_1089,
	GUI_FocusControl_m6_1090,
	GUI_DoToggle_m6_1091,
	GUI_INTERNAL_CALL_DoToggle_m6_1092,
	GUI_get_usePageScrollbars_m6_1093,
	GUI_InternalRepaintEditorWindow_m6_1094,
	GUI_DoWindow_m6_1095,
	GUI_INTERNAL_CALL_DoWindow_m6_1096,
	GUI_DragWindow_m6_1097,
	GUI_INTERNAL_CALL_DragWindow_m6_1098,
	GUIContent__ctor_m6_1099,
	GUIContent__ctor_m6_1100,
	GUIContent__ctor_m6_1101,
	GUIContent__cctor_m6_1102,
	GUIContent_get_text_m6_1103,
	GUIContent_set_text_m6_1104,
	GUIContent_get_tooltip_m6_1105,
	GUIContent_Temp_m6_1106,
	GUIContent_Temp_m6_1107,
	GUIContent_ClearStaticCache_m6_1108,
	GUIContent_Temp_m6_1109,
	LayoutedWindow__ctor_m6_1110,
	LayoutedWindow_DoWindow_m6_1111,
	GUILayout_Label_m6_1112,
	GUILayout_Label_m6_1113,
	GUILayout_DoLabel_m6_1114,
	GUILayout_Button_m6_1115,
	GUILayout_DoButton_m6_1116,
	GUILayout_TextField_m6_1117,
	GUILayout_DoTextField_m6_1118,
	GUILayout_Toggle_m6_1119,
	GUILayout_DoToggle_m6_1120,
	GUILayout_Toolbar_m6_1121,
	GUILayout_Toolbar_m6_1122,
	GUILayout_HorizontalSlider_m6_1123,
	GUILayout_DoHorizontalSlider_m6_1124,
	GUILayout_Space_m6_1125,
	GUILayout_FlexibleSpace_m6_1126,
	GUILayout_BeginHorizontal_m6_1127,
	GUILayout_BeginHorizontal_m6_1128,
	GUILayout_EndHorizontal_m6_1129,
	GUILayout_BeginVertical_m6_1130,
	GUILayout_BeginVertical_m6_1131,
	GUILayout_EndVertical_m6_1132,
	GUILayout_BeginArea_m6_1133,
	GUILayout_BeginArea_m6_1134,
	GUILayout_BeginArea_m6_1135,
	GUILayout_EndArea_m6_1136,
	GUILayout_BeginScrollView_m6_1137,
	GUILayout_BeginScrollView_m6_1138,
	GUILayout_EndScrollView_m6_1139,
	GUILayout_EndScrollView_m6_1140,
	GUILayout_Window_m6_1141,
	GUILayout_DoWindow_m6_1142,
	GUILayout_Width_m6_1143,
	GUILayout_MinWidth_m6_1144,
	GUILayout_Height_m6_1145,
	GUILayout_ExpandWidth_m6_1146,
	GUILayout_ExpandHeight_m6_1147,
	LayoutCache__ctor_m6_1148,
	GUILayoutUtility__cctor_m6_1149,
	GUILayoutUtility_SelectIDList_m6_1150,
	GUILayoutUtility_Begin_m6_1151,
	GUILayoutUtility_BeginWindow_m6_1152,
	GUILayoutUtility_EndGroup_m6_1153,
	GUILayoutUtility_Layout_m6_1154,
	GUILayoutUtility_LayoutFromEditorWindow_m6_1155,
	GUILayoutUtility_LayoutFreeGroup_m6_1156,
	GUILayoutUtility_LayoutSingleGroup_m6_1157,
	GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m6_1158,
	GUILayoutUtility_BeginLayoutGroup_m6_1159,
	GUILayoutUtility_EndLayoutGroup_m6_1160,
	GUILayoutUtility_BeginLayoutArea_m6_1161,
	GUILayoutUtility_GetRect_m6_1162,
	GUILayoutUtility_DoGetRect_m6_1163,
	GUILayoutUtility_GetRect_m6_1164,
	GUILayoutUtility_DoGetRect_m6_1165,
	GUILayoutUtility_get_spaceStyle_m6_1166,
	GUILayoutUtility_Internal_GetWindowRect_m6_1167,
	GUILayoutUtility_Internal_MoveWindow_m6_1168,
	GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6_1169,
	GUILayoutEntry__ctor_m6_1170,
	GUILayoutEntry__ctor_m6_1171,
	GUILayoutEntry__cctor_m6_1172,
	GUILayoutEntry_get_style_m6_1173,
	GUILayoutEntry_set_style_m6_1174,
	GUILayoutEntry_get_margin_m6_1175,
	GUILayoutEntry_CalcWidth_m6_1176,
	GUILayoutEntry_CalcHeight_m6_1177,
	GUILayoutEntry_SetHorizontal_m6_1178,
	GUILayoutEntry_SetVertical_m6_1179,
	GUILayoutEntry_ApplyStyleSettings_m6_1180,
	GUILayoutEntry_ApplyOptions_m6_1181,
	GUILayoutEntry_ToString_m6_1182,
	GUILayoutGroup__ctor_m6_1183,
	GUILayoutGroup_get_margin_m6_1184,
	GUILayoutGroup_ApplyOptions_m6_1185,
	GUILayoutGroup_ApplyStyleSettings_m6_1186,
	GUILayoutGroup_ResetCursor_m6_1187,
	GUILayoutGroup_GetNext_m6_1188,
	GUILayoutGroup_Add_m6_1189,
	GUILayoutGroup_CalcWidth_m6_1190,
	GUILayoutGroup_SetHorizontal_m6_1191,
	GUILayoutGroup_CalcHeight_m6_1192,
	GUILayoutGroup_SetVertical_m6_1193,
	GUILayoutGroup_ToString_m6_1194,
	GUIScrollGroup__ctor_m6_1195,
	GUIScrollGroup_CalcWidth_m6_1196,
	GUIScrollGroup_SetHorizontal_m6_1197,
	GUIScrollGroup_CalcHeight_m6_1198,
	GUIScrollGroup_SetVertical_m6_1199,
	GUIWordWrapSizer__ctor_m6_1200,
	GUIWordWrapSizer_CalcWidth_m6_1201,
	GUIWordWrapSizer_CalcHeight_m6_1202,
	GUILayoutOption__ctor_m6_1203,
	GUISettings__ctor_m6_1204,
	GUISettings_get_doubleClickSelectsWord_m6_1205,
	GUISettings_get_tripleClickSelectsLine_m6_1206,
	GUISettings_get_cursorColor_m6_1207,
	GUISettings_get_cursorFlashSpeed_m6_1208,
	GUISettings_get_selectionColor_m6_1209,
	GUISettings_Internal_GetCursorFlashSpeed_m6_1210,
	SkinChangedDelegate__ctor_m6_1211,
	SkinChangedDelegate_Invoke_m6_1212,
	SkinChangedDelegate_BeginInvoke_m6_1213,
	SkinChangedDelegate_EndInvoke_m6_1214,
	GUISkin__ctor_m6_1215,
	GUISkin_OnEnable_m6_1216,
	GUISkin_get_font_m6_1217,
	GUISkin_set_font_m6_1218,
	GUISkin_get_box_m6_1219,
	GUISkin_set_box_m6_1220,
	GUISkin_get_label_m6_1221,
	GUISkin_set_label_m6_1222,
	GUISkin_get_textField_m6_1223,
	GUISkin_set_textField_m6_1224,
	GUISkin_get_textArea_m6_1225,
	GUISkin_set_textArea_m6_1226,
	GUISkin_get_button_m6_1227,
	GUISkin_set_button_m6_1228,
	GUISkin_get_toggle_m6_1229,
	GUISkin_set_toggle_m6_1230,
	GUISkin_get_window_m6_1231,
	GUISkin_set_window_m6_1232,
	GUISkin_get_horizontalSlider_m6_1233,
	GUISkin_set_horizontalSlider_m6_1234,
	GUISkin_get_horizontalSliderThumb_m6_1235,
	GUISkin_set_horizontalSliderThumb_m6_1236,
	GUISkin_get_verticalSlider_m6_1237,
	GUISkin_set_verticalSlider_m6_1238,
	GUISkin_get_verticalSliderThumb_m6_1239,
	GUISkin_set_verticalSliderThumb_m6_1240,
	GUISkin_get_horizontalScrollbar_m6_1241,
	GUISkin_set_horizontalScrollbar_m6_1242,
	GUISkin_get_horizontalScrollbarThumb_m6_1243,
	GUISkin_set_horizontalScrollbarThumb_m6_1244,
	GUISkin_get_horizontalScrollbarLeftButton_m6_1245,
	GUISkin_set_horizontalScrollbarLeftButton_m6_1246,
	GUISkin_get_horizontalScrollbarRightButton_m6_1247,
	GUISkin_set_horizontalScrollbarRightButton_m6_1248,
	GUISkin_get_verticalScrollbar_m6_1249,
	GUISkin_set_verticalScrollbar_m6_1250,
	GUISkin_get_verticalScrollbarThumb_m6_1251,
	GUISkin_set_verticalScrollbarThumb_m6_1252,
	GUISkin_get_verticalScrollbarUpButton_m6_1253,
	GUISkin_set_verticalScrollbarUpButton_m6_1254,
	GUISkin_get_verticalScrollbarDownButton_m6_1255,
	GUISkin_set_verticalScrollbarDownButton_m6_1256,
	GUISkin_get_scrollView_m6_1257,
	GUISkin_set_scrollView_m6_1258,
	GUISkin_get_customStyles_m6_1259,
	GUISkin_set_customStyles_m6_1260,
	GUISkin_get_settings_m6_1261,
	GUISkin_get_error_m6_1262,
	GUISkin_Apply_m6_1263,
	GUISkin_BuildStyleCache_m6_1264,
	GUISkin_GetStyle_m6_1265,
	GUISkin_FindStyle_m6_1266,
	GUISkin_MakeCurrent_m6_1267,
	GUISkin_GetEnumerator_m6_1268,
	GUIStyleState__ctor_m6_1269,
	GUIStyleState__ctor_m6_1270,
	GUIStyleState_Finalize_m6_1271,
	GUIStyleState_Init_m6_1272,
	GUIStyleState_Cleanup_m6_1273,
	GUIStyleState_GetBackgroundInternal_m6_1274,
	GUIStyleState_set_textColor_m6_1275,
	GUIStyleState_INTERNAL_set_textColor_m6_1276,
	RectOffset__ctor_m6_1277,
	RectOffset__ctor_m6_1278,
	RectOffset__ctor_m6_1279,
	RectOffset_Finalize_m6_1280,
	RectOffset_ToString_m6_1281,
	RectOffset_Init_m6_1282,
	RectOffset_Cleanup_m6_1283,
	RectOffset_get_left_m6_1284,
	RectOffset_set_left_m6_1285,
	RectOffset_get_right_m6_1286,
	RectOffset_set_right_m6_1287,
	RectOffset_get_top_m6_1288,
	RectOffset_set_top_m6_1289,
	RectOffset_get_bottom_m6_1290,
	RectOffset_set_bottom_m6_1291,
	RectOffset_get_horizontal_m6_1292,
	RectOffset_get_vertical_m6_1293,
	RectOffset_Add_m6_1294,
	RectOffset_INTERNAL_CALL_Add_m6_1295,
	RectOffset_Remove_m6_1296,
	RectOffset_INTERNAL_CALL_Remove_m6_1297,
	GUIStyle__ctor_m6_1298,
	GUIStyle__ctor_m6_1299,
	GUIStyle__cctor_m6_1300,
	GUIStyle_Finalize_m6_1301,
	GUIStyle_get_normal_m6_1302,
	GUIStyle_get_margin_m6_1303,
	GUIStyle_get_padding_m6_1304,
	GUIStyle_set_padding_m6_1305,
	GUIStyle_get_font_m6_1306,
	GUIStyle_get_lineHeight_m6_1307,
	GUIStyle_Internal_Draw_m6_1308,
	GUIStyle_Draw_m6_1309,
	GUIStyle_Draw_m6_1310,
	GUIStyle_Draw_m6_1311,
	GUIStyle_Draw_m6_1312,
	GUIStyle_DrawCursor_m6_1313,
	GUIStyle_DrawWithTextSelection_m6_1314,
	GUIStyle_DrawWithTextSelection_m6_1315,
	GUIStyle_get_none_m6_1316,
	GUIStyle_GetCursorPixelPosition_m6_1317,
	GUIStyle_GetCursorStringIndex_m6_1318,
	GUIStyle_CalcSize_m6_1319,
	GUIStyle_CalcHeight_m6_1320,
	GUIStyle_get_isHeightDependantOnWidth_m6_1321,
	GUIStyle_CalcMinMaxWidth_m6_1322,
	GUIStyle_ToString_m6_1323,
	GUIStyle_Init_m6_1324,
	GUIStyle_InitCopy_m6_1325,
	GUIStyle_Cleanup_m6_1326,
	GUIStyle_get_name_m6_1327,
	GUIStyle_set_name_m6_1328,
	GUIStyle_GetStyleStatePtr_m6_1329,
	GUIStyle_GetRectOffsetPtr_m6_1330,
	GUIStyle_AssignRectOffset_m6_1331,
	GUIStyle_get_imagePosition_m6_1332,
	GUIStyle_set_alignment_m6_1333,
	GUIStyle_get_wordWrap_m6_1334,
	GUIStyle_set_wordWrap_m6_1335,
	GUIStyle_get_contentOffset_m6_1336,
	GUIStyle_set_contentOffset_m6_1337,
	GUIStyle_INTERNAL_get_contentOffset_m6_1338,
	GUIStyle_INTERNAL_set_contentOffset_m6_1339,
	GUIStyle_set_Internal_clipOffset_m6_1340,
	GUIStyle_INTERNAL_set_Internal_clipOffset_m6_1341,
	GUIStyle_get_fixedWidth_m6_1342,
	GUIStyle_get_fixedHeight_m6_1343,
	GUIStyle_get_stretchWidth_m6_1344,
	GUIStyle_set_stretchWidth_m6_1345,
	GUIStyle_get_stretchHeight_m6_1346,
	GUIStyle_set_stretchHeight_m6_1347,
	GUIStyle_Internal_GetLineHeight_m6_1348,
	GUIStyle_GetFontInternal_m6_1349,
	GUIStyle_Internal_Draw_m6_1350,
	GUIStyle_Internal_Draw2_m6_1351,
	GUIStyle_INTERNAL_CALL_Internal_Draw2_m6_1352,
	GUIStyle_Internal_GetCursorFlashOffset_m6_1353,
	GUIStyle_Internal_DrawCursor_m6_1354,
	GUIStyle_INTERNAL_CALL_Internal_DrawCursor_m6_1355,
	GUIStyle_Internal_DrawWithTextSelection_m6_1356,
	GUIStyle_SetDefaultFont_m6_1357,
	GUIStyle_Internal_GetCursorPixelPosition_m6_1358,
	GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6_1359,
	GUIStyle_Internal_GetCursorStringIndex_m6_1360,
	GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex_m6_1361,
	GUIStyle_Internal_CalcSize_m6_1362,
	GUIStyle_Internal_CalcHeight_m6_1363,
	GUIStyle_Internal_CalcMinMaxWidth_m6_1364,
	GUIUtility__cctor_m6_1365,
	GUIUtility_get_pixelsPerPoint_m6_1366,
	GUIUtility_GetControlID_m6_1367,
	GUIUtility_GetControlID_m6_1368,
	GUIUtility_GetStateObject_m6_1369,
	GUIUtility_get_hotControl_m6_1370,
	GUIUtility_set_hotControl_m6_1371,
	GUIUtility_GetDefaultSkin_m6_1372,
	GUIUtility_BeginGUI_m6_1373,
	GUIUtility_EndGUI_m6_1374,
	GUIUtility_EndGUIFromException_m6_1375,
	GUIUtility_CheckOnGUI_m6_1376,
	GUIUtility_Internal_GetPixelsPerPoint_m6_1377,
	GUIUtility_GetControlID_m6_1378,
	GUIUtility_Internal_GetNextControlID2_m6_1379,
	GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6_1380,
	GUIUtility_Internal_GetHotControl_m6_1381,
	GUIUtility_Internal_SetHotControl_m6_1382,
	GUIUtility_get_keyboardControl_m6_1383,
	GUIUtility_set_keyboardControl_m6_1384,
	GUIUtility_get_systemCopyBuffer_m6_1385,
	GUIUtility_set_systemCopyBuffer_m6_1386,
	GUIUtility_Internal_GetDefaultSkin_m6_1387,
	GUIUtility_Internal_ExitGUI_m6_1388,
	GUIUtility_Internal_GetGUIDepth_m6_1389,
	GUIUtility_get_mouseUsed_m6_1390,
	GUIUtility_set_mouseUsed_m6_1391,
	GUIUtility_set_textFieldInput_m6_1392,
	GUIClip_Push_m6_1393,
	GUIClip_INTERNAL_CALL_Push_m6_1394,
	GUIClip_Pop_m6_1395,
	WrapperlessIcall__ctor_m6_1396,
	IL2CPPStructAlignmentAttribute__ctor_m6_1397,
	AttributeHelperEngine__cctor_m6_1398,
	AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6_1399,
	AttributeHelperEngine_GetRequiredComponents_m6_1400,
	AttributeHelperEngine_CheckIsEditorScript_m6_1401,
	RequireComponent__ctor_m6_1402,
	AddComponentMenu__ctor_m6_1403,
	ExecuteInEditMode__ctor_m6_1404,
	HideInInspector__ctor_m6_1405,
	SetupCoroutine__ctor_m6_1406,
	SetupCoroutine_InvokeMember_m6_1407,
	SetupCoroutine_InvokeStatic_m6_1408,
	WritableAttribute__ctor_m6_1409,
	AssemblyIsEditorAssembly__ctor_m6_1410,
	GcUserProfileData_ToUserProfile_m6_1411,
	GcUserProfileData_AddToArray_m6_1412,
	GcAchievementDescriptionData_ToAchievementDescription_m6_1413,
	GcAchievementData_ToAchievement_m6_1414,
	GcScoreData_ToScore_m6_1415,
	Resolution_get_width_m6_1416,
	Resolution_set_width_m6_1417,
	Resolution_get_height_m6_1418,
	Resolution_set_height_m6_1419,
	Resolution_get_refreshRate_m6_1420,
	Resolution_set_refreshRate_m6_1421,
	Resolution_ToString_m6_1422,
	GUIStateObjects__cctor_m6_1423,
	GUIStateObjects_GetStateObject_m6_1424,
	LocalUser__ctor_m6_1425,
	LocalUser_SetFriends_m6_1426,
	LocalUser_SetAuthenticated_m6_1427,
	LocalUser_SetUnderage_m6_1428,
	LocalUser_get_authenticated_m6_1429,
	UserProfile__ctor_m6_1430,
	UserProfile__ctor_m6_1431,
	UserProfile_ToString_m6_1432,
	UserProfile_SetUserName_m6_1433,
	UserProfile_SetUserID_m6_1434,
	UserProfile_SetImage_m6_1435,
	UserProfile_get_userName_m6_1436,
	UserProfile_get_id_m6_1437,
	UserProfile_get_isFriend_m6_1438,
	UserProfile_get_state_m6_1439,
	Achievement__ctor_m6_1440,
	Achievement__ctor_m6_1441,
	Achievement__ctor_m6_1442,
	Achievement_ToString_m6_1443,
	Achievement_get_id_m6_1444,
	Achievement_set_id_m6_1445,
	Achievement_get_percentCompleted_m6_1446,
	Achievement_set_percentCompleted_m6_1447,
	Achievement_get_completed_m6_1448,
	Achievement_get_hidden_m6_1449,
	Achievement_get_lastReportedDate_m6_1450,
	AchievementDescription__ctor_m6_1451,
	AchievementDescription_ToString_m6_1452,
	AchievementDescription_SetImage_m6_1453,
	AchievementDescription_get_id_m6_1454,
	AchievementDescription_set_id_m6_1455,
	AchievementDescription_get_title_m6_1456,
	AchievementDescription_get_achievedDescription_m6_1457,
	AchievementDescription_get_unachievedDescription_m6_1458,
	AchievementDescription_get_hidden_m6_1459,
	AchievementDescription_get_points_m6_1460,
	Score__ctor_m6_1461,
	Score__ctor_m6_1462,
	Score_ToString_m6_1463,
	Score_get_leaderboardID_m6_1464,
	Score_set_leaderboardID_m6_1465,
	Score_get_value_m6_1466,
	Score_set_value_m6_1467,
	Leaderboard__ctor_m6_1468,
	Leaderboard_ToString_m6_1469,
	Leaderboard_SetLocalUserScore_m6_1470,
	Leaderboard_SetMaxRange_m6_1471,
	Leaderboard_SetScores_m6_1472,
	Leaderboard_SetTitle_m6_1473,
	Leaderboard_GetUserFilter_m6_1474,
	Leaderboard_get_id_m6_1475,
	Leaderboard_set_id_m6_1476,
	Leaderboard_get_userScope_m6_1477,
	Leaderboard_set_userScope_m6_1478,
	Leaderboard_get_range_m6_1479,
	Leaderboard_set_range_m6_1480,
	Leaderboard_get_timeScope_m6_1481,
	Leaderboard_set_timeScope_m6_1482,
	HitInfo_SendMessage_m6_1483,
	HitInfo_Compare_m6_1484,
	HitInfo_op_Implicit_m6_1485,
	SendMouseEvents__cctor_m6_1486,
	SendMouseEvents_SetMouseMoved_m6_1487,
	SendMouseEvents_DoSendMouseEvents_m6_1488,
	SendMouseEvents_SendEvents_m6_1489,
	Range__ctor_m6_1490,
	SliderState__ctor_m6_1491,
	SliderHandler__ctor_m6_1492,
	SliderHandler_Handle_m6_1493,
	SliderHandler_OnMouseDown_m6_1494,
	SliderHandler_OnMouseDrag_m6_1495,
	SliderHandler_OnMouseUp_m6_1496,
	SliderHandler_OnRepaint_m6_1497,
	SliderHandler_CurrentEventType_m6_1498,
	SliderHandler_CurrentScrollTroughSide_m6_1499,
	SliderHandler_IsEmptySlider_m6_1500,
	SliderHandler_SupportsPageMovements_m6_1501,
	SliderHandler_PageMovementValue_m6_1502,
	SliderHandler_PageUpMovementBound_m6_1503,
	SliderHandler_CurrentEvent_m6_1504,
	SliderHandler_ValueForCurrentMousePosition_m6_1505,
	SliderHandler_Clamp_m6_1506,
	SliderHandler_ThumbSelectionRect_m6_1507,
	SliderHandler_StartDraggingWithValue_m6_1508,
	SliderHandler_SliderState_m6_1509,
	SliderHandler_ThumbRect_m6_1510,
	SliderHandler_VerticalThumbRect_m6_1511,
	SliderHandler_HorizontalThumbRect_m6_1512,
	SliderHandler_ClampedCurrentValue_m6_1513,
	SliderHandler_MousePosition_m6_1514,
	SliderHandler_ValuesPerPixel_m6_1515,
	SliderHandler_ThumbSize_m6_1516,
	SliderHandler_MaxValue_m6_1517,
	SliderHandler_MinValue_m6_1518,
	StackTraceUtility__ctor_m6_1519,
	StackTraceUtility__cctor_m6_1520,
	StackTraceUtility_SetProjectFolder_m6_1521,
	StackTraceUtility_ExtractStackTrace_m6_1522,
	StackTraceUtility_IsSystemStacktraceType_m6_1523,
	StackTraceUtility_ExtractStringFromException_m6_1524,
	StackTraceUtility_ExtractStringFromExceptionInternal_m6_1525,
	StackTraceUtility_PostprocessStacktrace_m6_1526,
	StackTraceUtility_ExtractFormattedStackTrace_m6_1527,
	UnityException__ctor_m6_1528,
	UnityException__ctor_m6_1529,
	UnityException__ctor_m6_1530,
	UnityException__ctor_m6_1531,
	SharedBetweenAnimatorsAttribute__ctor_m6_1532,
	StateMachineBehaviour__ctor_m6_1533,
	SystemClock__cctor_m6_1534,
	SystemClock_get_now_m6_1535,
	TextEditor__ctor_m6_1536,
	TextEditor_get_position_m6_1537,
	TextEditor_set_position_m6_1538,
	TextEditor_get_cursorIndex_m6_1539,
	TextEditor_set_cursorIndex_m6_1540,
	TextEditor_get_selectIndex_m6_1541,
	TextEditor_set_selectIndex_m6_1542,
	TextEditor_ClearCursorPos_m6_1543,
	TextEditor_OnFocus_m6_1544,
	TextEditor_OnLostFocus_m6_1545,
	TextEditor_GrabGraphicalCursorPos_m6_1546,
	TextEditor_HandleKeyEvent_m6_1547,
	TextEditor_DeleteLineBack_m6_1548,
	TextEditor_DeleteWordBack_m6_1549,
	TextEditor_DeleteWordForward_m6_1550,
	TextEditor_Delete_m6_1551,
	TextEditor_Backspace_m6_1552,
	TextEditor_SelectAll_m6_1553,
	TextEditor_SelectNone_m6_1554,
	TextEditor_get_hasSelection_m6_1555,
	TextEditor_DeleteSelection_m6_1556,
	TextEditor_ReplaceSelection_m6_1557,
	TextEditor_Insert_m6_1558,
	TextEditor_MoveRight_m6_1559,
	TextEditor_MoveLeft_m6_1560,
	TextEditor_MoveUp_m6_1561,
	TextEditor_MoveDown_m6_1562,
	TextEditor_MoveLineStart_m6_1563,
	TextEditor_MoveLineEnd_m6_1564,
	TextEditor_MoveGraphicalLineStart_m6_1565,
	TextEditor_MoveGraphicalLineEnd_m6_1566,
	TextEditor_MoveTextStart_m6_1567,
	TextEditor_MoveTextEnd_m6_1568,
	TextEditor_IndexOfEndOfLine_m6_1569,
	TextEditor_MoveParagraphForward_m6_1570,
	TextEditor_MoveParagraphBackward_m6_1571,
	TextEditor_MoveCursorToPosition_m6_1572,
	TextEditor_SelectToPosition_m6_1573,
	TextEditor_SelectLeft_m6_1574,
	TextEditor_SelectRight_m6_1575,
	TextEditor_SelectUp_m6_1576,
	TextEditor_SelectDown_m6_1577,
	TextEditor_SelectTextEnd_m6_1578,
	TextEditor_SelectTextStart_m6_1579,
	TextEditor_MouseDragSelectsWholeWords_m6_1580,
	TextEditor_DblClickSnap_m6_1581,
	TextEditor_GetGraphicalLineStart_m6_1582,
	TextEditor_GetGraphicalLineEnd_m6_1583,
	TextEditor_FindNextSeperator_m6_1584,
	TextEditor_isLetterLikeChar_m6_1585,
	TextEditor_FindPrevSeperator_m6_1586,
	TextEditor_MoveWordRight_m6_1587,
	TextEditor_MoveToStartOfNextWord_m6_1588,
	TextEditor_MoveToEndOfPreviousWord_m6_1589,
	TextEditor_SelectToStartOfNextWord_m6_1590,
	TextEditor_SelectToEndOfPreviousWord_m6_1591,
	TextEditor_ClassifyChar_m6_1592,
	TextEditor_FindStartOfNextWord_m6_1593,
	TextEditor_FindEndOfPreviousWord_m6_1594,
	TextEditor_MoveWordLeft_m6_1595,
	TextEditor_SelectWordRight_m6_1596,
	TextEditor_SelectWordLeft_m6_1597,
	TextEditor_ExpandSelectGraphicalLineStart_m6_1598,
	TextEditor_ExpandSelectGraphicalLineEnd_m6_1599,
	TextEditor_SelectGraphicalLineStart_m6_1600,
	TextEditor_SelectGraphicalLineEnd_m6_1601,
	TextEditor_SelectParagraphForward_m6_1602,
	TextEditor_SelectParagraphBackward_m6_1603,
	TextEditor_SelectCurrentWord_m6_1604,
	TextEditor_FindEndOfClassification_m6_1605,
	TextEditor_SelectCurrentParagraph_m6_1606,
	TextEditor_UpdateScrollOffsetIfNeeded_m6_1607,
	TextEditor_UpdateScrollOffset_m6_1608,
	TextEditor_DrawCursor_m6_1609,
	TextEditor_PerformOperation_m6_1610,
	TextEditor_SaveBackup_m6_1611,
	TextEditor_Cut_m6_1612,
	TextEditor_Copy_m6_1613,
	TextEditor_ReplaceNewlinesWithSpaces_m6_1614,
	TextEditor_Paste_m6_1615,
	TextEditor_MapKey_m6_1616,
	TextEditor_InitKeyActions_m6_1617,
	TextEditor_DetectFocusChange_m6_1618,
	TextGenerationSettings_CompareColors_m6_1619,
	TextGenerationSettings_CompareVector2_m6_1620,
	TextGenerationSettings_Equals_m6_1621,
	TrackedReference_Equals_m6_1622,
	TrackedReference_GetHashCode_m6_1623,
	TrackedReference_op_Equality_m6_1624,
	ArgumentCache__ctor_m6_1625,
	ArgumentCache_TidyAssemblyTypeName_m6_1626,
	ArgumentCache_OnBeforeSerialize_m6_1627,
	ArgumentCache_OnAfterDeserialize_m6_1628,
	PersistentCall__ctor_m6_1629,
	PersistentCallGroup__ctor_m6_1630,
	InvokableCallList__ctor_m6_1631,
	InvokableCallList_ClearPersistent_m6_1632,
	UnityEventBase__ctor_m6_1633,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6_1634,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6_1635,
	UnityEventBase_DirtyPersistentCalls_m6_1636,
	UnityEventBase_ToString_m6_1637,
	UnityEvent__ctor_m6_1638,
	DefaultValueAttribute__ctor_m6_1639,
	DefaultValueAttribute_get_Value_m6_1640,
	DefaultValueAttribute_Equals_m6_1641,
	DefaultValueAttribute_GetHashCode_m6_1642,
	ExcludeFromDocsAttribute__ctor_m6_1643,
	FormerlySerializedAsAttribute__ctor_m6_1644,
	TypeInferenceRuleAttribute__ctor_m6_1645,
	TypeInferenceRuleAttribute__ctor_m6_1646,
	TypeInferenceRuleAttribute_ToString_m6_1647,
	GenericStack__ctor_m6_1648,
	UnityAdsDelegate__ctor_m6_1649,
	UnityAdsDelegate_Invoke_m6_1650,
	UnityAdsDelegate_BeginInvoke_m6_1651,
	UnityAdsDelegate_EndInvoke_m6_1652,
	ChatChannel__ctor_m7_0,
	ChatChannel_get_IsPrivate_m7_1,
	ChatChannel_set_IsPrivate_m7_2,
	ChatChannel_get_MessageCount_m7_3,
	ChatChannel_Add_m7_4,
	ChatChannel_Add_m7_5,
	ChatChannel_ClearMessages_m7_6,
	ChatChannel_ToStringMessages_m7_7,
	ChatClient__ctor_m7_8,
	ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_DebugReturn_m7_9,
	ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnEvent_m7_10,
	ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnOperationResponse_m7_11,
	ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnStatusChanged_m7_12,
	ChatClient_get_NameServerAddress_m7_13,
	ChatClient_set_NameServerAddress_m7_14,
	ChatClient_get_FrontendAddress_m7_15,
	ChatClient_set_FrontendAddress_m7_16,
	ChatClient_get_ChatRegion_m7_17,
	ChatClient_set_ChatRegion_m7_18,
	ChatClient_get_State_m7_19,
	ChatClient_set_State_m7_20,
	ChatClient_get_DisconnectedCause_m7_21,
	ChatClient_set_DisconnectedCause_m7_22,
	ChatClient_get_CanChat_m7_23,
	ChatClient_get_HasPeer_m7_24,
	ChatClient_get_AppVersion_m7_25,
	ChatClient_set_AppVersion_m7_26,
	ChatClient_get_AppId_m7_27,
	ChatClient_set_AppId_m7_28,
	ChatClient_get_AuthValues_m7_29,
	ChatClient_set_AuthValues_m7_30,
	ChatClient_get_UserId_m7_31,
	ChatClient_set_UserId_m7_32,
	ChatClient_Connect_m7_33,
	ChatClient_Service_m7_34,
	ChatClient_Disconnect_m7_35,
	ChatClient_StopThread_m7_36,
	ChatClient_Subscribe_m7_37,
	ChatClient_Subscribe_m7_38,
	ChatClient_Unsubscribe_m7_39,
	ChatClient_PublishMessage_m7_40,
	ChatClient_SendPrivateMessage_m7_41,
	ChatClient_SendPrivateMessage_m7_42,
	ChatClient_SetOnlineStatus_m7_43,
	ChatClient_SetOnlineStatus_m7_44,
	ChatClient_SetOnlineStatus_m7_45,
	ChatClient_AddFriends_m7_46,
	ChatClient_RemoveFriends_m7_47,
	ChatClient_GetPrivateChannelNameByUser_m7_48,
	ChatClient_TryGetChannel_m7_49,
	ChatClient_TryGetChannel_m7_50,
	ChatClient_SendAcksOnly_m7_51,
	ChatClient_set_DebugOut_m7_52,
	ChatClient_get_DebugOut_m7_53,
	ChatClient_SendChannelOperation_m7_54,
	ChatClient_HandlePrivateMessageEvent_m7_55,
	ChatClient_HandleChatMessagesEvent_m7_56,
	ChatClient_HandleSubscribeEvent_m7_57,
	ChatClient_HandleUnsubscribeEvent_m7_58,
	ChatClient_HandleAuthResponse_m7_59,
	ChatClient_HandleStatusUpdate_m7_60,
	ChatClient_ConnectToFrontEnd_m7_61,
	ChatClient_AuthenticateOnFrontEnd_m7_62,
	ChatEventCode__ctor_m7_63,
	ChatOperationCode__ctor_m7_64,
	ChatParameterCode__ctor_m7_65,
	ChatPeer__ctor_m7_66,
	ChatPeer__cctor_m7_67,
	ChatPeer_get_NameServerAddress_m7_68,
	ChatPeer_get_IsProtocolSecure_m7_69,
	ChatPeer_GetNameServerAddress_m7_70,
	ChatPeer_Connect_m7_71,
	ChatPeer_AuthenticateOnNameServer_m7_72,
	AuthenticationValues__ctor_m7_73,
	AuthenticationValues__ctor_m7_74,
	AuthenticationValues_get_AuthType_m7_75,
	AuthenticationValues_set_AuthType_m7_76,
	AuthenticationValues_get_AuthGetParameters_m7_77,
	AuthenticationValues_set_AuthGetParameters_m7_78,
	AuthenticationValues_get_AuthPostData_m7_79,
	AuthenticationValues_set_AuthPostData_m7_80,
	AuthenticationValues_get_Token_m7_81,
	AuthenticationValues_set_Token_m7_82,
	AuthenticationValues_get_UserId_m7_83,
	AuthenticationValues_set_UserId_m7_84,
	AuthenticationValues_SetAuthPostData_m7_85,
	AuthenticationValues_SetAuthPostData_m7_86,
	AuthenticationValues_AddAuthParameter_m7_87,
	AuthenticationValues_ToString_m7_88,
	ParameterCode__ctor_m7_89,
	ErrorCode__ctor_m7_90,
	Demo2DJumpAndRun__ctor_m8_0,
	Demo2DJumpAndRun_OnJoinedRoom_m8_1,
	JumpAndRunMovement__ctor_m8_2,
	JumpAndRunMovement_Awake_m8_3,
	JumpAndRunMovement_Update_m8_4,
	JumpAndRunMovement_FixedUpdate_m8_5,
	JumpAndRunMovement_UpdateFacingDirection_m8_6,
	JumpAndRunMovement_UpdateJumping_m8_7,
	JumpAndRunMovement_DoJump_m8_8,
	JumpAndRunMovement_UpdateMovement_m8_9,
	JumpAndRunMovement_UpdateIsRunning_m8_10,
	JumpAndRunMovement_UpdateIsGrounded_m8_11,
	DemoBoxesGui__ctor_m8_12,
	DemoBoxesGui_OnGUI_m8_13,
	OnAwakePhysicsSettings__ctor_m8_14,
	OnAwakePhysicsSettings_Awake_m8_15,
	ClickAndDrag__ctor_m8_16,
	ClickAndDrag_Update_m8_17,
	DemoOwnershipGui__ctor_m8_18,
	DemoOwnershipGui_OnOwnershipRequest_m8_19,
	DemoOwnershipGui_OnGUI_m8_20,
	InstantiateCube__ctor_m8_21,
	InstantiateCube_OnClick_m8_22,
	MaterialPerOwner__ctor_m8_23,
	MaterialPerOwner_Start_m8_24,
	MaterialPerOwner_Update_m8_25,
	OnClickDisableObj__ctor_m8_26,
	OnClickDisableObj_OnClick_m8_27,
	OnClickRequestOwnership__ctor_m8_28,
	OnClickRequestOwnership_OnClick_m8_29,
	OnClickRequestOwnership_ColorRpc_m8_30,
	OnClickRightDestroy__ctor_m8_31,
	OnClickRightDestroy_OnPressRight_m8_32,
	ChatGui__ctor_m8_33,
	ChatGui__cctor_m8_34,
	ChatGui_get_UserName_m8_35,
	ChatGui_set_UserName_m8_36,
	ChatGui_get_Instance_m8_37,
	ChatGui_Awake_m8_38,
	ChatGui_Start_m8_39,
	ChatGui_OnApplicationQuit_m8_40,
	ChatGui_OnDestroy_m8_41,
	ChatGui_Update_m8_42,
	ChatGui_OnGUI_m8_43,
	ChatGui_GuiSendsMsg_m8_44,
	ChatGui_PostHelpToCurrentChannel_m8_45,
	ChatGui_OnConnected_m8_46,
	ChatGui_DebugReturn_m8_47,
	ChatGui_OnDisconnected_m8_48,
	ChatGui_OnChatStateChange_m8_49,
	ChatGui_OnSubscribed_m8_50,
	ChatGui_OnUnsubscribed_m8_51,
	ChatGui_OnGetMessages_m8_52,
	ChatGui_OnPrivateMessage_m8_53,
	ChatGui_OnStatusUpdate_m8_54,
	NamePickGui__ctor_m8_55,
	NamePickGui_Awake_m8_56,
	NamePickGui_OnGUI_m8_57,
	NamePickGui_StartChat_m8_58,
	GUICustomAuth__ctor_m8_59,
	GUICustomAuth_Start_m8_60,
	GUICustomAuth_OnJoinedLobby_m8_61,
	GUICustomAuth_OnConnectedToMaster_m8_62,
	GUICustomAuth_OnCustomAuthenticationFailed_m8_63,
	GUICustomAuth_SetStateAuthInput_m8_64,
	GUICustomAuth_SetStateAuthHelp_m8_65,
	GUICustomAuth_SetStateAuthOrNot_m8_66,
	GUICustomAuth_SetStateAuthFailed_m8_67,
	GUICustomAuth_ConnectWithNickname_m8_68,
	GUICustomAuth_OnGUI_m8_69,
	GUIFriendFinding__ctor_m8_70,
	GUIFriendFinding_Start_m8_71,
	GUIFriendFinding_FetchFriendsFromCommunity_m8_72,
	GUIFriendFinding_OnUpdatedFriendList_m8_73,
	GUIFriendFinding_OnGUI_m8_74,
	GUIFriendsInRoom__ctor_m8_75,
	GUIFriendsInRoom_Start_m8_76,
	GUIFriendsInRoom_OnGUI_m8_77,
	OnClickCallMethod__ctor_m8_78,
	OnClickCallMethod_OnClick_m8_79,
	HubGui__ctor_m8_80,
	HubGui_Start_m8_81,
	HubGui_OnGUI_m8_82,
	MoveCam__ctor_m8_83,
	MoveCam_Start_m8_84,
	MoveCam_Update_m8_85,
	ToHubButton__ctor_m8_86,
	ToHubButton_get_Instance_m8_87,
	ToHubButton_Awake_m8_88,
	ToHubButton_Start_m8_89,
	ToHubButton_OnGUI_m8_90,
	DemoMecanimGUI__ctor_m8_91,
	DemoMecanimGUI_Awake_m8_92,
	DemoMecanimGUI_Update_m8_93,
	DemoMecanimGUI_FindRemoteAnimator_m8_94,
	DemoMecanimGUI_OnGUI_m8_95,
	DemoMecanimGUI_OnJoinedRoom_m8_96,
	DemoMecanimGUI_CreatePlayerObject_m8_97,
	MessageOverlay__ctor_m8_98,
	MessageOverlay_Start_m8_99,
	MessageOverlay_OnJoinedRoom_m8_100,
	MessageOverlay_OnLeftRoom_m8_101,
	MessageOverlay_SetActive_m8_102,
	OnCollideSwitchTeam__ctor_m8_103,
	OnCollideSwitchTeam_OnTriggerEnter_m8_104,
	OnPickedUpScript__ctor_m8_105,
	OnPickedUpScript_OnPickedUp_m8_106,
	PickupCamera__ctor_m8_107,
	PickupCamera_OnEnable_m8_108,
	PickupCamera_DebugDrawStuff_m8_109,
	PickupCamera_AngleDistance_m8_110,
	PickupCamera_Apply_m8_111,
	PickupCamera_LateUpdate_m8_112,
	PickupCamera_Cut_m8_113,
	PickupCamera_SetUpRotation_m8_114,
	PickupCamera_GetCenterOffset_m8_115,
	PickupController__ctor_m8_116,
	PickupController_Awake_m8_117,
	PickupController_Update_m8_118,
	PickupController_OnPhotonSerializeView_m8_119,
	PickupController_UpdateSmoothedMovementDirection_m8_120,
	PickupController_ApplyJumping_m8_121,
	PickupController_ApplyGravity_m8_122,
	PickupController_CalculateJumpVerticalSpeed_m8_123,
	PickupController_DidJump_m8_124,
	PickupController_OnControllerColliderHit_m8_125,
	PickupController_GetSpeed_m8_126,
	PickupController_IsJumping_m8_127,
	PickupController_IsGrounded_m8_128,
	PickupController_GetDirection_m8_129,
	PickupController_IsMovingBackwards_m8_130,
	PickupController_GetLockCameraTimer_m8_131,
	PickupController_IsMoving_m8_132,
	PickupController_HasJumpReachedApex_m8_133,
	PickupController_IsGroundedWithTimeout_m8_134,
	PickupController_Reset_m8_135,
	PickupDemoGui__ctor_m8_136,
	PickupDemoGui_OnGUI_m8_137,
	PickupTriggerForward__ctor_m8_138,
	PickupTriggerForward_OnTriggerEnter_m8_139,
	DemoRPGMovement__ctor_m8_140,
	DemoRPGMovement_OnJoinedRoom_m8_141,
	DemoRPGMovement_CreatePlayerObject_m8_142,
	RPGCamera__ctor_m8_143,
	RPGCamera_Start_m8_144,
	RPGCamera_LateUpdate_m8_145,
	RPGCamera_UpdateDistance_m8_146,
	RPGCamera_UpdateZoom_m8_147,
	RPGCamera_UpdatePosition_m8_148,
	RPGCamera_UpdateRotation_m8_149,
	RPGMovement__ctor_m8_150,
	RPGMovement_Start_m8_151,
	RPGMovement_Update_m8_152,
	RPGMovement_UpdateAnimation_m8_153,
	RPGMovement_ResetSpeedValues_m8_154,
	RPGMovement_ApplySynchronizedValues_m8_155,
	RPGMovement_ApplyGravityToCharacterController_m8_156,
	RPGMovement_MoveCharacterController_m8_157,
	RPGMovement_UpdateForwardMovement_m8_158,
	RPGMovement_UpdateBackwardMovement_m8_159,
	RPGMovement_UpdateStrafeMovement_m8_160,
	RPGMovement_UpdateRotateMovement_m8_161,
	CubeExtra__ctor_m8_162,
	CubeExtra_Awake_m8_163,
	CubeExtra_OnPhotonSerializeView_m8_164,
	CubeExtra_Update_m8_165,
	CubeInter__ctor_m8_166,
	CubeInter_Awake_m8_167,
	CubeInter_OnPhotonSerializeView_m8_168,
	CubeInter_Update_m8_169,
	CubeLerp__ctor_m8_170,
	CubeLerp_Awake_m8_171,
	CubeLerp_OnPhotonSerializeView_m8_172,
	CubeLerp_Update_m8_173,
	IELdemo__ctor_m8_174,
	IELdemo_Awake_m8_175,
	IELdemo_OnConnectedToMaster_m8_176,
	IELdemo_OnPhotonRandomJoinFailed_m8_177,
	IELdemo_OnJoinedRoom_m8_178,
	IELdemo_OnCreatedRoom_m8_179,
	IELdemo_Update_m8_180,
	IELdemo_OnGUI_m8_181,
	ThirdPersonCamera__ctor_m8_182,
	ThirdPersonCamera_OnEnable_m8_183,
	ThirdPersonCamera_DebugDrawStuff_m8_184,
	ThirdPersonCamera_AngleDistance_m8_185,
	ThirdPersonCamera_Apply_m8_186,
	ThirdPersonCamera_LateUpdate_m8_187,
	ThirdPersonCamera_Cut_m8_188,
	ThirdPersonCamera_SetUpRotation_m8_189,
	ThirdPersonCamera_GetCenterOffset_m8_190,
	ThirdPersonController__ctor_m8_191,
	ThirdPersonController_Awake_m8_192,
	ThirdPersonController_UpdateSmoothedMovementDirection_m8_193,
	ThirdPersonController_ApplyJumping_m8_194,
	ThirdPersonController_ApplyGravity_m8_195,
	ThirdPersonController_CalculateJumpVerticalSpeed_m8_196,
	ThirdPersonController_DidJump_m8_197,
	ThirdPersonController_Update_m8_198,
	ThirdPersonController_OnControllerColliderHit_m8_199,
	ThirdPersonController_GetSpeed_m8_200,
	ThirdPersonController_IsJumping_m8_201,
	ThirdPersonController_IsGrounded_m8_202,
	ThirdPersonController_GetDirection_m8_203,
	ThirdPersonController_IsMovingBackwards_m8_204,
	ThirdPersonController_GetLockCameraTimer_m8_205,
	ThirdPersonController_IsMoving_m8_206,
	ThirdPersonController_HasJumpReachedApex_m8_207,
	ThirdPersonController_IsGroundedWithTimeout_m8_208,
	ThirdPersonController_Reset_m8_209,
	ThirdPersonNetwork__ctor_m8_210,
	ThirdPersonNetwork_Awake_m8_211,
	ThirdPersonNetwork_OnPhotonSerializeView_m8_212,
	ThirdPersonNetwork_Update_m8_213,
	WorkerInGame__ctor_m8_214,
	WorkerInGame_Awake_m8_215,
	WorkerInGame_OnGUI_m8_216,
	WorkerInGame_OnMasterClientSwitched_m8_217,
	WorkerInGame_OnLeftRoom_m8_218,
	WorkerInGame_OnDisconnectedFromPhoton_m8_219,
	WorkerInGame_OnPhotonInstantiate_m8_220,
	WorkerInGame_OnPhotonPlayerConnected_m8_221,
	WorkerInGame_OnPhotonPlayerDisconnected_m8_222,
	WorkerInGame_OnFailedToConnectToPhoton_m8_223,
	WorkerMenu__ctor_m8_224,
	WorkerMenu__cctor_m8_225,
	WorkerMenu_get_ErrorDialog_m8_226,
	WorkerMenu_set_ErrorDialog_m8_227,
	WorkerMenu_Awake_m8_228,
	WorkerMenu_OnGUI_m8_229,
	WorkerMenu_OnJoinedRoom_m8_230,
	WorkerMenu_OnPhotonCreateRoomFailed_m8_231,
	WorkerMenu_OnPhotonJoinRoomFailed_m8_232,
	WorkerMenu_OnPhotonRandomJoinFailed_m8_233,
	WorkerMenu_OnCreatedRoom_m8_234,
	WorkerMenu_OnDisconnectedFromPhoton_m8_235,
	WorkerMenu_OnFailedToConnectToPhoton_m8_236,
	AudioRpc__ctor_m8_237,
	AudioRpc_Awake_m8_238,
	AudioRpc_Marco_m8_239,
	AudioRpc_Polo_m8_240,
	AudioRpc_OnApplicationFocus_m8_241,
	ClickDetector__ctor_m8_242,
	ClickDetector_Update_m8_243,
	ClickDetector_RaycastObject_m8_244,
	GameLogic__ctor_m8_245,
	GameLogic__cctor_m8_246,
	GameLogic_Start_m8_247,
	GameLogic_OnJoinedRoom_m8_248,
	GameLogic_OnPhotonPlayerConnected_m8_249,
	GameLogic_TagPlayer_m8_250,
	GameLogic_TaggedPlayer_m8_251,
	GameLogic_OnPhotonPlayerDisconnected_m8_252,
	GameLogic_OnMasterClientSwitched_m8_253,
	myThirdPersonController__ctor_m8_254,
	NetworkCharacter__ctor_m8_255,
	NetworkCharacter_Update_m8_256,
	NetworkCharacter_OnPhotonSerializeView_m8_257,
	RandomMatchmaker__ctor_m8_258,
	RandomMatchmaker_Start_m8_259,
	RandomMatchmaker_OnJoinedLobby_m8_260,
	RandomMatchmaker_OnConnectedToMaster_m8_261,
	RandomMatchmaker_OnPhotonRandomJoinFailed_m8_262,
	RandomMatchmaker_OnJoinedRoom_m8_263,
	RandomMatchmaker_OnGUI_m8_264,
	IdleRunJump__ctor_m8_265,
	IdleRunJump_Start_m8_266,
	IdleRunJump_Update_m8_267,
	PlayerDiamond__ctor_m8_268,
	PlayerDiamond_get_PhotonView_m8_269,
	PlayerDiamond_get_DiamondRenderer_m8_270,
	PlayerDiamond_Start_m8_271,
	PlayerDiamond_Update_m8_272,
	PlayerDiamond_UpdateDiamondPosition_m8_273,
	PlayerDiamond_UpdateDiamondRotation_m8_274,
	PlayerDiamond_UpdateDiamondVisibility_m8_275,
	PlayerVariables__ctor_m8_276,
	PlayerVariables__cctor_m8_277,
	PlayerVariables_GetColor_m8_278,
	PlayerVariables_GetColorName_m8_279,
	PlayerVariables_GetMaterial_m8_280,
	CustomTypes__cctor_m8_281,
	CustomTypes_Register_m8_282,
	CustomTypes_SerializeVector3_m8_283,
	CustomTypes_DeserializeVector3_m8_284,
	CustomTypes_SerializeVector2_m8_285,
	CustomTypes_DeserializeVector2_m8_286,
	CustomTypes_SerializeQuaternion_m8_287,
	CustomTypes_DeserializeQuaternion_m8_288,
	CustomTypes_SerializePhotonPlayer_m8_289,
	CustomTypes_DeserializePhotonPlayer_m8_290,
	Extensions_GetPhotonViewsInChildren_m8_291,
	Extensions_GetPhotonView_m8_292,
	Extensions_AlmostEquals_m8_293,
	Extensions_AlmostEquals_m8_294,
	Extensions_AlmostEquals_m8_295,
	Extensions_AlmostEquals_m8_296,
	Extensions_Merge_m8_297,
	Extensions_MergeStringKeys_m8_298,
	Extensions_ToStringFull_m8_299,
	Extensions_StripToStringKeys_m8_300,
	Extensions_StripKeysWithNullValues_m8_301,
	Extensions_Contains_m8_302,
	GameObjectExtensions_GetActive_m8_303,
	FriendInfo__ctor_m8_304,
	FriendInfo_get_Name_m8_305,
	FriendInfo_set_Name_m8_306,
	FriendInfo_get_IsOnline_m8_307,
	FriendInfo_set_IsOnline_m8_308,
	FriendInfo_get_Room_m8_309,
	FriendInfo_set_Room_m8_310,
	FriendInfo_get_IsInRoom_m8_311,
	FriendInfo_ToString_m8_312,
	GizmoTypeDrawer__ctor_m8_313,
	GizmoTypeDrawer_Draw_m8_314,
	EnterRoomParams__ctor_m8_315,
	OpJoinRandomRoomParams__ctor_m8_316,
	LoadbalancingPeer__ctor_m8_317,
	LoadbalancingPeer_get_IsProtocolSecure_m8_318,
	LoadbalancingPeer_OpGetRegions_m8_319,
	LoadbalancingPeer_OpJoinLobby_m8_320,
	LoadbalancingPeer_OpLeaveLobby_m8_321,
	LoadbalancingPeer_RoomOptionsToOpParameters_m8_322,
	LoadbalancingPeer_OpCreateRoom_m8_323,
	LoadbalancingPeer_OpJoinRoom_m8_324,
	LoadbalancingPeer_OpJoinRandomRoom_m8_325,
	LoadbalancingPeer_OpLeaveRoom_m8_326,
	LoadbalancingPeer_OpFindFriends_m8_327,
	LoadbalancingPeer_OpSetCustomPropertiesOfActor_m8_328,
	LoadbalancingPeer_OpSetPropertiesOfActor_m8_329,
	LoadbalancingPeer_OpSetPropertyOfRoom_m8_330,
	LoadbalancingPeer_OpSetCustomPropertiesOfRoom_m8_331,
	LoadbalancingPeer_OpSetPropertiesOfRoom_m8_332,
	LoadbalancingPeer_OpAuthenticate_m8_333,
	LoadbalancingPeer_OpChangeGroups_m8_334,
	LoadbalancingPeer_OpRaiseEvent_m8_335,
	ErrorCode__ctor_m8_336,
	ActorProperties__ctor_m8_337,
	GamePropertyKey__ctor_m8_338,
	EventCode__ctor_m8_339,
	ParameterCode__ctor_m8_340,
	OperationCode__ctor_m8_341,
	RaiseEventOptions__ctor_m8_342,
	RaiseEventOptions__cctor_m8_343,
	TypedLobby__ctor_m8_344,
	TypedLobby__ctor_m8_345,
	TypedLobby__cctor_m8_346,
	TypedLobby_get_IsDefault_m8_347,
	TypedLobby_ToString_m8_348,
	TypedLobbyInfo__ctor_m8_349,
	TypedLobbyInfo_ToString_m8_350,
	AuthenticationValues__ctor_m8_351,
	AuthenticationValues__ctor_m8_352,
	AuthenticationValues_get_AuthType_m8_353,
	AuthenticationValues_set_AuthType_m8_354,
	AuthenticationValues_get_AuthGetParameters_m8_355,
	AuthenticationValues_set_AuthGetParameters_m8_356,
	AuthenticationValues_get_AuthPostData_m8_357,
	AuthenticationValues_set_AuthPostData_m8_358,
	AuthenticationValues_get_Token_m8_359,
	AuthenticationValues_set_Token_m8_360,
	AuthenticationValues_get_UserId_m8_361,
	AuthenticationValues_set_UserId_m8_362,
	AuthenticationValues_SetAuthPostData_m8_363,
	AuthenticationValues_SetAuthPostData_m8_364,
	AuthenticationValues_AddAuthParameter_m8_365,
	AuthenticationValues_ToString_m8_366,
	NetworkingPeer__ctor_m8_367,
	NetworkingPeer__cctor_m8_368,
	NetworkingPeer_get_mAppVersionPun_m8_369,
	NetworkingPeer_get_CustomAuthenticationValues_m8_370,
	NetworkingPeer_set_CustomAuthenticationValues_m8_371,
	NetworkingPeer_get_NameServerAddress_m8_372,
	NetworkingPeer_get_MasterServerAddress_m8_373,
	NetworkingPeer_set_MasterServerAddress_m8_374,
	NetworkingPeer_get_mGameserver_m8_375,
	NetworkingPeer_set_mGameserver_m8_376,
	NetworkingPeer_get_server_m8_377,
	NetworkingPeer_set_server_m8_378,
	NetworkingPeer_get_State_m8_379,
	NetworkingPeer_set_State_m8_380,
	NetworkingPeer_get_IsUsingNameServer_m8_381,
	NetworkingPeer_set_IsUsingNameServer_m8_382,
	NetworkingPeer_get_IsAuthorizeSecretAvailable_m8_383,
	NetworkingPeer_get_AvailableRegions_m8_384,
	NetworkingPeer_set_AvailableRegions_m8_385,
	NetworkingPeer_get_CloudRegion_m8_386,
	NetworkingPeer_set_CloudRegion_m8_387,
	NetworkingPeer_get_requestLobbyStatistics_m8_388,
	NetworkingPeer_get_lobby_m8_389,
	NetworkingPeer_set_lobby_m8_390,
	NetworkingPeer_get_mPlayersOnMasterCount_m8_391,
	NetworkingPeer_set_mPlayersOnMasterCount_m8_392,
	NetworkingPeer_get_mGameCount_m8_393,
	NetworkingPeer_set_mGameCount_m8_394,
	NetworkingPeer_get_mPlayersInRoomsCount_m8_395,
	NetworkingPeer_set_mPlayersInRoomsCount_m8_396,
	NetworkingPeer_get_FriendsListAge_m8_397,
	NetworkingPeer_get_PlayerName_m8_398,
	NetworkingPeer_set_PlayerName_m8_399,
	NetworkingPeer_get_CurrentGame_m8_400,
	NetworkingPeer_set_CurrentGame_m8_401,
	NetworkingPeer_get_mLocalActor_m8_402,
	NetworkingPeer_set_mLocalActor_m8_403,
	NetworkingPeer_get_mMasterClientId_m8_404,
	NetworkingPeer_set_mMasterClientId_m8_405,
	NetworkingPeer_GetNameServerAddress_m8_406,
	NetworkingPeer_Connect_m8_407,
	NetworkingPeer_Connect_m8_408,
	NetworkingPeer_ConnectToNameServer_m8_409,
	NetworkingPeer_ConnectToRegionMaster_m8_410,
	NetworkingPeer_GetRegions_m8_411,
	NetworkingPeer_Disconnect_m8_412,
	NetworkingPeer_DisconnectToReconnect_m8_413,
	NetworkingPeer_LeftLobbyCleanup_m8_414,
	NetworkingPeer_LeftRoomCleanup_m8_415,
	NetworkingPeer_LocalCleanupAnythingInstantiated_m8_416,
	NetworkingPeer_ReadoutProperties_m8_417,
	NetworkingPeer_AddNewPlayer_m8_418,
	NetworkingPeer_RemovePlayer_m8_419,
	NetworkingPeer_RebuildPlayerListCopies_m8_420,
	NetworkingPeer_ResetPhotonViewsOnSerialize_m8_421,
	NetworkingPeer_HandleEventLeave_m8_422,
	NetworkingPeer_CheckMasterClient_m8_423,
	NetworkingPeer_UpdateMasterClient_m8_424,
	NetworkingPeer_ReturnLowestPlayerId_m8_425,
	NetworkingPeer_SetMasterClient_m8_426,
	NetworkingPeer_SetMasterClient_m8_427,
	NetworkingPeer_GetActorPropertiesForActorNr_m8_428,
	NetworkingPeer_GetPlayerWithId_m8_429,
	NetworkingPeer_SendPlayerName_m8_430,
	NetworkingPeer_GameEnteredOnGameServer_m8_431,
	NetworkingPeer_GetLocalActorProperties_m8_432,
	NetworkingPeer_ChangeLocalID_m8_433,
	NetworkingPeer_OpCreateGame_m8_434,
	NetworkingPeer_OpJoinRoom_m8_435,
	NetworkingPeer_OpJoinRandomRoom_m8_436,
	NetworkingPeer_OpLeave_m8_437,
	NetworkingPeer_OpRaiseEvent_m8_438,
	NetworkingPeer_DebugReturn_m8_439,
	NetworkingPeer_OnOperationResponse_m8_440,
	NetworkingPeer_OpFindFriends_m8_441,
	NetworkingPeer_OnStatusChanged_m8_442,
	NetworkingPeer_OnEvent_m8_443,
	NetworkingPeer_UpdatedActorList_m8_444,
	NetworkingPeer_SendVacantViewIds_m8_445,
	NetworkingPeer_SendMonoMessage_m8_446,
	NetworkingPeer_ExecuteRpc_m8_447,
	NetworkingPeer_CheckTypeMatch_m8_448,
	NetworkingPeer_SendInstantiate_m8_449,
	NetworkingPeer_DoInstantiate_m8_450,
	NetworkingPeer_StoreInstantiationData_m8_451,
	NetworkingPeer_FetchInstantiationData_m8_452,
	NetworkingPeer_RemoveInstantiationData_m8_453,
	NetworkingPeer_DestroyPlayerObjects_m8_454,
	NetworkingPeer_DestroyAll_m8_455,
	NetworkingPeer_RemoveInstantiatedGO_m8_456,
	NetworkingPeer_GetInstantiatedObjectsId_m8_457,
	NetworkingPeer_ServerCleanInstantiateAndDestroy_m8_458,
	NetworkingPeer_SendDestroyOfPlayer_m8_459,
	NetworkingPeer_SendDestroyOfAll_m8_460,
	NetworkingPeer_OpRemoveFromServerInstantiationsOfPlayer_m8_461,
	NetworkingPeer_RequestOwnership_m8_462,
	NetworkingPeer_TransferOwnership_m8_463,
	NetworkingPeer_LocalCleanPhotonView_m8_464,
	NetworkingPeer_GetPhotonView_m8_465,
	NetworkingPeer_RegisterPhotonView_m8_466,
	NetworkingPeer_OpCleanRpcBuffer_m8_467,
	NetworkingPeer_OpRemoveCompleteCacheOfPlayer_m8_468,
	NetworkingPeer_OpRemoveCompleteCache_m8_469,
	NetworkingPeer_RemoveCacheOfLeftPlayers_m8_470,
	NetworkingPeer_CleanRpcBufferIfMine_m8_471,
	NetworkingPeer_OpCleanRpcBuffer_m8_472,
	NetworkingPeer_RemoveRPCsInGroup_m8_473,
	NetworkingPeer_SetLevelPrefix_m8_474,
	NetworkingPeer_RPC_m8_475,
	NetworkingPeer_RPC_m8_476,
	NetworkingPeer_SetReceivingEnabled_m8_477,
	NetworkingPeer_SetReceivingEnabled_m8_478,
	NetworkingPeer_SetSendingEnabled_m8_479,
	NetworkingPeer_SetSendingEnabled_m8_480,
	NetworkingPeer_NewSceneLoaded_m8_481,
	NetworkingPeer_RunViewUpdate_m8_482,
	NetworkingPeer_OnSerializeWrite_m8_483,
	NetworkingPeer_OnSerializeRead_m8_484,
	NetworkingPeer_AlmostEquals_m8_485,
	NetworkingPeer_DeltaCompressionWrite_m8_486,
	NetworkingPeer_DeltaCompressionRead_m8_487,
	NetworkingPeer_ObjectIsSameWithInprecision_m8_488,
	NetworkingPeer_GetMethod_m8_489,
	NetworkingPeer_LoadLevelIfSynced_m8_490,
	NetworkingPeer_SetLevelInPropsIfSynced_m8_491,
	NetworkingPeer_SetApp_m8_492,
	NetworkingPeer_WebRpc_m8_493,
	MonoBehaviour__ctor_m8_494,
	MonoBehaviour_get_photonView_m8_495,
	MonoBehaviour_get_networkView_m8_496,
	PunBehaviour__ctor_m8_497,
	PunBehaviour_OnConnectedToPhoton_m8_498,
	PunBehaviour_OnLeftRoom_m8_499,
	PunBehaviour_OnMasterClientSwitched_m8_500,
	PunBehaviour_OnPhotonCreateRoomFailed_m8_501,
	PunBehaviour_OnPhotonJoinRoomFailed_m8_502,
	PunBehaviour_OnCreatedRoom_m8_503,
	PunBehaviour_OnJoinedLobby_m8_504,
	PunBehaviour_OnLeftLobby_m8_505,
	PunBehaviour_OnFailedToConnectToPhoton_m8_506,
	PunBehaviour_OnDisconnectedFromPhoton_m8_507,
	PunBehaviour_OnConnectionFail_m8_508,
	PunBehaviour_OnPhotonInstantiate_m8_509,
	PunBehaviour_OnReceivedRoomListUpdate_m8_510,
	PunBehaviour_OnJoinedRoom_m8_511,
	PunBehaviour_OnPhotonPlayerConnected_m8_512,
	PunBehaviour_OnPhotonPlayerDisconnected_m8_513,
	PunBehaviour_OnPhotonRandomJoinFailed_m8_514,
	PunBehaviour_OnConnectedToMaster_m8_515,
	PunBehaviour_OnPhotonMaxCccuReached_m8_516,
	PunBehaviour_OnPhotonCustomRoomPropertiesChanged_m8_517,
	PunBehaviour_OnPhotonPlayerPropertiesChanged_m8_518,
	PunBehaviour_OnUpdatedFriendList_m8_519,
	PunBehaviour_OnCustomAuthenticationFailed_m8_520,
	PunBehaviour_OnWebRpcResponse_m8_521,
	PunBehaviour_OnOwnershipRequest_m8_522,
	PunBehaviour_OnLobbyStatisticsUpdate_m8_523,
	PhotonMessageInfo__ctor_m8_524,
	PhotonMessageInfo__ctor_m8_525,
	PhotonMessageInfo_get_timestamp_m8_526,
	PhotonMessageInfo_ToString_m8_527,
	RoomOptions__ctor_m8_528,
	RoomOptions_get_isVisible_m8_529,
	RoomOptions_set_isVisible_m8_530,
	RoomOptions_get_isOpen_m8_531,
	RoomOptions_set_isOpen_m8_532,
	RoomOptions_get_cleanupCacheOnLeave_m8_533,
	RoomOptions_set_cleanupCacheOnLeave_m8_534,
	RoomOptions_get_suppressRoomEvents_m8_535,
	PunEvent__ctor_m8_536,
	PhotonStream__ctor_m8_537,
	PhotonStream_get_isWriting_m8_538,
	PhotonStream_get_isReading_m8_539,
	PhotonStream_get_Count_m8_540,
	PhotonStream_ReceiveNext_m8_541,
	PhotonStream_PeekNext_m8_542,
	PhotonStream_SendNext_m8_543,
	PhotonStream_ToArray_m8_544,
	PhotonStream_Serialize_m8_545,
	PhotonStream_Serialize_m8_546,
	PhotonStream_Serialize_m8_547,
	PhotonStream_Serialize_m8_548,
	PhotonStream_Serialize_m8_549,
	PhotonStream_Serialize_m8_550,
	PhotonStream_Serialize_m8_551,
	PhotonStream_Serialize_m8_552,
	PhotonStream_Serialize_m8_553,
	PhotonStream_Serialize_m8_554,
	WebRpcResponse__ctor_m8_555,
	WebRpcResponse_get_Name_m8_556,
	WebRpcResponse_set_Name_m8_557,
	WebRpcResponse_get_ReturnCode_m8_558,
	WebRpcResponse_set_ReturnCode_m8_559,
	WebRpcResponse_get_DebugMessage_m8_560,
	WebRpcResponse_set_DebugMessage_m8_561,
	WebRpcResponse_get_Parameters_m8_562,
	WebRpcResponse_set_Parameters_m8_563,
	WebRpcResponse_ToStringFull_m8_564,
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0__ctor_m8_565,
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_566,
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m8_567,
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_MoveNext_m8_568,
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_Dispose_m8_569,
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_Reset_m8_570,
	PhotonHandler__ctor_m8_571,
	PhotonHandler__cctor_m8_572,
	PhotonHandler_Awake_m8_573,
	PhotonHandler_OnApplicationQuit_m8_574,
	PhotonHandler_OnApplicationPause_m8_575,
	PhotonHandler_OnDestroy_m8_576,
	PhotonHandler_Update_m8_577,
	PhotonHandler_OnLevelWasLoaded_m8_578,
	PhotonHandler_OnJoinedRoom_m8_579,
	PhotonHandler_OnCreatedRoom_m8_580,
	PhotonHandler_StartFallbackSendAckThread_m8_581,
	PhotonHandler_StopFallbackSendAckThread_m8_582,
	PhotonHandler_FallbackSendAckThread_m8_583,
	PhotonHandler_get_BestRegionCodeInPreferences_m8_584,
	PhotonHandler_set_BestRegionCodeInPreferences_m8_585,
	PhotonHandler_PingAvailableRegionsAndConnectToBest_m8_586,
	PhotonHandler_PingAvailableRegionsCoroutine_m8_587,
	PhotonLagSimulationGui__ctor_m8_588,
	PhotonLagSimulationGui_get_Peer_m8_589,
	PhotonLagSimulationGui_set_Peer_m8_590,
	PhotonLagSimulationGui_Start_m8_591,
	PhotonLagSimulationGui_OnGUI_m8_592,
	PhotonLagSimulationGui_NetSimHasNoPeerWindow_m8_593,
	PhotonLagSimulationGui_NetSimWindow_m8_594,
	EventCallback__ctor_m8_595,
	EventCallback_Invoke_m8_596,
	EventCallback_BeginInvoke_m8_597,
	EventCallback_EndInvoke_m8_598,
	PhotonNetwork__cctor_m8_599,
	PhotonNetwork_get_gameVersion_m8_600,
	PhotonNetwork_set_gameVersion_m8_601,
	PhotonNetwork_get_ServerAddress_m8_602,
	PhotonNetwork_get_connected_m8_603,
	PhotonNetwork_get_connecting_m8_604,
	PhotonNetwork_get_connectedAndReady_m8_605,
	PhotonNetwork_get_connectionState_m8_606,
	PhotonNetwork_get_connectionStateDetailed_m8_607,
	PhotonNetwork_get_Server_m8_608,
	PhotonNetwork_get_AuthValues_m8_609,
	PhotonNetwork_set_AuthValues_m8_610,
	PhotonNetwork_get_room_m8_611,
	PhotonNetwork_get_player_m8_612,
	PhotonNetwork_get_masterClient_m8_613,
	PhotonNetwork_get_playerName_m8_614,
	PhotonNetwork_set_playerName_m8_615,
	PhotonNetwork_get_playerList_m8_616,
	PhotonNetwork_get_otherPlayers_m8_617,
	PhotonNetwork_get_Friends_m8_618,
	PhotonNetwork_set_Friends_m8_619,
	PhotonNetwork_get_FriendsListAge_m8_620,
	PhotonNetwork_get_PrefabPool_m8_621,
	PhotonNetwork_set_PrefabPool_m8_622,
	PhotonNetwork_get_offlineMode_m8_623,
	PhotonNetwork_set_offlineMode_m8_624,
	PhotonNetwork_get_automaticallySyncScene_m8_625,
	PhotonNetwork_set_automaticallySyncScene_m8_626,
	PhotonNetwork_get_autoCleanUpPlayerObjects_m8_627,
	PhotonNetwork_set_autoCleanUpPlayerObjects_m8_628,
	PhotonNetwork_get_autoJoinLobby_m8_629,
	PhotonNetwork_set_autoJoinLobby_m8_630,
	PhotonNetwork_get_EnableLobbyStatistics_m8_631,
	PhotonNetwork_set_EnableLobbyStatistics_m8_632,
	PhotonNetwork_get_LobbyStatistics_m8_633,
	PhotonNetwork_set_LobbyStatistics_m8_634,
	PhotonNetwork_get_insideLobby_m8_635,
	PhotonNetwork_get_lobby_m8_636,
	PhotonNetwork_set_lobby_m8_637,
	PhotonNetwork_get_sendRate_m8_638,
	PhotonNetwork_set_sendRate_m8_639,
	PhotonNetwork_get_sendRateOnSerialize_m8_640,
	PhotonNetwork_set_sendRateOnSerialize_m8_641,
	PhotonNetwork_get_isMessageQueueRunning_m8_642,
	PhotonNetwork_set_isMessageQueueRunning_m8_643,
	PhotonNetwork_get_unreliableCommandsLimit_m8_644,
	PhotonNetwork_set_unreliableCommandsLimit_m8_645,
	PhotonNetwork_get_time_m8_646,
	PhotonNetwork_get_ServerTimestamp_m8_647,
	PhotonNetwork_get_isMasterClient_m8_648,
	PhotonNetwork_get_inRoom_m8_649,
	PhotonNetwork_get_isNonMasterClientInRoom_m8_650,
	PhotonNetwork_get_countOfPlayersOnMaster_m8_651,
	PhotonNetwork_get_countOfPlayersInRooms_m8_652,
	PhotonNetwork_get_countOfPlayers_m8_653,
	PhotonNetwork_get_countOfRooms_m8_654,
	PhotonNetwork_get_NetworkStatisticsEnabled_m8_655,
	PhotonNetwork_set_NetworkStatisticsEnabled_m8_656,
	PhotonNetwork_get_ResentReliableCommands_m8_657,
	PhotonNetwork_get_CrcCheckEnabled_m8_658,
	PhotonNetwork_set_CrcCheckEnabled_m8_659,
	PhotonNetwork_get_PacketLossByCrcCheck_m8_660,
	PhotonNetwork_get_MaxResendsBeforeDisconnect_m8_661,
	PhotonNetwork_set_MaxResendsBeforeDisconnect_m8_662,
	PhotonNetwork_get_QuickResends_m8_663,
	PhotonNetwork_set_QuickResends_m8_664,
	PhotonNetwork_SwitchToProtocol_m8_665,
	PhotonNetwork_ConnectUsingSettings_m8_666,
	PhotonNetwork_ConnectToMaster_m8_667,
	PhotonNetwork_ConnectToBestCloudServer_m8_668,
	PhotonNetwork_ConnectToRegion_m8_669,
	PhotonNetwork_OverrideBestCloudServer_m8_670,
	PhotonNetwork_RefreshCloudServerRating_m8_671,
	PhotonNetwork_NetworkStatisticsReset_m8_672,
	PhotonNetwork_NetworkStatisticsToString_m8_673,
	PhotonNetwork_InitializeSecurity_m8_674,
	PhotonNetwork_VerifyCanUseNetwork_m8_675,
	PhotonNetwork_Disconnect_m8_676,
	PhotonNetwork_FindFriends_m8_677,
	PhotonNetwork_CreateRoom_m8_678,
	PhotonNetwork_CreateRoom_m8_679,
	PhotonNetwork_CreateRoom_m8_680,
	PhotonNetwork_JoinRoom_m8_681,
	PhotonNetwork_JoinOrCreateRoom_m8_682,
	PhotonNetwork_JoinRandomRoom_m8_683,
	PhotonNetwork_JoinRandomRoom_m8_684,
	PhotonNetwork_JoinRandomRoom_m8_685,
	PhotonNetwork_EnterOfflineRoom_m8_686,
	PhotonNetwork_JoinLobby_m8_687,
	PhotonNetwork_JoinLobby_m8_688,
	PhotonNetwork_LeaveLobby_m8_689,
	PhotonNetwork_LeaveRoom_m8_690,
	PhotonNetwork_GetRoomList_m8_691,
	PhotonNetwork_SetPlayerCustomProperties_m8_692,
	PhotonNetwork_RemovePlayerCustomProperties_m8_693,
	PhotonNetwork_RaiseEvent_m8_694,
	PhotonNetwork_AllocateViewID_m8_695,
	PhotonNetwork_AllocateSceneViewID_m8_696,
	PhotonNetwork_AllocateViewID_m8_697,
	PhotonNetwork_AllocateSceneViewIDs_m8_698,
	PhotonNetwork_UnAllocateViewID_m8_699,
	PhotonNetwork_Instantiate_m8_700,
	PhotonNetwork_Instantiate_m8_701,
	PhotonNetwork_InstantiateSceneObject_m8_702,
	PhotonNetwork_GetPing_m8_703,
	PhotonNetwork_FetchServerTimestamp_m8_704,
	PhotonNetwork_SendOutgoingCommands_m8_705,
	PhotonNetwork_CloseConnection_m8_706,
	PhotonNetwork_SetMasterClient_m8_707,
	PhotonNetwork_Destroy_m8_708,
	PhotonNetwork_Destroy_m8_709,
	PhotonNetwork_DestroyPlayerObjects_m8_710,
	PhotonNetwork_DestroyPlayerObjects_m8_711,
	PhotonNetwork_DestroyAll_m8_712,
	PhotonNetwork_RemoveRPCs_m8_713,
	PhotonNetwork_RemoveRPCs_m8_714,
	PhotonNetwork_RemoveRPCsInGroup_m8_715,
	PhotonNetwork_RPC_m8_716,
	PhotonNetwork_RPC_m8_717,
	PhotonNetwork_CacheSendMonoMessageTargets_m8_718,
	PhotonNetwork_FindGameObjectsWithComponent_m8_719,
	PhotonNetwork_SetReceivingEnabled_m8_720,
	PhotonNetwork_SetReceivingEnabled_m8_721,
	PhotonNetwork_SetSendingEnabled_m8_722,
	PhotonNetwork_SetSendingEnabled_m8_723,
	PhotonNetwork_SetLevelPrefix_m8_724,
	PhotonNetwork_LoadLevel_m8_725,
	PhotonNetwork_LoadLevel_m8_726,
	PhotonNetwork_WebRpc_m8_727,
	PhotonPlayer__ctor_m8_728,
	PhotonPlayer__ctor_m8_729,
	PhotonPlayer_get_ID_m8_730,
	PhotonPlayer_get_name_m8_731,
	PhotonPlayer_set_name_m8_732,
	PhotonPlayer_get_isMasterClient_m8_733,
	PhotonPlayer_get_customProperties_m8_734,
	PhotonPlayer_set_customProperties_m8_735,
	PhotonPlayer_get_allProperties_m8_736,
	PhotonPlayer_Equals_m8_737,
	PhotonPlayer_GetHashCode_m8_738,
	PhotonPlayer_InternalChangeLocalID_m8_739,
	PhotonPlayer_InternalCacheProperties_m8_740,
	PhotonPlayer_SetCustomProperties_m8_741,
	PhotonPlayer_Find_m8_742,
	PhotonPlayer_Get_m8_743,
	PhotonPlayer_GetNext_m8_744,
	PhotonPlayer_GetNextFor_m8_745,
	PhotonPlayer_GetNextFor_m8_746,
	PhotonPlayer_ToString_m8_747,
	PhotonPlayer_ToStringFull_m8_748,
	PhotonStatsGui__ctor_m8_749,
	PhotonStatsGui_Start_m8_750,
	PhotonStatsGui_Update_m8_751,
	PhotonStatsGui_OnGUI_m8_752,
	PhotonStatsGui_TrafficStatsWindow_m8_753,
	PhotonStreamQueue__ctor_m8_754,
	PhotonStreamQueue_BeginWritePackage_m8_755,
	PhotonStreamQueue_Reset_m8_756,
	PhotonStreamQueue_SendNext_m8_757,
	PhotonStreamQueue_HasQueuedObjects_m8_758,
	PhotonStreamQueue_ReceiveNext_m8_759,
	PhotonStreamQueue_Serialize_m8_760,
	PhotonStreamQueue_Deserialize_m8_761,
	PhotonView__ctor_m8_762,
	PhotonView_get_prefix_m8_763,
	PhotonView_set_prefix_m8_764,
	PhotonView_get_instantiationData_m8_765,
	PhotonView_set_instantiationData_m8_766,
	PhotonView_get_viewID_m8_767,
	PhotonView_set_viewID_m8_768,
	PhotonView_get_isSceneView_m8_769,
	PhotonView_get_owner_m8_770,
	PhotonView_get_OwnerActorNr_m8_771,
	PhotonView_get_isOwnerActive_m8_772,
	PhotonView_get_CreatorActorNr_m8_773,
	PhotonView_get_isMine_m8_774,
	PhotonView_Awake_m8_775,
	PhotonView_RequestOwnership_m8_776,
	PhotonView_TransferOwnership_m8_777,
	PhotonView_TransferOwnership_m8_778,
	PhotonView_OnDestroy_m8_779,
	PhotonView_SerializeView_m8_780,
	PhotonView_DeserializeView_m8_781,
	PhotonView_DeserializeComponent_m8_782,
	PhotonView_SerializeComponent_m8_783,
	PhotonView_ExecuteComponentOnSerialize_m8_784,
	PhotonView_RefreshRpcMonoBehaviourCache_m8_785,
	PhotonView_RPC_m8_786,
	PhotonView_RpcSecure_m8_787,
	PhotonView_RPC_m8_788,
	PhotonView_RpcSecure_m8_789,
	PhotonView_Get_m8_790,
	PhotonView_Get_m8_791,
	PhotonView_Find_m8_792,
	PhotonView_ToString_m8_793,
	U3CPingSocketU3Ec__Iterator1__ctor_m8_794,
	U3CPingSocketU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_795,
	U3CPingSocketU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m8_796,
	U3CPingSocketU3Ec__Iterator1_MoveNext_m8_797,
	U3CPingSocketU3Ec__Iterator1_Dispose_m8_798,
	U3CPingSocketU3Ec__Iterator1_Reset_m8_799,
	PhotonPingManager__ctor_m8_800,
	PhotonPingManager__cctor_m8_801,
	PhotonPingManager_get_BestRegion_m8_802,
	PhotonPingManager_get_Done_m8_803,
	PhotonPingManager_PingSocket_m8_804,
	PhotonPingManager_ResolveHost_m8_805,
	PunRPC__ctor_m8_806,
	Room__ctor_m8_807,
	Room_get_playerCount_m8_808,
	Room_get_name_m8_809,
	Room_set_name_m8_810,
	Room_get_maxPlayers_m8_811,
	Room_set_maxPlayers_m8_812,
	Room_get_open_m8_813,
	Room_set_open_m8_814,
	Room_get_visible_m8_815,
	Room_set_visible_m8_816,
	Room_get_propertiesListedInLobby_m8_817,
	Room_set_propertiesListedInLobby_m8_818,
	Room_get_autoCleanUp_m8_819,
	Room_get_masterClientId_m8_820,
	Room_set_masterClientId_m8_821,
	Room_SetCustomProperties_m8_822,
	Room_SetPropertiesListedInLobby_m8_823,
	Room_ToString_m8_824,
	Room_ToStringFull_m8_825,
	RoomInfo__ctor_m8_826,
	RoomInfo_get_removedFromList_m8_827,
	RoomInfo_set_removedFromList_m8_828,
	RoomInfo_get_serverSideMasterClient_m8_829,
	RoomInfo_set_serverSideMasterClient_m8_830,
	RoomInfo_get_customProperties_m8_831,
	RoomInfo_get_name_m8_832,
	RoomInfo_get_playerCount_m8_833,
	RoomInfo_set_playerCount_m8_834,
	RoomInfo_get_isLocalClientInside_m8_835,
	RoomInfo_set_isLocalClientInside_m8_836,
	RoomInfo_get_maxPlayers_m8_837,
	RoomInfo_get_open_m8_838,
	RoomInfo_get_visible_m8_839,
	RoomInfo_Equals_m8_840,
	RoomInfo_GetHashCode_m8_841,
	RoomInfo_ToString_m8_842,
	RoomInfo_ToStringFull_m8_843,
	RoomInfo_InternalCacheProperties_m8_844,
	Region__ctor_m8_845,
	Region_Parse_m8_846,
	Region_ParseFlag_m8_847,
	Region_ToString_m8_848,
	ServerSettings__ctor_m8_849,
	ServerSettings_UseCloudBestRegion_m8_850,
	ServerSettings_UseCloud_m8_851,
	ServerSettings_UseCloud_m8_852,
	ServerSettings_UseMyServer_m8_853,
	ServerSettings_ToString_m8_854,
	SynchronizedParameter__ctor_m8_855,
	SynchronizedLayer__ctor_m8_856,
	U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3__ctor_m8_857,
	U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey3_U3CU3Em__0_m8_858,
	U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4__ctor_m8_859,
	U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey4_U3CU3Em__1_m8_860,
	U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5__ctor_m8_861,
	U3CGetLayerSynchronizeTypeU3Ec__AnonStorey5_U3CU3Em__2_m8_862,
	U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6__ctor_m8_863,
	U3CGetParameterSynchronizeTypeU3Ec__AnonStorey6_U3CU3Em__3_m8_864,
	U3CSetLayerSynchronizedU3Ec__AnonStorey7__ctor_m8_865,
	U3CSetLayerSynchronizedU3Ec__AnonStorey7_U3CU3Em__4_m8_866,
	U3CSetParameterSynchronizedU3Ec__AnonStorey8__ctor_m8_867,
	U3CSetParameterSynchronizedU3Ec__AnonStorey8_U3CU3Em__5_m8_868,
	PhotonAnimatorView__ctor_m8_869,
	PhotonAnimatorView_Awake_m8_870,
	PhotonAnimatorView_Update_m8_871,
	PhotonAnimatorView_DoesLayerSynchronizeTypeExist_m8_872,
	PhotonAnimatorView_DoesParameterSynchronizeTypeExist_m8_873,
	PhotonAnimatorView_GetSynchronizedLayers_m8_874,
	PhotonAnimatorView_GetSynchronizedParameters_m8_875,
	PhotonAnimatorView_GetLayerSynchronizeType_m8_876,
	PhotonAnimatorView_GetParameterSynchronizeType_m8_877,
	PhotonAnimatorView_SetLayerSynchronized_m8_878,
	PhotonAnimatorView_SetParameterSynchronized_m8_879,
	PhotonAnimatorView_SerializeDataContinuously_m8_880,
	PhotonAnimatorView_DeserializeDataContinuously_m8_881,
	PhotonAnimatorView_SerializeDataDiscretly_m8_882,
	PhotonAnimatorView_DeserializeDataDiscretly_m8_883,
	PhotonAnimatorView_SerializeSynchronizationTypeState_m8_884,
	PhotonAnimatorView_DeserializeSynchronizationTypeState_m8_885,
	PhotonAnimatorView_OnPhotonSerializeView_m8_886,
	PhotonRigidbody2DView__ctor_m8_887,
	PhotonRigidbody2DView_Awake_m8_888,
	PhotonRigidbody2DView_OnPhotonSerializeView_m8_889,
	PhotonRigidbodyView__ctor_m8_890,
	PhotonRigidbodyView_Awake_m8_891,
	PhotonRigidbodyView_OnPhotonSerializeView_m8_892,
	PhotonTransformView__ctor_m8_893,
	PhotonTransformView_Awake_m8_894,
	PhotonTransformView_Update_m8_895,
	PhotonTransformView_UpdatePosition_m8_896,
	PhotonTransformView_UpdateRotation_m8_897,
	PhotonTransformView_UpdateScale_m8_898,
	PhotonTransformView_SetSynchronizedValues_m8_899,
	PhotonTransformView_OnPhotonSerializeView_m8_900,
	PhotonTransformView_DoDrawEstimatedPositionError_m8_901,
	PhotonTransformViewPositionControl__ctor_m8_902,
	PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m8_903,
	PhotonTransformViewPositionControl_SetSynchronizedValues_m8_904,
	PhotonTransformViewPositionControl_UpdatePosition_m8_905,
	PhotonTransformViewPositionControl_GetNetworkPosition_m8_906,
	PhotonTransformViewPositionControl_GetExtrapolatedPositionOffset_m8_907,
	PhotonTransformViewPositionControl_OnPhotonSerializeView_m8_908,
	PhotonTransformViewPositionControl_SerializeData_m8_909,
	PhotonTransformViewPositionControl_DeserializeData_m8_910,
	PhotonTransformViewPositionModel__ctor_m8_911,
	PhotonTransformViewRotationControl__ctor_m8_912,
	PhotonTransformViewRotationControl_GetRotation_m8_913,
	PhotonTransformViewRotationControl_OnPhotonSerializeView_m8_914,
	PhotonTransformViewRotationModel__ctor_m8_915,
	PhotonTransformViewScaleControl__ctor_m8_916,
	PhotonTransformViewScaleControl_GetScale_m8_917,
	PhotonTransformViewScaleControl_OnPhotonSerializeView_m8_918,
	PhotonTransformViewScaleModel__ctor_m8_919,
	ConnectAndJoinRandom__ctor_m8_920,
	ConnectAndJoinRandom_Start_m8_921,
	ConnectAndJoinRandom_Update_m8_922,
	ConnectAndJoinRandom_OnConnectedToMaster_m8_923,
	ConnectAndJoinRandom_OnJoinedLobby_m8_924,
	ConnectAndJoinRandom_OnPhotonRandomJoinFailed_m8_925,
	ConnectAndJoinRandom_OnFailedToConnectToPhoton_m8_926,
	ConnectAndJoinRandom_OnJoinedRoom_m8_927,
	HighlightOwnedGameObj__ctor_m8_928,
	HighlightOwnedGameObj_Update_m8_929,
	InRoomChat__ctor_m8_930,
	InRoomChat__cctor_m8_931,
	InRoomChat_Start_m8_932,
	InRoomChat_OnGUI_m8_933,
	InRoomChat_Chat_m8_934,
	InRoomChat_AddLine_m8_935,
	InRoomRoundTimer__ctor_m8_936,
	InRoomRoundTimer_StartRoundNow_m8_937,
	InRoomRoundTimer_OnJoinedRoom_m8_938,
	InRoomRoundTimer_OnPhotonCustomRoomPropertiesChanged_m8_939,
	InRoomRoundTimer_OnMasterClientSwitched_m8_940,
	InRoomRoundTimer_Update_m8_941,
	InRoomRoundTimer_OnGUI_m8_942,
	InputToEvent__ctor_m8_943,
	InputToEvent_get_goPointedAt_m8_944,
	InputToEvent_set_goPointedAt_m8_945,
	InputToEvent_get_DragVector_m8_946,
	InputToEvent_Start_m8_947,
	InputToEvent_Update_m8_948,
	InputToEvent_Press_m8_949,
	InputToEvent_Release_m8_950,
	InputToEvent_RaycastObject_m8_951,
	ManualPhotonViewAllocator__ctor_m8_952,
	ManualPhotonViewAllocator_AllocateManualPhotonView_m8_953,
	ManualPhotonViewAllocator_InstantiateRpc_m8_954,
	MoveByKeys__ctor_m8_955,
	MoveByKeys_Start_m8_956,
	MoveByKeys_FixedUpdate_m8_957,
	OnAwakeUsePhotonView__ctor_m8_958,
	OnAwakeUsePhotonView_Awake_m8_959,
	OnAwakeUsePhotonView_Start_m8_960,
	OnAwakeUsePhotonView_OnAwakeRPC_m8_961,
	OnAwakeUsePhotonView_OnAwakeRPC_m8_962,
	U3CDestroyRpcU3Ec__Iterator2__ctor_m8_963,
	U3CDestroyRpcU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_964,
	U3CDestroyRpcU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m8_965,
	U3CDestroyRpcU3Ec__Iterator2_MoveNext_m8_966,
	U3CDestroyRpcU3Ec__Iterator2_Dispose_m8_967,
	U3CDestroyRpcU3Ec__Iterator2_Reset_m8_968,
	OnClickDestroy__ctor_m8_969,
	OnClickDestroy_OnClick_m8_970,
	OnClickDestroy_DestroyRpc_m8_971,
	OnClickInstantiate__ctor_m8_972,
	OnClickInstantiate_OnClick_m8_973,
	OnClickInstantiate_OnGUI_m8_974,
	OnClickLoadSomething__ctor_m8_975,
	OnClickLoadSomething_OnClick_m8_976,
	OnJoinedInstantiate__ctor_m8_977,
	OnJoinedInstantiate_OnJoinedRoom_m8_978,
	OnStartDelete__ctor_m8_979,
	OnStartDelete_Start_m8_980,
	PickupItem__ctor_m8_981,
	PickupItem__cctor_m8_982,
	PickupItem_get_ViewID_m8_983,
	PickupItem_OnTriggerEnter_m8_984,
	PickupItem_OnPhotonSerializeView_m8_985,
	PickupItem_Pickup_m8_986,
	PickupItem_Drop_m8_987,
	PickupItem_Drop_m8_988,
	PickupItem_PunPickup_m8_989,
	PickupItem_PickedUp_m8_990,
	PickupItem_PunRespawn_m8_991,
	PickupItem_PunRespawn_m8_992,
	PickupItemSimple__ctor_m8_993,
	PickupItemSimple_OnTriggerEnter_m8_994,
	PickupItemSimple_Pickup_m8_995,
	PickupItemSimple_PunPickupSimple_m8_996,
	PickupItemSimple_RespawnAfter_m8_997,
	PickupItemSyncer__ctor_m8_998,
	PickupItemSyncer_OnPhotonPlayerConnected_m8_999,
	PickupItemSyncer_OnJoinedRoom_m8_1000,
	PickupItemSyncer_AskForPickupItemSpawnTimes_m8_1001,
	PickupItemSyncer_RequestForPickupTimes_m8_1002,
	PickupItemSyncer_SendPickedUpItems_m8_1003,
	PickupItemSyncer_PickupItemInit_m8_1004,
	PointedAtGameObjectInfo__ctor_m8_1005,
	PointedAtGameObjectInfo_OnGUI_m8_1006,
	PunPlayerScores__ctor_m8_1007,
	ScoreExtensions_SetScore_m8_1008,
	ScoreExtensions_AddScore_m8_1009,
	ScoreExtensions_GetScore_m8_1010,
	PunTeams__ctor_m8_1011,
	PunTeams_Start_m8_1012,
	PunTeams_OnJoinedRoom_m8_1013,
	PunTeams_OnPhotonPlayerPropertiesChanged_m8_1014,
	PunTeams_UpdateTeams_m8_1015,
	TeamExtensions_GetTeam_m8_1016,
	TeamExtensions_SetTeam_m8_1017,
	QuitOnEscapeOrBack__ctor_m8_1018,
	QuitOnEscapeOrBack_Update_m8_1019,
	ServerTime__ctor_m8_1020,
	ServerTime_OnGUI_m8_1021,
	ShowInfoOfPlayer__ctor_m8_1022,
	ShowInfoOfPlayer_Start_m8_1023,
	ShowInfoOfPlayer_Update_m8_1024,
	ShowStatusWhenConnecting__ctor_m8_1025,
	ShowStatusWhenConnecting_OnGUI_m8_1026,
	ShowStatusWhenConnecting_GetConnectingDots_m8_1027,
	SmoothSyncMovement__ctor_m8_1028,
	SmoothSyncMovement_Awake_m8_1029,
	SmoothSyncMovement_OnPhotonSerializeView_m8_1030,
	SmoothSyncMovement_Update_m8_1031,
	SupportLogger__ctor_m8_1032,
	SupportLogger_Start_m8_1033,
	SupportLogging__ctor_m8_1034,
	SupportLogging_Start_m8_1035,
	SupportLogging_OnApplicationPause_m8_1036,
	SupportLogging_OnApplicationQuit_m8_1037,
	SupportLogging_LogStats_m8_1038,
	SupportLogging_LogBasics_m8_1039,
	SupportLogging_OnConnectedToPhoton_m8_1040,
	SupportLogging_OnFailedToConnectToPhoton_m8_1041,
	SupportLogging_OnJoinedLobby_m8_1042,
	SupportLogging_OnJoinedRoom_m8_1043,
	SupportLogging_OnCreatedRoom_m8_1044,
	SupportLogging_OnLeftRoom_m8_1045,
	SupportLogging_OnDisconnectedFromPhoton_m8_1046,
	TimeKeeper__ctor_m8_1047,
	TimeKeeper_get_Interval_m8_1048,
	TimeKeeper_set_Interval_m8_1049,
	TimeKeeper_get_IsEnabled_m8_1050,
	TimeKeeper_set_IsEnabled_m8_1051,
	TimeKeeper_get_ShouldExecute_m8_1052,
	TimeKeeper_set_ShouldExecute_m8_1053,
	TimeKeeper_Reset_m8_1054,
};
