﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.DictionaryEntryEnumerator
struct DictionaryEntryEnumerator_t5_2;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_458;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void ExitGames.Client.Photon.DictionaryEntryEnumerator::.ctor(System.Collections.IDictionaryEnumerator)
extern "C" void DictionaryEntryEnumerator__ctor_m5_6 (DictionaryEntryEnumerator_t5_2 * __this, Object_t * ___original, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.DictionaryEntryEnumerator::MoveNext()
extern "C" bool DictionaryEntryEnumerator_MoveNext_m5_7 (DictionaryEntryEnumerator_t5_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.DictionaryEntryEnumerator::Reset()
extern "C" void DictionaryEntryEnumerator_Reset_m5_8 (DictionaryEntryEnumerator_t5_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExitGames.Client.Photon.DictionaryEntryEnumerator::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * DictionaryEntryEnumerator_System_Collections_IEnumerator_get_Current_m5_9 (DictionaryEntryEnumerator_t5_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry ExitGames.Client.Photon.DictionaryEntryEnumerator::get_Current()
extern "C" DictionaryEntry_t1_167  DictionaryEntryEnumerator_get_Current_m5_10 (DictionaryEntryEnumerator_t5_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.DictionaryEntryEnumerator::Dispose()
extern "C" void DictionaryEntryEnumerator_Dispose_m5_11 (DictionaryEntryEnumerator_t5_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
