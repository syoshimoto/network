﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.EnetChannel
struct EnetChannel_t5_5;
// ExitGames.Client.Photon.NCommand
struct NCommand_t5_13;

#include "codegen/il2cpp-codegen.h"

// System.Void ExitGames.Client.Photon.EnetChannel::.ctor(System.Byte,System.Int32)
extern "C" void EnetChannel__ctor_m5_58 (EnetChannel_t5_5 * __this, uint8_t ___channelNumber, int32_t ___commandBufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.EnetChannel::ContainsUnreliableSequenceNumber(System.Int32)
extern "C" bool EnetChannel_ContainsUnreliableSequenceNumber_m5_59 (EnetChannel_t5_5 * __this, int32_t ___unreliableSequenceNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.EnetChannel::ContainsReliableSequenceNumber(System.Int32)
extern "C" bool EnetChannel_ContainsReliableSequenceNumber_m5_60 (EnetChannel_t5_5 * __this, int32_t ___reliableSequenceNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.NCommand ExitGames.Client.Photon.EnetChannel::FetchReliableSequenceNumber(System.Int32)
extern "C" NCommand_t5_13 * EnetChannel_FetchReliableSequenceNumber_m5_61 (EnetChannel_t5_5 * __this, int32_t ___reliableSequenceNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetChannel::clearAll()
extern "C" void EnetChannel_clearAll_m5_62 (EnetChannel_t5_5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
