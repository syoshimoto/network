﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.EnetPeer
struct EnetPeer_t5_19;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1_888;
// System.Byte[]
struct ByteU5BU5D_t1_71;
// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>
struct Queue_1_t3_181;
// ExitGames.Client.Photon.NCommand
struct NCommand_t5_13;

#include "codegen/il2cpp-codegen.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase_EgMessageTyp.h"

// System.Void ExitGames.Client.Photon.EnetPeer::.ctor()
extern "C" void EnetPeer__ctor_m5_108 (EnetPeer_t5_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer::InitPeerBase()
extern "C" void EnetPeer_InitPeerBase_m5_109 (EnetPeer_t5_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.EnetPeer::Connect(System.String,System.String)
extern "C" bool EnetPeer_Connect_m5_110 (EnetPeer_t5_19 * __this, String_t* ___ipport, String_t* ___appID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer::Disconnect()
extern "C" void EnetPeer_Disconnect_m5_111 (EnetPeer_t5_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer::StopConnection()
extern "C" void EnetPeer_StopConnection_m5_112 (EnetPeer_t5_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer::FetchServerTimestamp()
extern "C" void EnetPeer_FetchServerTimestamp_m5_113 (EnetPeer_t5_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.EnetPeer::DispatchIncomingCommands()
extern "C" bool EnetPeer_DispatchIncomingCommands_m5_114 (EnetPeer_t5_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.EnetPeer::SendAcksOnly()
extern "C" bool EnetPeer_SendAcksOnly_m5_115 (EnetPeer_t5_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.EnetPeer::SendOutgoingCommands()
extern "C" bool EnetPeer_SendOutgoingCommands_m5_116 (EnetPeer_t5_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.EnetPeer::AreReliableCommandsInTransit()
extern "C" bool EnetPeer_AreReliableCommandsInTransit_m5_117 (EnetPeer_t5_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.EnetPeer::EnqueueOperation(System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Byte,System.Boolean,System.Byte,System.Boolean,ExitGames.Client.Photon.PeerBase/EgMessageType)
extern "C" bool EnetPeer_EnqueueOperation_m5_118 (EnetPeer_t5_19 * __this, Dictionary_2_t1_888 * ___parameters, uint8_t ___opCode, bool ___sendReliable, uint8_t ___channelId, bool ___encrypt, uint8_t ___messageType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.EnetPeer::CreateAndEnqueueCommand(System.Byte,System.Byte[],System.Byte)
extern "C" bool EnetPeer_CreateAndEnqueueCommand_m5_119 (EnetPeer_t5_19 * __this, uint8_t ___commandType, ByteU5BU5D_t1_71* ___payload, uint8_t ___channelNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExitGames.Client.Photon.EnetPeer::SerializeOperationToMessage(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,ExitGames.Client.Photon.PeerBase/EgMessageType,System.Boolean)
extern "C" ByteU5BU5D_t1_71* EnetPeer_SerializeOperationToMessage_m5_120 (EnetPeer_t5_19 * __this, uint8_t ___opc, Dictionary_2_t1_888 * ___parameters, uint8_t ___messageType, bool ___encrypt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.EnetPeer::SerializeToBuffer(System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>)
extern "C" int32_t EnetPeer_SerializeToBuffer_m5_121 (EnetPeer_t5_19 * __this, Queue_1_t3_181 * ___commandList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer::SendData(System.Byte[],System.Int32)
extern "C" void EnetPeer_SendData_m5_122 (EnetPeer_t5_19 * __this, ByteU5BU5D_t1_71* ___data, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer::QueueSentCommand(ExitGames.Client.Photon.NCommand)
extern "C" void EnetPeer_QueueSentCommand_m5_123 (EnetPeer_t5_19 * __this, NCommand_t5_13 * ___command, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer::QueueOutgoingReliableCommand(ExitGames.Client.Photon.NCommand)
extern "C" void EnetPeer_QueueOutgoingReliableCommand_m5_124 (EnetPeer_t5_19 * __this, NCommand_t5_13 * ___command, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer::QueueOutgoingUnreliableCommand(ExitGames.Client.Photon.NCommand)
extern "C" void EnetPeer_QueueOutgoingUnreliableCommand_m5_125 (EnetPeer_t5_19 * __this, NCommand_t5_13 * ___command, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer::QueueOutgoingAcknowledgement(ExitGames.Client.Photon.NCommand)
extern "C" void EnetPeer_QueueOutgoingAcknowledgement_m5_126 (EnetPeer_t5_19 * __this, NCommand_t5_13 * ___command, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer::ReceiveIncomingCommands(System.Byte[],System.Int32)
extern "C" void EnetPeer_ReceiveIncomingCommands_m5_127 (EnetPeer_t5_19 * __this, ByteU5BU5D_t1_71* ___inBuff, int32_t ___dataLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.EnetPeer::ExecuteCommand(ExitGames.Client.Photon.NCommand)
extern "C" bool EnetPeer_ExecuteCommand_m5_128 (EnetPeer_t5_19 * __this, NCommand_t5_13 * ___command, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.EnetPeer::QueueIncomingCommand(ExitGames.Client.Photon.NCommand)
extern "C" bool EnetPeer_QueueIncomingCommand_m5_129 (EnetPeer_t5_19 * __this, NCommand_t5_13 * ___command, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.NCommand ExitGames.Client.Photon.EnetPeer::RemoveSentReliableCommand(System.Int32,System.Int32)
extern "C" NCommand_t5_13 * EnetPeer_RemoveSentReliableCommand_m5_130 (EnetPeer_t5_19 * __this, int32_t ___ackReceivedReliableSequenceNumber, int32_t ___ackReceivedChannel, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer::<ExecuteCommand>b__13()
extern "C" void EnetPeer_U3CExecuteCommandU3Eb__13_m5_131 (EnetPeer_t5_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer::.cctor()
extern "C" void EnetPeer__cctor_m5_132 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
