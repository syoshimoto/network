﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExitGames.Client.Photon.NCommand
struct NCommand_t5_13;
// ExitGames.Client.Photon.EnetPeer
struct EnetPeer_t5_19;

#include "mscorlib_System_Object.h"

// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass11
struct  U3CU3Ec__DisplayClass11_t5_21  : public Object_t
{
	// ExitGames.Client.Photon.NCommand ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass11::readCommand
	NCommand_t5_13 * ___readCommand_0;
	// ExitGames.Client.Photon.EnetPeer ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass11::<>4__this
	EnetPeer_t5_19 * ___U3CU3E4__this_1;
};
