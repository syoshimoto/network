﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass11
struct U3CU3Ec__DisplayClass11_t5_21;

#include "codegen/il2cpp-codegen.h"

// System.Void ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass11::.ctor()
extern "C" void U3CU3Ec__DisplayClass11__ctor_m5_106 (U3CU3Ec__DisplayClass11_t5_21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EnetPeer/<>c__DisplayClass11::<ReceiveIncomingCommands>b__f()
extern "C" void U3CU3Ec__DisplayClass11_U3CReceiveIncomingCommandsU3Eb__f_m5_107 (U3CU3Ec__DisplayClass11_t5_21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
