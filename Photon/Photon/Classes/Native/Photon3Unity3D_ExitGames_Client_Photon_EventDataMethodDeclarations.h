﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.EventData
struct EventData_t5_44;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Object ExitGames.Client.Photon.EventData::get_Item(System.Byte)
extern "C" Object_t * EventData_get_Item_m5_265 (EventData_t5_44 * __this, uint8_t ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.EventData::ToString()
extern "C" String_t* EventData_ToString_m5_266 (EventData_t5_44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.EventData::.ctor()
extern "C" void EventData__ctor_m5_267 (EventData_t5_44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
