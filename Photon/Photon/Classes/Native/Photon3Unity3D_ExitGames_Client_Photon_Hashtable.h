﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExitGames.Client.Photon.DictionaryEntryEnumerator
struct DictionaryEntryEnumerator_t5_2;

#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1.h"

// ExitGames.Client.Photon.Hashtable
struct  Hashtable_t5_1  : public Dictionary_2_t1_884
{
	// ExitGames.Client.Photon.DictionaryEntryEnumerator ExitGames.Client.Photon.Hashtable::enumerator
	DictionaryEntryEnumerator_t5_2 * ___enumerator_16;
};
