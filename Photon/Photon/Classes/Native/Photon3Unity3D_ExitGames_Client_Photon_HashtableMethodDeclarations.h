﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.Hashtable
struct Hashtable_t5_1;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>
struct IEnumerator_1_t1_892;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ExitGames.Client.Photon.Hashtable::.ctor()
extern "C" void Hashtable__ctor_m5_0 (Hashtable_t5_1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Hashtable::.ctor(System.Int32)
extern "C" void Hashtable__ctor_m5_1 (Hashtable_t5_1 * __this, int32_t ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExitGames.Client.Photon.Hashtable::get_Item(System.Object)
extern "C" Object_t * Hashtable_get_Item_m5_2 (Hashtable_t5_1 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.Hashtable::set_Item(System.Object,System.Object)
extern "C" void Hashtable_set_Item_m5_3 (Hashtable_t5_1 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry> ExitGames.Client.Photon.Hashtable::GetEnumerator()
extern "C" Object_t* Hashtable_GetEnumerator_m5_4 (Hashtable_t5_1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.Hashtable::ToString()
extern "C" String_t* Hashtable_ToString_m5_5 (Hashtable_t5_1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
