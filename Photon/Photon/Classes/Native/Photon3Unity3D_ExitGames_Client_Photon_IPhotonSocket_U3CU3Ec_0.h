﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass3
struct U3CU3Ec__DisplayClass3_t5_26;
// System.Byte[]
struct ByteU5BU5D_t1_71;

#include "mscorlib_System_Object.h"

// ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass5
struct  U3CU3Ec__DisplayClass5_t5_27  : public Object_t
{
	// ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass3 ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass5::CS$<>8__locals4
	U3CU3Ec__DisplayClass3_t5_26 * ___CSU24U3CU3E8__locals4_0;
	// System.Byte[] ExitGames.Client.Photon.IPhotonSocket/<>c__DisplayClass5::inBufferCopy
	ByteU5BU5D_t1_71* ___inBufferCopy_1;
};
