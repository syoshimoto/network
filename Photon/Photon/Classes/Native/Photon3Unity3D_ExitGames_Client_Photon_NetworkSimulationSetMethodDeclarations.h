﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.NetworkSimulationSet
struct NetworkSimulationSet_t5_16;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Boolean ExitGames.Client.Photon.NetworkSimulationSet::get_IsSimulationEnabled()
extern "C" bool NetworkSimulationSet_get_IsSimulationEnabled_m5_166 (NetworkSimulationSet_t5_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_IsSimulationEnabled(System.Boolean)
extern "C" void NetworkSimulationSet_set_IsSimulationEnabled_m5_167 (NetworkSimulationSet_t5_16 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_OutgoingLag()
extern "C" int32_t NetworkSimulationSet_get_OutgoingLag_m5_168 (NetworkSimulationSet_t5_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_OutgoingLag(System.Int32)
extern "C" void NetworkSimulationSet_set_OutgoingLag_m5_169 (NetworkSimulationSet_t5_16 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_OutgoingJitter()
extern "C" int32_t NetworkSimulationSet_get_OutgoingJitter_m5_170 (NetworkSimulationSet_t5_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_OutgoingJitter(System.Int32)
extern "C" void NetworkSimulationSet_set_OutgoingJitter_m5_171 (NetworkSimulationSet_t5_16 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_OutgoingLossPercentage()
extern "C" int32_t NetworkSimulationSet_get_OutgoingLossPercentage_m5_172 (NetworkSimulationSet_t5_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_OutgoingLossPercentage(System.Int32)
extern "C" void NetworkSimulationSet_set_OutgoingLossPercentage_m5_173 (NetworkSimulationSet_t5_16 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_IncomingLag()
extern "C" int32_t NetworkSimulationSet_get_IncomingLag_m5_174 (NetworkSimulationSet_t5_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_IncomingLag(System.Int32)
extern "C" void NetworkSimulationSet_set_IncomingLag_m5_175 (NetworkSimulationSet_t5_16 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_IncomingJitter()
extern "C" int32_t NetworkSimulationSet_get_IncomingJitter_m5_176 (NetworkSimulationSet_t5_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_IncomingJitter(System.Int32)
extern "C" void NetworkSimulationSet_set_IncomingJitter_m5_177 (NetworkSimulationSet_t5_16 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_IncomingLossPercentage()
extern "C" int32_t NetworkSimulationSet_get_IncomingLossPercentage_m5_178 (NetworkSimulationSet_t5_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_IncomingLossPercentage(System.Int32)
extern "C" void NetworkSimulationSet_set_IncomingLossPercentage_m5_179 (NetworkSimulationSet_t5_16 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_LostPackagesOut()
extern "C" int32_t NetworkSimulationSet_get_LostPackagesOut_m5_180 (NetworkSimulationSet_t5_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_LostPackagesOut(System.Int32)
extern "C" void NetworkSimulationSet_set_LostPackagesOut_m5_181 (NetworkSimulationSet_t5_16 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_LostPackagesIn()
extern "C" int32_t NetworkSimulationSet_get_LostPackagesIn_m5_182 (NetworkSimulationSet_t5_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_LostPackagesIn(System.Int32)
extern "C" void NetworkSimulationSet_set_LostPackagesIn_m5_183 (NetworkSimulationSet_t5_16 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.NetworkSimulationSet::ToString()
extern "C" String_t* NetworkSimulationSet_ToString_m5_184 (NetworkSimulationSet_t5_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.NetworkSimulationSet::.ctor()
extern "C" void NetworkSimulationSet__ctor_m5_185 (NetworkSimulationSet_t5_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
