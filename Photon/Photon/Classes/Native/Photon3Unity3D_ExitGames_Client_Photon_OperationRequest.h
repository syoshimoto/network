﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1_888;

#include "mscorlib_System_Object.h"

// ExitGames.Client.Photon.OperationRequest
struct  OperationRequest_t5_42  : public Object_t
{
	// System.Byte ExitGames.Client.Photon.OperationRequest::OperationCode
	uint8_t ___OperationCode_0;
	// System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.OperationRequest::Parameters
	Dictionary_2_t1_888 * ___Parameters_1;
};
