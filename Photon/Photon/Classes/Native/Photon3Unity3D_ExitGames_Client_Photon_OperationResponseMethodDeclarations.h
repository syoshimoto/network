﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.OperationResponse
struct OperationResponse_t5_43;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Object ExitGames.Client.Photon.OperationResponse::get_Item(System.Byte)
extern "C" Object_t * OperationResponse_get_Item_m5_261 (OperationResponse_t5_43 * __this, uint8_t ___parameterCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.OperationResponse::ToString()
extern "C" String_t* OperationResponse_ToString_m5_262 (OperationResponse_t5_43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.OperationResponse::ToStringFull()
extern "C" String_t* OperationResponse_ToStringFull_m5_263 (OperationResponse_t5_43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.OperationResponse::.ctor()
extern "C" void OperationResponse__ctor_m5_264 (OperationResponse_t5_43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
