﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.PhotonPeer
struct PhotonPeer_t5_38;
// System.Type
struct Type_t;
// ExitGames.Client.Photon.IPhotonPeerListener
struct IPhotonPeerListener_t5_17;
// ExitGames.Client.Photon.TrafficStats
struct TrafficStats_t5_14;
// ExitGames.Client.Photon.TrafficStatsGameLevel
struct TrafficStatsGameLevel_t5_15;
// System.String
struct String_t;
// ExitGames.Client.Photon.NetworkSimulationSet
struct NetworkSimulationSet_t5_16;
// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1_888;
// ExitGames.Client.Photon.SerializeStreamMethod
struct SerializeStreamMethod_t5_47;
// ExitGames.Client.Photon.DeserializeStreamMethod
struct DeserializeStreamMethod_t5_49;

#include "codegen/il2cpp-codegen.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DebugLevel.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerStateValue.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"

// System.Void ExitGames.Client.Photon.PhotonPeer::set_SocketImplementation(System.Type)
extern "C" void PhotonPeer_set_SocketImplementation_m5_197 (PhotonPeer_t5_38 * __this, Type_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::set_DebugOut(ExitGames.Client.Photon.DebugLevel)
extern "C" void PhotonPeer_set_DebugOut_m5_198 (PhotonPeer_t5_38 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PhotonPeer::get_DebugOut()
extern "C" uint8_t PhotonPeer_get_DebugOut_m5_199 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PhotonPeer::get_Listener()
extern "C" Object_t * PhotonPeer_get_Listener_m5_200 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::set_Listener(ExitGames.Client.Photon.IPhotonPeerListener)
extern "C" void PhotonPeer_set_Listener_m5_201 (PhotonPeer_t5_38 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsEnabled()
extern "C" bool PhotonPeer_get_TrafficStatsEnabled_m5_202 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::set_TrafficStatsEnabled(System.Boolean)
extern "C" void PhotonPeer_set_TrafficStatsEnabled_m5_203 (PhotonPeer_t5_38 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsElapsedMs()
extern "C" int64_t PhotonPeer_get_TrafficStatsElapsedMs_m5_204 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::TrafficStatsReset()
extern "C" void PhotonPeer_TrafficStatsReset_m5_205 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsIncoming()
extern "C" TrafficStats_t5_14 * PhotonPeer_get_TrafficStatsIncoming_m5_206 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsOutgoing()
extern "C" TrafficStats_t5_14 * PhotonPeer_get_TrafficStatsOutgoing_m5_207 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsGameLevel()
extern "C" TrafficStatsGameLevel_t5_15 * PhotonPeer_get_TrafficStatsGameLevel_m5_208 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ExitGames.Client.Photon.PhotonPeer::get_QuickResendAttempts()
extern "C" uint8_t PhotonPeer_get_QuickResendAttempts_m5_209 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::set_QuickResendAttempts(System.Byte)
extern "C" void PhotonPeer_set_QuickResendAttempts_m5_210 (PhotonPeer_t5_38 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.PeerStateValue ExitGames.Client.Photon.PhotonPeer::get_PeerState()
extern "C" uint8_t PhotonPeer_get_PeerState_m5_211 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.PhotonPeer::get_LimitOfUnreliableCommands()
extern "C" int32_t PhotonPeer_get_LimitOfUnreliableCommands_m5_212 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::set_LimitOfUnreliableCommands(System.Int32)
extern "C" void PhotonPeer_set_LimitOfUnreliableCommands_m5_213 (PhotonPeer_t5_38 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::get_CrcEnabled()
extern "C" bool PhotonPeer_get_CrcEnabled_m5_214 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::set_CrcEnabled(System.Boolean)
extern "C" void PhotonPeer_set_CrcEnabled_m5_215 (PhotonPeer_t5_38 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.PhotonPeer::get_PacketLossByCrc()
extern "C" int32_t PhotonPeer_get_PacketLossByCrc_m5_216 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.PhotonPeer::get_ResentReliableCommands()
extern "C" int32_t PhotonPeer_get_ResentReliableCommands_m5_217 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.PhotonPeer::get_SentCountAllowance()
extern "C" int32_t PhotonPeer_get_SentCountAllowance_m5_218 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::set_SentCountAllowance(System.Int32)
extern "C" void PhotonPeer_set_SentCountAllowance_m5_219 (PhotonPeer_t5_38 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::set_TimePingInterval(System.Int32)
extern "C" void PhotonPeer_set_TimePingInterval_m5_220 (PhotonPeer_t5_38 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.PhotonPeer::get_DisconnectTimeout()
extern "C" int32_t PhotonPeer_get_DisconnectTimeout_m5_221 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::set_DisconnectTimeout(System.Int32)
extern "C" void PhotonPeer_set_DisconnectTimeout_m5_222 (PhotonPeer_t5_38 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.PhotonPeer::get_ServerTimeInMilliSeconds()
extern "C" int32_t PhotonPeer_get_ServerTimeInMilliSeconds_m5_223 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.PhotonPeer::get_RoundTripTime()
extern "C" int32_t PhotonPeer_get_RoundTripTime_m5_224 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.PhotonPeer::get_RoundTripTimeVariance()
extern "C" int32_t PhotonPeer_get_RoundTripTimeVariance_m5_225 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.PhotonPeer::get_TimestampOfLastSocketReceive()
extern "C" int32_t PhotonPeer_get_TimestampOfLastSocketReceive_m5_226 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.PhotonPeer::get_ServerAddress()
extern "C" String_t* PhotonPeer_get_ServerAddress_m5_227 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::get_UsedProtocol()
extern "C" uint8_t PhotonPeer_get_UsedProtocol_m5_228 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::get_IsSimulationEnabled()
extern "C" bool PhotonPeer_get_IsSimulationEnabled_m5_229 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::set_IsSimulationEnabled(System.Boolean)
extern "C" void PhotonPeer_set_IsSimulationEnabled_m5_230 (PhotonPeer_t5_38 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.NetworkSimulationSet ExitGames.Client.Photon.PhotonPeer::get_NetworkSimulationSettings()
extern "C" NetworkSimulationSet_t5_16 * PhotonPeer_get_NetworkSimulationSettings_m5_231 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::get_IsEncryptionAvailable()
extern "C" bool PhotonPeer_get_IsEncryptionAvailable_m5_232 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::set_IsSendingOnlyAcks(System.Boolean)
extern "C" void PhotonPeer_set_IsSendingOnlyAcks_m5_233 (PhotonPeer_t5_38 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::.ctor(ExitGames.Client.Photon.ConnectionProtocol)
extern "C" void PhotonPeer__ctor_m5_234 (PhotonPeer_t5_38 * __this, uint8_t ___protocolType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::.ctor(ExitGames.Client.Photon.IPhotonPeerListener,ExitGames.Client.Photon.ConnectionProtocol)
extern "C" void PhotonPeer__ctor_m5_235 (PhotonPeer_t5_38 * __this, Object_t * ___listener, uint8_t ___protocolType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::Connect(System.String,System.String)
extern "C" bool PhotonPeer_Connect_m5_236 (PhotonPeer_t5_38 * __this, String_t* ___serverAddress, String_t* ___applicationName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::Disconnect()
extern "C" void PhotonPeer_Disconnect_m5_237 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::StopThread()
extern "C" void PhotonPeer_StopThread_m5_238 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::FetchServerTimestamp()
extern "C" void PhotonPeer_FetchServerTimestamp_m5_239 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::EstablishEncryption()
extern "C" bool PhotonPeer_EstablishEncryption_m5_240 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPeer::Service()
extern "C" void PhotonPeer_Service_m5_241 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::SendOutgoingCommands()
extern "C" bool PhotonPeer_SendOutgoingCommands_m5_242 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::SendAcksOnly()
extern "C" bool PhotonPeer_SendAcksOnly_m5_243 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::DispatchIncomingCommands()
extern "C" bool PhotonPeer_DispatchIncomingCommands_m5_244 (PhotonPeer_t5_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.PhotonPeer::VitalStatsToString(System.Boolean)
extern "C" String_t* PhotonPeer_VitalStatsToString_m5_245 (PhotonPeer_t5_38 * __this, bool ___all, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::OpCustom(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean)
extern "C" bool PhotonPeer_OpCustom_m5_246 (PhotonPeer_t5_38 * __this, uint8_t ___customOpCode, Dictionary_2_t1_888 * ___customOpParameters, bool ___sendReliable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::OpCustom(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean,System.Byte)
extern "C" bool PhotonPeer_OpCustom_m5_247 (PhotonPeer_t5_38 * __this, uint8_t ___customOpCode, Dictionary_2_t1_888 * ___customOpParameters, bool ___sendReliable, uint8_t ___channelId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::OpCustom(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean,System.Byte,System.Boolean)
extern "C" bool PhotonPeer_OpCustom_m5_248 (PhotonPeer_t5_38 * __this, uint8_t ___customOpCode, Dictionary_2_t1_888 * ___customOpParameters, bool ___sendReliable, uint8_t ___channelId, bool ___encrypt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPeer::RegisterType(System.Type,System.Byte,ExitGames.Client.Photon.SerializeStreamMethod,ExitGames.Client.Photon.DeserializeStreamMethod)
extern "C" bool PhotonPeer_RegisterType_m5_249 (Object_t * __this /* static, unused */, Type_t * ___customType, uint8_t ___code, SerializeStreamMethod_t5_47 * ___serializeMethod, DeserializeStreamMethod_t5_49 * ___constructor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
