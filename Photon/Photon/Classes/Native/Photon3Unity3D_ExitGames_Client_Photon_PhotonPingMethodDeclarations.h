﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.PhotonPing
struct PhotonPing_t5_39;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Boolean ExitGames.Client.Photon.PhotonPing::StartPing(System.String)
extern "C" bool PhotonPing_StartPing_m5_250 (PhotonPing_t5_39 * __this, String_t* ___ip, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.PhotonPing::Done()
extern "C" bool PhotonPing_Done_m5_251 (PhotonPing_t5_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPing::Dispose()
extern "C" void PhotonPing_Dispose_m5_252 (PhotonPing_t5_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPing::Init()
extern "C" void PhotonPing_Init_m5_253 (PhotonPing_t5_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.PhotonPing::.ctor()
extern "C" void PhotonPing__ctor_m5_254 (PhotonPing_t5_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
