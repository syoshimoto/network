﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.SocketUdp
struct SocketUdp_t5_53;
// ExitGames.Client.Photon.PeerBase
struct PeerBase_t5_10;
// System.Byte[]
struct ByteU5BU5D_t1_71;

#include "codegen/il2cpp-codegen.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonSocketError.h"

// System.Void ExitGames.Client.Photon.SocketUdp::.ctor(ExitGames.Client.Photon.PeerBase)
extern "C" void SocketUdp__ctor_m5_350 (SocketUdp_t5_53 * __this, PeerBase_t5_10 * ___npeer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.SocketUdp::Dispose()
extern "C" void SocketUdp_Dispose_m5_351 (SocketUdp_t5_53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.SocketUdp::Connect()
extern "C" bool SocketUdp_Connect_m5_352 (SocketUdp_t5_53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExitGames.Client.Photon.SocketUdp::Disconnect()
extern "C" bool SocketUdp_Disconnect_m5_353 (SocketUdp_t5_53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.SocketUdp::Send(System.Byte[],System.Int32)
extern "C" int32_t SocketUdp_Send_m5_354 (SocketUdp_t5_53 * __this, ByteU5BU5D_t1_71* ___data, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.SocketUdp::DnsAndConnect()
extern "C" void SocketUdp_DnsAndConnect_m5_355 (SocketUdp_t5_53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.SocketUdp::ReceiveLoop()
extern "C" void SocketUdp_ReceiveLoop_m5_356 (SocketUdp_t5_53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
