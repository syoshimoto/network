﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.SupportClass/<>c__DisplayClass1
struct U3CU3Ec__DisplayClass1_t5_56;

#include "codegen/il2cpp-codegen.h"

// System.Void ExitGames.Client.Photon.SupportClass/<>c__DisplayClass1::.ctor()
extern "C" void U3CU3Ec__DisplayClass1__ctor_m5_363 (U3CU3Ec__DisplayClass1_t5_56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.SupportClass/<>c__DisplayClass1::<CallInBackground>b__0()
extern "C" void U3CU3Ec__DisplayClass1_U3CCallInBackgroundU3Eb__0_m5_364 (U3CU3Ec__DisplayClass1_t5_56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
