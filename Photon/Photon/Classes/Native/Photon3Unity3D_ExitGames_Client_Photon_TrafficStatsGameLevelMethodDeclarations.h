﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.TrafficStatsGameLevel
struct TrafficStatsGameLevel_t5_15;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_OperationByteCount()
extern "C" int32_t TrafficStatsGameLevel_get_OperationByteCount_m5_397 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_OperationByteCount(System.Int32)
extern "C" void TrafficStatsGameLevel_set_OperationByteCount_m5_398 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_OperationCount()
extern "C" int32_t TrafficStatsGameLevel_get_OperationCount_m5_399 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_OperationCount(System.Int32)
extern "C" void TrafficStatsGameLevel_set_OperationCount_m5_400 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_ResultByteCount()
extern "C" int32_t TrafficStatsGameLevel_get_ResultByteCount_m5_401 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_ResultByteCount(System.Int32)
extern "C" void TrafficStatsGameLevel_set_ResultByteCount_m5_402 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_ResultCount()
extern "C" int32_t TrafficStatsGameLevel_get_ResultCount_m5_403 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_ResultCount(System.Int32)
extern "C" void TrafficStatsGameLevel_set_ResultCount_m5_404 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_EventByteCount()
extern "C" int32_t TrafficStatsGameLevel_get_EventByteCount_m5_405 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_EventByteCount(System.Int32)
extern "C" void TrafficStatsGameLevel_set_EventByteCount_m5_406 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_EventCount()
extern "C" int32_t TrafficStatsGameLevel_get_EventCount_m5_407 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_EventCount(System.Int32)
extern "C" void TrafficStatsGameLevel_set_EventCount_m5_408 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestOpResponseCallback()
extern "C" int32_t TrafficStatsGameLevel_get_LongestOpResponseCallback_m5_409 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestOpResponseCallback(System.Int32)
extern "C" void TrafficStatsGameLevel_set_LongestOpResponseCallback_m5_410 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestOpResponseCallbackOpCode()
extern "C" uint8_t TrafficStatsGameLevel_get_LongestOpResponseCallbackOpCode_m5_411 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestOpResponseCallbackOpCode(System.Byte)
extern "C" void TrafficStatsGameLevel_set_LongestOpResponseCallbackOpCode_m5_412 (TrafficStatsGameLevel_t5_15 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestEventCallback()
extern "C" int32_t TrafficStatsGameLevel_get_LongestEventCallback_m5_413 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestEventCallback(System.Int32)
extern "C" void TrafficStatsGameLevel_set_LongestEventCallback_m5_414 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestEventCallbackCode()
extern "C" uint8_t TrafficStatsGameLevel_get_LongestEventCallbackCode_m5_415 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestEventCallbackCode(System.Byte)
extern "C" void TrafficStatsGameLevel_set_LongestEventCallbackCode_m5_416 (TrafficStatsGameLevel_t5_15 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestDeltaBetweenDispatching()
extern "C" int32_t TrafficStatsGameLevel_get_LongestDeltaBetweenDispatching_m5_417 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestDeltaBetweenDispatching(System.Int32)
extern "C" void TrafficStatsGameLevel_set_LongestDeltaBetweenDispatching_m5_418 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestDeltaBetweenSending()
extern "C" int32_t TrafficStatsGameLevel_get_LongestDeltaBetweenSending_m5_419 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestDeltaBetweenSending(System.Int32)
extern "C" void TrafficStatsGameLevel_set_LongestDeltaBetweenSending_m5_420 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_DispatchIncomingCommandsCalls()
extern "C" int32_t TrafficStatsGameLevel_get_DispatchIncomingCommandsCalls_m5_421 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_DispatchIncomingCommandsCalls(System.Int32)
extern "C" void TrafficStatsGameLevel_set_DispatchIncomingCommandsCalls_m5_422 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_SendOutgoingCommandsCalls()
extern "C" int32_t TrafficStatsGameLevel_get_SendOutgoingCommandsCalls_m5_423 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_SendOutgoingCommandsCalls(System.Int32)
extern "C" void TrafficStatsGameLevel_set_SendOutgoingCommandsCalls_m5_424 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_TotalMessageCount()
extern "C" int32_t TrafficStatsGameLevel_get_TotalMessageCount_m5_425 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_TotalIncomingMessageCount()
extern "C" int32_t TrafficStatsGameLevel_get_TotalIncomingMessageCount_m5_426 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_TotalOutgoingMessageCount()
extern "C" int32_t TrafficStatsGameLevel_get_TotalOutgoingMessageCount_m5_427 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::CountOperation(System.Int32)
extern "C" void TrafficStatsGameLevel_CountOperation_m5_428 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___operationBytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::CountResult(System.Int32)
extern "C" void TrafficStatsGameLevel_CountResult_m5_429 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___resultBytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::CountEvent(System.Int32)
extern "C" void TrafficStatsGameLevel_CountEvent_m5_430 (TrafficStatsGameLevel_t5_15 * __this, int32_t ___eventBytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::TimeForResponseCallback(System.Byte,System.Int32)
extern "C" void TrafficStatsGameLevel_TimeForResponseCallback_m5_431 (TrafficStatsGameLevel_t5_15 * __this, uint8_t ___code, int32_t ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::TimeForEventCallback(System.Byte,System.Int32)
extern "C" void TrafficStatsGameLevel_TimeForEventCallback_m5_432 (TrafficStatsGameLevel_t5_15 * __this, uint8_t ___code, int32_t ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::DispatchIncomingCommandsCalled()
extern "C" void TrafficStatsGameLevel_DispatchIncomingCommandsCalled_m5_433 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::SendOutgoingCommandsCalled()
extern "C" void TrafficStatsGameLevel_SendOutgoingCommandsCalled_m5_434 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.TrafficStatsGameLevel::ToString()
extern "C" String_t* TrafficStatsGameLevel_ToString_m5_435 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.TrafficStatsGameLevel::ToStringVitalStats()
extern "C" String_t* TrafficStatsGameLevel_ToStringVitalStats_m5_436 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::.ctor()
extern "C" void TrafficStatsGameLevel__ctor_m5_437 (TrafficStatsGameLevel_t5_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
