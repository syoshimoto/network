﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitGames.Client.Photon.TrafficStats
struct TrafficStats_t5_14;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Int32 ExitGames.Client.Photon.TrafficStats::get_PackageHeaderSize()
extern "C" int32_t TrafficStats_get_PackageHeaderSize_m5_438 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_PackageHeaderSize(System.Int32)
extern "C" void TrafficStats_set_PackageHeaderSize_m5_439 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStats::get_ReliableCommandCount()
extern "C" int32_t TrafficStats_get_ReliableCommandCount_m5_440 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_ReliableCommandCount(System.Int32)
extern "C" void TrafficStats_set_ReliableCommandCount_m5_441 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStats::get_UnreliableCommandCount()
extern "C" int32_t TrafficStats_get_UnreliableCommandCount_m5_442 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_UnreliableCommandCount(System.Int32)
extern "C" void TrafficStats_set_UnreliableCommandCount_m5_443 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStats::get_FragmentCommandCount()
extern "C" int32_t TrafficStats_get_FragmentCommandCount_m5_444 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_FragmentCommandCount(System.Int32)
extern "C" void TrafficStats_set_FragmentCommandCount_m5_445 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStats::get_ControlCommandCount()
extern "C" int32_t TrafficStats_get_ControlCommandCount_m5_446 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_ControlCommandCount(System.Int32)
extern "C" void TrafficStats_set_ControlCommandCount_m5_447 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStats::get_TotalPacketCount()
extern "C" int32_t TrafficStats_get_TotalPacketCount_m5_448 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_TotalPacketCount(System.Int32)
extern "C" void TrafficStats_set_TotalPacketCount_m5_449 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStats::get_TotalCommandsInPackets()
extern "C" int32_t TrafficStats_get_TotalCommandsInPackets_m5_450 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_TotalCommandsInPackets(System.Int32)
extern "C" void TrafficStats_set_TotalCommandsInPackets_m5_451 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStats::get_ReliableCommandBytes()
extern "C" int32_t TrafficStats_get_ReliableCommandBytes_m5_452 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_ReliableCommandBytes(System.Int32)
extern "C" void TrafficStats_set_ReliableCommandBytes_m5_453 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStats::get_UnreliableCommandBytes()
extern "C" int32_t TrafficStats_get_UnreliableCommandBytes_m5_454 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_UnreliableCommandBytes(System.Int32)
extern "C" void TrafficStats_set_UnreliableCommandBytes_m5_455 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStats::get_FragmentCommandBytes()
extern "C" int32_t TrafficStats_get_FragmentCommandBytes_m5_456 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_FragmentCommandBytes(System.Int32)
extern "C" void TrafficStats_set_FragmentCommandBytes_m5_457 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStats::get_ControlCommandBytes()
extern "C" int32_t TrafficStats_get_ControlCommandBytes_m5_458 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_ControlCommandBytes(System.Int32)
extern "C" void TrafficStats_set_ControlCommandBytes_m5_459 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::.ctor(System.Int32)
extern "C" void TrafficStats__ctor_m5_460 (TrafficStats_t5_14 * __this, int32_t ___packageHeaderSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStats::get_TotalCommandBytes()
extern "C" int32_t TrafficStats_get_TotalCommandBytes_m5_461 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExitGames.Client.Photon.TrafficStats::get_TotalPacketBytes()
extern "C" int32_t TrafficStats_get_TotalPacketBytes_m5_462 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_TimestampOfLastAck(System.Int32)
extern "C" void TrafficStats_set_TimestampOfLastAck_m5_463 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::set_TimestampOfLastReliableCommand(System.Int32)
extern "C" void TrafficStats_set_TimestampOfLastReliableCommand_m5_464 (TrafficStats_t5_14 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::CountControlCommand(System.Int32)
extern "C" void TrafficStats_CountControlCommand_m5_465 (TrafficStats_t5_14 * __this, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::CountReliableOpCommand(System.Int32)
extern "C" void TrafficStats_CountReliableOpCommand_m5_466 (TrafficStats_t5_14 * __this, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::CountUnreliableOpCommand(System.Int32)
extern "C" void TrafficStats_CountUnreliableOpCommand_m5_467 (TrafficStats_t5_14 * __this, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitGames.Client.Photon.TrafficStats::CountFragmentOpCommand(System.Int32)
extern "C" void TrafficStats_CountFragmentOpCommand_m5_468 (TrafficStats_t5_14 * __this, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExitGames.Client.Photon.TrafficStats::ToString()
extern "C" String_t* TrafficStats_ToString_m5_469 (TrafficStats_t5_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
