﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t1_160;
// System.UInt32[]
struct UInt32U5BU5D_t1_97;

#include "mscorlib_System_Object.h"

// Photon.SocketServer.Numeric.BigInteger
struct  BigInteger_t5_3  : public Object_t
{
	// System.UInt32[] Photon.SocketServer.Numeric.BigInteger::data
	UInt32U5BU5D_t1_97* ___data_2;
	// System.Int32 Photon.SocketServer.Numeric.BigInteger::dataLength
	int32_t ___dataLength_3;
};
struct BigInteger_t5_3_StaticFields{
	// System.Int32[] Photon.SocketServer.Numeric.BigInteger::primesBelow2000
	Int32U5BU5D_t1_160* ___primesBelow2000_1;
};
