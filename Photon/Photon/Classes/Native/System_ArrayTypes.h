﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// System.Net.IPAddress[]
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t3_57  : public Array_t { };
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct X509ChainStatusU5BU5D_t3_84  : public Array_t { };
// System.Text.RegularExpressions.Capture[]
// System.Text.RegularExpressions.Capture[]
struct CaptureU5BU5D_t3_109  : public Array_t { };
// System.Text.RegularExpressions.Group[]
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_t3_112  : public Array_t { };
// System.Text.RegularExpressions.Mark[]
// System.Text.RegularExpressions.Mark[]
struct MarkU5BU5D_t3_140  : public Array_t { };
// System.Uri/UriScheme[]
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_t3_170  : public Array_t { };
