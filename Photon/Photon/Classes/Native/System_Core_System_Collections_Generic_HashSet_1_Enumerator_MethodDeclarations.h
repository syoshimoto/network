﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__1MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<PickupItem>::.ctor(System.Collections.Generic.HashSet`1<T>)
#define Enumerator__ctor_m2_103(__this, ___hashset, method) (( void (*) (Enumerator_t2_18 *, HashSet_1_t2_17 *, const MethodInfo*))Enumerator__ctor_m2_86_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<PickupItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2_104(__this, method) (( Object_t * (*) (Enumerator_t2_18 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2_87_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<PickupItem>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2_105(__this, method) (( void (*) (Enumerator_t2_18 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2_88_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<PickupItem>::MoveNext()
#define Enumerator_MoveNext_m2_50(__this, method) (( bool (*) (Enumerator_t2_18 *, const MethodInfo*))Enumerator_MoveNext_m2_89_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<PickupItem>::get_Current()
#define Enumerator_get_Current_m2_49(__this, method) (( PickupItem_t8_163 * (*) (Enumerator_t2_18 *, const MethodInfo*))Enumerator_get_Current_m2_90_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<PickupItem>::Dispose()
#define Enumerator_Dispose_m2_106(__this, method) (( void (*) (Enumerator_t2_18 *, const MethodInfo*))Enumerator_Dispose_m2_91_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<PickupItem>::CheckState()
#define Enumerator_CheckState_m2_107(__this, method) (( void (*) (Enumerator_t2_18 *, const MethodInfo*))Enumerator_CheckState_m2_92_gshared)(__this, method)
