﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_gen_2MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1<PickupItem>::.ctor()
#define HashSet_1__ctor_m2_59(__this, method) (( void (*) (HashSet_1_t2_17 *, const MethodInfo*))HashSet_1__ctor_m2_62_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<PickupItem>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1__ctor_m2_108(__this, ___info, ___context, method) (( void (*) (HashSet_1_t2_17 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))HashSet_1__ctor_m2_63_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<PickupItem>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2_109(__this, method) (( Object_t* (*) (HashSet_1_t2_17 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2_64_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<PickupItem>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2_110(__this, method) (( bool (*) (HashSet_1_t2_17 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2_65_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<PickupItem>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2_111(__this, ___array, ___index, method) (( void (*) (HashSet_1_t2_17 *, PickupItemU5BU5D_t8_181*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2_66_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<PickupItem>::System.Collections.Generic.ICollection<T>.Add(T)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2_112(__this, ___item, method) (( void (*) (HashSet_1_t2_17 *, PickupItem_t8_163 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2_67_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<PickupItem>::System.Collections.IEnumerable.GetEnumerator()
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2_113(__this, method) (( Object_t * (*) (HashSet_1_t2_17 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2_68_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<PickupItem>::get_Count()
#define HashSet_1_get_Count_m2_114(__this, method) (( int32_t (*) (HashSet_1_t2_17 *, const MethodInfo*))HashSet_1_get_Count_m2_69_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<PickupItem>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1_Init_m2_115(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t2_17 *, int32_t, Object_t*, const MethodInfo*))HashSet_1_Init_m2_70_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<PickupItem>::InitArrays(System.Int32)
#define HashSet_1_InitArrays_m2_116(__this, ___size, method) (( void (*) (HashSet_1_t2_17 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m2_71_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<PickupItem>::SlotsContainsAt(System.Int32,System.Int32,T)
#define HashSet_1_SlotsContainsAt_m2_117(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t2_17 *, int32_t, int32_t, PickupItem_t8_163 *, const MethodInfo*))HashSet_1_SlotsContainsAt_m2_72_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<PickupItem>::CopyTo(T[])
#define HashSet_1_CopyTo_m2_61(__this, ___array, method) (( void (*) (HashSet_1_t2_17 *, PickupItemU5BU5D_t8_181*, const MethodInfo*))HashSet_1_CopyTo_m2_73_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.HashSet`1<PickupItem>::CopyTo(T[],System.Int32)
#define HashSet_1_CopyTo_m2_118(__this, ___array, ___index, method) (( void (*) (HashSet_1_t2_17 *, PickupItemU5BU5D_t8_181*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2_74_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<PickupItem>::CopyTo(T[],System.Int32,System.Int32)
#define HashSet_1_CopyTo_m2_119(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t2_17 *, PickupItemU5BU5D_t8_181*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2_75_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<PickupItem>::Resize()
#define HashSet_1_Resize_m2_120(__this, method) (( void (*) (HashSet_1_t2_17 *, const MethodInfo*))HashSet_1_Resize_m2_76_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<PickupItem>::GetLinkHashCode(System.Int32)
#define HashSet_1_GetLinkHashCode_m2_121(__this, ___index, method) (( int32_t (*) (HashSet_1_t2_17 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m2_77_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<PickupItem>::GetItemHashCode(T)
#define HashSet_1_GetItemHashCode_m2_122(__this, ___item, method) (( int32_t (*) (HashSet_1_t2_17 *, PickupItem_t8_163 *, const MethodInfo*))HashSet_1_GetItemHashCode_m2_78_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<PickupItem>::Add(T)
#define HashSet_1_Add_m2_60(__this, ___item, method) (( bool (*) (HashSet_1_t2_17 *, PickupItem_t8_163 *, const MethodInfo*))HashSet_1_Add_m2_79_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<PickupItem>::Clear()
#define HashSet_1_Clear_m2_123(__this, method) (( void (*) (HashSet_1_t2_17 *, const MethodInfo*))HashSet_1_Clear_m2_80_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<PickupItem>::Contains(T)
#define HashSet_1_Contains_m2_124(__this, ___item, method) (( bool (*) (HashSet_1_t2_17 *, PickupItem_t8_163 *, const MethodInfo*))HashSet_1_Contains_m2_81_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<PickupItem>::Remove(T)
#define HashSet_1_Remove_m2_125(__this, ___item, method) (( bool (*) (HashSet_1_t2_17 *, PickupItem_t8_163 *, const MethodInfo*))HashSet_1_Remove_m2_82_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<PickupItem>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1_GetObjectData_m2_126(__this, ___info, ___context, method) (( void (*) (HashSet_1_t2_17 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))HashSet_1_GetObjectData_m2_83_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<PickupItem>::OnDeserialization(System.Object)
#define HashSet_1_OnDeserialization_m2_127(__this, ___sender, method) (( void (*) (HashSet_1_t2_17 *, Object_t *, const MethodInfo*))HashSet_1_OnDeserialization_m2_84_gshared)(__this, ___sender, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<PickupItem>::GetEnumerator()
#define HashSet_1_GetEnumerator_m2_48(__this, method) (( Enumerator_t2_18  (*) (HashSet_1_t2_17 *, const MethodInfo*))HashSet_1_GetEnumerator_m2_85_gshared)(__this, method)
