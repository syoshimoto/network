﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t2_15;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_177;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t1_1407;
// System.Int32[]
struct Int32U5BU5D_t1_160;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1_1157;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__2.h"

// System.Void System.Collections.Generic.HashSet`1<System.Int32>::.ctor()
extern "C" void HashSet_1__ctor_m2_51_gshared (HashSet_1_t2_15 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m2_51(__this, method) (( void (*) (HashSet_1_t2_15 *, const MethodInfo*))HashSet_1__ctor_m2_51_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1__ctor_m2_128_gshared (HashSet_1_t2_15 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method);
#define HashSet_1__ctor_m2_128(__this, ___info, ___context, method) (( void (*) (HashSet_1_t2_15 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))HashSet_1__ctor_m2_128_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2_129_gshared (HashSet_1_t2_15 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2_129(__this, method) (( Object_t* (*) (HashSet_1_t2_15 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2_129_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2_130_gshared (HashSet_1_t2_15 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2_130(__this, method) (( bool (*) (HashSet_1_t2_15 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2_130_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2_131_gshared (HashSet_1_t2_15 * __this, Int32U5BU5D_t1_160* ___array, int32_t ___index, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2_131(__this, ___array, ___index, method) (( void (*) (HashSet_1_t2_15 *, Int32U5BU5D_t1_160*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2_131_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2_132_gshared (HashSet_1_t2_15 * __this, int32_t ___item, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2_132(__this, ___item, method) (( void (*) (HashSet_1_t2_15 *, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2_132_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2_133_gshared (HashSet_1_t2_15 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2_133(__this, method) (( Object_t * (*) (HashSet_1_t2_15 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2_133_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Int32>::get_Count()
extern "C" int32_t HashSet_1_get_Count_m2_134_gshared (HashSet_1_t2_15 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m2_134(__this, method) (( int32_t (*) (HashSet_1_t2_15 *, const MethodInfo*))HashSet_1_get_Count_m2_134_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C" void HashSet_1_Init_m2_135_gshared (HashSet_1_t2_15 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define HashSet_1_Init_m2_135(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t2_15 *, int32_t, Object_t*, const MethodInfo*))HashSet_1_Init_m2_135_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::InitArrays(System.Int32)
extern "C" void HashSet_1_InitArrays_m2_136_gshared (HashSet_1_t2_15 * __this, int32_t ___size, const MethodInfo* method);
#define HashSet_1_InitArrays_m2_136(__this, ___size, method) (( void (*) (HashSet_1_t2_15 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m2_136_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int32>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C" bool HashSet_1_SlotsContainsAt_m2_137_gshared (HashSet_1_t2_15 * __this, int32_t ___index, int32_t ___hash, int32_t ___item, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m2_137(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t2_15 *, int32_t, int32_t, int32_t, const MethodInfo*))HashSet_1_SlotsContainsAt_m2_137_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::CopyTo(T[])
extern "C" void HashSet_1_CopyTo_m2_138_gshared (HashSet_1_t2_15 * __this, Int32U5BU5D_t1_160* ___array, const MethodInfo* method);
#define HashSet_1_CopyTo_m2_138(__this, ___array, method) (( void (*) (HashSet_1_t2_15 *, Int32U5BU5D_t1_160*, const MethodInfo*))HashSet_1_CopyTo_m2_138_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void HashSet_1_CopyTo_m2_139_gshared (HashSet_1_t2_15 * __this, Int32U5BU5D_t1_160* ___array, int32_t ___index, const MethodInfo* method);
#define HashSet_1_CopyTo_m2_139(__this, ___array, ___index, method) (( void (*) (HashSet_1_t2_15 *, Int32U5BU5D_t1_160*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2_139_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::CopyTo(T[],System.Int32,System.Int32)
extern "C" void HashSet_1_CopyTo_m2_140_gshared (HashSet_1_t2_15 * __this, Int32U5BU5D_t1_160* ___array, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define HashSet_1_CopyTo_m2_140(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t2_15 *, Int32U5BU5D_t1_160*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2_140_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::Resize()
extern "C" void HashSet_1_Resize_m2_141_gshared (HashSet_1_t2_15 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m2_141(__this, method) (( void (*) (HashSet_1_t2_15 *, const MethodInfo*))HashSet_1_Resize_m2_141_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Int32>::GetLinkHashCode(System.Int32)
extern "C" int32_t HashSet_1_GetLinkHashCode_m2_142_gshared (HashSet_1_t2_15 * __this, int32_t ___index, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m2_142(__this, ___index, method) (( int32_t (*) (HashSet_1_t2_15 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m2_142_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Int32>::GetItemHashCode(T)
extern "C" int32_t HashSet_1_GetItemHashCode_m2_143_gshared (HashSet_1_t2_15 * __this, int32_t ___item, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m2_143(__this, ___item, method) (( int32_t (*) (HashSet_1_t2_15 *, int32_t, const MethodInfo*))HashSet_1_GetItemHashCode_m2_143_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int32>::Add(T)
extern "C" bool HashSet_1_Add_m2_57_gshared (HashSet_1_t2_15 * __this, int32_t ___item, const MethodInfo* method);
#define HashSet_1_Add_m2_57(__this, ___item, method) (( bool (*) (HashSet_1_t2_15 *, int32_t, const MethodInfo*))HashSet_1_Add_m2_57_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::Clear()
extern "C" void HashSet_1_Clear_m2_144_gshared (HashSet_1_t2_15 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m2_144(__this, method) (( void (*) (HashSet_1_t2_15 *, const MethodInfo*))HashSet_1_Clear_m2_144_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int32>::Contains(T)
extern "C" bool HashSet_1_Contains_m2_145_gshared (HashSet_1_t2_15 * __this, int32_t ___item, const MethodInfo* method);
#define HashSet_1_Contains_m2_145(__this, ___item, method) (( bool (*) (HashSet_1_t2_15 *, int32_t, const MethodInfo*))HashSet_1_Contains_m2_145_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int32>::Remove(T)
extern "C" bool HashSet_1_Remove_m2_146_gshared (HashSet_1_t2_15 * __this, int32_t ___item, const MethodInfo* method);
#define HashSet_1_Remove_m2_146(__this, ___item, method) (( bool (*) (HashSet_1_t2_15 *, int32_t, const MethodInfo*))HashSet_1_Remove_m2_146_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1_GetObjectData_m2_147_gshared (HashSet_1_t2_15 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method);
#define HashSet_1_GetObjectData_m2_147(__this, ___info, ___context, method) (( void (*) (HashSet_1_t2_15 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))HashSet_1_GetObjectData_m2_147_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::OnDeserialization(System.Object)
extern "C" void HashSet_1_OnDeserialization_m2_148_gshared (HashSet_1_t2_15 * __this, Object_t * ___sender, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m2_148(__this, ___sender, method) (( void (*) (HashSet_1_t2_15 *, Object_t *, const MethodInfo*))HashSet_1_OnDeserialization_m2_148_gshared)(__this, ___sender, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t2_30  HashSet_1_GetEnumerator_m2_149_gshared (HashSet_1_t2_15 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m2_149(__this, method) (( Enumerator_t2_30  (*) (HashSet_1_t2_15 *, const MethodInfo*))HashSet_1_GetEnumerator_m2_149_gshared)(__this, method)
