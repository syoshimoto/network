﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_gen_2MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::.ctor()
#define HashSet_1__ctor_m2_52(__this, method) (( void (*) (HashSet_1_t2_16 *, const MethodInfo*))HashSet_1__ctor_m2_62_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1__ctor_m2_161(__this, ___info, ___context, method) (( void (*) (HashSet_1_t2_16 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))HashSet_1__ctor_m2_63_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2_162(__this, method) (( Object_t* (*) (HashSet_1_t2_16 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2_64_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2_163(__this, method) (( bool (*) (HashSet_1_t2_16 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2_65_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2_164(__this, ___array, ___index, method) (( void (*) (HashSet_1_t2_16 *, GameObjectU5BU5D_t6_258*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2_66_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::System.Collections.Generic.ICollection<T>.Add(T)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2_165(__this, ___item, method) (( void (*) (HashSet_1_t2_16 *, GameObject_t6_85 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2_67_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::System.Collections.IEnumerable.GetEnumerator()
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2_166(__this, method) (( Object_t * (*) (HashSet_1_t2_16 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2_68_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::get_Count()
#define HashSet_1_get_Count_m2_167(__this, method) (( int32_t (*) (HashSet_1_t2_16 *, const MethodInfo*))HashSet_1_get_Count_m2_69_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1_Init_m2_168(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t2_16 *, int32_t, Object_t*, const MethodInfo*))HashSet_1_Init_m2_70_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::InitArrays(System.Int32)
#define HashSet_1_InitArrays_m2_169(__this, ___size, method) (( void (*) (HashSet_1_t2_16 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m2_71_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::SlotsContainsAt(System.Int32,System.Int32,T)
#define HashSet_1_SlotsContainsAt_m2_170(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t2_16 *, int32_t, int32_t, GameObject_t6_85 *, const MethodInfo*))HashSet_1_SlotsContainsAt_m2_72_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::CopyTo(T[])
#define HashSet_1_CopyTo_m2_171(__this, ___array, method) (( void (*) (HashSet_1_t2_16 *, GameObjectU5BU5D_t6_258*, const MethodInfo*))HashSet_1_CopyTo_m2_73_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::CopyTo(T[],System.Int32)
#define HashSet_1_CopyTo_m2_172(__this, ___array, ___index, method) (( void (*) (HashSet_1_t2_16 *, GameObjectU5BU5D_t6_258*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2_74_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::CopyTo(T[],System.Int32,System.Int32)
#define HashSet_1_CopyTo_m2_173(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t2_16 *, GameObjectU5BU5D_t6_258*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2_75_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::Resize()
#define HashSet_1_Resize_m2_174(__this, method) (( void (*) (HashSet_1_t2_16 *, const MethodInfo*))HashSet_1_Resize_m2_76_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::GetLinkHashCode(System.Int32)
#define HashSet_1_GetLinkHashCode_m2_175(__this, ___index, method) (( int32_t (*) (HashSet_1_t2_16 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m2_77_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::GetItemHashCode(T)
#define HashSet_1_GetItemHashCode_m2_176(__this, ___item, method) (( int32_t (*) (HashSet_1_t2_16 *, GameObject_t6_85 *, const MethodInfo*))HashSet_1_GetItemHashCode_m2_78_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::Add(T)
#define HashSet_1_Add_m2_53(__this, ___item, method) (( bool (*) (HashSet_1_t2_16 *, GameObject_t6_85 *, const MethodInfo*))HashSet_1_Add_m2_79_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::Clear()
#define HashSet_1_Clear_m2_177(__this, method) (( void (*) (HashSet_1_t2_16 *, const MethodInfo*))HashSet_1_Clear_m2_80_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::Contains(T)
#define HashSet_1_Contains_m2_178(__this, ___item, method) (( bool (*) (HashSet_1_t2_16 *, GameObject_t6_85 *, const MethodInfo*))HashSet_1_Contains_m2_81_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::Remove(T)
#define HashSet_1_Remove_m2_179(__this, ___item, method) (( bool (*) (HashSet_1_t2_16 *, GameObject_t6_85 *, const MethodInfo*))HashSet_1_Remove_m2_82_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1_GetObjectData_m2_180(__this, ___info, ___context, method) (( void (*) (HashSet_1_t2_16 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))HashSet_1_GetObjectData_m2_83_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::OnDeserialization(System.Object)
#define HashSet_1_OnDeserialization_m2_181(__this, ___sender, method) (( void (*) (HashSet_1_t2_16 *, Object_t *, const MethodInfo*))HashSet_1_OnDeserialization_m2_84_gshared)(__this, ___sender, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<UnityEngine.GameObject>::GetEnumerator()
#define HashSet_1_GetEnumerator_m2_54(__this, method) (( Enumerator_t2_19  (*) (HashSet_1_t2_16 *, const MethodInfo*))HashSet_1_GetEnumerator_m2_85_gshared)(__this, method)
