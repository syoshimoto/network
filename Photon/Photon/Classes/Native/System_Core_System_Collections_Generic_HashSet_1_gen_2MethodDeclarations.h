﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t2_20;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_177;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_1402;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1_1015;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__1.h"

// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C" void HashSet_1__ctor_m2_62_gshared (HashSet_1_t2_20 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m2_62(__this, method) (( void (*) (HashSet_1_t2_20 *, const MethodInfo*))HashSet_1__ctor_m2_62_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1__ctor_m2_63_gshared (HashSet_1_t2_20 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method);
#define HashSet_1__ctor_m2_63(__this, ___info, ___context, method) (( void (*) (HashSet_1_t2_20 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))HashSet_1__ctor_m2_63_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2_64_gshared (HashSet_1_t2_20 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2_64(__this, method) (( Object_t* (*) (HashSet_1_t2_20 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2_64_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2_65_gshared (HashSet_1_t2_20 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2_65(__this, method) (( bool (*) (HashSet_1_t2_20 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2_65_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2_66_gshared (HashSet_1_t2_20 * __this, ObjectU5BU5D_t1_157* ___array, int32_t ___index, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2_66(__this, ___array, ___index, method) (( void (*) (HashSet_1_t2_20 *, ObjectU5BU5D_t1_157*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2_66_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2_67_gshared (HashSet_1_t2_20 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2_67(__this, ___item, method) (( void (*) (HashSet_1_t2_20 *, Object_t *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2_67_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2_68_gshared (HashSet_1_t2_20 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2_68(__this, method) (( Object_t * (*) (HashSet_1_t2_20 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2_68_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::get_Count()
extern "C" int32_t HashSet_1_get_Count_m2_69_gshared (HashSet_1_t2_20 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m2_69(__this, method) (( int32_t (*) (HashSet_1_t2_20 *, const MethodInfo*))HashSet_1_get_Count_m2_69_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C" void HashSet_1_Init_m2_70_gshared (HashSet_1_t2_20 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define HashSet_1_Init_m2_70(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t2_20 *, int32_t, Object_t*, const MethodInfo*))HashSet_1_Init_m2_70_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::InitArrays(System.Int32)
extern "C" void HashSet_1_InitArrays_m2_71_gshared (HashSet_1_t2_20 * __this, int32_t ___size, const MethodInfo* method);
#define HashSet_1_InitArrays_m2_71(__this, ___size, method) (( void (*) (HashSet_1_t2_20 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m2_71_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C" bool HashSet_1_SlotsContainsAt_m2_72_gshared (HashSet_1_t2_20 * __this, int32_t ___index, int32_t ___hash, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m2_72(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t2_20 *, int32_t, int32_t, Object_t *, const MethodInfo*))HashSet_1_SlotsContainsAt_m2_72_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[])
extern "C" void HashSet_1_CopyTo_m2_73_gshared (HashSet_1_t2_20 * __this, ObjectU5BU5D_t1_157* ___array, const MethodInfo* method);
#define HashSet_1_CopyTo_m2_73(__this, ___array, method) (( void (*) (HashSet_1_t2_20 *, ObjectU5BU5D_t1_157*, const MethodInfo*))HashSet_1_CopyTo_m2_73_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void HashSet_1_CopyTo_m2_74_gshared (HashSet_1_t2_20 * __this, ObjectU5BU5D_t1_157* ___array, int32_t ___index, const MethodInfo* method);
#define HashSet_1_CopyTo_m2_74(__this, ___array, ___index, method) (( void (*) (HashSet_1_t2_20 *, ObjectU5BU5D_t1_157*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2_74_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32,System.Int32)
extern "C" void HashSet_1_CopyTo_m2_75_gshared (HashSet_1_t2_20 * __this, ObjectU5BU5D_t1_157* ___array, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define HashSet_1_CopyTo_m2_75(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t2_20 *, ObjectU5BU5D_t1_157*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2_75_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Resize()
extern "C" void HashSet_1_Resize_m2_76_gshared (HashSet_1_t2_20 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m2_76(__this, method) (( void (*) (HashSet_1_t2_20 *, const MethodInfo*))HashSet_1_Resize_m2_76_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetLinkHashCode(System.Int32)
extern "C" int32_t HashSet_1_GetLinkHashCode_m2_77_gshared (HashSet_1_t2_20 * __this, int32_t ___index, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m2_77(__this, ___index, method) (( int32_t (*) (HashSet_1_t2_20 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m2_77_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetItemHashCode(T)
extern "C" int32_t HashSet_1_GetItemHashCode_m2_78_gshared (HashSet_1_t2_20 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m2_78(__this, ___item, method) (( int32_t (*) (HashSet_1_t2_20 *, Object_t *, const MethodInfo*))HashSet_1_GetItemHashCode_m2_78_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(T)
extern "C" bool HashSet_1_Add_m2_79_gshared (HashSet_1_t2_20 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Add_m2_79(__this, ___item, method) (( bool (*) (HashSet_1_t2_20 *, Object_t *, const MethodInfo*))HashSet_1_Add_m2_79_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
extern "C" void HashSet_1_Clear_m2_80_gshared (HashSet_1_t2_20 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m2_80(__this, method) (( void (*) (HashSet_1_t2_20 *, const MethodInfo*))HashSet_1_Clear_m2_80_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(T)
extern "C" bool HashSet_1_Contains_m2_81_gshared (HashSet_1_t2_20 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Contains_m2_81(__this, ___item, method) (( bool (*) (HashSet_1_t2_20 *, Object_t *, const MethodInfo*))HashSet_1_Contains_m2_81_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(T)
extern "C" bool HashSet_1_Remove_m2_82_gshared (HashSet_1_t2_20 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Remove_m2_82(__this, ___item, method) (( bool (*) (HashSet_1_t2_20 *, Object_t *, const MethodInfo*))HashSet_1_Remove_m2_82_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1_GetObjectData_m2_83_gshared (HashSet_1_t2_20 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method);
#define HashSet_1_GetObjectData_m2_83(__this, ___info, ___context, method) (( void (*) (HashSet_1_t2_20 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))HashSet_1_GetObjectData_m2_83_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::OnDeserialization(System.Object)
extern "C" void HashSet_1_OnDeserialization_m2_84_gshared (HashSet_1_t2_20 * __this, Object_t * ___sender, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m2_84(__this, ___sender, method) (( void (*) (HashSet_1_t2_20 *, Object_t *, const MethodInfo*))HashSet_1_OnDeserialization_m2_84_gshared)(__this, ___sender, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2_23  HashSet_1_GetEnumerator_m2_85_gshared (HashSet_1_t2_20 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m2_85(__this, method) (( Enumerator_t2_23  (*) (HashSet_1_t2_20 *, const MethodInfo*))HashSet_1_GetEnumerator_m2_85_gshared)(__this, method)
