﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`1<System.Boolean>
struct Func_1_t2_14;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C" void Func_1__ctor_m2_58_gshared (Func_1_t2_14 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Func_1__ctor_m2_58(__this, ___object, ___method, method) (( void (*) (Func_1_t2_14 *, Object_t *, IntPtr_t, const MethodInfo*))Func_1__ctor_m2_58_gshared)(__this, ___object, ___method, method)
// TResult System.Func`1<System.Boolean>::Invoke()
extern "C" bool Func_1_Invoke_m2_47_gshared (Func_1_t2_14 * __this, const MethodInfo* method);
#define Func_1_Invoke_m2_47(__this, method) (( bool (*) (Func_1_t2_14 *, const MethodInfo*))Func_1_Invoke_m2_47_gshared)(__this, method)
// System.IAsyncResult System.Func`1<System.Boolean>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * Func_1_BeginInvoke_m2_101_gshared (Func_1_t2_14 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Func_1_BeginInvoke_m2_101(__this, ___callback, ___object, method) (( Object_t * (*) (Func_1_t2_14 *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Func_1_BeginInvoke_m2_101_gshared)(__this, ___callback, ___object, method)
// TResult System.Func`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C" bool Func_1_EndInvoke_m2_102_gshared (Func_1_t2_14 * __this, Object_t * ___result, const MethodInfo* method);
#define Func_1_EndInvoke_m2_102(__this, ___result, method) (( bool (*) (Func_1_t2_14 *, Object_t *, const MethodInfo*))Func_1_EndInvoke_m2_102_gshared)(__this, ___result, method)
