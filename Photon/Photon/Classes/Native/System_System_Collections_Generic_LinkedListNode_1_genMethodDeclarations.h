﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedListNode_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedListNode`1<ExitGames.Client.Photon.SimulationItem>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
#define LinkedListNode_1__ctor_m3_1178(__this, ___list, ___value, method) (( void (*) (LinkedListNode_1_t3_187 *, LinkedList_1_t3_183 *, SimulationItem_t5_28 *, const MethodInfo*))LinkedListNode_1__ctor_m3_1082_gshared)(__this, ___list, ___value, method)
// System.Void System.Collections.Generic.LinkedListNode`1<ExitGames.Client.Photon.SimulationItem>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m3_1179(__this, ___list, ___value, ___previousNode, ___nextNode, method) (( void (*) (LinkedListNode_1_t3_187 *, LinkedList_1_t3_183 *, SimulationItem_t5_28 *, LinkedListNode_1_t3_187 *, LinkedListNode_1_t3_187 *, const MethodInfo*))LinkedListNode_1__ctor_m3_1083_gshared)(__this, ___list, ___value, ___previousNode, ___nextNode, method)
// System.Void System.Collections.Generic.LinkedListNode`1<ExitGames.Client.Photon.SimulationItem>::Detach()
#define LinkedListNode_1_Detach_m3_1180(__this, method) (( void (*) (LinkedListNode_1_t3_187 *, const MethodInfo*))LinkedListNode_1_Detach_m3_1084_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<ExitGames.Client.Photon.SimulationItem>::get_List()
#define LinkedListNode_1_get_List_m3_1181(__this, method) (( LinkedList_1_t3_183 * (*) (LinkedListNode_1_t3_187 *, const MethodInfo*))LinkedListNode_1_get_List_m3_1085_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<ExitGames.Client.Photon.SimulationItem>::get_Next()
#define LinkedListNode_1_get_Next_m3_1028(__this, method) (( LinkedListNode_1_t3_187 * (*) (LinkedListNode_1_t3_187 *, const MethodInfo*))LinkedListNode_1_get_Next_m3_1086_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<ExitGames.Client.Photon.SimulationItem>::get_Value()
#define LinkedListNode_1_get_Value_m3_1029(__this, method) (( SimulationItem_t5_28 * (*) (LinkedListNode_1_t3_187 *, const MethodInfo*))LinkedListNode_1_get_Value_m3_1087_gshared)(__this, method)
