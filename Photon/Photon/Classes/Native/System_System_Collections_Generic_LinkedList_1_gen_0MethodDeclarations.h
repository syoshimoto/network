﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t3_191;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_177;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_1402;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t3_192;
// System.Object[]
struct ObjectU5BU5D_t1_157;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern "C" void LinkedList_1__ctor_m3_1058_gshared (LinkedList_1_t3_191 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m3_1058(__this, method) (( void (*) (LinkedList_1_t3_191 *, const MethodInfo*))LinkedList_1__ctor_m3_1058_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1__ctor_m3_1059_gshared (LinkedList_1_t3_191 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method);
#define LinkedList_1__ctor_m3_1059(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t3_191 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))LinkedList_1__ctor_m3_1059_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3_1060_gshared (LinkedList_1_t3_191 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3_1060(__this, ___value, method) (( void (*) (LinkedList_1_t3_191 *, Object_t *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3_1060_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m3_1061_gshared (LinkedList_1_t3_191 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m3_1061(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t3_191 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m3_1061_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1062_gshared (LinkedList_1_t3_191 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1062(__this, method) (( Object_t* (*) (LinkedList_1_t3_191 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1062_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m3_1063_gshared (LinkedList_1_t3_191 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m3_1063(__this, method) (( Object_t * (*) (LinkedList_1_t3_191 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m3_1063_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3_1064_gshared (LinkedList_1_t3_191 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3_1064(__this, method) (( bool (*) (LinkedList_1_t3_191 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3_1064_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m3_1065_gshared (LinkedList_1_t3_191 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m3_1065(__this, method) (( Object_t * (*) (LinkedList_1_t3_191 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m3_1065_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_VerifyReferencedNode_m3_1066_gshared (LinkedList_1_t3_191 * __this, LinkedListNode_1_t3_192 * ___node, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m3_1066(__this, ___node, method) (( void (*) (LinkedList_1_t3_191 *, LinkedListNode_1_t3_192 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m3_1066_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddBefore(System.Collections.Generic.LinkedListNode`1<T>,T)
extern "C" LinkedListNode_1_t3_192 * LinkedList_1_AddBefore_m3_1067_gshared (LinkedList_1_t3_191 * __this, LinkedListNode_1_t3_192 * ___node, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_AddBefore_m3_1067(__this, ___node, ___value, method) (( LinkedListNode_1_t3_192 * (*) (LinkedList_1_t3_191 *, LinkedListNode_1_t3_192 *, Object_t *, const MethodInfo*))LinkedList_1_AddBefore_m3_1067_gshared)(__this, ___node, ___value, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C" LinkedListNode_1_t3_192 * LinkedList_1_AddLast_m3_1068_gshared (LinkedList_1_t3_191 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_AddLast_m3_1068(__this, ___value, method) (( LinkedListNode_1_t3_192 * (*) (LinkedList_1_t3_191 *, Object_t *, const MethodInfo*))LinkedList_1_AddLast_m3_1068_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C" void LinkedList_1_Clear_m3_1069_gshared (LinkedList_1_t3_191 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m3_1069(__this, method) (( void (*) (LinkedList_1_t3_191 *, const MethodInfo*))LinkedList_1_Clear_m3_1069_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C" bool LinkedList_1_Contains_m3_1070_gshared (LinkedList_1_t3_191 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Contains_m3_1070(__this, ___value, method) (( bool (*) (LinkedList_1_t3_191 *, Object_t *, const MethodInfo*))LinkedList_1_Contains_m3_1070_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void LinkedList_1_CopyTo_m3_1071_gshared (LinkedList_1_t3_191 * __this, ObjectU5BU5D_t1_157* ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_CopyTo_m3_1071(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t3_191 *, ObjectU5BU5D_t1_157*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3_1071_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C" LinkedListNode_1_t3_192 * LinkedList_1_Find_m3_1072_gshared (LinkedList_1_t3_191 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Find_m3_1072(__this, ___value, method) (( LinkedListNode_1_t3_192 * (*) (LinkedList_1_t3_191 *, Object_t *, const MethodInfo*))LinkedList_1_Find_m3_1072_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3_193  LinkedList_1_GetEnumerator_m3_1073_gshared (LinkedList_1_t3_191 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m3_1073(__this, method) (( Enumerator_t3_193  (*) (LinkedList_1_t3_191 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3_1073_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1_GetObjectData_m3_1074_gshared (LinkedList_1_t3_191 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m3_1074(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t3_191 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))LinkedList_1_GetObjectData_m3_1074_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern "C" void LinkedList_1_OnDeserialization_m3_1075_gshared (LinkedList_1_t3_191 * __this, Object_t * ___sender, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m3_1075(__this, ___sender, method) (( void (*) (LinkedList_1_t3_191 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m3_1075_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C" bool LinkedList_1_Remove_m3_1076_gshared (LinkedList_1_t3_191 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Remove_m3_1076(__this, ___value, method) (( bool (*) (LinkedList_1_t3_191 *, Object_t *, const MethodInfo*))LinkedList_1_Remove_m3_1076_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_Remove_m3_1077_gshared (LinkedList_1_t3_191 * __this, LinkedListNode_1_t3_192 * ___node, const MethodInfo* method);
#define LinkedList_1_Remove_m3_1077(__this, ___node, method) (( void (*) (LinkedList_1_t3_191 *, LinkedListNode_1_t3_192 *, const MethodInfo*))LinkedList_1_Remove_m3_1077_gshared)(__this, ___node, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveFirst()
extern "C" void LinkedList_1_RemoveFirst_m3_1078_gshared (LinkedList_1_t3_191 * __this, const MethodInfo* method);
#define LinkedList_1_RemoveFirst_m3_1078(__this, method) (( void (*) (LinkedList_1_t3_191 *, const MethodInfo*))LinkedList_1_RemoveFirst_m3_1078_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C" void LinkedList_1_RemoveLast_m3_1079_gshared (LinkedList_1_t3_191 * __this, const MethodInfo* method);
#define LinkedList_1_RemoveLast_m3_1079(__this, method) (( void (*) (LinkedList_1_t3_191 *, const MethodInfo*))LinkedList_1_RemoveLast_m3_1079_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C" int32_t LinkedList_1_get_Count_m3_1080_gshared (LinkedList_1_t3_191 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m3_1080(__this, method) (( int32_t (*) (LinkedList_1_t3_191 *, const MethodInfo*))LinkedList_1_get_Count_m3_1080_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C" LinkedListNode_1_t3_192 * LinkedList_1_get_First_m3_1081_gshared (LinkedList_1_t3_191 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m3_1081(__this, method) (( LinkedListNode_1_t3_192 * (*) (LinkedList_1_t3_191 *, const MethodInfo*))LinkedList_1_get_First_m3_1081_gshared)(__this, method)
