﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_genMethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<ExitGames.Client.Photon.NCommand>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m3_1137(__this, ___q, method) (( void (*) (Enumerator_t3_198 *, Queue_1_t3_181 *, const MethodInfo*))Enumerator__ctor_m3_1108_gshared)(__this, ___q, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<ExitGames.Client.Photon.NCommand>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3_1138(__this, method) (( void (*) (Enumerator_t3_198 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3_1109_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<ExitGames.Client.Photon.NCommand>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3_1139(__this, method) (( Object_t * (*) (Enumerator_t3_198 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3_1110_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<ExitGames.Client.Photon.NCommand>::Dispose()
#define Enumerator_Dispose_m3_1140(__this, method) (( void (*) (Enumerator_t3_198 *, const MethodInfo*))Enumerator_Dispose_m3_1111_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<ExitGames.Client.Photon.NCommand>::MoveNext()
#define Enumerator_MoveNext_m3_1141(__this, method) (( bool (*) (Enumerator_t3_198 *, const MethodInfo*))Enumerator_MoveNext_m3_1112_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<ExitGames.Client.Photon.NCommand>::get_Current()
#define Enumerator_get_Current_m3_1142(__this, method) (( NCommand_t5_13 * (*) (Enumerator_t3_198 *, const MethodInfo*))Enumerator_get_Current_m3_1113_gshared)(__this, method)
