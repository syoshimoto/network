﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<UnityEngine.Vector3>
struct Queue_1_t3_190;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen_5.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C" void Enumerator__ctor_m3_1256_gshared (Enumerator_t3_204 * __this, Queue_1_t3_190 * ___q, const MethodInfo* method);
#define Enumerator__ctor_m3_1256(__this, ___q, method) (( void (*) (Enumerator_t3_204 *, Queue_1_t3_190 *, const MethodInfo*))Enumerator__ctor_m3_1256_gshared)(__this, ___q, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m3_1257_gshared (Enumerator_t3_204 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3_1257(__this, method) (( void (*) (Enumerator_t3_204 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3_1257_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m3_1258_gshared (Enumerator_t3_204 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3_1258(__this, method) (( Object_t * (*) (Enumerator_t3_204 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3_1258_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C" void Enumerator_Dispose_m3_1259_gshared (Enumerator_t3_204 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3_1259(__this, method) (( void (*) (Enumerator_t3_204 *, const MethodInfo*))Enumerator_Dispose_m3_1259_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C" bool Enumerator_MoveNext_m3_1260_gshared (Enumerator_t3_204 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3_1260(__this, method) (( bool (*) (Enumerator_t3_204 *, const MethodInfo*))Enumerator_MoveNext_m3_1260_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C" Vector3_t6_49  Enumerator_get_Current_m3_1261_gshared (Enumerator_t3_204 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3_1261(__this, method) (( Vector3_t6_49  (*) (Enumerator_t3_204 *, const MethodInfo*))Enumerator_get_Current_m3_1261_gshared)(__this, method)
