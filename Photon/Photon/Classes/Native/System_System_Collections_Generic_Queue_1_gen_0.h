﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExitGames.Client.Photon.CmdLogItem[]
struct CmdLogItemU5BU5D_t5_69;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Queue`1<ExitGames.Client.Photon.CmdLogItem>
struct  Queue_1_t3_184  : public Object_t
{
	// T[] System.Collections.Generic.Queue`1::_array
	CmdLogItemU5BU5D_t5_69* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;
};
