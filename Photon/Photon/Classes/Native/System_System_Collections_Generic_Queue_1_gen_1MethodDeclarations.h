﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen_5MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::.ctor()
#define Queue_1__ctor_m3_1032(__this, method) (( void (*) (Queue_1_t3_182 *, const MethodInfo*))Queue_1__ctor_m3_1094_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::.ctor(System.Int32)
#define Queue_1__ctor_m3_1143(__this, ___count, method) (( void (*) (Queue_1_t3_182 *, int32_t, const MethodInfo*))Queue_1__ctor_m3_1095_gshared)(__this, ___count, method)
// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m3_1144(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_182 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3_1096_gshared)(__this, ___array, ___idx, method)
// System.Object System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1145(__this, method) (( Object_t * (*) (Queue_1_t3_182 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1097_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1146(__this, method) (( Object_t* (*) (Queue_1_t3_182 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1098_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1147(__this, method) (( Object_t * (*) (Queue_1_t3_182 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1099_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::Clear()
#define Queue_1_Clear_m3_1148(__this, method) (( void (*) (Queue_1_t3_182 *, const MethodInfo*))Queue_1_Clear_m3_1100_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m3_1149(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_182 *, MyActionU5BU5D_t5_68*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3_1101_gshared)(__this, ___array, ___idx, method)
// T System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::Dequeue()
#define Queue_1_Dequeue_m3_1036(__this, method) (( MyAction_t5_6 * (*) (Queue_1_t3_182 *, const MethodInfo*))Queue_1_Dequeue_m3_1102_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::Peek()
#define Queue_1_Peek_m3_1150(__this, method) (( MyAction_t5_6 * (*) (Queue_1_t3_182 *, const MethodInfo*))Queue_1_Peek_m3_1103_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::Enqueue(T)
#define Queue_1_Enqueue_m3_1025(__this, ___item, method) (( void (*) (Queue_1_t3_182 *, MyAction_t5_6 *, const MethodInfo*))Queue_1_Enqueue_m3_1104_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m3_1151(__this, ___new_size, method) (( void (*) (Queue_1_t3_182 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3_1105_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::get_Count()
#define Queue_1_get_Count_m3_1152(__this, method) (( int32_t (*) (Queue_1_t3_182 *, const MethodInfo*))Queue_1_get_Count_m3_1106_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<ExitGames.Client.Photon.PeerBase/MyAction>::GetEnumerator()
#define Queue_1_GetEnumerator_m3_1153(__this, method) (( Enumerator_t3_199  (*) (Queue_1_t3_182 *, const MethodInfo*))Queue_1_GetEnumerator_m3_1107_gshared)(__this, method)
