﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen_5MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<System.Byte[]>::.ctor()
#define Queue_1__ctor_m3_1218(__this, method) (( void (*) (Queue_1_t3_186 *, const MethodInfo*))Queue_1__ctor_m3_1094_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Byte[]>::.ctor(System.Int32)
#define Queue_1__ctor_m3_1047(__this, ___count, method) (( void (*) (Queue_1_t3_186 *, int32_t, const MethodInfo*))Queue_1__ctor_m3_1095_gshared)(__this, ___count, method)
// System.Void System.Collections.Generic.Queue`1<System.Byte[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m3_1219(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_186 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3_1096_gshared)(__this, ___array, ___idx, method)
// System.Object System.Collections.Generic.Queue`1<System.Byte[]>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1220(__this, method) (( Object_t * (*) (Queue_1_t3_186 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1097_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Byte[]>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1221(__this, method) (( Object_t* (*) (Queue_1_t3_186 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1098_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Byte[]>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1222(__this, method) (( Object_t * (*) (Queue_1_t3_186 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1099_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Byte[]>::Clear()
#define Queue_1_Clear_m3_1048(__this, method) (( void (*) (Queue_1_t3_186 *, const MethodInfo*))Queue_1_Clear_m3_1100_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Byte[]>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m3_1223(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_186 *, ByteU5BU5DU5BU5D_t1_883*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3_1101_gshared)(__this, ___array, ___idx, method)
// T System.Collections.Generic.Queue`1<System.Byte[]>::Dequeue()
#define Queue_1_Dequeue_m3_1049(__this, method) (( ByteU5BU5D_t1_71* (*) (Queue_1_t3_186 *, const MethodInfo*))Queue_1_Dequeue_m3_1102_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<System.Byte[]>::Peek()
#define Queue_1_Peek_m3_1224(__this, method) (( ByteU5BU5D_t1_71* (*) (Queue_1_t3_186 *, const MethodInfo*))Queue_1_Peek_m3_1103_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Byte[]>::Enqueue(T)
#define Queue_1_Enqueue_m3_1050(__this, ___item, method) (( void (*) (Queue_1_t3_186 *, ByteU5BU5D_t1_71*, const MethodInfo*))Queue_1_Enqueue_m3_1104_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<System.Byte[]>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m3_1225(__this, ___new_size, method) (( void (*) (Queue_1_t3_186 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3_1105_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<System.Byte[]>::get_Count()
#define Queue_1_get_Count_m3_1226(__this, method) (( int32_t (*) (Queue_1_t3_186 *, const MethodInfo*))Queue_1_get_Count_m3_1106_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Byte[]>::GetEnumerator()
#define Queue_1_GetEnumerator_m3_1227(__this, method) (( Enumerator_t3_202  (*) (Queue_1_t3_186 *, const MethodInfo*))Queue_1_GetEnumerator_m3_1107_gshared)(__this, method)
