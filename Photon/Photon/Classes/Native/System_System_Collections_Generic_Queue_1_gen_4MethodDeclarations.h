﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<UnityEngine.Vector3>
struct Queue_1_t3_190;
// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t1_1495;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t6_290;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen_5.h"

// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::.ctor()
extern "C" void Queue_1__ctor_m3_1054_gshared (Queue_1_t3_190 * __this, const MethodInfo* method);
#define Queue_1__ctor_m3_1054(__this, method) (( void (*) (Queue_1_t3_190 *, const MethodInfo*))Queue_1__ctor_m3_1054_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::.ctor(System.Int32)
extern "C" void Queue_1__ctor_m3_1246_gshared (Queue_1_t3_190 * __this, int32_t ___count, const MethodInfo* method);
#define Queue_1__ctor_m3_1246(__this, ___count, method) (( void (*) (Queue_1_t3_190 *, int32_t, const MethodInfo*))Queue_1__ctor_m3_1246_gshared)(__this, ___count, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Queue_1_System_Collections_ICollection_CopyTo_m3_1247_gshared (Queue_1_t3_190 * __this, Array_t * ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m3_1247(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_190 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3_1247_gshared)(__this, ___array, ___idx, method)
// System.Object System.Collections.Generic.Queue`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1248_gshared (Queue_1_t3_190 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1248(__this, method) (( Object_t * (*) (Queue_1_t3_190 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1248_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1249_gshared (Queue_1_t3_190 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1249(__this, method) (( Object_t* (*) (Queue_1_t3_190 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1249_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1250_gshared (Queue_1_t3_190 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1250(__this, method) (( Object_t * (*) (Queue_1_t3_190 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1250_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Clear()
extern "C" void Queue_1_Clear_m3_1251_gshared (Queue_1_t3_190 * __this, const MethodInfo* method);
#define Queue_1_Clear_m3_1251(__this, method) (( void (*) (Queue_1_t3_190 *, const MethodInfo*))Queue_1_Clear_m3_1251_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void Queue_1_CopyTo_m3_1252_gshared (Queue_1_t3_190 * __this, Vector3U5BU5D_t6_290* ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_CopyTo_m3_1252(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_190 *, Vector3U5BU5D_t6_290*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3_1252_gshared)(__this, ___array, ___idx, method)
// T System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Dequeue()
extern "C" Vector3_t6_49  Queue_1_Dequeue_m3_1057_gshared (Queue_1_t3_190 * __this, const MethodInfo* method);
#define Queue_1_Dequeue_m3_1057(__this, method) (( Vector3_t6_49  (*) (Queue_1_t3_190 *, const MethodInfo*))Queue_1_Dequeue_m3_1057_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Peek()
extern "C" Vector3_t6_49  Queue_1_Peek_m3_1055_gshared (Queue_1_t3_190 * __this, const MethodInfo* method);
#define Queue_1_Peek_m3_1055(__this, method) (( Vector3_t6_49  (*) (Queue_1_t3_190 *, const MethodInfo*))Queue_1_Peek_m3_1055_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Enqueue(T)
extern "C" void Queue_1_Enqueue_m3_1056_gshared (Queue_1_t3_190 * __this, Vector3_t6_49  ___item, const MethodInfo* method);
#define Queue_1_Enqueue_m3_1056(__this, ___item, method) (( void (*) (Queue_1_t3_190 *, Vector3_t6_49 , const MethodInfo*))Queue_1_Enqueue_m3_1056_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::SetCapacity(System.Int32)
extern "C" void Queue_1_SetCapacity_m3_1253_gshared (Queue_1_t3_190 * __this, int32_t ___new_size, const MethodInfo* method);
#define Queue_1_SetCapacity_m3_1253(__this, ___new_size, method) (( void (*) (Queue_1_t3_190 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3_1253_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t Queue_1_get_Count_m3_1254_gshared (Queue_1_t3_190 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m3_1254(__this, method) (( int32_t (*) (Queue_1_t3_190 *, const MethodInfo*))Queue_1_get_Count_m3_1254_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Enumerator_t3_204  Queue_1_GetEnumerator_m3_1255_gshared (Queue_1_t3_190 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m3_1255(__this, method) (( Enumerator_t3_204  (*) (Queue_1_t3_190 *, const MethodInfo*))Queue_1_GetEnumerator_m3_1255_gshared)(__this, method)
