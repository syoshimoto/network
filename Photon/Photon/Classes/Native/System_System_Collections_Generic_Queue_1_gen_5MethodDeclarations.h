﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t3_194;
// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_1402;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object[]
struct ObjectU5BU5D_t1_157;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor()
extern "C" void Queue_1__ctor_m3_1094_gshared (Queue_1_t3_194 * __this, const MethodInfo* method);
#define Queue_1__ctor_m3_1094(__this, method) (( void (*) (Queue_1_t3_194 *, const MethodInfo*))Queue_1__ctor_m3_1094_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor(System.Int32)
extern "C" void Queue_1__ctor_m3_1095_gshared (Queue_1_t3_194 * __this, int32_t ___count, const MethodInfo* method);
#define Queue_1__ctor_m3_1095(__this, ___count, method) (( void (*) (Queue_1_t3_194 *, int32_t, const MethodInfo*))Queue_1__ctor_m3_1095_gshared)(__this, ___count, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Queue_1_System_Collections_ICollection_CopyTo_m3_1096_gshared (Queue_1_t3_194 * __this, Array_t * ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m3_1096(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_194 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3_1096_gshared)(__this, ___array, ___idx, method)
// System.Object System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1097_gshared (Queue_1_t3_194 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1097(__this, method) (( Object_t * (*) (Queue_1_t3_194 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1097_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1098_gshared (Queue_1_t3_194 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1098(__this, method) (( Object_t* (*) (Queue_1_t3_194 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1098_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1099_gshared (Queue_1_t3_194 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1099(__this, method) (( Object_t * (*) (Queue_1_t3_194 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1099_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::Clear()
extern "C" void Queue_1_Clear_m3_1100_gshared (Queue_1_t3_194 * __this, const MethodInfo* method);
#define Queue_1_Clear_m3_1100(__this, method) (( void (*) (Queue_1_t3_194 *, const MethodInfo*))Queue_1_Clear_m3_1100_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Queue_1_CopyTo_m3_1101_gshared (Queue_1_t3_194 * __this, ObjectU5BU5D_t1_157* ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_CopyTo_m3_1101(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_194 *, ObjectU5BU5D_t1_157*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3_1101_gshared)(__this, ___array, ___idx, method)
// T System.Collections.Generic.Queue`1<System.Object>::Dequeue()
extern "C" Object_t * Queue_1_Dequeue_m3_1102_gshared (Queue_1_t3_194 * __this, const MethodInfo* method);
#define Queue_1_Dequeue_m3_1102(__this, method) (( Object_t * (*) (Queue_1_t3_194 *, const MethodInfo*))Queue_1_Dequeue_m3_1102_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<System.Object>::Peek()
extern "C" Object_t * Queue_1_Peek_m3_1103_gshared (Queue_1_t3_194 * __this, const MethodInfo* method);
#define Queue_1_Peek_m3_1103(__this, method) (( Object_t * (*) (Queue_1_t3_194 *, const MethodInfo*))Queue_1_Peek_m3_1103_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::Enqueue(T)
extern "C" void Queue_1_Enqueue_m3_1104_gshared (Queue_1_t3_194 * __this, Object_t * ___item, const MethodInfo* method);
#define Queue_1_Enqueue_m3_1104(__this, ___item, method) (( void (*) (Queue_1_t3_194 *, Object_t *, const MethodInfo*))Queue_1_Enqueue_m3_1104_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::SetCapacity(System.Int32)
extern "C" void Queue_1_SetCapacity_m3_1105_gshared (Queue_1_t3_194 * __this, int32_t ___new_size, const MethodInfo* method);
#define Queue_1_SetCapacity_m3_1105(__this, ___new_size, method) (( void (*) (Queue_1_t3_194 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3_1105_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count()
extern "C" int32_t Queue_1_get_Count_m3_1106_gshared (Queue_1_t3_194 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m3_1106(__this, method) (( int32_t (*) (Queue_1_t3_194 *, const MethodInfo*))Queue_1_get_Count_m3_1106_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3_195  Queue_1_GetEnumerator_m3_1107_gshared (Queue_1_t3_194 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m3_1107(__this, method) (( Enumerator_t3_195  (*) (Queue_1_t3_194 *, const MethodInfo*))Queue_1_GetEnumerator_m3_1107_gshared)(__this, method)
