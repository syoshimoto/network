﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Specialized.ListDictionary/DictionaryNodeCollection
struct DictionaryNodeCollection_t3_8;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t3_4;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::.ctor(System.Collections.Specialized.ListDictionary,System.Boolean)
extern "C" void DictionaryNodeCollection__ctor_m3_33 (DictionaryNodeCollection_t3_8 * __this, ListDictionary_t3_4 * ___dict, bool ___isKeyList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::get_Count()
extern "C" int32_t DictionaryNodeCollection_get_Count_m3_34 (DictionaryNodeCollection_t3_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::get_SyncRoot()
extern "C" Object_t * DictionaryNodeCollection_get_SyncRoot_m3_35 (DictionaryNodeCollection_t3_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::CopyTo(System.Array,System.Int32)
extern "C" void DictionaryNodeCollection_CopyTo_m3_36 (DictionaryNodeCollection_t3_8 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::GetEnumerator()
extern "C" Object_t * DictionaryNodeCollection_GetEnumerator_m3_37 (DictionaryNodeCollection_t3_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
