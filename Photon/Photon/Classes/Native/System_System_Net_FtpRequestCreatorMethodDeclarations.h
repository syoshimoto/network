﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.FtpRequestCreator
struct FtpRequestCreator_t3_45;
// System.Net.WebRequest
struct WebRequest_t3_40;
// System.Uri
struct Uri_t3_41;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.FtpRequestCreator::.ctor()
extern "C" void FtpRequestCreator__ctor_m3_192 (FtpRequestCreator_t3_45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FtpRequestCreator::Create(System.Uri)
extern "C" WebRequest_t3_40 * FtpRequestCreator_Create_m3_193 (FtpRequestCreator_t3_45 * __this, Uri_t3_41 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
