﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.HttpRequestCreator
struct HttpRequestCreator_t3_49;
// System.Net.WebRequest
struct WebRequest_t3_40;
// System.Uri
struct Uri_t3_41;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.HttpRequestCreator::.ctor()
extern "C" void HttpRequestCreator__ctor_m3_198 (HttpRequestCreator_t3_49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.HttpRequestCreator::Create(System.Uri)
extern "C" WebRequest_t3_40 * HttpRequestCreator_Create_m3_199 (HttpRequestCreator_t3_49 * __this, Uri_t3_41 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
