﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t3_67;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t4_0;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t3_68;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_547;
// System.Security.Cryptography.Oid
struct Oid_t3_69;
// System.Byte[]
struct ByteU5BU5D_t1_71;
// System.Security.Cryptography.DSA
struct DSA_t1_106;
// System.Security.Cryptography.RSA
struct RSA_t1_118;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.X509Certificates.PublicKey::.ctor(Mono.Security.X509.X509Certificate)
extern "C" void PublicKey__ctor_m3_333 (PublicKey_t3_67 * __this, X509Certificate_t4_0 * ___certificate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedKeyValue()
extern "C" AsnEncodedData_t3_68 * PublicKey_get_EncodedKeyValue_m3_334 (PublicKey_t3_67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedParameters()
extern "C" AsnEncodedData_t3_68 * PublicKey_get_EncodedParameters_m3_335 (PublicKey_t3_67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::get_Key()
extern "C" AsymmetricAlgorithm_t1_547 * PublicKey_get_Key_m3_336 (PublicKey_t3_67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::get_Oid()
extern "C" Oid_t3_69 * PublicKey_get_Oid_m3_337 (PublicKey_t3_67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.PublicKey::GetUnsignedBigInteger(System.Byte[])
extern "C" ByteU5BU5D_t1_71* PublicKey_GetUnsignedBigInteger_m3_338 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_71* ___integer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeDSA(System.Byte[],System.Byte[])
extern "C" DSA_t1_106 * PublicKey_DecodeDSA_m3_339 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_71* ___rawPublicKey, ByteU5BU5D_t1_71* ___rawParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeRSA(System.Byte[])
extern "C" RSA_t1_118 * PublicKey_DecodeRSA_m3_340 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_71* ___rawPublicKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
