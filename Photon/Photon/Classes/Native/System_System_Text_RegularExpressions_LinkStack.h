﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Stack
struct Stack_t1_142;

#include "System_System_Text_RegularExpressions_LinkRef.h"

// System.Text.RegularExpressions.LinkStack
struct  LinkStack_t3_132  : public LinkRef_t3_128
{
	// System.Collections.Stack System.Text.RegularExpressions.LinkStack::stack
	Stack_t1_142 * ___stack_0;
};
