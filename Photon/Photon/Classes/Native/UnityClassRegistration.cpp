struct ClassRegistrationContext;
void InvokeRegisterStaticallyLinkedModuleClasses(ClassRegistrationContext& context)
{
	// Do nothing (we're in stripping mode)
}

void RegisterStaticallyLinkedModulesGranular()
{
	void RegisterModule_Animation();
	RegisterModule_Animation();

	void RegisterModule_Audio();
	RegisterModule_Audio();

	void RegisterModule_Physics();
	RegisterModule_Physics();

	void RegisterModule_Physics2D();
	RegisterModule_Physics2D();

	void RegisterModule_Terrain();
	RegisterModule_Terrain();

	void RegisterModule_TerrainPhysics();
	RegisterModule_TerrainPhysics();

	void RegisterModule_TextRendering();
	RegisterModule_TextRendering();

	void RegisterModule_IMGUI();
	RegisterModule_IMGUI();

}

void RegisterAllClasses()
{
	//Total: 86 classes
	//0. AssetBundle
	void RegisterClass_AssetBundle();
	RegisterClass_AssetBundle();

	//1. NamedObject
	void RegisterClass_NamedObject();
	RegisterClass_NamedObject();

	//2. EditorExtension
	void RegisterClass_EditorExtension();
	RegisterClass_EditorExtension();

	//3. Renderer
	void RegisterClass_Renderer();
	RegisterClass_Renderer();

	//4. Component
	void RegisterClass_Component();
	RegisterClass_Component();

	//5. GUILayer
	void RegisterClass_GUILayer();
	RegisterClass_GUILayer();

	//6. Behaviour
	void RegisterClass_Behaviour();
	RegisterClass_Behaviour();

	//7. Texture
	void RegisterClass_Texture();
	RegisterClass_Texture();

	//8. Texture2D
	void RegisterClass_Texture2D();
	RegisterClass_Texture2D();

	//9. RenderTexture
	void RegisterClass_RenderTexture();
	RegisterClass_RenderTexture();

	//10. RectTransform
	void RegisterClass_RectTransform();
	RegisterClass_RectTransform();

	//11. Transform
	void RegisterClass_Transform();
	RegisterClass_Transform();

	//12. Shader
	void RegisterClass_Shader();
	RegisterClass_Shader();

	//13. TextAsset
	void RegisterClass_TextAsset();
	RegisterClass_TextAsset();

	//14. Material
	void RegisterClass_Material();
	RegisterClass_Material();

	//15. Camera
	void RegisterClass_Camera();
	RegisterClass_Camera();

	//16. MonoBehaviour
	void RegisterClass_MonoBehaviour();
	RegisterClass_MonoBehaviour();

	//17. GameObject
	void RegisterClass_GameObject();
	RegisterClass_GameObject();

	//18. Rigidbody
	void RegisterClass_Rigidbody();
	RegisterClass_Rigidbody();

	//19. Collider
	void RegisterClass_Collider();
	RegisterClass_Collider();

	//20. CharacterController
	void RegisterClass_CharacterController();
	RegisterClass_CharacterController();

	//21. Rigidbody2D
	void RegisterClass_Rigidbody2D();
	RegisterClass_Rigidbody2D();

	//22. AudioClip
	void RegisterClass_AudioClip();
	RegisterClass_AudioClip();

	//23. SampleClip
	void RegisterClass_SampleClip();
	RegisterClass_SampleClip();

	//24. AudioSource
	void RegisterClass_AudioSource();
	RegisterClass_AudioSource();

	//25. AudioBehaviour
	void RegisterClass_AudioBehaviour();
	RegisterClass_AudioBehaviour();

	//26. Animation
	void RegisterClass_Animation();
	RegisterClass_Animation();

	//27. Animator
	void RegisterClass_Animator();
	RegisterClass_Animator();

	//28. DirectorPlayer
	void RegisterClass_DirectorPlayer();
	RegisterClass_DirectorPlayer();

	//29. TextMesh
	void RegisterClass_TextMesh();
	RegisterClass_TextMesh();

	//30. Font
	void RegisterClass_Font();
	RegisterClass_Font();

	//31. Collider2D
	void RegisterClass_Collider2D();
	RegisterClass_Collider2D();

	//32. AnimationClip
	void RegisterClass_AnimationClip();
	RegisterClass_AnimationClip();

	//33. Motion
	void RegisterClass_Motion();
	RegisterClass_Motion();

	//34. SpriteRenderer
	void RegisterClass_SpriteRenderer();
	RegisterClass_SpriteRenderer();

	//35. MeshRenderer
	void RegisterClass_MeshRenderer();
	RegisterClass_MeshRenderer();

	//36. PreloadData
	void RegisterClass_PreloadData();
	RegisterClass_PreloadData();

	//37. Cubemap
	void RegisterClass_Cubemap();
	RegisterClass_Cubemap();

	//38. Texture3D
	void RegisterClass_Texture3D();
	RegisterClass_Texture3D();

	//39. LevelGameManager
	void RegisterClass_LevelGameManager();
	RegisterClass_LevelGameManager();

	//40. GameManager
	void RegisterClass_GameManager();
	RegisterClass_GameManager();

	//41. MeshFilter
	void RegisterClass_MeshFilter();
	RegisterClass_MeshFilter();

	//42. Mesh
	void RegisterClass_Mesh();
	RegisterClass_Mesh();

	//43. BoxCollider2D
	void RegisterClass_BoxCollider2D();
	RegisterClass_BoxCollider2D();

	//44. PhysicsMaterial2D
	void RegisterClass_PhysicsMaterial2D();
	RegisterClass_PhysicsMaterial2D();

	//45. MeshCollider
	void RegisterClass_MeshCollider();
	RegisterClass_MeshCollider();

	//46. BoxCollider
	void RegisterClass_BoxCollider();
	RegisterClass_BoxCollider();

	//47. AudioListener
	void RegisterClass_AudioListener();
	RegisterClass_AudioListener();

	//48. Avatar
	void RegisterClass_Avatar();
	RegisterClass_Avatar();

	//49. AnimatorController
	void RegisterClass_AnimatorController();
	RegisterClass_AnimatorController();

	//50. RuntimeAnimatorController
	void RegisterClass_RuntimeAnimatorController();
	RegisterClass_RuntimeAnimatorController();

	//51. RenderSettings
	void RegisterClass_RenderSettings();
	RegisterClass_RenderSettings();

	//52. Light
	void RegisterClass_Light();
	RegisterClass_Light();

	//53. MonoScript
	void RegisterClass_MonoScript();
	RegisterClass_MonoScript();

	//54. FlareLayer
	void RegisterClass_FlareLayer();
	RegisterClass_FlareLayer();

	//55. GUITexture
	void RegisterClass_GUITexture();
	RegisterClass_GUITexture();

	//56. GUIElement
	void RegisterClass_GUIElement();
	RegisterClass_GUIElement();

	//57. GUIText
	void RegisterClass_GUIText();
	RegisterClass_GUIText();

	//58. SphereCollider
	void RegisterClass_SphereCollider();
	RegisterClass_SphereCollider();

	//59. SkinnedMeshRenderer
	void RegisterClass_SkinnedMeshRenderer();
	RegisterClass_SkinnedMeshRenderer();

	//60. TerrainCollider
	void RegisterClass_TerrainCollider();
	RegisterClass_TerrainCollider();

	//61. TerrainData
	void RegisterClass_TerrainData();
	RegisterClass_TerrainData();

	//62. LightmapSettings
	void RegisterClass_LightmapSettings();
	RegisterClass_LightmapSettings();

	//63. SubstanceArchive
	void RegisterClass_SubstanceArchive();
	RegisterClass_SubstanceArchive();

	//64. ProceduralMaterial
	void RegisterClass_ProceduralMaterial();
	RegisterClass_ProceduralMaterial();

	//65. ProceduralTexture
	void RegisterClass_ProceduralTexture();
	RegisterClass_ProceduralTexture();

	//66. Sprite
	void RegisterClass_Sprite();
	RegisterClass_Sprite();

	//67. Terrain
	void RegisterClass_Terrain();
	RegisterClass_Terrain();

	//68. TimeManager
	void RegisterClass_TimeManager();
	RegisterClass_TimeManager();

	//69. GlobalGameManager
	void RegisterClass_GlobalGameManager();
	RegisterClass_GlobalGameManager();

	//70. AudioManager
	void RegisterClass_AudioManager();
	RegisterClass_AudioManager();

	//71. InputManager
	void RegisterClass_InputManager();
	RegisterClass_InputManager();

	//72. Physics2DSettings
	void RegisterClass_Physics2DSettings();
	RegisterClass_Physics2DSettings();

	//73. GraphicsSettings
	void RegisterClass_GraphicsSettings();
	RegisterClass_GraphicsSettings();

	//74. QualitySettings
	void RegisterClass_QualitySettings();
	RegisterClass_QualitySettings();

	//75. PhysicsManager
	void RegisterClass_PhysicsManager();
	RegisterClass_PhysicsManager();

	//76. TagManager
	void RegisterClass_TagManager();
	RegisterClass_TagManager();

	//77. ScriptMapper
	void RegisterClass_ScriptMapper();
	RegisterClass_ScriptMapper();

	//78. DelayedCallManager
	void RegisterClass_DelayedCallManager();
	RegisterClass_DelayedCallManager();

	//79. MonoManager
	void RegisterClass_MonoManager();
	RegisterClass_MonoManager();

	//80. PlayerSettings
	void RegisterClass_PlayerSettings();
	RegisterClass_PlayerSettings();

	//81. BuildSettings
	void RegisterClass_BuildSettings();
	RegisterClass_BuildSettings();

	//82. ResourceManager
	void RegisterClass_ResourceManager();
	RegisterClass_ResourceManager();

	//83. NetworkManager
	void RegisterClass_NetworkManager();
	RegisterClass_NetworkManager();

	//84. MasterServerInterface
	void RegisterClass_MasterServerInterface();
	RegisterClass_MasterServerInterface();

	//85. RuntimeInitializeOnLoadManager
	void RegisterClass_RuntimeInitializeOnLoadManager();
	RegisterClass_RuntimeInitializeOnLoadManager();

}
