﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// UnityEngine.Object[]
// UnityEngine.Object[]
struct ObjectU5BU5D_t6_252  : public Array_t { };
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct IAchievementDescriptionU5BU5D_t6_266  : public Array_t { };
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t6_268  : public Array_t { };
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t6_218  : public Array_t { };
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t6_214  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t6_19  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t6_20  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct GcLeaderboardU5BU5D_t6_275  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t6_253  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct AchievementU5BU5D_t6_267  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t6_254  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct ScoreU5BU5D_t6_269  : public Array_t { };
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct CameraU5BU5D_t6_224  : public Array_t { };
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct BehaviourU5BU5D_t6_291  : public Array_t { };
// UnityEngine.Component[]
// UnityEngine.Component[]
struct ComponentU5BU5D_t6_274  : public Array_t { };
// UnityEngine.Display[]
// UnityEngine.Display[]
struct DisplayU5BU5D_t6_79  : public Array_t { };
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t6_258  : public Array_t { };
// UnityEngine.ContactPoint[]
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t6_103  : public Array_t { };
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct Rigidbody2DU5BU5D_t6_278  : public Array_t { };
// UnityEngine.ContactPoint2D[]
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t6_116  : public Array_t { };
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t6_259  : public Array_t { };
// UnityEngine.CharacterInfo[]
// UnityEngine.CharacterInfo[]
struct CharacterInfoU5BU5D_t6_260  : public Array_t { };
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t6_261  : public Array_t { };
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t6_262  : public Array_t { };
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t6_263  : public Array_t { };
// UnityEngine.GUIContent[]
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t6_264  : public Array_t { };
// UnityEngine.Rect[]
// UnityEngine.Rect[]
struct RectU5BU5D_t6_265  : public Array_t { };
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t6_165  : public Array_t { };
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct LayoutCacheU5BU5D_t6_279  : public Array_t { };
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t6_280  : public Array_t { };
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t6_179  : public Array_t { };
// UnityEngine.DisallowMultipleComponent[]
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t6_192  : public Array_t { };
// UnityEngine.ExecuteInEditMode[]
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t6_193  : public Array_t { };
// UnityEngine.RequireComponent[]
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t6_194  : public Array_t { };
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t6_223  : public Array_t { };
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct TextEditOpU5BU5D_t6_282  : public Array_t { };
// UnityEngine.Event[]
// UnityEngine.Event[]
struct EventU5BU5D_t6_281  : public Array_t { };
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t6_283  : public Array_t { };
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t6_284  : public Array_t { };
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct TransformU5BU5D_t6_270  : public Array_t { };
// UnityEngine.Color[]
// UnityEngine.Color[]
struct ColorU5BU5D_t6_271  : public Array_t { };
// UnityEngine.Material[]
// UnityEngine.Material[]
struct MaterialU5BU5D_t6_272  : public Array_t { };
// UnityEngine.MonoBehaviour[]
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t6_273  : public Array_t { };
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t6_290  : public Array_t { };
