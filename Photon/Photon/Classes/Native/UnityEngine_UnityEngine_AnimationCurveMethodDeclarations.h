﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnimationCurve
struct AnimationCurve_t6_132;
struct AnimationCurve_t6_132_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t6_259;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m6_793 (AnimationCurve_t6_132 * __this, KeyframeU5BU5D_t6_259* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m6_794 (AnimationCurve_t6_132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m6_795 (AnimationCurve_t6_132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m6_796 (AnimationCurve_t6_132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m6_797 (AnimationCurve_t6_132 * __this, KeyframeU5BU5D_t6_259* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void AnimationCurve_t6_132_marshal(const AnimationCurve_t6_132& unmarshaled, AnimationCurve_t6_132_marshaled& marshaled);
extern "C" void AnimationCurve_t6_132_marshal_back(const AnimationCurve_t6_132_marshaled& marshaled, AnimationCurve_t6_132& unmarshaled);
extern "C" void AnimationCurve_t6_132_marshal_cleanup(AnimationCurve_t6_132_marshaled& marshaled);
