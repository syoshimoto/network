﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Animation/Enumerator
struct Enumerator_t6_134;
// UnityEngine.Animation
struct Animation_t6_135;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Animation/Enumerator::.ctor(UnityEngine.Animation)
extern "C" void Enumerator__ctor_m6_798 (Enumerator_t6_134 * __this, Animation_t6_135 * ___outer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Animation/Enumerator::get_Current()
extern "C" Object_t * Enumerator_get_Current_m6_799 (Enumerator_t6_134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation/Enumerator::MoveNext()
extern "C" bool Enumerator_MoveNext_m6_800 (Enumerator_t6_134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation/Enumerator::Reset()
extern "C" void Enumerator_Reset_m6_801 (Enumerator_t6_134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
