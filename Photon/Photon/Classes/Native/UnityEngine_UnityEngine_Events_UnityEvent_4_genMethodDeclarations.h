﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct UnityEvent_4_t6_288;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C" void UnityEvent_4__ctor_m6_1708_gshared (UnityEvent_4_t6_288 * __this, const MethodInfo* method);
#define UnityEvent_4__ctor_m6_1708(__this, method) (( void (*) (UnityEvent_4_t6_288 *, const MethodInfo*))UnityEvent_4__ctor_m6_1708_gshared)(__this, method)
