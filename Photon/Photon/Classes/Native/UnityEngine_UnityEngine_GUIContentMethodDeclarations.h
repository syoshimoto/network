﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIContent
struct GUIContent_t6_163;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t6_32;
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t6_264;
// System.String[]
struct StringU5BU5D_t1_202;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.GUIContent::.ctor()
extern "C" void GUIContent__ctor_m6_1099 (GUIContent_t6_163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(System.String)
extern "C" void GUIContent__ctor_m6_1100 (GUIContent_t6_163 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(UnityEngine.GUIContent)
extern "C" void GUIContent__ctor_m6_1101 (GUIContent_t6_163 * __this, GUIContent_t6_163 * ___src, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.cctor()
extern "C" void GUIContent__cctor_m6_1102 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUIContent::get_text()
extern "C" String_t* GUIContent_get_text_m6_1103 (GUIContent_t6_163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::set_text(System.String)
extern "C" void GUIContent_set_text_m6_1104 (GUIContent_t6_163 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUIContent::get_tooltip()
extern "C" String_t* GUIContent_get_tooltip_m6_1105 (GUIContent_t6_163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(System.String)
extern "C" GUIContent_t6_163 * GUIContent_Temp_m6_1106 (Object_t * __this /* static, unused */, String_t* ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(UnityEngine.Texture)
extern "C" GUIContent_t6_163 * GUIContent_Temp_m6_1107 (Object_t * __this /* static, unused */, Texture_t6_32 * ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::ClearStaticCache()
extern "C" void GUIContent_ClearStaticCache_m6_1108 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent[] UnityEngine.GUIContent::Temp(System.String[])
extern "C" GUIContentU5BU5D_t6_264* GUIContent_Temp_m6_1109 (Object_t * __this /* static, unused */, StringU5BU5D_t1_202* ___texts, const MethodInfo* method) IL2CPP_METHOD_ATTR;
