﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIWordWrapSizer
struct GUIWordWrapSizer_t6_174;
// UnityEngine.GUIStyle
struct GUIStyle_t6_166;
// UnityEngine.GUIContent
struct GUIContent_t6_163;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t6_165;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.GUIWordWrapSizer::.ctor(UnityEngine.GUIStyle,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern "C" void GUIWordWrapSizer__ctor_m6_1200 (GUIWordWrapSizer_t6_174 * __this, GUIStyle_t6_166 * ___style, GUIContent_t6_163 * ___content, GUILayoutOptionU5BU5D_t6_165* ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIWordWrapSizer::CalcWidth()
extern "C" void GUIWordWrapSizer_CalcWidth_m6_1201 (GUIWordWrapSizer_t6_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIWordWrapSizer::CalcHeight()
extern "C" void GUIWordWrapSizer_CalcHeight_m6_1202 (GUIWordWrapSizer_t6_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
