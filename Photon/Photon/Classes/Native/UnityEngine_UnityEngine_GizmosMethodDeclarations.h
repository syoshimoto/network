﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.Gizmos::DrawWireSphere(UnityEngine.Vector3,System.Single)
extern "C" void Gizmos_DrawWireSphere_m6_159 (Object_t * __this /* static, unused */, Vector3_t6_49  ___center, float ___radius, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireSphere(UnityEngine.Vector3&,System.Single)
extern "C" void Gizmos_INTERNAL_CALL_DrawWireSphere_m6_160 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___center, float ___radius, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawSphere(UnityEngine.Vector3,System.Single)
extern "C" void Gizmos_DrawSphere_m6_161 (Object_t * __this /* static, unused */, Vector3_t6_49  ___center, float ___radius, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawSphere(UnityEngine.Vector3&,System.Single)
extern "C" void Gizmos_INTERNAL_CALL_DrawSphere_m6_162 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___center, float ___radius, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Gizmos_DrawWireCube_m6_163 (Object_t * __this /* static, unused */, Vector3_t6_49  ___center, Vector3_t6_49  ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Gizmos_INTERNAL_CALL_DrawWireCube_m6_164 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___center, Vector3_t6_49 * ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Gizmos_DrawCube_m6_165 (Object_t * __this /* static, unused */, Vector3_t6_49  ___center, Vector3_t6_49  ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Gizmos_INTERNAL_CALL_DrawCube_m6_166 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___center, Vector3_t6_49 * ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C" void Gizmos_set_color_m6_167 (Object_t * __this /* static, unused */, Color_t6_40  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void Gizmos_INTERNAL_set_color_m6_168 (Object_t * __this /* static, unused */, Color_t6_40 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
