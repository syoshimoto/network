﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Particle.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Color.h"

// UnityEngine.Vector3 UnityEngine.Particle::get_position()
extern "C" Vector3_t6_49  Particle_get_position_m6_695 (Particle_t6_96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_position(UnityEngine.Vector3)
extern "C" void Particle_set_position_m6_696 (Particle_t6_96 * __this, Vector3_t6_49  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Particle::get_velocity()
extern "C" Vector3_t6_49  Particle_get_velocity_m6_697 (Particle_t6_96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_velocity(UnityEngine.Vector3)
extern "C" void Particle_set_velocity_m6_698 (Particle_t6_96 * __this, Vector3_t6_49  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Particle::get_energy()
extern "C" float Particle_get_energy_m6_699 (Particle_t6_96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_energy(System.Single)
extern "C" void Particle_set_energy_m6_700 (Particle_t6_96 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Particle::get_startEnergy()
extern "C" float Particle_get_startEnergy_m6_701 (Particle_t6_96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_startEnergy(System.Single)
extern "C" void Particle_set_startEnergy_m6_702 (Particle_t6_96 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Particle::get_size()
extern "C" float Particle_get_size_m6_703 (Particle_t6_96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_size(System.Single)
extern "C" void Particle_set_size_m6_704 (Particle_t6_96 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Particle::get_rotation()
extern "C" float Particle_get_rotation_m6_705 (Particle_t6_96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_rotation(System.Single)
extern "C" void Particle_set_rotation_m6_706 (Particle_t6_96 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Particle::get_angularVelocity()
extern "C" float Particle_get_angularVelocity_m6_707 (Particle_t6_96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_angularVelocity(System.Single)
extern "C" void Particle_set_angularVelocity_m6_708 (Particle_t6_96 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Particle::get_color()
extern "C" Color_t6_40  Particle_get_color_m6_709 (Particle_t6_96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_color(UnityEngine.Color)
extern "C" void Particle_set_color_m6_710 (Particle_t6_96 * __this, Color_t6_40  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
