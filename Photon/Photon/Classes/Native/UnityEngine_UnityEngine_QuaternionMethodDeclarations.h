﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Quaternion__ctor_m6_243 (Quaternion_t6_51 * __this, float ___x, float ___y, float ___z, float ___w, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" Quaternion_t6_51  Quaternion_get_identity_m6_244 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" float Quaternion_Dot_m6_245 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___a, Quaternion_t6_51  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern "C" Quaternion_t6_51  Quaternion_LookRotation_m6_246 (Object_t * __this /* static, unused */, Vector3_t6_49  ___forward, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" Quaternion_t6_51  Quaternion_INTERNAL_CALL_LookRotation_m6_247 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___forward, Vector3_t6_49 * ___upwards, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C" Quaternion_t6_51  Quaternion_Slerp_m6_248 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___a, Quaternion_t6_51  ___b, float ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)
extern "C" Quaternion_t6_51  Quaternion_INTERNAL_CALL_Slerp_m6_249 (Object_t * __this /* static, unused */, Quaternion_t6_51 * ___a, Quaternion_t6_51 * ___b, float ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::SlerpUnclamped(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C" Quaternion_t6_51  Quaternion_SlerpUnclamped_m6_250 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___a, Quaternion_t6_51  ___b, float ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)
extern "C" Quaternion_t6_51  Quaternion_INTERNAL_CALL_SlerpUnclamped_m6_251 (Object_t * __this /* static, unused */, Quaternion_t6_51 * ___a, Quaternion_t6_51 * ___b, float ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C" Quaternion_t6_51  Quaternion_Lerp_m6_252 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___a, Quaternion_t6_51  ___b, float ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)
extern "C" Quaternion_t6_51  Quaternion_INTERNAL_CALL_Lerp_m6_253 (Object_t * __this /* static, unused */, Quaternion_t6_51 * ___a, Quaternion_t6_51 * ___b, float ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::RotateTowards(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C" Quaternion_t6_51  Quaternion_RotateTowards_m6_254 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___from, Quaternion_t6_51  ___to, float ___maxDegreesDelta, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C" Quaternion_t6_51  Quaternion_Inverse_m6_255 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___rotation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&)
extern "C" Quaternion_t6_51  Quaternion_INTERNAL_CALL_Inverse_m6_256 (Object_t * __this /* static, unused */, Quaternion_t6_51 * ___rotation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Quaternion::ToString()
extern "C" String_t* Quaternion_ToString_m6_257 (Quaternion_t6_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" float Quaternion_Angle_m6_258 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___a, Quaternion_t6_51  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C" Vector3_t6_49  Quaternion_get_eulerAngles_m6_259 (Quaternion_t6_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C" Quaternion_t6_51  Quaternion_Euler_m6_260 (Object_t * __this /* static, unused */, float ___x, float ___y, float ___z, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C" Vector3_t6_49  Quaternion_Internal_ToEulerRad_m6_261 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___rotation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&)
extern "C" Vector3_t6_49  Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m6_262 (Object_t * __this /* static, unused */, Quaternion_t6_51 * ___rotation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C" Quaternion_t6_51  Quaternion_Internal_FromEulerRad_m6_263 (Object_t * __this /* static, unused */, Vector3_t6_49  ___euler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&)
extern "C" Quaternion_t6_51  Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6_264 (Object_t * __this /* static, unused */, Vector3_t6_49 * ___euler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C" int32_t Quaternion_GetHashCode_m6_265 (Quaternion_t6_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern "C" bool Quaternion_Equals_m6_266 (Quaternion_t6_51 * __this, Object_t * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" Quaternion_t6_51  Quaternion_op_Multiply_m6_267 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___lhs, Quaternion_t6_51  ___rhs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" Vector3_t6_49  Quaternion_op_Multiply_m6_268 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___rotation, Vector3_t6_49  ___point, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" bool Quaternion_op_Inequality_m6_269 (Object_t * __this /* static, unused */, Quaternion_t6_51  ___lhs, Quaternion_t6_51  ___rhs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
