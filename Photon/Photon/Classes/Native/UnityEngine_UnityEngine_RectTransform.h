﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t6_59;

#include "UnityEngine_UnityEngine_Transform.h"

// UnityEngine.RectTransform
struct  RectTransform_t6_60  : public Transform_t6_61
{
};
struct RectTransform_t6_60_StaticFields{
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t6_59 * ___reapplyDrivenProperties_2;
};
