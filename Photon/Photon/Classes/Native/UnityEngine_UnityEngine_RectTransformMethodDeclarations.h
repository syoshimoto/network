﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.RectTransform
struct RectTransform_t6_60;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern "C" void RectTransform_SendReapplyDrivenProperties_m6_405 (Object_t * __this /* static, unused */, RectTransform_t6_60 * ___driven, const MethodInfo* method) IL2CPP_METHOD_ATTR;
