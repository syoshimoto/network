﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t6_85;
// UnityEngine.Camera
struct Camera_t6_75;

#include "mscorlib_System_ValueType.h"

// UnityEngine.SendMouseEvents/HitInfo
struct  HitInfo_t6_221 
{
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t6_85 * ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_t6_75 * ___camera_1;
};
