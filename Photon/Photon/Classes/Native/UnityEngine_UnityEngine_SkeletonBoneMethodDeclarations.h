﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void SkeletonBone_t6_139_marshal(const SkeletonBone_t6_139& unmarshaled, SkeletonBone_t6_139_marshaled& marshaled);
extern "C" void SkeletonBone_t6_139_marshal_back(const SkeletonBone_t6_139_marshaled& marshaled, SkeletonBone_t6_139& unmarshaled);
extern "C" void SkeletonBone_t6_139_marshal_cleanup(SkeletonBone_t6_139_marshaled& marshaled);
