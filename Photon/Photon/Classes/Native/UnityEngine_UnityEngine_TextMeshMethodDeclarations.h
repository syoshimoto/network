﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.TextMesh
struct TextMesh_t6_145;
// System.String
struct String_t;
// UnityEngine.Font
struct Font_t6_148;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"

// System.Void UnityEngine.TextMesh::set_text(System.String)
extern "C" void TextMesh_set_text_m6_858 (TextMesh_t6_145 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_font(UnityEngine.Font)
extern "C" void TextMesh_set_font_m6_859 (TextMesh_t6_145 * __this, Font_t6_148 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_anchor(UnityEngine.TextAnchor)
extern "C" void TextMesh_set_anchor_m6_860 (TextMesh_t6_145 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_characterSize(System.Single)
extern "C" void TextMesh_set_characterSize_m6_861 (TextMesh_t6_145 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
