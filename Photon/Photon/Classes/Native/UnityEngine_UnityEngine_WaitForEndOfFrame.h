﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_YieldInstruction.h"

// UnityEngine.WaitForEndOfFrame
struct  WaitForEndOfFrame_t6_14  : public YieldInstruction_t6_12
{
};
