﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// System.Object[]
// System.Object[]
struct ObjectU5BU5D_t1_157  : public Array_t { };
// System.Char[]
// System.Char[]
struct CharU5BU5D_t1_16  : public Array_t { };
// System.IConvertible[]
// System.IConvertible[]
struct IConvertibleU5BU5D_t1_1499  : public Array_t { };
// System.IComparable[]
// System.IComparable[]
struct IComparableU5BU5D_t1_1500  : public Array_t { };
// System.IComparable`1<System.Char>[]
// System.IComparable`1<System.Char>[]
struct IComparable_1U5BU5D_t1_1501  : public Array_t { };
// System.IEquatable`1<System.Char>[]
// System.IEquatable`1<System.Char>[]
struct IEquatable_1U5BU5D_t1_1502  : public Array_t { };
// System.ValueType[]
// System.ValueType[]
struct ValueTypeU5BU5D_t1_1503  : public Array_t { };
// System.String[]
// System.String[]
struct StringU5BU5D_t1_202  : public Array_t { };
// System.Collections.IEnumerable[]
// System.Collections.IEnumerable[]
struct IEnumerableU5BU5D_t1_1504  : public Array_t { };
// System.ICloneable[]
// System.ICloneable[]
struct ICloneableU5BU5D_t1_1505  : public Array_t { };
// System.IComparable`1<System.String>[]
// System.IComparable`1<System.String>[]
struct IComparable_1U5BU5D_t1_1506  : public Array_t { };
// System.IEquatable`1<System.String>[]
// System.IEquatable`1<System.String>[]
struct IEquatable_1U5BU5D_t1_1507  : public Array_t { };
// System.Type[]
// System.Type[]
struct TypeU5BU5D_t1_31  : public Array_t { };
// System.Reflection.IReflect[]
// System.Reflection.IReflect[]
struct IReflectU5BU5D_t1_1508  : public Array_t { };
// System.Runtime.InteropServices._Type[]
// System.Runtime.InteropServices._Type[]
struct _TypeU5BU5D_t1_1509  : public Array_t { };
// System.Reflection.MemberInfo[]
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1_517  : public Array_t { };
// System.Reflection.ICustomAttributeProvider[]
// System.Reflection.ICustomAttributeProvider[]
struct ICustomAttributeProviderU5BU5D_t1_1510  : public Array_t { };
// System.Runtime.InteropServices._MemberInfo[]
// System.Runtime.InteropServices._MemberInfo[]
struct _MemberInfoU5BU5D_t1_1511  : public Array_t { };
// System.Byte[]
// System.Byte[]
struct ByteU5BU5D_t1_71  : public Array_t { };
// System.IFormattable[]
// System.IFormattable[]
struct IFormattableU5BU5D_t1_1512  : public Array_t { };
// System.IComparable`1<System.Byte>[]
// System.IComparable`1<System.Byte>[]
struct IComparable_1U5BU5D_t1_1513  : public Array_t { };
// System.IEquatable`1<System.Byte>[]
// System.IEquatable`1<System.Byte>[]
struct IEquatable_1U5BU5D_t1_1514  : public Array_t { };
// System.Single[,]
// System.Single[,]
struct SingleU5BU2CU5D_t1_1515  : public Array_t { };
// System.Single[]
// System.Single[]
struct SingleU5BU5D_t1_863  : public Array_t { };
// System.IComparable`1<System.Single>[]
// System.IComparable`1<System.Single>[]
struct IComparable_1U5BU5D_t1_1516  : public Array_t { };
// System.IEquatable`1<System.Single>[]
// System.IEquatable`1<System.Single>[]
struct IEquatable_1U5BU5D_t1_1517  : public Array_t { };
// System.Int32[]
// System.Int32[]
struct Int32U5BU5D_t1_160  : public Array_t { };
// System.IComparable`1<System.Int32>[]
// System.IComparable`1<System.Int32>[]
struct IComparable_1U5BU5D_t1_1518  : public Array_t { };
// System.IEquatable`1<System.Int32>[]
// System.IEquatable`1<System.Int32>[]
struct IEquatable_1U5BU5D_t1_1519  : public Array_t { };
// System.Delegate[]
// System.Delegate[]
struct DelegateU5BU5D_t1_805  : public Array_t { };
// System.Runtime.Serialization.ISerializable[]
// System.Runtime.Serialization.ISerializable[]
struct ISerializableU5BU5D_t1_1520  : public Array_t { };
// System.Reflection.ParameterInfo[]
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1_812  : public Array_t { };
// System.Runtime.InteropServices._ParameterInfo[]
// System.Runtime.InteropServices._ParameterInfo[]
struct _ParameterInfoU5BU5D_t1_1521  : public Array_t { };
// System.Reflection.ParameterModifier[]
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1_808  : public Array_t { };
// System.UInt16[]
// System.UInt16[]
struct UInt16U5BU5D_t1_584  : public Array_t { };
// System.IComparable`1<System.UInt16>[]
// System.IComparable`1<System.UInt16>[]
struct IComparable_1U5BU5D_t1_1522  : public Array_t { };
// System.IEquatable`1<System.UInt16>[]
// System.IEquatable`1<System.UInt16>[]
struct IEquatable_1U5BU5D_t1_1523  : public Array_t { };
// System.UInt32[]
// System.UInt32[]
struct UInt32U5BU5D_t1_97  : public Array_t { };
// System.IComparable`1<System.UInt32>[]
// System.IComparable`1<System.UInt32>[]
struct IComparable_1U5BU5D_t1_1524  : public Array_t { };
// System.IEquatable`1<System.UInt32>[]
// System.IEquatable`1<System.UInt32>[]
struct IEquatable_1U5BU5D_t1_1525  : public Array_t { };
// System.UInt64[]
// System.UInt64[]
struct UInt64U5BU5D_t1_605  : public Array_t { };
// System.IComparable`1<System.UInt64>[]
// System.IComparable`1<System.UInt64>[]
struct IComparable_1U5BU5D_t1_1526  : public Array_t { };
// System.IEquatable`1<System.UInt64>[]
// System.IEquatable`1<System.UInt64>[]
struct IEquatable_1U5BU5D_t1_1527  : public Array_t { };
// System.Int16[]
// System.Int16[]
struct Int16U5BU5D_t1_832  : public Array_t { };
// System.IComparable`1<System.Int16>[]
// System.IComparable`1<System.Int16>[]
struct IComparable_1U5BU5D_t1_1528  : public Array_t { };
// System.IEquatable`1<System.Int16>[]
// System.IEquatable`1<System.Int16>[]
struct IEquatable_1U5BU5D_t1_1529  : public Array_t { };
// System.SByte[]
// System.SByte[]
struct SByteU5BU5D_t1_662  : public Array_t { };
// System.IComparable`1<System.SByte>[]
// System.IComparable`1<System.SByte>[]
struct IComparable_1U5BU5D_t1_1530  : public Array_t { };
// System.IEquatable`1<System.SByte>[]
// System.IEquatable`1<System.SByte>[]
struct IEquatable_1U5BU5D_t1_1531  : public Array_t { };
// System.Int64[]
// System.Int64[]
struct Int64U5BU5D_t1_806  : public Array_t { };
// System.IComparable`1<System.Int64>[]
// System.IComparable`1<System.Int64>[]
struct IComparable_1U5BU5D_t1_1532  : public Array_t { };
// System.IEquatable`1<System.Int64>[]
// System.IEquatable`1<System.Int64>[]
struct IEquatable_1U5BU5D_t1_1533  : public Array_t { };
// System.Double[]
// System.Double[]
struct DoubleU5BU5D_t1_807  : public Array_t { };
// System.IComparable`1<System.Double>[]
// System.IComparable`1<System.Double>[]
struct IComparable_1U5BU5D_t1_1534  : public Array_t { };
// System.IEquatable`1<System.Double>[]
// System.IEquatable`1<System.Double>[]
struct IEquatable_1U5BU5D_t1_1535  : public Array_t { };
// System.Reflection.FieldInfo[]
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1_814  : public Array_t { };
// System.Runtime.InteropServices._FieldInfo[]
// System.Runtime.InteropServices._FieldInfo[]
struct _FieldInfoU5BU5D_t1_1536  : public Array_t { };
// System.Reflection.MethodInfo[]
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_343  : public Array_t { };
// System.Runtime.InteropServices._MethodInfo[]
// System.Runtime.InteropServices._MethodInfo[]
struct _MethodInfoU5BU5D_t1_1537  : public Array_t { };
// System.Reflection.MethodBase[]
// System.Reflection.MethodBase[]
struct MethodBaseU5BU5D_t1_815  : public Array_t { };
// System.Runtime.InteropServices._MethodBase[]
// System.Runtime.InteropServices._MethodBase[]
struct _MethodBaseU5BU5D_t1_1538  : public Array_t { };
// System.Reflection.ConstructorInfo[]
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t1_813  : public Array_t { };
// System.Runtime.InteropServices._ConstructorInfo[]
// System.Runtime.InteropServices._ConstructorInfo[]
struct _ConstructorInfoU5BU5D_t1_1539  : public Array_t { };
// System.IntPtr[]
// System.IntPtr[]
struct IntPtrU5BU5D_t1_34  : public Array_t { };
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct TableRangeU5BU5D_t1_68  : public Array_t { };
// Mono.Globalization.Unicode.TailoringInfo[]
// Mono.Globalization.Unicode.TailoringInfo[]
struct TailoringInfoU5BU5D_t1_76  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1_1405  : public Array_t { };
// System.Collections.Generic.Link[]
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t1_1010  : public Array_t { };
// System.Collections.DictionaryEntry[]
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t1_1540  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1_1404  : public Array_t { };
// Mono.Globalization.Unicode.Contraction[]
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t1_85  : public Array_t { };
// Mono.Globalization.Unicode.Level2Map[]
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t1_86  : public Array_t { };
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t1_809  : public Array_t { };
// System.Security.Cryptography.KeySizes[]
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1_548  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_1410  : public Array_t { };
// System.Boolean[]
// System.Boolean[]
struct BooleanU5BU5D_t1_264  : public Array_t { };
// System.IComparable`1<System.Boolean>[]
// System.IComparable`1<System.Boolean>[]
struct IComparable_1U5BU5D_t1_1541  : public Array_t { };
// System.IEquatable`1<System.Boolean>[]
// System.IEquatable`1<System.Boolean>[]
struct IEquatable_1U5BU5D_t1_1542  : public Array_t { };
// System.Collections.Hashtable/Slot[]
// System.Collections.Hashtable/Slot[]
struct SlotU5BU5D_t1_176  : public Array_t { };
// System.Collections.SortedList/Slot[]
// System.Collections.SortedList/Slot[]
struct SlotU5BU5D_t1_188  : public Array_t { };
// System.Diagnostics.StackFrame[]
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t1_200  : public Array_t { };
// System.Globalization.Calendar[]
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t1_209  : public Array_t { };
// System.Reflection.Emit.ModuleBuilder[]
// System.Reflection.Emit.ModuleBuilder[]
struct ModuleBuilderU5BU5D_t1_270  : public Array_t { };
// System.Runtime.InteropServices._ModuleBuilder[]
// System.Runtime.InteropServices._ModuleBuilder[]
struct _ModuleBuilderU5BU5D_t1_1543  : public Array_t { };
// System.Reflection.Module[]
// System.Reflection.Module[]
struct ModuleU5BU5D_t1_271  : public Array_t { };
// System.Runtime.InteropServices._Module[]
// System.Runtime.InteropServices._Module[]
struct _ModuleU5BU5D_t1_1544  : public Array_t { };
// System.Reflection.Emit.ParameterBuilder[]
// System.Reflection.Emit.ParameterBuilder[]
struct ParameterBuilderU5BU5D_t1_276  : public Array_t { };
// System.Runtime.InteropServices._ParameterBuilder[]
// System.Runtime.InteropServices._ParameterBuilder[]
struct _ParameterBuilderU5BU5D_t1_1545  : public Array_t { };
// System.Type[][]
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1_277  : public Array_t { };
// System.Array[]
// System.Array[]
struct ArrayU5BU5D_t1_1546  : public Array_t { };
// System.Collections.ICollection[]
// System.Collections.ICollection[]
struct ICollectionU5BU5D_t1_1547  : public Array_t { };
// System.Collections.IList[]
// System.Collections.IList[]
struct IListU5BU5D_t1_1548  : public Array_t { };
// System.Reflection.Emit.ILTokenInfo[]
// System.Reflection.Emit.ILTokenInfo[]
struct ILTokenInfoU5BU5D_t1_286  : public Array_t { };
// System.Reflection.Emit.ILGenerator/LabelData[]
// System.Reflection.Emit.ILGenerator/LabelData[]
struct LabelDataU5BU5D_t1_287  : public Array_t { };
// System.Reflection.Emit.ILGenerator/LabelFixup[]
// System.Reflection.Emit.ILGenerator/LabelFixup[]
struct LabelFixupU5BU5D_t1_288  : public Array_t { };
// System.Reflection.Emit.GenericTypeParameterBuilder[]
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t1_291  : public Array_t { };
// System.Reflection.Emit.TypeBuilder[]
// System.Reflection.Emit.TypeBuilder[]
struct TypeBuilderU5BU5D_t1_294  : public Array_t { };
// System.Runtime.InteropServices._TypeBuilder[]
// System.Runtime.InteropServices._TypeBuilder[]
struct _TypeBuilderU5BU5D_t1_1549  : public Array_t { };
// System.Reflection.Emit.MethodBuilder[]
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t1_301  : public Array_t { };
// System.Runtime.InteropServices._MethodBuilder[]
// System.Runtime.InteropServices._MethodBuilder[]
struct _MethodBuilderU5BU5D_t1_1550  : public Array_t { };
// System.Reflection.Emit.ConstructorBuilder[]
// System.Reflection.Emit.ConstructorBuilder[]
struct ConstructorBuilderU5BU5D_t1_302  : public Array_t { };
// System.Runtime.InteropServices._ConstructorBuilder[]
// System.Runtime.InteropServices._ConstructorBuilder[]
struct _ConstructorBuilderU5BU5D_t1_1551  : public Array_t { };
// System.Reflection.Emit.FieldBuilder[]
// System.Reflection.Emit.FieldBuilder[]
struct FieldBuilderU5BU5D_t1_303  : public Array_t { };
// System.Runtime.InteropServices._FieldBuilder[]
// System.Runtime.InteropServices._FieldBuilder[]
struct _FieldBuilderU5BU5D_t1_1552  : public Array_t { };
// System.Reflection.PropertyInfo[]
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1_816  : public Array_t { };
// System.Runtime.InteropServices._PropertyInfo[]
// System.Runtime.InteropServices._PropertyInfo[]
struct _PropertyInfoU5BU5D_t1_1553  : public Array_t { };
// System.Reflection.CustomAttributeTypedArgument[]
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1_838  : public Array_t { };
// System.Reflection.CustomAttributeNamedArgument[]
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1_839  : public Array_t { };
// System.Resources.ResourceReader/ResourceInfo[]
// System.Resources.ResourceReader/ResourceInfo[]
struct ResourceInfoU5BU5D_t1_367  : public Array_t { };
// System.Resources.ResourceReader/ResourceCacheItem[]
// System.Resources.ResourceReader/ResourceCacheItem[]
struct ResourceCacheItemU5BU5D_t1_368  : public Array_t { };
// System.Runtime.Remoting.Contexts.IContextProperty[]
// System.Runtime.Remoting.Contexts.IContextProperty[]
struct IContextPropertyU5BU5D_t1_821  : public Array_t { };
// System.Runtime.Remoting.Messaging.Header[]
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1_778  : public Array_t { };
// System.Runtime.Remoting.Services.ITrackingHandler[]
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct ITrackingHandlerU5BU5D_t1_859  : public Array_t { };
// System.Runtime.Remoting.Contexts.IContextAttribute[]
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct IContextAttributeU5BU5D_t1_825  : public Array_t { };
// System.DateTime[]
// System.DateTime[]
struct DateTimeU5BU5D_t1_861  : public Array_t { };
// System.IComparable`1<System.DateTime>[]
// System.IComparable`1<System.DateTime>[]
struct IComparable_1U5BU5D_t1_1554  : public Array_t { };
// System.IEquatable`1<System.DateTime>[]
// System.IEquatable`1<System.DateTime>[]
struct IEquatable_1U5BU5D_t1_1555  : public Array_t { };
// System.Decimal[]
// System.Decimal[]
struct DecimalU5BU5D_t1_862  : public Array_t { };
// System.IComparable`1<System.Decimal>[]
// System.IComparable`1<System.Decimal>[]
struct IComparable_1U5BU5D_t1_1556  : public Array_t { };
// System.IEquatable`1<System.Decimal>[]
// System.IEquatable`1<System.Decimal>[]
struct IEquatable_1U5BU5D_t1_1557  : public Array_t { };
// System.TimeSpan[]
// System.TimeSpan[]
struct TimeSpanU5BU5D_t1_864  : public Array_t { };
// System.IComparable`1<System.TimeSpan>[]
// System.IComparable`1<System.TimeSpan>[]
struct IComparable_1U5BU5D_t1_1558  : public Array_t { };
// System.IEquatable`1<System.TimeSpan>[]
// System.IEquatable`1<System.TimeSpan>[]
struct IEquatable_1U5BU5D_t1_1559  : public Array_t { };
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct TypeTagU5BU5D_t1_865  : public Array_t { };
// System.Enum[]
// System.Enum[]
struct EnumU5BU5D_t1_1560  : public Array_t { };
// System.MonoType[]
// System.MonoType[]
struct MonoTypeU5BU5D_t1_868  : public Array_t { };
// System.Byte[,]
// System.Byte[,]
struct ByteU5BU2CU5D_t1_559  : public Array_t { };
// System.Security.Policy.StrongName[]
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t1_1101  : public Array_t { };
// System.Reflection.CustomAttributeData[]
// System.Reflection.CustomAttributeData[]
struct CustomAttributeDataU5BU5D_t1_827  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t1_1417  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t1_1416  : public Array_t { };
// System.Security.Cryptography.X509Certificates.X509Certificate[]
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct X509CertificateU5BU5D_t1_882  : public Array_t { };
// System.Runtime.Serialization.IDeserializationCallback[]
// System.Runtime.Serialization.IDeserializationCallback[]
struct IDeserializationCallbackU5BU5D_t1_1561  : public Array_t { };
// System.Byte[][]
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t1_883  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_1423  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,ExitGames.Client.Photon.NCommand>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,ExitGames.Client.Photon.NCommand>[]
struct KeyValuePair_2U5BU5D_t1_1422  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_1426  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Byte,ExitGames.Client.Photon.EnetChannel>[]
// System.Collections.Generic.KeyValuePair`2<System.Byte,ExitGames.Client.Photon.EnetChannel>[]
struct KeyValuePair_2U5BU5D_t1_1429  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Type,ExitGames.Client.Photon.CustomType>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,ExitGames.Client.Photon.CustomType>[]
struct KeyValuePair_2U5BU5D_t1_1431  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Byte,ExitGames.Client.Photon.CustomType>[]
// System.Collections.Generic.KeyValuePair`2<System.Byte,ExitGames.Client.Photon.CustomType>[]
struct KeyValuePair_2U5BU5D_t1_1434  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
struct KeyValuePair_2U5BU5D_t1_1446  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct KeyValuePair_2U5BU5D_t1_1449  : public Array_t { };
// System.Attribute[]
// System.Attribute[]
struct AttributeU5BU5D_t1_1562  : public Array_t { };
// System.Runtime.InteropServices._Attribute[]
// System.Runtime.InteropServices._Attribute[]
struct _AttributeU5BU5D_t1_1563  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t1_1452  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t1_1451  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>[]
// System.Collections.Generic.KeyValuePair`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>[]
struct KeyValuePair_2U5BU5D_t1_1457  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1_1460  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_1463  : public Array_t { };
// System.Collections.Generic.List`1<PhotonPlayer>[]
// System.Collections.Generic.List`1<PhotonPlayer>[]
struct List_1U5BU5D_t1_1332  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>[]
// System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>[]
struct KeyValuePair_2U5BU5D_t1_1466  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,PhotonPlayer>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,PhotonPlayer>[]
struct KeyValuePair_2U5BU5D_t1_1468  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,RoomInfo>[]
// System.Collections.Generic.KeyValuePair`2<System.String,RoomInfo>[]
struct KeyValuePair_2U5BU5D_t1_1469  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,PhotonView>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,PhotonView>[]
struct KeyValuePair_2U5BU5D_t1_1471  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,ExitGames.Client.Photon.Hashtable>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,ExitGames.Client.Photon.Hashtable>[]
struct KeyValuePair_2U5BU5D_t1_1473  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>[]
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>[]
struct KeyValuePair_2U5BU5D_t1_1476  : public Array_t { };
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>[]
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>[]
struct List_1U5BU5D_t1_1362  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>[]
struct KeyValuePair_2U5BU5D_t1_1479  : public Array_t { };
// System.Object[][]
// System.Object[][]
struct ObjectU5BU5DU5BU5D_t1_1368  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object[]>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object[]>[]
struct KeyValuePair_2U5BU5D_t1_1482  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_1489  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Component,System.Reflection.MethodInfo>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Component,System.Reflection.MethodInfo>[]
struct KeyValuePair_2U5BU5D_t1_1492  : public Array_t { };
