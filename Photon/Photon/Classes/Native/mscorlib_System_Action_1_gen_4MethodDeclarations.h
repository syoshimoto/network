﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen_5MethodDeclarations.h"

// System.Void System.Action`1<UnityEngine.Font>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1_8380(__this, ___object, ___method, method) (( void (*) (Action_1_t1_912 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1_5897_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.Font>::Invoke(T)
#define Action_1_Invoke_m1_5599(__this, ___obj, method) (( void (*) (Action_1_t1_912 *, Font_t6_148 *, const MethodInfo*))Action_1_Invoke_m1_5898_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.Font>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m1_8381(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t1_912 *, Font_t6_148 *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m1_5899_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.Font>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m1_8382(__this, ___result, method) (( void (*) (Action_1_t1_912 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m1_5900_gshared)(__this, ___result, method)
