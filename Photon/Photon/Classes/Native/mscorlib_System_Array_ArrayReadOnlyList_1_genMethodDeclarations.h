﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t1_1002;
// System.Object[]
struct ObjectU5BU5D_t1_157;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_1402;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m1_5969_gshared (ArrayReadOnlyList_1_t1_1002 * __this, ObjectU5BU5D_t1_157* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m1_5969(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t1_1002 *, ObjectU5BU5D_t1_157*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m1_5969_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_5970_gshared (ArrayReadOnlyList_1_t1_1002 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_5970(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1_1002 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_5970_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m1_5971_gshared (ArrayReadOnlyList_1_t1_1002 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m1_5971(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1_1002 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m1_5971_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m1_5972_gshared (ArrayReadOnlyList_1_t1_1002 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m1_5972(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t1_1002 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m1_5972_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m1_5973_gshared (ArrayReadOnlyList_1_t1_1002 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m1_5973(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t1_1002 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m1_5973_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m1_5974_gshared (ArrayReadOnlyList_1_t1_1002 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m1_5974(__this, method) (( bool (*) (ArrayReadOnlyList_1_t1_1002 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m1_5974_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m1_5975_gshared (ArrayReadOnlyList_1_t1_1002 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m1_5975(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1_1002 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m1_5975_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m1_5976_gshared (ArrayReadOnlyList_1_t1_1002 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m1_5976(__this, method) (( void (*) (ArrayReadOnlyList_1_t1_1002 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m1_5976_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m1_5977_gshared (ArrayReadOnlyList_1_t1_1002 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m1_5977(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1_1002 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m1_5977_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m1_5978_gshared (ArrayReadOnlyList_1_t1_1002 * __this, ObjectU5BU5D_t1_157* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m1_5978(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1_1002 *, ObjectU5BU5D_t1_157*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m1_5978_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m1_5979_gshared (ArrayReadOnlyList_1_t1_1002 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m1_5979(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t1_1002 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m1_5979_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m1_5980_gshared (ArrayReadOnlyList_1_t1_1002 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m1_5980(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t1_1002 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m1_5980_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m1_5981_gshared (ArrayReadOnlyList_1_t1_1002 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m1_5981(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1_1002 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m1_5981_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m1_5982_gshared (ArrayReadOnlyList_1_t1_1002 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m1_5982(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1_1002 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m1_5982_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m1_5983_gshared (ArrayReadOnlyList_1_t1_1002 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m1_5983(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1_1002 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m1_5983_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t1_33 * ArrayReadOnlyList_1_ReadOnlyError_m1_5984_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m1_5984(__this /* static, unused */, method) (( Exception_t1_33 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m1_5984_gshared)(__this /* static, unused */, method)
