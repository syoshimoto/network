﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_103.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_8912_gshared (InternalEnumerator_1_t1_1261 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_8912(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1261 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_8912_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_8913_gshared (InternalEnumerator_1_t1_1261 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_8913(__this, method) (( void (*) (InternalEnumerator_1_t1_1261 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_8913_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_8914_gshared (InternalEnumerator_1_t1_1261 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_8914(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1261 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_8914_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_8915_gshared (InternalEnumerator_1_t1_1261 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_8915(__this, method) (( void (*) (InternalEnumerator_1_t1_1261 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_8915_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_8916_gshared (InternalEnumerator_1_t1_1261 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_8916(__this, method) (( bool (*) (InternalEnumerator_1_t1_1261 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_8916_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern "C" HitInfo_t6_221  InternalEnumerator_1_get_Current_m1_8917_gshared (InternalEnumerator_1_t1_1261 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_8917(__this, method) (( HitInfo_t6_221  (*) (InternalEnumerator_1_t1_1261 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_8917_gshared)(__this, method)
