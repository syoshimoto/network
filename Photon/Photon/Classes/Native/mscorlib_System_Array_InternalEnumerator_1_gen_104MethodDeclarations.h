﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_104.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_9009_gshared (InternalEnumerator_1_t1_1267 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_9009(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1267 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_9009_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9010_gshared (InternalEnumerator_1_t1_1267 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9010(__this, method) (( void (*) (InternalEnumerator_1_t1_1267 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9010_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9011_gshared (InternalEnumerator_1_t1_1267 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9011(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1267 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9011_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_9012_gshared (InternalEnumerator_1_t1_1267 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_9012(__this, method) (( void (*) (InternalEnumerator_1_t1_1267 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_9012_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_9013_gshared (InternalEnumerator_1_t1_1267 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_9013(__this, method) (( bool (*) (InternalEnumerator_1_t1_1267 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_9013_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::get_Current()
extern "C" KeyValuePair_2_t1_1266  InternalEnumerator_1_get_Current_m1_9014_gshared (InternalEnumerator_1_t1_1267 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_9014(__this, method) (( KeyValuePair_2_t1_1266  (*) (InternalEnumerator_1_t1_1267 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_9014_gshared)(__this, method)
