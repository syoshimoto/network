﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_107.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"

// System.Void System.Array/InternalEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_9413_gshared (InternalEnumerator_1_t1_1296 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_9413(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1296 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_9413_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9414_gshared (InternalEnumerator_1_t1_1296 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9414(__this, method) (( void (*) (InternalEnumerator_1_t1_1296 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9414_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9415_gshared (InternalEnumerator_1_t1_1296 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9415(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1296 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9415_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_9416_gshared (InternalEnumerator_1_t1_1296 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_9416(__this, method) (( void (*) (InternalEnumerator_1_t1_1296 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_9416_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_9417_gshared (InternalEnumerator_1_t1_1296 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_9417(__this, method) (( bool (*) (InternalEnumerator_1_t1_1296 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_9417_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>::get_Current()
extern "C" uint8_t InternalEnumerator_1_get_Current_m1_9418_gshared (InternalEnumerator_1_t1_1296 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_9418(__this, method) (( uint8_t (*) (InternalEnumerator_1_t1_1296 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_9418_gshared)(__this, method)
