﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_110.h"
#include "AssemblyU2DCSharp_PunTeams_Team.h"

// System.Void System.Array/InternalEnumerator`1<PunTeams/Team>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_9696_gshared (InternalEnumerator_1_t1_1321 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_9696(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1321 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_9696_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<PunTeams/Team>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9697_gshared (InternalEnumerator_1_t1_1321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9697(__this, method) (( void (*) (InternalEnumerator_1_t1_1321 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9697_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<PunTeams/Team>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9698_gshared (InternalEnumerator_1_t1_1321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9698(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1321 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9698_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<PunTeams/Team>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_9699_gshared (InternalEnumerator_1_t1_1321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_9699(__this, method) (( void (*) (InternalEnumerator_1_t1_1321 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_9699_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<PunTeams/Team>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_9700_gshared (InternalEnumerator_1_t1_1321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_9700(__this, method) (( bool (*) (InternalEnumerator_1_t1_1321 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_9700_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<PunTeams/Team>::get_Current()
extern "C" uint8_t InternalEnumerator_1_get_Current_m1_9701_gshared (InternalEnumerator_1_t1_1321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_9701(__this, method) (( uint8_t (*) (InternalEnumerator_1_t1_1321 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_9701_gshared)(__this, method)
