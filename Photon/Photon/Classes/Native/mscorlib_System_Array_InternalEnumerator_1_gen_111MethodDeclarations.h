﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_111.h"
#include "AssemblyU2DCSharp_CubeInter_State.h"

// System.Void System.Array/InternalEnumerator`1<CubeInter/State>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_9896_gshared (InternalEnumerator_1_t1_1335 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_9896(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1335 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_9896_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<CubeInter/State>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9897_gshared (InternalEnumerator_1_t1_1335 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9897(__this, method) (( void (*) (InternalEnumerator_1_t1_1335 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_9897_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<CubeInter/State>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9898_gshared (InternalEnumerator_1_t1_1335 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9898(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1335 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_9898_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<CubeInter/State>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_9899_gshared (InternalEnumerator_1_t1_1335 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_9899(__this, method) (( void (*) (InternalEnumerator_1_t1_1335 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_9899_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<CubeInter/State>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_9900_gshared (InternalEnumerator_1_t1_1335 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_9900(__this, method) (( bool (*) (InternalEnumerator_1_t1_1335 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_9900_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<CubeInter/State>::get_Current()
extern "C" State_t8_41  InternalEnumerator_1_get_Current_m1_9901_gshared (InternalEnumerator_1_t1_1335 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_9901(__this, method) (( State_t8_41  (*) (InternalEnumerator_1_t1_1335 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_9901_gshared)(__this, method)
