﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_117.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen_1.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_10215_gshared (InternalEnumerator_1_t1_1351 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_10215(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1351 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_10215_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_10216_gshared (InternalEnumerator_1_t1_1351 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_10216(__this, method) (( void (*) (InternalEnumerator_1_t1_1351 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_10216_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_10217_gshared (InternalEnumerator_1_t1_1351 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_10217(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1351 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_10217_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_10218_gshared (InternalEnumerator_1_t1_1351 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_10218(__this, method) (( void (*) (InternalEnumerator_1_t1_1351 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_10218_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_10219_gshared (InternalEnumerator_1_t1_1351 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_10219(__this, method) (( bool (*) (InternalEnumerator_1_t1_1351 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_10219_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::get_Current()
extern "C" Link_t2_29  InternalEnumerator_1_get_Current_m1_10220_gshared (InternalEnumerator_1_t1_1351 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_10220(__this, method) (( Link_t2_29  (*) (InternalEnumerator_1_t1_1351 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_10220_gshared)(__this, method)
