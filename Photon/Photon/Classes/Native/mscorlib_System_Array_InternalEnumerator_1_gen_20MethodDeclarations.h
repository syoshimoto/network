﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_20.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_6015_gshared (InternalEnumerator_1_t1_1008 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_6015(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1008 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_6015_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_6016_gshared (InternalEnumerator_1_t1_1008 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_6016(__this, method) (( void (*) (InternalEnumerator_1_t1_1008 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_6016_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_6017_gshared (InternalEnumerator_1_t1_1008 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_6017(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1008 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_6017_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_6018_gshared (InternalEnumerator_1_t1_1008 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_6018(__this, method) (( void (*) (InternalEnumerator_1_t1_1008 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_6018_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_6019_gshared (InternalEnumerator_1_t1_1008 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_6019(__this, method) (( bool (*) (InternalEnumerator_1_t1_1008 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_6019_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern "C" TableRange_t1_66  InternalEnumerator_1_get_Current_m1_6020_gshared (InternalEnumerator_1_t1_1008 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_6020(__this, method) (( TableRange_t1_66  (*) (InternalEnumerator_1_t1_1008 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_6020_gshared)(__this, method)
