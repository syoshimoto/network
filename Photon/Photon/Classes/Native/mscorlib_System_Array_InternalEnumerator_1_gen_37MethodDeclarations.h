﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<System.Type[]>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1_6469(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1060 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_5687_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Type[]>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_6470(__this, method) (( void (*) (InternalEnumerator_1_t1_1060 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_5688_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Type[]>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_6471(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1060 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_5689_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Type[]>::Dispose()
#define InternalEnumerator_1_Dispose_m1_6472(__this, method) (( void (*) (InternalEnumerator_1_t1_1060 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_5690_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Type[]>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1_6473(__this, method) (( bool (*) (InternalEnumerator_1_t1_1060 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_5691_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Type[]>::get_Current()
#define InternalEnumerator_1_get_Current_m1_6474(__this, method) (( TypeU5BU5D_t1_31* (*) (InternalEnumerator_1_t1_1060 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_5692_gshared)(__this, method)
