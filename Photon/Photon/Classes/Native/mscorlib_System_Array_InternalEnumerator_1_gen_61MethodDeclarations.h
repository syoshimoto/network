﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_61.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_7051_gshared (InternalEnumerator_1_t1_1121 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_7051(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1121 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_7051_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_7052_gshared (InternalEnumerator_1_t1_1121 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_7052(__this, method) (( void (*) (InternalEnumerator_1_t1_1121 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_7052_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_7053_gshared (InternalEnumerator_1_t1_1121 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_7053(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1121 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_7053_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_7054_gshared (InternalEnumerator_1_t1_1121 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_7054(__this, method) (( void (*) (InternalEnumerator_1_t1_1121 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_7054_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_7055_gshared (InternalEnumerator_1_t1_1121 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_7055(__this, method) (( bool (*) (InternalEnumerator_1_t1_1121 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_7055_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern "C" Link_t2_22  InternalEnumerator_1_get_Current_m1_7056_gshared (InternalEnumerator_1_t1_1121 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_7056(__this, method) (( Link_t2_22  (*) (InternalEnumerator_1_t1_1121 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_7056_gshared)(__this, method)
