﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_90.h"
#include "UnityEngine_UnityEngine_ContactPoint2D.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_8362_gshared (InternalEnumerator_1_t1_1224 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_8362(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1224 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_8362_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_8363_gshared (InternalEnumerator_1_t1_1224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_8363(__this, method) (( void (*) (InternalEnumerator_1_t1_1224 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_8363_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_8364_gshared (InternalEnumerator_1_t1_1224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_8364(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1224 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_8364_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_8365_gshared (InternalEnumerator_1_t1_1224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_8365(__this, method) (( void (*) (InternalEnumerator_1_t1_1224 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_8365_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_8366_gshared (InternalEnumerator_1_t1_1224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_8366(__this, method) (( bool (*) (InternalEnumerator_1_t1_1224 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_8366_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::get_Current()
extern "C" ContactPoint2D_t6_114  InternalEnumerator_1_get_Current_m1_8367_gshared (InternalEnumerator_1_t1_1224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_8367(__this, method) (( ContactPoint2D_t6_114  (*) (InternalEnumerator_1_t1_1224 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_8367_gshared)(__this, method)
