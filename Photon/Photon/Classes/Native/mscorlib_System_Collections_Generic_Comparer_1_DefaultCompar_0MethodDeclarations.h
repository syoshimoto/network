﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t1_1105;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C" void DefaultComparer__ctor_m1_6951_gshared (DefaultComparer_t1_1105 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_6951(__this, method) (( void (*) (DefaultComparer_t1_1105 *, const MethodInfo*))DefaultComparer__ctor_m1_6951_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m1_6952_gshared (DefaultComparer_t1_1105 * __this, DateTime_t1_127  ___x, DateTime_t1_127  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m1_6952(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1_1105 *, DateTime_t1_127 , DateTime_t1_127 , const MethodInfo*))DefaultComparer_Compare_m1_6952_gshared)(__this, ___x, ___y, method)
