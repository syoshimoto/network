﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<System.DateTimeOffset>
struct Comparer_1_t1_1108;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.Comparer`1<System.DateTimeOffset>::.ctor()
extern "C" void Comparer_1__ctor_m1_6964_gshared (Comparer_1_t1_1108 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m1_6964(__this, method) (( void (*) (Comparer_1_t1_1108 *, const MethodInfo*))Comparer_1__ctor_m1_6964_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.DateTimeOffset>::.cctor()
extern "C" void Comparer_1__cctor_m1_6965_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m1_6965(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m1_6965_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.DateTimeOffset>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m1_6966_gshared (Comparer_1_t1_1108 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m1_6966(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t1_1108 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m1_6966_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.DateTimeOffset>::get_Default()
extern "C" Comparer_1_t1_1108 * Comparer_1_get_Default_m1_6967_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m1_6967(__this /* static, unused */, method) (( Comparer_1_t1_1108 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m1_6967_gshared)(__this /* static, unused */, method)
