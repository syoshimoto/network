﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_10297(__this, ___dictionary, method) (( void (*) (Enumerator_t1_965 *, Dictionary_2_t1_937 *, const MethodInfo*))Enumerator__ctor_m1_7484_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_10298(__this, method) (( Object_t * (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_7485_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_10299(__this, method) (( void (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_7486_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_10300(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_7487_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_10301(__this, method) (( Object_t * (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_7488_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_10302(__this, method) (( Object_t * (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_7489_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::MoveNext()
#define Enumerator_MoveNext_m1_5663(__this, method) (( bool (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_MoveNext_m1_7490_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::get_Current()
#define Enumerator_get_Current_m1_5661(__this, method) (( KeyValuePair_2_t1_964  (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_get_Current_m1_7491_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_10303(__this, method) (( int32_t (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_7492_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_10304(__this, method) (( PhotonView_t8_3 * (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_7493_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::Reset()
#define Enumerator_Reset_m1_10305(__this, method) (( void (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_Reset_m1_7494_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::VerifyState()
#define Enumerator_VerifyState_m1_10306(__this, method) (( void (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_VerifyState_m1_7495_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_10307(__this, method) (( void (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_7496_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PhotonView>::Dispose()
#define Enumerator_Dispose_m1_10308(__this, method) (( void (*) (Enumerator_t1_965 *, const MethodInfo*))Enumerator_Dispose_m1_7497_gshared)(__this, method)
