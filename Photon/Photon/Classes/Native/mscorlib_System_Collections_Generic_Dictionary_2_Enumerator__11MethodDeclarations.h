﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_8046(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1202 *, Dictionary_2_t1_890 *, const MethodInfo*))Enumerator__ctor_m1_7661_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_8047(__this, method) (( Object_t * (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_7662_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_8048(__this, method) (( void (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_7663_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_8049(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_7664_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_8050(__this, method) (( Object_t * (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_7665_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_8051(__this, method) (( Object_t * (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_7666_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::MoveNext()
#define Enumerator_MoveNext_m1_8052(__this, method) (( bool (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_MoveNext_m1_5578_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::get_Current()
#define Enumerator_get_Current_m1_8053(__this, method) (( KeyValuePair_2_t1_1199  (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_get_Current_m1_5575_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_8054(__this, method) (( uint8_t (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_7667_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_8055(__this, method) (( CustomType_t5_50 * (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_7668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::Reset()
#define Enumerator_Reset_m1_8056(__this, method) (( void (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_Reset_m1_7669_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::VerifyState()
#define Enumerator_VerifyState_m1_8057(__this, method) (( void (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_VerifyState_m1_7670_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_8058(__this, method) (( void (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_7671_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.CustomType>::Dispose()
#define Enumerator_Dispose_m1_8059(__this, method) (( void (*) (Enumerator_t1_1202 *, const MethodInfo*))Enumerator_Dispose_m1_5579_gshared)(__this, method)
