﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1_1264;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_9046_gshared (Enumerator_t1_1271 * __this, Dictionary_2_t1_1264 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m1_9046(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1271 *, Dictionary_2_t1_1264 *, const MethodInfo*))Enumerator__ctor_m1_9046_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_9047_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_9047(__this, method) (( Object_t * (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_9047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_9048_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_9048(__this, method) (( void (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_9048_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_167  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9049_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9049(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9049_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9050_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9050(__this, method) (( Object_t * (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9050_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9051_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9051(__this, method) (( Object_t * (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9051_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_9052_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_9052(__this, method) (( bool (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_MoveNext_m1_9052_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" KeyValuePair_2_t1_1266  Enumerator_get_Current_m1_9053_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_9053(__this, method) (( KeyValuePair_2_t1_1266  (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_get_Current_m1_9053_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m1_9054_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1_9054(__this, method) (( Object_t * (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_9054_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m1_9055_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1_9055(__this, method) (( int32_t (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_9055_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C" void Enumerator_Reset_m1_9056_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1_9056(__this, method) (( void (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_Reset_m1_9056_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_9057_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_9057(__this, method) (( void (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_VerifyState_m1_9057_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m1_9058_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1_9058(__this, method) (( void (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_9058_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m1_9059_gshared (Enumerator_t1_1271 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_9059(__this, method) (( void (*) (Enumerator_t1_1271 *, const MethodInfo*))Enumerator_Dispose_m1_9059_gshared)(__this, method)
