﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_9146(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1283 *, Dictionary_2_t1_920 *, const MethodInfo*))Enumerator__ctor_m1_9046_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_9147(__this, method) (( Object_t * (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_9047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_9148(__this, method) (( void (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_9048_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9149(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_9049_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9150(__this, method) (( Object_t * (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_9050_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9151(__this, method) (( Object_t * (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_9051_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m1_9152(__this, method) (( bool (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_MoveNext_m1_9052_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m1_9153(__this, method) (( KeyValuePair_2_t1_1280  (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_get_Current_m1_9053_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_9154(__this, method) (( Event_t6_154 * (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_9054_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_9155(__this, method) (( int32_t (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_9055_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Reset()
#define Enumerator_Reset_m1_9156(__this, method) (( void (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_Reset_m1_9056_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m1_9157(__this, method) (( void (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_VerifyState_m1_9057_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_9158(__this, method) (( void (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_9058_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m1_9159(__this, method) (( void (*) (Enumerator_t1_1283 *, const MethodInfo*))Enumerator_Dispose_m1_9059_gshared)(__this, method)
