﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_10387(__this, ___dictionary, method) (( void (*) (Enumerator_t1_968 *, Dictionary_2_t1_938 *, const MethodInfo*))Enumerator__ctor_m1_7484_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_10388(__this, method) (( Object_t * (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_7485_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_10389(__this, method) (( void (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_7486_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_10390(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_7487_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_10391(__this, method) (( Object_t * (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_7488_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_10392(__this, method) (( Object_t * (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_7489_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::MoveNext()
#define Enumerator_MoveNext_m1_5671(__this, method) (( bool (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_MoveNext_m1_7490_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::get_Current()
#define Enumerator_get_Current_m1_5668(__this, method) (( KeyValuePair_2_t1_967  (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_get_Current_m1_7491_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_10393(__this, method) (( int32_t (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_7492_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_10394(__this, method) (( Hashtable_t5_1 * (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_7493_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::Reset()
#define Enumerator_Reset_m1_10395(__this, method) (( void (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_Reset_m1_7494_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::VerifyState()
#define Enumerator_VerifyState_m1_10396(__this, method) (( void (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_VerifyState_m1_7495_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_10397(__this, method) (( void (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_7496_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ExitGames.Client.Photon.Hashtable>::Dispose()
#define Enumerator_Dispose_m1_10398(__this, method) (( void (*) (Enumerator_t1_968 *, const MethodInfo*))Enumerator_Dispose_m1_7497_gshared)(__this, method)
