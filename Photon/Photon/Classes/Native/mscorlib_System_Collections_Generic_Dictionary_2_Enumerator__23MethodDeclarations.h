﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_10575(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1367 *, Dictionary_2_t1_939 *, const MethodInfo*))Enumerator__ctor_m1_6372_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_10576(__this, method) (( Object_t * (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_6373_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_10577(__this, method) (( void (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_6374_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_10578(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_6375_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_10579(__this, method) (( Object_t * (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_6376_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_10580(__this, method) (( Object_t * (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_6377_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::MoveNext()
#define Enumerator_MoveNext_m1_10581(__this, method) (( bool (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_MoveNext_m1_6378_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::get_Current()
#define Enumerator_get_Current_m1_10582(__this, method) (( KeyValuePair_2_t1_1364  (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_get_Current_m1_6379_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_10583(__this, method) (( Type_t * (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_6380_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_10584(__this, method) (( List_1_t1_893 * (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_6381_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::Reset()
#define Enumerator_Reset_m1_10585(__this, method) (( void (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_Reset_m1_6382_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::VerifyState()
#define Enumerator_VerifyState_m1_10586(__this, method) (( void (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_VerifyState_m1_6383_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_10587(__this, method) (( void (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_6384_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::Dispose()
#define Enumerator_Dispose_m1_10588(__this, method) (( void (*) (Enumerator_t1_1367 *, const MethodInfo*))Enumerator_Dispose_m1_6385_gshared)(__this, method)
