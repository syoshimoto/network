﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_10670(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1373 *, Dictionary_2_t1_940 *, const MethodInfo*))Enumerator__ctor_m1_7484_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_10671(__this, method) (( Object_t * (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_7485_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_10672(__this, method) (( void (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_7486_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_10673(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_7487_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_10674(__this, method) (( Object_t * (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_7488_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_10675(__this, method) (( Object_t * (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_7489_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::MoveNext()
#define Enumerator_MoveNext_m1_10676(__this, method) (( bool (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_MoveNext_m1_7490_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::get_Current()
#define Enumerator_get_Current_m1_10677(__this, method) (( KeyValuePair_2_t1_1370  (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_get_Current_m1_7491_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_10678(__this, method) (( int32_t (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_7492_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_10679(__this, method) (( ObjectU5BU5D_t1_157* (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_7493_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::Reset()
#define Enumerator_Reset_m1_10680(__this, method) (( void (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_Reset_m1_7494_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::VerifyState()
#define Enumerator_VerifyState_m1_10681(__this, method) (( void (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_VerifyState_m1_7495_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_10682(__this, method) (( void (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_7496_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object[]>::Dispose()
#define Enumerator_Dispose_m1_10683(__this, method) (( void (*) (Enumerator_t1_1373 *, const MethodInfo*))Enumerator_Dispose_m1_7497_gshared)(__this, method)
