﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1_1013;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_6155_gshared (Enumerator_t1_1021 * __this, Dictionary_2_t1_1013 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m1_6155(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1021 *, Dictionary_2_t1_1013 *, const MethodInfo*))Enumerator__ctor_m1_6155_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_6156_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_6156(__this, method) (( Object_t * (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_6156_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_6157_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_6157(__this, method) (( void (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_6157_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_167  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_6158_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_6158(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_6158_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_6159_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_6159(__this, method) (( Object_t * (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_6159_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_6160_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_6160(__this, method) (( Object_t * (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_6160_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_6161_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_6161(__this, method) (( bool (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_MoveNext_m1_6161_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t1_1016  Enumerator_get_Current_m1_6162_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_6162(__this, method) (( KeyValuePair_2_t1_1016  (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_get_Current_m1_6162_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m1_6163_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1_6163(__this, method) (( Object_t * (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_6163_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m1_6164_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1_6164(__this, method) (( int32_t (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_6164_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Reset()
extern "C" void Enumerator_Reset_m1_6165_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1_6165(__this, method) (( void (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_Reset_m1_6165_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_6166_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_6166(__this, method) (( void (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_VerifyState_m1_6166_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m1_6167_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1_6167(__this, method) (( void (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_6167_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m1_6168_gshared (Enumerator_t1_1021 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_6168(__this, method) (( void (*) (Enumerator_t1_1021 *, const MethodInfo*))Enumerator_Dispose_m1_6168_gshared)(__this, method)
