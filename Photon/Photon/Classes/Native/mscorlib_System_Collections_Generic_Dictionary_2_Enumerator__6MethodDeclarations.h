﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_7288(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1143 *, Dictionary_2_t1_881 *, const MethodInfo*))Enumerator__ctor_m1_7185_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_7289(__this, method) (( Object_t * (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_7186_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_7290(__this, method) (( void (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_7187_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_7291(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_7188_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_7292(__this, method) (( Object_t * (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_7189_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_7293(__this, method) (( Object_t * (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_7190_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m1_7294(__this, method) (( bool (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_MoveNext_m1_7191_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m1_7295(__this, method) (( KeyValuePair_2_t1_1140  (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_get_Current_m1_7192_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_7296(__this, method) (( String_t* (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_7193_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_7297(__this, method) (( bool (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_7194_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Reset()
#define Enumerator_Reset_m1_7298(__this, method) (( void (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_Reset_m1_7195_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m1_7299(__this, method) (( void (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_VerifyState_m1_7196_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_7300(__this, method) (( void (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_7197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m1_7301(__this, method) (( void (*) (Enumerator_t1_1143 *, const MethodInfo*))Enumerator_Dispose_m1_7198_gshared)(__this, method)
