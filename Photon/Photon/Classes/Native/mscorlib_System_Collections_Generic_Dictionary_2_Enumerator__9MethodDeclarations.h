﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_7804(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1190 *, Dictionary_2_t1_886 *, const MethodInfo*))Enumerator__ctor_m1_7661_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_7805(__this, method) (( Object_t * (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_7662_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_7806(__this, method) (( void (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_7663_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_7807(__this, method) (( DictionaryEntry_t1_167  (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_7664_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_7808(__this, method) (( Object_t * (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_7665_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_7809(__this, method) (( Object_t * (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_7666_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::MoveNext()
#define Enumerator_MoveNext_m1_7810(__this, method) (( bool (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_MoveNext_m1_5578_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::get_Current()
#define Enumerator_get_Current_m1_7811(__this, method) (( KeyValuePair_2_t1_1188  (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_get_Current_m1_5575_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_7812(__this, method) (( uint8_t (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_7667_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_7813(__this, method) (( EnetChannel_t5_5 * (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_7668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::Reset()
#define Enumerator_Reset_m1_7814(__this, method) (( void (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_Reset_m1_7669_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::VerifyState()
#define Enumerator_VerifyState_m1_7815(__this, method) (( void (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_VerifyState_m1_7670_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_7816(__this, method) (( void (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_7671_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::Dispose()
#define Enumerator_Dispose_m1_7817(__this, method) (( void (*) (Enumerator_t1_1190 *, const MethodInfo*))Enumerator_Dispose_m1_5579_gshared)(__this, method)
