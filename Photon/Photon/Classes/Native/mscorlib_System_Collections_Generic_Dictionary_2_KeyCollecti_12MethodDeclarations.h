﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1_1124;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_12.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_7179_gshared (Enumerator_t1_1129 * __this, Dictionary_2_t1_1124 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m1_7179(__this, ___host, method) (( void (*) (Enumerator_t1_1129 *, Dictionary_2_t1_1124 *, const MethodInfo*))Enumerator__ctor_m1_7179_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_7180_gshared (Enumerator_t1_1129 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_7180(__this, method) (( Object_t * (*) (Enumerator_t1_1129 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_7180_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_7181_gshared (Enumerator_t1_1129 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_7181(__this, method) (( void (*) (Enumerator_t1_1129 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_7181_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m1_7182_gshared (Enumerator_t1_1129 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_7182(__this, method) (( void (*) (Enumerator_t1_1129 *, const MethodInfo*))Enumerator_Dispose_m1_7182_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_7183_gshared (Enumerator_t1_1129 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_7183(__this, method) (( bool (*) (Enumerator_t1_1129 *, const MethodInfo*))Enumerator_MoveNext_m1_7183_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m1_7184_gshared (Enumerator_t1_1129 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_7184(__this, method) (( Object_t * (*) (Enumerator_t1_1129 *, const MethodInfo*))Enumerator_get_Current_m1_7184_gshared)(__this, method)
