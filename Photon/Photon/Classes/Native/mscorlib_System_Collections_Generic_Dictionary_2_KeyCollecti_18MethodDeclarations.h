﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_16MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1_7779(__this, ___dictionary, method) (( void (*) (KeyCollection_t1_1189 *, Dictionary_2_t1_886 *, const MethodInfo*))KeyCollection__ctor_m1_7642_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_7780(__this, ___item, method) (( void (*) (KeyCollection_t1_1189 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_7643_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_7781(__this, method) (( void (*) (KeyCollection_t1_1189 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_7644_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_7782(__this, ___item, method) (( bool (*) (KeyCollection_t1_1189 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_7645_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_7783(__this, ___item, method) (( bool (*) (KeyCollection_t1_1189 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_7646_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_7784(__this, method) (( Object_t* (*) (KeyCollection_t1_1189 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_7647_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1_7785(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_1189 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1_7648_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_7786(__this, method) (( Object_t * (*) (KeyCollection_t1_1189 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_7649_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_7787(__this, method) (( bool (*) (KeyCollection_t1_1189 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_7650_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_7788(__this, method) (( Object_t * (*) (KeyCollection_t1_1189 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_7651_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1_7789(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_1189 *, ByteU5BU5D_t1_71*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1_7652_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1_7790(__this, method) (( Enumerator_t1_1430  (*) (KeyCollection_t1_1189 *, const MethodInfo*))KeyCollection_GetEnumerator_m1_7653_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::get_Count()
#define KeyCollection_get_Count_m1_7791(__this, method) (( int32_t (*) (KeyCollection_t1_1189 *, const MethodInfo*))KeyCollection_get_Count_m1_7654_gshared)(__this, method)
