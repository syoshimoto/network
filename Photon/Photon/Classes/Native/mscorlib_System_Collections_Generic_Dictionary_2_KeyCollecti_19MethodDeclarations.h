﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_0MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1_7925(__this, ___dictionary, method) (( void (*) (KeyCollection_t1_1195 *, Dictionary_2_t1_889 *, const MethodInfo*))KeyCollection__ctor_m1_6357_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_7926(__this, ___item, method) (( void (*) (KeyCollection_t1_1195 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_6358_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_7927(__this, method) (( void (*) (KeyCollection_t1_1195 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_6359_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_7928(__this, ___item, method) (( bool (*) (KeyCollection_t1_1195 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_6360_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_7929(__this, ___item, method) (( bool (*) (KeyCollection_t1_1195 *, Type_t *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_6361_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_7930(__this, method) (( Object_t* (*) (KeyCollection_t1_1195 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_6362_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1_7931(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_1195 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1_6363_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_7932(__this, method) (( Object_t * (*) (KeyCollection_t1_1195 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_6364_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_7933(__this, method) (( bool (*) (KeyCollection_t1_1195 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_6365_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_7934(__this, method) (( Object_t * (*) (KeyCollection_t1_1195 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_6366_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1_7935(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_1195 *, TypeU5BU5D_t1_31*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1_6367_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1_7936(__this, method) (( Enumerator_t1_1432  (*) (KeyCollection_t1_1195 *, const MethodInfo*))KeyCollection_GetEnumerator_m1_5551_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,ExitGames.Client.Photon.CustomType>::get_Count()
#define KeyCollection_get_Count_m1_7937(__this, method) (( int32_t (*) (KeyCollection_t1_1195 *, const MethodInfo*))KeyCollection_get_Count_m1_6368_gshared)(__this, method)
