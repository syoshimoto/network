﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1_1264;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_24.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_9040_gshared (Enumerator_t1_1270 * __this, Dictionary_2_t1_1264 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m1_9040(__this, ___host, method) (( void (*) (Enumerator_t1_1270 *, Dictionary_2_t1_1264 *, const MethodInfo*))Enumerator__ctor_m1_9040_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_9041_gshared (Enumerator_t1_1270 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_9041(__this, method) (( Object_t * (*) (Enumerator_t1_1270 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_9041_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_9042_gshared (Enumerator_t1_1270 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_9042(__this, method) (( void (*) (Enumerator_t1_1270 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_9042_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m1_9043_gshared (Enumerator_t1_1270 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_9043(__this, method) (( void (*) (Enumerator_t1_1270 *, const MethodInfo*))Enumerator_Dispose_m1_9043_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_9044_gshared (Enumerator_t1_1270 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_9044(__this, method) (( bool (*) (Enumerator_t1_1270 *, const MethodInfo*))Enumerator_MoveNext_m1_9044_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m1_9045_gshared (Enumerator_t1_1270 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_9045(__this, method) (( Object_t * (*) (Enumerator_t1_1270 *, const MethodInfo*))Enumerator_get_Current_m1_9045_gshared)(__this, method)
