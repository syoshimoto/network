﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>
struct KeyCollection_t1_1322;
// System.Collections.Generic.Dictionary`2<PunTeams/Team,System.Object>
struct Dictionary_2_t1_1316;
// System.Collections.Generic.IEnumerator`1<PunTeams/Team>
struct IEnumerator_1_t1_1465;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// PunTeams/Team[]
struct TeamU5BU5D_t8_184;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PunTeams_Team.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_28.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void KeyCollection__ctor_m1_9702_gshared (KeyCollection_t1_1322 * __this, Dictionary_2_t1_1316 * ___dictionary, const MethodInfo* method);
#define KeyCollection__ctor_m1_9702(__this, ___dictionary, method) (( void (*) (KeyCollection_t1_1322 *, Dictionary_2_t1_1316 *, const MethodInfo*))KeyCollection__ctor_m1_9702_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_9703_gshared (KeyCollection_t1_1322 * __this, uint8_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_9703(__this, ___item, method) (( void (*) (KeyCollection_t1_1322 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_9703_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_9704_gshared (KeyCollection_t1_1322 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_9704(__this, method) (( void (*) (KeyCollection_t1_1322 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_9704_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_9705_gshared (KeyCollection_t1_1322 * __this, uint8_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_9705(__this, ___item, method) (( bool (*) (KeyCollection_t1_1322 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_9705_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_9706_gshared (KeyCollection_t1_1322 * __this, uint8_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_9706(__this, ___item, method) (( bool (*) (KeyCollection_t1_1322 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_9706_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_9707_gshared (KeyCollection_t1_1322 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_9707(__this, method) (( Object_t* (*) (KeyCollection_t1_1322 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_9707_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m1_9708_gshared (KeyCollection_t1_1322 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1_9708(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_1322 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1_9708_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_9709_gshared (KeyCollection_t1_1322 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_9709(__this, method) (( Object_t * (*) (KeyCollection_t1_1322 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_9709_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_9710_gshared (KeyCollection_t1_1322 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_9710(__this, method) (( bool (*) (KeyCollection_t1_1322 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_9710_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_9711_gshared (KeyCollection_t1_1322 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_9711(__this, method) (( Object_t * (*) (KeyCollection_t1_1322 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_9711_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m1_9712_gshared (KeyCollection_t1_1322 * __this, TeamU5BU5D_t8_184* ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_CopyTo_m1_9712(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_1322 *, TeamU5BU5D_t8_184*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1_9712_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_1314  KeyCollection_GetEnumerator_m1_9713_gshared (KeyCollection_t1_1322 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1_9713(__this, method) (( Enumerator_t1_1314  (*) (KeyCollection_t1_1322 *, const MethodInfo*))KeyCollection_GetEnumerator_m1_9713_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Object>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m1_9714_gshared (KeyCollection_t1_1322 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1_9714(__this, method) (( int32_t (*) (KeyCollection_t1_1322 *, const MethodInfo*))KeyCollection_get_Count_m1_9714_gshared)(__this, method)
