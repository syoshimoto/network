﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_14MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1_10361(__this, ___dictionary, method) (( void (*) (KeyCollection_t1_1355 *, Dictionary_2_t1_938 *, const MethodInfo*))KeyCollection__ctor_m1_7465_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_10362(__this, ___item, method) (( void (*) (KeyCollection_t1_1355 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_7466_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_10363(__this, method) (( void (*) (KeyCollection_t1_1355 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_7467_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_10364(__this, ___item, method) (( bool (*) (KeyCollection_t1_1355 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_7468_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_10365(__this, ___item, method) (( bool (*) (KeyCollection_t1_1355 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_7469_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_10366(__this, method) (( Object_t* (*) (KeyCollection_t1_1355 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_7470_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1_10367(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_1355 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1_7471_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_10368(__this, method) (( Object_t * (*) (KeyCollection_t1_1355 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_7472_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_10369(__this, method) (( bool (*) (KeyCollection_t1_1355 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_7473_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_10370(__this, method) (( Object_t * (*) (KeyCollection_t1_1355 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_7474_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1_10371(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_1355 *, Int32U5BU5D_t1_160*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1_7475_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1_10372(__this, method) (( Enumerator_t1_1474  (*) (KeyCollection_t1_1355 *, const MethodInfo*))KeyCollection_GetEnumerator_m1_7476_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ExitGames.Client.Photon.Hashtable>::get_Count()
#define KeyCollection_get_Count_m1_10373(__this, method) (( int32_t (*) (KeyCollection_t1_1355 *, const MethodInfo*))KeyCollection_get_Count_m1_7477_gshared)(__this, method)
