﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_28MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_9605(__this, ___host, method) (( void (*) (Enumerator_t1_954 *, Dictionary_2_t1_951 *, const MethodInfo*))Enumerator__ctor_m1_9606_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_9607(__this, method) (( Object_t * (*) (Enumerator_t1_954 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_9608_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_9609(__this, method) (( void (*) (Enumerator_t1_954 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_9610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::Dispose()
#define Enumerator_Dispose_m1_9611(__this, method) (( void (*) (Enumerator_t1_954 *, const MethodInfo*))Enumerator_Dispose_m1_9612_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::MoveNext()
#define Enumerator_MoveNext_m1_5633(__this, method) (( bool (*) (Enumerator_t1_954 *, const MethodInfo*))Enumerator_MoveNext_m1_9613_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::get_Current()
#define Enumerator_get_Current_m1_5629(__this, method) (( uint8_t (*) (Enumerator_t1_954 *, const MethodInfo*))Enumerator_get_Current_m1_9614_gshared)(__this, method)
