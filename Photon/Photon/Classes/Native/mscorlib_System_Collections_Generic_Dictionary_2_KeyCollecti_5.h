﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>
struct Dictionary_2_t1_951;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>
struct  KeyCollection_t1_957  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection::dictionary
	Dictionary_2_t1_951 * ___dictionary_0;
};
