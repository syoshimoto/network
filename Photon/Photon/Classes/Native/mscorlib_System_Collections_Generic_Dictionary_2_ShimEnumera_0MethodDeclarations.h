﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t1_1051;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1_884;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_6417_gshared (ShimEnumerator_t1_1051 * __this, Dictionary_2_t1_884 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m1_6417(__this, ___host, method) (( void (*) (ShimEnumerator_t1_1051 *, Dictionary_2_t1_884 *, const MethodInfo*))ShimEnumerator__ctor_m1_6417_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_6418_gshared (ShimEnumerator_t1_1051 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1_6418(__this, method) (( bool (*) (ShimEnumerator_t1_1051 *, const MethodInfo*))ShimEnumerator_MoveNext_m1_6418_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1_167  ShimEnumerator_get_Entry_m1_6419_gshared (ShimEnumerator_t1_1051 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1_6419(__this, method) (( DictionaryEntry_t1_167  (*) (ShimEnumerator_t1_1051 *, const MethodInfo*))ShimEnumerator_get_Entry_m1_6419_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_6420_gshared (ShimEnumerator_t1_1051 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1_6420(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1051 *, const MethodInfo*))ShimEnumerator_get_Key_m1_6420_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_6421_gshared (ShimEnumerator_t1_1051 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1_6421(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1051 *, const MethodInfo*))ShimEnumerator_get_Value_m1_6421_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m1_6422_gshared (ShimEnumerator_t1_1051 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1_6422(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1051 *, const MethodInfo*))ShimEnumerator_get_Current_m1_6422_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m1_6423_gshared (ShimEnumerator_t1_1051 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1_6423(__this, method) (( void (*) (ShimEnumerator_t1_1051 *, const MethodInfo*))ShimEnumerator_Reset_m1_6423_gshared)(__this, method)
