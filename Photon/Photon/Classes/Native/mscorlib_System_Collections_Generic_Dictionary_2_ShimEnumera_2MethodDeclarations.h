﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t1_1169;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1_919;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_7533_gshared (ShimEnumerator_t1_1169 * __this, Dictionary_2_t1_919 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m1_7533(__this, ___host, method) (( void (*) (ShimEnumerator_t1_1169 *, Dictionary_2_t1_919 *, const MethodInfo*))ShimEnumerator__ctor_m1_7533_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_7534_gshared (ShimEnumerator_t1_1169 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1_7534(__this, method) (( bool (*) (ShimEnumerator_t1_1169 *, const MethodInfo*))ShimEnumerator_MoveNext_m1_7534_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1_167  ShimEnumerator_get_Entry_m1_7535_gshared (ShimEnumerator_t1_1169 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1_7535(__this, method) (( DictionaryEntry_t1_167  (*) (ShimEnumerator_t1_1169 *, const MethodInfo*))ShimEnumerator_get_Entry_m1_7535_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_7536_gshared (ShimEnumerator_t1_1169 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1_7536(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1169 *, const MethodInfo*))ShimEnumerator_get_Key_m1_7536_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_7537_gshared (ShimEnumerator_t1_1169 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1_7537(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1169 *, const MethodInfo*))ShimEnumerator_get_Value_m1_7537_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m1_7538_gshared (ShimEnumerator_t1_1169 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1_7538(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1169 *, const MethodInfo*))ShimEnumerator_get_Current_m1_7538_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m1_7539_gshared (ShimEnumerator_t1_1169 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1_7539(__this, method) (( void (*) (ShimEnumerator_t1_1169 *, const MethodInfo*))ShimEnumerator_Reset_m1_7539_gshared)(__this, method)
