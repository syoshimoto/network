﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Byte,System.Object>
struct ShimEnumerator_t1_1183;
// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1_888;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Byte,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_7707_gshared (ShimEnumerator_t1_1183 * __this, Dictionary_2_t1_888 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m1_7707(__this, ___host, method) (( void (*) (ShimEnumerator_t1_1183 *, Dictionary_2_t1_888 *, const MethodInfo*))ShimEnumerator__ctor_m1_7707_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Byte,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_7708_gshared (ShimEnumerator_t1_1183 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1_7708(__this, method) (( bool (*) (ShimEnumerator_t1_1183 *, const MethodInfo*))ShimEnumerator_MoveNext_m1_7708_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Byte,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1_167  ShimEnumerator_get_Entry_m1_7709_gshared (ShimEnumerator_t1_1183 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1_7709(__this, method) (( DictionaryEntry_t1_167  (*) (ShimEnumerator_t1_1183 *, const MethodInfo*))ShimEnumerator_get_Entry_m1_7709_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Byte,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_7710_gshared (ShimEnumerator_t1_1183 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1_7710(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1183 *, const MethodInfo*))ShimEnumerator_get_Key_m1_7710_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Byte,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_7711_gshared (ShimEnumerator_t1_1183 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1_7711(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1183 *, const MethodInfo*))ShimEnumerator_get_Value_m1_7711_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Byte,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m1_7712_gshared (ShimEnumerator_t1_1183 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1_7712(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1183 *, const MethodInfo*))ShimEnumerator_get_Current_m1_7712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Byte,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m1_7713_gshared (ShimEnumerator_t1_1183 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1_7713(__this, method) (( void (*) (ShimEnumerator_t1_1183 *, const MethodInfo*))ShimEnumerator_Reset_m1_7713_gshared)(__this, method)
