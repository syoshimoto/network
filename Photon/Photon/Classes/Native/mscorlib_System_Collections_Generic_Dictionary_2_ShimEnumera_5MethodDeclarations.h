﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct ShimEnumerator_t1_1305;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct Dictionary_2_t1_933;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_9487_gshared (ShimEnumerator_t1_1305 * __this, Dictionary_2_t1_933 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m1_9487(__this, ___host, method) (( void (*) (ShimEnumerator_t1_1305 *, Dictionary_2_t1_933 *, const MethodInfo*))ShimEnumerator__ctor_m1_9487_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_9488_gshared (ShimEnumerator_t1_1305 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1_9488(__this, method) (( bool (*) (ShimEnumerator_t1_1305 *, const MethodInfo*))ShimEnumerator_MoveNext_m1_9488_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t1_167  ShimEnumerator_get_Entry_m1_9489_gshared (ShimEnumerator_t1_1305 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1_9489(__this, method) (( DictionaryEntry_t1_167  (*) (ShimEnumerator_t1_1305 *, const MethodInfo*))ShimEnumerator_get_Entry_m1_9489_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_9490_gshared (ShimEnumerator_t1_1305 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1_9490(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1305 *, const MethodInfo*))ShimEnumerator_get_Key_m1_9490_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_9491_gshared (ShimEnumerator_t1_1305 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1_9491(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1305 *, const MethodInfo*))ShimEnumerator_get_Value_m1_9491_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m1_9492_gshared (ShimEnumerator_t1_1305 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1_9492(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1305 *, const MethodInfo*))ShimEnumerator_get_Current_m1_9492_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::Reset()
extern "C" void ShimEnumerator_Reset_m1_9493_gshared (ShimEnumerator_t1_1305 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1_9493(__this, method) (( void (*) (ShimEnumerator_t1_1305 *, const MethodInfo*))ShimEnumerator_Reset_m1_9493_gshared)(__this, method)
