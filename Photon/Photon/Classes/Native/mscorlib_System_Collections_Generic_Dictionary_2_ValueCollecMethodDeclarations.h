﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_19MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_7864(__this, ___host, method) (( void (*) (Enumerator_t1_896 *, Dictionary_2_t1_886 *, const MethodInfo*))Enumerator__ctor_m1_7689_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_7865(__this, method) (( Object_t * (*) (Enumerator_t1_896 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_7690_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_7866(__this, method) (( void (*) (Enumerator_t1_896 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_7691_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::Dispose()
#define Enumerator_Dispose_m1_5563(__this, method) (( void (*) (Enumerator_t1_896 *, const MethodInfo*))Enumerator_Dispose_m1_7692_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::MoveNext()
#define Enumerator_MoveNext_m1_5562(__this, method) (( bool (*) (Enumerator_t1_896 *, const MethodInfo*))Enumerator_MoveNext_m1_7693_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Byte,ExitGames.Client.Photon.EnetChannel>::get_Current()
#define Enumerator_get_Current_m1_5561(__this, method) (( EnetChannel_t5_5 * (*) (Enumerator_t1_896 *, const MethodInfo*))Enumerator_get_Current_m1_7694_gshared)(__this, method)
