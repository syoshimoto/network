﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_18MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1_7792(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_897 *, Dictionary_2_t1_886 *, const MethodInfo*))ValueCollection__ctor_m1_7676_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_7793(__this, ___item, method) (( void (*) (ValueCollection_t1_897 *, EnetChannel_t5_5 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_7677_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_7794(__this, method) (( void (*) (ValueCollection_t1_897 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_7678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_7795(__this, ___item, method) (( bool (*) (ValueCollection_t1_897 *, EnetChannel_t5_5 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_7679_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_7796(__this, ___item, method) (( bool (*) (ValueCollection_t1_897 *, EnetChannel_t5_5 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_7680_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_7797(__this, method) (( Object_t* (*) (ValueCollection_t1_897 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_7681_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_7798(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_897 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_7682_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_7799(__this, method) (( Object_t * (*) (ValueCollection_t1_897 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_7683_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_7800(__this, method) (( bool (*) (ValueCollection_t1_897 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_7684_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_7801(__this, method) (( Object_t * (*) (ValueCollection_t1_897 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_7685_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1_7802(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_897 *, EnetChannelU5BU5D_t5_22*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_7686_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1_5560(__this, method) (( Enumerator_t1_896  (*) (ValueCollection_t1_897 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_7687_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,ExitGames.Client.Photon.EnetChannel>::get_Count()
#define ValueCollection_get_Count_m1_7803(__this, method) (( int32_t (*) (ValueCollection_t1_897 *, const MethodInfo*))ValueCollection_get_Count_m1_7688_gshared)(__this, method)
