﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1_919;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_16.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_7515_gshared (Enumerator_t1_1166 * __this, Dictionary_2_t1_919 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m1_7515(__this, ___host, method) (( void (*) (Enumerator_t1_1166 *, Dictionary_2_t1_919 *, const MethodInfo*))Enumerator__ctor_m1_7515_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_7516_gshared (Enumerator_t1_1166 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_7516(__this, method) (( Object_t * (*) (Enumerator_t1_1166 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_7516_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_7517_gshared (Enumerator_t1_1166 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_7517(__this, method) (( void (*) (Enumerator_t1_1166 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_7517_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_7518_gshared (Enumerator_t1_1166 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_7518(__this, method) (( void (*) (Enumerator_t1_1166 *, const MethodInfo*))Enumerator_Dispose_m1_7518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_7519_gshared (Enumerator_t1_1166 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_7519(__this, method) (( bool (*) (Enumerator_t1_1166 *, const MethodInfo*))Enumerator_MoveNext_m1_7519_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m1_7520_gshared (Enumerator_t1_1166 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_7520(__this, method) (( Object_t * (*) (Enumerator_t1_1166 *, const MethodInfo*))Enumerator_get_Current_m1_7520_gshared)(__this, method)
