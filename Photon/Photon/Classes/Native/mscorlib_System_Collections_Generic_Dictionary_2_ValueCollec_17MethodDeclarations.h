﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_15MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1_7562(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_1171 *, Dictionary_2_t1_885 *, const MethodInfo*))ValueCollection__ctor_m1_7502_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_7563(__this, ___item, method) (( void (*) (ValueCollection_t1_1171 *, NCommand_t5_13 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_7503_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_7564(__this, method) (( void (*) (ValueCollection_t1_1171 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_7504_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_7565(__this, ___item, method) (( bool (*) (ValueCollection_t1_1171 *, NCommand_t5_13 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_7505_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_7566(__this, ___item, method) (( bool (*) (ValueCollection_t1_1171 *, NCommand_t5_13 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_7506_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_7567(__this, method) (( Object_t* (*) (ValueCollection_t1_1171 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_7507_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_7568(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1171 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_7508_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_7569(__this, method) (( Object_t * (*) (ValueCollection_t1_1171 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_7509_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_7570(__this, method) (( bool (*) (ValueCollection_t1_1171 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_7510_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_7571(__this, method) (( Object_t * (*) (ValueCollection_t1_1171 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_7511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1_7572(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1171 *, NCommandU5BU5D_t5_67*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_7512_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1_7573(__this, method) (( Enumerator_t1_1425  (*) (ValueCollection_t1_1171 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_7513_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ExitGames.Client.Photon.NCommand>::get_Count()
#define ValueCollection_get_Count_m1_7574(__this, method) (( int32_t (*) (ValueCollection_t1_1171 *, const MethodInfo*))ValueCollection_get_Count_m1_7514_gshared)(__this, method)
