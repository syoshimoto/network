﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_10MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1_7938(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_1196 *, Dictionary_2_t1_889 *, const MethodInfo*))ValueCollection__ctor_m1_6390_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_7939(__this, ___item, method) (( void (*) (ValueCollection_t1_1196 *, CustomType_t5_50 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_6391_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_7940(__this, method) (( void (*) (ValueCollection_t1_1196 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_6392_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_7941(__this, ___item, method) (( bool (*) (ValueCollection_t1_1196 *, CustomType_t5_50 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_6393_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_7942(__this, ___item, method) (( bool (*) (ValueCollection_t1_1196 *, CustomType_t5_50 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_6394_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_7943(__this, method) (( Object_t* (*) (ValueCollection_t1_1196 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_6395_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_7944(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1196 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_6396_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_7945(__this, method) (( Object_t * (*) (ValueCollection_t1_1196 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_6397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_7946(__this, method) (( bool (*) (ValueCollection_t1_1196 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_6398_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_7947(__this, method) (( Object_t * (*) (ValueCollection_t1_1196 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_6399_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1_7948(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1196 *, CustomTypeU5BU5D_t5_70*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_6400_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1_7949(__this, method) (( Enumerator_t1_1433  (*) (ValueCollection_t1_1196 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_6401_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,ExitGames.Client.Photon.CustomType>::get_Count()
#define ValueCollection_get_Count_m1_7950(__this, method) (( int32_t (*) (ValueCollection_t1_1196 *, const MethodInfo*))ValueCollection_get_Count_m1_6402_gshared)(__this, method)
