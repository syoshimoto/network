﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_16MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,PhotonView>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_10734(__this, ___host, method) (( void (*) (Enumerator_t1_958 *, Dictionary_2_t1_937 *, const MethodInfo*))Enumerator__ctor_m1_7515_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,PhotonView>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_10735(__this, method) (( Object_t * (*) (Enumerator_t1_958 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_7516_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,PhotonView>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_10736(__this, method) (( void (*) (Enumerator_t1_958 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_7517_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,PhotonView>::Dispose()
#define Enumerator_Dispose_m1_10737(__this, method) (( void (*) (Enumerator_t1_958 *, const MethodInfo*))Enumerator_Dispose_m1_7518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,PhotonView>::MoveNext()
#define Enumerator_MoveNext_m1_5646(__this, method) (( bool (*) (Enumerator_t1_958 *, const MethodInfo*))Enumerator_MoveNext_m1_7519_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,PhotonView>::get_Current()
#define Enumerator_get_Current_m1_5645(__this, method) (( PhotonView_t8_3 * (*) (Enumerator_t1_958 *, const MethodInfo*))Enumerator_get_Current_m1_7520_gshared)(__this, method)
