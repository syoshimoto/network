﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::.ctor()
#define Dictionary_2__ctor_m1_5639(__this, method) (( void (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2__ctor_m1_5546_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1_10494(__this, ___comparer, method) (( void (*) (Dictionary_2_t1_939 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_6304_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m1_10495(__this, ___capacity, method) (( void (*) (Dictionary_2_t1_939 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1_5547_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m1_10496(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_939 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))Dictionary_2__ctor_m1_6305_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1_10497(__this, method) (( Object_t * (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1_6306_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1_10498(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_939 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1_6307_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1_10499(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_939 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1_6308_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m1_10500(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_939 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1_6309_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m1_10501(__this, ___key, method) (( bool (*) (Dictionary_2_t1_939 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1_6310_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m1_10502(__this, ___key, method) (( void (*) (Dictionary_2_t1_939 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1_6311_gshared)(__this, ___key, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_10503(__this, method) (( Object_t * (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_6312_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_10504(__this, method) (( bool (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_6313_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_10505(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1_939 *, KeyValuePair_2_t1_1364 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_6314_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_10506(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_939 *, KeyValuePair_2_t1_1364 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_6315_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_10507(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_939 *, KeyValuePair_2U5BU5D_t1_1479*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_6316_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_10508(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_939 *, KeyValuePair_2_t1_1364 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_6317_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1_10509(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_939 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1_6318_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_10510(__this, method) (( Object_t * (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_6319_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_10511(__this, method) (( Object_t* (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_6320_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_10512(__this, method) (( Object_t * (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_6321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::get_Count()
#define Dictionary_2_get_Count_m1_10513(__this, method) (( int32_t (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2_get_Count_m1_6322_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::get_Item(TKey)
#define Dictionary_2_get_Item_m1_10514(__this, ___key, method) (( List_1_t1_893 * (*) (Dictionary_2_t1_939 *, Type_t *, const MethodInfo*))Dictionary_2_get_Item_m1_6323_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m1_10515(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_939 *, Type_t *, List_1_t1_893 *, const MethodInfo*))Dictionary_2_set_Item_m1_5549_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m1_10516(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1_939 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m1_6324_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m1_10517(__this, ___size, method) (( void (*) (Dictionary_2_t1_939 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1_6325_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m1_10518(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_939 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1_6326_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m1_10519(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1_1364  (*) (Object_t * /* static, unused */, Type_t *, List_1_t1_893 *, const MethodInfo*))Dictionary_2_make_pair_m1_6327_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m1_10520(__this /* static, unused */, ___key, ___value, method) (( Type_t * (*) (Object_t * /* static, unused */, Type_t *, List_1_t1_893 *, const MethodInfo*))Dictionary_2_pick_key_m1_6328_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m1_10521(__this /* static, unused */, ___key, ___value, method) (( List_1_t1_893 * (*) (Object_t * /* static, unused */, Type_t *, List_1_t1_893 *, const MethodInfo*))Dictionary_2_pick_value_m1_6329_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m1_10522(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_939 *, KeyValuePair_2U5BU5D_t1_1479*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1_6330_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::Resize()
#define Dictionary_2_Resize_m1_10523(__this, method) (( void (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2_Resize_m1_6331_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::Add(TKey,TValue)
#define Dictionary_2_Add_m1_10524(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_939 *, Type_t *, List_1_t1_893 *, const MethodInfo*))Dictionary_2_Add_m1_6332_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::Clear()
#define Dictionary_2_Clear_m1_10525(__this, method) (( void (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2_Clear_m1_6333_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m1_10526(__this, ___key, method) (( bool (*) (Dictionary_2_t1_939 *, Type_t *, const MethodInfo*))Dictionary_2_ContainsKey_m1_6334_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m1_10527(__this, ___value, method) (( bool (*) (Dictionary_2_t1_939 *, List_1_t1_893 *, const MethodInfo*))Dictionary_2_ContainsValue_m1_6335_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m1_10528(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_939 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))Dictionary_2_GetObjectData_m1_6336_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m1_10529(__this, ___sender, method) (( void (*) (Dictionary_2_t1_939 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m1_6337_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::Remove(TKey)
#define Dictionary_2_Remove_m1_10530(__this, ___key, method) (( bool (*) (Dictionary_2_t1_939 *, Type_t *, const MethodInfo*))Dictionary_2_Remove_m1_6338_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m1_10531(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1_939 *, Type_t *, List_1_t1_893 **, const MethodInfo*))Dictionary_2_TryGetValue_m1_5548_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::get_Keys()
#define Dictionary_2_get_Keys_m1_10532(__this, method) (( KeyCollection_t1_1365 * (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2_get_Keys_m1_5550_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::get_Values()
#define Dictionary_2_get_Values_m1_10533(__this, method) (( ValueCollection_t1_1366 * (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2_get_Values_m1_6339_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m1_10534(__this, ___key, method) (( Type_t * (*) (Dictionary_2_t1_939 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m1_6340_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m1_10535(__this, ___value, method) (( List_1_t1_893 * (*) (Dictionary_2_t1_939 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m1_6341_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m1_10536(__this, ___pair, method) (( bool (*) (Dictionary_2_t1_939 *, KeyValuePair_2_t1_1364 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1_6342_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m1_10537(__this, method) (( Enumerator_t1_1367  (*) (Dictionary_2_t1_939 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1_6343_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m1_10538(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1_167  (*) (Object_t * /* static, unused */, Type_t *, List_1_t1_893 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1_6344_gshared)(__this /* static, unused */, ___key, ___value, method)
