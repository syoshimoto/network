﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1_888;
// System.Collections.Generic.IEqualityComparer`1<System.Byte>
struct IEqualityComparer_1_t1_1174;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_177;
// System.Collections.ICollection
struct ICollection_t1_811;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_1426;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>>
struct IEnumerator_1_t1_1427;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_458;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Byte,System.Object>
struct KeyCollection_t1_1176;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Byte,System.Object>
struct ValueCollection_t1_1179;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m1_5588_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1_5588(__this, method) (( void (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2__ctor_m1_5588_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_7589_gshared (Dictionary_2_t1_888 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_7589(__this, ___comparer, method) (( void (*) (Dictionary_2_t1_888 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_7589_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m1_5556_gshared (Dictionary_2_t1_888 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m1_5556(__this, ___capacity, method) (( void (*) (Dictionary_2_t1_888 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1_5556_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m1_7590_gshared (Dictionary_2_t1_888 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m1_7590(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_888 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))Dictionary_2__ctor_m1_7590_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m1_7591_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1_7591(__this, method) (( Object_t * (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1_7591_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m1_7592_gshared (Dictionary_2_t1_888 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1_7592(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_888 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1_7592_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m1_7593_gshared (Dictionary_2_t1_888 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1_7593(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_888 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1_7593_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m1_7594_gshared (Dictionary_2_t1_888 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1_7594(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_888 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1_7594_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m1_7595_gshared (Dictionary_2_t1_888 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1_7595(__this, ___key, method) (( bool (*) (Dictionary_2_t1_888 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1_7595_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m1_7596_gshared (Dictionary_2_t1_888 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1_7596(__this, ___key, method) (( void (*) (Dictionary_2_t1_888 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1_7596_gshared)(__this, ___key, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_7597_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_7597(__this, method) (( Object_t * (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_7597_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_7598_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_7598(__this, method) (( bool (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_7598_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_7599_gshared (Dictionary_2_t1_888 * __this, KeyValuePair_2_t1_902  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_7599(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1_888 *, KeyValuePair_2_t1_902 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_7599_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_7600_gshared (Dictionary_2_t1_888 * __this, KeyValuePair_2_t1_902  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_7600(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_888 *, KeyValuePair_2_t1_902 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_7600_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_7601_gshared (Dictionary_2_t1_888 * __this, KeyValuePair_2U5BU5D_t1_1426* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_7601(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_888 *, KeyValuePair_2U5BU5D_t1_1426*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_7601_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_7602_gshared (Dictionary_2_t1_888 * __this, KeyValuePair_2_t1_902  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_7602(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_888 *, KeyValuePair_2_t1_902 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_7602_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m1_7603_gshared (Dictionary_2_t1_888 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1_7603(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_888 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1_7603_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_7604_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_7604(__this, method) (( Object_t * (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_7604_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_7605_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_7605(__this, method) (( Object_t* (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_7605_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_7606_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_7606(__this, method) (( Object_t * (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_7606_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m1_7607_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1_7607(__this, method) (( int32_t (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2_get_Count_m1_7607_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m1_7608_gshared (Dictionary_2_t1_888 * __this, uint8_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m1_7608(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_888 *, uint8_t, const MethodInfo*))Dictionary_2_get_Item_m1_7608_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m1_7609_gshared (Dictionary_2_t1_888 * __this, uint8_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m1_7609(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_888 *, uint8_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m1_7609_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m1_7610_gshared (Dictionary_2_t1_888 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m1_7610(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1_888 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m1_7610_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m1_7611_gshared (Dictionary_2_t1_888 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1_7611(__this, ___size, method) (( void (*) (Dictionary_2_t1_888 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1_7611_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m1_7612_gshared (Dictionary_2_t1_888 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1_7612(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_888 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1_7612_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1_902  Dictionary_2_make_pair_m1_7613_gshared (Object_t * __this /* static, unused */, uint8_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m1_7613(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1_902  (*) (Object_t * /* static, unused */, uint8_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m1_7613_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::pick_key(TKey,TValue)
extern "C" uint8_t Dictionary_2_pick_key_m1_7614_gshared (Object_t * __this /* static, unused */, uint8_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m1_7614(__this /* static, unused */, ___key, ___value, method) (( uint8_t (*) (Object_t * /* static, unused */, uint8_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m1_7614_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m1_7615_gshared (Object_t * __this /* static, unused */, uint8_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m1_7615(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, uint8_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m1_7615_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m1_7616_gshared (Dictionary_2_t1_888 * __this, KeyValuePair_2U5BU5D_t1_1426* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1_7616(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_888 *, KeyValuePair_2U5BU5D_t1_1426*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1_7616_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m1_7617_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1_7617(__this, method) (( void (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2_Resize_m1_7617_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m1_7618_gshared (Dictionary_2_t1_888 * __this, uint8_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m1_7618(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_888 *, uint8_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m1_7618_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m1_7619_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1_7619(__this, method) (( void (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2_Clear_m1_7619_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m1_7620_gshared (Dictionary_2_t1_888 * __this, uint8_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1_7620(__this, ___key, method) (( bool (*) (Dictionary_2_t1_888 *, uint8_t, const MethodInfo*))Dictionary_2_ContainsKey_m1_7620_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m1_7621_gshared (Dictionary_2_t1_888 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1_7621(__this, ___value, method) (( bool (*) (Dictionary_2_t1_888 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m1_7621_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m1_7622_gshared (Dictionary_2_t1_888 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1_7622(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_888 *, SerializationInfo_t1_177 *, StreamingContext_t1_514 , const MethodInfo*))Dictionary_2_GetObjectData_m1_7622_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m1_7623_gshared (Dictionary_2_t1_888 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1_7623(__this, ___sender, method) (( void (*) (Dictionary_2_t1_888 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m1_7623_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m1_7624_gshared (Dictionary_2_t1_888 * __this, uint8_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m1_7624(__this, ___key, method) (( bool (*) (Dictionary_2_t1_888 *, uint8_t, const MethodInfo*))Dictionary_2_Remove_m1_7624_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m1_7625_gshared (Dictionary_2_t1_888 * __this, uint8_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1_7625(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1_888 *, uint8_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m1_7625_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Keys()
extern "C" KeyCollection_t1_1176 * Dictionary_2_get_Keys_m1_7626_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1_7626(__this, method) (( KeyCollection_t1_1176 * (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2_get_Keys_m1_7626_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::get_Values()
extern "C" ValueCollection_t1_1179 * Dictionary_2_get_Values_m1_7627_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1_7627(__this, method) (( ValueCollection_t1_1179 * (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2_get_Values_m1_7627_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::ToTKey(System.Object)
extern "C" uint8_t Dictionary_2_ToTKey_m1_7628_gshared (Dictionary_2_t1_888 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1_7628(__this, ___key, method) (( uint8_t (*) (Dictionary_2_t1_888 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m1_7628_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m1_7629_gshared (Dictionary_2_t1_888 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1_7629(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t1_888 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m1_7629_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m1_7630_gshared (Dictionary_2_t1_888 * __this, KeyValuePair_2_t1_902  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1_7630(__this, ___pair, method) (( bool (*) (Dictionary_2_t1_888 *, KeyValuePair_2_t1_902 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1_7630_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_903  Dictionary_2_GetEnumerator_m1_5574_gshared (Dictionary_2_t1_888 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1_5574(__this, method) (( Enumerator_t1_903  (*) (Dictionary_2_t1_888 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1_5574_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1_167  Dictionary_2_U3CCopyToU3Em__0_m1_7631_gshared (Object_t * __this /* static, unused */, uint8_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1_7631(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1_167  (*) (Object_t * /* static, unused */, uint8_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1_7631_gshared)(__this /* static, unused */, ___key, ___value, method)
