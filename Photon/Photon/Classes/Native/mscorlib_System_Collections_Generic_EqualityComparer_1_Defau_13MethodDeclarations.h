﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ExitGames.Client.Photon.ConnectionProtocol>
struct DefaultComparer_t1_1307;

#include "codegen/il2cpp-codegen.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ExitGames.Client.Photon.ConnectionProtocol>::.ctor()
extern "C" void DefaultComparer__ctor_m1_9499_gshared (DefaultComparer_t1_1307 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_9499(__this, method) (( void (*) (DefaultComparer_t1_1307 *, const MethodInfo*))DefaultComparer__ctor_m1_9499_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ExitGames.Client.Photon.ConnectionProtocol>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_9500_gshared (DefaultComparer_t1_1307 * __this, uint8_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1_9500(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1_1307 *, uint8_t, const MethodInfo*))DefaultComparer_GetHashCode_m1_9500_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ExitGames.Client.Photon.ConnectionProtocol>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_9501_gshared (DefaultComparer_t1_1307 * __this, uint8_t ___x, uint8_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m1_9501(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1_1307 *, uint8_t, uint8_t, const MethodInfo*))DefaultComparer_Equals_m1_9501_gshared)(__this, ___x, ___y, method)
