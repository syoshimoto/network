﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.Int32>
struct GenericComparer_1_t1_1149;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.Int32>::.ctor()
extern "C" void GenericComparer_1__ctor_m1_7330_gshared (GenericComparer_1_t1_1149 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m1_7330(__this, method) (( void (*) (GenericComparer_1_t1_1149 *, const MethodInfo*))GenericComparer_1__ctor_m1_7330_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int32>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m1_7331_gshared (GenericComparer_1_t1_1149 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m1_7331(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t1_1149 *, int32_t, int32_t, const MethodInfo*))GenericComparer_1_Compare_m1_7331_gshared)(__this, ___x, ___y, method)
