﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
struct GenericEqualityComparer_1_t1_876;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m1_5539_gshared (GenericEqualityComparer_1_t1_876 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m1_5539(__this, method) (( void (*) (GenericEqualityComparer_1_t1_876 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m1_5539_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m1_6991_gshared (GenericEqualityComparer_1_t1_876 * __this, Guid_t1_730  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m1_6991(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1_876 *, Guid_t1_730 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m1_6991_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m1_6992_gshared (GenericEqualityComparer_1_t1_876 * __this, Guid_t1_730  ___x, Guid_t1_730  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m1_6992(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1_876 *, Guid_t1_730 , Guid_t1_730 , const MethodInfo*))GenericEqualityComparer_1_Equals_m1_6992_gshared)(__this, ___x, ___y, method)
