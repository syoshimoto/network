﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.Byte>
struct GenericEqualityComparer_1_t1_1185;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m1_7719_gshared (GenericEqualityComparer_1_t1_1185 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m1_7719(__this, method) (( void (*) (GenericEqualityComparer_1_t1_1185 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m1_7719_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m1_7720_gshared (GenericEqualityComparer_1_t1_1185 * __this, uint8_t ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m1_7720(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1_1185 *, uint8_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m1_7720_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m1_7721_gshared (GenericEqualityComparer_1_t1_1185 * __this, uint8_t ___x, uint8_t ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m1_7721(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1_1185 *, uint8_t, uint8_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m1_7721_gshared)(__this, ___x, ___y, method)
