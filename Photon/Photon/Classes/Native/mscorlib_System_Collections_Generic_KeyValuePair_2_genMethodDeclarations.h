﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m1_7638_gshared (KeyValuePair_2_t1_902 * __this, uint8_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1_7638(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_902 *, uint8_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m1_7638_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>::get_Key()
extern "C" uint8_t KeyValuePair_2_get_Key_m1_5576_gshared (KeyValuePair_2_t1_902 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1_5576(__this, method) (( uint8_t (*) (KeyValuePair_2_t1_902 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_5576_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m1_7639_gshared (KeyValuePair_2_t1_902 * __this, uint8_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1_7639(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_902 *, uint8_t, const MethodInfo*))KeyValuePair_2_set_Key_m1_7639_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m1_5577_gshared (KeyValuePair_2_t1_902 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1_5577(__this, method) (( Object_t * (*) (KeyValuePair_2_t1_902 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_5577_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m1_7640_gshared (KeyValuePair_2_t1_902 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1_7640(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_902 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m1_7640_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m1_7641_gshared (KeyValuePair_2_t1_902 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1_7641(__this, method) (( String_t* (*) (KeyValuePair_2_t1_902 *, const MethodInfo*))KeyValuePair_2_ToString_m1_7641_gshared)(__this, method)
