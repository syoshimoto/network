﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_genMethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Byte,ExitGames.Client.Photon.CustomType>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_8014(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1199 *, uint8_t, CustomType_t5_50 *, const MethodInfo*))KeyValuePair_2__ctor_m1_7638_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Byte,ExitGames.Client.Photon.CustomType>::get_Key()
#define KeyValuePair_2_get_Key_m1_8015(__this, method) (( uint8_t (*) (KeyValuePair_2_t1_1199 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_5576_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Byte,ExitGames.Client.Photon.CustomType>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_8016(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1199 *, uint8_t, const MethodInfo*))KeyValuePair_2_set_Key_m1_7639_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Byte,ExitGames.Client.Photon.CustomType>::get_Value()
#define KeyValuePair_2_get_Value_m1_8017(__this, method) (( CustomType_t5_50 * (*) (KeyValuePair_2_t1_1199 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_5577_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Byte,ExitGames.Client.Photon.CustomType>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_8018(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1199 *, CustomType_t5_50 *, const MethodInfo*))KeyValuePair_2_set_Value_m1_7640_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Byte,ExitGames.Client.Photon.CustomType>::ToString()
#define KeyValuePair_2_ToString_m1_8019(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1199 *, const MethodInfo*))KeyValuePair_2_ToString_m1_7641_gshared)(__this, method)
