﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_8793(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1253 *, String_t*, GUIStyle_t6_166 *, const MethodInfo*))KeyValuePair_2__ctor_m1_6351_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Key()
#define KeyValuePair_2_get_Key_m1_8794(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1253 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_6352_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_8795(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1253 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1_6353_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Value()
#define KeyValuePair_2_get_Value_m1_8796(__this, method) (( GUIStyle_t6_166 * (*) (KeyValuePair_2_t1_1253 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_6354_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_8797(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1253 *, GUIStyle_t6_166 *, const MethodInfo*))KeyValuePair_2_set_Value_m1_6355_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::ToString()
#define KeyValuePair_2_ToString_m1_8798(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1253 *, const MethodInfo*))KeyValuePair_2_ToString_m1_6356_gshared)(__this, method)
