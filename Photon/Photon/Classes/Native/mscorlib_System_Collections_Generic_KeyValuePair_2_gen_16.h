﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ExitGames.Client.Photon.Chat.ChatChannel
struct ChatChannel_t7_1;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>
struct  KeyValuePair_2_t1_1289 
{
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	ChatChannel_t7_1 * ___value_1;
};
