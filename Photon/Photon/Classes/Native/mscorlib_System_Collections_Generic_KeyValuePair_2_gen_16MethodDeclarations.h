﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_9310(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1289 *, String_t*, ChatChannel_t7_1 *, const MethodInfo*))KeyValuePair_2__ctor_m1_6351_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::get_Key()
#define KeyValuePair_2_get_Key_m1_9311(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1289 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_6352_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_9312(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1289 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1_6353_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::get_Value()
#define KeyValuePair_2_get_Value_m1_9313(__this, method) (( ChatChannel_t7_1 * (*) (KeyValuePair_2_t1_1289 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_6354_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_9314(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1289 *, ChatChannel_t7_1 *, const MethodInfo*))KeyValuePair_2_set_Value_m1_6355_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>::ToString()
#define KeyValuePair_2_ToString_m1_9315(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1289 *, const MethodInfo*))KeyValuePair_2_ToString_m1_6356_gshared)(__this, method)
