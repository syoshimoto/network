﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_17.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionProtocol.h"

// System.Void System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m1_9407_gshared (KeyValuePair_2_t1_1294 * __this, uint8_t ___key, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1_9407(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1294 *, uint8_t, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m1_9407_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_Key()
extern "C" uint8_t KeyValuePair_2_get_Key_m1_9408_gshared (KeyValuePair_2_t1_1294 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1_9408(__this, method) (( uint8_t (*) (KeyValuePair_2_t1_1294 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_9408_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m1_9409_gshared (KeyValuePair_2_t1_1294 * __this, uint8_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1_9409(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1294 *, uint8_t, const MethodInfo*))KeyValuePair_2_set_Key_m1_9409_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m1_9410_gshared (KeyValuePair_2_t1_1294 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1_9410(__this, method) (( int32_t (*) (KeyValuePair_2_t1_1294 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_9410_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m1_9411_gshared (KeyValuePair_2_t1_1294 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1_9411(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1294 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1_9411_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m1_9412_gshared (KeyValuePair_2_t1_1294 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1_9412(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1294 *, const MethodInfo*))KeyValuePair_2_ToString_m1_9412_gshared)(__this, method)
