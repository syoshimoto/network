﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_9865(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1331 *, uint8_t, List_1_t1_955 *, const MethodInfo*))KeyValuePair_2__ctor_m1_9638_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::get_Key()
#define KeyValuePair_2_get_Key_m1_9866(__this, method) (( uint8_t (*) (KeyValuePair_2_t1_1331 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_9639_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_9867(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1331 *, uint8_t, const MethodInfo*))KeyValuePair_2_set_Key_m1_9640_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::get_Value()
#define KeyValuePair_2_get_Value_m1_9868(__this, method) (( List_1_t1_955 * (*) (KeyValuePair_2_t1_1331 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_9641_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_9869(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1331 *, List_1_t1_955 *, const MethodInfo*))KeyValuePair_2_set_Value_m1_9642_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Collections.Generic.List`1<PhotonPlayer>>::ToString()
#define KeyValuePair_2_ToString_m1_9870(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1331 *, const MethodInfo*))KeyValuePair_2_ToString_m1_9643_gshared)(__this, method)
