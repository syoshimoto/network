﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object[]>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_10638(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1370 *, int32_t, ObjectU5BU5D_t1_157*, const MethodInfo*))KeyValuePair_2__ctor_m1_7459_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object[]>::get_Key()
#define KeyValuePair_2_get_Key_m1_10639(__this, method) (( int32_t (*) (KeyValuePair_2_t1_1370 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_7460_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object[]>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_10640(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1370 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1_7461_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object[]>::get_Value()
#define KeyValuePair_2_get_Value_m1_10641(__this, method) (( ObjectU5BU5D_t1_157* (*) (KeyValuePair_2_t1_1370 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_7462_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object[]>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_10642(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1370 *, ObjectU5BU5D_t1_157*, const MethodInfo*))KeyValuePair_2_set_Value_m1_7463_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object[]>::ToString()
#define KeyValuePair_2_ToString_m1_10643(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1370 *, const MethodInfo*))KeyValuePair_2_ToString_m1_7464_gshared)(__this, method)
