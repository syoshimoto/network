﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Component
struct Component_t6_26;
// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Component,System.Reflection.MethodInfo>
struct  KeyValuePair_2_t1_1390 
{
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Component_t6_26 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	MethodInfo_t * ___value_1;
};
