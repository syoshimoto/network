﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_6232(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1032 *, String_t*, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m1_6124_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m1_6233(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1032 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_6125_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_6234(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1032 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1_6126_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m1_6235(__this, method) (( int32_t (*) (KeyValuePair_2_t1_1032 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_6127_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_6236(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1032 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1_6128_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m1_6237(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1032 *, const MethodInfo*))KeyValuePair_2_ToString_m1_6129_gshared)(__this, method)
