﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m1_6351_gshared (KeyValuePair_2_t1_1044 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1_6351(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1044 *, Object_t *, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m1_6351_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m1_6352_gshared (KeyValuePair_2_t1_1044 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1_6352(__this, method) (( Object_t * (*) (KeyValuePair_2_t1_1044 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_6352_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m1_6353_gshared (KeyValuePair_2_t1_1044 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1_6353(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1044 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m1_6353_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m1_6354_gshared (KeyValuePair_2_t1_1044 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1_6354(__this, method) (( Object_t * (*) (KeyValuePair_2_t1_1044 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_6354_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m1_6355_gshared (KeyValuePair_2_t1_1044 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1_6355(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1044 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m1_6355_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m1_6356_gshared (KeyValuePair_2_t1_1044 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1_6356(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1044 *, const MethodInfo*))KeyValuePair_2_ToString_m1_6356_gshared)(__this, method)
