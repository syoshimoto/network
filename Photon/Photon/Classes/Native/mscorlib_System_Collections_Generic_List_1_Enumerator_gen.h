﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<ExitGames.Client.Photon.NCommand>
struct List_1_t1_887;
// ExitGames.Client.Photon.NCommand
struct NCommand_t5_13;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<ExitGames.Client.Photon.NCommand>
struct  Enumerator_t1_900 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1_887 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NCommand_t5_13 * ___current_3;
};
