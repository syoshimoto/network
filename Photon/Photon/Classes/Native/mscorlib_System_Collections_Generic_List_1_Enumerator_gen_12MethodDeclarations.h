﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody2D>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1_8355(__this, ___l, method) (( void (*) (Enumerator_t1_1223 *, List_1_t1_911 *, const MethodInfo*))Enumerator__ctor_m1_5779_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody2D>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_8356(__this, method) (( void (*) (Enumerator_t1_1223 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_5780_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody2D>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_8357(__this, method) (( Object_t * (*) (Enumerator_t1_1223 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_5781_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody2D>::Dispose()
#define Enumerator_Dispose_m1_8358(__this, method) (( void (*) (Enumerator_t1_1223 *, const MethodInfo*))Enumerator_Dispose_m1_5782_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody2D>::VerifyState()
#define Enumerator_VerifyState_m1_8359(__this, method) (( void (*) (Enumerator_t1_1223 *, const MethodInfo*))Enumerator_VerifyState_m1_5783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody2D>::MoveNext()
#define Enumerator_MoveNext_m1_8360(__this, method) (( bool (*) (Enumerator_t1_1223 *, const MethodInfo*))Enumerator_MoveNext_m1_5784_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody2D>::get_Current()
#define Enumerator_get_Current_m1_8361(__this, method) (( Rigidbody2D_t6_113 * (*) (Enumerator_t1_1223 *, const MethodInfo*))Enumerator_get_Current_m1_5785_gshared)(__this, method)
