﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1_914;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_8494_gshared (Enumerator_t1_1233 * __this, List_1_t1_914 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_8494(__this, ___l, method) (( void (*) (Enumerator_t1_1233 *, List_1_t1_914 *, const MethodInfo*))Enumerator__ctor_m1_8494_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_8495_gshared (Enumerator_t1_1233 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_8495(__this, method) (( void (*) (Enumerator_t1_1233 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_8495_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_8496_gshared (Enumerator_t1_1233 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_8496(__this, method) (( Object_t * (*) (Enumerator_t1_1233 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_8496_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C" void Enumerator_Dispose_m1_8497_gshared (Enumerator_t1_1233 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_8497(__this, method) (( void (*) (Enumerator_t1_1233 *, const MethodInfo*))Enumerator_Dispose_m1_8497_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_8498_gshared (Enumerator_t1_1233 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_8498(__this, method) (( void (*) (Enumerator_t1_1233 *, const MethodInfo*))Enumerator_VerifyState_m1_8498_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_8499_gshared (Enumerator_t1_1233 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_8499(__this, method) (( bool (*) (Enumerator_t1_1233 *, const MethodInfo*))Enumerator_MoveNext_m1_8499_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t6_149  Enumerator_get_Current_m1_8500_gshared (Enumerator_t1_1233 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_8500(__this, method) (( UICharInfo_t6_149  (*) (Enumerator_t1_1233 *, const MethodInfo*))Enumerator_get_Current_m1_8500_gshared)(__this, method)
