﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1_915;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_8559_gshared (Enumerator_t1_1238 * __this, List_1_t1_915 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_8559(__this, ___l, method) (( void (*) (Enumerator_t1_1238 *, List_1_t1_915 *, const MethodInfo*))Enumerator__ctor_m1_8559_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_8560_gshared (Enumerator_t1_1238 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_8560(__this, method) (( void (*) (Enumerator_t1_1238 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_8560_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_8561_gshared (Enumerator_t1_1238 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_8561(__this, method) (( Object_t * (*) (Enumerator_t1_1238 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_8561_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C" void Enumerator_Dispose_m1_8562_gshared (Enumerator_t1_1238 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_8562(__this, method) (( void (*) (Enumerator_t1_1238 *, const MethodInfo*))Enumerator_Dispose_m1_8562_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_8563_gshared (Enumerator_t1_1238 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_8563(__this, method) (( void (*) (Enumerator_t1_1238 *, const MethodInfo*))Enumerator_VerifyState_m1_8563_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_8564_gshared (Enumerator_t1_1238 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_8564(__this, method) (( bool (*) (Enumerator_t1_1238 *, const MethodInfo*))Enumerator_MoveNext_m1_8564_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t6_150  Enumerator_get_Current_m1_8565_gshared (Enumerator_t1_1238 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_8565(__this, method) (( UILineInfo_t6_150  (*) (Enumerator_t1_1238 *, const MethodInfo*))Enumerator_get_Current_m1_8565_gshared)(__this, method)
