﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1_9255(__this, ___l, method) (( void (*) (Enumerator_t1_1287 *, List_1_t1_922 *, const MethodInfo*))Enumerator__ctor_m1_5779_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_9256(__this, method) (( void (*) (Enumerator_t1_1287 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_5780_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_9257(__this, method) (( Object_t * (*) (Enumerator_t1_1287 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_5781_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::Dispose()
#define Enumerator_Dispose_m1_9258(__this, method) (( void (*) (Enumerator_t1_1287 *, const MethodInfo*))Enumerator_Dispose_m1_5782_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::VerifyState()
#define Enumerator_VerifyState_m1_9259(__this, method) (( void (*) (Enumerator_t1_1287 *, const MethodInfo*))Enumerator_VerifyState_m1_5783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::MoveNext()
#define Enumerator_MoveNext_m1_9260(__this, method) (( bool (*) (Enumerator_t1_1287 *, const MethodInfo*))Enumerator_MoveNext_m1_5784_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::get_Current()
#define Enumerator_get_Current_m1_9261(__this, method) (( BaseInvokableCall_t6_240 * (*) (Enumerator_t1_1287 *, const MethodInfo*))Enumerator_get_Current_m1_5785_gshared)(__this, method)
