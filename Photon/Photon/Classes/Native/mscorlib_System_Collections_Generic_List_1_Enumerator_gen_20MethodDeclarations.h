﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<TypedLobbyInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1_10115(__this, ___l, method) (( void (*) (Enumerator_t1_1346 *, List_1_t1_934 *, const MethodInfo*))Enumerator__ctor_m1_5779_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TypedLobbyInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_10116(__this, method) (( void (*) (Enumerator_t1_1346 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_5780_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<TypedLobbyInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_10117(__this, method) (( Object_t * (*) (Enumerator_t1_1346 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_5781_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TypedLobbyInfo>::Dispose()
#define Enumerator_Dispose_m1_10118(__this, method) (( void (*) (Enumerator_t1_1346 *, const MethodInfo*))Enumerator_Dispose_m1_5782_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TypedLobbyInfo>::VerifyState()
#define Enumerator_VerifyState_m1_10119(__this, method) (( void (*) (Enumerator_t1_1346 *, const MethodInfo*))Enumerator_VerifyState_m1_5783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<TypedLobbyInfo>::MoveNext()
#define Enumerator_MoveNext_m1_10120(__this, method) (( bool (*) (Enumerator_t1_1346 *, const MethodInfo*))Enumerator_MoveNext_m1_5784_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<TypedLobbyInfo>::get_Current()
#define Enumerator_get_Current_m1_10121(__this, method) (( TypedLobbyInfo_t8_95 * (*) (Enumerator_t1_1346 *, const MethodInfo*))Enumerator_get_Current_m1_5785_gshared)(__this, method)
