﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Byte>
struct List_1_t1_966;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_22.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_10793_gshared (Enumerator_t1_1379 * __this, List_1_t1_966 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_10793(__this, ___l, method) (( void (*) (Enumerator_t1_1379 *, List_1_t1_966 *, const MethodInfo*))Enumerator__ctor_m1_10793_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_10794_gshared (Enumerator_t1_1379 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_10794(__this, method) (( void (*) (Enumerator_t1_1379 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_10794_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_10795_gshared (Enumerator_t1_1379 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_10795(__this, method) (( Object_t * (*) (Enumerator_t1_1379 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_10795_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m1_10796_gshared (Enumerator_t1_1379 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_10796(__this, method) (( void (*) (Enumerator_t1_1379 *, const MethodInfo*))Enumerator_Dispose_m1_10796_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_10797_gshared (Enumerator_t1_1379 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_10797(__this, method) (( void (*) (Enumerator_t1_1379 *, const MethodInfo*))Enumerator_VerifyState_m1_10797_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_10798_gshared (Enumerator_t1_1379 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_10798(__this, method) (( bool (*) (Enumerator_t1_1379 *, const MethodInfo*))Enumerator_MoveNext_m1_10798_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Byte>::get_Current()
extern "C" uint8_t Enumerator_get_Current_m1_10799_gshared (Enumerator_t1_1379 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_10799(__this, method) (( uint8_t (*) (Enumerator_t1_1379 *, const MethodInfo*))Enumerator_get_Current_m1_10799_gshared)(__this, method)
