﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Single>
struct List_1_t1_971;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_11141_gshared (Enumerator_t1_1396 * __this, List_1_t1_971 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_11141(__this, ___l, method) (( void (*) (Enumerator_t1_1396 *, List_1_t1_971 *, const MethodInfo*))Enumerator__ctor_m1_11141_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_11142_gshared (Enumerator_t1_1396 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_11142(__this, method) (( void (*) (Enumerator_t1_1396 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_11142_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_11143_gshared (Enumerator_t1_1396 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_11143(__this, method) (( Object_t * (*) (Enumerator_t1_1396 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_11143_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::Dispose()
extern "C" void Enumerator_Dispose_m1_11144_gshared (Enumerator_t1_1396 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_11144(__this, method) (( void (*) (Enumerator_t1_1396 *, const MethodInfo*))Enumerator_Dispose_m1_11144_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_11145_gshared (Enumerator_t1_1396 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_11145(__this, method) (( void (*) (Enumerator_t1_1396 *, const MethodInfo*))Enumerator_VerifyState_m1_11145_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Single>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_11146_gshared (Enumerator_t1_1396 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_11146(__this, method) (( bool (*) (Enumerator_t1_1396 *, const MethodInfo*))Enumerator_MoveNext_m1_11146_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Single>::get_Current()
extern "C" float Enumerator_get_Current_m1_11147_gshared (Enumerator_t1_1396 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_11147(__this, method) (( float (*) (Enumerator_t1_1396 *, const MethodInfo*))Enumerator_get_Current_m1_11147_gshared)(__this, method)
