﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t1_931;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_5779_gshared (Enumerator_t1_975 * __this, List_1_t1_931 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_5779(__this, ___l, method) (( void (*) (Enumerator_t1_975 *, List_1_t1_931 *, const MethodInfo*))Enumerator__ctor_m1_5779_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_5780_gshared (Enumerator_t1_975 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_5780(__this, method) (( void (*) (Enumerator_t1_975 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_5780_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_5781_gshared (Enumerator_t1_975 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_5781(__this, method) (( Object_t * (*) (Enumerator_t1_975 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_5781_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_5782_gshared (Enumerator_t1_975 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_5782(__this, method) (( void (*) (Enumerator_t1_975 *, const MethodInfo*))Enumerator_Dispose_m1_5782_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_5783_gshared (Enumerator_t1_975 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_5783(__this, method) (( void (*) (Enumerator_t1_975 *, const MethodInfo*))Enumerator_VerifyState_m1_5783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_5784_gshared (Enumerator_t1_975 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_5784(__this, method) (( bool (*) (Enumerator_t1_975 *, const MethodInfo*))Enumerator_MoveNext_m1_5784_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m1_5785_gshared (Enumerator_t1_975 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_5785(__this, method) (( Object_t * (*) (Enumerator_t1_975 *, const MethodInfo*))Enumerator_get_Current_m1_5785_gshared)(__this, method)
