﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
#define List_1__ctor_m1_5519(__this, method) (( void (*) (List_1_t1_829 *, const MethodInfo*))List_1__ctor_m1_5617_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1_5623(__this, ___collection, method) (( void (*) (List_1_t1_829 *, Object_t*, const MethodInfo*))List_1__ctor_m1_5673_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Int32)
#define List_1__ctor_m1_5705(__this, ___capacity, method) (( void (*) (List_1_t1_829 *, int32_t, const MethodInfo*))List_1__ctor_m1_5706_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.String>::.cctor()
#define List_1__cctor_m1_5707(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_5708_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.String>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_5709(__this, method) (( Object_t* (*) (List_1_t1_829 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_5710_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1_5711(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_829 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_5712_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.String>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_5713(__this, method) (( Object_t * (*) (List_1_t1_829 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_5714_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1_5715(__this, ___item, method) (( int32_t (*) (List_1_t1_829 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_5716_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1_5717(__this, ___item, method) (( bool (*) (List_1_t1_829 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_5718_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1_5719(__this, ___item, method) (( int32_t (*) (List_1_t1_829 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_5720_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1_5721(__this, ___index, ___item, method) (( void (*) (List_1_t1_829 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_5722_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1_5723(__this, ___item, method) (( void (*) (List_1_t1_829 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_5724_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_5725(__this, method) (( bool (*) (List_1_t1_829 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_5726_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_5727(__this, method) (( Object_t * (*) (List_1_t1_829 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_5728_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.String>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1_5729(__this, ___index, method) (( Object_t * (*) (List_1_t1_829 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_5730_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1_5731(__this, ___index, ___value, method) (( void (*) (List_1_t1_829 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_5732_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.String>::Add(T)
#define List_1_Add_m1_5733(__this, ___item, method) (( void (*) (List_1_t1_829 *, String_t*, const MethodInfo*))List_1_Add_m1_5734_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1_5735(__this, ___newCount, method) (( void (*) (List_1_t1_829 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_5736_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.String>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1_5737(__this, ___collection, method) (( void (*) (List_1_t1_829 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_5738_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.String>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1_5739(__this, ___enumerable, method) (( void (*) (List_1_t1_829 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_5740_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.String>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1_5618(__this, ___collection, method) (( void (*) (List_1_t1_829 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_5619_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.String>::Clear()
#define List_1_Clear_m1_5741(__this, method) (( void (*) (List_1_t1_829 *, const MethodInfo*))List_1_Clear_m1_5742_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::Contains(T)
#define List_1_Contains_m1_5743(__this, ___item, method) (( bool (*) (List_1_t1_829 *, String_t*, const MethodInfo*))List_1_Contains_m1_5744_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1_5745(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_829 *, StringU5BU5D_t1_202*, int32_t, const MethodInfo*))List_1_CopyTo_m1_5746_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1_5747(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_980 *, const MethodInfo*))List_1_CheckMatch_m1_5748_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m1_5749(__this, ___match, method) (( int32_t (*) (List_1_t1_829 *, Predicate_1_t1_980 *, const MethodInfo*))List_1_FindIndex_m1_5750_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1_5751(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_829 *, int32_t, int32_t, Predicate_1_t1_980 *, const MethodInfo*))List_1_GetIndex_m1_5752_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.String>::GetEnumerator()
#define List_1_GetEnumerator_m1_5753(__this, method) (( Enumerator_t1_981  (*) (List_1_t1_829 *, const MethodInfo*))List_1_GetEnumerator_m1_5754_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(T)
#define List_1_IndexOf_m1_5755(__this, ___item, method) (( int32_t (*) (List_1_t1_829 *, String_t*, const MethodInfo*))List_1_IndexOf_m1_5756_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1_5757(__this, ___start, ___delta, method) (( void (*) (List_1_t1_829 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_5758_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1_5759(__this, ___index, method) (( void (*) (List_1_t1_829 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_5760_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::Insert(System.Int32,T)
#define List_1_Insert_m1_5761(__this, ___index, ___item, method) (( void (*) (List_1_t1_829 *, int32_t, String_t*, const MethodInfo*))List_1_Insert_m1_5762_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1_5763(__this, ___collection, method) (( void (*) (List_1_t1_829 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_5764_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::Remove(T)
#define List_1_Remove_m1_5765(__this, ___item, method) (( bool (*) (List_1_t1_829 *, String_t*, const MethodInfo*))List_1_Remove_m1_5766_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1_5767(__this, ___index, method) (( void (*) (List_1_t1_829 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_5768_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.String>::ToArray()
#define List_1_ToArray_m1_5520(__this, method) (( StringU5BU5D_t1_202* (*) (List_1_t1_829 *, const MethodInfo*))List_1_ToArray_m1_5672_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Capacity()
#define List_1_get_Capacity_m1_5769(__this, method) (( int32_t (*) (List_1_t1_829 *, const MethodInfo*))List_1_get_Capacity_m1_5770_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1_5771(__this, ___value, method) (( void (*) (List_1_t1_829 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_5772_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
#define List_1_get_Count_m1_5773(__this, method) (( int32_t (*) (List_1_t1_829 *, const MethodInfo*))List_1_get_Count_m1_5774_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
#define List_1_get_Item_m1_5775(__this, ___index, method) (( String_t* (*) (List_1_t1_829 *, int32_t, const MethodInfo*))List_1_get_Item_m1_5776_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::set_Item(System.Int32,T)
#define List_1_set_Item_m1_5777(__this, ___index, ___value, method) (( void (*) (List_1_t1_829 *, int32_t, String_t*, const MethodInfo*))List_1_set_Item_m1_5778_gshared)(__this, ___index, ___value, method)
