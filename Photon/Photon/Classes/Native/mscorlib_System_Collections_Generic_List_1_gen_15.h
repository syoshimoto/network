﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhotonAnimatorView/SynchronizedParameter[]
struct SynchronizedParameterU5BU5D_t8_183;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<PhotonAnimatorView/SynchronizedParameter>
struct  List_1_t1_949  : public Object_t
{
	// T[] System.Collections.Generic.List`1::_items
	SynchronizedParameterU5BU5D_t8_183* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
};
struct List_1_t1_949_StaticFields{
	// T[] System.Collections.Generic.List`1::EmptyArray
	SynchronizedParameterU5BU5D_t8_183* ___EmptyArray_4;
};
