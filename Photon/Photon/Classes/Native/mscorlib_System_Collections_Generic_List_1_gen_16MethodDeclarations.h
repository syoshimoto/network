﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<PhotonPlayer>::.ctor()
#define List_1__ctor_m1_5649(__this, method) (( void (*) (List_1_t1_955 *, const MethodInfo*))List_1__ctor_m1_5617_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1_9779(__this, ___collection, method) (( void (*) (List_1_t1_955 *, Object_t*, const MethodInfo*))List_1__ctor_m1_5673_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::.ctor(System.Int32)
#define List_1__ctor_m1_9780(__this, ___capacity, method) (( void (*) (List_1_t1_955 *, int32_t, const MethodInfo*))List_1__ctor_m1_5706_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::.cctor()
#define List_1__cctor_m1_9781(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_5708_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<PhotonPlayer>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_9782(__this, method) (( Object_t* (*) (List_1_t1_955 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_5710_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1_9783(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_955 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_5712_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<PhotonPlayer>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_9784(__this, method) (( Object_t * (*) (List_1_t1_955 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_5714_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PhotonPlayer>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1_9785(__this, ___item, method) (( int32_t (*) (List_1_t1_955 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_5716_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<PhotonPlayer>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1_9786(__this, ___item, method) (( bool (*) (List_1_t1_955 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_5718_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<PhotonPlayer>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1_9787(__this, ___item, method) (( int32_t (*) (List_1_t1_955 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_5720_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1_9788(__this, ___index, ___item, method) (( void (*) (List_1_t1_955 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_5722_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1_9789(__this, ___item, method) (( void (*) (List_1_t1_955 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_5724_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<PhotonPlayer>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_9790(__this, method) (( bool (*) (List_1_t1_955 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_5726_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PhotonPlayer>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_9791(__this, method) (( Object_t * (*) (List_1_t1_955 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_5728_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PhotonPlayer>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1_9792(__this, ___index, method) (( Object_t * (*) (List_1_t1_955 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_5730_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1_9793(__this, ___index, ___value, method) (( void (*) (List_1_t1_955 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_5732_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::Add(T)
#define List_1_Add_m1_9794(__this, ___item, method) (( void (*) (List_1_t1_955 *, PhotonPlayer_t8_102 *, const MethodInfo*))List_1_Add_m1_5734_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1_9795(__this, ___newCount, method) (( void (*) (List_1_t1_955 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_5736_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1_9796(__this, ___collection, method) (( void (*) (List_1_t1_955 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_5738_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1_9797(__this, ___enumerable, method) (( void (*) (List_1_t1_955 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_5740_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1_9798(__this, ___collection, method) (( void (*) (List_1_t1_955 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_5619_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::Clear()
#define List_1_Clear_m1_9799(__this, method) (( void (*) (List_1_t1_955 *, const MethodInfo*))List_1_Clear_m1_5742_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PhotonPlayer>::Contains(T)
#define List_1_Contains_m1_9800(__this, ___item, method) (( bool (*) (List_1_t1_955 *, PhotonPlayer_t8_102 *, const MethodInfo*))List_1_Contains_m1_5744_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1_9801(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_955 *, PhotonPlayerU5BU5D_t8_101*, int32_t, const MethodInfo*))List_1_CopyTo_m1_5746_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1_9802(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_1313 *, const MethodInfo*))List_1_CheckMatch_m1_5748_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<PhotonPlayer>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m1_9803(__this, ___match, method) (( int32_t (*) (List_1_t1_955 *, Predicate_1_t1_1313 *, const MethodInfo*))List_1_FindIndex_m1_5750_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<PhotonPlayer>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1_9804(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_955 *, int32_t, int32_t, Predicate_1_t1_1313 *, const MethodInfo*))List_1_GetIndex_m1_5752_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<PhotonPlayer>::GetEnumerator()
#define List_1_GetEnumerator_m1_5630(__this, method) (( Enumerator_t1_956  (*) (List_1_t1_955 *, const MethodInfo*))List_1_GetEnumerator_m1_5754_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PhotonPlayer>::IndexOf(T)
#define List_1_IndexOf_m1_9805(__this, ___item, method) (( int32_t (*) (List_1_t1_955 *, PhotonPlayer_t8_102 *, const MethodInfo*))List_1_IndexOf_m1_5756_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1_9806(__this, ___start, ___delta, method) (( void (*) (List_1_t1_955 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_5758_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1_9807(__this, ___index, method) (( void (*) (List_1_t1_955 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_5760_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::Insert(System.Int32,T)
#define List_1_Insert_m1_9808(__this, ___index, ___item, method) (( void (*) (List_1_t1_955 *, int32_t, PhotonPlayer_t8_102 *, const MethodInfo*))List_1_Insert_m1_5762_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1_9809(__this, ___collection, method) (( void (*) (List_1_t1_955 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_5764_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<PhotonPlayer>::Remove(T)
#define List_1_Remove_m1_9810(__this, ___item, method) (( bool (*) (List_1_t1_955 *, PhotonPlayer_t8_102 *, const MethodInfo*))List_1_Remove_m1_5766_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1_9811(__this, ___index, method) (( void (*) (List_1_t1_955 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_5768_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<PhotonPlayer>::ToArray()
#define List_1_ToArray_m1_5650(__this, method) (( PhotonPlayerU5BU5D_t8_101* (*) (List_1_t1_955 *, const MethodInfo*))List_1_ToArray_m1_5672_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PhotonPlayer>::get_Capacity()
#define List_1_get_Capacity_m1_9812(__this, method) (( int32_t (*) (List_1_t1_955 *, const MethodInfo*))List_1_get_Capacity_m1_5770_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1_9813(__this, ___value, method) (( void (*) (List_1_t1_955 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_5772_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<PhotonPlayer>::get_Count()
#define List_1_get_Count_m1_9814(__this, method) (( int32_t (*) (List_1_t1_955 *, const MethodInfo*))List_1_get_Count_m1_5774_gshared)(__this, method)
// T System.Collections.Generic.List`1<PhotonPlayer>::get_Item(System.Int32)
#define List_1_get_Item_m1_9815(__this, ___index, method) (( PhotonPlayer_t8_102 * (*) (List_1_t1_955 *, int32_t, const MethodInfo*))List_1_get_Item_m1_5776_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<PhotonPlayer>::set_Item(System.Int32,T)
#define List_1_set_Item_m1_9816(__this, ___index, ___value, method) (( void (*) (List_1_t1_955 *, int32_t, PhotonPlayer_t8_102 *, const MethodInfo*))List_1_set_Item_m1_5778_gshared)(__this, ___index, ___value, method)
