﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1_945;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1_1485;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t1_1407;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t1_1486;
// System.Int32[]
struct Int32U5BU5D_t1_160;
// System.Predicate`1<System.Int32>
struct Predicate_1_t1_1375;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21.h"

// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C" void List_1__ctor_m1_5642_gshared (List_1_t1_945 * __this, const MethodInfo* method);
#define List_1__ctor_m1_5642(__this, method) (( void (*) (List_1_t1_945 *, const MethodInfo*))List_1__ctor_m1_5642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_10684_gshared (List_1_t1_945 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_10684(__this, ___collection, method) (( void (*) (List_1_t1_945 *, Object_t*, const MethodInfo*))List_1__ctor_m1_10684_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_10685_gshared (List_1_t1_945 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_10685(__this, ___capacity, method) (( void (*) (List_1_t1_945 *, int32_t, const MethodInfo*))List_1__ctor_m1_10685_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.cctor()
extern "C" void List_1__cctor_m1_10686_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_10686(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_10686_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_10687_gshared (List_1_t1_945 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_10687(__this, method) (( Object_t* (*) (List_1_t1_945 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_10687_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_10688_gshared (List_1_t1_945 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_10688(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_945 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_10688_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_10689_gshared (List_1_t1_945 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_10689(__this, method) (( Object_t * (*) (List_1_t1_945 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_10689_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_10690_gshared (List_1_t1_945 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_10690(__this, ___item, method) (( int32_t (*) (List_1_t1_945 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_10690_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_10691_gshared (List_1_t1_945 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_10691(__this, ___item, method) (( bool (*) (List_1_t1_945 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_10691_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_10692_gshared (List_1_t1_945 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_10692(__this, ___item, method) (( int32_t (*) (List_1_t1_945 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_10692_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_10693_gshared (List_1_t1_945 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_10693(__this, ___index, ___item, method) (( void (*) (List_1_t1_945 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_10693_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_10694_gshared (List_1_t1_945 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_10694(__this, ___item, method) (( void (*) (List_1_t1_945 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_10694_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_10695_gshared (List_1_t1_945 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_10695(__this, method) (( bool (*) (List_1_t1_945 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_10695_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_10696_gshared (List_1_t1_945 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_10696(__this, method) (( Object_t * (*) (List_1_t1_945 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_10696_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_10697_gshared (List_1_t1_945 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_10697(__this, ___index, method) (( Object_t * (*) (List_1_t1_945 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_10697_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_10698_gshared (List_1_t1_945 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_10698(__this, ___index, ___value, method) (( void (*) (List_1_t1_945 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_10698_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
extern "C" void List_1_Add_m1_10699_gshared (List_1_t1_945 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m1_10699(__this, ___item, method) (( void (*) (List_1_t1_945 *, int32_t, const MethodInfo*))List_1_Add_m1_10699_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_10700_gshared (List_1_t1_945 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_10700(__this, ___newCount, method) (( void (*) (List_1_t1_945 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_10700_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_10701_gshared (List_1_t1_945 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_10701(__this, ___collection, method) (( void (*) (List_1_t1_945 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_10701_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_10702_gshared (List_1_t1_945 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_10702(__this, ___enumerable, method) (( void (*) (List_1_t1_945 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_10702_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_10703_gshared (List_1_t1_945 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_10703(__this, ___collection, method) (( void (*) (List_1_t1_945 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_10703_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C" void List_1_Clear_m1_10704_gshared (List_1_t1_945 * __this, const MethodInfo* method);
#define List_1_Clear_m1_10704(__this, method) (( void (*) (List_1_t1_945 *, const MethodInfo*))List_1_Clear_m1_10704_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T)
extern "C" bool List_1_Contains_m1_10705_gshared (List_1_t1_945 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m1_10705(__this, ___item, method) (( bool (*) (List_1_t1_945 *, int32_t, const MethodInfo*))List_1_Contains_m1_10705_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_10706_gshared (List_1_t1_945 * __this, Int32U5BU5D_t1_160* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_10706(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_945 *, Int32U5BU5D_t1_160*, int32_t, const MethodInfo*))List_1_CopyTo_m1_10706_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_10707_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_1375 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_10707(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_1375 *, const MethodInfo*))List_1_CheckMatch_m1_10707_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_10708_gshared (List_1_t1_945 * __this, Predicate_1_t1_1375 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_10708(__this, ___match, method) (( int32_t (*) (List_1_t1_945 *, Predicate_1_t1_1375 *, const MethodInfo*))List_1_FindIndex_m1_10708_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_10709_gshared (List_1_t1_945 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_1375 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_10709(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_945 *, int32_t, int32_t, Predicate_1_t1_1375 *, const MethodInfo*))List_1_GetIndex_m1_10709_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t1_1374  List_1_GetEnumerator_m1_10710_gshared (List_1_t1_945 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_10710(__this, method) (( Enumerator_t1_1374  (*) (List_1_t1_945 *, const MethodInfo*))List_1_GetEnumerator_m1_10710_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_10711_gshared (List_1_t1_945 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_10711(__this, ___item, method) (( int32_t (*) (List_1_t1_945 *, int32_t, const MethodInfo*))List_1_IndexOf_m1_10711_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_10712_gshared (List_1_t1_945 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_10712(__this, ___start, ___delta, method) (( void (*) (List_1_t1_945 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_10712_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_10713_gshared (List_1_t1_945 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_10713(__this, ___index, method) (( void (*) (List_1_t1_945 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_10713_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_10714_gshared (List_1_t1_945 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m1_10714(__this, ___index, ___item, method) (( void (*) (List_1_t1_945 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m1_10714_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_10715_gshared (List_1_t1_945 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_10715(__this, ___collection, method) (( void (*) (List_1_t1_945 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_10715_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T)
extern "C" bool List_1_Remove_m1_10716_gshared (List_1_t1_945 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m1_10716(__this, ___item, method) (( bool (*) (List_1_t1_945 *, int32_t, const MethodInfo*))List_1_Remove_m1_10716_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_10717_gshared (List_1_t1_945 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_10717(__this, ___index, method) (( void (*) (List_1_t1_945 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_10717_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C" Int32U5BU5D_t1_160* List_1_ToArray_m1_5659_gshared (List_1_t1_945 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_5659(__this, method) (( Int32U5BU5D_t1_160* (*) (List_1_t1_945 *, const MethodInfo*))List_1_ToArray_m1_5659_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_10718_gshared (List_1_t1_945 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_10718(__this, method) (( int32_t (*) (List_1_t1_945 *, const MethodInfo*))List_1_get_Capacity_m1_10718_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_10719_gshared (List_1_t1_945 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_10719(__this, ___value, method) (( void (*) (List_1_t1_945 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_10719_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C" int32_t List_1_get_Count_m1_10720_gshared (List_1_t1_945 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_10720(__this, method) (( int32_t (*) (List_1_t1_945 *, const MethodInfo*))List_1_get_Count_m1_10720_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m1_10721_gshared (List_1_t1_945 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_10721(__this, ___index, method) (( int32_t (*) (List_1_t1_945 *, int32_t, const MethodInfo*))List_1_get_Item_m1_10721_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_10722_gshared (List_1_t1_945 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m1_10722(__this, ___index, ___value, method) (( void (*) (List_1_t1_945 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m1_10722_gshared)(__this, ___index, ___value, method)
