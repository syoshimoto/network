﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Byte>
struct List_1_t1_966;
// System.Collections.Generic.IEnumerable`1<System.Byte>
struct IEnumerable_1_t1_1487;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t1_1428;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Byte>
struct ICollection_1_t1_1488;
// System.Byte[]
struct ByteU5BU5D_t1_71;
// System.Predicate`1<System.Byte>
struct Predicate_1_t1_1380;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_22.h"

// System.Void System.Collections.Generic.List`1<System.Byte>::.ctor()
extern "C" void List_1__ctor_m1_5664_gshared (List_1_t1_966 * __this, const MethodInfo* method);
#define List_1__ctor_m1_5664(__this, method) (( void (*) (List_1_t1_966 *, const MethodInfo*))List_1__ctor_m1_5664_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_10754_gshared (List_1_t1_966 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_10754(__this, ___collection, method) (( void (*) (List_1_t1_966 *, Object_t*, const MethodInfo*))List_1__ctor_m1_10754_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_10755_gshared (List_1_t1_966 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_10755(__this, ___capacity, method) (( void (*) (List_1_t1_966 *, int32_t, const MethodInfo*))List_1__ctor_m1_10755_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::.cctor()
extern "C" void List_1__cctor_m1_10756_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_10756(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_10756_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Byte>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_10757_gshared (List_1_t1_966 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_10757(__this, method) (( Object_t* (*) (List_1_t1_966 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_10757_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_10758_gshared (List_1_t1_966 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_10758(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_966 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_10758_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_10759_gshared (List_1_t1_966 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_10759(__this, method) (( Object_t * (*) (List_1_t1_966 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_10759_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_10760_gshared (List_1_t1_966 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_10760(__this, ___item, method) (( int32_t (*) (List_1_t1_966 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_10760_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_10761_gshared (List_1_t1_966 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_10761(__this, ___item, method) (( bool (*) (List_1_t1_966 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_10761_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_10762_gshared (List_1_t1_966 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_10762(__this, ___item, method) (( int32_t (*) (List_1_t1_966 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_10762_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_10763_gshared (List_1_t1_966 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_10763(__this, ___index, ___item, method) (( void (*) (List_1_t1_966 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_10763_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_10764_gshared (List_1_t1_966 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_10764(__this, ___item, method) (( void (*) (List_1_t1_966 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_10764_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_10765_gshared (List_1_t1_966 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_10765(__this, method) (( bool (*) (List_1_t1_966 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_10765_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Byte>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_10766_gshared (List_1_t1_966 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_10766(__this, method) (( Object_t * (*) (List_1_t1_966 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_10766_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_10767_gshared (List_1_t1_966 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_10767(__this, ___index, method) (( Object_t * (*) (List_1_t1_966 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_10767_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_10768_gshared (List_1_t1_966 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_10768(__this, ___index, ___value, method) (( void (*) (List_1_t1_966 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_10768_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::Add(T)
extern "C" void List_1_Add_m1_10769_gshared (List_1_t1_966 * __this, uint8_t ___item, const MethodInfo* method);
#define List_1_Add_m1_10769(__this, ___item, method) (( void (*) (List_1_t1_966 *, uint8_t, const MethodInfo*))List_1_Add_m1_10769_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_10770_gshared (List_1_t1_966 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_10770(__this, ___newCount, method) (( void (*) (List_1_t1_966 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_10770_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_10771_gshared (List_1_t1_966 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_10771(__this, ___collection, method) (( void (*) (List_1_t1_966 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_10771_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_10772_gshared (List_1_t1_966 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_10772(__this, ___enumerable, method) (( void (*) (List_1_t1_966 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_10772_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_10773_gshared (List_1_t1_966 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_10773(__this, ___collection, method) (( void (*) (List_1_t1_966 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_10773_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::Clear()
extern "C" void List_1_Clear_m1_10774_gshared (List_1_t1_966 * __this, const MethodInfo* method);
#define List_1_Clear_m1_10774(__this, method) (( void (*) (List_1_t1_966 *, const MethodInfo*))List_1_Clear_m1_10774_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte>::Contains(T)
extern "C" bool List_1_Contains_m1_10775_gshared (List_1_t1_966 * __this, uint8_t ___item, const MethodInfo* method);
#define List_1_Contains_m1_10775(__this, ___item, method) (( bool (*) (List_1_t1_966 *, uint8_t, const MethodInfo*))List_1_Contains_m1_10775_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_10776_gshared (List_1_t1_966 * __this, ByteU5BU5D_t1_71* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_10776(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_966 *, ByteU5BU5D_t1_71*, int32_t, const MethodInfo*))List_1_CopyTo_m1_10776_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_10777_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_1380 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_10777(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_1380 *, const MethodInfo*))List_1_CheckMatch_m1_10777_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_10778_gshared (List_1_t1_966 * __this, Predicate_1_t1_1380 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_10778(__this, ___match, method) (( int32_t (*) (List_1_t1_966 *, Predicate_1_t1_1380 *, const MethodInfo*))List_1_FindIndex_m1_10778_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_10779_gshared (List_1_t1_966 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_1380 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_10779(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_966 *, int32_t, int32_t, Predicate_1_t1_1380 *, const MethodInfo*))List_1_GetIndex_m1_10779_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Byte>::GetEnumerator()
extern "C" Enumerator_t1_1379  List_1_GetEnumerator_m1_10780_gshared (List_1_t1_966 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_10780(__this, method) (( Enumerator_t1_1379  (*) (List_1_t1_966 *, const MethodInfo*))List_1_GetEnumerator_m1_10780_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_10781_gshared (List_1_t1_966 * __this, uint8_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_10781(__this, ___item, method) (( int32_t (*) (List_1_t1_966 *, uint8_t, const MethodInfo*))List_1_IndexOf_m1_10781_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_10782_gshared (List_1_t1_966 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_10782(__this, ___start, ___delta, method) (( void (*) (List_1_t1_966 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_10782_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_10783_gshared (List_1_t1_966 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_10783(__this, ___index, method) (( void (*) (List_1_t1_966 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_10783_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_10784_gshared (List_1_t1_966 * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method);
#define List_1_Insert_m1_10784(__this, ___index, ___item, method) (( void (*) (List_1_t1_966 *, int32_t, uint8_t, const MethodInfo*))List_1_Insert_m1_10784_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_10785_gshared (List_1_t1_966 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_10785(__this, ___collection, method) (( void (*) (List_1_t1_966 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_10785_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte>::Remove(T)
extern "C" bool List_1_Remove_m1_10786_gshared (List_1_t1_966 * __this, uint8_t ___item, const MethodInfo* method);
#define List_1_Remove_m1_10786(__this, ___item, method) (( bool (*) (List_1_t1_966 *, uint8_t, const MethodInfo*))List_1_Remove_m1_10786_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_10787_gshared (List_1_t1_966 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_10787(__this, ___index, method) (( void (*) (List_1_t1_966 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_10787_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.Byte>::ToArray()
extern "C" ByteU5BU5D_t1_71* List_1_ToArray_m1_5665_gshared (List_1_t1_966 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_5665(__this, method) (( ByteU5BU5D_t1_71* (*) (List_1_t1_966 *, const MethodInfo*))List_1_ToArray_m1_5665_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_10788_gshared (List_1_t1_966 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_10788(__this, method) (( int32_t (*) (List_1_t1_966 *, const MethodInfo*))List_1_get_Capacity_m1_10788_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_10789_gshared (List_1_t1_966 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_10789(__this, ___value, method) (( void (*) (List_1_t1_966 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_10789_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte>::get_Count()
extern "C" int32_t List_1_get_Count_m1_10790_gshared (List_1_t1_966 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_10790(__this, method) (( int32_t (*) (List_1_t1_966 *, const MethodInfo*))List_1_get_Count_m1_10790_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Byte>::get_Item(System.Int32)
extern "C" uint8_t List_1_get_Item_m1_10791_gshared (List_1_t1_966 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_10791(__this, ___index, method) (( uint8_t (*) (List_1_t1_966 *, int32_t, const MethodInfo*))List_1_get_Item_m1_10791_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_10792_gshared (List_1_t1_966 * __this, int32_t ___index, uint8_t ___value, const MethodInfo* method);
#define List_1_set_Item_m1_10792(__this, ___index, ___value, method) (( void (*) (List_1_t1_966 *, int32_t, uint8_t, const MethodInfo*))List_1_set_Item_m1_10792_gshared)(__this, ___index, ___value, method)
