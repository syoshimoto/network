﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t1_1073;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerable_1_t1_1413;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t1_1412;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ICollection_1_t1_842;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1_838;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t1_1077;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_8.h"

// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void List_1__ctor_m1_6599_gshared (List_1_t1_1073 * __this, const MethodInfo* method);
#define List_1__ctor_m1_6599(__this, method) (( void (*) (List_1_t1_1073 *, const MethodInfo*))List_1__ctor_m1_6599_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_6600_gshared (List_1_t1_1073 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_6600(__this, ___collection, method) (( void (*) (List_1_t1_1073 *, Object_t*, const MethodInfo*))List_1__ctor_m1_6600_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_6601_gshared (List_1_t1_1073 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_6601(__this, ___capacity, method) (( void (*) (List_1_t1_1073 *, int32_t, const MethodInfo*))List_1__ctor_m1_6601_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C" void List_1__cctor_m1_6602_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_6602(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_6602_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_6603_gshared (List_1_t1_1073 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_6603(__this, method) (( Object_t* (*) (List_1_t1_1073 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_6603_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_6604_gshared (List_1_t1_1073 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_6604(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1073 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_6604_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_6605_gshared (List_1_t1_1073 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_6605(__this, method) (( Object_t * (*) (List_1_t1_1073 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_6605_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_6606_gshared (List_1_t1_1073 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_6606(__this, ___item, method) (( int32_t (*) (List_1_t1_1073 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_6606_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_6607_gshared (List_1_t1_1073 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_6607(__this, ___item, method) (( bool (*) (List_1_t1_1073 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_6607_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_6608_gshared (List_1_t1_1073 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_6608(__this, ___item, method) (( int32_t (*) (List_1_t1_1073 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_6608_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_6609_gshared (List_1_t1_1073 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_6609(__this, ___index, ___item, method) (( void (*) (List_1_t1_1073 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_6609_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_6610_gshared (List_1_t1_1073 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_6610(__this, ___item, method) (( void (*) (List_1_t1_1073 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_6610_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_6611_gshared (List_1_t1_1073 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_6611(__this, method) (( bool (*) (List_1_t1_1073 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_6611_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_6612_gshared (List_1_t1_1073 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_6612(__this, method) (( Object_t * (*) (List_1_t1_1073 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_6612_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_6613_gshared (List_1_t1_1073 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_6613(__this, ___index, method) (( Object_t * (*) (List_1_t1_1073 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_6613_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_6614_gshared (List_1_t1_1073 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_6614(__this, ___index, ___value, method) (( void (*) (List_1_t1_1073 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_6614_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void List_1_Add_m1_6615_gshared (List_1_t1_1073 * __this, CustomAttributeTypedArgument_t1_332  ___item, const MethodInfo* method);
#define List_1_Add_m1_6615(__this, ___item, method) (( void (*) (List_1_t1_1073 *, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))List_1_Add_m1_6615_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_6616_gshared (List_1_t1_1073 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_6616(__this, ___newCount, method) (( void (*) (List_1_t1_1073 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_6616_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_6617_gshared (List_1_t1_1073 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_6617(__this, ___collection, method) (( void (*) (List_1_t1_1073 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_6617_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_6618_gshared (List_1_t1_1073 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_6618(__this, ___enumerable, method) (( void (*) (List_1_t1_1073 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_6618_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_6619_gshared (List_1_t1_1073 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_6619(__this, ___collection, method) (( void (*) (List_1_t1_1073 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_6619_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void List_1_Clear_m1_6620_gshared (List_1_t1_1073 * __this, const MethodInfo* method);
#define List_1_Clear_m1_6620(__this, method) (( void (*) (List_1_t1_1073 *, const MethodInfo*))List_1_Clear_m1_6620_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool List_1_Contains_m1_6621_gshared (List_1_t1_1073 * __this, CustomAttributeTypedArgument_t1_332  ___item, const MethodInfo* method);
#define List_1_Contains_m1_6621(__this, ___item, method) (( bool (*) (List_1_t1_1073 *, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))List_1_Contains_m1_6621_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_6622_gshared (List_1_t1_1073 * __this, CustomAttributeTypedArgumentU5BU5D_t1_838* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_6622(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1073 *, CustomAttributeTypedArgumentU5BU5D_t1_838*, int32_t, const MethodInfo*))List_1_CopyTo_m1_6622_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_6623_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_1077 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_6623(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_1077 *, const MethodInfo*))List_1_CheckMatch_m1_6623_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_6624_gshared (List_1_t1_1073 * __this, Predicate_1_t1_1077 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_6624(__this, ___match, method) (( int32_t (*) (List_1_t1_1073 *, Predicate_1_t1_1077 *, const MethodInfo*))List_1_FindIndex_m1_6624_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_6625_gshared (List_1_t1_1073 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_1077 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_6625(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1073 *, int32_t, int32_t, Predicate_1_t1_1077 *, const MethodInfo*))List_1_GetIndex_m1_6625_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Enumerator_t1_1074  List_1_GetEnumerator_m1_6626_gshared (List_1_t1_1073 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_6626(__this, method) (( Enumerator_t1_1074  (*) (List_1_t1_1073 *, const MethodInfo*))List_1_GetEnumerator_m1_6626_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_6627_gshared (List_1_t1_1073 * __this, CustomAttributeTypedArgument_t1_332  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_6627(__this, ___item, method) (( int32_t (*) (List_1_t1_1073 *, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))List_1_IndexOf_m1_6627_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_6628_gshared (List_1_t1_1073 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_6628(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1073 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_6628_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_6629_gshared (List_1_t1_1073 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_6629(__this, ___index, method) (( void (*) (List_1_t1_1073 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_6629_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_6630_gshared (List_1_t1_1073 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_332  ___item, const MethodInfo* method);
#define List_1_Insert_m1_6630(__this, ___index, ___item, method) (( void (*) (List_1_t1_1073 *, int32_t, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))List_1_Insert_m1_6630_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_6631_gshared (List_1_t1_1073 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_6631(__this, ___collection, method) (( void (*) (List_1_t1_1073 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_6631_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool List_1_Remove_m1_6632_gshared (List_1_t1_1073 * __this, CustomAttributeTypedArgument_t1_332  ___item, const MethodInfo* method);
#define List_1_Remove_m1_6632(__this, ___item, method) (( bool (*) (List_1_t1_1073 *, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))List_1_Remove_m1_6632_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_6633_gshared (List_1_t1_1073 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_6633(__this, ___index, method) (( void (*) (List_1_t1_1073 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_6633_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::ToArray()
extern "C" CustomAttributeTypedArgumentU5BU5D_t1_838* List_1_ToArray_m1_6634_gshared (List_1_t1_1073 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_6634(__this, method) (( CustomAttributeTypedArgumentU5BU5D_t1_838* (*) (List_1_t1_1073 *, const MethodInfo*))List_1_ToArray_m1_6634_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_6635_gshared (List_1_t1_1073 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_6635(__this, method) (( int32_t (*) (List_1_t1_1073 *, const MethodInfo*))List_1_get_Capacity_m1_6635_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_6636_gshared (List_1_t1_1073 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_6636(__this, ___value, method) (( void (*) (List_1_t1_1073 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_6636_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t List_1_get_Count_m1_6637_gshared (List_1_t1_1073 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_6637(__this, method) (( int32_t (*) (List_1_t1_1073 *, const MethodInfo*))List_1_get_Count_m1_6637_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1_332  List_1_get_Item_m1_6638_gshared (List_1_t1_1073 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_6638(__this, ___index, method) (( CustomAttributeTypedArgument_t1_332  (*) (List_1_t1_1073 *, int32_t, const MethodInfo*))List_1_get_Item_m1_6638_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_6639_gshared (List_1_t1_1073 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_332  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_6639(__this, ___index, ___value, method) (( void (*) (List_1_t1_1073 *, int32_t, CustomAttributeTypedArgument_t1_332 , const MethodInfo*))List_1_set_Item_m1_6639_gshared)(__this, ___index, ___value, method)
