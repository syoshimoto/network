﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1_913;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex>
struct IEnumerable_1_t1_1437;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t1_1438;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>
struct ICollection_1_t1_1439;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t6_261;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t1_1231;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
extern "C" void List_1__ctor_m1_8389_gshared (List_1_t1_913 * __this, const MethodInfo* method);
#define List_1__ctor_m1_8389(__this, method) (( void (*) (List_1_t1_913 *, const MethodInfo*))List_1__ctor_m1_8389_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_8390_gshared (List_1_t1_913 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_8390(__this, ___collection, method) (( void (*) (List_1_t1_913 *, Object_t*, const MethodInfo*))List_1__ctor_m1_8390_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_5600_gshared (List_1_t1_913 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_5600(__this, ___capacity, method) (( void (*) (List_1_t1_913 *, int32_t, const MethodInfo*))List_1__ctor_m1_5600_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.cctor()
extern "C" void List_1__cctor_m1_8391_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_8391(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_8391_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_8392_gshared (List_1_t1_913 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_8392(__this, method) (( Object_t* (*) (List_1_t1_913 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_8392_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_8393_gshared (List_1_t1_913 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_8393(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_913 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_8393_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_8394_gshared (List_1_t1_913 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_8394(__this, method) (( Object_t * (*) (List_1_t1_913 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_8394_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_8395_gshared (List_1_t1_913 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_8395(__this, ___item, method) (( int32_t (*) (List_1_t1_913 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_8395_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_8396_gshared (List_1_t1_913 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_8396(__this, ___item, method) (( bool (*) (List_1_t1_913 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_8396_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_8397_gshared (List_1_t1_913 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_8397(__this, ___item, method) (( int32_t (*) (List_1_t1_913 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_8397_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_8398_gshared (List_1_t1_913 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_8398(__this, ___index, ___item, method) (( void (*) (List_1_t1_913 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_8398_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_8399_gshared (List_1_t1_913 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_8399(__this, ___item, method) (( void (*) (List_1_t1_913 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_8399_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_8400_gshared (List_1_t1_913 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_8400(__this, method) (( bool (*) (List_1_t1_913 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_8400_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_8401_gshared (List_1_t1_913 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_8401(__this, method) (( Object_t * (*) (List_1_t1_913 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_8401_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_8402_gshared (List_1_t1_913 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_8402(__this, ___index, method) (( Object_t * (*) (List_1_t1_913 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_8402_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_8403_gshared (List_1_t1_913 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_8403(__this, ___index, ___value, method) (( void (*) (List_1_t1_913 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_8403_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(T)
extern "C" void List_1_Add_m1_8404_gshared (List_1_t1_913 * __this, UIVertex_t6_153  ___item, const MethodInfo* method);
#define List_1_Add_m1_8404(__this, ___item, method) (( void (*) (List_1_t1_913 *, UIVertex_t6_153 , const MethodInfo*))List_1_Add_m1_8404_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_8405_gshared (List_1_t1_913 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_8405(__this, ___newCount, method) (( void (*) (List_1_t1_913 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_8405_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_8406_gshared (List_1_t1_913 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_8406(__this, ___collection, method) (( void (*) (List_1_t1_913 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_8406_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_8407_gshared (List_1_t1_913 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_8407(__this, ___enumerable, method) (( void (*) (List_1_t1_913 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_8407_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_8408_gshared (List_1_t1_913 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_8408(__this, ___collection, method) (( void (*) (List_1_t1_913 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_8408_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
extern "C" void List_1_Clear_m1_8409_gshared (List_1_t1_913 * __this, const MethodInfo* method);
#define List_1_Clear_m1_8409(__this, method) (( void (*) (List_1_t1_913 *, const MethodInfo*))List_1_Clear_m1_8409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool List_1_Contains_m1_8410_gshared (List_1_t1_913 * __this, UIVertex_t6_153  ___item, const MethodInfo* method);
#define List_1_Contains_m1_8410(__this, ___item, method) (( bool (*) (List_1_t1_913 *, UIVertex_t6_153 , const MethodInfo*))List_1_Contains_m1_8410_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_8411_gshared (List_1_t1_913 * __this, UIVertexU5BU5D_t6_261* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_8411(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_913 *, UIVertexU5BU5D_t6_261*, int32_t, const MethodInfo*))List_1_CopyTo_m1_8411_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_8412_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_1231 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_8412(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_1231 *, const MethodInfo*))List_1_CheckMatch_m1_8412_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_8413_gshared (List_1_t1_913 * __this, Predicate_1_t1_1231 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_8413(__this, ___match, method) (( int32_t (*) (List_1_t1_913 *, Predicate_1_t1_1231 *, const MethodInfo*))List_1_FindIndex_m1_8413_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_8414_gshared (List_1_t1_913 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_1231 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_8414(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_913 *, int32_t, int32_t, Predicate_1_t1_1231 *, const MethodInfo*))List_1_GetIndex_m1_8414_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Enumerator_t1_1228  List_1_GetEnumerator_m1_8415_gshared (List_1_t1_913 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_8415(__this, method) (( Enumerator_t1_1228  (*) (List_1_t1_913 *, const MethodInfo*))List_1_GetEnumerator_m1_8415_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_8416_gshared (List_1_t1_913 * __this, UIVertex_t6_153  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_8416(__this, ___item, method) (( int32_t (*) (List_1_t1_913 *, UIVertex_t6_153 , const MethodInfo*))List_1_IndexOf_m1_8416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_8417_gshared (List_1_t1_913 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_8417(__this, ___start, ___delta, method) (( void (*) (List_1_t1_913 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_8417_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_8418_gshared (List_1_t1_913 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_8418(__this, ___index, method) (( void (*) (List_1_t1_913 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_8418_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_8419_gshared (List_1_t1_913 * __this, int32_t ___index, UIVertex_t6_153  ___item, const MethodInfo* method);
#define List_1_Insert_m1_8419(__this, ___index, ___item, method) (( void (*) (List_1_t1_913 *, int32_t, UIVertex_t6_153 , const MethodInfo*))List_1_Insert_m1_8419_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_8420_gshared (List_1_t1_913 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_8420(__this, ___collection, method) (( void (*) (List_1_t1_913 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_8420_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool List_1_Remove_m1_8421_gshared (List_1_t1_913 * __this, UIVertex_t6_153  ___item, const MethodInfo* method);
#define List_1_Remove_m1_8421(__this, ___item, method) (( bool (*) (List_1_t1_913 *, UIVertex_t6_153 , const MethodInfo*))List_1_Remove_m1_8421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_8422_gshared (List_1_t1_913 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_8422(__this, ___index, method) (( void (*) (List_1_t1_913 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_8422_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UIVertex>::ToArray()
extern "C" UIVertexU5BU5D_t6_261* List_1_ToArray_m1_8423_gshared (List_1_t1_913 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_8423(__this, method) (( UIVertexU5BU5D_t6_261* (*) (List_1_t1_913 *, const MethodInfo*))List_1_ToArray_m1_8423_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_8424_gshared (List_1_t1_913 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_8424(__this, method) (( int32_t (*) (List_1_t1_913 *, const MethodInfo*))List_1_get_Capacity_m1_8424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_8425_gshared (List_1_t1_913 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_8425(__this, ___value, method) (( void (*) (List_1_t1_913 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_8425_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t List_1_get_Count_m1_8426_gshared (List_1_t1_913 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_8426(__this, method) (( int32_t (*) (List_1_t1_913 *, const MethodInfo*))List_1_get_Count_m1_8426_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t6_153  List_1_get_Item_m1_8427_gshared (List_1_t1_913 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_8427(__this, ___index, method) (( UIVertex_t6_153  (*) (List_1_t1_913 *, int32_t, const MethodInfo*))List_1_get_Item_m1_8427_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_8428_gshared (List_1_t1_913 * __this, int32_t ___index, UIVertex_t6_153  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_8428(__this, ___index, ___value, method) (( void (*) (List_1_t1_913 *, int32_t, UIVertex_t6_153 , const MethodInfo*))List_1_set_Item_m1_8428_gshared)(__this, ___index, ___value, method)
