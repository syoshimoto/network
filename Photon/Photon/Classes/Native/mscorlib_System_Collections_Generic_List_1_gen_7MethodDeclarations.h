﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1_914;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>
struct IEnumerable_1_t1_1440;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t1_1441;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>
struct ICollection_1_t1_1442;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t6_262;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t1_1236;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void List_1__ctor_m1_8454_gshared (List_1_t1_914 * __this, const MethodInfo* method);
#define List_1__ctor_m1_8454(__this, method) (( void (*) (List_1_t1_914 *, const MethodInfo*))List_1__ctor_m1_8454_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_8455_gshared (List_1_t1_914 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_8455(__this, ___collection, method) (( void (*) (List_1_t1_914 *, Object_t*, const MethodInfo*))List_1__ctor_m1_8455_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_5601_gshared (List_1_t1_914 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_5601(__this, ___capacity, method) (( void (*) (List_1_t1_914 *, int32_t, const MethodInfo*))List_1__ctor_m1_5601_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void List_1__cctor_m1_8456_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_8456(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_8456_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_8457_gshared (List_1_t1_914 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_8457(__this, method) (( Object_t* (*) (List_1_t1_914 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_8457_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_8458_gshared (List_1_t1_914 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_8458(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_914 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_8458_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_8459_gshared (List_1_t1_914 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_8459(__this, method) (( Object_t * (*) (List_1_t1_914 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_8459_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_8460_gshared (List_1_t1_914 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_8460(__this, ___item, method) (( int32_t (*) (List_1_t1_914 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_8460_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_8461_gshared (List_1_t1_914 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_8461(__this, ___item, method) (( bool (*) (List_1_t1_914 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_8461_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_8462_gshared (List_1_t1_914 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_8462(__this, ___item, method) (( int32_t (*) (List_1_t1_914 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_8462_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_8463_gshared (List_1_t1_914 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_8463(__this, ___index, ___item, method) (( void (*) (List_1_t1_914 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_8463_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_8464_gshared (List_1_t1_914 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_8464(__this, ___item, method) (( void (*) (List_1_t1_914 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_8464_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_8465_gshared (List_1_t1_914 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_8465(__this, method) (( bool (*) (List_1_t1_914 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_8465_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_8466_gshared (List_1_t1_914 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_8466(__this, method) (( Object_t * (*) (List_1_t1_914 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_8466_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_8467_gshared (List_1_t1_914 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_8467(__this, ___index, method) (( Object_t * (*) (List_1_t1_914 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_8467_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_8468_gshared (List_1_t1_914 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_8468(__this, ___index, ___value, method) (( void (*) (List_1_t1_914 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_8468_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void List_1_Add_m1_8469_gshared (List_1_t1_914 * __this, UICharInfo_t6_149  ___item, const MethodInfo* method);
#define List_1_Add_m1_8469(__this, ___item, method) (( void (*) (List_1_t1_914 *, UICharInfo_t6_149 , const MethodInfo*))List_1_Add_m1_8469_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_8470_gshared (List_1_t1_914 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_8470(__this, ___newCount, method) (( void (*) (List_1_t1_914 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_8470_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_8471_gshared (List_1_t1_914 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_8471(__this, ___collection, method) (( void (*) (List_1_t1_914 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_8471_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_8472_gshared (List_1_t1_914 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_8472(__this, ___enumerable, method) (( void (*) (List_1_t1_914 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_8472_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_8473_gshared (List_1_t1_914 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_8473(__this, ___collection, method) (( void (*) (List_1_t1_914 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_8473_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Clear()
extern "C" void List_1_Clear_m1_8474_gshared (List_1_t1_914 * __this, const MethodInfo* method);
#define List_1_Clear_m1_8474(__this, method) (( void (*) (List_1_t1_914 *, const MethodInfo*))List_1_Clear_m1_8474_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool List_1_Contains_m1_8475_gshared (List_1_t1_914 * __this, UICharInfo_t6_149  ___item, const MethodInfo* method);
#define List_1_Contains_m1_8475(__this, ___item, method) (( bool (*) (List_1_t1_914 *, UICharInfo_t6_149 , const MethodInfo*))List_1_Contains_m1_8475_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_8476_gshared (List_1_t1_914 * __this, UICharInfoU5BU5D_t6_262* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_8476(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_914 *, UICharInfoU5BU5D_t6_262*, int32_t, const MethodInfo*))List_1_CopyTo_m1_8476_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_8477_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_1236 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_8477(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_1236 *, const MethodInfo*))List_1_CheckMatch_m1_8477_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_8478_gshared (List_1_t1_914 * __this, Predicate_1_t1_1236 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_8478(__this, ___match, method) (( int32_t (*) (List_1_t1_914 *, Predicate_1_t1_1236 *, const MethodInfo*))List_1_FindIndex_m1_8478_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_8479_gshared (List_1_t1_914 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_1236 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_8479(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_914 *, int32_t, int32_t, Predicate_1_t1_1236 *, const MethodInfo*))List_1_GetIndex_m1_8479_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Enumerator_t1_1233  List_1_GetEnumerator_m1_8480_gshared (List_1_t1_914 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_8480(__this, method) (( Enumerator_t1_1233  (*) (List_1_t1_914 *, const MethodInfo*))List_1_GetEnumerator_m1_8480_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_8481_gshared (List_1_t1_914 * __this, UICharInfo_t6_149  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_8481(__this, ___item, method) (( int32_t (*) (List_1_t1_914 *, UICharInfo_t6_149 , const MethodInfo*))List_1_IndexOf_m1_8481_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_8482_gshared (List_1_t1_914 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_8482(__this, ___start, ___delta, method) (( void (*) (List_1_t1_914 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_8482_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_8483_gshared (List_1_t1_914 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_8483(__this, ___index, method) (( void (*) (List_1_t1_914 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_8483_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_8484_gshared (List_1_t1_914 * __this, int32_t ___index, UICharInfo_t6_149  ___item, const MethodInfo* method);
#define List_1_Insert_m1_8484(__this, ___index, ___item, method) (( void (*) (List_1_t1_914 *, int32_t, UICharInfo_t6_149 , const MethodInfo*))List_1_Insert_m1_8484_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_8485_gshared (List_1_t1_914 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_8485(__this, ___collection, method) (( void (*) (List_1_t1_914 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_8485_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool List_1_Remove_m1_8486_gshared (List_1_t1_914 * __this, UICharInfo_t6_149  ___item, const MethodInfo* method);
#define List_1_Remove_m1_8486(__this, ___item, method) (( bool (*) (List_1_t1_914 *, UICharInfo_t6_149 , const MethodInfo*))List_1_Remove_m1_8486_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_8487_gshared (List_1_t1_914 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_8487(__this, ___index, method) (( void (*) (List_1_t1_914 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_8487_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UICharInfo>::ToArray()
extern "C" UICharInfoU5BU5D_t6_262* List_1_ToArray_m1_8488_gshared (List_1_t1_914 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_8488(__this, method) (( UICharInfoU5BU5D_t6_262* (*) (List_1_t1_914 *, const MethodInfo*))List_1_ToArray_m1_8488_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_8489_gshared (List_1_t1_914 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_8489(__this, method) (( int32_t (*) (List_1_t1_914 *, const MethodInfo*))List_1_get_Capacity_m1_8489_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_8490_gshared (List_1_t1_914 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_8490(__this, ___value, method) (( void (*) (List_1_t1_914 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_8490_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m1_8491_gshared (List_1_t1_914 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_8491(__this, method) (( int32_t (*) (List_1_t1_914 *, const MethodInfo*))List_1_get_Count_m1_8491_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t6_149  List_1_get_Item_m1_8492_gshared (List_1_t1_914 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_8492(__this, ___index, method) (( UICharInfo_t6_149  (*) (List_1_t1_914 *, int32_t, const MethodInfo*))List_1_get_Item_m1_8492_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_8493_gshared (List_1_t1_914 * __this, int32_t ___index, UICharInfo_t6_149  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_8493(__this, ___index, ___value, method) (( void (*) (List_1_t1_914 *, int32_t, UICharInfo_t6_149 , const MethodInfo*))List_1_set_Item_m1_8493_gshared)(__this, ___index, ___value, method)
