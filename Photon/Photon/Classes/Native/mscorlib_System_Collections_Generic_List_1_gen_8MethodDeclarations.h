﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1_915;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UILineInfo>
struct IEnumerable_1_t1_1443;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t1_1444;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_130;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>
struct ICollection_1_t1_1445;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t6_263;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t1_1241;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void List_1__ctor_m1_8519_gshared (List_1_t1_915 * __this, const MethodInfo* method);
#define List_1__ctor_m1_8519(__this, method) (( void (*) (List_1_t1_915 *, const MethodInfo*))List_1__ctor_m1_8519_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_8520_gshared (List_1_t1_915 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_8520(__this, ___collection, method) (( void (*) (List_1_t1_915 *, Object_t*, const MethodInfo*))List_1__ctor_m1_8520_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_5602_gshared (List_1_t1_915 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_5602(__this, ___capacity, method) (( void (*) (List_1_t1_915 *, int32_t, const MethodInfo*))List_1__ctor_m1_5602_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.cctor()
extern "C" void List_1__cctor_m1_8521_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_8521(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_8521_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_8522_gshared (List_1_t1_915 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_8522(__this, method) (( Object_t* (*) (List_1_t1_915 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_8522_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_8523_gshared (List_1_t1_915 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_8523(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_915 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_8523_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_8524_gshared (List_1_t1_915 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_8524(__this, method) (( Object_t * (*) (List_1_t1_915 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_8524_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_8525_gshared (List_1_t1_915 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_8525(__this, ___item, method) (( int32_t (*) (List_1_t1_915 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_8525_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_8526_gshared (List_1_t1_915 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_8526(__this, ___item, method) (( bool (*) (List_1_t1_915 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_8526_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_8527_gshared (List_1_t1_915 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_8527(__this, ___item, method) (( int32_t (*) (List_1_t1_915 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_8527_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_8528_gshared (List_1_t1_915 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_8528(__this, ___index, ___item, method) (( void (*) (List_1_t1_915 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_8528_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_8529_gshared (List_1_t1_915 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_8529(__this, ___item, method) (( void (*) (List_1_t1_915 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_8529_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_8530_gshared (List_1_t1_915 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_8530(__this, method) (( bool (*) (List_1_t1_915 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_8530_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_8531_gshared (List_1_t1_915 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_8531(__this, method) (( Object_t * (*) (List_1_t1_915 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_8531_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_8532_gshared (List_1_t1_915 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_8532(__this, ___index, method) (( Object_t * (*) (List_1_t1_915 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_8532_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_8533_gshared (List_1_t1_915 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_8533(__this, ___index, ___value, method) (( void (*) (List_1_t1_915 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_8533_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void List_1_Add_m1_8534_gshared (List_1_t1_915 * __this, UILineInfo_t6_150  ___item, const MethodInfo* method);
#define List_1_Add_m1_8534(__this, ___item, method) (( void (*) (List_1_t1_915 *, UILineInfo_t6_150 , const MethodInfo*))List_1_Add_m1_8534_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_8535_gshared (List_1_t1_915 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_8535(__this, ___newCount, method) (( void (*) (List_1_t1_915 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_8535_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_8536_gshared (List_1_t1_915 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_8536(__this, ___collection, method) (( void (*) (List_1_t1_915 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_8536_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_8537_gshared (List_1_t1_915 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_8537(__this, ___enumerable, method) (( void (*) (List_1_t1_915 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_8537_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_8538_gshared (List_1_t1_915 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_8538(__this, ___collection, method) (( void (*) (List_1_t1_915 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_8538_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Clear()
extern "C" void List_1_Clear_m1_8539_gshared (List_1_t1_915 * __this, const MethodInfo* method);
#define List_1_Clear_m1_8539(__this, method) (( void (*) (List_1_t1_915 *, const MethodInfo*))List_1_Clear_m1_8539_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool List_1_Contains_m1_8540_gshared (List_1_t1_915 * __this, UILineInfo_t6_150  ___item, const MethodInfo* method);
#define List_1_Contains_m1_8540(__this, ___item, method) (( bool (*) (List_1_t1_915 *, UILineInfo_t6_150 , const MethodInfo*))List_1_Contains_m1_8540_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_8541_gshared (List_1_t1_915 * __this, UILineInfoU5BU5D_t6_263* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_8541(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_915 *, UILineInfoU5BU5D_t6_263*, int32_t, const MethodInfo*))List_1_CopyTo_m1_8541_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_8542_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_1241 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_8542(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_1241 *, const MethodInfo*))List_1_CheckMatch_m1_8542_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_8543_gshared (List_1_t1_915 * __this, Predicate_1_t1_1241 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_8543(__this, ___match, method) (( int32_t (*) (List_1_t1_915 *, Predicate_1_t1_1241 *, const MethodInfo*))List_1_FindIndex_m1_8543_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_8544_gshared (List_1_t1_915 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_1241 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_8544(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_915 *, int32_t, int32_t, Predicate_1_t1_1241 *, const MethodInfo*))List_1_GetIndex_m1_8544_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Enumerator_t1_1238  List_1_GetEnumerator_m1_8545_gshared (List_1_t1_915 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_8545(__this, method) (( Enumerator_t1_1238  (*) (List_1_t1_915 *, const MethodInfo*))List_1_GetEnumerator_m1_8545_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_8546_gshared (List_1_t1_915 * __this, UILineInfo_t6_150  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_8546(__this, ___item, method) (( int32_t (*) (List_1_t1_915 *, UILineInfo_t6_150 , const MethodInfo*))List_1_IndexOf_m1_8546_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_8547_gshared (List_1_t1_915 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_8547(__this, ___start, ___delta, method) (( void (*) (List_1_t1_915 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_8547_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_8548_gshared (List_1_t1_915 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_8548(__this, ___index, method) (( void (*) (List_1_t1_915 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_8548_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_8549_gshared (List_1_t1_915 * __this, int32_t ___index, UILineInfo_t6_150  ___item, const MethodInfo* method);
#define List_1_Insert_m1_8549(__this, ___index, ___item, method) (( void (*) (List_1_t1_915 *, int32_t, UILineInfo_t6_150 , const MethodInfo*))List_1_Insert_m1_8549_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_8550_gshared (List_1_t1_915 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_8550(__this, ___collection, method) (( void (*) (List_1_t1_915 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_8550_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool List_1_Remove_m1_8551_gshared (List_1_t1_915 * __this, UILineInfo_t6_150  ___item, const MethodInfo* method);
#define List_1_Remove_m1_8551(__this, ___item, method) (( bool (*) (List_1_t1_915 *, UILineInfo_t6_150 , const MethodInfo*))List_1_Remove_m1_8551_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_8552_gshared (List_1_t1_915 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_8552(__this, ___index, method) (( void (*) (List_1_t1_915 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_8552_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UILineInfo>::ToArray()
extern "C" UILineInfoU5BU5D_t6_263* List_1_ToArray_m1_8553_gshared (List_1_t1_915 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_8553(__this, method) (( UILineInfoU5BU5D_t6_263* (*) (List_1_t1_915 *, const MethodInfo*))List_1_ToArray_m1_8553_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_8554_gshared (List_1_t1_915 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_8554(__this, method) (( int32_t (*) (List_1_t1_915 *, const MethodInfo*))List_1_get_Capacity_m1_8554_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_8555_gshared (List_1_t1_915 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_8555(__this, ___value, method) (( void (*) (List_1_t1_915 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_8555_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m1_8556_gshared (List_1_t1_915 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_8556(__this, method) (( int32_t (*) (List_1_t1_915 *, const MethodInfo*))List_1_get_Count_m1_8556_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t6_150  List_1_get_Item_m1_8557_gshared (List_1_t1_915 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_8557(__this, ___index, method) (( UILineInfo_t6_150  (*) (List_1_t1_915 *, int32_t, const MethodInfo*))List_1_get_Item_m1_8557_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_8558_gshared (List_1_t1_915 * __this, int32_t ___index, UILineInfo_t6_150  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_8558(__this, ___index, ___value, method) (( void (*) (List_1_t1_915 *, int32_t, UILineInfo_t6_150 , const MethodInfo*))List_1_set_Item_m1_8558_gshared)(__this, ___index, ___value, method)
