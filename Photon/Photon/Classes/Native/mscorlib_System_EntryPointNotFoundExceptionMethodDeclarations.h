﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.EntryPointNotFoundException
struct EntryPointNotFoundException_t1_715;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_177;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.EntryPointNotFoundException::.ctor()
extern "C" void EntryPointNotFoundException__ctor_m1_5017 (EntryPointNotFoundException_t1_715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EntryPointNotFoundException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void EntryPointNotFoundException__ctor_m1_5018 (EntryPointNotFoundException_t1_715 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
