﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.BinaryWriter
struct BinaryWriter_t1_224;
// System.IO.Stream
struct Stream_t1_221;
// System.Text.Encoding
struct Encoding_t1_222;
// System.Byte[]
struct ByteU5BU5D_t1_71;

#include "mscorlib_System_Object.h"

// System.IO.BinaryWriter
struct  BinaryWriter_t1_224  : public Object_t
{
	// System.IO.Stream System.IO.BinaryWriter::OutStream
	Stream_t1_221 * ___OutStream_1;
	// System.Text.Encoding System.IO.BinaryWriter::m_encoding
	Encoding_t1_222 * ___m_encoding_2;
	// System.Byte[] System.IO.BinaryWriter::buffer
	ByteU5BU5D_t1_71* ___buffer_3;
	// System.Boolean System.IO.BinaryWriter::disposed
	bool ___disposed_4;
};
struct BinaryWriter_t1_224_StaticFields{
	// System.IO.BinaryWriter System.IO.BinaryWriter::Null
	BinaryWriter_t1_224 * ___Null_0;
};
