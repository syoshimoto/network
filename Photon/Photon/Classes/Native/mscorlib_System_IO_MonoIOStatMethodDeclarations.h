﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void MonoIOStat_t1_243_marshal(const MonoIOStat_t1_243& unmarshaled, MonoIOStat_t1_243_marshaled& marshaled);
extern "C" void MonoIOStat_t1_243_marshal_back(const MonoIOStat_t1_243_marshaled& marshaled, MonoIOStat_t1_243& unmarshaled);
extern "C" void MonoIOStat_t1_243_marshal_cleanup(MonoIOStat_t1_243_marshaled& marshaled);
