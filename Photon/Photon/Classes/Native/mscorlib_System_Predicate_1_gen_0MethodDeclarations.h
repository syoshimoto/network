﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen_1MethodDeclarations.h"

// System.Void System.Predicate`1<PhotonAnimatorView/SynchronizedParameter>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m1_5682(__this, ___object, ___method, method) (( void (*) (Predicate_1_t1_970 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m1_5800_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<PhotonAnimatorView/SynchronizedParameter>::Invoke(T)
#define Predicate_1_Invoke_m1_9595(__this, ___obj, method) (( bool (*) (Predicate_1_t1_970 *, SynchronizedParameter_t8_128 *, const MethodInfo*))Predicate_1_Invoke_m1_5801_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<PhotonAnimatorView/SynchronizedParameter>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1_9596(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t1_970 *, SynchronizedParameter_t8_128 *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m1_5802_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<PhotonAnimatorView/SynchronizedParameter>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m1_9597(__this, ___result, method) (( bool (*) (Predicate_1_t1_970 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m1_5803_gshared)(__this, ___result, method)
