﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.Hash
struct Hash_t1_622;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_177;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_71;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Security.Policy.Hash::.ctor()
extern "C" void Hash__ctor_m1_4114 (Hash_t1_622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Hash::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Hash__ctor_m1_4115 (Hash_t1_622 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Hash::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Hash_GetObjectData_m1_4116 (Hash_t1_622 * __this, SerializationInfo_t1_177 * ___info, StreamingContext_t1_514  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.Hash::ToString()
extern "C" String_t* Hash_ToString_m1_4117 (Hash_t1_622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Policy.Hash::GetData()
extern "C" ByteU5BU5D_t1_71* Hash_GetData_m1_4118 (Hash_t1_622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
